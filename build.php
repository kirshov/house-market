<?php
class build
{
	/**
	 *
	 */
	protected $path;

	/**
	 *
	 */
	protected $pubicPath;

	/**
	 * build constructor.
	 */
	public function __construct()
	{
		error_reporting(E_ALL ^ E_NOTICE);
		$this->path = dirname(__FILE__).'/';

		if (is_dir($this->path.'www')) {
			$this->pubicPath = 'www';
		} else {
			$this->pubicPath = 'public';
		}

		require_once $this->path.'protected/modules/webforma/helpers/Helper.php';
	}

	/**
	 *
	 */
	public function init()
	{
		$this->clean();
		$this->createDirectoryIfNotExist($this->path.$this->pubicPath.'/assets', 0777);
		$this->createDirectoryIfNotExist($this->path.$this->pubicPath.'/uploads', 0777);
		$this->createDirectoryIfNotExist($this->path.$this->pubicPath.'/responsivefm', 0777);
		$this->createDirectoryIfNotExist($this->path.$this->pubicPath.'/uploads/thumbs', 0777);
		$this->createDirectoryIfNotExist($this->path.'protected/runtime', 0777);

		Helper::clearDir($this->path.'protected/config/modules/');
		$this->createDirectoryIfNotExist($this->path.'protected/config/modules', 0777);
		copy($this->path.'protected/modules/install/install/install.php', $this->path.'protected/config/modules/install.php');

		$this->createGitIgnore($this->path.'protected/runtime/');
		$this->createGitIgnore($this->path.$this->pubicPath.'/assets/');
		$this->createGitIgnore($this->path.$this->pubicPath.'/responsivefm/');
		$this->createGitIgnore($this->path.$this->pubicPath.'/uploads/thumbs/');
		//copy($this->path.'protected/config/db.back.php', $this->path.'protected/config/db.php');

		@unlink($this->path.$this->pubicPath.'/robots.src.txt');
		@unlink($this->path.$this->pubicPath.'/robots.txt');
		@unlink($this->path.$this->pubicPath.'/favicon.ico');
		@unlink($this->path.$this->pubicPath.'/uploads/logo.png');
	}

	/**
	 *
	 */
	public function clean()
	{
		Helper::clearDir($this->path.'protected/runtime/');
		Helper::clearDir($this->path.$this->pubicPath.'/assets/');
		Helper::clearDir($this->path.$this->pubicPath.'/responsivefm/');
		Helper::clearDir($this->path.$this->pubicPath.'/uploads/', true, ['email', 'help']);
	}

	/**
	 * @param $path
	 */
	protected function createGitIgnore($path){
		if(!file_exists($path.'.gitignore')){
			file_put_contents($path.'.gitignore', "*\n!.gitignore");
		}
	}

	/**
	 * @param $dir
	 * @param $mode
	 * @return bool
	 */
	public static function createDirectoryIfNotExist($dir, $mode = null){
		if(!is_dir($dir)){
			return self::createDirectory($dir, $mode);
		}
		return true;
	}

	/**
	 * @param $dst
	 * @param null $mode
	 * @param bool $recursive
	 * @return bool
	 */
	public static function createDirectory($dst,$mode=null,$recursive=false){
		if ($mode === null)
			$mode = 0777;
		$prevDir = dirname($dst);
		if ($recursive && !is_dir($dst) && !is_dir($prevDir))
			self::createDirectory(dirname($dst), $mode, true);
		$res = mkdir($dst, $mode);
		@chmod($dst, $mode);

		return $res;
	}
}

if(isset($_SERVER['argv'][1])){
	$command = $_SERVER['argv'][1];
	$build = new build();
	$build->{$command}();
}