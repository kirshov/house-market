<?php
/**
 * Entry point script
 **/

/**
 * @link    http://www.yiiframework.ru/doc/guide/ru/basics.entry
 */

//define('COOKIE_DOMAIN', '.domain.ru');

// Setting internal encoding to UTF-8.
if (!ini_get('mbstring.internal_encoding')) {
    @ini_set("mbstring.internal_encoding", 'UTF-8');
    mb_internal_encoding('UTF-8');
}

define('IS_BACKEND', mb_strpos($_SERVER['REQUEST_URI'], '/backend/') !== false);
// Comment next two lines on production
define('YII_DEBUG', true);

if(defined('YII_DEBUG') && YII_DEBUG){
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
	error_reporting(E_ALL ^ E_NOTICE);
	define('YII_ENABLE_ERROR_HANDLER',false);
	define('YII_ENABLE_EXCEPTION_HANDLER',false);
} else {
	error_reporting(0);
}

require __DIR__ . '/../vendor/yiisoft/yii/framework/yii.php';

$base = require __DIR__ . '/../protected/config/main.php';

if(IS_BACKEND){
	$backend = require __DIR__ . '/../protected/config/backend.php';
	$base = CMap::mergeArray($base, $backend);
} /*else {
	$frontend = require __DIR__ . '/../protected/config/frontend.php';
	$base = CMap::mergeArray($base, $frontend);
}*/

$confManager = new webforma\components\ConfigManager();
$confManager->sentEnv(\webforma\components\ConfigManager::ENV_WEB);

require __DIR__ . '/../vendor/autoload.php';

Yii::createWebApplication($confManager->merge($base))->run();
