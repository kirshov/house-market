/*
	jQuery autoComplete v1
    Copyright (c) 2020 FORMA.ru
	License: http://www.opensource.org/licenses/mit-license.php
*/

(function($){
    var defaultOptions = {
            delay: 150,
            length: 3,
            url: false,
        },
        methods = {
            init: function(options) {
                this.each(function () {
                    var _this = $(this);
                    options = $.extend(defaultOptions, options);
                    if(!options.url){
                        options.url = _this.closest('form').attr('action');
                    }

                    _this.closest('form')
                        .wrap('<div class="autocomplete-widget"/>').wrap('<div class="autocomplete-control"/>')
                        .closest('.autocomplete-widget').append('<div class="autocomplete-result hidden"/>');

                    _this.resultWrapper = _this.closest('.autocomplete-widget').find('.autocomplete-result');

                    _this.on('keydown.autocomplete', function(e){
                        switch (e.which) {
                            case 13: //enter
                                if(_this.resultWrapper.find('.search-result__item.selected').length){
                                    window.location.href = _this.resultWrapper.find('.search-result__item.selected a').attr('href');
                                    _this.resultWrapper.html('').addClass('hidden');
                                    return false;
                                }
                                return true;
                            case 27: //esc
                                _this.resultWrapper.html('').addClass('hidden');
                                _this.val('');
                            case 38: //up
                            case 40: //down
                                var selected = _this.resultWrapper.find('.search-result__item.selected'),
                                    next;

                                if(selected.length){
                                    var index = _this.resultWrapper.find('.search-result__item').index(selected);
                                    if(e.which == 38){
                                        index--;
                                    } else {
                                        index++;
                                    }

                                    next = _this.resultWrapper.find('.search-result__item').eq(index);
                                }
                                if(!next || !next.length){
                                    if(e.which == 38){
                                        next = _this.resultWrapper.find('.search-result__item:last');
                                    } else {
                                        next = _this.resultWrapper.find('.search-result__item:first');
                                    }
                                }
                                _this.resultWrapper.find('.search-result__item').removeClass('selected');
                                next.addClass('selected');

                                return false;
                        }
                    }).on('input.autocomplete', function () {
                        var q = $(this).val();
                        if(q.length < options.length || !options.url){
                            _this.resultWrapper.html('').removeClass('hidden');
                        }

                        setTimeout(function(){
                            $.get(options.url, {'q': q}, function (response) {
                                _this.resultWrapper.html(response).removeClass('hidden');
                            });
                        }, options.delay);
                    }).on('focus.autocomplete', function () {
                        if(_this.resultWrapper.html()){
                            _this.resultWrapper.removeClass('hidden');
                        }
                    });
                });

                $('body').on('click', function (event) {
                    if(!$(event.target).closest('.autocomplete-widget').length){
                        $('.autocomplete-result').addClass('hidden');
                    } else {
                        $('.autocomplete-result').not($(event.target).closest('.autocomplete-widget').find('.autocomplete-result')).addClass('hidden');
                    }
                });
            },
            destroy: function() {
            }
        };

    $.fn.autocomplete = function(method){
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || ! method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('autocomplete: method ' +  method + ' not found');
        }
    };
}(jQuery));
