$(document).ready(function(){

	$('.input-price').on('keyup', function () {
		var price = 0;
		price = $(this).val().replace(/[^\d]+/g, '');

		var max = parseInt(price);

		if(isNaN(max)){
			max = 0;
		}

		$(this).closest('.feedback-form__inner').find('.pay_slider').slider('option', {'max': max});
	});

	$.each($('.pay_slider'), function() {
		initSlider($(this));
	});

    headerMigrate();
	$(window).resize(function(event) {
		headerMigrate();
	});

	$('.product-calc__button').on('click', function () {
		var modal = $('.popup-ipoteka'),
			price = parseInt($(this).data('price'));

		openModal(modal, $(this));

		if(!isNaN(price)){
			modal.find('.input-price').val(number_format(price, 0, '.', ' '));
			modal.find('.input-price').attr('data-price', price);
			modal.find('.pay_slider').slider('option', {'max': price});
		}
	});

	$('.dictionary-item.item-14 a').on('click', function () {
		var modal = $('.cooperation-popup');

		openModal(modal, $(this));

		return false;
	});

	$('.compare-toggle').on('change', function () {
		if($(this).is(':checked')){
			$('.compare-page .compare__characteristic-item-wrap.same').addClass('hidden');
		} else {
			$('.compare-page .compare__characteristic-item-wrap.same').removeClass('hidden');
		}
		return false;
	});

	$('.product-item__img-gallery .product-gallery__item')
		.on('mouseover', function () {
			var wrapper = $(this).closest('.product-item__img'),
				index = $(this).closest('.product-item__img-gallery').find('.product-gallery__item').index($(this));
			wrapper.find('.product-item__img-wrapper img').attr('src', $(this).find('img').attr('src'));

			wrapper.find('.product-item__img-bullets .product-bullets__item')
				.removeClass('active')
				.eq(index)
				.addClass('active');

			if(index == 4){
				wrapper.find('.product-item__img-more').show();
			} else {
				wrapper.find('.product-item__img-more').hide();
			}
		});


	$('.product-item__img').on('mouseleave', function () {
		$(this).find('.product-item__img-wrapper img').attr('src', $(this).find('.product-gallery__item:first img').attr('src'));

		$(this).find('.product-item__img-bullets .product-bullets__item')
			.removeClass('active')
			.eq(0)
			.addClass('active');

		$(this).find('.product-item__img-more').hide();
	});

	$('#price_from, #price_to, .input-money').mask('000 000 000 000 000', {reverse: true});

	$('.copy-link-compare').on('click', function () {
		var _this = $(this),
			copyText = document.getElementById('compareLink');
		copyText.select();
		document.execCommand('copy');
		_this.addClass('copied');
		setTimeout(function () {
			_this.removeClass('copied');
		},4000);
	});
});

function deleteCompare(){
	var posIds = [];
	$.each($('.compare-col.product-item'), function(){
		posIds.push($(this).data('id'));
	});
	var currentLink = $('#compareLink').data('url') + '?items=' + posIds.join(',');
	$('#compareLink').val(currentLink);

	try {
		history.pushState(null, null, currentLink);
		return;
	} catch(e) {}
}

function initSlider(_this) {
	var max = _this.closest('.ipoteka-wrapper').find('.input-price').val();

	max = max.replace(/[^\d]+/g, '');
	max = parseInt(max);

	if(isNaN(max)){
		max = 0;
	}

	_this.slider({
		min: 0,
		max: max,
		step: 10000,
		slide: function (event, ui) {
			_this.closest('.form-group').find('.input-pay').val(number_format(ui.value, 0, '.', ' '));

			var max = _this.closest('.ipoteka-wrapper').find('.input-price').val(),
				p;

			max = max.replace(/[^\d]+/g, '');
			max = parseInt(max);

			if(!isNaN(max) && max > 0) {
				p = Math.round(ui.value / max * 100);
			} else {
				p = 0;
			}

			_this.closest('.ipoteka-wrapper').find('.pay_percent').html(p + '%');
		}
	});
}

function headerMigrate() {
	if ($(window).width() < 861) {

	} else {

	}

    if ($(window).width() < 650) {

    } else {

    }
}

function number_format( number, decimals, dec_point, thousands_sep ) {
	var i, j, kw, kd, km, minus = "";

	if(number < 0){
		minus = "-";
		number = number*-1;
	}

	// input sanitation & defaults
	if( isNaN(decimals = Math.abs(decimals)) ){
		decimals = 2;
	}
	if( dec_point == undefined ){
		dec_point = ",";
	}
	if( thousands_sep == undefined ){
		thousands_sep = ".";
	}

	i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

	if( (j = i.length) > 3 ){
		j = j % 3;
	} else{
		j = 0;
	}

	km = (j ? i.substr(0, j) + thousands_sep : "");
	kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
	//kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
	kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");


	return minus + km + kw + kd;
}