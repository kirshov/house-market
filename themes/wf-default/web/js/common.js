var doTarget = function(target) {
	try{
		doTargetRun(target);
	} catch (error){}
};

$(document).ready(function(){
	$('a[href^="tel:"]').on('click', function () {
		doTarget('call');
	});

	$('.lt_search_opener').on('click', function(){
		$('.l_header .bottom_side').slideToggle(50);
		$('.search-bar__input:first').trigger('focus');
	});

	$('body.mobile .b-link-item').on('click', function(event){
		if($(event.target).hasClass('b-link-item')){
			window.location = $(this).find('a').attr('href');
		}
	});

	if($('.lh_search .search-bar__input').length){
		$('.lh_search .search-bar__input').autoComplete({
			source: function(term, response){
				$.getJSON($('form.search-bar__form').attr('action'), { q: term }, function(data){
					response(data);
				});
			},
			minChars: 3,
			cache: false,
			delay: 50,
			menuClass: 'site-search'
		});
	}

	$('body').on('click', '.modal-show', function() {
		var modal = $('.modal-window.' + $(this).data('modal'));
		if(modal.length == 0){
			return false;
		}
		openModal(modal, $(this));

		return false;
	});

	$(document).on('keydown', function(e) {
		if( e.keyCode === 27 ) {
			formCloseAll();
			return false;
		}
	});

	$('body').on('click', '.modal-close', function () {
		formCloseAll();
	});

	applyStyler();

	$('body').on('change', '#disable-cart-window', function () {
		var _this = $(this);
		$.ajax({
			url: disableCartWindowUrl,
			data: {status: _this.is(':checked')}
		});
	});

	$('.privacy-bottom__close').on('click', function () {
		$(this).closest('.privacy-bottom').slideUp(100);
		$.ajax({
			url: $(this).data('href')
		});
	});

	$('a.anchor-scroll').click(function(event) {
		event.preventDefault();
		var hash = $(this).attr('href').split('#')[1],
			destination = $('[name="'+ hash + '"]').offset().top;

		if(destination){
			$('html, body').animate({
				scrollTop: destination
			}, 300);
		}
	});

	if($('.input-phone').length){
		$.each($('.input-phone'), function () {
			setPhoneMask($(this));
		});
	}

	$('body').on('click', '#overlay', function(){
		formCloseAll();
	});

	$('.tabs.js-tabs-mobile li.tabs__item .tabs__link').on('click', function () {
		var wrapper = $(this).closest('.tabs__list'),
			li = $(this).closest('li'),
			delay = 150;
		if(li.hasClass('active')){
			return false;
		}
		$.each(wrapper.find('li.active'), function () {
			var _li = $(this);
			_li.find('.tabs__body').slideUp(delay, function () {
				_li.removeClass('active');
			});
		});
		li.addClass('active').find('.tabs__body').slideDown(delay);
	});

	$('.faq-widget .faq-title').on('click', function(){
		$(this).closest('faq-item').toggleClass('active');
		$(this).siblings('.faq-body').slideToggle(250);
	});

	$('.catalog-controls__view_type a').on('click', function (e) {
		e.preventDefault();
		$('body').removeClass('view-type-list').removeClass('view-type-block').addClass('view-type-' + $(this).data('type'));
		$(this).closest('.catalog-controls__view_type').find('a').removeClass('active');
		$(this).addClass('active');
		$.get($(this).data('href'));
	});

	$('body').on('click', '.remove-file', function(){
		$(this).closest('.form__input-item').find('input[type="file"]').val();
		$(this).closest('.form__input-item').removeClass('is-file');
		$(this).closest('.file-item').html('');
	});


	$('.mobile .main__product-tabs .tabs__link').on('click', function(){
		if($(this).hasClass('active')) {
			$(this).removeClass('active');
			$(this).siblings('.tabs__body').stop(true, true).slideUp(250);
		} else {
			/*$('.mobile .main__product-tabs .tabs__link').removeClass('active');
			$('.mobile .main__product-tabs .tabs__body').slideUp(250);*/
			$(this).addClass('active');
			$(this).siblings('.tabs__body').stop(true, true).slideDown(250);
		}

	});

    //menu
    $('.lh__burger').click(function(){
        $('.l_header').toggleClass('menu-open');
        return false;
    });

    var headerHeight = $('.l_header').outerHeight();

    if($(window).width() > 991) {
        $(window).on('load scroll resize', function() {
            //fix header
            if ($(window).scrollTop() > 1) {
                $('.site_container').addClass('header-fix');
                $('.site_container').css('paddingTop', headerHeight);
            } else {
                $('.site_container').removeClass('header-fix');
                $('.site_container').css('paddingTop', 0);
            }
        });
	}
});

function applyStyler() {
	$('input.styler, select.styler, input[type="radio"], input[type="checkbox"]').styler();
}

function setPhoneMask(elem){
	elem.on('paste input', function (event) {
		try {
			var l = elem.val().length,
				lastKey = elem.val()[l - 1],
				prevStr = elem.val().substr(0, l - 1);

			if(lastKey == '8'){
				if(!prevStr || prevStr == '+' || prevStr == '+7' || prevStr == '+7 '){
					elem.val('+7');
					return false;
				}
			}
		} catch (e){}

		var phone = $(this).val().replace(/\D/g, ''),
			phoneFull = $(this).val().replace(/[^\d\+]/g, ''),
			firstChar = phone.substr(0, 1);

		if(phoneFull.substr(0, 1) == '+'){
			if(phoneFull.length > 12){
				$(this).val('+7' + phoneFull.substr(2, 10));
			}
		} else if((firstChar == '8' && phone.length > 10) || ((firstChar == '+' || firstChar == '7') && phone.length > 11)){
			$(this).val('+7' + phone.substr(1, 10));
		}
	});
	elem.mask('+7 (000) 000-00-00');
}


function isFilterOpened() {
	return $('.catalog-filter').hasClass('opened');
}

function getWindowWidth() {
	return Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
}

function getWindowHeight() {
	return Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
}

function openModal(modal, _this) {
	var scrollTop = $(window).scrollTop();
	_this = _this || false;

	var top = scrollTop;
	if(modal.outerHeight(true) > $(window).height()){
		top = scrollTop + 10;
	} else {
		top = scrollTop + ($(window).height() - modal.outerHeight(true)) / 2;
	}

	modal
		.css('top', top)
		.fadeIn(300).find('input:text:first').trigger('focus');

	var title = false;
	if(_this && _this.data('title')){
		title = _this.data('title');
	} else {
		title = modal.find('.modal-title').data('default');
	}

	if(title){
		modal.find('.modal-title').html(title);
	}

	if(_this && _this.data('theme')){
		if(modal.find('form .theme').length == 0){
			modal.find('form').append('<input type="hidden" name="theme" class="theme dynamic-theme">');
		}
		modal.find('form .theme').val(_this.data('theme'));
	} else {
		if(modal.find('form .theme').length != 0){
			modal.find('form .theme').val('');
		}
	}

	if(_this && _this.data('text')){
		modal.find('form input.text-input').val(_this.data('text'));
	} else {
		modal.find('form input.text-input').val('');
	}

	createOverlay();
	$('#overlay').fadeIn(300);

	if(modal.data('on-show') && window[modal.data('on-show')]){
		window[modal.data('on-show')]();
	}
}

function formCloseAll() {
	$('.modal-window').fadeOut(100, function () {
		$('.modal-window.add-in-cart').remove();
	});

	$('#overlay').remove();
}

function createOverlay() {
	if($('#overlay').length == 0){
		$('body').append('<div id="overlay"/>');
	}
}

function hasScrollbar() {
	return document.body.scrollHeight > document.body.clientHeight;
}

function getScrollbarSize() {
	var outer = document.createElement('div');
	outer.style.visibility = 'hidden';
	outer.style.width = '100px';
	outer.style.msOverflowStyle = 'scrollbar';

	document.body.appendChild(outer);

	var widthNoScroll = outer.offsetWidth;
	outer.style.overflow = 'scroll';

	var inner = document.createElement('div');
	inner.style.width = '100%';
	outer.appendChild(inner);

	var widthWithScroll = inner.offsetWidth;

	outer.parentNode.removeChild(outer);

	return widthNoScroll - widthWithScroll;
}

function fScrollTo(value) {
	var top;
	if(typeof value == 'number'){
		top = value;
	} else if(typeof value == 'object') {
		top = value.offset().top - 10;
	} else {
		return false;
	}
	$('body, html').stop().animate({scrollTop: top}, 200);
}

function attachFile(input) {
	if (input.files && input.files[0]) {
		$(input).closest('.form__input-item').addClass('is-file');
	} else {
		$(input).closest('.form__input-item').removeClass('is-file');
	}
	$(input).closest('.form__input-item').find('.file-item').html('<span>' +  input.files[0].name + '</span> <span class="remove-file">&times;</span>');
}