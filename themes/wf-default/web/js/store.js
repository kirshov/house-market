$(document).ready(function () {
	$('.get-uds').on('click', function () {
		var _this = $(this),
			code = $('.uds-code').val(),
			wrapper = _this.closest('.uds-wrapper');

		if(code){
			$.ajax(_this.data('url') + '?id=' + code, {
				type: 'get',
				dataType: 'json',
				success: function (response) {
					if(response.data){
						wrapper.find('.uds-error').html('');
						wrapper.find('.uds-success').html('');
						wrapper.find('.uds-dropdown').slideDown();
						wrapper.find('.uds-client .uds-value').html(response.data.name);
						wrapper.find('.uds-score .uds-value').html(response.data.scores).attr('data-value', response.data.scores);
					} else {
						wrapper.find('.uds-error').html('Не удалось найти код!');
						wrapper.find('.uds-success').html('');
						wrapper.find('.uds-dropdown').slideUp();
						wrapper.find('.uds-client .uds-value').html('');
						wrapper.find('.uds-score .uds-value').html('');
					}
				}
			});
		}
	});

	$('.do-uds').on('click', function(){
		var _this = $(this),
			score = parseFloat($('.uds-score .uds-value').attr('data-value')),
			wrapper = _this.closest('.uds-wrapper'),
			currentScore = parseFloat($('.uds-score').val()).toFixed(0);

			if(currentScore){
				$.ajax(_this.data('url') + '?score=' + currentScore, {
					type: 'get',
					dataType: 'json',
					success: function (response) {
						if(response.data){
							wrapper.find('.uds-error').html('');
							wrapper.find('.uds-success').html('');
						} else {
							wrapper.find('.uds-error').html('');
							wrapper.find('.uds-success').html('');
						}
					}
				});
			}
	});

	$('.uds-score').on('change', function(){
		var score = parseFloat($('.uds-score .uds-value').attr('data-value')),
			currentScore = parseFloat($(this).val()).toFixed(0);

		if(isNaN(currentScore) || currentScore < 0){
			currentScore = 0;
		}

		if(currentScore > score){
			currentScore = score;
		}

		$(this).val(currentScore);
	});

	$.each($('.price_slider'), function(){
        var _this = $(this),
            wrapper = _this.closest('.price_slider_wrap');

        _this.slider({
            range: true,
            min: wrapper.data('min'),
            max: wrapper.data('max'),
            step: 100,
            values: [wrapper.data('from'), wrapper.data('to')],
            slide: function (event, ui) {
                wrapper.find('.filter_input__price_from').val(ui.values[0]);
                wrapper.find('.filter_input__price_to').val(ui.values[1]);
            }
        });
    });

    miniCartListeners();
    refreshDeliveryTypes();
    updateAllCosts();

    $('#start-payment').on('click', function (event) {
    	var _this = $(this),
			form = $('.order-box-payment input.radio__input:checked').closest('.payment-item').find('form'),
			errorBlock = _this.closest('.order-box__bottom').find('.error-block');

		_this.attr('disabled', true);
    	if(form.hasClass('ajax')){
			errorBlock.html('').addClass('hidden');
    		event.preventDefault();
    		$.ajax({
				url: form.attr('action'),
				type: 'post',
				data: form.serialize(),
				dataType: 'json',
				success: function(response){
					if(response.status){
						if(response.url){
							window.location.href = response.url;
						}
					} else {
						errorBlock.html(response.error).removeClass('hidden');
					}
					_this.removeAttr('disabled');
				},
				error: function () {
					errorBlock.html('Произошла внутренняя ошибка, пожалуйста, попробуйте позже').removeClass('hidden');
					_this.removeAttr('disabled');
				}
			});
		} else {
			form.submit();
		}
    });

    $('body').on('click', '.clear-cart', function (e) {
        e.preventDefault();
        var data = {};
        data[webformaTokenName] = webformaToken;
        $.ajax({
            url: '/coupon/clear/',
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.result) {
                    updateCartWidget();
                    updateAllCosts();
                }
            }
        });
    });

    $('.page-cart').on('click', '#add-coupon-code', function (e) {
        e.preventDefault();
        var code = $('#coupon-code').val();
        var button = $(this);
        $('.coupon-box .coupon-messages').addClass('hidden').html('');
        if (code) {
            var data = {'code': code};
            data[webformaTokenName] = webformaToken;
            $.ajax({
                url: '/coupon/add/',
                type: 'post',
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data.result) {
                        $.get('/cart/', {'refresh': 1}, function(response){
                            $('.main__cart-box .main__cart-products').replaceWith(response);
                            updateAllCosts();
                        });
                        //window.location.reload();
                        $('#coupon-code').val('');
                    } else {
                        $('.coupon-box .coupon-messages')
                            .addClass('alert alert-error')
                            .removeClass('hidden')
                            .html(data.data.join('; '));
                    }
                }
            });
        }
    });

    $('.page-cart').on('click', '.coupon .close', function (e) {
        e.preventDefault();
        var code = $(this).closest('.coupon').find('.coupon-input').data('code');
        var data = {'code': code};
        var el = $(this).closest('.coupon');
        data[webformaTokenName] = webformaToken;
        $.ajax({
            url: '/coupon/remove/',
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.result) {
                    el.remove();
                    updateAllCosts();
                }
            }
        });
    });

    $('.page-cart').on('keypress', '#coupon-code', function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $('#add-coupon-code').click();
        }
    });

	$('.page-cart').on('submit', '.order-form', function () {
        $(this).find('button[type="submit"]').prop('disabled', true);
    });

    $('input.product-entry__variant-change, select.product-entry__variant-change').change(function () {
        updatePrice($(this));
		changePhoto($(this));
		var quantity = $(this).data('quantity');
		if(isNaN(quantity) || quantity < 0){
			quantity = 0;
		}
		$('.attributes-quantity').html(quantity);
    });

    $('.product-quantity-increase').on('click', function () {
    	var quantityElement = $(this).closest('.product-quantity-area').find('.product-quantity-input'),
        	quantity = parseInt(quantityElement.val());

    	if(!isNaN(quantity) && quantity > 0){
			quantity++;
		} else {
			quantity = 1;
		}
        quantityElement.val(quantity);
    });

    $('.product-quantity-decrease').on('click', function () {
		var quantityElement = $(this).closest('.product-quantity-area').find('.product-quantity-input'),
			quantity = parseInt(quantityElement.val());

		if(!isNaN(quantity) && quantity > 1) {
			quantity--;
		} else {
			quantity = 1;
		}
		quantityElement.val(quantity);
    });

    $('body').on('click', '.add-product-to-cart', function (e) {
        e.preventDefault();
        var button = $(this);
        var form = $(this).parents('form'),
			productId = form.find('[name="Product[id]"]').val();

		doTarget('toCart');
		$(this).closest('.product-buy-wrapper').find('.buy-message').remove();
        $.ajax({
            type: 'post',
            dataType: 'json',
            data: form.serialize(),
            url: form.attr('action'),
            success: function (data) {
                if (data.result) {
                    $('.add-in-cart').remove();
                    if(typeof data.data.window != 'undefined' && data.data.window != null){
						$('body').append(data.data.window);
						openModal($('.modal-window.add-in-cart'));
					}

                    updateCartWidget();
                } else if(typeof data.data.message != 'undefined'){
					showAddCartMessage(button, data.data.message);
				}
            }
        });
    });


    $('body').on('click', '.quick-add-product-to-cart', function (e) {
        if($(this).hasClass('btn_success')){
            return true;
        }

		e.preventDefault();
		quickAddToCart($(this));
    });

    $('.page-cart').on('click', '.cart-quantity-increase',function () {
        var target = $($(this).data('target'));
        target.val(parseInt(target.val()) + 1).trigger('change');
    });

    $('.page-cart').on('click', '.cart-quantity-decrease',function () {
        var target = $($(this).data('target'));
        if (parseInt(target.val()) > 1) {
            target.val(parseInt(target.val()) - 1).trigger('change');
        }
    });

    $('.page-cart').on('click', '.cart-delete-product', function (e) {
        e.preventDefault();
        var el = $(this);
        var data = {'id': el.data('position-id')};
        data[webformaTokenName] = webformaToken;
        $.ajax({
            url: webformaCartDeleteProductUrl,
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.result) {
                    el.closest('.cart-list__item').remove();

                    if ($('.cart-list .cart-list__item').not('.coupon').length == 0) {
                        $('#order-form').remove();
                        $('.cart__empty').removeClass('hidden');
                    }
                    $('#cart-total-product-count').text($('.cart-list .cart-item').length);
                    updateCartTotalCost();
                    updateCartWidget();
                    calculateMinSumPrice();
                }
            }
        });
    });

    $('body').on('change', '.position-count', function () {
        var el = $(this).parents('.cart-list__item');
        var quantity = parseInt(el.find('.position-count').val());
        var productId = el.find('.position-id').val();

        if (quantity <= 0 || isNaN(quantity)) {
            quantity = 1;
        }

		if(!isNaN(el.find('.position-count').data('quantity'))){
			var maxQuantity = el.find('.position-count').data('quantity');
			if(quantity > maxQuantity){
				quantity = maxQuantity;
				showAddCartMessage($(this), 'Нельзя добавить товара больше, чем есть в наличии');
			}
		}
		el.find('.position-count').val(quantity);

        updatePositionSumPrice(el);
        changePositionQuantity(productId, quantity);
    });

	$('body').on('keydown', '.position-count', function (event) {
		if(event.keyCode == 13){
			$(this).trigger('change');
			return false;
		}
	});

    $('.page-cart').on('change', 'input[name="Order[delivery_id]"]', function () {
        updateShippingCost($(this));
    });

    $('body').on('click', '.buy-one-click__button', function(){
		var modal = $('.show-popup');

		openModal(modal, $(this));

    	return true;
        if($(this).hasClass('loaded')){
            return false;
        }
        var _this = $(this),
            wrapper = _this.closest('.product-wrapper'),
            count = 1;

        if(wrapper.find('.spinput__value').length){
            count = parseInt(wrapper.find('.spinput__value').val());
        }
        if(isNaN(count) || count < 1){
            count = 1;
        }

        var data = {
            id: _this.data('product-id'),
            count: count,
            variant: []
        };

        $.each(wrapper.find('form .product-entry__variants').find('select option:selected'), function () {
            data['variant'].push($(this).val());
        });
        $.each(wrapper.find('form .product-entry__variants').find('input:checked'), function () {
            data['variant'].push($(this).val());
        });
        if(data.variant.length > 0){
            data.variant.sort(function(a,b){
                return a.value.prop > b.value.prop ? -1 : 1;
            });
        }

        var positionId = _this.data('product-id') + '_' + data.variant.join('_'),
            windowClass = 'body > .modal-window.buy-one-click.product_' + positionId;

        _this.addClass('loaded');

        if($(windowClass).length) {
            openModal($(windowClass));
            _this.removeClass('loaded');
            return false;
        }

        $.ajax({
            url: _this.data('url'),
            type: 'get',
            data: data,
            success: function (data) {
                _this.removeClass('loaded');
                $('body').append(data);
                if($(windowClass).find('#Order_phone').length){
                    setPhoneMask($(windowClass).find('#Order_phone'));
                }
                openModal($(windowClass));
            }
        });
    });


    $('body').on('submit', '.buy-one-click__form', function(){
        var form = $(this),
            otherError = 'Неизвестная ошибка, пожалуйста, повторите попытку позже.',
            wrapper = $(this).closest('.form-wrapper');

        $.ajax({
            url: $('.modal-window.buy-one-click').data('url'),
            type: 'POST',
            data: form.serialize(),
            success: function (response) {
				var productId = form.find('[name*="[product_id]"]').val();

                form.find('#Order_name_em_').html('').hide();
                form.find('#Order_phone_em_').html('').hide();
                wrapper.find('.alert-block').html('').hide();
                if (response.result) {
                    form.trigger('reset');
                    wrapper.closest('.right').find('.modal-title').addClass('hidden');
                    wrapper.closest('.modal-content').find('.cart-modal-inner__product-characteristics').addClass('hidden');
                    wrapper.html(response.data);
					doTarget('orderOneClick');
                } else {
                    try{
                        if(typeof response.data.errors != 'undefined'){
                            if(response.data.errors.name){
                                form.find('#Order_name_em_').html(response.data.errors.name).show();
                            }
                            if(response.data.errors.phone){
                                form.find('#Order_phone_em_').html(response.data.errors.phone).show();
                            }
                            if(response.data.errors.total_price){
                                wrapper.find('.alert-block').html(response.data.errors.total_price[0]).show();
                            }

							if(typeof response == 'object' && Object.keys(response).length) {
								wrapper.find('.alert-block').html('<p>Необходимо исправить следующие ошибки:</p><ul>');
								$.each(response.data.errors, function (index, value) {
									$.each(value, function (key, item) {
										wrapper.find('.alert-block').append('<li>' + item + '</li>');
									});
								});
								wrapper.find('.alert-block').append('</ul>');
								wrapper.find('.alert-block').show();
							}
                        } else {
                            wrapper.find('.alert-block').html(otherError).show();
                        }
                    } catch (e) {
                        wrapper.find('.alert-block').html(otherError).show();
                    }
                }
            },
            error: function () {
                form.find('.alert-block').html(otherError);
            }
        });
        return false;
    });

    if($('.product-gallery__body .product-images').length){
		$('.product-gallery__img-wrap a[data-fancybox]').on('click', function(event){
			$('.product-gallery__body .product-images a[data-index="0"]').trigger('click');
			event.preventDefault();
			return false;
		});

		/*if($('a[data-fancybox]').length){
			try{
				$('body.gadget').swipe({
					swipeLeft: function() {
						$.fancybox.next();
					},
					swipeRight: function() {
						$.fancybox.prev();
					},
				});
			} catch (e) {}
		}*/
	}

    $('.filter-expand').on('click', function(){
    	var _this = $(this),
			expandBlock = _this.closest('.filter-block__body').find('.filter-block__list-expand');

    	if(_this.hasClass('expanded')){
			_this.removeClass('expanded').html(_this.data('closed'));
			expandBlock.slideUp(150);
		} else {
			_this.addClass('expanded').html(_this.data('expanded'));
			expandBlock.slideDown(150);
		}
	});
   /* $('.to-brand-lines').on('click', function(){
        var wrap = $(this).closest('.brand__lines');
        if($(this).hasClass('expanded')){
            $(this)
                .html($(this).data('title-expand'))
                .removeClass('expanded');
            wrap.find('.brand__line-items').slideUp(100);
        } else {
            $(this)
                .html($(this).data('title-expanded'))
                .addClass('expanded');
            wrap.find('.brand__line-items').slideDown(100);
        }
    });

    $('.product-gallery__body .product-gallery__img-wrap').on('click', 'a', function(e){
    	e.preventDefault();
		var wrap = $(this).closest('.product-gallery__body').find('.product-images');

		wrap.find('a[href="' + $(this).attr('href') + '"] img').trigger('click');
	});*/
});

function showAddCartMessage(_this, message) {
	_this.closest('.product-buy-wrapper').prepend('<div class="buy-message">' + message + '</div>');
	setTimeout(function (){
		$('.buy-message').fadeOut(150, function () {
			$(this).remove();
		});
	}, 5000);
}
function changePhoto(_this){
	var wrap = _this.closest('.product-detail').find('.product-gallery__body');
	if(wrap.find('img').length <= 1){
		return;
	}
	var imageId;
	if(_this.get(0).tagName == 'SELECT'){
		imageId = parseInt(_this.find('option:selected').data('image'));
	} else {
		imageId = parseInt(_this.data('image'));
	}
	if(isNaN(imageId)){
		imageId = 0;
	}
	var el = wrap.find('.product-images').find('a[data-index="' + imageId + '"]'),
		index = wrap.find('.product-images').index(el);

	wrap.find('.product-gallery__img-wrap a').attr('href', el.attr('href'));
	wrap.find('.product-gallery__img-wrap a img').attr('src', el.data('big'));
}

function quickAddToCart(el) {
	var data = {'Product[id]': el.data('product-id')};
	data[webformaTokenName] = webformaToken;
	doTarget('toCart');
	$.ajax({
		url: el.data('cart-add-url'),
		type: 'post',
		data: data,
		dataType: 'json',
		success: function (data) {
			if (data.result) {
				$('.add-in-cart').remove();
				if(typeof data.data.window != 'undefined'){
					$('body').append(data.data.window);
					openModal($('.modal-window.add-in-cart'));
				}

				updateCartWidget();
				el.off('click','.quick-add-product-to-cart');
				el.removeClass('btn_cart')
					.addClass('btn_success')
					.html('Оформить заказ')
					.attr('href', '/cart');
			}
		}
	});
}

function miniCartListeners() {
	$('.mini-cart-delete-product').click(function (e) {
		e.preventDefault();
		var el = $(this);
		var data = {'id': el.data('position-id')};
		data[webformaTokenName] = webformaToken;
		$.ajax({
			url: webformaCartDeleteProductUrl,
			type: 'post',
			data: data,
			dataType: 'json',
			success: function (data) {
				if (data.result) {
					updateCartWidget();
				}
			}
		});
	});

	$('#cart-toggle-link').click(function (e) {
		e.preventDefault();
		$('#cart-mini').toggle();
	});
}

function getClearTotalCost(){
    var cost = 0;
    $.each($('.position-sum-price'), function (index, elem) {
        var item = $(this).closest('.cart-item');
        var count = parseInt(item.find('.position-count').val());
        var price = parseFloat(item.find('.position-price').data('price'));
        cost += price * count;
    });

    return cost;
}

function getCartTotalCost(onlyDelta) {
	var cost = getClearTotalCost(),
        costForDiscount = 0,
        delta = 0,
        deltaTemp = 0,
        coupons = getCoupons();
	onlyDelta = onlyDelta || false;

    costForDiscount = cost;

	$.each(coupons, function (index, el) {
		deltaTemp = 0;
		switch (el.type) {
			case 0: // руб
				deltaTemp = parseFloat(el.value);
				break;
			case 1: // %
				deltaTemp = (parseFloat(el.value) / 100) * costForDiscount;
				break;
		}
		if(deltaTemp > 0){
			var decimals = parseInt(storeRoundPrice) > 0 ? 0 : 2;
			deltaTemp = Math.round(deltaTemp, decimals);
			delta += deltaTemp;
			costForDiscount -= deltaTemp;
		}
	});
    if(onlyDelta){
        return delta > cost ? cost : delta;
    }
	return delta > cost ? 0 : cost - delta;
}

function updateCartTotalCost() {
	$('#cart-full-cost').html(formatPrice(getClearTotalCost()));
	var discount = getCartTotalCost(true);
    $('#coupon-full-cost').html(formatPrice(discount));
	if(discount && discount > 0){
        $('#coupon-full-cost').closest('.cart-box__subtotal.subtotal-coupon').removeClass('hidden');
    } else {
        $('#coupon-full-cost').closest('.cart-box__subtotal.subtotal-coupon').addClass('hidden');
    }

	refreshDeliveryTypes();
	updateShippingCost();
	updateFullCostWithShipping();
	updateDeliveryPrices();
}

function refreshDeliveryTypes() {
	var cartTotalCost = getClearTotalCost();
	$.each($('input[name="Order[delivery_id]"]'), function (index, el) {
		var elem = $(el);
		var availableFrom = elem.data('available-from');
		if (availableFrom.length && parseFloat(availableFrom) >= cartTotalCost) {
			elem.prop('disabled', true);
		} else {
			elem.prop('disabled', false);
		}
	});
}

function getShippingCost() {
	var cartTotalCost = getClearTotalCost();
	var coupons = getCoupons();
	var freeShipping = false;
	$.each(coupons, function (index, el) {
		if (el.free_shipping && cartTotalCost >= parseFloat(el.min_order_price)) {
			freeShipping = true;
		}
	});
	if (freeShipping) {
		return 0;
	}
	var selectedDeliveryType = $('input[name="Order[delivery_id]"]:checked');
	if (!selectedDeliveryType[0]) {
		return 0;
	}
	if (parseInt(selectedDeliveryType.data('separate-payment')) || parseFloat(selectedDeliveryType.data('free-from')) <= cartTotalCost) {
		return 0;
	} else {
		return parseFloat(selectedDeliveryType.attr('data-price'));
	}
}

function updateShippingCost(_this) {
	_this = _this || $('input[name="Order[delivery_id]"]').filter(':checked');
	if(_this.length > 0){
		if(_this.data('need-address')){
			$('.order-box-delivery__address').stop(true, true).slideDown(200);
		} else {
			$('.order-box-delivery__address').stop(true, true).slideUp(200);
		}
	}

	$('#cart-shipping-cost').html(getShippingCost());
	updateFullCostWithShipping();
}

function updateFullCostWithShipping() {
	$('#cart-full-cost-with-shipping').html(formatPrice(getShippingCost() + getCartTotalCost()));
}

function updateAllCosts() {
	updateCartTotalCost();
}

function updatePrice(_this) {
	var productWrapper = _this.closest('.product-wrapper'),
		_basePrice = parseInt(productWrapper.find('.base-price').val()),
        //hasBasePriceVariant = false,
		varElements,
		option;

	if(_this.get(0).tagName == 'SELECT'){
		_this = _this.find('option:selected');
		varElements = productWrapper.find('select.product-entry__variant-change option:selected');
	} else {
		varElements = productWrapper.find('input.product-entry__variant-change');
	}

	var oldPrice = _this.data('old-price'),
        discount = _this.data('discount'),
        currency = _this.data('currency');

	/* выбираем вариант, меняющий базовую цену максимально*/
    $.each(varElements, function (index, elem) {
        var varId = $(elem).val(),
			variantAmount;
        if (varId) {
        	if($(elem).get(0).tagName == 'OPTION'){
                option = $(elem);//.find('option[value="' + varId + '"]');
			} else {
                option = $(elem).filter('[value="' + varId + '"]').filter(':checked');
			}
        	if(!option.length){
        		return true;
			}

			variantAmount = option.data('amount');
            switch (option.data('type')) {
                case 2: // base price
                    // еще не было варианта
                   /* if (!hasBasePriceVariant) {
                        _basePrice = variant.amount;
                        hasBasePriceVariant = true;
                    }
                    else {*/
                        if (_basePrice < variantAmount) {
                            _basePrice = variantAmount;
                        }
                    //}
					break;
            }
        }
    });

    var newPrice = _basePrice;
    $.each(varElements, function (index, elem) {
        var varId = $(elem).val();
        if (varId) {
			if($(elem).get(0).tagName == 'OPTION'){
                option = $(elem);//.find('option[value="' + varId + '"]');
            } else {
                option = $(elem).filter('[value="' + varId + '"]').filter(':checked');
            }
			if(!option.length){
				return true;
			}
            switch (option.data('type')) {
                case 0: // sum
                    newPrice += parseInt(option.data('amount'));
                    break;
                case 1: // percent
                    newPrice += _basePrice * (parseInt(option.data('amount')) / 100);
                    break;
				case 2: // sum
					newPrice = parseInt(option.data('amount'));
					break;
				case 3: // percent
					newPrice -= parseInt(option.data('amount'));
					break;
				/*default: // sum
					newPrice = variant.amount;
					break;*/
            }
        }
    });

	var prefix = productWrapper.hasClass('product-detail') ? '.product-detail' : '.product-item';
	if($('.product-item__price-old').length){
	    if(discount){
            productWrapper.find(prefix + '__discount').removeClass('hidden-opacity');
        } else {
            productWrapper.find(prefix + '__discount').addClass('hidden-opacity');
        }

        productWrapper.find(prefix + '__discount span').html(discount);
	    if(oldPrice > 0){
            productWrapper.find(prefix + '__price-old span.price-old__value').html(formatPrice(parseFloat(oldPrice).toFixed(2)) + ' ' + currency);
			productWrapper.find(prefix + '__price-old').removeClass('hidden-opacity');
        } else {
            productWrapper.find(prefix + '__price-old span.price-old__value').html('');
            productWrapper.find(prefix + '__price-old').addClass('hidden-opacity');
        }
    }

    var price = formatPrice(parseFloat(newPrice).toFixed(2));
	productWrapper.find('.result-price .result-price-value').html(price);
}

function updateCartWidget() {
	$('#shopping-cart-widget').load($('#cart-widget').data('cart-widget-url'), function () {
		miniCartListeners();
	});
}

function getCoupons() {
	var coupons = [];
	$.each($('.coupon-input'), function (index, elem) {
		var $elem = $(elem);
		coupons.push({
			code: $elem.data('code'),
			name: $elem.data('name'),
			value: $elem.data('value'),
			type: $elem.data('type'),
			min_order_price: $elem.data('min-order-price'),
			free_shipping: $elem.data('free-shipping')
		});
	});
	return coupons;
}

function updatePositionSumPrice(tr) {
	var count = parseInt(tr.find('.position-count').val());
	var price = parseFloat(tr.find('.position-price').data('price'));
	tr.find('.position-sum-price').html(formatPrice(price * count));
	updateCartTotalCost();
}

function changePositionQuantity(productId, quantity) {
	var data = {'quantity': quantity, 'id': productId};
	data[webformaTokenName] = webformaToken;
	$.ajax({
		url: webformaCartUpdateUrl,
		type: 'post',
		data: data,
		dataType: 'json',
		success: function (data) {
			if (data.result) {
				updateCartWidget();

				calculateMinSumPrice();
			}
		}
	});
}

function updateDeliveryPrices() {
	var productCost = getClearTotalCost();
	var realPrice = 0;
	$('.order-box-delivery .order-radio__item').each(function(){
		if($(this).find('.real-price').hasClass('loader')){
			return true;
		}
		var elem = $(this).find('input.radio__input');

		if(productCost > elem.data('free-from')){
			realPrice = elem.data('free-text') ? elem.data('free-text') : 'Бесплатно';
		} else {
			realPrice = formatPrice(elem.attr('data-price')) + ' ' + elem.data('currency');
		}
		$(this).find('.real-price').html(realPrice);
	});
}

function formatPrice(number, decimals, dec_point, thousands_sep) {
    var i, j, kw, kd, km, minus = '';

    if(number < 0){
        minus = '-';
        number = number*-1;
    }

    // input sanitation & defaults
    if( isNaN(decimals = Math.abs(decimals)) ){
        decimals = parseInt(storeRoundPrice) > 0 ? 0 : 2;
        if(decimals == 2){
        	decimals = parseInt(number) == number ? 0 : 2;
		}
    }
    if( dec_point == undefined ){
        dec_point = '.';
    }
    if( thousands_sep == undefined ){
        thousands_sep = ' ';
    }

    i = parseInt(number = (+number || 0).toFixed(decimals)) + '';

    if( (j = i.length) > 3 ){
        j = j % 3;
    } else{
        j = 0;
    }
    km = (j ? i.substr(0, j) + thousands_sep : '');
    kw = i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thousands_sep);
    kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : '');

    return minus + km + kw + kd;
}

function calculateMinSumPrice() {
    var minSumOrder = $('#order-form').data('min-sum'),
        updateCart = false;

    if(minSumOrder && minSumOrder > 0){
        var cost = getClearTotalCost(),
            hasMinSumWrapper = $('.cart__min-sum').length;

        if(cost >= minSumOrder && hasMinSumWrapper){
            updateCart = true;
        } else if(cost < minSumOrder && !hasMinSumWrapper) {
            updateCart = true;
        }
    }

    if(updateCart){
        $.get('/cart/', {'get-cart': 1}, function(response){
            $('.cart-order-wrapper').replaceWith(response);
            updateCartTotalCost();
        });
    } else {
        updateCartTotalCost();
    }
}
