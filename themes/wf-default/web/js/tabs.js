(function ($) {
    'use strict';

    if (typeof $.tabs !== 'undefined') {
        throw('$.tabs is already defined');
    }

    $.tabs = function (el, options) {
        var ACTIVE_CLASS = 'active',
            root = this,
            $root = $(root);

        root.$el = $(el);
        root.$nav = root.$el.find('[data-nav]').first();

        root.init = function () {
            root.options = $.extend({}, $.tabs.defaultOptions, options);
            root.$nav.on('click', '[href]', function () {
                var current = $root.data().current,
                    $this = $(this),
                    tabID = $this.attr('href').substring(1),
                    $bodiesWrapper = root.$el.find('.js-tabs-bodies-holder');

                if ((tabID !== current)) {
                    $bodiesWrapper.find('#' + current).fadeOut(root.options.speed, function () {
                        var $target = root.$el.find('#' + tabID);

                        $target.fadeIn(root.options.speed);

                        root.$nav.find('.' + ACTIVE_CLASS).removeClass(ACTIVE_CLASS);
                        $this.addClass(ACTIVE_CLASS);

                        history.pushState(null,null,'#' + tabID);

                        $target.find('.js-slick').each(function () {
                            $(this).slider();
                        });
                    });
                }

                $root.data().current = tabID;

                return false;
            });

            (function () {
                var hash = window.location.hash,
                    $bodiesWrapper = root.$el.find('.js-tabs-bodies-holder').first(),
                    $first = root.$nav.find('.' + ACTIVE_CLASS);

                if ($first.length) {
                    hash = $first.attr('href');
                } else {
                    $first = null;
                }

                if (hash) {
                    $first = root.$nav.find('[href=' + hash + ']');
                }

                if (!$first || !$first.length) {
                    $first = root.$nav.find('[href]').first();
                    hash = $first.attr('href');
                }

                if(hash){
                    $root.data().current = hash.substring(1);
                }
                $bodiesWrapper.children('.js-tab').not(hash).hide();
                $bodiesWrapper.find(hash).show();

                $first.addClass(ACTIVE_CLASS);
            })();

            $(window).bind('hashchange', function () {
                if(window.location.hash){
                    $('[href=' + window.location.hash + ']').trigger('click');
                }
            });
        };

        root.init();
    };

    $.tabs.defaultOptions = {
        speed: 0
    };

    $.fn.tabs = function (options) {
        return this.each(function () {
            (new $.tabs(this, options));
        });
    };
})(jQuery);
