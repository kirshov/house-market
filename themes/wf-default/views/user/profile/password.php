<?php
/**
 * @var $model ProfileForm
 * @var $form CActiveForm
 */

$title = Yii::t('UserModule.user', 'Change password');
$this->breadcrumbs = [
    Yii::t('UserModule.user', 'User profile') => ['/user/profile/profile'],
	$title
];
$this->title = Helper::prepareSeoTitle($title);
$this->setTitle($title);
$this->setTemplateVar('bodyClass', 'page-profile');

$this->layout = '//layouts/profile';

Yii::app()->clientScript->registerScript(
    'show-password',
    "$(function () {
    $('#show_pass').on('change', function () {
        $('#ProfilePasswordForm_password').prop('type', $(this).prop('checked') ? 'text' : 'password');
        $('#ProfilePasswordForm_cPassword').prop('type', $(this).prop('checked') ? 'text' : 'password');
    });
});"
);
?>


<div class="page page-user-profile">
	<?php
	$form = $this->beginWidget('CActiveForm', [
		'id' => 'profile-password-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => true,
		'htmlOptions' => [
			'class' => 'site-form',
		],
	]); ?>

	<?= $form->errorSummary($model); ?>

	<div class="form__input-item">
		<div class="form-group">
			<?= $form->labelEx($model, 'password'); ?>
			<div class="form-group__input">
				<?= $form->passwordField($model, 'password', ['class' => 'input', 'autocomplete' => 'off']); ?>
			</div>
			<div class="form-group__help hidden">
				<?= $form->error($model, 'password') ?>
			</div>
		</div>
	</div>

	<div class="form__input-item">
		<div class="form-group">
			<?= $form->labelEx($model, 'cPassword'); ?>
			<div class="form-group__input">
				<?= $form->passwordField($model, 'cPassword', ['class' => 'input']); ?>
			</div>
			<div class="form-group__help hidden">
				<?= $form->error($model, 'cPassword') ?>
			</div>
		</div>
	</div>

	<div class="form__input-item">
		<label class="checkbox">
			<input type="checkbox" value="1" id="show_pass"> <?= Yii::t('UserModule.user', 'show password') ?>
		</label>
	</div>

	<div class="form__input-buttons">
		<?= CHtml::submitButton(Yii::t('UserModule.user', 'Change password'), [
			'id' => 'login-btn',
			'class' => 'btn btn_big btn_wide btn_white'
		]) ?>
	</div>
	<?php $this->endWidget(); ?>
</div>