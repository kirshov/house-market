<?php
/**
 * @var CActiveForm $form
 */
$title = Yii::t('UserModule.user', 'User profile');
$this->breadcrumbs = [$title];
$this->title = Helper::prepareSeoTitle($title);
$this->setTitle($title);
$this->setTemplateVar('bodyClass', 'page-profile');

if(Helper::hasFlash('user-registered')){
	Helper::setFlash('user-registered', null);
	Yii::app()->getClientScript()->registerScript('registerYaGoal', 'yaGoal("registration");');
}
$this->layout = '//layouts/profile';
?>

<div class="page page-user-profile">
	<?php $this->widget('webforma\widgets\WFlashMessages'); ?>

    <?php $form = $this->beginWidget('CActiveForm', [
        'id' => 'profile-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
		'htmlOptions' => [
            'validateOnChange' => false,
            'validateOnType' => false,
            'hideErrorMessage' => true,
			'class' => 'site-form',
		],
    ]); ?>

    <?= $form->errorSummary($model); ?>

	<?if(Yii::app()->hasModule('discount')){
		$this->widget('discountInfo');
	} else {
		$discount = Yii::app()->getUser()->getDiscount();
		if($discount && $discount > 0){
			echo '<div class="form__input-item">Ваша персональная скидка: '.$discount.'%</div>';
		}
	}?>

    <div class="form__input-item form__email-change">
        <div class="user__email">
			<div class="profile__change-email">
				<?= $form->labelEx($user, 'email'); ?>
	            <?= CHtml::link(Yii::t('UserModule.user', 'Change email'), ['/user/profile/email']) ?>
	        </div>
            <?= $form->textField($user, 'email', [
                'disabled' => true,
                'class' => 'input input-email'.Yii::app()->getUser()->profile->getIsVerifyEmail() ? ' success' : '',
            ]); ?>
            <?php if (Yii::app()->getUser()->profile->getIsVerifyEmail()): { /*?>
                <p class="email-status-confirmed text-success">
                    <?= Yii::t('UserModule.user', 'E-mail was verified'); ?>
                </p>
            <?php*/ } else: { ?>
                <p class="email-status-not-confirmed text-error">
                    <?= Yii::t('UserModule.user', 'e-mail was not confirmed, please check you mail!'); ?>
                </p>
            <?php } endif ?>
        </div>
    </div>

	<div class="form__input-item">
		<div class="form-group">
			<?= $form->labelEx($model, 'first_name'); ?>
			<div class="form-group__input">
				<?= $form->textField($model, 'first_name', ['class' => 'input']); ?>
			</div>
			<div class="form-group__help hidden">
				<?= $form->error($model, 'first_name') ?>
			</div>
		</div>
	</div>
	<?/*
    <div class="form__input-item">
        <div class="form-group">
            <?= $form->labelEx($model, 'last_name'); ?>
            <div class="form-group__input">
                <?= $form->textField($model, 'last_name', ['class' => 'input']); ?>
            </div>
            <div class="form-group__help hidden">
                <?= $form->error($model, 'last_name') ?>
            </div>
        </div>
    </div>


    <div class="form__input-item">
        <div class="form-group">
            <?= $form->labelEx($model, 'middle_name'); ?>
            <div class="form-group__input">
                <?= $form->textField($model, 'middle_name', ['class' => 'input']); ?>
            </div>
            <div class="form-group__help">
                <?= $form->error($model, 'middle_name') ?>
            </div>
        </div>
    </div>
*/?>
    <div class="form__input-item">
        <div class="form-group">
            <?= $form->labelEx($model, 'phone'); ?>
            <div class="form-group__input">
                <?= $form->textField($model, 'phone', ['class' => 'input input-phone']); ?>
            </div>
            <div class="form-group__help hidden">
                <?= $form->error($model, 'phone') ?>
            </div>
        </div>
    </div>

    <div class="form__input-item form__input-checkbox">
        <div class="form-group">
            <div class="form-group__input">
                <?= $form->checkBox($model, 'news_subscribe', ['class' => 'styler']); ?>
                <?= $form->labelEx($model, 'news_subscribe'); ?>
            </div>
        </div>
    </div>

	<?/*
    <div class="form__input-item">
        <div class="form-group">
            <?= $form->labelEx($model, 'birth_date'); ?>
            <div class="form-group__input">
                <?= $form->dateField($model, 'birth_date', ['class' => 'input']) ?>
            </div>
            <div class="form-group__help">
                <?= $form->error($model, 'birth_date') ?>
            </div>
        </div>
    </div>
	*/?>

    <div class="form__input-buttons">
        <div class="profile__save">
            <?= CHtml::submitButton(Yii::t('UserModule.user', 'Save profile'), ['class' => 'btn']) ?>
        </div>
        <div class="profile__change-pass">
            <?= CHtml::link(Yii::t('UserModule.user', 'Change password'), ['/user/profile/password'], ['class' => 'reset-pass pull-right']) ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>
</div>
