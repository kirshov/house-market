<?php
/**
 * @var CActiveForm $form
 */
$title = Yii::t('UserModule.user', 'Change email');
$this->breadcrumbs = [
    Yii::t('UserModule.user', 'User profile') => ['/user/profile/profile'],
	$title
];
$this->title = Helper::prepareSeoTitle($title);
$this->setTitle($title);
$this->setTemplateVar('bodyClass', 'page-profile');
$this->layout = '//layouts/profile';
?>
<div class="page page-user-profile">
	<?php
	$form = $this->beginWidget('CActiveForm', [
		'id' => 'profile-email-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => true,
		'htmlOptions' => [
			'class' => 'site-form'
		]
	]); ?>

	<?php if (Yii::app()->getUser()->profile->getIsVerifyEmail()) : { ?>
		<div class="bs-callout bs-callout-danger">
			<p>
				<?= Yii::t(
					'UserModule.user',
					'Warning! After changing your e-mail you will receive a message explaining how to verify it'
				); ?>
			</p>
		</div>
	<?php } endif; ?>

	<?= $form->errorSummary($model); ?>

	<div class="form__input-item">
		<div class="form-group">
			<?= $form->labelEx($model, 'email', ['label' => 'Введите новый e-mail']); ?>
			<div class="form-group__input">
				<?= $form->textField($model, 'email', ['class' => 'input']); ?>
			</div>
			<div class="form-group__help hidden">
				<?= $form->error($model, 'email') ?>
			</div>
		</div>
	</div>

	<div class="fast-order__inputs">
		<?= CHtml::submitButton(Yii::t('UserModule.user', 'Change email'), [
			'id' => 'login-btn',
			'class' => 'btn btn_big btn_wide btn_white'
		]) ?>
	</div>
	<?php $this->endWidget(); ?>
</div>
