<?php
/**
 * @var CActiveForm $form
 */
$title = Yii::t('UserModule.user', 'Password recovery');
$this->breadcrumbs = [$title];
$this->title = Helper::prepareSeoTitle($title);
$this->setTitle($title);
$this->setTemplateVar('bodyClass', 'login-page');
?>

<div class="page page-user-profile">
    <?php $form = $this->beginWidget('CActiveForm', [
        'id' => 'recovery-form',
        'enableClientValidation' => true,
        'htmlOptions' => [
            'class' => 'site-form',
        ],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <div class="form__input-item">
        <div class="form-group">
            <?= $form->labelEx($model, 'email', ['label' => Yii::t('UserModule.user', 'Enter an email you have used during signup'),]); ?>
            <div class="form-group__input">
                <?= $form->textField($model, 'email', [
                    'class' => 'input',

                ]); ?>
            </div>
            <div class="form-group__help hidden">
                <?= $form->error($model, 'email') ?>
            </div>
        </div>
    </div>

    <div class="form__input-buttons">
        <?= CHtml::submitButton(Yii::t('UserModule.user', 'Recover password'), [
            'class' => 'btn'
        ]) ?>
    </div>

    <?php $this->endWidget(); ?>
</div>