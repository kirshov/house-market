<?php
/**
 * @var CActiveForm $form
 */
$title = Yii::t('UserModule.user', 'Sign up');
$this->breadcrumbs = [$title];
$this->title = Helper::prepareSeoTitle($title);
$this->setTitle($title);
$this->setTemplateVar('bodyClass', 'login-page');
?>

<div class="page page-user-profile page-user-reg">
    <?php $form = $this->beginWidget('CActiveForm', [
        'id' => 'registration-form',
        'enableClientValidation' => true,
        'htmlOptions' => [
            'class' => 'site-form',
        ],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <div class="form__input-item">
        <div class="form-group">
            <?= $form->labelEx($model, 'first_name'); ?>
            <div class="form-group__input">
                <?= $form->textField($model, 'first_name', [
                    'class' => 'input',

                ]); ?>
            </div>
            <div class="form-group__help hidden">
                <?= $form->error($model, 'first_name') ?>
            </div>
        </div>
    </div>

    <div class="form__input-item">
        <div class="form-group">
            <?= $form->labelEx($model, 'email'); ?>
            <div class="form-group__input">
                <?= $form->textField($model, 'email', [
                    'class' => 'input',

                ]); ?>
            </div>
            <div class="form-group__help hidden">
                <?= $form->error($model, 'email') ?>
            </div>
        </div>
    </div>

    <div class="form__input-item">
        <div class="form-group">
            <?= $form->labelEx($model, 'password'); ?>
            <div class="form-group__input">
                <?= $form->passwordField($model, 'password', [
                    'class' => 'input',

                ]); ?>
            </div>
            <div class="form-group__help hidden">
                <?= $form->error($model, 'password') ?>
            </div>
        </div>
    </div>

    <div class="form__input-item">
        <div class="form-group">
            <?= $form->labelEx($model, 'cPassword'); ?>
            <div class="form-group__input">
                <?= $form->passwordField($model, 'cPassword', [
                    'class' => 'input',

                ]); ?>
            </div>
            <div class="form-group__help hidden">
                <?= $form->error($model, 'cPassword') ?>
            </div>
        </div>
    </div>

    <?php if ($module->showCaptcha && CCaptcha::checkRequirements()): ?>
		<div class="form__input-item form__input-captcha">
			<div class="form-group">
			    <?= $form->labelEx($model, 'verifyCode', ['required' => true]); ?>
				<div class="form-group__input">
				    <?= $form->textField($model, 'verifyCode', ['class' => 'input input-captcha']); ?>
				</div>
			</div>

			<div class="form-image">
			    <?php $this->widget('CCaptcha', [
				    'showRefreshButton' => false,
				    'clickableImage' => true,
				    'imageOptions' => [
					    'height' => '38px',
					    'title' => 'Кликните, чтобы получить другой код',
					    'class' => 'image-captcha',
				    ],
			    ]);?>
			</div>
		</div>
    <?php endif; ?>

    <div class="form__input-buttons">
        <?= CHtml::submitButton(Yii::t('UserModule.user', 'Sign up'), [
            'class' => 'btn'
        ]) ?>
    </div>

    <?php if (Yii::app()->hasModule('social') && 1 == 2): { ?>
        <div class="fast-order__inputs">
            <?php
            $this->widget('vendor.nodge.yii-eauth.EAuthWidget', [
                'action' => '/social/login',
                'predefinedServices' => ['google', 'facebook', 'vkontakte', 'twitter', 'github'],
            ]); ?>
        </div>
    <?php } endif; ?>

    <?php $this->endWidget(); ?>
    <div class="registration_text">
        <p>Если вы впервые на сайте, и хотите, чтобы мы вас<br/> помнили, и все ваши заказы сохранялись, то<br/> пройдите регистрацию</p>
    </div>
</div>
