<?php
/**
 * @var CActiveForm $form
 */
$title = Yii::t('UserModule.user', 'Sign in');
$this->breadcrumbs = [$title];
$this->title = Helper::prepareSeoTitle($title);
$this->setTitle($title);
$this->setTemplateVar('bodyClass', 'login-page');
?>

<div class="page page-user-profile">
	<?php $this->widget('webforma\widgets\WFlashMessages'); ?>

	<?php $form = $this->beginWidget('CActiveForm', [
		'id' => 'login-form',
		'enableClientValidation' => false,
		'htmlOptions' => [
			'class' => 'site-form',
		]
	]); ?>

	<?= $form->errorSummary($model); ?>

	<div class="form__input-item">
		<div class="form-group">
			<?= $form->labelEx($model, 'email'); ?>
			<div class="form-group__input">
				<?= $form->textField($model, 'email', ['class' => 'input']); ?>
			</div>
			<div class="form-group__help">
				<?= $form->error($model, 'email') ?>
			</div>
		</div>
	</div>

	<div class="form__input-item">
		<div class="form-group">
			<?= $form->labelEx($model, 'password'); ?>
			<div class="form-group__input">
				<?= $form->passwordField($model, 'password', ['class' => 'input']); ?>
			</div>
			<div class="form-group__help">
				<?= $form->error($model, 'password') ?>
			</div>
		</div>
	</div>

	<?php if ($this->getModule()->sessionLifeTime > 0): { ?>
		<div class="fast-order__inputs">
			<?= $form->checkBox($model, 'remember_me', ['checked' => true]); ?>
			<?= $form->labelEx($model, 'remember_me'); ?>
		</div>
	<?php } endif; ?>

	<?php if (Yii::app()->getUser()->getState('badLoginCount', 0) >= 3 && CCaptcha::checkRequirements('gd')): { ?>
		<div class="form__input-buttons">
            <div class="form__input-item">
                <div class="form-group">
                    <?= $form->textField($model, 'verifyCode', [
                        'class' => 'input',
                        'placeholder' => Yii::t('UserModule.user', 'Please enter the text from the image')
                    ]); ?>
                </div>
            </div>
			<div class="colfast-order__inputs">
				<?php $this->widget(
					'CCaptcha',
					[
						'showRefreshButton' => true,
						'imageOptions' => [
							'width' => '150',
						],
						'buttonOptions' => [
							'class' => '',
						],
						'buttonLabel' => 'Обновить код',
					]
				); ?>
			</div>
		</div>
	<?php } endif; ?>


	<div class="form__input-buttons">
		<div class="profile__singin-link">
			<?= CHtml::submitButton(Yii::t('UserModule.user', 'Sign in'), [
				'id' => 'login-btn',
				'class' => 'btn'
			]) ?>
		</div>
		<div class="profile__recovery">
			<?= CHtml::link(Yii::t('UserModule.user', 'Forgot your password?'), ['/user/account/recovery']) ?>
		</div>
	</div>
	<div class="profile__reg">
		<?= CHtml::link(Yii::t('UserModule.user', 'Sign up'), Yii::app()->createUrl('/user/account/registration')) ?>
	</div>

	<?php if (Yii::app()->hasModule('social') && 1 == 2): { ?>
		<div class="fast-order__inputs">
			<?php
			$this->widget(
				'vendor.nodge.yii-eauth.EAuthWidget',
				[
					'action' => '/social/login',
					'predefinedServices' => ['google', 'facebook', 'vkontakte', 'twitter'],
				]
			);
			?>
		</div>
	<?php } endif; ?>

	<?php $this->endWidget(); ?>
</div>
