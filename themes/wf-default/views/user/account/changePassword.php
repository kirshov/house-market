<?php
$title = Yii::t('UserModule.user', 'Password recovery');
$this->breadcrumbs = [$title];
$this->title = Helper::prepareSeoTitle($title);
$this->setTitle($title);
$this->setTemplateVar('bodyClass', 'login-page');
?>

<div class="page page-user-profile">
	<?php $form = $this->beginWidget('CActiveForm', [
		'id' => 'recovery-form',
		'enableClientValidation' => true,
		'htmlOptions' => [
			'class' => 'site-form',
		]
	]); ?>

	<?= $form->errorSummary($model); ?>

	<div class="form__input-item">
		<div class="form-group">
			<?= $form->labelEx($model, 'password'); ?>
			<div class="form-group__input">
				<?= $form->passwordField($model, 'password', ['class' => 'input']); ?>
			</div>
			<div class="form-group__help hidden">
				<?= $form->error($model, 'password') ?>
			</div>
		</div>
	</div>

	<div class="form__input-item">
		<div class="form-group">
			<?= $form->labelEx($model, 'cPassword'); ?>
			<div class="form-group__input">
				<?= $form->passwordField($model, 'cPassword', ['class' => 'input']); ?>
			</div>
			<div class="form-group__help hidden">
				<?= $form->error($model, 'cPassword') ?>
			</div>
		</div>
	</div>

	<div class="form__input-buttons">
		<?= CHtml::submitButton(Yii::t('UserModule.user', 'Change password'), [
			'class' => 'btn btn_big btn_wide btn_white'
		]) ?>
	</div>

	<?php $this->endWidget(); ?>
</div>
