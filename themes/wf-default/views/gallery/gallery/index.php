<?php
/**
 * @var $this GalleryController
 * @var $model Gallery
 */

$this->breadcrumbs = [
	Yii::t('GalleryModule.gallery', 'Gallery')
];
$this->setTitle(Yii::t('GalleryModule.gallery', 'Gallery'));
$this->title = Yii::t('GalleryModule.gallery', 'Gallery');
?>
<div class="page page-gallery-list">
	<?php
	$this->widget(
		'zii.widgets.CListView',
		[
			'dataProvider' => $dataProvider,
			'itemView' => '_item',
			'template' => "{items}\n{pager}",
			'summaryText' => '',
			'enableHistory' => true,
			'cssFile' => false,
			'ajaxUpdate' => false,
			'itemsCssClass' => 'gallery__list',
			'htmlOptions' => [
				'class' => 'catalog'
			],
			'pagerCssClass' => 'catalog__pagination',
			'pager' => [
				'header' => '',
				'prevPageLabel' => '<i class="fa fa-long-arrow-left"></i>',
				'nextPageLabel' => '<i class="fa fa-long-arrow-right"></i>',
				'firstPageLabel' => false,
				'lastPageLabel' => false,
				'htmlOptions' => [
					'class' => 'pagination'
				]
			]
		]
	); ?>
</div>