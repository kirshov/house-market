<?$url = Yii::app()->createUrl('gallery/gallery/view', ['id' => $data->id]);?>
<div class="gallery-item">
	<div class="gallery-item__image">
		<a href="<?=$url?>" data-fancybox="gallery" rel="group">
			<?= CHtml::image($data->getImageUrl(226, 226), $data->name) ?>
		</a>
	</div>
	<div class="gallery-item__title">
		<a href="<?=$url?>"><?=$data->name;?></a>
	</div>
</div>
