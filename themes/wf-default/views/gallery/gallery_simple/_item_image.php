<div class="gallery-item">
	<div class="gallery-item__image">
		<a href="<?= $data->getImageUrl() ?>" data-fancybox="gallery" rel="group">
			<?= CHtml::image($data->getImageUrl(226, 226), $data->name) ?>
		</a>
	</div>
</div>
