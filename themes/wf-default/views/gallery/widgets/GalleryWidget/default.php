<?php
/**
 * Отображение для GalleryWidget/gallerywidget:
 **/

if($dataProvider->getTotalItemCount() > 0){
	echo '<div class="h3"><span>Фотогалерея</span> <div class="view_all"><a rel="nofollow" href="'.Yii::app()->createUrl('gallery/gallery/gallery').'">Смотреть все</a></div></div>';

	$this->widget(
		'webforma.widgets.WListView',
		[
			'dataProvider' => $dataProvider,
			'itemView' => '_image',
			'viewData' => [
				'width' => $width,
				'height' => $height,
			],
			'template' => "{items}",
			'summaryText' => '',
			'itemsCssClass' => 'gallery_items',
			'htmlOptions' => [
				'class' => 'gallery_wrap'
			],
			'ajaxUpdate' => false,
		]
	);
}
