<?php
/**
 * Отображение для GalleryWidget/gallerywidget:
 **/

if($dataProvider->getTotalItemCount() > 0){
	$this->widget(
		'webforma.widgets.WListView',
		[
			'dataProvider' => $dataProvider,
			'itemView' => '_image',
			'viewData' => [
				'width' => $width,
				'height' => $height,
			],
			'template' => "{items}",
			'summaryText' => '',
			'itemsCssClass' => 'gallery_items',
			'htmlOptions' => [
				'class' => 'gallery_wrap'
			],
			'ajaxUpdate' => false,
		]
	);
}