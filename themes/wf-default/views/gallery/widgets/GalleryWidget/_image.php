<div class="gallery_item">
	<a href="<?= $data->image->getImageUrl(); ?>" data-id="<?=$data->image->id?>" data-fancybox="gallery" rel="group">
		<img src="<?= $data->image->getImageUrl($width, $height, false); ?>" alt="<?=$data->image->alt?>">
	</a>
</div>