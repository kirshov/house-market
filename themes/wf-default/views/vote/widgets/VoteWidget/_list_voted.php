<?php
/**
 * @var Vote $vote
 */
?>

<div class="vote-inner">
	<div class="vote-list">
		<?foreach ($vote->voteItems as $voteItem):?>
			<div class="vote-list-item">
				<div class="vote-list-top">
					<span class="vote-value">
						<span><?=$voteItem->percent?>%</span>
					</span>
					<span class="vote-label">
						<span><?=$voteItem->title?></span>
					</span>
				</div>
				<div class="vote-percent" style="width: <?=$voteItem->percent?>%;"></div>
			</div>
		<?endforeach;?>
	</div>
</div>
