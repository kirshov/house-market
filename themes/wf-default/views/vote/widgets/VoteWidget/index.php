<?php
/**
 * @var Vote $vote
 */
?>

<?if($vote && $vote->voteItems):?>
	<div class="vote-widget">
		<div class="h3">Опрос</div>
		<div class="vote-wrap">
			<form class="vote-form">
				<div class="vote-title"><?=$vote->name?></div>
				<?if($isVoted){
					$this->renderPartial('_list_voted', ['vote' => $vote]);
				} else {
					$this->renderPartial('_list', ['vote' => $vote]);
				}?>
			</form>
		</div>
	</div>
<?endif;?>
