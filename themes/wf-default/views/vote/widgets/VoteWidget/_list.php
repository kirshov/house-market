<?php
/**
 * @var Vote $vote
 */
?>
<div class="vote-inner">
	<div class="vote-list">
		<?foreach ($vote->voteItems as $voteItem):?>
			<div class="vote-list">
				<input type="radio" name="vote" value="<?=$voteItem->id?>" id="vote-<?=$voteItem->id?>">
				<label for="vote-<?=$voteItem->id?>">
					<span><?=$voteItem->title?></span>
				</label>
			</div>
		<?endforeach;?>
	</div>
</div>
<div class="buttons">
	<input type="submit" value="Голосовать" class="disabled">
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('.vote-list input:radio').on('change', function () {
			$(this).closest('form').find('input:submit').removeClass('disabled');
		});

		$('form.vote-form').on('submit', function (event) {
			event.preventDefault();
			var form = $(this);
			if(form.hasClass('loaded') || form.find('input:submit').hasClass('disabled')){
				return false;
			}

			form.addClass('loaded');

			var data = {};
			data[webformaTokenName] = webformaToken;
			data['vote'] = $('.vote-list input:radio:checked').val();

			$.ajax({
				url: '/set-vote/',
				data: data,
				method: 'post',
				success: function (response) {
					form.closest('.vote-widget').replaceWith(response);
					form.removeClass('loaded');
				}
			});

			return false;
		})
	})
</script>