<?
/**
 * @var DictionaryGroup $model
 * @var DictionaryData[] $items
 * @var bool $displayTitle
 */
?>
<?if($items):?>
	<div class="dictionary-widget dictionary-<?=$model->code?>">
		<?foreach ($items as $item):?>
			<div class="dictionary-item">
				<div class="dictionary-info">
					<div class="dictionary-title"><?=$item->name;?></div>
					<?if($item->description):?>
						<div class="dictionary-body"><?=$item->description;?></div>
					<?endif;?>
				</div>
			</div>
		<?endforeach;?>
	</div>
<?endif;?>