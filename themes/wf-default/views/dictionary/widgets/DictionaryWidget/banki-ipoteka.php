<?
/**
 * @var DictionaryGroup $model
 * @var DictionaryData[] $items
 * @var bool $displayTitle
 */
?>
<?if($items):?>
	<div class="dictionary-widget dictionary-<?=$model->code?>">
		<?foreach ($items as $item):?>
			<?if($item->image):?>
				<div class="dictionary-item">
					<div class="dictionary-image">
						<img src="<?=$item->getImageUrl();?>">
					</div>
				</div>
			<?endif;?>
		<?endforeach;?>
	</div>
<?endif;?>