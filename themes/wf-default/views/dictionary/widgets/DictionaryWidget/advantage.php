<?
/**
 * @var DictionaryGroup $model
 * @var DictionaryData[] $items
 */
?>
<?if($items):?>
	<div class="mp_adv">
		<div class="wrapper">
			<ul>
				<?foreach ($items as $item):?>
					<li>
						<div class="mpa_icon<?=preg_match('/icon_adv[1-4]{1}/', $item->image) ? ' theme-color' : ''?>">
							<img src="<?=$item->getImageUrl();?>">
						</div>
						<div class="mpa_text">
							<strong><?=$item->name;?></strong>
							<br/>
							<?=$item->description;?>
						</div>
					</li>
				<?endforeach;?>
			</ul>
		</div>
	</div>
<?endif;?>
