<?php
/**
 * @var \webforma\components\controllers\FrontController $this
 */
$title = 'Ошибка ' . $error['code'];
$this->title = Helper::prepareSeoTitle($title);
$this->setTitle($title);
$this->setTemplateVar('bodyClass', 'page');
$this->breadcrumbs = [
	$title
];
?>
<div class="page page-error">
    <?php
    switch ($error['code']) {
        case '404':
	        echo '<div class="search-in-page">';
			echo '<div class="error-detail">';
			echo '<p>Страница которую вы запросили не найдена :(</p>';

	        if(Yii::app()->hasModule('store')){
		        echo '<p>Чтобы быстро найти интересующий Вас раздел или товар, воспользуйтесь поиском:</p>';
		        $this->widget('searchProduct');
		        echo '</div>';
	        } elseif(Yii::app()->hasModule('search')) {
		        echo '<p>Чтобы быстро найти интересующий Вас раздел, воспользуйтесь поиском:</p>';
		        $this->widget('search');
		        echo '</div>';
	        }

			if($extend404 && ($product || $category || $producer || $line)){
				echo '<p>Возможно вы искали:</p>';
				if($product){
					echo '<div class="catalog__product-items">';
					$this->renderPartial('//store/product/_item', ['data' => $product]);
					echo '</div>';
				}
				if($category){
					echo '<p><a href="'.Yii::app()->createUrl('/store/category/view', ['path' => $category->getPath()]).'">Товары из категории "'.$category->name.'"</a></p>';
				}
				if($producer){
					echo '<p><a href="'.Yii::app()->createUrl('/store/producer/view', ['slug' => $producer->slug]).'">Товары производителя "'.$producer->getFullName().'"</a></p>';
				}
				if($line){
					echo '<p><a href="'.Yii::app()->createUrl('/store/producer/line', ['slug' => $line->producer->slug, 'line' => $line->slug]).'">Товары линии "'.$line->getFullName().'"</a></p>';
				}
				echo '<p>&nbsp;</p>';
			}



			echo '</div>';
            break;
        default:
            echo '<p class="error-detail">'.$error['message'].'</p>';
            break;
    }
    ?>
</div>
