<?php //if($this->beginCache('home-cats', array('duration' => 3600, 'dependency' => \TaggedCache\TaggingCacheHelper::getDependency(['store', 'store::category'])))):?>
	<div class="main-category-widget">
		<?foreach (StoreCategory::getMainCats(6) as $category):?>
			<div class="main-category__item main-category-<?=$category->id?>">
				<?/*if($category->image):?>
					<div class="main-category__image">
						<img src="<?=$category->getImageUrl()?>" title="<?=$category->getImageTitle()?>" alt="<?=$category->getImageAlt()?>">
					</div>
				<?endif;*/?>

				<div class="main-category__title"><?=$category->name?></div>

				<?$this->widget('storeMainCategory', ['parentId' => $category->id, 'limit' => 5]);?>

				<div class="main-category__more">
					<a href="<?=Yii::app()->createUrl('/store/category/view', ['path' => $category->getPath()]);?>">Все категории</a>
				</div>
			</div>
		<?endforeach;?>
	</div>
	<?//$this->endCache();?>
<?//endif;?>