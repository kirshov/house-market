<?php
Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/tabs.js', CClientScript::POS_END);
Yii::app()->getClientScript()->registerScript('productTabInit', '$(document).ready(function(){$(".js-tabs").tabs();});', CClientScript::POS_END);


$tabs = ['new' => 'Новинки', 'special' => 'Распродажа', 'popular' => 'Хит продаж'];
$tabContent = [];
foreach ($tabs as $tab => $title) {
	$tabContent[$tab] = $this->widget('products', [
		'type' => $tab,
		'view' => 'main',
		'limit' => 8,
		'options' => [
			'wrapper-class' => 'main-'.$tab,
		],
	], true);
}
?>
<?if($tabContent): ?>
	<div class="main-products">
		<div class="tabs js-tabs">
			<div class="js-tabs-holder">
				<ul data-nav="data-nav" class="tabs__list">
					<?foreach ($tabs as $tab => $title):?>
						<?if($tabContent[$tab]):?>
							<li class="tabs__item">
								<a href="#tab-<?=$tab?>" class="tabs__link"><?=$title?></a>
							</li>
						<?endif;?>
					<?endforeach;?>
				</ul>
			</div>

			<div class="tabs__bodies js-tabs-bodies">
				<div class="js-tabs-bodies-holder">
					<?foreach ($tabs as $tab => $title):?>
						<?if($tabContent[$tab]):?>
							<div id="tab-<?=$tab?>" class="tabs__body js-tab">
								<?=$tabContent[$tab];?>
							</div>
						<?endif;?>
					<?endforeach;?>
				</div>
			</div>
		</div>
	</div>
<?endif;?>