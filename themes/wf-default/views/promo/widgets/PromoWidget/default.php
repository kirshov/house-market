<?php if (isset($models) && $models != []): ?>
	<div class="promo-items">
		<?foreach ($models as $model):?>
			<div class="promo-item">
				<div class="promo-items__image">
					<a href="<?= Yii::app()->createUrl('/promo/promo/view/', ['slug' => $model->slug]) ?>">
						<img src="<?= $model->getImageUrl($width, $height, false); ?>" title="<?= CHtml::encode($model->title); ?>"/>
					</a>
				</div>
			</div>
		<?endforeach;?>
	</div>
<?php endif; ?>
