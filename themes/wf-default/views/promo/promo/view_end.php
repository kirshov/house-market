<?php
/**
 * @var PromoController $this
 * @var Promo $promo
 * @var CActiveDataProvider $dataProvider
 */

$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
$cs = Yii::app()->getClientScript();
$cs->registerCoreScript('jquery.ui');
$cs->registerCssFile($cs->getCoreScriptUrl(). '/jui/css/base/jquery-ui.css');

$this->title = $promo->getMetaTitle();
$this->description = $promo->getMetaDescription();
$this->keywords =  $promo->getMetaKeywords();
$this->canonical = $promo->getMetaCanonical();

$this->breadcrumbs = [
    'Акции' => ['/promo/promo/index'],
    $promo->getTitle()
];
//$showBy = $this->renderPartial('/category/_show_by', null, true);

$isGetQuery = Yii::app()->getModule('store')->isGetFilterParams();

$this->setTemplateVar([
	'descriptionsAfterPos' => !$isGetQuery ? $promo->description_bottom : null,
]);
$this->setTemplateVar('bodyClass', 'catalog-page');
$this->setTitle($promo->getTitle());

?>
<div class="promo-top-description">
	<p>Акция закончилась!</p>
</div>