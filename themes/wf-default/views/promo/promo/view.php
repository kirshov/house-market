<?php
/**
 * @var PromoController $this
 * @var Promo $promo
 * @var CActiveDataProvider $dataProvider
 */

$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
$cs = Yii::app()->getClientScript();
$cs->registerCoreScript('jquery.ui');
$cs->registerCssFile($cs->getCoreScriptUrl(). '/jui/css/base/jquery-ui.css');

$this->title = $promo->getMetaTitle();
$this->description = $promo->getMetaDescription();
$this->keywords =  $promo->getMetaKeywords();
$this->canonical = $promo->getMetaCanonical();

$this->breadcrumbs = [
    'Акции' => ['/promo/promo/index'],
    $promo->getTitle()
];
//$showBy = $this->renderPartial('/category/_show_by', null, true);

$isGetQuery = Yii::app()->getModule('store')->isGetFilterParams();

$this->setTemplateVar([
	'descriptionsAfterPos' => !$isGetQuery ? $promo->description_bottom : null,
]);
$this->setTemplateVar('bodyClass', 'catalog-page');


$dates = ' <div class="promo-dates">';
if($promo->date_start){
	$dates .= '<span class="promo_start">Начало: '.$promo->date_start.'</span>';
	if($promo->date_end){
		$dates .= ' | ';
	}
}
if($promo->date_end){
	$dates .= '<span class="promo_start">Завершение: '.$promo->date_end.'</span>';
}
$dates .= '</div>';

$this->setTitle($promo->getTitle().$dates);
?>

<?if(!$isGetQuery && $promo->description):?>
	<div class="promo-top-description">
		<?=$promo->description?>
	</div>
<?endif;?>

<?if($dataProvider && $dataProvider->getTotalItemCount() > 0):?>
	<div class="catalog">
		<?/*$this->renderPartial('/category/_filter', [
			'category' => null,
			'producersOnFilter' => $producers,
		]);*/?>
		<?$this->renderPartial('//store/category/_product_list', ['dataProvider' => $dataProvider]);?>
	</div>
<?endif;?>