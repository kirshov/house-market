<?php
/**
 * @var ProductGroupsController $this
 */

$module = Yii::app()->getModule('promo');

$defaultTitle = 'Акции';
$title = $module->promoH1 ?: $defaultTitle;
$this->breadcrumbs = [
    $defaultTitle
];

$this->title = $module->metaPromoTitle ?: $defaultTitle;
$this->description = $module->metaPromoKeywords;
$this->keywords = $module->metaPromoDescription;
$this->setTitle($module->promoH1 ?: $defaultTitle);
$childList = [];
?>

<div class="page page-catalog page-store">
    <?php
    $this->widget(
        'webforma.widgets.WListView', [
            'dataProvider' => $dataProvider,
            'itemView' => '_item',
            'template' => '{items}{pager}',
            'summaryText' => '',
            'itemsCssClass' => 'catalog__promo-items',
            'sortableAttributes' => [],
            'pagerCssClass' => 'catalog__pagination',
            'pager' => [
                'header' => '',
                'prevPageLabel' => '&laquo;',
                'nextPageLabel' => '&raquo;',
                'firstPageLabel' => false,
                'lastPageLabel' => false,
                'htmlOptions' => [
                    'class' => 'pagination'
                ]
            ],
            'viewData' => [
                'markerTitles' => Product::getMarkerTitles(),
            ]
        ]
    );?>
</div>