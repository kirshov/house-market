<div class="catalog__promo-item">
	<?if($data->image):?>
		<div class="promo-item__image">
			<a href="<?=Yii::app()->createUrl('/promo/promo/view', ['slug' => $data->slug])?>">
				<img src="<?=$data->getImageUrl(null, null, true);?>"/>
			</a>
		</div>
	<?endif;?>
	<div class="promo-item__title">
		<a class="promo-item__link" href="<?=Yii::app()->createUrl('/promo/promo/view', ['slug' => $data->slug])?>">
			<?=$data->name?>
		</a>
	</div>
	<div class="promo-item__dates">
		<?if($data->date_start):?>
			<span class="promo_start">Начало: <?=$data->date_start?></span>
		<?endif;?>
		<?if($data->date_end):?>
			<?if($data->date_start):?>
				|
			<?endif;?>
			<span class="promo_start">Завершение: <?=$data->date_end?></span>
		<?endif;?>
	</div>
</div>