<?php $form = $this->beginWidget(
	'CActiveForm',
	[
		'action' => ['/search/search/index'],
		'method' => 'GET',
		'htmlOptions' => [
			'class' => 'search-bar__form',
			'autocomplete' => 'off',
		]
	]
) ?>
<?= CHtml::textField('q', CHtml::encode(Yii::app()->getRequest()->getQuery('q')), [
	'class' => 'search-bar__input',
	'placeholder' => 'Найти на сайте',
]); ?>
<input type="submit" class="search-bar__submit" value="">
<?php $this->endWidget(); ?>
