<?php
/**
 * @var $dataProvider CActiveDataProvider
 */
$searchQuery = trim(Yii::app()->getRequest()->getParam('q'));
$this->title = Yii::t('SearchModule.search', 'Search by request: ') . CHtml::encode($searchQuery);
$this->breadcrumbs = [
	'Результаты поиска'
];

?>
<div class="search-in-page">
	<?$this->widget('search');?>

    <div class="text-search__results">
	    <?if ($dataProvider->getTotalItemCount() > 0): ?>
			<?php
			$this->widget(
				'webforma.widgets.WListView',
				[
					'dataProvider' => $dataProvider,
					'itemView' => '_item',
					'template' => "{items}\n{pager}",
					'summaryText' => '',
					'pagerCssClass' => 'catalog__pagination',
					'pager' => [
						'header' => '',
						'prevPageLabel' => '&laquo;',
						'nextPageLabel' => '&raquo;',
						'firstPageLabel' => false,
						'lastPageLabel' => false,
						'htmlOptions' => [
							'class' => 'pagination'
						]
					]
				]
			); ?>
        <?php else: ?>
			<div class="text-search__empty">
				<p><?= Yii::t('SearchModule.search', 'Nothing was found'); ?></p>
			</div>
        <?php endif; ?>
    </div>
</div>
