<?php
/* @var $data Search */
?>
<div class="search-result__item">
	<div class="search-result__item-title"><?= CHtml::link(CHtml::encode($data->title), $data->getLink(), ['class' => 'search-result__link']); ?></div>
	<div class="search-result__item-description">
		<?= $data->getHighlightContent(); ?>
	</div>
</div>
