<?
/**
 * @var Image[] $images
 */
?>

<?php if($images):?>
	<?foreach ($images as $image):?>
	<div class="image-item">
		<div class="image-item__img">
			<a href="<?= $image->getImageUrl(); ?>" data-id="<?=$image->id?>" data-fancybox="gallery" rel="group">
				<img src="<?= $image->getImageUrl(297, 412, false); ?>" alt="<?=$image->alt?>">
			</a>
		</div>
		<div class="image-item__title"><?=$image->name?></div>
	</div>
	<?endforeach;?>
<?endif;?>
