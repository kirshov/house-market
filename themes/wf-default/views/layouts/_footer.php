<footer class="l_footer">
	<div class="lf_top">
		<div class="wrapper">
			<div class="l_footer__menu">
				<?php $this->widget('menu', ['name' => 'bottom-menu']); ?>
			</div>
            <div class="l_footer__contacts">
                <div class="lf_phone"><?php $this->widget('content', ['code' => 'phone']); ?></div>
                <div class="lf_copy"><?php $this->widget('content', ['code' => 'copy']); ?></div>
            </div>
		</div>
	</div>
</footer>
