<?php
/**
 * @var \webforma\components\controllers\FrontController $this
 */
$siteUrl = Yii::app()->createAbsoluteUrl('/');
$imageUrl = $siteUrl.'/uploads/email/';
$mailModule = Yii::app()->getModule('mail');
$socialLogos = [
	'socInstagram' => $siteUrl.'uploads/email/icons/icon_ig.png',
	'socVk' => $siteUrl.'uploads/email/icons/icon_vk.png',
	'socFacebook' => $siteUrl.'uploads/email/icons/icon_fb.png',
	'socOK' => $siteUrl.'uploads/email/icons/icon_ok.png',
	'socTelegram' => $siteUrl.'uploads/email/icons/icon_tg.png',
	'socTwitter' => $siteUrl.'uploads/email/icons/icon_tw.png',
	'socYoutube' => $siteUrl.'uploads/email/icons/icon_yt.png',
];
$socialList = '';

$siteName = Yii::app()->getModule('webforma')->getShortSiteName();
?>
<!DOCTYPE html>
<html lang="en" style="background: #f9f9f9;">
<head>
	<title><?=$this->title?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<style type="text/css">
		#outlook a{padding:0;}
		.ReadMsgBody{width:100%;} .ExternalClass{width:100%;}
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
		body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;}
		table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}
		img{-ms-interpolation-mode:bicubic;}
		body{margin:0; padding:0;}
		img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
		table{border-collapse:collapse !important;}
		body{height:100% !important; margin:0; padding:0; width:100% !important;}

		.appleBody a {color:#68440a; text-decoration: none;}
		.appleFooter a {color:#999999; text-decoration: none;}

		@media screen and (max-width: 525px) {
			table[class="responsive-table"]{width:100%!important;}
		}
	</style>
</head>
<body style="margin: 0; padding: 0;">

<table border="0" cellpadding="0" cellspacing="0" width="560px" style="margin: 0 auto;">
	<tr>
		<td align="center" style="padding: 0 20px; background: #fff;">

			<?=$content;?>

			<div style="padding-top: 60px; margin-top: 40px; border-top: 1px solid #eee;"></div>

			<?foreach ($socialLogos as $key => $image){
				if($mailModule->{$key}){
					$socialList .= '<a style="display: inline-block; margin: 0 10px;" href="'.$mailModule->{$key}.'"><img src="'.$image.'"></a>';
				}
			}
			if($socialList){
				echo '<div style="text-align: center; margin-bottom: 35px;">'.$socialList.'</div>';
			}
			?>

			<?if($mailModule->footerText && !$this->getTemplateVar('isAdmin')):?>
				<div style="text-align: center; color: #222; font: <?=$mailModule->font?>; margin-bottom: 30px;">
					<?=$mailModule->footerText;?>
				</div>
			<?endif;?>
			<div style="color: #b1b1b1; text-align: center; font: <?=$mailModule->font?>;margin-bottom: 15px;">©<?=date('Y');?> <?=$siteName?>. Все права защищены.</div>
		</td>
	</tr>
</table>

</body>
</html>
