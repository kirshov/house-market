<?php $this->beginContent('//layouts/email/main'); ?>
	<!-- ONE COLUMN SECTION -->

	<table border="0" cellpadding="0" cellspacing="0" bgcolor="#fff" width="100%">
		<tr>
			<td align="center" style="padding: 0 0 20px;">
				<table border="0" cellpadding="0" cellspacing="0" class="responsive-table" width="100%">
					<tr>
						<td>
							<?=$content;?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

<?php $this->endContent(); ?>
