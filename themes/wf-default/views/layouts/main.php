<?php
/**
	 * @var \webforma\components\controllers\FrontController $this
	 */
$isMobile = $this->mobileDetect();
?>
<!DOCTYPE html>
<html lang="<?= Yii::app()->getLanguage(); ?>">

<head>
	<?php
	\webforma\components\TemplateEvent::fire(ShopThemeEvents::HEAD_START);
	$resourcePrefix = '';
	$sc = Yii::app()->getClientScript();
	$sc->registerCoreScript('jquery');

	$sc->registerCssFile($this->mainAssets . '/css/common.css');
	$sc->registerCssFile($this->mainAssets . '/css/style.css?'.$resourcePrefix);
	$sc->registerCssFile($this->mainAssets . '/css/multiple-select.css?'.$resourcePrefix);

	$sc->registerScriptFile($this->mainAssets . '/js/jquery.formstyler.min.js', CClientScript::POS_END);
	$sc->registerScriptFile($this->mainAssets . '/js/jquery.mask.min.js', CClientScript::POS_END);

	$this->initPlugins(['slick', 'fancybox3']);

	$sc->registerCoreScript('jquery.ui');

	$sc->registerScriptFile($this->mainAssets . '/js/common.js', CClientScript::POS_END);
	$sc->registerScriptFile($this->mainAssets . '/js/store.js', CClientScript::POS_END);
	$sc->registerScriptFile($this->mainAssets . '/js/app.js?'.$resourcePrefix, CClientScript::POS_END);
	$sc->registerScriptFile($this->mainAssets . '/js/jquery.ui.touch-punch.min.js', CClientScript::POS_END);

	$this->renderPartial('//layouts/_meta');
	$logo = Yii::app()->getModule('webforma')->getLogo();

	\webforma\components\TemplateEvent::fire(ShopThemeEvents::HEAD_END);
	?>
</head>

<body class="<?=$this->getBodyClass();?>">
<?php \webforma\components\TemplateEvent::fire(ShopThemeEvents::BODY_START);?>

<div class="site_container">
	<div class="page_rubber">
		<header class="l_header">
			<div class="wrapper">
				<div class="lh_l">
					<div class="lh_logo">
						<a href="<?= Yii::app()->createUrl('/') ?>" title="Каталог домов">
							<?if($logo):?>
								<div class="logo_img"><img src="<?=$logo;?>" alt="<?=$this->getSettingValue('siteName')?>"></div>
							<?endif;?>
						</a>
					</div>
				</div>

				<div class="lh_r">
					<div class="lhr_left">
						<div class="lh_phone">
							<?$this->widget('content', ['code' => 'phone'])?>
						</div>
						<div class="lh_feedback">
							<span class="modal-show" data-modal="callback"></span>
						</div>
						<?$this->widget('compareWidget', ['view' => 'hm'])?>
						<?$this->widget('favoriteWidget', ['view' => 'hm'])?>
                        <button class="lh__burger">
                            <span class="icon-bars">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </span>
                        </button>
					</div>
                    <div class="lt_menu">
                        <?php if (Yii::app()->hasModule('menu')): ?>
                            <?php $this->widget('menu', ['name' => 'top-menu']); ?>
                        <?php endif; ?>
                        <div class="lh_phone">
                            <?$this->widget('content', ['code' => 'phone'])?>
                        </div>
                    </div>
				</div>
			</div>
		</header>

		<?= $content ?>

		<?$this->renderPartial('//layouts/_footer', ['logo' => $logo]);?>
	</div>
</div>

<!--noindex-->
<?$this->widget('feedback', ['view' => 'ipoteka-popup'])?>
<?$this->widget('feedback', ['view' => 'show-popup'])?>
<?$this->widget('feedback', ['view' => 'cooperation-popup'])?>
<!--noindex-->

<?php \webforma\components\TemplateEvent::fire(ShopThemeEvents::BODY_END);?>
</body>
</html>
