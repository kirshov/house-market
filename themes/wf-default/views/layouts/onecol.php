<?/**
 * @var \webforma\components\controllers\FrontController $this
*/
$this->beginContent('//layouts/main');
$title = $this->getTitle();
$this->setTemplateVar('bodyClass', 'page-onecol', true, ' ');
?>
<div class="l_content">
	<div class="lc_bread">
		<div class="wrapper">
			<?$this->renderPartial('//layouts/_breadcrumb');?>
		</div>
	</div>
	<div class="lct_content">
		<?if($title){
			CHtml::tag('h1', [], $title);
		}?>
		<div class="wrapper">
			<?= $content; ?>
		</div>
	</div>
</div>

<?php $this->endContent(); ?>
