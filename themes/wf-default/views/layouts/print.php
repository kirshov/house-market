<?php
/**
	 * @var \webforma\components\controllers\FrontController $this
	 */
?>
<!DOCTYPE html>
<html lang="<?= Yii::app()->getLanguage(); ?>">

<head>
	<?php
	$sc = Yii::app()->getClientScript();
	//$sc->registerCssFile($this->mainAssets . '/css/common.css');
	$this->renderPartial('//layouts/_meta');
?>
<body>
	<?= $content ?>
</body>
</html>
