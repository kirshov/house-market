<?php
$this->widget('webforma.widgets.WBreadcrumbs', [
	'links' => $this->breadcrumbs,
	'encodeLabel' => false,
	'separator' => '<span class="separator">-</span>',
]);