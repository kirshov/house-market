<? /**
 * @var \webforma\components\controllers\FrontController $this
 */
$webforma = Yii::app()->getModule('webforma');
$siteName = $webforma->siteName;
$siteUrl = Yii::app()->createAbsoluteUrl('/');
$currentUrl = rtrim($siteUrl, '/').Yii::app()->request->requestUri;
$metaImage = $this->getMetaImage();
$metaSrcImage = $this->getMetaSrcImage();
$title = $this->getMetaTitle() ?: $siteName;

echo $this->getSettingValue('headTags');
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Language" content="ru-RU" />
<meta name="viewport" content="width=device-width">

<meta http-equiv="msthemecompatible" content="no" />
	<meta http-equiv="cleartype" content="on" />
<meta http-equiv="HandheldFriendly" content="True" />
<meta http-equiv="format-detection" content="address=no" />
<meta http-equiv="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="apple-mobile-web-app-status-bar-style" content="black-translucent" />

<?if($this->getSettingValue('favicon')){
	Yii::app()->getClientScript()->registerLinkTag('shortcut icon', null, Yii::app()->createAbsoluteUrl('/') . 'favicon.ico');
}?>

<title><?=$title;?></title>
<meta name="description" content="<?= $this->description;?>" />
<meta name="keywords" content="<?= $this->keywords;?>" />
<?php if ($this->canonical): ?>
	<link rel="canonical" href="<?= $this->canonical ?>" />
<?php endif; ?>
<?
$description = $this->description;
if(mb_strlen($description) > 200){
	$description = mb_substr($description, 197).'...';
}
?>
<?if($metaSrcImage):?>
	<link rel="image_src" href="<?=$metaSrcImage?>" />
<?endif;?>

<? /* twitter
<? /*
<meta name="twitter:card" content="summary"/>
<meta name="twitter:site" content="<?=$siteName?>"/>
<meta name="twitter:title" content="<?=$title?>">
<meta name="twitter:description" content="<?=$description?>"/>
<meta name="twitter:image:src" content="<?=$metaImage?>"/>
<meta name="twitter:domain" content="<?=$siteUrl?>"/>
 */ ?>

<? /* Open Graph для Facebook и VK */ ?>
<meta property="og:locale" content="ru_RU"/>
<meta property="og:type" content="<?=$this->getOpenGraphType()?>"/>
<meta property="og:title" content="<?=$this->title?>"/>
<meta property="og:description" content="<?=$description?>"/>
<meta property="og:image" content="<?=$metaImage?>"/>
<meta property="og:url" content="<?=$currentUrl?>"/>
<meta property="og:site_name" content="<?=$siteName?>"/>
<meta property="og:see_also" content="<?=$siteUrl;?>"/>

<script type="text/javascript">
	var storeRoundPrice = <?=(Yii::app()->hasModule('store')) ? (int) Yii::app()->getModule('store') : 0;?>;
	var webformaTokenName = '<?= Yii::app()->getRequest()->csrfTokenName;?>';
	var webformaToken = '<?= Yii::app()->getRequest()->getCsrfToken();?>';
	var webformaCartDeleteProductUrl = '<?= Yii::app()->createUrl('/cart/cart/delete/')?>';
	var webformaCartUpdateUrl = '<?= Yii::app()->createUrl('/cart/cart/update/')?>';
	var disableCartWindowUrl = '<?= Yii::app()->createUrl('/cart/disable-cart-window/')?>';
	var slickProductOptions = {
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 5,
		dots: false,
		arrows: true,
		autoplay: false,
		autoplaySpeed: 5000,
		responsive: [
			{
				breakpoint: 1160,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 4
				}
			},
			{
				breakpoint: 860,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},
			{
				breakpoint: 650,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					dots: true,
					arrows: false
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					dots: true,
					arrows: false,
					adaptiveHeight: true
				}
			}
		]
	};
</script>
