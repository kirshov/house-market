<?/**
 * @var \webforma\components\controllers\FrontController $this
*/
?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="l_content">
	<div class="lc_bread">
		<div class="wrapper">
			<?$this->renderPartial('//layouts/_breadcrumb');?>
		</div>
	</div>
	<div class="wrapper">
		<?if(!Yii::app()->getUser()->getIsGuest()):?>
			<div class="l_col lc_left">
				<ul class="left-menu">
					<?php
					$this->widget('application.modules.webforma.components.WMenu', [
						'items' => [
							[
								'label' => Yii::t('UserModule.user', 'User profile'),
								'url' => Yii::app()->createUrl('/user/profile/profile'),
							],
							[
								'label' => 'Админ-панель',
								'url' => Yii::app()->createAbsoluteUrl('backend'),
								'visible' => Yii::app()->getUser()->checkAccess('admin'),
							],
							[
								'label' => 'Мои заказы',
								'url' => Yii::app()->createUrl('/order/user/index'),
							],
							[
								'label' => 'Выйти',
								'url' => Yii::app()->createUrl('/user/account/logout'),
							]
						],
						'itemCssClass' => 'left-menu__item',
						'htmlOptions' => [
							'class' => 'left-menu'
						],
					]);?>
				</ul>
			</div>
		<?endif;?>
		<div class="lct_content">
			<h1><?=$this->getTitle()?></h1>
			<?= $content; ?>
		</div>
	</div>
</div>

<?php $this->endContent(); ?>
