<?/**
 * @var \webforma\components\controllers\FrontController $this
*/

$filter = $this->getTemplateVar('filter');
?>
<?php $this->beginContent('//layouts/main'); ?>

<div class="l_content">
	<?/*
	<div class="lc_bread">
		<div class="wrapper">
			<?$this->renderPartial('//layouts/_breadcrumb');?>
		</div>
	</div>
	*/?>
	<div class="wrapper">
		<div class="lct_content">

			<?if($this->getTitle()):?>
				<h1><?=$this->getTitle()?></h1>
			<?elseif($this->hasTemplateVar('h1')):?>
				<div class="h1"><?=$this->getTemplateVar('h1')?></div>
			<?endif?>
			<?if($filter):?>
				<div class="lc_filter">
					<?=$filter;?>
				</div>
			<?endif;?>

			<?= $content; ?>
		</div>
	</div>
</div>

<?php $this->endContent(); ?>
