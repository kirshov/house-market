<?php if(false === $compare->has($product->id)):?>
	<a href="javascript:void(0);" class="store-compare store-compare-add" data-id="<?= $product->id;?>" title="Добавить в сравнение"></a>
<?php else:?>
	<a href="javascript:void(0);" class="store-compare store-compare-remove" data-id="<?= $product->id;?>" title="Удалить из сравнения"></a>
<?php endif;?>
