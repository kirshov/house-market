<?php
/**
 * @var Product $data
 * @var ProductController $this
 */
$currency = Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency);
$minSumCart = Yii::app()->getModule('store')->minCost;

if($data->hasDiscount()){
	$discount = $data->getDiscount();
}
?>
<td class="product-item__title" style="width:200px;">
	<b><?= CHtml::encode($data->getNameInList()); ?></b>
</td>

