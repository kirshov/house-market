<?php
/**
 * @var Product $data
 * @var ProductController $this
 */
$currency = Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency);
$minSumCart = Yii::app()->getModule('store')->minCost;

if($data->hasDiscount()){
	$discount = $data->getDiscount();
}
?>
<div class="compare-col product-item product-wrapper product_id_<?=$data->id?>" data-id="<?=$data->id?>">
	<a href="javascript:void(0);" class="store-compare-remove close-btn" data-id="<?= $data->id;?>">&times;</a>
	<div class="product-item__img">
		<a href="<?= ProductHelper::getUrl($data); ?>">
			<img src="<?= StoreImage::product($data, 250, 173, 2); ?>"
				 alt="<?= CHtml::encode($data->getImageAlt()); ?>"
				 title="<?= CHtml::encode($data->getImageTitle()); ?>"
			/>
		</a>
	</div>
	<div class="product-item__title">
		<a href="<?= ProductHelper::getUrl($data); ?>" title="<?= CHtml::encode($data->getNameInList()); ?>"><?= CHtml::encode($data->getNameInList()); ?></a>
	</div>
</div>
