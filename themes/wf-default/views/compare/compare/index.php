<?php
/**
 * @var StoreCategory[] $categories
 * @var Product[] $catsProducts
 * @var Product[] $products
 * @var integer $currentCategory
 * @var array $characteristic
 * @var string $link
 * @var string $clearLink
 * @var string $printLink
 */
$mainAssets = Yii::app()->getTheme()->getAssetsUrl();

$title = 'Сравнение';
$this->title = Helper::prepareSeoTitle($title);
$this->breadcrumbs = [$title];
$this->setTitle($title);

$this->setTemplateVar('bodyClass', 'compare-page');

Yii::app()->getClientScript()->registerScriptFile(
	Yii::app()->getAssetManager()->publish(
		Yii::getPathOfAlias('application.modules.compare.view.web') . '/compare.js'
	),
	CClientScript::POS_END
);

?>
<input type="text" style="opacity: 0;" id="compareLink" value="<?=$link?>" data-url="<?=$clearLink?>"/>
<div class="compare-page catalog">
	<?if(sizeof($products) > 0){
		/*echo '<div class="compare__category-list-wrapper">';
			echo '<div class="compare__category-list-title">Разделы: </div>';
			echo '<div class="compare__category-list">';
			foreach ($categories as $category){
				if($category->id == $currentCategory){
					echo '<div class="compare__category-item active"><a>'.$category->name.'</a></div>';
				} else {
					echo '<div class="compare__category-item active"><a href="'.Yii::app()->createUrl('compare/compare/index', ['category' => $category->id]).'">'.$category->name.'</a></div>';
				}
			}
			echo '</div>';
		echo '</div>';*/

		echo '<div class="compare__items-wrap">';


		echo '<div class="compare__top">';
			echo '<div class="compare__show-toggle">';
				echo '<input type="checkbox" class="compare-toggle styler" id="compare-toggle">
					<label for="compare-toggle">Показать только различия</label>';
			echo '</div>';

			echo '<div class="compare__share">';
				echo '<span>Поделиться: </span>';
				$this->widget('content', ['code' => 'share']);
			echo '</div>';

			echo '<div class="compare__copy">';
				echo '<span class="a copy-link-compare">
					<span class="tooltiptext ">Ссылка скопирована в буфер обмена</span>
					Скопировать ссылку
				</span>';
			echo '</div>';

			echo '<div class="compare__sprint">';
				echo '<span><a href="'.$printLink.'">Распечатать страницу</a></span>';
			echo '</div>';

		echo '</div>';

		echo '<div class="compare__items-inner">';
			echo '<div class="compare__items">';
			echo CHtml::tag('div', ['class' => 'compare__ch-title'], 'Параметры');
			echo '<div class="compare-cols">';
			foreach ($products as $product){
				$this->renderPartial('_product-item', ['data' => $product]);
			}
			echo '</div>';
			echo '</div>';

			if($characteristic){
				$this->renderPartial('_characteristic-item', [
					'products' => $products,
					'characteristics' => $characteristic,
				]);
			}
		echo '</div>';
		echo '</div>';

	} else {
		echo CHtml::tag('div', ['class' => 'empty-text'], 'Нет данных для сравнения');
	}?>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		$('.compare-cols').slick({
			speed: 0,
			arrows: true,
			infinite: false,
			slidesToShow: 3,
			slidesToScroll: 1,
			variableWidth: true,
			asNavFor: '.compare__characteristic-item',
			waitForAnimate: false,
			swipe: false,
			draggable: false,
			responsive: [{
				breakpoint: 1199,
				settings: {
					slidesToShow: 2
				}
			}, {
				breakpoint: 600,
				settings: {
					slidesToShow: 1
				}
			}],
			init: function () {
				$('.compare__ch-item').off('click');
				$('.compare__ch-item').on('click', function () {
					return false;
				});
			}
		});
		$('.compare__characteristic-item').slick({
			speed: 0,
			arrows: false,
			infinite: false,
			asNavFor: '.compare-cols',
			slidesToShow: 3,
			variableWidth: true,
			focusOnSelect: true,
			waitForAnimate: false,
			swipe: false,
			draggable: false,
			responsive: [{
				breakpoint: 1199,
				settings: {
					slidesToShow: 2
				}
			}, {
				breakpoint: 600,
				settings: {
					slidesToShow: 1
				}
			}],
			init: function () {
				$('.compare__ch-item').off('click');
				$('.compare__ch-item').on('click', function () {
					return false;
				});
			}
		});

		$('.compare__ch-item').off('click');
		$('.compare__ch-item').on('click', function () {
			return false;
		});
	})
</script>
