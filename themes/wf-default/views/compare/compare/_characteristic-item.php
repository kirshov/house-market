<?php
/**
 * @var Product[] $products
 * @var array[] $characteristics
 */
?>
<div class="compare__characteristic-items">
	<?foreach ($characteristics as $chTitle => $chItem){
		$html = '';
		$p = 0;
		if($p == 0){
			$html .= CHtml::tag('div', ['class' => 'compare__ch-title'], $chTitle);
		}
		$html .= CHtml::openTag('div', ['class' => 'compare-col compare__characteristic-item']);

		$oldValue = '';
		$same = true;
		foreach ($products as $product){
			$value = $chItem[$product->id]
				? '<span class="compare__ch-value" data-product-id="'.$product->id.'">'.$chItem[$product->id].'</span>'
				: '<span class="compare__ch-value compare__empty-value">-</span>';

			$html .= CHtml::tag('div', ['class' => 'compare__ch-item product_id_'.$product->id], $value);
			$p++;
			if($same && $oldValue && $oldValue != strip_tags($value)){
				$same = false;
			}
			$oldValue = strip_tags($value);
		}

		$html .= CHtml::closeTag('div');

		echo CHtml::openTag('div', ['class' => 'compare__characteristic-item-wrap'.($same ? ' same' : '')]);
		echo $html;
		echo CHtml::closeTag('div');
	}?>
</div>
