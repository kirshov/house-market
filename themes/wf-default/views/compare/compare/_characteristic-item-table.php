<?php
/**
 * @var Product[] $products
 * @var array[] $characteristics
 */
?>
	<?foreach ($characteristics as $chTitle => $chItem){
		$html = '';
		$p = 0;
		if($p == 0){
			$html .= CHtml::tag('td', ['class' => 'compare__ch-title', 'width' => '200px'], $chTitle);
		}

		$oldValue = '';
		$same = true;
		foreach ($products as $product){
			$value = $chItem[$product->id]
				? '<span class="compare__ch-value" data-product-id="'.$product->id.'">'.$chItem[$product->id].'</span>'
				: '<span class="compare__ch-value compare__empty-value">-</span>';

			$html .= CHtml::tag('td', ['class' => 'compare__ch-item product_id_'.$product->id], $value);
			$p++;
			if($same && $oldValue && $oldValue != strip_tags($value)){
				$same = false;
			}
			$oldValue = strip_tags($value);
		}

		echo CHtml::openTag('tr', ['class' => 'compare__characteristic-item-wrap'.($same ? ' same' : '')]);
		echo $html;
		echo CHtml::closeTag('tr');
	}?>
