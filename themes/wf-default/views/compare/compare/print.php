<?php
/**
 * @var StoreCategory[] $categories
 * @var Product[] $catsProducts
 * @var Product[] $products
 * @var integer $currentCategory
 * @var array $characteristic
 * @var string $link
 * @var string $clearLink
 */

$this->layout = '//layouts/print';

$title = 'Сравнение';
$this->title = Helper::prepareSeoTitle($title);
$this->setTitle($title);
?>
<style type="text/css">
	table {
		border-spacing: 0px;
		border-collapse: collapse;
	}
	table td{
		border:1px solid #aaa;
		padding: 5px 10px;
		min-width: 150px;
		border-spacing: 0px;
		border-collapse: collapse;
	}
	.compare__characteristic-item-wrap.same{
		display: none;
	}
</style>
<table>
	<tr>
	<?
		echo CHtml::tag('td', ['class' => 'compare__ch-title'], 'Параметры');
		foreach ($products as $product){
			$this->renderPartial('_product-item-table', ['data' => $product]);
		}
	?>
	</tr>

	<?
		if($characteristic){
			$this->renderPartial('_characteristic-item-table', [
				'products' => $products,
				'characteristics' => $characteristic,
			]);
		}
	?>
</table>
<script>
	window.print();
</script>