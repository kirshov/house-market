<?php
/**
 * @var Cases[] $cases
 */
?>

<?if($cases):?>
	<?foreach ($cases as $case):?>
		<div class="case-item">
			<div class="case-item__images">
				<div class="case-item__image-item"><img src="<?=$case->getImageUrl(600, 489);?>"></div>
				<?if($case->getImages()):?>
					<?foreach ($case->getImages() as $image):?>
						<div class="case-item__image-item"><img src="<?=$image->getImageUrl(100, 100);?>"></div>
					<?endforeach;?>
				<?endif;?>
			</div>
		</div>
	<?endforeach;?>
<?endif?>
