<?php
/**
 * @var Cases[] $cases
 */
?>
<div class="main-cases main-cases__left">
	<?$this->renderPartial('_special_item', ['case' => $cases[0], 'width' => 100, 'left' => 100]);?>
</div>

<div class="main-cases main-cases__right">
	<?$this->renderPartial('_special_item', ['case' => $cases[1], 'width' => 100, 'left' => 100]);?>
	<?$this->renderPartial('_special_item', ['case' => $cases[2], 'width' => 100, 'left' => 100]);?>
	<?$this->renderPartial('_special_item', ['case' => $cases[3], 'width' => 100, 'left' => 100]);?>
	<?$this->renderPartial('_special_item', ['case' => $cases[4], 'width' => 100, 'left' => 100]);?>
</div>