<?php
/**
 * @var Cases $case
 * @var integer $width
 * @var integer $height
 */
?>
<?if($case):?>
	<?
	$url = Yii::app()->createUrl('/cases/cases/view', ['slug' => $case->slug])
	?>
	<div class="main-categories__item">
		<div class="main-categories__item-image">
			<a href="<?=$url?>">
				<img src="<?=$case->getImageUrl($width, $height)?>">
			</a>
		</div>
		<div class="main-categories__item-info">
			<div class="main-categories__item-title">
				<a href="<?=$url?>">
					<?=$case->name?>
				</a>
			</div>
			<div class="main-categories__item-date">
				<a href="<?=$url?>">
					<?=DateHelper::getDate($case->date, 2);?>
				</a>
			</div>
		</div>
	</div>
<?endif;?>