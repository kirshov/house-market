<?php
/**
 * @var \webforma\components\controllers\FrontController $this
 * @var CActiveDataProvider $dataProvider
 */
$title = $this->getModule()->title;
$this->title = $this->getModule()->metaTitle ?: Helper::prepareSeoTitle($title);
$this->description = $this->getModule()->metaDescription;
$this->keywords = $this->getModule()->metaKeyWords;

$this->breadcrumbs = [$title];
$this->setTitle($title);
$this->setTemplateVar('pageDescription', $this->getModule()->descriptionText);
?>

<div class="page page-cases-list">
<?if($this->getModule()->descriptionText):?>
	<div class="page-top-text"><?=$this->getModule()->descriptionText;?></div>
<?endif;?>
<?$this->widget(
		'webforma.widgets.WListView', [
			'dataProvider' => $dataProvider,
			'itemView' => '_item',
			'summaryText' => '',
			'itemsCssClass' => 'cases-items',
			'pagerCssClass' => 'catalog__pagination',
			'pager' => [
				'header' => '',
				'prevPageLabel' => '&laquo;',
				'nextPageLabel' => '&raquo;',
				'firstPageLabel' => false,
				'lastPageLabel' => false,
				'htmlOptions' => [
					'class' => 'pagination'
				]
			],
			'emptyText' => $emptyText ?: null,
		]
	);

?>
</div>
