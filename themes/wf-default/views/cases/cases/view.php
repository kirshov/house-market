<?php
/**
 * @var \webforma\components\controllers\FrontController $this
 * @var Cases $case
 */

$title = $case->name;
$this->title = $case->getMetaTitle();
$this->description = $case->meta_description;
$this->keywords = $case->meta_keywords;

$this->breadcrumbs = [
	$this->getModule()->title => Yii::app()->createUrl('/cases/cases/index'),
	$case->name
];
$this->setTitle($case->getTitle());
?>
<div class="page page-cases-item">

	<div class="case-info">
		<div class="case-item__images">
			<div class="wrapper">
				<div class="case-item__images-holder">
					<div class="case-item__image-item">
						<img src="<?=$case->getImageUrl(800, 600);?>" data-id="main">
					</div>
					<?if($case->getImages()):?>
						<?foreach ($case->getImages() as $key => $image):?>
							<div class="case-item__image-item">
								<img src="<?=$image->getImageUrl(800, 600);?>" data-id="<?=$key?>">
							</div>
						<?endforeach;?>
					<?endif;?>
				</div>
			</div>
		</div>
		<div class="case-item__info">
			<div class="description">
				<?=$case->description?>
			</div>
		</div>
	</div>



	<?$this->widget('feedback', ['view' => 'main'])?>

</div>
