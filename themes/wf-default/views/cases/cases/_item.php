<?php
/* @var $data Cases */
$url = Yii::app()->createUrl('/cases/cases/view', ['slug' => $data->slug]);

?>

	<div class="case-item">
		<div class="wrapper">
			<div class="case-item__left">
				<div class="case-item__image">
					<a href="<?=$url?>">
						<img src="<?=$data->getImageUrl(100, 100);?>">
					</a>
				</div>
			</div>

			<div class="case-item__right">
				<div class="case-item__name">
					<a href="<?=$url?>">
						<?=$data->name?>
					</a>
				</div>

				<div class="case-item__description">
					<?=$data->short_description?>
				</div>


				<div class="case-item__link">
					<a href="<?=$url?>">Подробнее</a>
				</div>
			</div>
		</div>
	</div>
