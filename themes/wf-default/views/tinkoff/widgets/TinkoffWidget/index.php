<?
/**
 * @var Order $order
 */
?>

<p class="notice">
    Внимание! Чек об оплате придет Вам на электронную почту.<br/>
    Возможна задержка обновления статуса оплаты на сайте.
</p>
<script src="https://securepay.tinkoff.ru/html/payForm/js/tinkoff_v2.js"></script>
<form name="TinkoffPayForm" onsubmit="pay(this); return false;">
    <input type="hidden" name="terminalkey" value="<?=Yii::app()->getModule('tinkoff')->identificator?>">
    <input type="hidden" name="frame" value="false">
    <input type="hidden" name="language" value="ru">
    <input type="hidden" name="amount" value="<?=$order->getTotalPriceWithDelivery()?>">
    <input type="hidden" name="order" value="<?=$order->getNumberForUser();?>">
    <!--<input class="tinkoffPayRow" type="text" placeholder="" name="description">-->
    <input type="hidden" name="name" value="<?=$order->name?>">
    <input type="hidden" name="email" value="<?=$order->email?>">
    <input type="hidden" name="phone" value="<?=$order->phone?>">
    <input type="submit" class="btn" value="Оплатить on-line">
</form>