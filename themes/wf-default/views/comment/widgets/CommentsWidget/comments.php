<?php
/* @var $this CommentsWidget */
/* @var $form TbActiveForm */
/* @var $model Comment */
/* @var $spamField string */
/* @var $spamFieldValue string */
/* @var $redirectTo string */
/* @var $comments Comment[] */
/* @var $module CommentModule */

Yii::app()->clientScript
    ->registerScriptFile(Yii::app()->getModule('comment')->getAssetsUrl() . '/js/comments.js')
    ->registerScript(
        __FILE__,
        "$(document).ready(function(){
        $(document).on('focus', '#Comment_text', function() {
            $('#$spamField').val('$spamFieldValue');
        })
    });"
    );
?>

<?php if ($this->showComments): ?>
    <div class="comments__body" id="comments">
        <?php if (empty($comments)){
        	echo 'Комментариев еще нет';
        } else {
			$this->widget('webforma.widgets.WListView', [
				'dataProvider' => $comments,
				'itemView' => '_comment',
				'template' => "{summary}\n{items}\n{pager}",
				'summaryText' => '',
				'itemsCssClass' => 'comment-items',
				'pagerCssClass' => 'catalog__pagination',
				'pager' => [
					'header' => '',
					'prevPageLabel' => '&laquo;',
					'nextPageLabel' => '&raquo;',
					'firstPageLabel' => false,
					'lastPageLabel' => false,
					'htmlOptions' => [
						'class' => 'pagination',
					],
				],
				'emptyText' => 'Комментариев еще нет',
			]
		);
	}
	?>
        <?php if ($this->showForm): ?>
            <?php if (!$this->isAllowed()): ?>
                <div class="alert alert-warning">
                    <?= Yii::t(
                        'CommentModule.comment',
                        'Please, {login} or {register} for commenting!',
                        [
                            '{login}' => CHtml::link(Yii::t('CommentModule.comment', 'login'), ['/user/account/login']),
                            '{register}' => CHtml::link(
                                Yii::t('CommentModule.comment', 'register'),
                                ['/user/account/registration']
                            )
                        ]
                    ); ?>
                </div>
            <?php else: ?>
                <div class="comments-form">
                    <?php $form = $this->beginWidget(
                        'CActiveForm',
                        [
                            'action' => Yii::app()->createUrl('/comment/comment/add/'),
                            'id' => 'comment-form',

	                        'enableAjaxValidation' => true,
	                        'enableClientValidation' => false,
	                        'clientOptions' => [
		                        'validateOnSubmit' => true,
		                        'validateOnChange' => false,
		                        'validateOnType' => false,
		                        'beforeValidate' => 'js:function(form, data, hasError){$(form).find("button[type=\'submit\']").prop("disabled", true); commentSendForm(form, data, hasError);}',
		                        'afterValidate' => 'js:function(form, data, hasError){$(form).find("button[type=\'submit\']").prop("disabled", false); return !hasError;}',
	                        ],
	                        'htmlOptions' => [
		                        'hideErrorMessage' => false,
	                        ],
                        ]
                    ); ?>

                    <?//= $form->errorSummary($model); ?>

                    <?= $form->hiddenField($model, 'model'); ?>
                    <?= $form->hiddenField($model, 'model_id'); ?>
                    <?= $form->hiddenField($model, 'parent_id'); ?>
                    <?= CHtml::hiddenField('redirectTo', $redirectTo); ?>

                    <?= $form->textField($model, 'spamField', [
                        'name' => $spamField,
                        'style' => 'position:absolute;display:none;visibility:hidden;',
                    ]); ?>

                    <?= $form->textField($model, 'comment', [
                        'style' => 'position:absolute;display:none;visibility:hidden;'
                    ]); ?>

                    <div class="comments__body-title">Оставить отзыв</div>
                    <div class="comments__body-subtitle">Мы не публикуем ваши контактные данные</div>

					<div class="bs-callout result-message hidden"></div>

					<div class="comment-form__top">
						<div class="form__input-item order-form__item">
							<div class="form-group">
								<?= $form->labelEx($model, 'name', ['class' => 'form-group__label']); ?>
								<div class="form-group__input">
									<?= $form->textField($model, 'name', ['class' => 'input']); ?>
								</div>
								<div class="form-group__help hidden">
									<?= $form->error($model, 'name'); ?>
								</div>
							</div>
						</div>

						<div class="form__input-item order-form__item">
							<div class="form-group">
								<?= $form->labelEx($model, 'email'); ?>
								<div class="form-group__input">
									<?= $form->textField($model, 'email', ['class' => 'input']); ?>
								</div>
								<div class="form-group__help hidden">
									<?= $form->error($model, 'email'); ?>
								</div>
							</div>
						</div>

						<div class="form__input-item order-form__item">
							<div class="form-group">
								<?= $form->labelEx($model, 'rating'); ?>
								<div class="form-group__input">
									<?= $form->dropDownList($model, 'rating', Comment::getRatingRanges(), ['class' => 'styler input input-rating']); ?>
								</div>
								<div class="form-group__help hidden">
									<?= $form->error($model, 'rating'); ?>
								</div>
							</div>
						</div>
					</div>

					<div class="comment-form__body">
						<div class="form-group">
							<?= $form->labelEx($model, 'text'); ?>
							<?= $form->textArea($model, 'text'); ?>
							<?= $form->error($model, 'text'); ?>
						</div>
                    </div>
					<div class="comment-form__bottom">
						<?php if ($module->showCaptcha && Yii::app()->getUser()->getIsGuest()): ?>
							<?php if (CCaptcha::checkRequirements()) : ?>
								<div class="form__input-item order-form__item order-form__captcha">
                                    <div class="form-group">
    									<?= $form->labelEx($model, 'verifyCode', ['required' => true]); ?>
    									<div class="form-group__input">
    										<?= $form->textField($model, 'verifyCode', ['class' => 'input input-captcha']); ?>
    									</div>
                                    </div>

									<div class="form-image">
										<?php $this->widget('CCaptcha', [
											'showRefreshButton' => false,
											'clickableImage' => true,
											'imageOptions' => [
												'height' => '38px',
												'title' => 'Кликните, чтобы получить другой код',
												'class' => 'image-captcha',
											],
											'captchaAction' => '/comment/comment/captcha/',
										]);?>
									</div>
								</div>
							<?php endif; ?>
						<?php endif; ?>

						<div class="button_privacy">
							<div class="button">
								<button class="btn btn_primary" id="add-comment" type="submit" name="add-comment">
									<?= Yii::t('CommentModule.comment', 'Отправить отзыв'); ?>
								</button>
							</div>
							<div class="privacy"><?=Yii::app()->getModule('webforma')->getPrivacyTextForm();?></div>
						</div>
					</div>
                    <?php $this->endWidget(); ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    </div>
<?php endif; ?>
