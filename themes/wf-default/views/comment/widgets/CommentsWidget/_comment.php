<?php
/* @var $data Comment */
?>

<div class="comment-item" data-id="<?= $data->id; ?>">
	<div class="product-review-user">
		<span class="product-review-user__name"><?=$data->name?></span>
	</div>

	<div class="comment-item__date">
		<?= Yii::app()->getDateFormatter()->formatDateTime($data->create_time, 'long', 'short'); ?>
	</div>

	<div class="comment-item__text">
		<?= trim($data->getText()); ?>
	</div>

	<?if($data->rating):?>
		<div class="comment-item__rating-wrap">
			<span class="comment-item__rating-separator">Оценка: </span>
			<span class="comment-item__rating-stars">
				<?foreach (range(1, 5) as $r):?>
					<span class="comment-item__rating-star<?=($r <= $data->rating ? ' fill' : '')?>"></span>
				<?endforeach;?>
			</span>
		</div>
	<?endif;?>

	<?if($data->admin_text):?>
		<div class="comment-item__admin">
			<div class="comment-item__admin-text-title">Ответ администратора:</div>
			<div class="comment-item__admin-text"><?=$data->admin_text?></div>
		</div>
	<?endif;?>
</div>
