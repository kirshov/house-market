<?php
$title = Yii::t('NewsModule.news', 'News');
$this->title = Yii::app()->getModule('news')->metaTitle ?: Helper::prepareSeoTitle($title);
$this->description = Yii::app()->getModule('news')->metaDescription;
$this->keywords = Yii::app()->getModule('news')->metaKeyWords;

$this->breadcrumbs = [$title];
$this->setTitle($title);
$this->setTemplateVar('bodyClass', 'page-news');
?>

<div class="page page-news-list">
    <?php
    $this->widget(
        'webforma.widgets.WListView',
        [
            'dataProvider' => $dataProvider,
            'itemView' => '_item',
            'template' => "{items}\n{pager}",
            'summaryText' => '',
            'pagerCssClass' => 'catalog__pagination',
            'pager' => [
                'header' => '',
	            'prevPageLabel' => '&laquo;',
	            'nextPageLabel' => '&raquo;',
                'firstPageLabel' => false,
                'lastPageLabel' => false,
                'htmlOptions' => [
                    'class' => 'pagination'
                ]
            ]
        ]
    ); ?>
</div>
