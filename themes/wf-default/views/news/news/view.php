<?php
/**
 * Отображение для ./themes/default/views/news/news/news.php
 *
 * @var $this NewsController
 * @var $model News
 **/
?>
<?php
$this->title = Helper::prepareSeoTitle($model->title);
$this->description = $model->description;
$this->keywords = $model->keywords;
$this->setTitle($model->title);
$this->setTemplateVar('bodyClass', 'page-news');
?>

<?php
$this->breadcrumbs = [
    Yii::t('NewsModule.news', 'News') => ['/news/news/index'],
    $model->title
];
?>

<div class="page page-news">
	<div class="news-date"><?=$model->date;?></div>
    <?php if ($model->image): ?>
		<div class="news-full__image">
			<?= CHtml::image($model->getImageUrl(280, 187, false), $model->title); ?>
		</div>
    <?php endif; ?>
    <div class="news-full__text"> <?= $model->full_text; ?></div>
</div>
