<?php
/* @var $data News */
$url = Yii::app()->createUrl('/news/news/view', ['slug' => $data->slug]);
?>
<div class="news-item">
    <?php if ($data->image): ?>
        <div class="news-img">
			<a href="<?=$url?>" class="news-img__link">
            	<?= CHtml::image($data->getImageUrl(180, 150, false), $data->title); ?>
			</a>
        </div>
    <?php endif; ?>
    <div class="news-info">
        <div class="news-date"><?=$data->date;?></div>
        <div class="news-title"><?= CHtml::link(CHtml::encode($data->title), $url, ['class' => 'cart-item__link']); ?></div>
        <div class="news-description"><?= $data->short_text; ?></div>
    </div>
</div>
