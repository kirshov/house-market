<?php
Yii::import('application.modules.news.NewsModule');
/**
 * @var News[] $models
 */
?>
<?php if (isset($models) && $models != []): ?>
	<?foreach ($models as $model):?>
		<div class="main-news-item">
			<div class="main-news-item__image">
				<a href="<?= Yii::app()->createUrl('/news/news/view/', ['slug' => $model->slug]) ?>">
					<img src="<?= $model->getImageUrl($width, $height, false); ?>" title="<?= CHtml::encode($model->title); ?>"/>
				</a>
			</div>
			<div class="main-news-item__info">
				<div class="main-news-item__title h3">
					<?= CHtml::link($model->title, ['/news/news/view/', 'slug' => $model->slug]); ?>
				</div>
				<div class="main-news-item__annotation">
					<?=$model->short_text?>
				</div>
				<div class="main-news-item__date">
					<?=$model->getFormatDate();?>
				</div>
			</div>
		</div>
	<?endforeach;?>
<?php endif; ?>
