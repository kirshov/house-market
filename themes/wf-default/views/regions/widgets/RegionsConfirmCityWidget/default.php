<?
$city = Yii::app()->getComponent('region')->getCity();
?>
<div class="lh_city">
	<?if(Yii::app()->getComponent('region')->getCountRegions() > 1):?>
		<span>Ваш город:</span> <a href="#" class="modal-show" data-modal="change-city"><?=$city?></a>
	<?else:?>
		<span>Ваш город:</span> <a><?=$city?></a>
	<?endif;?>
	<?if(Yii::app()->getComponent('region')->askCity && $popup):?>
		<div class="lh_city-popup">
			<div class="lh_city-popup__text">Выбран Ваш город?</div>
			<div class="lh_city-popup__link"><a href="#confirm" class="confirm-city" data-region="<?=Yii::app()->getComponent('region')->getCurrentSubDomain()?>">Да</a>
			<a href="#" class="modal-show btn" data-modal="change-city">Нет</a></div>
		</div>
	<?endif;?>
</div>
