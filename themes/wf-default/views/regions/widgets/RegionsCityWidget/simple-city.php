<?
/**
 * @var array $cities
 */
$currentSubDomain = Yii::app()->getComponent('region')->getCurrentSubDomain();
if(!$currentSubDomain){
	$currentSubDomain = Yii::app()->getModule('regions')->defaultRegion;
}
?>
<div class="modal-window change-city">
	<div class="modal-content">
		<div class="modal-header">
			<div class="modal-close">&times;</div>
			<div class="modal-title">Выберите Ваш город</div>
			<div class="modal-subtitle">Выберите ближайший город, в котором собираетесь получать товары.</div>
		</div>
		<div class="modal-body">
			<ul class="city-list">
				<?php
				foreach ($cities as $city){
					echo '<li class="city-item'.($city->domain == $currentSubDomain ? ' active' : '').'"><a class="city-item-link" href="'.$city->getLink().'">'.$city->title.'</a></li>';
				}
				?>
			</ul>
		</div>
	</div>
</div>

