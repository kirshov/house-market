<?php
Yii::import('application.modules.articles.ArticlesModule');
/**
 * @var Articles[] $models
 */
?>
<?php if (isset($models) && $models != []): ?>
	<?foreach ($models as $model):?>
		<div class="main-news-item">
			<div class="main-news-item__info">
				<div class="main-news-item__title h3">
					<?= CHtml::link($model->title, ['/articles/articles/view/', 'slug' => $model->slug]); ?>
				</div>
				<div class="main-news-item__annotation">
					<?=$model->short_text?>
				</div>
			</div>
		</div>
	<?endforeach;?>
<?php endif; ?>
