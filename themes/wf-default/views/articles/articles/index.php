<?php
/**
 * @var \webforma\components\controllers\FrontController $this
 */
$this->title = Yii::app()->getModule('articles')->metaTitle ?: Yii::t('ArticlesModule.articles', 'Articles');
$this->description = Yii::app()->getModule('articles')->metaDescription;
$this->keywords = Yii::app()->getModule('articles')->metaKeyWords;

if($categoryName){
	$this->breadcrumbs = [
		Yii::t('ArticlesModule.articles', 'Articles') => Yii::app()->createUrl('articles/articles/index'),
		$categoryName,
	];
} else {
	$this->breadcrumbs = [Yii::t('ArticlesModule.articles', 'Articles')];
}

$this->setTitle(Yii::t('ArticlesModule.articles', 'Articles'));
$this->setTemplateVar('bodyClass', 'page-news');

//$this->layout = '//layouts/onecol';
?>

<div class="page page-news-list">
    <?php
    $this->widget(
        'webforma.widgets.WListView',
        [
            'dataProvider' => $dataProvider,
            'itemView' => '_item',
            'template' => "{items}\n{pager}",
            'summaryText' => '',
            'pagerCssClass' => 'catalog__pagination',
            'pager' => [
                'header' => '',
	            'prevPageLabel' => '&laquo;',
	            'nextPageLabel' => '&raquo;',
                'firstPageLabel' => false,
                'lastPageLabel' => false,
                'htmlOptions' => [
                    'class' => 'pagination'
                ]
            ]
        ]
    ); ?>
</div>
