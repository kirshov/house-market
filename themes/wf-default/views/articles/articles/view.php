
<?php
$this->title = $model->meta_title ?: $model->title;
$this->description = $model->description;
$this->keywords = $model->keywords;

$this->setTitle(false);
$this->setTemplateVar('bodyClass', 'page-news');
$this->setTemplateVar('h1', Yii::t('ArticlesModule.articles', 'Articles'));
//$this->layout = '//layouts/onecol';
?>

<?php
/*$this->breadcrumbs = [
    Yii::t('ArticlesModule.articles', 'Articles') => ['/articles/articles/index'],
    $model->title
];*/
?>

<div class="page page-news">
	<div class="news-full__title">
		<h1><?=$model->title?></h1>
	</div>

	<?php if ($model->image): ?>
		<div class="news-full__image">
			<?= CHtml::image($model->getImageUrl(280, 187, false), $model->title); ?>
		</div>
	<?php endif; ?>
	<div class="news-full__text"> <?= $model->full_text; ?></div>
</div>
