<?php
/* @var $data Articles */
$url = Yii::app()->createUrl('/articles/articles/view', ['slug' => $data->slug]);
?>
<div class="news-item">
	<div class="news-img">
		<a href="<?=$url?>" class="news-img__link">
			<?= CHtml::image($data->getImageUrl(300, 195, false), $data->title); ?>
		</a>
	</div>
	<div class="news-info">
		<div class="news-title"><?= CHtml::link(CHtml::encode($data->title), $url); ?></div>
		<div class="news-description"><?= $data->short_text; ?></div>
		<div class="news-more"><a href="<?=$url?>">Подробнее</a></div>
	</div>
</div>
