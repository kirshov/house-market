<?php
/**
 * @var \webforma\components\controllers\FrontController $this
 * @var CDataProvider $dataProvider
 */

$mainAssets = Yii::app()->getTheme()->getAssetsUrl();

$title = 'Избранное';
$this->title = Helper::prepareSeoTitle($title);
$this->breadcrumbs = [$title];
$this->setTitle($title);

$this->setTemplateVar('bodyClass', 'catalog-page catalog-favorite-page');
Yii::app()->getModule('dictionary');
$this->setTemplateVar('filter', '<div class="catalog-filter"><div class="view-type">'.$this->renderPartial('//store/category/_view_type', [], true).'</div></div>');
?>
<div class="catalog favorite-page">
    <p class="text-center">Вы добавили:</p>
    <?$this->renderPartial('//store/category/_product_list', [
		'dataProvider' => $dataProvider,
		'in_favorite' => true,
	]);?>
</div>
