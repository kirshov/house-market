<?php if(false === $favorite->has($product->id)):?>
    <a href="javascript:void(0);" class="store-favorite store-favorite-add" data-id="<?= $product->id;?>" title="Добавить в избранное"></a>
<?php else:?>
    <a href="javascript:void(0);" class="store-favorite store-favorite-remove" data-id="<?= $product->id;?>" title="Удалить из избранного">&times;</a>
<?php endif;?>
