<?php if(false === $favorite->has($product->id)):?>
    <a href="javascript:void(0);" class="store-favorite store-favorite-add" data-id="<?= $product->id;?>">В избранное</a>
<?php else:?>
    <a href="javascript:void(0);" class="store-favorite store-favorite-remove" data-id="<?= $product->id;?>">В избранном</a>
<?php endif;?>
