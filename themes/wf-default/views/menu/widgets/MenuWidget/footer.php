<?php
$this->widget('zii.widgets.CMenu', [
	'items' => $this->params['items'],
	'itemCssClass' => 'footer-menu__item',
	'htmlOptions' => [
		'class' => 'footer-menu'
	],
	'submenuHtmlOptions' => [
		'class' => 'dropdown-menu'
	]
]);
