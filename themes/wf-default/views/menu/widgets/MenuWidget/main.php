<nav>
    <?php
    $this->widget('application.modules.webforma.components.WMenu', [
        'items' => $this->params['items'],
        'itemCssClass' => 'top-menu__item',
        'htmlOptions' => [
            'class' => 'top-menu'
        ],
        'submenuHtmlOptions' => [
            'class' => 'dropdown-menu'
        ]
    ]);?>
</nav>