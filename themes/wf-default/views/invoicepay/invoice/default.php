<?
/**
 * @var Order $model
 * @var array $paymentSettings
 * @var string $totalStr
 * @var string $deadlineStr
 */
if($paymentSettings['logo']){
	$logo = Yii::app()->getModule('webforma')->getLogo();
}

?>
<!doctype html>
<html moznomarginboxes mozdisallowselectionprint>
<head>
    <title>Счет №<?=$model->id?> от <?=date('d.m.Y', strtotime($model->date))?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style>
        body { width: 210mm; margin-left: auto; margin-right: auto; border: 1px #efefef solid; font-size: 11pt;}
        table.invoice_bank_rekv { border-collapse: collapse; border: 1px solid; }
        table.invoice_bank_rekv > tbody > tr > td, table.invoice_bank_rekv > tr > td { border: 1px solid; }
        table.invoice_items { border: 1px solid; border-collapse: collapse;}
        table.invoice_items td, table.invoice_items th { border: 1px solid;}
    </style>
	<style type="text/css" media="print">
		@page {size: auto;padding: 20px;margin: 0 0 0 0;}
	</style>
</head>
<body>
<table width="100%">
    <tr>
        <td width="45%">
			<?if($logo):?>
				<img src="<?=$logo;?>"/>
			<?endif;?>
		</td>
        <td></td>
        <td width="45%">
			<?if($paymentSettings['contacts']):?>
				<div style="text-align: right; margin-bottom: 20px; font-weight: bold;">
					<?=$paymentSettings['contacts'];?>
				</div>
			<?endif;?>
        </td>
    </tr>
</table>


<table width="100%" cellpadding="2" cellspacing="2" class="invoice_bank_rekv">
    <tr>
        <td style="min-height:6mm; height:auto; width: 50mm;">
            <div>ИНН <?=$paymentSettings['inn']?></div>
        </td>
        <td style="min-height:6mm; height:auto; width: 55mm;">
            <div>КПП <?=$paymentSettings['kpp']?></div>
        </td>
        <td rowspan="2" style="min-height:19mm; height:auto; vertical-align: top; width: 25mm;">
            <div>Сч. №</div>
        </td>
        <td rowspan="2" style="min-height:19mm; height:auto; vertical-align: top; width: 60mm;">
            <div><?=$paymentSettings['ks']?></div>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="height:auto;">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 105mm;">
				<tr>
					<td valign="bottom" style="height: 3mm;">
						<div style="font-size: 10pt;">Получатель</div>
					</td>
				</tr>
                <tr>
                    <td valign="top">
                        <div><?=$paymentSettings['recipient']?></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

	<tr>
		<td colspan="2" rowspan="2" style="width: 105mm;" valign="top">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td valign="top" style="height: 3mm;">
						<div style="font-size:10pt;">Банк получателя</div>
					</td>
				</tr>
				<tr>
					<td valign="top">
						<div><?=$paymentSettings['bank']?></div>
					</td>
				</tr>
			</table>
		</td>
		<td style="min-height:7mm;height:auto; width: 25mm;">
			<div>БИK</div>
		</td>
		<td rowspan="2" style="vertical-align: top; width: 60mm;">
			<div style=" height: 7mm; line-height: 7mm; vertical-align: middle;"><?=$paymentSettings['bik']?></div>
			<div><?=$paymentSettings['rs']?></div>
		</td>
	</tr>
	<tr>
		<td style="width: 25mm;">
			<div>Сч. №</div>
		</td>
	</tr>
</table>
<br/>

<div style="font-weight: bold; font-size: 16pt; padding-left:5px;">
    Счет №<?=$model->id?> от <?=date('d.m.Y', strtotime($model->date))?>
</div>
<br/>

<div style="background-color:#000000; width:100%; font-size:1px; height:2px;">&nbsp;</div>

<table width="100%">
    <tr>
        <td style="width: 30mm;">
            <div style=" padding-left:2px;">Поставщик:</div>
        </td>
        <td>
            <div style="font-weight:bold;  padding-left:2px;"><?=$paymentSettings['recipient']?></div>
        </td>
    </tr>
    <tr>
        <td style="width: 30mm;">
            <div style=" padding-left:2px;">Покупатель:</div>
        </td>
        <td>
            <div style="font-weight:bold;  padding-left:2px;"><?=$model->name?></div>
        </td>
    </tr>
</table>


<table class="invoice_items" width="100%" cellpadding="2" cellspacing="2">
    <thead>
		<tr>
			<th style="width:13mm;">№</th>
			<th style="width:40mm;">Код</th>
			<th>Товар</th>
			<th style="width:20mm;">Кол-во</th>
			<th style="width:17mm;">Ед.</th>
			<th style="width:27mm;">Цена</th>
			<th style="width:27mm;">Сумма</th>
		</tr>
    </thead>
    <tbody>
		<?if($model->products):?>
			<?$i = 1;?>
			<?foreach ($model->products as $product):?>
				<tr>
					<td align="center"><?=$i?></td>
					<td align="left"><?=$product->sku?></td>
					<td align="left"><?=$product->product_name?></td>
					<td align="right"><?=$product->quantity?></td>
					<td align="center"><?=$product->product && $product->product->category->unit ? mb_substr($product->product->category->unit, 0, 1).'.' : 'т.'?></td>
					<td align="right"><?=Helper::getFormatPrice($product->price, 2)?></td>
					<td align="right"><?=Helper::getFormatPrice($product->price * $product->quantity, 2)?></td>
				</tr>
				<?$i++;?>
			<?endforeach;?>
			<?if($model->getDeliveryPrice() > 0):?>
				<tr>
					<td align="center"><?=$i?></td>
					<td align="left"></td>
					<td align="left"><?=$model->delivery ? $model->delivery->name : 'Доставка'?></td>
					<td align="right">1</td>
					<td align="center">шт</td>
					<td align="right"><?=$model->getDeliveryPrice()?></td>
					<td align="right"><?=$model->getDeliveryPrice()?></td>
				</tr>
			<?endif;?>
		<?endif;?>
	</tbody>
</table>

<table border="0" width="100%" cellpadding="1" cellspacing="1">
    <tr>
        <td></td>
        <td style="width:27mm; font-weight:bold;  text-align:right;">Итого:</td>
        <td style="width:27mm; font-weight:bold;  text-align:right;"><?=Helper::getFormatPrice($model->getTotalPriceWithDelivery(), 2)?></td>
    </tr>
	<?if($paymentSettings['nds']):?>
		<?$nds = $model->getTotalPriceWithDelivery() * (float) $paymentSettings['nds'] / 100;?>
		<tr>
			<td colspan="2" style="font-weight:bold;  text-align:right;">В том числе НДС:</td>
			<td style="width:27mm; font-weight:bold;  text-align:right;"><?=Helper::getFormatPrice($nds, 2)?></td>
		</tr>
	<?endif;?>
</table>

<br />
<div>
Всего наименований <?=sizeof($model->products)?> на сумму <?=Helper::getFormatPrice($model->getTotalPriceWithDelivery(), 2)?> рублей.<br />
<?=Helper::mb_ucfirst($totalStr)?></div>
<br /><br />
<div style="background-color:#000000; width:100%; font-size:1px; height:2px;">&nbsp;</div>
<br/>

<div>Руководитель <u><?=$paymentSettings['director']?></u> (Фамилия И.О.)</div>
<br/>

<div>Главный бухгалтер <u><?=$paymentSettings['booker']?></u> (Фамилия И.О.)</div>
<br/>

<div style="width: 85mm;text-align:center;">М.П.</div>
<br/>

<?if($paymentSettings['deadline']):?>
	<div style="margin-top:50px; text-align:left;font-size:10pt;">Счет действителен к оплате в течении <?=$paymentSettings['deadline']?> <?=Helper::pluralForm($paymentSettings['deadline'], ['дня', 'дней', 'дней'])?>.</div>
<?endif;?>
<script type="text/javascript">
	window.print();
</script>
</body>
</html>
