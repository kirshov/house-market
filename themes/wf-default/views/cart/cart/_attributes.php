<?php
/**
 * @var CartProduct $position
 * @var Product $product
 */
$variantsData = [];
if($position->selectedVariants){
	$i = 0;
	foreach ($position->selectedVariants as $variant){
		echo CHtml::hiddenField('OrderProduct[' . $position->getId() . '][variant_ids][]', $variant->id);
		if($i == 0 && $variant['sku']){
			$variantsData[] = '<span class="cart-item__variant-item">Артикул: '.$variant['sku'].'</span>';
			$i++;
		}
		$variantsData[] = '<span class="cart-item__variant-item">'.$variant->attribute->title.': '.$variant->getOptionValue().'</span>';
	}
} else {
	if($product->sku) {
		$variantsData[] = '<span class="cart-item__variant-item">Артикул: '.$product->sku.'</span>';
	}

	$variants = $this->renderPartial('//store/product/_general_attributes', ['product' => $product], true);
	$variants = trim($variants);
	if($variants){
		$variantsData[] = $variants;
	}
}
if($variantsData):?>
	<div class="cart-item__data"><?=implode(', ', $variantsData);?></div>
<?endif;?>