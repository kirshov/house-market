<?
	if(Yii::app()->hasComponent('geoLocateManager')){
		$currentCity = Yii::app()->getComponent('geoLocateManager')->getCurrentCity();
	}

	$myCity = Yii::app()->getModule('delivery')->myCity;
	if(!$currentCity){
		$currentCity = $myCity;
	}
	$showAllRegion = empty($myCity);
	$isMyRegion = ($myCity && $myCity == $currentCity);

	Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.delivery.views.web') . '/delivery-cost.js'), CClientScript::POS_END);Yii::app()->setParams(['needChangeCity' => true]);

	$currency = Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency);
?>
<div class="order-box__inner">
	<?if(Yii::app()->hasComponent('geoLocateManager')):?>
		<div class="order-box__header">Доставка в
			<span class="a modal-show" data-modal="change-city"><?=$currentCity;?></span>
			<span class="change-city__in-cart modal-show" data-modal="change-city">[Изменить город]</span>
		</div>
	<?else:?>
		<div class="order-box__header">Доставка</div>
	<?endif;?>

	<?php if (!empty($deliveryTypes)): ?>
		<div class="order-box__body">
			<div class="order-box-delivery">
				<div class="order-box-delivery__type">
					<?$i = 0;?>
					<?php foreach ($deliveryTypes as $key => $delivery): ?>
						<?
							if(!($showAllRegion || $delivery->show_region == Delivery::SHOW_REGION_ALL
								|| ($delivery->show_region == Delivery::SHOW_REGION_MY && $isMyRegion)
								|| ($delivery->show_region == Delivery::SHOW_REGION_OTHER && !$isMyRegion))
							){
								continue;
							}
							$i++;
						?>

						<div class="order-box__item order-radio__item<?=($delivery->pickup_group_id ? ' has_pickup' : '')?><?=$delivery->deliveryCalc ? ' disabled' : ''?>">
							<input type="radio" name="Order[delivery_id]" id="delivery-<?= $delivery->id; ?>" class="radio__input" value="<?= $delivery->id; ?>" <?=($delivery->id == $order->delivery_id || (!$order->delivery_id && $i == 1) ? 'checked' : '')?>
							   data-price="<?= $delivery->price; ?>" data-free-from="<?= $delivery->free_from; ?>" data-available-from="<?= $delivery->available_from; ?>" data-separate-payment="<?= $delivery->separate_payment; ?>"
							   data-currency="<?=$currency?>" data-free-text="<?=($delivery->free_text ? $delivery->free_text : 'Бесплатно')?>"
							   data-need-address="<?=$delivery->need_address?>"
							   <?=$delivery->deliveryCalc ? ' disabled' : ''?>>

							<label for="delivery-<?= $delivery->id; ?>">
								<div class="order-box__item-title">
									<span><?= $delivery->name; ?></span> -
									<?if($delivery->deliveryCalc):?>
										<span class="real-price loader <?=$delivery->deliveryCalc->type?>" data-tariff="<?=$delivery->deliveryCalc->value?>">&nbsp;</span>
									<?else:?>
										<span class="real-price"></span>
									<?endif;?>
								</div>
							</label>

							<div class="radio-body__text">
								<?
								echo $delivery->description;

								if($delivery->deliveryCalc && $delivery->deliveryCalc->hasPickup()){
									$deliveryList = $delivery->deliveryCalc->getPickupList();

									if($deliveryList){
										echo CHtml::dropDownList('deliveryCalc['.$delivery->deliveryCalc->type.']['.$delivery->deliveryCalc->value.']',
											$_POST['deliveryCalc'][$delivery->deliveryCalc->type][$delivery->deliveryCalc->value],
											$deliveryList,
											['class' => 'delivery-system-pickups styler']
										);
									}
								}
								?>
							</div>

							<?if($delivery->pickup_group_id):?>
								<?
								$pickupItemTitle = 'Выбрать пункт самовывоза';
								$pickupItemOtherTitle = 'Выбрать другой пункт самовывоза';
								$pickupAddress = '';
								if($order->pickup_id){
									$pickupAddress = Pickup::getPickupAddress($order->pickup_id);
									if($pickupAddress){
										$pickupItemTitle = $pickupItemOtherTitle;
									}
								}
								?>
								<div class="delivery-pickup pickup-select" data-pickup="<?=$delivery->id?>">
									<span class="selected-pickup<?=($pickupAddress) ? ' selected' : '';?>"><?=$pickupAddress;?></span>
									<span class="a button-title" data-other-title="<?=$pickupItemOtherTitle?>"><?=$pickupItemTitle?></span>
									<input type="hidden" name="Order[pickup_id]" value="<?=$order->pickup_id?>" class="input-pickup-id">
								</div>
								<?$this->setTemplateVar('pickup-map', $this->widget('pickup', ['deliveryId' => $delivery->id, 'group' => $delivery->pickup_group_id], true))?>
							<?endif;?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</div>
