<?
$currency = Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency);
?>
<div class="order-box__header">Доставка</div>
<?php if (!empty($deliveryTypes)): ?>
	<div class="order-box__body">
		<div class="order-box-delivery">
			<div class="order-box-delivery__type">
				<?php foreach ($deliveryTypes as $key => $delivery): ?>
					<div class="order-box__item order-radio__item<?=($delivery->pickup_group_id ? ' has_pickup' : '')?>">
						<input type="radio" name="Order[delivery_id]" id="delivery-<?= $delivery->id; ?>" class="radio__input" value="<?= $delivery->id; ?>" <?=$delivery->id == $order->delivery_id ? 'checked' : ''?>
							   data-price="<?= $delivery->price; ?>" data-free-from="<?= $delivery->free_from; ?>" data-available-from="<?= $delivery->available_from; ?>" data-separate-payment="<?= $delivery->separate_payment; ?>"
							   data-currency="<?=$currency?>" data-free-text="<?=($delivery->free_text ? $delivery->free_text : 'Бесплатно')?>" data-need-address="<?=$delivery->need_address?>">

						<label for="delivery-<?= $delivery->id; ?>">
							<div class="order-box__item-title">
								<?= $delivery->name; ?> - <span class="real-price"></span>
							</div>
						</label>
						<div class="radio-body__text">
							<?= $delivery->description; ?>
						</div>

						<?if($delivery->pickup_group_id):?>
							<?
							$pickupItemTitle = 'Выбрать пункт самовывоза';
							$pickupItemOtherTitle = 'Выбрать другой пункт самовывоза';
							$pickupAddress = '';
							if($order->pickup_id){
								$pickupAddress = Pickup::getPickupAddress($order->pickup_id);
								if($pickupAddress){
									$pickupItemTitle = $pickupItemOtherTitle;
								}
							}
							?>
							<div class="delivery-pickup pickup-select" data-pickup="<?=$delivery->id?>">
								<span class="selected-pickup<?=($pickupAddress) ? ' selected' : '';?>"><?=$pickupAddress;?></span>
								<span class="a button-title" data-other-title="<?=$pickupItemOtherTitle?>"><?=$pickupItemTitle?></span>
								<input type="hidden" name="Order[pickup_id]" value="<?=$order->pickup_id?>" class="input-pickup-id">
							</div>
							<?$this->setTemplateVar('pickup-map', $this->widget('pickup', ['deliveryId' => $delivery->id, 'group' => $delivery->pickup_group_id], true))?>
						<?endif;?>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
<?php endif; ?>