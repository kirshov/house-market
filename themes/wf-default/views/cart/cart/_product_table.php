<?
/**
 * @var Product[] $positions
 * @var Coupon[] $coupons
 */
$currency = Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency);
?>
<div class="main__cart-products">
    <div class="order-box__header order-box__header_black">
        <div class="cart-list-header">
            <div class="cart-list__column cart-list__column_info">Информация о товаре</div>
            <div class="cart-list__column cart-list__column_price">Стоимость</div>
            <div class="cart-list__column cart-list__column_count">Количество</div>
            <div class="cart-list__column cart-list__column_sum">Сумма</div>
            <div class="cart-list__column cart-list__column_actions"></div>
        </div>
    </div>
    <div class="cart-list">
        <?php
        foreach ($positions as $position):
            $product = $position->getProductModel();
            if (is_null($product)) continue;
            ?>
            <div class="cart-list__item">
                <?php $positionId = $position->getId(); ?>
                <?php $productUrl = ProductHelper::getUrl($product); ?>
                <?= CHtml::hiddenField('OrderProduct['.$positionId.'][product_id]', $position->id); ?>
                <input type="hidden" class="position-id" value="<?= $positionId; ?>"/>

                <div class="cart-item">
                    <div class="cart-item__info">
                        <div class="cart-item__thumbnail">
                            <a href="<?= $productUrl; ?>">
                                <img src="<?= $product->getImageUrl(120, 120, false); ?>" class="cart-item__img"/>
                            </a>
                        </div>

                        <div class="cart-item__content">
							<?if($position->hasDiscount()):?>
	                            <div class="product-item__price-old-wrap">
	                                <div class="product-item__discount">
	                                    Скидка <?=$position->getDiscount()?>%
	                                </div>
	                            </div>
	                        <?endif;?>
                            <div class="cart-item__title">
                                <a href="<?= $productUrl; ?>" class="cart-item__link"><?= CHtml::encode($position->name); ?></a>
                            </div>

                            <?php if ($product->getCategoryId()): ?>
                                <div class="cart-item__category"><?= $product->category->name ?></div>
                            <?php endif; ?>
                            <?$variantsData = [];?>
                            <?if($position->selectedVariants){
                                $i = 0;
                                foreach ($position->selectedVariants as $variant){
                                    echo CHtml::hiddenField('OrderProduct[' . $positionId . '][variant_ids][]', $variant->id);
                                    if($i == 0 && $variant['sku']){
                                        $variantsData[] = '<span class="cart-item__variant-item">Артикул: '.$variant['sku'].'</span>';
                                        $i++;
                                    }
                                    $variantsData[] = '<span class="cart-item__variant-item">'.$variant->attribute->title.': '.$variant->getOptionValue().'</span>';
                                }
                            } elseif($product->sku) {
                                $variantsData[] = '<span class="cart-item__variant-item">Артикул: '.$position->sku.'</span>';
                            }
                            ?>
                            <?if($variantsData):?>
                                <div class="cart-item__data"><?=implode(', ', $variantsData);?></div>
                            <?endif;?>
                        </div>
                    </div>


                    <div class="cart-item__price">
						<div class="cart-item__price_holder">
	                        <span class="position-price" data-price="<?=$position->getPrice();?>"><?= ProductHelper::getFormatPrice($position->getPrice()); ?></span>
	                        <span class="ruble"><?=$currency?></span>
							<?if($position->hasDiscount()):?>
								<div class="product-item__price-old-wrap">
									<div class="product-item__price-old">
										<?= $position->getFormatPrice($position->getPrice($position->getSelectedVariantsAsArray(), 'base')) ?> <?=$currency;?>
									</div>
								</div>
							<?endif;?>
						</div>
                    </div>

                    <div class="cart-item__quantity">
                        <span class="cart-quantity-decrease" data-target="#cart_<?= $positionId; ?>">-</span>
                        <?= CHtml::textField('OrderProduct['.$positionId.'][quantity]', $position->getQuantity(), ['id' => 'cart_'.$positionId, 'class' => 'position-count', 'autocomplete'=>'off']); ?>
                        <span class="cart-quantity-increase" data-target="#cart_<?= $positionId; ?>">+</span>
                    </div>

                    <div class="cart-item__summ">
                        <span class="position-sum-price"><?= ProductHelper::getFormatPrice($position->getSumPrice()); ?></span>
                        <span class="currency"><?=$currency?></span>
                    </div>

                    <div>
                        <div class="cart-item__actions">
                            <a class="cart-delete-product" data-position-id="<?= $positionId; ?>"></a>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>

        <?php foreach ($coupons as $coupon): ?>
            <div class="cart-list__item coupon">
                <div class="cart-item">
                    <div class="cart-item__info">
                        <div class="cart-item__thumbnail empty">
                        </div>

                        <div class="cart-item__content">
                            <div class="cart-item__title"><?= $coupon->name; ?></div>
                        </div>
                    </div>

                    <div class="cart-item__price"></div>
                    <div class="cart-item__quantity"></div>

                    <div class="cart-item__summ">-<?=ProductHelper::getFormatPrice($coupon->value)?> <span class="ruble"> <?= CouponType::title($coupon->type) ?></span></div>

                    <div>
                        <div class="cart-item__actions">
                            <span class="cart-delete-coupon close"></span>
                        </div>
                    </div>

                    <?= CHtml::hiddenField("Order[couponCodes][{$coupon->code}]", $coupon->code, [
                        'class' => 'coupon-input',
                        'data-code' => $coupon->code,
                        'data-name' => $coupon->name,
                        'data-value' => $coupon->value,
                        'data-type' => $coupon->type,
                        'data-min-order-price' => $coupon->min_order_price,
                        'data-free-shipping' => $coupon->free_shipping,
                    ]); ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>