<div class="uds-wrapper">
	<div class="uds-form">
		<div class="uds-top">
			<div class="uds-top-icon"></div>
			<div class="uds-top-code">
				<input type="text" class="uds-code" value="" placeholder="Введите UDS код">
			</div>

			<div class="uds-top-btn">
				<div class="btn get-uds" data-url="<?=Yii::app()->createUrl('/uds/uds/get/')?>">Получить скидку</div>
			</div>
		</div>
		<div class="uds-error"></div>

		<div class="uds-dropdown">
			<div class="uds-info">
				<div class="line uds-client">
					<span class="uds-title">Данные клиента:</span>
					<span class="uds-value"></span>
				</div>
				<div class="line uds-score">
					<span class="uds-title">Баллы:</span>
					<span class="uds-value"></span>
				</div>
			</div>

			<div class="uds-bottom">
				<div class="uds-bottom-title">
					Баллов
				</div>
				<div class="uds-bottom-code">
					<input type="text" class="uds-score" value="">
				</div>

				<div class="uds-bottom-btn">
					<div class="btn do-uds" data-url="<?=Yii::app()->createUrl('/uds/uds/set/')?>">Списать баллы</div>
				</div>
			</div>
			<div class="uds-success"></div>
		</div>
	</div>

	<div class="uds-hint">
		<div class="uds-hint-title"><span>Как получить скидку?</span></div>
		<div class="uds-hint-text">
			<?$this->widget('content', ['code' => 'uds-cart']);?>
		</div>
	</div>
</div>
