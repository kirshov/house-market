<?php
/**
 * @var CartController $this
 * @var Product[] $positions
 * @var Order $order
 * @var Coupon[] $coupons
 * @var CActiveForm $form
*/

$this->layout = '//layouts/onecol';

$title = Yii::t('CartModule.cart', 'Cart');
$this->title = Helper::prepareSeoTitle($title);
$this->breadcrumbs = [
    $title,
];
$this->setTitle($title);
$this->setTemplateVar('bodyClass', 'page-basket');
?>
<div class="page page-cart">
    <div class="wrapper">
        <div class="cart__empty<?=!Yii::app()->cart->isEmpty() ? ' hidden' : '';?>">
            <div class="cart__empty-title">
                Ваша корзина пуста
            </div>
            <?/*<div class="cart__empty-description">
                <a href="<?=Yii::app()->createUrl('/store/category/index')?>" class="btn">Перейти в каталог</a>
            </div>*/?>
        </div>

		<?if(!Yii::app()->cart->isEmpty()):?>
			<?if(Yii::app()->getModule('order')->cartBeforeMessage):?>
				<div class="cart__before-message"><?=Yii::app()->getModule('order')->cartBeforeMessage;?></div>
			<?endif;?>

			<?$this->renderPartial('_order_form', [
				'positions' => $positions,
				'order' => $order,
				'coupons' => $coupons,
				'deliveryTypes' => $deliveryTypes,
				'paymentSystems' => $paymentSystems,
			])?>
		<?endif;?>
    </div>
</div>
