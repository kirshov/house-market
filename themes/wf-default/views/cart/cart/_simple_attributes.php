<?php foreach ($product->getAttributeGroups() as $groupName => $items): ?>
	<?php foreach ($items as $attribute): ?>
		<?
        if($attribute->hide_in_site || $product->hasVariantByAttribute($attribute->id)){
            continue;
        }
		$value = AttributeRender::renderValue($attribute, $product->attribute($attribute),true, '{item}');

		if(!$attribute->title || !$value || mb_strpos($attribute->title, 'Вариант - ') === false){
			continue;
		}
		echo '<span class="product-cart__variant-value">'.AttributeRender::formatSting($value).'</span>';
		?>
	<?php endforeach; ?>
<?php endforeach; ?>
