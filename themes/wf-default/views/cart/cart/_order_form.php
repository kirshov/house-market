<?
$minSumCart = Yii::app()->getModule('store')->minCost;
$currency = Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency);
?>
<div class="cart-order-wrapper">
	<?php if (!Yii::app()->cart->isEmpty()): ?>
		<?php
		$form = $this->beginWidget(
			'CActiveForm',
			[
				'action' => ['/cart/cart/index'],
				'id' => 'order-form',
				'enableAjaxValidation' => false,
				'enableClientValidation' => true,
				'clientOptions' => [
					'validateOnSubmit' => true,
					'validateOnChange' => false,
					'validateOnType' => false,
					//errorSummary
					'beforeValidate' => 'js:function(form){$(form).find("button[type=\'submit\']").prop("disabled", true); return true;}',
					'afterValidate' => 'js:function(form, data, hasError){$(form).find("button[type=\'submit\']").prop("disabled", false);if(hasError){fScrollTo(form.find(".errorSummary"))}; return !hasError;}',
				],
				'htmlOptions' => [
					'hideErrorMessage' => false,
					'data-min-sum' => $minSumCart,
					'enctype' => 'multipart/form-data',
				],
			]
		); ?>

		<div class="main__cart-box">
			<div class="cart-left">
				<?=$this->renderPartial('_product_table', [
					'positions' => $positions,
					'coupons' => $coupons,
				])?>

				<?if($minSumCart > 0 && $minSumCart > Yii::app()->cart->getCost()):?>
					<div class="cart-box__subtotal">
						Товаров на сумму: <span id="cart-full-cost"><?=Yii::app()->cart->getCost()?></span><span class="ruble"> <?= $currency ?></span>
					</div>
					<div class="cart-box__subtotal subtotal-coupon hidden">
						Скидка: <span id="coupon-full-cost"></span><span class="ruble"> <?= $currency ?></span>
					</div>

					<div class="cart__min-sum">
						<div class="errorSummary">
							Минимальная сумма заказа <?=ProductHelper::getFormatPrice($minSumCart)?> <?=$currency?>
						</div>
					</div>

					<?php if (!empty($deliveryTypes)){
						foreach ($deliveryTypes as $key => $delivery){
							if($delivery->pickup_group_id){
								$this->setTemplateVar('pickup-map', $this->widget('pickup', ['deliveryId' => $delivery->id, 'group' => $delivery->pickup_group_id], true));							break;
							}
						}
					}
					?>
				<?else:?>
					<?= $form->errorSummary($order); ?>

					<div class="cart__order-form">
						<div class="cart__order-form-title">Ваши данные</div>
						<div class="cart__order-form-top">
							<div class="form__input-item order-form__item">
								<div class="form-group">
									<?= $form->labelEx($order, 'name', ['class' => 'form-group__label']); ?>
									<div class="form-group__input">
										<?= $form->textField($order, 'name', ['class' => 'input']); ?>
									</div>
									<div class="form-group__help hidden">
										<?= $form->error($order, 'name'); ?>
									</div>
								</div>
							</div>

							<div class="form__input-item order-form__item">
								<div class="form-group">
									<?= $form->labelEx($order, 'phone'); ?>
									<div class="form-group__input">
										<?= $form->textField($order, 'phone', ['class' => 'input input-phone']); ?>
									</div>
									<div class="form-group__help hidden">
										<?= $form->error($order, 'phone'); ?>
									</div>
								</div>
							</div>

							<div class="form__input-item order-form__item">
								<div class="form-group">
									<?= $form->labelEx($order, 'email'); ?>
									<div class="form-group__input">
										<?= $form->textField($order, 'email', ['class' => 'input input-email']); ?>
									</div>
									<div class="form-group__help hidden">
										<?= $form->error($order, 'email'); ?>
									</div>
								</div>
							</div>
						</div>
						<div class="form__input-item order-form__item order-form__comment">
							<div class="form-group">
								<?= $form->labelEx( $order,'comment'); ?>
								<div class="form-group__input">
									<?= $form->textArea($order, 'comment', ['class' => 'input']); ?>
								</div>
							</div>
						</div>
					</div>

					<?if(sizeof($deliveryTypes) > 0):?>
						<div class="order-box order-delivery">
							<?$this->renderPartial('_delivery_v2', [
								'order' => $order,
								'deliveryTypes' => $deliveryTypes,
							]);?>

							<div class="order-box-delivery__address">
								<div class="order-form">
									<div class="order-form__item order-form__address">
										<div class="form-group">
											<?= $form->labelEx($order,'address', ['label' => 'Адрес доставки *','class' => 'form-group__label']); ?>
											<div class="form-group__input">
												<?= $form->textArea($order, 'address', ['class' => 'input']); ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?endif;?>

					<?if(!empty($paymentSystems) && sizeof($paymentSystems) > 1):?>
						<div class="order-box order-payment">
							<div class="order-box__inner">
								<div class="order-box__header">Способ оплаты</div>
								<div class="order-box__body">
									<div class="order-box-payment">
										<div class="order-box-payment__type">
											<?$i = 0;?>
											<?php foreach ($paymentSystems as $key => $payment): ?>
												<div class="order-box__item">
													<input type="radio" name="Order[payment_method_id]" id="payment-<?= $payment->id; ?>" class="radio__input" value="<?= $payment->id; ?>" <?=($payment->id == $order->payment_method_id || (!$order->payment_method_id && $i == 1) ? 'checked' : '')?>>

													<label for="payment-<?= $payment->id; ?>">
														<div class="order-box__item-title">
															<span><?= $payment->name; ?></span>
														</div>
													</label>

													<?if($payment->description):?>
														<div class="radio-body__text"><?=$payment->description;?></div>
													<?endif;?>
												</div>
											<?endforeach;?>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?endif;?>

				<?endif;?>
			</div>

			<div class="cart-right">
				<div class="cart-box__subtotal-wrapper">
					<div class="cart-box__subtotal-price">
						<div class="cart-box__subtotal-price-title">Сумма заказа</div>
						<div class="cart-box__subtotal">
							<span class="cart-subtotal-title">Товары:</span> <span id="cart-full-cost"><?=Yii::app()->cart->getCost()?></span><span class="ruble"> <?= $currency ?></span>
						</div>
						<div class="cart-box__subtotal subtotal-coupon hidden">
							<span class="cart-subtotal-title">Скидка:</span> <span id="coupon-full-cost"></span><span class="ruble"> <?= $currency ?></span>
						</div>

						<div class="cart-box__subtotal">
							<span class="cart-subtotal-title">Доставка:</span> <span id="cart-shipping-cost">0</span><span class="ruble"> <?= $currency ?></span>
						</div>

						<div class="cart-box__subtotal cart-box__subtotal-resume">
							<span class="cart-subtotal-title">Итого:</span> <span id="cart-full-cost-with-shipping">0</span><span class="ruble"> <?= $currency ?></span>
						</div>

						<div class="cart-box__order-button">
							<button type="submit" class="btn">Оформить заказ</button>
						</div>
						<div class="cart__privacy-policy">Оформляя заказ Вы соглашаетесь с <a href="/privacy-policy/" target="_blank" rel="nofollow">политикой конфиденциальности</a></div>
					</div>

					<?php if (Yii::app()->hasModule('coupon')): ?>
						<div class="cart-box__subtotal-coupon">
							<div class="order-box__coupon">
								<div class="coupon-box">
									<div class="order-form__item">
										<div class="form-group">
											<div class="coupon-box-title">Купон на скидку</div>
											<label class="form-group__label coupon-label" for="coupon-code">Введите номер купона или подарочной карты</label>
											<div class="form-group__input coupon-input-box">
												<input type="text" id="coupon-code" class="input coupon-box__input">&nbsp;&nbsp;
												<button class="btn btn_primary coupon-box__button" type="button" id="add-coupon-code">Применить</button>
											</div>
											<div class="coupon-messages"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endif;?>
				</div>
			</div>
		</div>
		<?php $this->endWidget(); ?>
	<?php endif; ?>
</div>
<?if(Yii::app()->getRequest()->getIsAjaxRequest()):?>
	<script type="text/javascript">
		applyStyler();

		if($('.input-phone').length){
			$.each($('.input-phone'), function () {
                setPhoneMask($(this));
			});
		}
	</script>
<?endif;?>
