<?
	$currency = Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency);
?>
<?php if (!empty($deliveryTypes)): ?>
	<div class="form__input-item order-form__item order-form__radio order-box-delivery">
		<div class="form-group">
			<?= $form->labelEx($order, 'delivery', ['class' => 'form-group__label']); ?>
			<div class="form-group__input">
				<?$i = 0;?>
				<?php foreach ($deliveryTypes as $key => $delivery): ?>
					<?$i++;?>
					<div class="order-radio__item">
						<input type="radio" name="Order[delivery_id]" id="delivery-<?= $delivery->id; ?>" class="radio__input" value="<?= $delivery->id; ?>" <?=($delivery->id == $order->delivery_id || (!$order->delivery_id && $i == 1) ? 'checked' : '')?>
							   data-price="<?= $delivery->price; ?>" data-free-from="<?= $delivery->free_from; ?>" data-available-from="<?= $delivery->available_from; ?>" data-separate-payment="<?= $delivery->separate_payment; ?>"
							   data-currency="<?=$currency?>" data-free-text="<?=($delivery->free_text ? $delivery->free_text : 'Бесплатно')?>"
							   data-need-address="<?=$delivery->need_address?>">

						<label for="delivery-<?= $delivery->id; ?>">
							<div class="order-box__item-title">
								<span><?= $delivery->name; ?></span> -
								<span class="real-price"></span>
							</div>
						</label>

						<div class="radio-body__text">
							<?echo $delivery->description;?>
						</div>

					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
