<?php
$currency = Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency);
?>
<div id="shopping-cart-widget">
	<div class="cart-widget" id="cart-widget" data-cart-widget-url="<?= Yii::app()->createUrl('/cart/cart/widget'); ?>">
	<?php if (Yii::app()->cart->isEmpty()): ?>
		<p>Ваша корзина пуста</p>
	<?else:?>
		<a href="<?=Yii::app()->createUrl('/cart/cart/index')?>">
			<span><?=Yii::app()->cart->getItemsCount()?></span>
			<?=Helper::pluralForm(Yii::app()->cart->getItemsCount(), ['товар', 'товара', 'товаров'])?>
			на <?=ProductHelper::getFormatPrice(Yii::app()->cart->getCost())?> <?=$currency?>
		</a>
	<?/*
		<div class="cart-widget__items">
			<?php foreach (Yii::app()->cart->getPositions() as $product): ?>
				<?php $price = $product->getFormatPrice(); ?>
				<div class="cart-mini__item">
					<div class="cart-mini__thumbnail">
						<img src="<?= $product->getImageUrl(70, 70, false); ?>" class="cart-mini__img"/>
					</div>
					<div class="cart-mini__info">
						<div class="cart-mini__title">
							<?= CHtml::link($product->name, ProductHelper::getUrl($product),
								['class' => 'cart-mini__link']); ?>
						</div>
						<div class="cart-mini__base-price">
							<?= $price ?>
							<span class="ruble"><?= Yii::t('CartModule.cart', $currency); ?></span>
							&times; <?= $product->getQuantity(); ?> <?= Yii::t('CartModule.cart', 'pcs'); ?>
							=
							<div class="product-price">
								<?= ProductHelper::getFormatPrice($product->getSumPrice()); ?>
								<span class="ruble"><?= Yii::t('CartModule.cart', $currency); ?></span>
							</div>
						</div>
					</div>
					<div class="cart-mini__delete-btn mini-cart-delete-product" data-position-id="<?= $product->getId(); ?>">
						&times;
					</div>
				</div>
			<?php endforeach; ?>
			<a class="btn cart-mini__to-cart" href="<?=Yii::app()->createUrl('/cart/cart/index')?>">Оформить заказ</a>
		</div>
	*/?>
	<?endif;?>
	</div>
</div>