<?
/**
 * @var CartProduct $product
 */
$currency = Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency);
?>
<div class="modal-window add-in-cart">
	<div class="modal-content">
		<div class="modal-header">
			<div class="modal-close">&times;</div>
		</div>

		<div class="left">
			<div class="cart-modal-inner">
				<div class="cart-modal-inner__product-image">
					<img src="<?= StoreImage::product($product, 120, 120, false); ?>" alt="<?= CHtml::encode($product->getImageAlt()); ?>">
				</div>
				<div class="cart-modal-inner__product-info">
					<div class="cart-modal-inner__product-title">
						<?= CHtml::encode($product->getName()); ?>
					</div>
					<div class="cart-modal-inner__product-characteristics">
                        <?
                        $addCharacteristic = '';
                        if($product->selectedVariants){
                            $i = 0;
                            foreach ($product->selectedVariants as $variant){
                                if($i == 0 && $variant['sku']){
                                    $product->sku = $variant['sku'];
                                }

                                $addCharacteristic .= '<dl class="product-spec-item">
                                    <dt class="product-spec-item__name">
                                        <span class="product-spec-item__name-inner">'.$variant->attribute->title.':</span>
                                    </dt>
                                    <dd class="product-spec-item__value">
                                        <span class="product-spec-item__value-inner">'.$variant->getOptionValue().'</span>
                                    </dd>
                                </dl>';
                            }
                        }

                        $this->renderPartial('/product/_simple_attributes', ['product' => $product, 'short' => false]);
                        echo $addCharacteristic;
                        ?>
					</div>
				</div>
			</div>
		</div>

		<div class="right">
			<div class="modal-title">Товар добавлен в корзину</div>
			<div class="cart-modal-inner">
				<div class="modal-title__count-in-cart">В корзине <?=Yii::app()->cart->getItemsCount()?> <?=Helper::pluralForm(Yii::app()->cart->getItemsCount(), ['товар', 'товара', 'товаров'])?></div>
				<div class="cart-modal-inner__summ">На <span><?=ProductHelper::getFormatPrice(Yii::app()->cart->getCost());?> <?= Yii::t('StoreModule.store', $currency); ?></span></div>
			</div>
			<div class="cart-modal-inner__buttons">
				<input type="checkbox" class="disable-cart-window styler" id="disable-cart-window"> <label for="disable-cart-window">Больше не показывать</label>

				<div class="cart-modal-inner__buttons-action">
					<span class="button modal-close">Продолжить покупки</span>
					<a class="button" href="<?=Yii::app()->createUrl('/cart/cart/index')?>">Оформить заказ</a>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('.add-in-cart .styler').styler();
	});
</script>