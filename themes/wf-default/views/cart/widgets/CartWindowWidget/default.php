<?
/**
 * @var CartProduct $product
 */
$currency = Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency);
?>
<div class="modal-window add-in-cart">
	<div class="modal-content">
		<div class="modal-header">
			<div class="modal-close">&times;</div>
		</div>

		<div class="cart-modal-inner">
			<div class="modal-title">Товар добавлен в корзину</div>

			<div class="cart-modal-count">
				<div class="modal-title__count-in-cart">В корзине <?=Yii::app()->cart->getItemsCount()?> <?=Helper::pluralForm(Yii::app()->cart->getItemsCount(), ['товар', 'товара', 'товаров'])?>,</div>
				<div class="cart-modal-inner__summ">на сумму <span><?=ProductHelper::getFormatPrice(Yii::app()->cart->getCost());?> <?= Yii::t('StoreModule.store', $currency); ?></span></div>
			</div>

			<div class="cart-modal-inner__buttons">
				<div class="cart-modal-inner__buttons-action">
					<span class="button modal-close">Продолжить покупки</span>
					<a class="button" href="<?=Yii::app()->createUrl('/cart/cart/index')?>">Оформить заказ</a>
				</div>
			</div>

			<div class="cart-modal-inner__checkbox">
				<input type="checkbox" class="disable-cart-window styler" id="disable-cart-window"> <label for="disable-cart-window">Больше не показывать</label>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('.add-in-cart .styler').styler();
	});
</script>
