<?php
/**
 * @var Callback $model
 * @var TbActiveForm $form
 */
?>
<!--noindex-->
<div class="modal-window change-city" data-on-show="afterChangeCityOpen">
	<div class="modal-content">
		<div class="modal-header">
			<div class="modal-close">&times;</div>
			<div class="modal-title">Выбрать город</div>
		</div>
		<?=CHtml::beginForm(Yii::app()->createUrl('/home/setCity'), 'get', [
			'class' => 'change-city-form',
			'data-search-action' => Yii::app()->createUrl('/home/searchCity'),
			'data-set-action' => Yii::app()->createUrl('/home/setCity'),
		]);?>

		<div class="modal-body">
			<div class="form-inner">
				<div class="modal__input-item modal__input-item-text">
					<?=CHtml::textField('city', '', [
                        'placeholder' => 'Начните вводить свой город...',
                        'class' => 'search-city styler',
                    ])?>
				</div>

				<?=CHtml::hiddenField('city-id', '', ['class' => 'city-id',])?>

				<div class="modal__input-item modal__input-item-submit">
					<input type="button" value="Выбрать" class="btn disabled save-city" />
				</div>
			</div>
		</div>
		<?=CHtml::endForm(); ?>
	</div>
</div>
<!--/noindex-->