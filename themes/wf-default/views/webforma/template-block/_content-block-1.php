<div class="mp_promo">
	<div class="wrapper">
		<?php $this->widget('promo', ['limit' => 8, 'width' => 619, 'height' => 275]);?>
	</div>
</div>

<div class="mp_articles">
	<div class="wrapper">
		<div class="h3"><span>Статьи</span>
			<div class="view_all"><a rel="nofollow" href="<?=Yii::app()->createUrl('/articles/articles/index')?>">Все статьи</a></div>
		</div>

		<div class="mp_articles_wrap">
			<?php $this->widget('articles', ['limit' => 9, 'width' => 358, 'height' => 227, 'view' => 'last-articles-with-images']);?>
		</div>
	</div>
</div>
