<?php
/**
 * @var Pickup[] $items
 */
$pickups = [];
$rand = mt_rand();
if(!empty($items)):
    $cCoord1 = $cCoord2 = $count = 0;

?>
<div class="pickup-window-title">Выберите пункт самовывоза</div>
<div class="pickup-wrapper"<?=($deliveryId) ? ' data-id="'.$deliveryId.'"' : ''?>>
	<div class="pickup-nav">
		<div class="pickup-nav_all">
			Назад к списку
		</div>

		<?foreach ($items as $item):
			list($coord1, $coord2) = explode(',', $item->coords);
			$coord1 = trim($coord1);
			$coord2 = trim($coord2);
			if (!$coord1 || !$coord2) {
				continue;
			}
			$count++;
			$pickups[$item->id] = [
				'name' => $item->name,
				'coord1' => $coord1,
				'coord2' => $coord2,
			];
			$cCoord1 += $coord1;
			$cCoord2 += $coord2;
		?>
			<div class="pickup-nav__item" data-id="<?=$item->id?>">
				<div class="pickup-item__title">
					<span><?=$item->name;?>
				</div>
				<div class="pickup-item__info">
					<?if($item->description):?>
						<div class="pickup-item__list-item">
							<div class="pickup-item__list-description">
								<?=$item->description?>
							</div>
						</div>
					<?endif;?>

					<?if($item->payment):?>
						<div class="pickup-item__list-item">
							<div class="pickup-item__list-title">Форма оплаты:</div>
							<div class="pickup-item__list-value">
								<?=implode(', ', $item->getFormattedPayment());?>
							</div>
						</div>
					<?endif;?>

					<?if($item->worktime):?>
						<div class="pickup-item__list-item">
							<div class="pickup-item__list-title">Время работы:</div>
							<div class="pickup-item__list-value">
								<?=$item->getWorktimeAsRows()?>
							</div>
						</div>
					<?endif;?>

					<?if($item->how_to_get):?>
						<div class="pickup-item__list-item">
							<div class="pickup-item__list-title">Ближайшие остановки:</div>
							<div class="pickup-item__list-value">
								<?=$item->how_to_get?>
							</div>
						</div>
					<?endif;?>

					<?if($deliveryId):?>
						<div class="pickup-item__list-button">
							<button class="btn select-pickup-item" data-id="<?=$item->id?>" data-address="<?=CHtml::encode($item->name)?>">Выбрать пункт</button>
						</div>
					<?endif;?>
				</div>
			</div>
		<?endforeach;?>
	</div>

	<div class="pickup-map" id="map_<?=$rand?>"></div>
	<div class="clear"></div>
</div>

<?
if($cCoord1 && $cCoord2){
	$cCoord1R = $cCoord1 / $count;
	$cCoord2R = $cCoord2 / $count;
	$script = 'mapInit('.$rand.','.$cCoord1R.','.$cCoord2R.','.json_encode($pickups).');';
	Yii::app()->clientScript->registerScript('pickup', $script, CClientScript::POS_END);
}
endif;