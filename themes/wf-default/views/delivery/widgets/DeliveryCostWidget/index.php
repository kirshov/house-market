<?
/**
 * @var $this DeliveryCostWidget
 */
if(Yii::app()->hasComponent('geoLocateManager')){
	$currentCity = Yii::app()->getComponent('geoLocateManager')->getCurrentCity();
}
Yii::app()->setParams(['needChangeCity' => true]);
?>
<div class="delivery-cost-widget">
	<?if(!$currentCity || $currentCity == Yii::app()->getModule('webforma')->mainCity):?>
		<div class="widget-title">Доставка по <span class="a modal-show" data-modal="change-city">Екатеринбургу</span></div>
		<?if($product->getResultPrice() >= 1000):?>
			<div class="delivery-widget__list-item">
				<div class="delivery-widget__list-item__title">Курьером</div>
				<div class="delivery-widget__list-item__value">Бесплатно</div>
			</div>
		<?else:?>
			<div class="delivery-widget__list-item">
				<div class="delivery-widget__list-item__title">Курьером</div>
				<div class="delivery-widget__list-item__value">150 руб.</div>
			</div>
		<?endif;?>

		<div class="delivery-widget__list-item">
			<div class="delivery-widget__list-item__title">Самовывоз</div>
			<div class="delivery-widget__list-item__value">Бесплатно</div>
		</div>
	<?else:?>
		<div class="widget-title">Доставка в <span class="a modal-show" data-modal="change-city"><?=$currentCity;?></span></div>

		<div class="delivery-widget__list">
            <?if($isCdek):?>
                <div class="delivery-widget__list-item">
                    <div class="delivery-widget__list-item__title">ТК СДЕК до пункта самовывоза</div>
                    <div class="delivery-widget__list-item__value cdek cdek_stock loader">&nbsp;</div>
                </div>
                <div class="delivery-widget__list-item">
                    <div class="delivery-widget__list-item__title">ТК СДЕК до квартиры</div>
                    <div class="delivery-widget__list-item__value cdek cdek_apartment loader">&nbsp;</div>
                </div>
            <?endif;?>

			<div class="delivery-widget__list-item">
				<div class="delivery-widget__list-item__title">Почта России</div>
				<div class="delivery-widget__list-item__value">300 руб.</div>
			</div>
		</div>
	<?endif;?>
</div>