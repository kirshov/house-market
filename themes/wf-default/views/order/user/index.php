<?php
/* @var $orders Order[] */

$title = Yii::t('OrderModule.order', 'Orders history');
$this->title = Helper::prepareSeoTitle($title);
$this->breadcrumbs = [
    $title,
];
$currency = Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency);
$this->setTitle($title);
$this->layout = '//layouts/profile';
$this->setTemplateVar('bodyClass', 'page-profile page-history');
?>
<div class="page page-cart">
    <?if($orders):?>
        <div class="main__cart-box">
            <div class="order-box">
                <div class="order-box__header order-box__header_black">
                    <div class="cart-list-header">
                        <div class="cart-list__column cart-list__column_num">Номер заказа</div>
                        <div class="cart-list__column cart-list__column_date"><?= Yii::t("OrderModule.order", "Date");?></div>
                        <div class="cart-list__column cart-list__column_sum"><?= Yii::t("OrderModule.order", "Total price");?></div>
                        <div class="cart-list__column cart-list__column_status"><?= Yii::t("OrderModule.order", "Status");?></div>
                    </div>
                </div>
                <div class="orders-list">
                    <?php foreach ((array)$orders as $order): ?>
                        <div class="cart-list__item">
                            <div class="cart-item">

                                <div class="cart-list__column cart-list__column_num">
                                    <?= CHtml::link(
                                        Yii::t('OrderModule.order', 'Order #{n}', [$order->getNumberForUser()]),
                                        ['/order/order/view', 'url' => $order->url],
                                        ['class' => 'cart-item__link']
                                    ) . ($order->paid ? ' - ' . $order->getPaidStatus() : ''); ?>
                                </div>
                                <div class="cart-list__column cart-list__column_date"><?= date('d.m.Y в H:i', strtotime($order->date)); ?></div>
                                <div class="cart-list__column cart-list__column_sum"><?= ProductHelper::getFormatPrice($order->getTotalPriceWithDelivery()); ?> <?=$currency;?></div>
                                <div class="cart-list__column cart-list__column_status <?=$order->getStatusHtmlClass()?>"><span><?= CHtml::encode($order->getStatusTitle()); ?></span></div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?else:?>
        <div class="empty-data">У вас еще нет заказов, <a href="<?=Yii::app()->createUrl('/store/category/index')?>">перейти в каталог</a></div>
    <?endif?>
</div>
