<?
/**
 * @var CartProduct $product
 * @var integer $positionId
 * @var Order $order
 */
$currency = Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency);
$minSumCart = Yii::app()->getModule('store')->minCost;
?>
<div class="modal-window buy-one-click <?=$positionId?>" data-url="<?=Yii::app()->createUrl('/order/order/buyOneClick');?>">
	<div class="modal-content">
		<div class="modal-header">
			<div class="modal-close">&times;</div>
		</div>

		<div class="cart-modal-inner">
            <div class="modal-title">Купить в 1 клик</div>

            <?if($minSumCart > 0 && $minSumCart > $price):?>
                <div class="bs-callout bs-callout-danger alert-block">
                    Минимальная сумма заказа <?=ProductHelper::getFormatPrice($minSumCart)?> <?=$currency?>
                </div>
            <?else:?>
                <div class="form-wrapper">
                    <?php $form = $this->beginWidget('CActiveForm', [
						'action' => ['/order/order/buyOneClick'],
						'id' => 'boc-form',
						'enableClientValidation' => true,
						'clientOptions' => [
							'validateOnSubmit' => true,
							'validateOnChange' => false,
							'validateOnType' => false,
						],
						'htmlOptions' => [
							'hideErrorMessage' => false,
							'class' => 'buy-one-click__form',
						],
					]); ?>

					<?/*
					<div class="errorSummary bs-callout bs-callout-danger alert-block">
						<?= $form->errorSummary($order); ?>
					</div>
					*/?>

                    <div class="main__cart-box">
                        <div class="cart__order-form">
                            <div class="form__input-item order-form__item modal__input-item-text">
                                <div class="form-group">
                                    <?= $form->labelEx($order, 'name', ['class' => 'form-group__label']); ?>
                                    <div class="form-group__input">
                                        <?= $form->textField($order, 'name', ['class' => 'input']); ?>
                                    </div>
                                    <div class="form-group__help">
                                        <?= $form->error($order, 'name'); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form__input-item order-form__item modal__input-item-text">
                                <div class="form-group">
                                    <?= $form->labelEx($order, 'phone'); ?>
                                    <div class="form-group__input">
                                        <?= $form->textField($order, 'phone', ['class' => 'input input-phone']); ?>
                                    </div>
                                    <div class="form-group__help">
                                        <?= $form->error($order, 'phone'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="order-box__bottom">
                            <div class="cart-box__order-button">
                                <button type="submit" class="btn">Оформить заказ</button>
                            </div>

                            <div class="cart__privacy-policy"><?=Yii::app()->getModule('webforma')->getPrivacyTextForm();?></div>
                        </div>
                    </div>
                    <?php
                        echo CHtml::hiddenField('OrderProduct['.$positionId.'][product_id]', $product->id);
                        if(is_array($variants)){
                            foreach ($variants as $variant) {
                                echo CHtml::hiddenField('OrderProduct[' . $positionId . '][variant_ids][]', $variant);
                            }
                        }
                        echo CHtml::hiddenField('OrderProduct[' . $positionId . '][quantity]', $count);
                        echo $form->hiddenField($order, 'is_boc', ['value' => 1]);
                    ?>
                    <?php $this->endWidget(); ?>
                </div>
            <?endif;?>
		</div>
	</div>
</div>