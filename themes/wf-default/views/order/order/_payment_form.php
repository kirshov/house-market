<?/**
 * @var PaymentSystem[] $paymentSystems
 * @var bool $readyPaymentItem
 * @var string $paymentForm
 * @var int $paymentOption
 */

$paymentCount = 0;
$readyPayments = 0;
$forceShowForm = false;
$forms = '';

foreach ($paymentSystems as $payment){
	if($paymentOption == PaymentModule::PAYMENT_OPTION_CART && $payment->id != $model->payment_method_id) {
		continue;
	}
	$paymentForm = $payment->getPaymentForm($model);

	if(!$paymentForm){
		$paymentForm = '<div class="bs-callout bs-callout-danger error-block">Не удалось подключиться к платежной системе</div>';
		$readyPaymentItem = false;
	} else{
		$readyPayments++;
		$readyPaymentItem = true;
	}
	$forms .= $this->renderPartial('_payment_form_item', [
		'payment' => $payment,
		'readyPaymentItem' => $readyPaymentItem,
		'paymentForm' => $paymentForm,
		'paymentCount' => $paymentCount,
	], true);
	$paymentCount++;
}
?>
<?if($paymentOption == PaymentModule::PAYMENT_OPTION_MODERATION && in_array($model->status_id, [OrderStatus::STATUS_NEW, OrderStatus::STATUS_ACCEPTED, OrderStatus::STATUS_WORK])):?>
	<div class="notice">Возможность оплатить заказ онлайн появится после подтверждения заказа менеджером</div>
<?elseif($paymentCount && ($paymentOption == PaymentModule::PAYMENT_OPTION_CART && $model->isPaymentMethodSelected()) || ($paymentOption == PaymentModule::PAYMENT_OPTION_MODERATION && $model->status_id == OrderStatus::STATUS_WAIT_PAYMENT)):?>
	<div class="order-box order-payment">
		<div class="order-box order-payment-inner">
			<div class="order-box__header<?=($paymentCount == 1 && $readyPayments == 1) ? ' hidden' : ''?>">Оплатить заказ</div>

			<div class="order-box__body">
				<div class="order-box-payment">
					<div class="order-box-payment__type">
						<?=$forms?>
					</div>
				</div>
			</div>
		</div>
		<?/*if($readyPayments > 0):*/?><!--
			<div class="order-box__bottom order-box__bottom">
				<button type="submit" class="btn btn_big btn_primary" id="start-payment">Оплатить online</button>
				<div class="bs-callout bs-callout-danger error-block hidden"></div>
			</div>
		--><?/*endif;*/?>
	</div>
<?endif;?>