<?php
/* @var $model Order */
$title = 'Заказ №'.$model->getNumberForUser().' успешно отправлен';
$this->title = Helper::prepareSeoTitle($title);
$this->setTitle($title.'<div>Статус: <span class="'.$statusClass.'">'.$model->status->name.'</span></div>');

if(isset(Yii::app()->paymentManager)){
	$paymentSystems = Yii::app()->paymentManager->getPayments();
}

if(Helper::hasFlash('orderCreated')){
	Helper::setFlash('orderCreated', null);
	Yii::app()->getClientScript()->registerScript('orderDoTarget', '$(function(){doTarget("orderСart");});');
}
$this->setTemplateVar('bodyClass', 'page-profile page-order');
?>

<div class="h3 fill"><?=Yii::app()->getModule('order')->afterMessage;?></div>
<?php
$this->renderPartial('_order_detail', [
	'model' => $model,
	'paymentSystems' => $paymentSystems,
]);
?>
