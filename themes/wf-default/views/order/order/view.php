<?php
/* @var $model Order */
$title = 'Заказ №'.$model->getNumberForUser();
$this->title = Helper::prepareSeoTitle($title);
$statsTitle = in_array($model->status_id, [OrderStatus::STATUS_PICKUP, OrderStatus::STATUS_DELIVERY, OrderStatus::STATUS_DELETED, OrderStatus::STATUS_FINISHED]) ? '' : 'Статус: ';
$this->setTitle($title.'<div>'.$statsTitle.'<span class="'.$model->getStatusHtmlClass().'">'.$model->status->name.'</span></div>');

$this->breadcrumbs = [];
if(Yii::app()->getUser()->isAuthenticated()){
    $this->breadcrumbs[Yii::t('OrderModule.order', 'Orders history')] = Yii::app()->createUrl('/order/user/index');
}
$this->breadcrumbs[] = $title;

if(isset(Yii::app()->paymentManager)){
	$paymentSystems = Yii::app()->paymentManager->getPayments();
}
$currency = Yii::t("OrderModule.order", Yii::app()->getModule('store')->currency);
$this->setTemplateVar('bodyClass', 'page-profile page-order');

$this->renderPartial('_order_detail', [
	'model' => $model,
	'paymentSystems' => $paymentSystems,
]);


