<?
/**
 * @var Order $model
 */
$currency = Yii::t("OrderModule.order", Yii::app()->getModule('store')->currency);

$statusClass = '';
if($model->status_id == OrderStatus::STATUS_FINISHED){
	$statusClass = 'success';
} elseif($model->status_id == OrderStatus::STATUS_DELETED){
	$statusClass = 'text-error';
}

?>
<div class="page page-order">
	<a name="pay"></a>
	<?if($model->paid):?>
		<p><?=$model->getPaidStatus()?></p>
	<?endif;?>
	<div class="main__cart-box">
		<div class="order-box">
			<div class="order-box__header order-box__header_black">
				<div class="cart-list-header">
					<div class="cart-list__column cart-list__column_info">Инфомация о товаре</div>
					<div class="cart-list__column cart-list__column_price">Стоимость</div>
					<div class="cart-list__column cart-list__column_count">Количество</div>
					<div class="cart-list__column cart-list__column_sum">Сумма</div>
				</div>
			</div>
			<div class="cart-list">
				<?php foreach ((array)$model->products as $position): ?>
					<?php
					$product = $position->product;
					if($product){
						$productUrl = ProductHelper::getUrl($product);
					}
					?>
					<div class="cart-list__item">
						<div class="cart-item">
							<div class="cart-item__info">
								<div class="cart-item__thumbnail">
									<?if($product):?>
										<a href="<?= $productUrl; ?>">
											<img src="<?= $product->getImageUrl(90, 90, false); ?>" class="cart-item__img"/>
										</a>
									<?endif;?>
								</div>

								<div class="cart-item__content">
									<div class="cart-item__title">
										<?if($product):?>
											<a href="<?= $productUrl; ?>" class="cart-item__link"><?= CHtml::encode($position->getNameInEmail()); ?></a>
										<?else:?>
											<span class="cart-item__link"><?= CHtml::encode($position->product_name); ?></span>
										<?endif;?>
									</div>

									<?php if ($product && $product->getCategoryId()): ?>
										<div class="cart-item__category"><?= $product->category->name ?></div>
									<?php endif; ?>

									<?
									$variantsData = [];
									if($position->variantsArray){
										$i = 0;
										foreach ($position->variantsArray as $variant){
											if($i == 0 && $variant['sku']){
												$variantsData[] = '<span class="cart-item__variant-item">Артикул: '.$variant['sku'].'</span>';
												$i++;
											}
											$variantsData[] = '<span class="cart-item__variant-item">'.$variant['attribute_title'].': '.$variant['optionValue'].'</span>';
										}
									} elseif($product->sku) {
										$variantsData[] = '<span class="cart-item__variant-item">Артикул: '.$product->sku.'</span>';
									}
									?>
									<?if($variantsData):?>
										<div class="cart-item__data"><?=implode(', ', $variantsData);?></div>
									<?endif;?>
								</div>
							</div>
							<div class="cart-item__price"><?= ProductHelper::getFormatPrice($position->price); ?><span class="ruble"> <?= $currency; ?></span></div>
							<div class="cart-item__quantity"><?= $position->quantity; ?></div>
							<div class="cart-item__summ"><?= ProductHelper::getFormatPrice($position->getTotalPrice()); ?><span class="ruble"> <?= $currency ?></span></div>
						</div>
					</div>
				<?php endforeach; ?>

				<?php if ($model->hasCoupons()): ?>
					<?foreach ($model->getCoupons() as $coupon):?>
						<div class="cart-list__item coupon">
							<div class="cart-item">
								<div class="cart-item__info">
									<div class="cart-item__thumbnail empty">
									</div>

									<div class="cart-item__content">
										<div class="cart-item__title"><?= $coupon->name; ?></div>
									</div>
								</div>

								<div class="cart-item__price"></div>
								<div class="cart-item__quantity"></div>

								<div class="cart-item__summ">-<?=ProductHelper::getFormatPrice($coupon->value)?> <span class="ruble"> <?= CouponType::title($coupon->type) ?></span></div>
							</div>
						</div>
					<?endforeach;?>
				<?php endif; ?>
				<?php if($model->delivery):?>
					<?$titeDelivery = mb_stripos($model->delivery->name, 'доставка') !== false ? '' : 'Доставка: '?>
					<div class="delivery-notice"><?=$titeDelivery.$model->delivery->name?></div>
				<?endif;?>
			</div>
			<div class="order-box__bottom">
				<div class="cart-box__subtotal">
					<span class="cart-subtotal-title">Товаров:</span> <?= ProductHelper::getFormatPrice($model->getFullTotalPrice()); ?><span class="ruble"> <?= $currency ?></span>
				</div>
				<?if($model->isDiscount()):?>
					<div class="cart-box__subtotal">
						<span class="cart-subtotal-title">Скидка:</span> <?= ProductHelper::getFormatPrice($model->getDiscountPrice()); ?><span class="ruble"> <?= $currency ?></span>
					</div>
				<?endif;?>
				<div class="cart-box__subtotal">
					<span class="cart-subtotal-title">Стоимость доставки:</span> <?= ProductHelper::getFormatPrice($model->getDeliveryPrice());?><span class="ruble"> <?= $currency ?></span>
				</div>
				<div class="cart-box__subtotal cart-box__subtotal-resume">
					<span class="cart-subtotal-title">Итого:</span> <?= ProductHelper::getFormatPrice($model->getTotalPriceWithDelivery()); ?><span class="ruble"> <?= $currency ?></span>
				</div>
			</div>
		</div>
	</div>
	<?
	$paymentOption = Yii::app()->getModule('payment')->paymentOption;
	if($paymentOption && !$model->isPaid() && Yii::app()->hasModule('payment') && sizeof($paymentSystems) > 0){
		$this->renderPartial('_payment_form', [
			'paymentOption' => $paymentOption,
			'paymentSystems' => $paymentSystems,
			'model' => $model,
		]);
	}
	?>
	<?php if($model->isPaid()): ?>
		<div class="bs-callout bs-callout-success">Заказ оплачен</div>
	<?php endif; ?>
</div>
