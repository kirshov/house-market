<? /**
 * @var Slider[] $slides
 */
?>
<div class="slider slider_<?=$sliderId?>">
	<?foreach ($slides as $slide):?>
		<div class="slider_item slider-<?=$slide->id?>">
			<?= CHtml::image($slide->getImageUrl($width, $height, false), $slide->alt, ['class' => 'slider_image']); ?>

			<?if($slide->description):?>
				<div class="slider_content">
					<div class="slider_title">
						<?=$slide->name;?>
					</div>
					<div class="slider_description">
						<?=str_replace("\n", '<br/>', $slide->description);?>
						<?if($slide->link):?>
							<div><a href="<?=$slide->link?>">Подробнее</a></div>
						<?endif;?>
					</div>
				</div>
			<?endif;?>
		</div>
	<?endforeach;?>
</div>
<script type="text/javascript">
	$('.slider_<?=$sliderId?>').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		dots: <?=$navigation?>,
		adaptiveHeight: true,
		autoplay:<?=$autoPlay?>,
		autoplaySpeed: <?=$autoPlaySpeed;?>
	});
</script>
