<?php
/**
 * @var Review $review
 */
?>
<div class="review-item">
	<?if($review->image):?>
		<div class="review-item__image">
			<img src="<?=$review->getImageUrl(200, 200, true)?>">
		</div>
	<?endif;?>
	<div class="review-item__info">
		<div class="review-item__name">
			<?=$review->name?>
		</div>

		<?if($review->admin_text):?>
			<div class="review-item__text video">
				<?=$review->admin_text?>
			</div>
		<?else:?>
			<div class="review-item__text">
				<?=$review->text?>
			</div>
		<?endif;?>
	</div>
</div>
