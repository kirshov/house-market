<?php
/*
 * @var Review[] $reviews
 */
?>
<?php if (!empty($reviews)): ?>
	<div class="reviews__items">
		<?php foreach ($reviews as $review): ?>
			<?php $this->render('_review', ['review' => $review]) ?>
		<?php endforeach; ?>
	</div>
<?php endif; ?>