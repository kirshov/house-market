<?
$spamField = Yii::app()->getUser()->getState('spamField');
$spamFieldValue = Yii::app()->getUser()->getState('spamFieldValue');

Yii::app()->clientScript->registerScriptFile(Yii::app()->getModule('review')->getAssetsUrl() . '/js/review.js');
?>

<div class="feedback-form review-form">
    <div class="h3">Добавить отзыв</div>
    <div class="notice">
        Ваши контактные данные не публикуются на сайте, а служат для связи с Вами!
    </div>
    <p></p>

    <?php $form = $this->beginWidget(
        'CActiveForm',
        [
            'action' => Yii::app()->createUrl('/review/review/add/'),
            'id' => 'review-form',

            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'clientOptions' => [
                'validateOnSubmit' => true,
                'validateOnChange' => true,
                'validateOnType' => false,
                'beforeValidate' => 'js:function(form, data, hasError){$(form).find("button[type=\'submit\']").prop("disabled", true); reviewSendForm(form, data, hasError);}',
                'afterValidate' => 'js:function(form, data, hasError){$(form).find("button[type=\'submit\']").prop("disabled", false); return !hasError;}',
            ],
            'htmlOptions' => [
                'hideErrorMessage' => false,
            ],
        ]
    ); ?>

    <?/*= $form->errorSummary($model); */?>
    <div class="bs-callout result-message hidden"></div>
    <?= $form->hiddenField($model, 'parent_id'); ?>
    <?= CHtml::hiddenField('redirectTo', $redirectTo); ?>

    <?= $form->textField($model, 'spamField', [
        'name' => $spamField,
        'style' => 'position:absolute;display:none;visibility:hidden;',
    ]); ?>

    <?= $form->textField($model, 'text', [
        'style' => 'position:absolute;display:none;visibility:hidden;'
    ]); ?>

    <div class="feedback-form__inner">
        <div class="feedback-form__inputs">
            <div class="form__input-item order-form__item">
                <div class="form-group">
                    <?= $form->labelEx($model, 'name', ['class' => 'form-group__label']); ?>
                    <div class="form-group__input">
                        <?= $form->textField($model, 'name', ['class' => 'input']); ?>
                    </div>
                    <div class="form-group__help hidden">
                        <?= $form->error($model, 'name'); ?>
                    </div>
                </div>
            </div>

            <div class="form__input-item order-form__item">
                <div class="form-group">
                    <?= $form->labelEx($model, 'phone'); ?>
                    <div class="form-group__input">
                        <?= $form->textField($model, 'phone', ['class' => 'input input-phone']); ?>
                    </div>
                    <div class="form-group__help hidden">
                        <?= $form->error($model, 'phone'); ?>
                    </div>
                </div>
            </div>

			<div class="form__input-item order-form__item">
				<div class="form-group">
			        <?= $form->labelEx($model, 'rating'); ?>
					<div class="form-group__input">
				        <?= $form->dropDownList($model, 'rating', Review::getRatingRanges(), ['class' => 'styler input input-rating']); ?>
					</div>
					<div class="form-group__help hidden">
				        <?= $form->error($model, 'rating'); ?>
					</div>
				</div>
			</div>

			<?/*
            <div class="form__input-item order-form__item">
                <div class="form-group">
                    <?= $form->labelEx($model, 'email'); ?>
                    <div class="form-group__input">
                        <?= $form->textField($model, 'email', ['class' => 'input input-email']); ?>
                    </div>
                    <div class="form-group__help hidden">
                        <?= $form->error($model, 'email'); ?>
                    </div>
                </div>
            </div>
			*/?>

            <div class="form__input-item order-form__item order-form__comment">
                <div class="form-group">
                    <?= $form->labelEx( $model,'text'); ?>
                    <div class="form-group__input">
                        <?= $form->textArea($model, 'text', ['class' => 'input']); ?>
                    </div>
                </div>
            </div>

            <?php if ($module->showCaptcha && Yii::app()->getUser()->getIsGuest()): ?>
                <?php if (CCaptcha::checkRequirements()) : ?>
                        <div class="form-group">
                            <?php $this->widget(
                                'CCaptcha',
                                [
                                    'showRefreshButton' => true,
                                    'imageOptions' => [
                                        'width' => '150px',
                                        'style' => 'float: left;'
                                    ],
                                    'buttonOptions' => [
                                        'class' => 'btn btn_primary',
                                        'id' => 'captcha-refresh'
                                    ],
                                    'buttonLabel' => 'получить другой код',
                                    'captchaAction' => '/review/review/captcha'
                                ]
                            ); ?>
                        </div>
                    <div class="form-group grid-module-3">
                        <?= $form->textField(
                            $model,
                            'verifyCode',
                            [
                                'class' => 'input',
                                'placeholder' => Yii::t(
                                    'ReviewModule.review',
                                    'Insert symbols you see on picture'
                                )
                            ]
                        ); ?>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>

        <div class="feedback-form__bottom">
            <div class="feedback-form__order-button">
                <button type="submit" class="btn" id="add-review" name="add-review">Отправить</button>
            </div>

            <div class="feedback__privacy-policy"><?=Yii::app()->getModule('webforma')->getPrivacyTextForm();?></div>
        </div>
    <?php $this->endWidget(); ?>
</div>
