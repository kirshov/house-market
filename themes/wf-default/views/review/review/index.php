<?php
/**
 * @var ReviewModule
 * @var string $type
 */
$module = Yii::app()->getModule('review');

$this->setTitle($module->h1);
$this->title = $module->meta_title ?: $module->h1;
$this->description = $module->meta_description;
$this->keywords = $module->meta_keywords;

echo '<div class="page page-review">';
?>

	<div class="review_select">
		<a href="<?=Yii::app()->createUrl('/review/review/index')?>">Текст</a>
		<a href="<?=Yii::app()->createUrl('/review/review/index')?>?type=video">Видео</a>
	</div>
<?$this->widget(
    'webforma.widgets.WListView', [
        'dataProvider' => $dataProvider,
        'itemView' => $type == 'video' ? '_video' : '_review',
        'template' => "{summary}\n{items}\n{pager}",
        'summaryText' => '',
        'itemsCssClass' => 'review-items',
        'pagerCssClass' => 'catalog__pagination',
        'pager' => [
            'header' => '',
            'prevPageLabel' => '&laquo;',
            'nextPageLabel' => '&raquo;',
            'firstPageLabel' => false,
            'lastPageLabel' => false,
            'htmlOptions' => [
                'class' => 'pagination'
            ]
        ],
    ]
);
echo '</div>';