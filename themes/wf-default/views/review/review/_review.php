<?php
/**
 * @var Review $data
 */
?>
<div class="review-item" data-id="<?= $data->id; ?>">
	<div class="review-item__image">
		<img src="<?=$data->getImageUrl(300, 195, true)?>">
	</div>
	<div class="review-item__info">
		<div class="review-item__name">
			<?=$data->name?>
		</div>

		<div class="review-item__text">
			<?=$data->text?>
		</div>
    </div>
</div>
