<div class="subscribe-form-wrap">
	<div class="subscribe-form-left">
		<div class="title">Подпишитесь на рассылку</div>
		<div class="sub-title">Первыми узнавайте о спецпредложениях и акциях нашего магазина</div>
	</div>
	<?php $form = $this->beginWidget(
	'CActiveForm',
	[
		'action' => Yii::app()->createUrl('/subscribe/subscribe/index'),
		'enableAjaxValidation' => true,
		'enableClientValidation' => false,
		'clientOptions' => [
			'validateOnSubmit' => true,
			'validateOnChange' => true,
			'validateOnType' => false,
			'beforeValidate' => 'js:function(form, data, hasError){$(form).find("button[type=\'submit\']").prop("disabled", true); subscribeSendForm(form, data, hasError);}',
			'afterValidate' => 'js:function(form, data, hasError){$(form).find("button[type=\'submit\']").prop("disabled", false); return !hasError;}',
		],
	]
); ?>
	<?=$form->hiddenField($model, 'theme', ['value' => 'Подписка на рассылку'])?>

	<div class="subscribe-form-right">
		<div class="subscribe-form__success result-message bs-callout"></div>
		<div class="subscribe-form__inputs">
			<?=$form->textField($model, 'email', ['placeholder' => 'Ваше e-mail'])?>
			<div class="form-group__help">
				<?= $form->error($model, 'email'); ?>
			</div>
			<input type="submit" value="Подписаться"/>
		</div>
	</div>

	<?php $this->endWidget(); ?>
</div>
