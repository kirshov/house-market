<?php
$title = 'Подписка на новости';
$this->breadcrumbs = [$title];
$this->title = Helper::prepareSeoTitle($title);
$this->setTitle($title);
?>
<div class="page">
	<?if($status):?>
		<p class="alert-success alert">Ваш адрес подтвержден</p>
	<?else:?>
		<p class="bs-callout-danger alert">Адрес не найден</p>
	<?endif;?>
</div>