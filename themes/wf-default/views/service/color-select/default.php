<?
$module = Yii::app()->getModule('webforma');
?>
<div class="color-select">
	<div class="color-select__toggle-item"></div>

	<div class="color-select_inner">
		<div class="cs-title">Выбор цветовой схемы</div>

		<div class="cs-items">
			<?foreach ($module->colorThemeCodes as $_name => $_color):?>
				<div class="color-item">
					<div class="color-item__color<?=$_name == $module->colorTheme ? ' active' : ''?>" data-color="<?=$_name?>" title="<?=$module->colorThemeItems[$_name]?>"><span style="background-color: <?=$_color?>">&nbsp;</span></div>
				</div>
			<?endforeach;?>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('.color-select .color-item__color').on('click', function () {
		$(this).closest('.cs-items').find('.color-item__color').removeClass('active');
		$(this).addClass('active');

		var bodyClass = $('body').attr('class');
		bodyClass = bodyClass.replace(/\s*theme-[a-z]*/gi, '');
		bodyClass += ' theme-' + $(this).data('color');
		$('body').attr('class', bodyClass);
	});
</script>
