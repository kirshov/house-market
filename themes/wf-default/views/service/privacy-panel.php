<div class="privacy-bottom">
	<div class="wrapper">
		<div class="privacy-bottom__inner">
			<?=Yii::app()->getModule('webforma')->getPrivacyTextPanel();?>
		</div>
		<div class="privacy-bottom__close btn" data-href="<?= Yii::app()->createUrl('/home/disablePrivacyWindow/')?>">Понятно</div>
	</div>
</div>