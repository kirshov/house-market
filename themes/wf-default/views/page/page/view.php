<?php
/**
 * @var $model Page
 * @var $this \webforma\components\controllers\FrontController
 * @var $dataProvider CActiveDataProvider
 */

$this->renderPartial('_page_header', ['model' => $model])
?>

<div class="page page-<?=$model->id?>">
	<?php
	if($dataProvider && $dataProvider->getTotalItemCount()){
		$this->widget('webforma.widgets.WListView', [
			'dataProvider' => $dataProvider,
			'itemView' => '_subpage',
			'template' => "{items}",
			'htmlOptions' => [
				'class' => 'subpage-items',
			],
			'summaryText' => '',
			'pager' => false,
		]);
	}
	?>
	<div class="page-content">
		<?= $model->getBody(); ?>

		<?if($model->slug == 'contacts'):?>
			<div class="contacts__buttons">
				<div class="contacts__buttons-item">
					<span class="button-callback modal-show" data-modal="callback">Заказать обратный звонок</span>
				</div>

				<div class="contacts__buttons-item">
					<span class="button-callback modal-show" data-modal="cooperation-popup">Заявка на&nbsp;сотрудничество</span>
				</div>
			</div>
		<?endif;?>
	</div>
</div>
