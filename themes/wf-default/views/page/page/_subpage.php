<?php
/* @var $data Page */
$url = Yii::app()->createUrl($data->getPath());
?>
<div class="subpage-item">
    <?//php if ($data->image): ?>
        <div class="subpage-img">
			<a href="<?=$url?>" class="subpage-img__link">
            	<?= CHtml::image($data->getImageUrl(180, 150, false), $data->title); ?>
			</a>
        </div>
    <?//php endif; ?>
    <div class="subpage-info">
        <div class="subpage-title"><?= CHtml::link(CHtml::encode($data->title), $url, ['class' => 'cart-item__link']); ?></div>
        <!--<div class="page-description"><?/*= $data->short_body; */?></div>-->
    </div>
</div>
