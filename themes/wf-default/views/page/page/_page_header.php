<?php
/* @var $model Page */
/* @var $this \webforma\components\controllers\FrontController */

if ($model->layout) {
	$this->layout = "//layouts/{$model->layout}";
}
$module = Yii::app()->getModule('webforma');
$this->title = $model->getMetaTitle();
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->description;
$this->keywords = $model->keywords;
$this->setTitle($model->getTitle());
$this->setTemplateVar('bodyClass', 'page');