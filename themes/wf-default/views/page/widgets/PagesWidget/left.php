<?
/**
 * @var PagesWidget $this
 * @var Page[] $pages
 */
if(!$pages){
	return true;
}

$currentUrl = trim(Yii::app()->getRequest()->requestUri, '/');
?>
<ul class="services-left-list">
	<?foreach ($pages as $page):?>
		<?$link = Yii::app()->createUrl($page->getPath());?>
		<li class="service-item<?=(trim($link, '/') == $currentUrl) ? ' active' : ''?>">
			<a href="<?=$link?>" class="service-item__title-link">
				<?=$page->getTitle()?>
			</a>
		</li>
	<?endforeach;?>
</ul>
