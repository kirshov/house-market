<?
/**
 * @var PagesWidget $this
 * @var Page[] $pages
 */
if(!$pages){
	return true;
}
$pageBody = Page::getBodyById(1);
?>
<div>
	<div class="h3">Категории услуг</div>
	<?if($pageBody):?>
		<div class="services-description">
			<?=$pageBody?>
		</div>
	<?endif;?>

	<div class="services-list">
		<?foreach ($pages as $page):?>
			<?$link = Yii::app()->createUrl($page->getPath());?>
			<div class="service-item">
				<?if($page->image):?>
					<div class="service-item__image">
						<a href="<?=$link?>" class="service-item__image-link">
							<img src="<?=$page->getImageUrl(180, 150)?>" alt="<?=CHtml::encode($page->getTitle())?>">
						</a>
					</div>
				<?endif;?>
				<div class="service-item__info">
					<div class="service-item__title">
						<a href="<?=$link?>" class="service-item__title-link">
							<?=$page->getTitle()?>
						</a>
					</div>
					<div class="service-item__text">
						<?=$page->short_body;?>
					</div>
				</div>
			</div>
		<?endforeach;?>
	</div>
</div>
