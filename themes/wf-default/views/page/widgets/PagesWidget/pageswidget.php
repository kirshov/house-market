<?
/**
 * @var Page[] $pages
 */

if($pages){
	echo CHtml::openTag('div', ['class' => 'main-block main-pages']);
	foreach ($pages as $page){
		echo CHtml::openTag('div', ['class' => 'main-pages__item']);
		echo CHtml::tag('div', ['class' => 'main-pages__item-image'], CHtml::image($page->getImageUrl(100, 100)));
		echo CHtml::tag('div', ['class' => 'main-pages__item-title'], CHtml::link($page->getTitle(), $page->getPath()));
		echo CHtml::closeTag('div');
	}
	echo CHtml::closeTag('div');
}

