<?
/**
 * @var PagesWidget $this
 * @var Page[] $pages
 */
if(!$pages){
	return true;
}
?>
<ul class="top-page-list">
	<?foreach ($pages as $page):?>
		<?
			$link = Yii::app()->createUrl($page->getPath());
			$activeClass = Helper::isItemActive($link) ? ' active' : '';
		?>
		<li class="page-item<?=$activeClass?>">
			<a href="<?=$link?>" class="page-item__title-link">
				<?=$page->getTitle()?>
			</a>
		</li>
	<?endforeach;?>
</ul>