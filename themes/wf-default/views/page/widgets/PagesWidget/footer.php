<?
/**
 * @var PagesWidget $this
 * @var Page[] $pages
 */
if(!$pages){
	return true;
}
?>
<div>
	<div class="h3">Наши услуги</div>
	<ul class="services-footer-list<?=sizeof($pages) >= 6 ? ' big' : ''?>">
		<?foreach ($pages as $page):?>
			<?$link = Yii::app()->createUrl($page->getPath());?>
			<li class="service-item">
				<a href="<?=$link?>" class="service-item__title-link">
					<?=$page->getTitle()?>
				</a>
			</li>
		<?endforeach;?>
	</ul>
</div>
