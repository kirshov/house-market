<?php
/**
 * @var Discount[] $discounts
 */
?>
<?if($discounts):?>
	<table class="table center w-50">
		<thead>
			<th>Сумма заказов</th>
			<th>Скидка</th>
		</thead>
		<tbody>
			<?foreach ($discounts as $discount):?>
				<tr>
					<td>от <?=Helper::getFormatPrice($discount->total_cost_orders)?> <?=$currency?></td>
					<td><?=$discount->value?>%</td>
				</tr>
			<?endforeach;?>
		</tbody>
	</table>
<?endif;?>