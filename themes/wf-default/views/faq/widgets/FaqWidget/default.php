<?
/**
 * @var Faq[] $items
 */
?>
<?if($items):?>
	<div class="faq-widget">
		<?foreach ($items as $item):?>
			<div class="faq-item">
				<div class="faq-title"><?=$item->name;?></div>
				<div class="faq-body"><?=$item->content;?></div>
			</div>
		<?endforeach;?>
	</div>
<?endif;?>