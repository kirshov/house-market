<?php
/**
 * @var Callback $model
 * @var TbActiveForm $form
 */
?>

<div class="main-callback">
	<?php $form = $this->beginWidget('CActiveForm', [
		'action' => ['/callback/callback/send'],
		'enableClientValidation' => true,
		'clientOptions' => [
			'validateOnSubmit' => true,
			'validateOnChange' => false,
			'validateOnType' => false,
			'beforeValidate' => 'js:function(form, data, hasError){$(form).find("button[type=\'submit\']").prop("disabled", true); callbackSendForm(form, data, hasError);}',
			'afterValidate' => 'js:function(form, data, hasError){$(form).find("button[type=\'submit\']").prop("disabled", false); return !hasError;}',
		],
		'htmlOptions' => [
			'hideErrorMessage' => false,
		],
	]); ?>

	<div class="callback-form__left">
		<div class="modal__input-item modal__input-item-text">
			<div class="form-group">
				<?= $form->labelEx($model, 'phone', ['class' => 'form-group__label']); ?>
				<div class="form-group__input">
					<?= $form->textField($model, 'phone', ['class' => 'input input-phone']); ?>
				</div>
				<div class="form-group__help">
					<?= $form->error($model, 'phone'); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="callback-form__right">
		<div class="modal__input-item modal__input-item-submit">
			<input type="submit" value="Перезвонить мне" class="btn" />
		</div>
	</div>
	<?php $this->endWidget(); ?>
</div>
