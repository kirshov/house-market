<?php
/**
 * @var Callback $model
 * @var TbActiveForm $form
 */
?>

<div class="modal-window callback">
	<div class="modal-content">
		<div class="modal-header">
			<div class="modal-close">&times;</div>
			<div class="modal-title" data-default="Заказать звонок">Заказать звонок</div>
		</div>

		<?php $form = $this->beginWidget('CActiveForm', [
			'action' => ['/callback/callback/send'],
			'enableClientValidation' => true,
			'clientOptions' => [
				'validateOnSubmit' => true,
				'validateOnChange' => false,
				'validateOnType' => false,
				'beforeValidate' => 'js:function(form, data, hasError){$(form).find("button[type=\'submit\']").prop("disabled", true); callbackSendForm(form, data, hasError);}',
				'afterValidate' => 'js:function(form, data, hasError){$(form).find("button[type=\'submit\']").prop("disabled", false); return !hasError;}',
			],
			'htmlOptions' => [
				'hideErrorMessage' => false,
			],
		]); ?>

		<div class="modal-body">
			<div class="bs-callout errorSummary result-message hidden"></div>

			<div class="modal-inner">
				<div class="feedback-form__middle">

					<div class="modal__input-item modal__input-item-text">
						<div class="form-group">
							<div class="form-group__input">
								<?= $form->textField($model, 'name', ['class' => 'input', 'placeholder' => $model->getAttributeLabel('name')]); ?>
							</div>
							<div class="form-group__help">
								<?= $form->error($model, 'name'); ?>
							</div>
						</div>
					</div>

					<div class="modal__input-item modal__input-item-text">
						<div class="form-group">
							<div class="form-group__input">
								<?= $form->textField($model, 'phone', ['class' => 'input input-phone', 'placeholder' => $model->getAttributeLabel('phone')]); ?>
							</div>
							<div class="form-group__help">
								<?= $form->error($model, 'phone'); ?>
							</div>
						</div>
					</div>

					<?=$form->hiddenField($model, 'comment', ['class' => 'theme']);?>

					<div class="feedback-form__bottom">
						<div class="feedback-bottoms">
							<div class="modal__input-item modal__input-item-submit modal__input-item-submit_callback">
								<input type="submit" value="Заказать звонок" class="btn" />
							</div>

							<div class="modal__input-item modal__input-item-submit modal__input-item-submit_whatsapp">
								<?$this->widget('content', ['code' => 'to-whatsapp'])?>
							</div>
						</div>

						<div class="modal__item-text"><?=Yii::app()->getModule('webforma')->getPrivacyTextForm();?></div>
					</div>
				</div>
			</div>
		</div>
		<?php $this->endWidget(); ?>
	</div>
</div>
