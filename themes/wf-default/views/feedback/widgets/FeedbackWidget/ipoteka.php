<?php
/**
 * @var FeedBackForm $model
 * @var TbActiveForm $form
 */
?>
<div class="feedback-form">
	<?php $form = $this->beginWidget('CActiveForm', [
			'action' => Yii::app()->createUrl('/feedback/contact/index'),
			'enableClientValidation' => true,
			'clientOptions' => [
				'validateOnSubmit' => true,
				'validateOnChange' => false,
				'validateOnType' => false,
				'beforeValidate' => 'js:function(form){$(form).find("button[type=\'submit\']").prop("disabled", true); feedbackSendForm(form);}',
				'afterValidate' => 'js:function(form){$(form).find("button[type=\'submit\']").prop("disabled", false); return true;}',
			],
			'htmlOptions' => [
				'hideErrorMessage' => false,
			],
		]);

		echo CHtml::hiddenField('formClass', 'FeedBackIpotekaForm');
		echo CHtml::activeHiddenField($model, 'theme', ['value' => 'Расчет ипотеки']);
	?>

		<div class="feedback-form__wrapper ipoteka-wrapper">
			<div class="feedback-form__title">Предварительный расчет ипотеки</div>

			<div class="bs-callout result-message hidden"></div>

			<div class="feedback-form__inner">
				<div class="feedback-form__inputs">
					<div class="form__input-item order-form__item">
						<div class="form-group">
							<div class="form-group__input">
								<?= $form->textField($model, 'price', ['class' => 'input input-price input-money', 'placeholder' => $model->getAttributeLabel('price')]); ?>
							</div>
							<div class="form-group__help">
								<?= $form->error($model, 'price'); ?>
							</div>
						</div>
					</div>

                    <div class="form__input-item order-form__item pay_group-wrap">
						<div class="form-group">
							<div class="form-group__input pay_group">
								<?= $form->textField($model, 'pay', ['readonly' =>true, 'class' => 'input input-pay input-money', 'placeholder' => $model->getAttributeLabel('pay')]); ?>
								<span class="pay_percent">0%</span>
							</div>
							<div class="pay_slider">
							</div>
							<div class="form-group__help">
								<?= $form->error($model, 'pay'); ?>
							</div>
						</div>
					</div>

					<div class="form__input-item order-form__item">
						<div class="form-group">
                            <div class="form-group__input form-group__input_checkbox">
								<?= $form->checkBox($model, 'mother', ['class' => 'input styler', 'value' => 'Да']); ?>
								<?= $form->label($model,'mother'); ?>
							</div>
						</div>
					</div>

					<div class="form__input-item order-form__item">
						<div class="form-group">
							<div class="form-group__input">
								<?= $form->textField($model, 'deadline', ['class' => 'input', 'placeholder' => $model->getAttributeLabel('deadline')]); ?>
							</div>
							<div class="form-group__help">
								<?= $form->error($model, 'deadline'); ?>
							</div>
						</div>
					</div>

					<div class="form__input-item order-form__item">
						<div class="form-group">
							<div class="form-group__input">
								<?= $form->textField($model, 'name', ['class' => 'input', 'placeholder' => $model->getAttributeLabel('name')]); ?>
							</div>
							<div class="form-group__help">
								<?= $form->error($model, 'name'); ?>
							</div>
						</div>
					</div>

					<div class="form__input-item order-form__item">
						<div class="form-group">
							<div class="form-group__input">
								<?= $form->textField($model, 'phone', ['class' => 'input input-phone', 'placeholder' => $model->getAttributeLabel('phone')]); ?>
							</div>
							<div class="form-group__help">
								<?= $form->error($model, 'phone'); ?>
							</div>
						</div>
					</div>

					<?php if (Yii::app()->getModule('feedback')->showCaptcha && Yii::app()->getUser()->getIsGuest() && CCaptcha::checkRequirements()): ?>
						<div class="form__input-item order-form__item order-form__captcha">
							<div class="form-group">
								<?= $form->labelEx($model, 'verifyCode', ['required' => true]); ?>
								<div class="form-group__input">
									<?= $form->textField($model, 'verifyCode', ['class' => 'input input-captcha']); ?>
								</div>
							</div>

							<div class="form-image">
								<?php $this->widget('CCaptcha', [
									'showRefreshButton' => false,
									'clickableImage' => true,
									'imageOptions' => [
										'height' => '38px',
										'title' => 'Кликните, чтобы получить другой код',
										'class' => 'image-captcha',
									],
									'captchaAction' => '/feedback/contact/captcha/',
								]);?>
							</div>
						</div>
					<?php endif; ?>
				</div>

				<div class="feedback-form__bottom">
					<div class="feedback-form__order-button">
						<button type="submit" class="btn">Рассчитать ипотеку</button>
					</div>

					<div class="feedback__privacy-policy"><?=Yii::app()->getModule('webforma')->getPrivacyTextForm();?></div>
				</div>
			</div>
		</div>
	<?php $this->endWidget(); ?>
</div>
