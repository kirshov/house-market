<?php
/**
 * @var FeedBackForm $model
 * @var TbActiveForm $form
 */
?>
<div class="feedback-form">
	<?php $form = $this->beginWidget('CActiveForm', [
			'action' => Yii::app()->createUrl('/feedback/contact/index'),
			'enableClientValidation' => true,
			'clientOptions' => [
				'validateOnSubmit' => true,
				'validateOnChange' => false,
				'validateOnType' => false,
				'beforeValidate' => 'js:function(form){$(form).find("button[type=\'submit\']").prop("disabled", true); feedbackSendForm(form);}',
				'afterValidate' => 'js:function(form){$(form).find("button[type=\'submit\']").prop("disabled", false); return true;}',
			],
			'htmlOptions' => [
				'hideErrorMessage' => false,
			],
		]); ?>

		<div class="feedback-form__wrapper">
			<div class="feedback-form__title">Оставьте заявку</div>

			<div class="bs-callout result-message hidden"></div>

			<div class="feedback-form__inner">
				<div class="feedback-form__inputs">
					<div class="form__input-item order-form__item">
						<div class="form-group">
							<?= $form->labelEx($model, 'name', ['class' => 'form-group__label']); ?>
							<div class="form-group__input">
								<?= $form->textField($model, 'name', ['class' => 'input']); ?>
							</div>
							<div class="form-group__help hidden">
								<?= $form->error($model, 'name'); ?>
							</div>
						</div>
					</div>

					<div class="form__input-item order-form__item">
						<div class="form-group">
							<?= $form->labelEx($model, 'phone'); ?>
							<div class="form-group__input">
								<?= $form->textField($model, 'phone', ['class' => 'input input-phone']); ?>
							</div>
							<div class="form-group__help hidden">
								<?= $form->error($model, 'phone'); ?>
							</div>
						</div>
					</div>

					<div class="form__input-item order-form__item">
						<div class="form-group">
							<?= $form->labelEx($model, 'email'); ?>
							<div class="form-group__input">
								<?= $form->textField($model, 'email', ['class' => 'input input-email']); ?>
							</div>
							<div class="form-group__help hidden">
								<?= $form->error($model, 'email'); ?>
							</div>
						</div>
					</div>

					<div class="form__input-item order-form__item order-form__comment">
						<div class="form-group">
							<?= $form->labelEx( $model,'text'); ?>
							<div class="form-group__input">
								<?= $form->textArea($model, 'text', ['class' => 'input']); ?>
							</div>
						</div>
					</div>

					<?php if (Yii::app()->getModule('feedback')->showCaptcha && Yii::app()->getUser()->getIsGuest() && CCaptcha::checkRequirements()): ?>
						<div class="form__input-item order-form__item order-form__captcha">
							<div class="form-group">
								<?= $form->labelEx($model, 'verifyCode', ['required' => true]); ?>
								<div class="form-group__input">
									<?= $form->textField($model, 'verifyCode', ['class' => 'input input-captcha']); ?>
								</div>
							</div>

							<div class="form-image">
								<?php $this->widget('CCaptcha', [
									'showRefreshButton' => false,
									'clickableImage' => true,
									'imageOptions' => [
										'height' => '38px',
										'title' => 'Кликните, чтобы получить другой код',
										'class' => 'image-captcha',
									],
									'captchaAction' => '/feedback/contact/captcha/',
								]);?>
							</div>
						</div>
					<?php endif; ?>
				</div>

				<div class="feedback-form__bottom">
					<div class="feedback-form__order-button">
						<button type="submit" class="btn">Отправить</button>
					</div>

					<div class="feedback__privacy-policy"><?=Yii::app()->getModule('webforma')->getPrivacyTextForm();?></div>
				</div>
			</div>
		</div>
	<?php $this->endWidget(); ?>
</div>
