<?php
/**
 * @var FeedBackForm $model
 * @var TbActiveForm $form
 */
?>

<div class="modal-window show-popup">
	<div class="modal-content">
		<div class="modal-header">
			<div class="modal-close">&times;</div>
			<div class="modal-title" data-default="Запись на просмотр">Запись на просмотр</div>
		</div>

		<?php $form = $this->beginWidget('CActiveForm', [
			'action' => ['/feedback/contact/index'],
			'enableClientValidation' => true,
			'clientOptions' => [
				'validateOnSubmit' => true,
				'validateOnChange' => false,
				'validateOnType' => false,
				'beforeValidate' => 'js:function(form){$(form).find("button[type=\'submit\']").prop("disabled", true); feedbackSendForm(form);}',
				'afterValidate' => 'js:function(form){$(form).find("button[type=\'submit\']").prop("disabled", false); return true;}',
			],
			'htmlOptions' => [
				'hideErrorMessage' => false,
			],
		]);

		echo CHtml::activeHiddenField($model, 'theme', ['value' => 'Запись на просмотр']);
		echo CHtml::activeHiddenField($model, 'text', ['value' => '', 'class' => 'text-input']);
		?>

		<div class="modal-body">
			<div class="bs-callout errorSummary result-message hidden"></div>

			<div class="modal-inner">
				<div class="feedback-form__middle">
					<div class="modal__input-item modal__input-item-text">
						<div class="form-group">
							<div class="form-group__input">
								<?= $form->textField($model, 'name', ['class' => 'input', 'placeholder' => $model->getAttributeLabel('name')]); ?>
							</div>
							<div class="form-group__help">
								<?= $form->error($model, 'name'); ?>
							</div>
						</div>
					</div>

					<div class="modal__input-item modal__input-item-text">
						<div class="form-group">
							<div class="form-group__input">
								<?= $form->textField($model, 'phone', ['class' => 'input input-phone', 'placeholder' => $model->getAttributeLabel('phone')]); ?>
							</div>
							<div class="form-group__help">
								<?= $form->error($model, 'phone'); ?>
							</div>
						</div>
					</div>

					<?php if (Yii::app()->getModule('feedback')->showCaptcha && Yii::app()->getUser()->getIsGuest() && CCaptcha::checkRequirements()): ?>
						<div class="form__input-item order-form__item order-form__captcha">
							<div class="form-group">
								<?= $form->labelEx($model, 'verifyCode', ['required' => true]); ?>
								<div class="form-group__input">
									<?= $form->textField($model, 'verifyCode', ['class' => 'input input-captcha']); ?>
								</div>
							</div>

							<div class="form-image">
								<?php $this->widget('CCaptcha', [
									'showRefreshButton' => false,
									'clickableImage' => true,
									'imageOptions' => [
										'height' => '38px',
										'title' => 'Кликните, чтобы получить другой код',
										'class' => 'image-captcha',
									],
									'captchaAction' => '/feedback/contact/captcha/',
								]);?>
							</div>
						</div>
					<?php endif; ?>
				</div>

				<div class="feedback-form__bottom">
					<div class="modal__input-item modal__input-item-submit">
						<input type="submit" value="Записаться на просмотр" class="btn" />
					</div>

					<div class="modal__item-text"><?=Yii::app()->getModule('webforma')->getPrivacyTextForm();?></div>
				</div>
			</div>
		</div>
		<?php $this->endWidget(); ?>
	</div>
</div>

