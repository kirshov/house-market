<?php
/**
 * @var FeedBackForm $model
 * @var TbActiveForm $form
 */
?>
<div class="feedback-form">
	<?php
		$form = $this->beginWidget(
			'CActiveForm',
			[
				'action' => ['/feedback/contact/index'],
				'id' => 'feedback-form',
				'enableAjaxValidation' => false,
				'enableClientValidation' => true,
				'clientOptions' => [
					'validateOnSubmit' => true,
					'validateOnChange' => false,
					'validateOnType' => false,
					'beforeValidate' => 'js:function(form){$(form).find("button[type=\'submit\']").prop("disabled", true); feedbackSendForm(form);}',
					'afterValidate' => 'js:function(form){$(form).find("button[type=\'submit\']").prop("disabled", false); return true;}',
				],
				'htmlOptions' => [
					'hideErrorMessage' => false,
					'enctype' => 'multipart/form-data',
				],
			]
		); ?>

		<div class="feedback-form__wrapper">
			<div class="feedback-form__title">Оставьте заявку на расчет проекта</div>

			<div class="bs-callout result-message hidden"></div>

			<div class="feedback-form__inner">
				<div class="feedback-form__inputs">
					<div class="form__input-item order-form__item">
						<div class="form-group">
							<div class="form-group__input">
								<?= $form->textField($model, 'name', ['class' => 'input', 'placeholder' => 'Имя*',]); ?>
							</div>
							<div class="form-group__help hidden">
								<?= $form->error($model, 'name'); ?>
							</div>
						</div>
					</div>

					<div class="form__input-item order-form__item">
						<div class="form-group">
							<div class="form-group__input">
								<?= $form->textField($model, 'phone', ['class' => 'input input-phone', 'placeholder' => 'Телефон*',]); ?>
							</div>
							<div class="form-group__help hidden">
								<?= $form->error($model, 'phone'); ?>
							</div>
						</div>
					</div>

					<div class="form__input-item order-form__item order-form__comment">
						<div class="form-group">
							<div class="form-group__input">
								<?= $form->textArea($model, 'text', ['class' => 'input', 'placeholder' => 'Сообщение']); ?>
							</div>
						</div>
					</div>

					<div class="form__input-item order-form__item">
						<div class="file-item"></div>
						<?= $form->fileField($model, 'attachment', ['class' => 'input input-file', 'onChange' => 'attachFile(this);']); ?>
					</div>
				</div>

				<div class="feedback-form__bottom">
					<div class="feedback-form__order-button">
						<button type="submit" class="btn">Отправить заявку</button>
					</div>

					<div class="feedback__privacy-policy"><?=Yii::app()->getModule('webforma')->getPrivacyTextForm();?></div>
				</div>
			</div>
		</div>
	<?php $this->endWidget(); ?>
</div>
