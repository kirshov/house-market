<?
/**
 * @var VideoItem[] $videos
 */
?>

<?php if($videos):?>
	<div class="h3">Видеоматериалы <a class="view_all" rel="nofollow" href="/video/">Смотреть все</a></div>
	<div class="main-video-wrapper">
		<div class="main-video-inner">
			<?foreach ($videos as $video):?>
			<div class="main-video-item">
				<div class="video-item">
					<?=$video->code?>
				</div>
			</div>
			<?endforeach;?>
		</div>
	</div>
<?endif;?>
