<?php
/**
 * @var $this GalleryController
 * @var $model Gallery
 */

$this->title = $model->name;
$this->breadcrumbs = [
    Yii::t('VideoModule.video', 'Video') => ['/video/video/index'],
    $model->name
];

$this->pageTitle = CHtml::encode($model->name);
?>
<div class="page page-gallery page-video-gallery">
	<?php
	$this->widget(
		'zii.widgets.CListView',
		[
			'dataProvider' => $dataProvider,
			'itemView' => '_item_video',
			'template' => "{pager}\n{items}\n{pager}",
			'summaryText' => '',
			'enableHistory' => true,
			'cssFile' => false,
			'ajaxUpdate' => false,
			'itemsCssClass' => 'gallery__items',
			'htmlOptions' => [
				'class' => 'catalog'
			],
			'pagerCssClass' => 'catalog__pagination',
			'pager' => [
				'header' => '',
				'prevPageLabel' => '<i class="fa fa-long-arrow-left"></i>',
				'nextPageLabel' => '<i class="fa fa-long-arrow-right"></i>',
				'firstPageLabel' => false,
				'lastPageLabel' => false,
				'htmlOptions' => [
					'class' => 'pagination'
				]
			]
		]
	); ?>
</div>