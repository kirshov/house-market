<?php
/**
 * Отображение для gallery/list
 *
 * @var $this GalleryController
 * @var $dataProvider CActiveDataProvider
 **/

$this->title = Yii::t('VideoModule.video', 'Video');
$this->breadcrumbs = [Yii::t('VideoModule.video', 'Video')];
$this->pageTitle = Yii::t('VideoModule.video', 'Video');
?>
<div class="page page-gallery page-video-gallery page-video-gallery-category">
	<?php
	$this->widget(
		'zii.widgets.CListView',
		[
			'dataProvider' => $dataProvider,
			'itemView' => '_item',
			'template' => "{pager}\n{items}\n{pager}",
			'summaryText' => '',
			'enableHistory' => true,
			'cssFile' => false,
			'ajaxUpdate' => false,
			'itemsCssClass' => 'gallery__items',
			'htmlOptions' => [
				'class' => 'catalog'
			],
			'pagerCssClass' => 'catalog__pagination',
			'pager' => [
				'header' => '',
				'prevPageLabel' => '<i class="fa fa-long-arrow-left"></i>',
				'nextPageLabel' => '<i class="fa fa-long-arrow-right"></i>',
				'firstPageLabel' => false,
				'lastPageLabel' => false,
				'htmlOptions' => [
					'class' => 'pagination'
				]
			]
		]
	); ?>
</div>
