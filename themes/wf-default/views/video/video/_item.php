<?php
$url = Yii::app()->createUrl('/video/video/view', ['id' => $data->id]);
?>
<div class="gallery-item">
	<div class="gallery-item__image">
		<a href="<?= $url ?>">
			<?= CHtml::image($data->getImageUrl(288, 174, false), $data->name) ?>
		</a>
	</div>
	<div class="gallery-item__name">
		<?= CHtml::link(CHtml::encode($data->name), $url); ?>
	</div>
</div>
