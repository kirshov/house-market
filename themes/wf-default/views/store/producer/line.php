<?php
/**
 * @var Producer $brand
 * @var ProducerLine $line
 * @var CActiveDataProvider $products
 */

$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
$cs = Yii::app()->getClientScript();
$cs->registerCoreScript('jquery.ui');
$cs->registerCssFile($cs->getCoreScriptUrl(). '/jui/css/base/jquery-ui.css');

$this->title = $line->getMetaTitle();
$this->description = $line->getMetaDescription();
$this->keywords = $line->getMetaKeywords();

if($line->image){
	$this->setMetaImage($line->getImageUrl(100, 100, false));
}

$this->breadcrumbs = [
    Yii::t('StoreModule.store', 'Producers list') => ['/store/producer/index'],
    $brand->getName() => ['/store/producer/view', 'slug' => $brand->slug],
	CHtml::encode($line->getName()),
];
$this->setTemplateVar([
	'bodyClass' => 'catalog-page',
	'retargeting-code' => $brand->retargeting_code ?: null,
]);
$this->setTitle($line->getH1());

$isGetQuery = Yii::app()->getModule('store')->isGetFilterParams();
?>
<?if($line->description && !$isGetQuery):?>
	<div class="page page-brands__line">
		<!--noindex-->
		<div class="brand__detail">
			<div class="brand__description">
				<?= $line->description ?>
			</div>
		</div>
		<!--/noindex-->
	</div>
<?endif;?>
<div class="catalog">
    <?
		$filters = $this->widget('attributesFilter', ['lineIds' => [$line->id]], true);

		$this->renderPartial('/category/_filter', [
			'category' => null,
			'showProducers' => false,
			'filters' => $filters,
		]);
		$this->renderPartial('/category/_product_list', ['dataProvider' => $products, 'showActionBar' => true]);
	?>
</div>