<?php
/**
 * @var \webforma\components\controllers\FrontController $this
 * @var Producer $brand
 * @var CActiveDataProvider $products
 */

$cs = Yii::app()->getClientScript();
$cs->registerCoreScript('jquery.ui');
$cs->registerCssFile($cs->getCoreScriptUrl(). '/jui/css/base/jquery-ui.css');

$this->title = $brand->getMetaTitle();
$this->description = $brand->getMetaDescription();
$this->keywords = $brand->getMetaKeywords();
$this->canonical = $brand->getMetaCanonical();

if($brand->image){
	$this->setMetaImage($brand->getImageUrl(100, 100, false));
}

$this->breadcrumbs = [
    Yii::t('StoreModule.store', 'Producers list') => ['/store/producer/index'],
	CHtml::encode($brand->getName()),
];

$isGetQuery = Yii::app()->getModule('store')->isGetFilterParams();

$filters = $this->widget('attributesFilter', ['producerIds' => [$brand->id]], true);

$this->setTemplateVar([
    'bodyClass' => 'catalog-page',
    'retargeting-code' => $brand->retargeting_code ?: null,
	'filter' => $this->renderPartial('/category/_filter', ['category' => null, 'showProducers' => false, 'filters' => $filters], true),
]);

$this->setTitle($brand->getH1());
?>
<div class="catalog">
	<?if(!$isGetQuery):?>
		<div class="page page-brands">
			<div class="brand__detail">
				<div class="brand__image">
					<img src="<?= StoreImage::producer($brand, 170, 100, false);?>" alt="<?= CHtml::encode($brand->name); ?>">
				</div>
				<div class="brand__description">
					<?= $brand->description ?>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	<?endif;?>
	<?if($brand->line) {
		echo $this->renderPartial('_lines', ['brand' => $brand, 'isGetQuery' => $isGetQuery]);
	}?>

	<?$this->renderPartial('/category/_product_list', ['dataProvider' => $products]);?>

	<?if(!$isGetQuery && $brand->short_description):?>
		<div class="catalog-bottom-description">
			<?=$brand->short_description?>
		</div>
	<?endif;?>
</div>