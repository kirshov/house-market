<?php
/**
 * @var ProducerCountry $country
 * @var CActiveDataProvider $products
 */

$mainAssets = Yii::app()->getTheme()->getAssetsUrl();

$cs = Yii::app()->getClientScript();
$cs->registerCoreScript('jquery.ui');
$cs->registerCssFile($cs->getCoreScriptUrl(). '/jui/css/base/jquery-ui.css');

$this->title = $country->getMetaTitle();
$this->description = $country->getMetaDescription();
$this->keywords = $country->getMetaKeywords();
$this->canonical = $country->getMetaCanonical();

$this->breadcrumbs = [
    //Yii::app()->getModule('store')->metaProducerH1 ?: Yii::t('StoreModule.store', 'By country') => ['/store/producer/countryIndex'],
	CHtml::encode($country->getH1()),
];

$isGetQuery = Yii::app()->getModule('store')->isGetFilterParams();

$this->setTemplateVar([
    'bodyClass' => 'catalog-page',
    'descriptionsAfterPos' => !$isGetQuery ? $country->description_bottom : null,
]);
$this->setTitle($country->getH1());

?>
<?if($country->description && !$isGetQuery):?>
    <div class="page page-country">
        <div class="country__detail">
            <div class="country__description">
                <?= $country->description ?>
            </div>
        </div>
    </div>
<?endif;?>

<?if($country->producersActive):?>
	<div class="brand__lines">
		<div class="h3">
			Бренды
			<?if($isGetQuery):?>
				<a class="to-brand-lines" data-title-expand="Развернуть" data-title-expanded="Свернуть">Развернуть</a>
			<?else:?>
				<a class="to-brand-lines expanded" data-title-expand="Развернуть" data-title-expanded="Свернуть">Свернуть</a>
			<?endif;?>
		</div>

		<div class="brand__line-items"<?=($isGetQuery)?' style="display:none"':''?>>
			<?foreach ($country->producersActive as $producer):?>
				<?$link = Yii::app()->createUrl('/store/producer/view', ['slug' => $producer->slug]);?>
				<div class="brand-line__item">

					<div class="brand-line__thumbnail">
						<a href="<?=$link?>">
							<img src="<?=$producer->getImageUrl(140, 140, false)?>" class="brand-line-item__img">
						</a>
					</div>
					<div class="brand-line__title">
						<a href="<?=$link?>" class="brand-line-item__link"><?=$producer->getName()?></a>
					</div>
				</div>
			<?endforeach;?>
		</div>
	</div>
<?endif;?>

<div class="catalog">
	<?
	$brandsIds = $country->getProducersAsArray();
	$filters = $this->widget('attributesFilter', ['producerIds' => $brandsIds], true);

	$this->renderPartial('/category/_filter', [
		'category' => null,
		'showProducers' => true,
		'brandsIds' => $brandsIds,
		'filters' => $filters,
	]);

	$this->renderPartial('/category/_product_list', ['dataProvider' => $products, 'showActionBar' => true]);
	?>
</div>