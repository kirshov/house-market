<div class="brand__lines">
	<div class="h3">
		Линии <?=$brand->getName()?>
		<?if($isGetQuery):?>
			<a class="to-brand-lines" data-title-expand="Развернуть" data-title-expanded="Свернуть">Развернуть</a>
		<?else:?>
			<a class="to-brand-lines expanded" data-title-expand="Развернуть" data-title-expanded="Свернуть">Свернуть</a>
		<?endif;?>
	</div>

	<div class="brand__line-items"<?=($isGetQuery)?' style="display:none"':''?>>
		<?foreach ($brand->line as $line):?>
			<?$link = Yii::app()->createUrl('/store/producer/line/', ['slug' => $brand->slug, 'line' => $line->slug]);?>
			<div class="brand-line__item">

				<div class="brand-line__thumbnail">
					<a href="<?=$link?>">
						<img src="<?=$line->getImageUrl(140, 140, false)?>" class="brand-line-item__img">
					</a>
				</div>
				<div class="brand-line__title">
					<a href="<?=$link?>" class="brand-line-item__link"><?=$line->getName()?></a>
				</div>
			</div>
		<?endforeach;?>
    </div>
</div>