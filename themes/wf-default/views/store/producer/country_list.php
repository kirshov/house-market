<?php
/**
 * @var ProducerCountry[] $countries
 */

$module = Yii::app()->getModule('store');
$webforma = Yii::app()->getModule('webforma');

$title = $module->metaCountryH1 ?: Yii::t('StoreModule.store', 'By country');
$this->setTitle($title);
$this->breadcrumbs = [
	$title
];

$this->title = $module->metaCountryTitle ?: Helper::prepareSeoTitle($title);
$this->description = $module->metaCountryDescription ?: '';
$this->keywords = $module->metaCountryKeyWords ?: '';

$currentUrl = Yii::app()->getRequest()->getUrl();

$this->setTemplateVar([
	'descriptionsAfterPos' => $this->widget('content', ['code' => 'country-text'], true),
]);
?>
<div class="page page-countries">
    <ul class="country-items column">
		<?foreach ($countries as $country):?>
            <li class="country-item">
                <a href="<?=Yii::app()->createUrl('/store/producer/country', ['slug' => $country->slug])?>">
                    <?=$country->getName()?>
                </a>
            </li>
		<?endforeach;?>
	</ul>
</div>