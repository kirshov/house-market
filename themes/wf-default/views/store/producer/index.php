<?php
/**
 * @var \webforma\components\controllers\FrontController $this
 * @var Producer[] $brands
 * @var StoreModule $module
 */

$module = Yii::app()->getModule('store');
$webforma = Yii::app()->getModule('webforma');

$title = $module->metaProducerH1 ?: Yii::t('StoreModule.store', 'Producers list');
$this->setTitle($title);
$this->breadcrumbs = [
	$title
];
$this->setTemplateVar('bodyClass', 'catalog-page');
$this->title = $module->metaProducerTitle ?: Helper::prepareSeoTitle($title);
$this->description = $module->metaProducerDescription ?: $webforma->siteDescription;
$this->keywords = $module->metaProducerKeyWords ?: $webforma->siteKeyWords;

$currentUrl = Yii::app()->getRequest()->getUrl();
?>
<div class="page page-brands">

	<div class="brands__letters">
		<?foreach ($brandsLetters as $letter => $brands):?>
			<div class="brands__letter-item"><a href="<?=$currentUrl.'#'.$letter;?>" class="anchor-scroll"><?=$letter?></a></div>
		<?endforeach;?>
	</div>

	<div class="brands-items">
		<?foreach ($brandsLetters as $letter => $brands):?>
			<a name="<?=$letter?>"></a>
			<div class="brands__letter-big"><?=$letter?></div>
			<div class="brands__box">
				<?foreach ($brands as $brand):?>
					<?
						$url = Yii::app()->createUrl('/store/producer/view', ['slug' => $brand->slug]);
					?>
					<div class="brands__box-item">
						<div class="brand-item__image">
							<a href="<?=$url?>">
								<img src="<?=$brand->getImageUrl(170, 100, false);?>" alt="<?=$brand->name?>" title="<?=$brand->getFullName()?>">
							</a>
						</div>
						<div class="brand-item__title">
							<a href="<?=$url?>">
								<?=$brand->getFullName()?>
							</a>
						</div>
					</div>
				<?endforeach;?>
			</div>
		<?endforeach;?>
	</div>

	<?if($module->producerPageText):?>
		<div class="catalog-bottom-description">
			<?=$module->producerPageText?>
		</div>
	<?endif;?>
</div>