<?
/**
 * @var Attribute $attribute
 */

$fieldName = Yii::app()->getComponent('attributesFilter')->getFieldName($attribute);
$simpleName = trim($fieldName, '[]');
$actualAttributes = Yii::app()->getComponent('attributesRepository')->getActualOptionValues();
$attributes = array_intersect_key(CHtml::listData($attribute->options, 'id', 'value'), $actualAttributes);
?>
<?if(sizeOf($attributes) > 1):?>
	<div class="filter-block__body">
		<div class="filter-block__select">
			<?=CHtml::dropDownList(
				$fieldName,
				Yii::app()->getRequest()->getParam($simpleName),
				$attributes,
				[
					'data-placeholder' => $attribute->title,
					'multiple' => true,
					'class' => 'multi-select',
					'size' => 1,
				]
			)?>
		</div>
	</div>
<?endif;?>