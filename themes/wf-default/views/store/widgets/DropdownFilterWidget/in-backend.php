<?
/**
 * @var Attribute $attribute
 */

$fieldName = Yii::app()->getComponent('attributesFilter')->getFieldName($attribute);
$simpleName = trim($fieldName, '[]');
$actualAttributes = Yii::app()->getComponent('attributesRepository')->getActualOptionValues();
$attributes = array_intersect_key(CHtml::listData($attribute->options, 'id', 'value'), $actualAttributes);
$state = (array) Yii::app()->user->getState('pg_attributes', []);

$simpleName = $simpleName ?: 'default';
?>
<?if(sizeOf($attributes) > 1):?>
	<div class="col-sm-3 backend-attr-filter-item">
		<label><?=$attribute->title?></label>
		<?=CHtml::dropDownList(
			'attributes['.$simpleName.'][]',
			$state[$simpleName],
			['--- Не выбрано ---'] + $attributes,
			[
				'data-placeholder' => $attribute->title,
				'multiple' => true,
				'size' => 10,
				'class' => 'backend-attr-filter',
			]
		)?>
	</div>
<?endif;?>