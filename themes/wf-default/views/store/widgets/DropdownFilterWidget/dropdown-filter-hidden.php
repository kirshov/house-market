<?php
$filter = Yii::app()->getComponent('attributesFilter');
$options = $attribute->getOptionsList();
$countOptions = sizeof($options);
$i = 1;
?>
<div class="filter-block">
    <div class="filter-block__title"><?= $attribute->title ?></div>
    <div class="filter-block__body">
        <div class="filter-block__list">
	        <?if($countOptions > 6):?>
				<div class="filter-block__list-main">
			<?endif;?>
            <?php foreach ($options as $option): ?>
                <div class="filter-block__list-item">
                    <?= CHtml::checkBox(
                        $filter->getDropdownOptionName($option),
                        $filter->getIsDropdownOptionChecked($option, $option->id),
                        [
                            'value' => $option->id,
                            'class' => 'checkbox',
                            'id' => 'filter-attribute-' . $option->id
                        ]) ?>
                    <?= CHtml::label($option->value, 'filter-attribute-' . $option->id,
                        ['class' => 'checkbox__label']) ?>
                </div>
				<?
				if($i == 6){
					echo '</div><div class="filter-block__list-expand">';
				}
	            $i++;
				?>
            <?php endforeach; ?>
			<?if($countOptions > 6):?>
				</div>
				<span class="filter-expand" data-closed="показать все" data-expanded="свернуть">показать все</span>
			<?endif;?>
        </div>
    </div>
</div>
