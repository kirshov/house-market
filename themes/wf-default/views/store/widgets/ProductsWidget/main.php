<?php
/**
 * @var Product[] $products
 * @var array $options
 */
$currency = Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency);
if(!empty($products)):?>
	<?if($options['wrapper-class']){
		echo CHtml::openTag('div', ['class' => 'wrapper-class']);
	}?>

	<?if($options['title']):?>
		<div class="h2"><?=$options['title']?></div>
	<?endif;?>
	<div class="products-main">
		<div class="products-main-inner">
			<div class="products-list">
				<? foreach ($products as $data){
					include dirname(dirname(__DIR__)).'/product/_item.php';
				}?>

			</div>
		</div>
		<?=$this->after;?>
	</div>

	<?if($options['wrapper-class']){
		echo CHtml::closeTag('div');
	}?>

	<?if($options['isSlider'] && sizeof($products) >= $options['countBySlider']){
		Yii::app()->getClientScript()->registerScript('products-main-slick',
			'$(".products-main .products-list").slick(slickProductOptions);'
		);
	}
	?>
<?endif;?>
