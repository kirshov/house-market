<?php
/**
 * @var Product[] $products
 * @var array $options
 */
$currency = Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency);
if(!empty($products)):?>
	<?if($options['wrapper-class']){
		echo CHtml::openTag('div', ['class' => 'wrapper-class']);
	}?>

	<?if($options['title']):?>
		<div class="h2"><?=$options['title']?></div>
	<?endif;?>
	<div class="widget-products catalog__product-items">
		<? foreach ($products as $data){
			include dirname(dirname(__DIR__)).'/product/_item.php';
		}?>
	</div>

	<?if($options['wrapper-class']){
		echo CHtml::closeTag('div');
	}?>
<?endif;?>
