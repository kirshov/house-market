<?if($options['displayEditable']):?>
	<div style="text-align: center; margin-top: 5px;"><a href="/backend/store/product/update/<?=$product->id?>/" target="_blank">Перейти к редактированию</a></div>
<?endif?>

<?if($options['displayPurchasePrice'] && $purchasePrices):?>
	<div style="text-align: center; margin-top: 5px;">Закупочные цены: <?=implode(' / ', $purchasePrices);?> <?=$currency;?></div>
<?endif?>