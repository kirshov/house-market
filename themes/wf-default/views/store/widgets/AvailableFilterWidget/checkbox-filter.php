<div class="filter-block">
	<div class="filter-block__title">Наличие</div>
	<div class="filter-block__body">
		<div class="filter-block__list">
			<?= CHtml::checkBoxList('available', Yii::app()->getRequest()->getParam('available'), [
				1 => 'В наличии',
				2 => 'Под заказ',
			], [
				'class' => 'checkbox',
				'separator' => "\n",
				'labelOptions' => [
					'class' => 'checkbox__label',
				],
				'container' => false,
				'template' => '<div class="filter-block__list-item">{input} {label}</div>',
			]) ?>
		</div>
	</div>
</div>