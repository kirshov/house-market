<?/**
 * @var StoreCategory[] $categories
 */
?>

<div class="main-categories main-categories__left">
	<div class="main-categories__left-wrap">
		<div class="main-categories__left-one">
			<?$this->renderPartial('_item', ['category' => $categories[0], 'width' => 270, 'left' => 270]);?>
			<?$this->renderPartial('_item', ['category' => $categories[1], 'width' => 270, 'left' => 270]);?>
		</div>
		<div class="main-categories__left-two">
			<?$this->renderPartial('_item', ['category' => $categories[2], 'width' => 270, 'left' => 570]);?>
		</div>
	</div>
</div>

<div class="main-categories main-categories__right">
	<div class="main-categories__right-one">
		<?$this->renderPartial('_item', ['category' => $categories[3], 'width' => 270, 'left' => 270]);?>
		<?$this->renderPartial('_item', ['category' => $categories[5], 'width' => 270, 'left' => 270]);?>
	</div>
	<div class="main-categories__right-two">
		<?$this->renderPartial('_item', ['category' => $categories[4], 'width' => 570, 'left' => 270]);?>
	</div>
</div>
