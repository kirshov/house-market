<?/**
 * @var StoreCategory[] $categories
 */
?>
<?if(!empty($categories)):?>
	<div class="main-category__subitems">
		<ul>
			<?foreach ($categories as $category):?>
				<li>
					<a href="<?=Yii::app()->createUrl('/store/category/view', ['path' => $category->getPath()])?>" title="<?=CHtml::encode($category->getTitle())?>">
						<?=$category->getTitle();?>
					</a>
				</li>
			<?endforeach;?>
		</ul>
	</div>
<?endif;?>
