<?php
/**
 * @var StoreCategory $category
 * @var integer $width
 * @var integer $height
 */
?>
<?if($category):?>
	<?
	$url = StoreCategoryHelper::getUrl($category);
	?>
	<div class="main-categories__item">
		<div class="main-categories__item-image">
			<a href="<?=$url?>">
				<img src="<?=$category->getImageUrl($width, $height)?>">
			</a>
		</div>
		<div class="main-categories__item-title">
			<a href="<?=$url?>">
				<?=$category->name?>
			</a>
		</div>
	</div>
<?endif;?>