<?
/**
 * @var ProductGroups[] $groups
 */
?>
<?if($groups):?>
	<div class="product-groups__items">
		<?foreach ($groups as $group):?>
			<div class="product-groups__item">
				<a href="<?=$group->getUrl()?>"><?=$group->getTitle()?></a>
			</div>
		<?endforeach;?>
	</div>
<?endif;?>