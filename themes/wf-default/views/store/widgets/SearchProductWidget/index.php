<?php
/**
 * @var string $placeholder
 * @var string $autocomplete
 */
echo CHtml::openTag('div', ['class' => 'search-bar-widget']);
$form = $this->beginWidget(
    'CActiveForm',
    [
        'action' => ['/store/product/search'],
        'method' => 'GET',
        'htmlOptions' => [
            'class' => 'search-bar__form',
			'autocomplete' => 'off',
        ]
    ]
);
echo CHtml::textField(AttributeFilter::MAIN_SEARCH_QUERY_NAME, CHtml::encode(Yii::app()->getRequest()->getQuery(AttributeFilter::MAIN_SEARCH_QUERY_NAME)), [
	'class' => 'search-bar__input'.($autocomplete ? ' autocomplete' : ''),
	'placeholder' => $this->placeholder ?: 'Найти товар в каталоге',
	'id' => false,
]);
echo CHtml::submitButton('', ['class' => 'search-bar__submit', 'name' => false]);
$this->endWidget();
echo CHtml::closeTag('div');