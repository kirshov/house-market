<?php
/**
 * @var array $data
 */
?>
<table class="price-list-table">
	<?foreach ($data as $category):?>
		<tr>
			<th><?=$category['title']?></th>
			<th>Ед. изм.</th>
			<th>Цена, руб.</th>
		</tr>
		<?foreach ($category['products'] as $product):?>
			<tr>
				<td><?=$product['name']?></td>
				<td><?=$product['unit']?></td>
				<td><?=Helper::getFormatPrice($product['price'])?></td>
			</tr>
		<?endforeach;?>
	<?endforeach;?>
</table>
