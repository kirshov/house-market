<?php
/**
 * @var Producer[] $brands
 */
$lastLetter = '';
?>
<?php if ($brands): ?>
	<div class="sidebar sidebar-brands">
        <div class="sidebar__title">
            <span><?= $title ?></span>
        </div>
        <div class="sidebar__content">
            <div class="sidebar-brands__items">
                <?php foreach ($brands as $brand):
                    $tempLetter = mb_substr($brand->name, 0, 1);
                    /*if(!$lastLetter){
                        $lastLetter = $tempLetter;
                    }*/
                    if($lastLetter != $tempLetter):
                        $lastLetter = $tempLetter;?>
                        <div class="sidebar-brands__letter"><?=$lastLetter?></div>
                    <?endif;?>
                    <div class="sidebar-brands__item">
                        <a href="<?= Yii::app()->createUrl('/store/producer/view', ['slug' => $brand->slug]) ?>" title="<?= $brand->name ?>">
                            <?= $brand->name ?>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
	</div>
<?php endif; ?>
