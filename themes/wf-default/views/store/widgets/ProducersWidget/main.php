<?php
/**
 * @var Producer[] $brands
 */
?>
<?php if ($brands): ?>
	<div class="main-brands">
		<?php foreach ($brands as $brand): ?>
			<div class="main-brands__item">
				<a href="<?= Yii::app()->createUrl('/store/producer/view', ['slug' => $brand->slug]) ?>" title="<?= $brand->name ?>">
					<img src="<?= StoreImage::producer($brand, 170, 100, false) ?>" class="main-brands__img" alt="<?= $brand->name ?>">
				</a>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>