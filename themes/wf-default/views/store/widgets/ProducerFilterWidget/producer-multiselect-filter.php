<?php if(!empty($producers)):?>
	<div class="filter-block__body">
		<div class="filter-block__select">
			<?=CHtml::dropDownList(
				'brand',
				Yii::app()->getRequest()->getParam('brand'),
				CHtml::listData($producers, 'id', 'name'),
				[
					'data-placeholder' => Yii::t('StoreModule.store', 'Producers'),
					'multiple' => true,
					'class' => 'multi-select',
					'size' => 1,
				]
			)?>
		</div>
	</div>
<?php endif;?>