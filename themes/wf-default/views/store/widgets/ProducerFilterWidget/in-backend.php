<?php if(!empty($producers)):?>
	<div class="col-sm-3 backend-attr-filter-item">
		<label>Производитель</label>
		<?=CHtml::dropDownList(
			'brand',
			(array) Yii::app()->user->getState('pg_brand', []),
			['--- Не выбрано ---'] + CHtml::listData($producers, 'id', 'name'),
			[
				'data-placeholder' => Yii::t('StoreModule.store', 'Producers'),
				'multiple' => true,
				'size' => 10,
				'class' => 'backend-attr-filter',
			]
		)?>
	</div>
<?php endif;?>