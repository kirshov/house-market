<?
$filter = Yii::app()->getComponent('attributesFilter');
$price = (array) Yii::app()->user->getState('pg_price', []);

if(!isset($min)){
	$min = 0;
}
if(!isset($max)){
	$max = 100000;
}
?>

<div class="col-sm-6 backend-attr-filter-item">
	<div class="price_fields">
		<span class="price_fields__label-from">Цена от</span>
		<?= CHtml::textField('price[from]', $price['from'], [
			'class' => 'filter_input__price_from',
			'placeholder' => $min,
		]); ?>
		<span class="price_fields__label-to">до</span>
		<?= CHtml::textField('price[to]', $price['to'], [
			'class' => 'filter_input__price_to',
			'placeholder' => $max,
		]); ?>
	</div>
</div>