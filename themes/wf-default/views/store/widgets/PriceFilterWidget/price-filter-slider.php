<?
$filter = Yii::app()->getComponent('attributesFilter');
$from = $filter->getMainSearchParamsValue('price', 'from', Yii::app()->getRequest());
$to = $filter->getMainSearchParamsValue('price', 'to', Yii::app()->getRequest());
if(!isset($min) || $min == $max){
	$min = 0;
}
if(!isset($max)){
	$max = 100000;
}

if(!is_numeric($from)) $from = $min;
if(!is_numeric($to)) $to = $max;
?>

<div class="price_slider_wrap" data-max="<?=$max?>" data-min="<?=$min?>" data-from="<?=$from?>" data-to="<?=$to?>">
	<div class="price_fields">
		<span class="price_fields__label-from">Цена</span>
		<div class="price_line">
			<?= CHtml::textField('price[from]', $from, [
				'class' => 'filter_input__price_from',
				'placeholder' => $min,
			]); ?>
			<span class="price_fields__label-to"> - </span>
			<?= CHtml::textField('price[to]', $to, [
				'class' => 'filter_input__price_to',
				'placeholder' => $max,
			]); ?>
		</div>
	</div>

	<div class="price_slider"></div>
</div>
