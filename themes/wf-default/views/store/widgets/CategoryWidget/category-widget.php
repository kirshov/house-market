<?php
if(!function_exists('renderMenu')){
	function renderMenu($items, $excluded, $level = 0){
		$menu = '';

		if ($level == 1) {
			$menu .= CHtml::openTag('div', ['class' => 'menu-catalog-submenu']);
		}

		$menu .= CHtml::openTag('ul');

		foreach ($items as $item) {
			if(in_array($item['id'], $excluded)){
				continue;
			}
			/*if ((!empty($item['items']) && $level == 0) && $item['active']) {
				$liOptions = ['class' => 'has-submenu active'];
			} else*/
			if (!empty($item['items'])) {
				$liOptions = ['class' => 'has-submenu'];
			} else {
				$liOptions = ['class' => ''];
			}

			if($item['active'] || Helper::hasSameURL($item['url'])) {
				$liOptions['class'] .= ' active sub-active';
				$item['active'] = true;
			}

			$aOptions = $item['active'] ? ['class' => 'active'] : [];

			$menu .= CHtml::openTag('li', $liOptions);
			$menu .= CHtml::link($item['label'], $item['url'], $aOptions);

			if (!empty($item['items'])) {
				$menu .= renderMenu($item['items'], $excluded, $level + 1);
			}

			$menu .= CHtml::closeTag('li');
		}

		$menu .= CHtml::closeTag('ul');

		if ($level == 1) {
			$menu .= CHtml::closeTag('div');
		}

		return $menu;
	}
}
?>

<div class="menu-catalog">
    <?= renderMenu($tree, $excluded); ?>
</div>
