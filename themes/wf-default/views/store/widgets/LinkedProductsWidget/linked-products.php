<?
/**
 * @var Product[] $products
 */
$needSlider = $options['isSlider'] && sizeof($products) >= $options['countBySlider'];
?>
<?php if (!empty($products)): ?>
	<div class="similar_items">
		<div class="wrapper">
			<?if($options['wrapper-class']){
				echo CHtml::openTag('div', ['class' => 'wrapper-class']);
			}?>

			<?if($options['title']):?>
				<div class="h2"><span><?=$options['title']?></span></div>
			<?endif;?>

			<div class="similar-box<?=($needSlider ? ' similar-box-slider slider-'.$this->getId() : '')?>">
				<div class="similar-box__title"><?=$title?></div>
				<div class="similar-product__list">
					<?foreach ($products as $_item):?>
						<?$this->render('_item', ['data' => $_item]);?>
					<?endforeach;?>
				</div>
			</div>

			<?if($options['wrapper-class']){
				echo CHtml::closeTag('div');
			}?>

			<?if($needSlider){
				Yii::app()->getClientScript()->registerScript('products-main-slick',
					'$(".similar-box-slider .similar-product__list").slick($.extend(slickProductOptions, {infinite: false}));'
				);
			}
			?>
		</div>
	</div>
<?php endif; ?>
