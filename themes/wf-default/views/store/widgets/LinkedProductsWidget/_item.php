<?
/**
 * @var Product $data
 */
$store = Yii::app()->getModule('store');
$currency = Yii::t("StoreModule.store", $store->currency);
$type = $data->getType();
$markerTitles = Product::getMarkerTitles();
$url = ProductHelper::getUrl($data);
?>
<div class="similar-item product-item product-wrapper">
	<?if($type):?>
		<div class="marker marker-<?=$type?>">
			<?=$markerTitles[$type]?>
		</div>
	<?endif;?>

	<div class="product-item__img">
		<a href="<?=$url?>">
			<img src="<?= $data->getImageUrl(160, 160, false); ?>" class="similar-item__img">
		</a>
	</div>
	<div class="product-item__title">
		<a href="<?= $url; ?>" class="similar-item__link">
			<?= Chtml::encode($data->getNameInList()); ?>
		</a>
	</div>
	<div class="product-entry__price">
		<?if($data->hasDiscount()):?>
			<div class="product-item__price-old-wrap">
				<div class="product-item__price-old">
					<?= $data->getFormatPrice($data->getBasePrice()) ?> <?=$currency;?>
				</div>
				<div class="product-item__discount">
					Скидка <?=$data->getDiscount()?>%
				</div>
			</div>
		<?endif;?>

		<div class="product-item__price-actual result-price">
			<?= $data->getFormatPrice() ?> <span class="ruble"><?=$currency;?></span>
		</div>
	</div>
	<div class="product-item-quantity-buy">
		<a href="javascript:void(0);" class="btn btn_cart quick-add-product-to-cart" data-product-id="<?= $data->id; ?>" data-cart-add-url="<?= Yii::app()->createUrl('/cart/cart/add');?>">
			<?= Yii::t('StoreModule.store', 'Into cart') ?>
		</a>
	</div>
	<div class="product-item__buy-one-click">
		<?if($data->getBasePrice() >= $minSumCart):?>
			<span class="buy-one-click__button" data-product-id="<?=$data->id;?>" data-url="<?=Yii::app()->createUrl('/order/order/buyOneClick');?>">
				Купить в 1 клик
			</span>
		<?endif;?>
	</div>
</div>
