<?php
/**
 * @var ProductGroupsController $this
 * @var StoreCategory $category
 */

$module = Yii::app()->getModule('store');

//$titleGroup = $module->productGroupH1 ?: Yii::t('StoreModule.store', 'Popular');
$titleGroup = Yii::t('StoreModule.store', 'Popular');
$title = $category->getGroupsTitle();
$this->breadcrumbs = [
	$titleGroup => ['/store/productGroups/index'],
	$title
];

$this->title = $category->getGroupsMetaTitle() ?: $title;
$this->description = $category->getGroupsMetaDescription();
$this->keywords = $category->getGroupsMetaKeywords();
$this->setTemplateVar('bodyClass', 'catalog-page');

$this->setTitle($title);
$childList = [];
?>
<div class="page page-catalog page-store">
	<div class="store__items">
		<div class="store__item-detail">
			<ul class="store__item-subitems column">
			<?foreach ($groups as $group){
				echo '<li class="store__item-subitem"><a href="'.$group->getUrl().'">'.$group->name.'</a></li>';
				//$childList[] = '<span class="store__item-subitem"><a href="'.Yii::app()->createUrl('/store/productGroups/view', ['slug' => $group->slug]).'">'.$group->name.'</a></span>';
			}
			?>
			</ul>
		</div>
	</div>
</div>