<?php
/**
 * @var ProductGroupsController $this
 */

$module = Yii::app()->getModule('store');

$title = $module->productGroupH1 ?: Yii::t('StoreModule.store', 'Popular');
$this->breadcrumbs = [$title];

$this->title = $module->metaProductGroupTitle ?: $title;
$this->description = $module->metaProductGroupDescription;
$this->keywords = $module->metaProductGroupKeywords;
$this->setTitle($title);
$this->setTemplateVar('bodyClass', 'catalog-page');
$maxList = 1;
?>
<?if($categories):?>
	<div class="page-catalog page-store page-popular">
		<div class="brand__line-items">
			<?foreach ($categories as $category):?>
				<?$link = Yii::app()->createUrl('/store/productGroups/category', ['slug' => $category['category']->groups_alias])?>
				<div class="brand-line__item">
					<div class="brand-line__thumbnail">
						<a href="<?=$link?>">
							<img src="<?=StoreImage::category($category['category'], 140, 140, true)?>">
						</a>
					</div>
					<div class="brand-line__title">
						<a href="<?=$link?>" class="brand-line-item__link"><?=$category['category']->getGroupName();?></a>
					</div>
					<?/*
					<div class="store__item-detail">
						<div class="store__item-title"><a href="<?=Yii::app()->createUrl('/store/category/view', ['path' => $category['category']->getPath()])?>"><?=$category['category']->name?></a></div>
						<?if($category['items']){
							$childList = [];
							echo '<div class="store__item-subitems">';
							$i = 1;
							foreach ($category['items'] as $child){
								if($i > $maxList){
									break;
								}
								$childList[] = '<span class="store__item-subitem"><a href="'.Yii::app()->createUrl('/store/productGroups/view', ['slug' => $child->slug]).'">'.$child->name.'</a></span>';
								$i++;

							}
							echo '<div class="store__item-subitems">'.implode(' <span class="bull">&bull;</span> ', $childList).'</div>';
							if($i > $maxList) {
								echo '<div class="store__item-subitems all view_more"><a href="'.Yii::app()->createUrl('/store/productGroups/category', ['slug' => $category['category']->slug]).'">Показать все</a></div>';
							}
							echo '</div>';
						}?>
					</div>
					*/?>
				</div>
			<?endforeach;?>
		</div>
	</div>
<?endif;?>