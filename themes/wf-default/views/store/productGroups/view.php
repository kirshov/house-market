<?php
/**
 * @var ProductGroupsController $this
 * @var ProductGroups $group
 * @var CActiveDataProvider $dataProvider
 */

$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
$cs = Yii::app()->getClientScript();
$cs->registerCoreScript('jquery.ui');
$cs->registerCssFile($cs->getCoreScriptUrl(). '/jui/css/base/jquery-ui.css');

$this->title = $group->getMetaTitle();
$this->description = $group->getMetaDescription();
$this->keywords =  $group->getMetaKeywords();
$this->canonical = $group->getMetaCanonical();

$this->breadcrumbs = $this->getBreadcrumbs($group);
$showBy = $this->renderPartial('/category/_show_by', null, true);

$isGetQuery = Yii::app()->getModule('store')->isGetFilterParams();

$this->setTemplateVar([
	/*'brandInCats' => $this->widget('producers', [
			'view' => 'main',
			'category' => $category,
			'title' => sprintf('Бренды категории «%s»', $category->name)
		], true),*/
	'descriptionsAfterPos' => !$isGetQuery ? $group->description_bottom : null,
	//'categorySelectId' => $group->id,
]);
$this->setTemplateVar('bodyClass', 'catalog-page');
$this->setTitle($group->getTitle());
$this->setTemplateVar('filter', $this->renderPartial('//store/category/_filter', [
	'category' => null,
	'producersOnFilter' => $producers,
], true));
?>
<?if(!$isGetQuery && $group->description):?>
	<div class="catalog-top-description">
		<?=$group->description?>
	</div>
<?endif;?>

<div class="catalog">
	<?$this->renderPartial('/category/_product_list', ['dataProvider' => $dataProvider]);?>
</div>
