<?
/**
 * @var \webforma\components\controllers\FrontController $this
 * @var Product $product
 */
$store = Yii::app()->getModule('store');
$tabsData = $product->getTabsFromKey();
$characteristic = '';
$needCharacteristic = false;
foreach ($this->getModule()->productTabs as $_key => $_tab){
	if($tabsData['presets'] = 'characteristic'){
		$needCharacteristic = true;
	}
}

if($needCharacteristic){
	$characteristic = $this->renderPartial('_simple_attributes', ['product' => $product, 'microdata' => true], true);
	$characteristic .= $this->renderPartial('_variants_attributes', ['product' => $product], true);
	$characteristic = trim($characteristic);
}
?>

<div class="main__product-tabs">
	<div class="tabs js-tabs-mobile">
		<div class="js-tabs-holder">
			<div class="wrapper">
				<ul data-nav="data-nav" class="tabs__list">
					<?foreach ($this->getModule()->productTabs as $_key => $_tab):?>
						<?
						$tabName = '';
						if(!$_tab['presets'] && !$tabsData[$_key]->content){
							continue;
						}
						if($_tab['presets'] == 'description'){
							$tabName = 'description';
							if(!$product->description){
								continue;
							}
						}
						if($_tab['presets'] == 'characteristic'){
							if(!$characteristic){
								continue;
							}
							$tabName = 'description';
						}
						if($_tab['presets'] == 'text' && !$_tab['presets-text']){
							continue;
						}

						if($_tab['presets'] == 'comments'){
							$tabName = 'comments';
						}
						?>
						<li class="tabs__item">
							<span class="tabs__link"><?=$_tab['name']?></span>
							<div class="tabs__body"<?=($_tab['presets'] == 'description') ? ' itemprop="description"' : ''?>>
								<div class="product-tab-<?=($tabName) ? $tabName : $_key?>">
									<?switch ($_tab['presets']){
										case 'characteristic':
											echo $characteristic;
											break;

										case 'comments':
											$this->widget('comments', [
												'model' => $product,
											]);
											break;

										case 'text':
											echo $_tab['presets-text'];
											break;

										case 'description':
											if($store->notProductIndexDescription && !$product->force_index){
												echo '<!--noindex-->';
											}
											echo $product->description;
											if($store->notProductIndexDescription && !$product->force_index){
												echo '<!--/noindex-->';
											}
											break;

										default:
											echo $tabsData[$_key]->content;
									}
									?>
								</div>
							</div>
						</li>
					<?endforeach;?>
				</ul>
			</div>
		</div>
	</div>
</div>
