<?
/**
 * @var Product $product
 * @var string $sku
 */

$variantsOptions = $product->getVariantsOptions();
$variantsGroups = $product->getVariantsGroup();
$html = '';

if ($variantsGroups) {
	foreach ($variantsGroups as $type => $variantsDataGroup) {
		foreach ($variantsDataGroup as $title => $variantsGroup) {
			$itemsValues = [];
			foreach ($variantsGroup as $itemTitle => $item) {
				$itemsValues[] = strip_tags($itemTitle);
			}
			if($itemsValues){?>
			<dl class="product-spec-item">
				<dt class="product-spec-item__name">
					<span class="product-spec-item__name-inner"><?=$title?>:</span>
				</dt>
				<dd class="product-spec-item__value">
					<span class="product-spec-item__value-inner"><?= implode(', ', $itemsValues) ?></span>
				</dd>
			</dl>
			<?php
			}
		}
	}

	if($html){
		echo CHtml::tag('div', ['class' => 'product-entry__variants'], $html);
	}
}