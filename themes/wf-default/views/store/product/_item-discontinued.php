<?php
/**
 * @var Product $data
 * @var ProductController $this
 */
$attributes = Yii::app()->getController()->renderPartial('//store/product/_simple_attributes', ['product' => $data], true);

?>
<div class="product-item product-wrapper">
	<div class="product-item__img">
		<a href="<?= ProductHelper::getUrl($data); ?>">
			<img src="<?= StoreImage::product($data, $store->productListImageWidth, $store->productListImageHeight, $store->productListImageCrop); ?>"
				 alt="<?= CHtml::encode($data->getImageAlt()); ?>"
				 title="<?= CHtml::encode($data->getImageTitle()); ?>"
			/>
		</a>
	</div>
	<div class="product-item__title">
		<a href="<?= ProductHelper::getUrl($data); ?>" title="<?= CHtml::encode($data->getNameInList()); ?>"><?= CHtml::encode($data->getNameInList()); ?></a>
	</div>

	<?if($attributes):?>
		<div class="product-item__attributes">
			<?=$attributes;?>
		</div>
	<?endif?>

    <form method="post" class="product-item__price-area">
		<div class="product-item__not-available">Снят с производства!</div>
    </form>
</div>
