<?php
/**
 * @var Product $data
 * @var ProductController $this
 */
if(!$type){
	$type = $data->getType();
}
$store = Yii::app()->getModule('store');
$currency = Yii::t('StoreModule.store', $store->currency);
$minSumCart = Yii::app()->getModule('store')->minCost;

if($data->hasDiscount()){
	$discount = $data->getDiscount();
}
?>
<div class="product-item product-wrapper" data-id="<?=$data->id?>">
	<?if($in_favorite):?>
		<a href="javascript:void(0);" class="store-favorite-remove close-btn" data-id="<?= $data->id;?>">&times;</a>
	<?endif;?>
	<div class="product-item__img">
		<a href="<?= ProductHelper::getUrl($data); ?>">
			<div class="product-item__img-wrapper">
				<img src="<?= StoreImage::product($data, 335, 216, 2); ?>"
					 alt="<?= CHtml::encode($data->getImageAlt()); ?>"
					 title="<?= CHtml::encode($data->getImageTitle()); ?>"
				/>
			</div>
			<?$this->renderPartial('//store/product/_images', ['product' => $data, 'width' => 335, 'height' => 216])?>
		</a>
	</div>

	<div class="product-item__info">
		<div class="product-item__title">
			<a href="<?= ProductHelper::getUrl($data); ?>" title="<?= CHtml::encode($data->getNameInList()); ?>"><?= CHtml::encode($data->getNameInList()); ?></a>
		</div>

		<div class="product-item__characteristic">
				<dl class="product-spec-item">
					<dt class="product-spec-item__name">
						<span class="product-spec-item__name-inner">Площадь:</span>
					</dt>
					<dd class="product-spec-item__value">
						<span class="product-spec-item__value-inner"><?= Helper::getFormatPrice($data->square)?> м<sup>2</sup></span>
					</dd>
				</dl>
				<dl class="product-spec-item">
					<dt class="product-spec-item__name">
						<span class="product-spec-item__name-inner">Готовность:</span>
					</dt>
					<dd class="product-spec-item__value">
						<span class="product-spec-item__value-inner"><?=DictionaryData::getReadiness($data)?></span>
					</dd>
				</dl>
                <dl class="product-spec-item">
                    <dt class="product-spec-item__name">
                        <span class="product-spec-item__name-inner">Этажность:</span>
                    </dt>
                    <dd class="product-spec-item__value">
                        <span class="product-spec-item__value-inner"><?=DictionaryData::getFloor($data->floor_id)?></span>
                    </dd>
                </dl>
				<dl class="product-spec-item product-spec-item_price">
					<dt class="product-spec-item__name">
						<span class="product-spec-item__name-inner">Цена:</span>
					</dt>
					<dd class="product-spec-item__value">
						<span class="product-spec-item__value-inner">
							<span class="result-price"><span class="result-price-value"><?= $data->getFormatPrice() ?></span> <span class="currency"><?=$currency;?></span></span>
						</span>
					</dd>
				</dl>
		</div>
	</div>

	<div class="product-item__buttons">
		<form action="<?= Yii::app()->createUrl('cart/cart/add'); ?>" method="post" class="product-item__price-area">
			<input type="hidden" name="Product[id]" value="<?= $data->id; ?>"/>
			<?= CHtml::hiddenField(Yii::app()->getRequest()->csrfTokenName, Yii::app()->getRequest()->csrfToken, ['id' => false]); ?>
			<input type="hidden" class="base-price" value="<?= round($data->getResultPrice(), 2); ?>"/>

			<div class="product-item-quantity-buy">
				<div class="product-item__buy">
					<div class="product-item__buy-one-click">
						<span class="buy-one-click__button" data-text="<?=CHtml::encode($data->getNameInList());?>">
							Записаться на&nbsp;просмотр
						</span>
					</div>
				</div>

				<div class="product-item__calc">
					<span class="product-calc__button" data-price="<?=$data->getResultPrice()?>">
						Рассчитать ипотеку
					</span>
				</div>
			</div>
		</form>

		<div class="product-item__widgets">
			<?$this->widget('compare', ['product' => $data])?>
			<?$this->widget('favorite', ['product' => $data])?>
		</div>
	</div>
</div>
