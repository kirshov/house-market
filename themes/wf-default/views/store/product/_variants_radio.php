<?
$variantsOptions = $product->getVariantsOptions();
$variantsGroups = $product->getVariantsGroup();
$html = '';

if ($variantsGroups) {
	foreach ($variantsGroups as $type => $variantsDataGroup) {
		foreach ($variantsDataGroup as $title => $variantsGroup) {
			$i = 0;
			if (sizeof($variantsGroup) > 1) {
				$html .= CHtml::openTag('div', ['class' => 'product-entry__variant']);
				if ($type == Attribute::TYPE_RADIO) {
					foreach ($variantsGroup as $item) {
						$html .= '<label>';
						$html .= CHtml::radioButton(
							'ProductVariant['.$product->id.']['.$item->attribute_id.']',
							($variantId && $variantsOptions[$variantId]) ? $item->id == $variantId : $i == 0,
							CMap::mergeArray(
								(array) $variantsOptions[$item->id],
								[
									'class' => 'product-entry__variant-change',
									'id' => 'ProductVariant_'.$item->attribute_id.'_'.$item->id.'_'.$product->id,
									'value' => $item->id,
								]
							)
						);
						$html .= CHtml::tag('span', [], $item->optionValue);
						$html .= '</label>';
						if(empty($variantsOptions[$item->id]['disabled'])){
							$i++;
						}
					}
				}
				$html .= CHtml::closeTag('div');
			}
		}
	}

	if($html){
		echo CHtml::tag('div', ['class' => 'product-entry__variants'], $html);
	}
}