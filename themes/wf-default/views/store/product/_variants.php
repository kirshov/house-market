<?
/**
 * @var $variantId integer|null
 */
$variantsOptions = $product->getVariantsOptions();
$variantsGroups = $product->getVariantsGroup();
$html = '';

if ($variantsGroups) {
	foreach ($variantsGroups as $type => $variantsDataGroup) {

		foreach ($variantsDataGroup as $title => $variantsGroup) {
			$i = 0;
			if (sizeof($variantsGroup) > 1) {
				$html .= CHtml::openTag('div', ['class' => 'product-entry__variant '.Attribute::getTypesName($type)]);
				$html .= CHtml::tag('div', ['class' => 'product-entry__variant-title'], $title);
				$html .= CHtml::openTag('div', ['class' => 'product-entry__variant-inner']);
				$html .= CHtml::openTag('div', ['class' => 'product-entry__variant-holder']);
				switch ($type){
					case Attribute::TYPE_DROPDOWN:
						$itemList = [];
						foreach ($variantsGroup as $itemTitle => $item) {
							$itemList[$item->attribute_id][$item->id] = $itemTitle;

							$checked = ($variantId && $variantsOptions[$variantId]) ? $item->id == $variantId : $i == 0;
							if($checked) {
								$this->setTemplateVar('product_variant_'.$product->id, $item);
							}

							if(empty($variantsOptions[$item->id]['disabled'])){
								$i++;
							}
						}

						foreach ($itemList as $attributeId => $itemSubList) {
							$html .= CHtml::dropDownList(
								'ProductVariant['.$product->id.']['.$attributeId.']',
								$variantId,
								$itemSubList,
								[
									'options' => (array) $variantsOptions,
									'class' => 'product-entry__variant-change styler',
								]
							);
						}
						break;

					case Attribute::TYPE_RADIO:
					case Attribute::TYPE_COLOR:
						foreach ($variantsGroup as $itemTitle => $item) {
							$checked = ($variantId && $variantsOptions[$variantId]) ? $item->id == $variantId : $i == 0;
							if($checked) {
								$this->setTemplateVar('product_variant_'.$product->id, $item);
							}

							$html .= '<label>';
							$html .= CHtml::radioButton(
								'ProductVariant['.$product->id.']['.$item->attribute_id.']',
								($variantId && $variantsOptions[$variantId]) ? $item->id == $variantId : $i == 0,
								CMap::mergeArray(
									(array) $variantsOptions[$item->id],
									[
										'class' => 'product-entry__variant-change',
										'id' => 'ProductVariant_'.$item->attribute_id.'_'.$item->id.'_'.$product->id,
										'value' => $item->id,
									]
								)
							);
							$html .= CHtml::tag('span', [], CHtml::tag('em', [], $itemTitle));
							$html .= '</label>';
							if(empty($variantsOptions[$item->id]['disabled'])){
								$i++;
							}
						}
						break;

				}
				$html .= CHtml::closeTag('div');
				$html .= CHtml::closeTag('div');
				$html .= CHtml::closeTag('div');
			}
		}
	}

	if($html){
		echo CHtml::tag('div', ['class' => 'product-entry__variants'], $html);
	}
}