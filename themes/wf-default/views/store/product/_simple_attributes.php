<?
/**
 * @var Product $product
 * @var string $sku
 */
$links = false;
?>
<?php foreach ($product->getAttributeGroups() as $groupName => $items): ?>
	<?php foreach ($items as $attribute): ?>
		<?
        if($attribute->hide_in_site || $product->hasVariantByAttribute($attribute->id)){
            continue;
        }
		$value = AttributeRender::renderValue($attribute, $product->attribute($attribute),true, '{item}');
		if(!$attribute->title || !$value){
			continue;
		}
		?>
		<dl class="product-spec-item">
			<dt class="product-spec-item__name">
				<span class="product-spec-item__name-inner"><?= CHtml::encode($attribute->title); ?>:</span>
			</dt>
			<dd class="product-spec-item__value">
				<span class="product-spec-item__value-inner"><?= AttributeRender::formatSting($value); ?></span>
			</dd>
		</dl>
	<?php endforeach; ?>
<?php endforeach; ?>
