<?php
/**
 * @var Product $product
 * @var int $width
 * @var int $height
 */

$countImages = sizeof($product->getImages());
if(!$countImages){
	return;
}
$store = Yii::app()->getModule('store');
$countImages++;
$i = 0;
?>
<div class="product-item__img-bullets">
	<?for ($i = 1; $i<=5; $i++):?>
		<div class="product-bullets__item<?=($i==1) ? ' active' : ''?>"></div>
	<?endfor;?>
</div>

<?$i++;?>
<?if($countImages > 5):?>
	<div class="product-item__img-more">
		<div class="gallery-more__icon"></div>
		<div class="gallery-more__text">Показать еще <?=$countImages - 5?></div>
	</div>
<?endif;?>

<div class="product-item__img-gallery">
	<div class="product-gallery__item">
		<img src="<?= StoreImage::product($product, $width, $height, 2); ?>" class="product-gallery__item-img">
	</div>
	<?$i=0;?>
	<?foreach ($product->getImages() as $image):?>
		<?
			if($i >= 4){
				continue;
			}
			$i++;
		?>
		<div class="product-gallery__item">
			<img src="<?= $image->getImageUrl($width, $height, 2); ?>" class="product-gallery__item-img">
		</div>
	<?endforeach;?>
</div>