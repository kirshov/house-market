<?php foreach ($product->getAttributeGroups() as $groupName => $items): ?>
	<?php foreach ($items as $attribute): ?>
		<?
		$value = AttributeRender::renderValue($attribute, $product->attribute($attribute),true, '{item}');
		if(!$attribute->is_general || !$attribute->title || !$value){
			continue;
		}

		echo '<span class="cart-item__variant-item">'.$attribute->title.': '.AttributeRender::formatSting($value).'</span>';
		?>
	<?php endforeach; ?>
<?php endforeach; ?>
