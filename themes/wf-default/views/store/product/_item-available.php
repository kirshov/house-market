<?php
/**
 * @var Product $data
 * @var ProductController $this
 */
if(!$type){
	$type = $data->getType();
}
$store = Yii::app()->getModule('store');
$attributes = Yii::app()->getController()->renderPartial('//store/product/_simple_attributes_list', ['product' => $data], true);
$currency = Yii::t('StoreModule.store', $store->currency);
$minSumCart = Yii::app()->getModule('store')->minCost;

if($data->hasDiscount()){
	$discount = $data->getDiscount();
}
?>
<div class="product-item product-wrapper" data-id="<?=$data->id?>">
	<?if($in_favorite):?>
		<a href="javascript:void(0);" class="store-favorite-remove close-btn" data-id="<?= $data->id;?>">&times;</a>
	<?endif;?>
	<div class="product-item__img">
		<a href="<?= ProductHelper::getUrl($data); ?>">
			<div class="product-item__img-wrapper">
				<img src="<?= StoreImage::product($data, 300, 192, 2); ?>"
					 alt="<?= CHtml::encode($data->getImageAlt()); ?>"
					 title="<?= CHtml::encode($data->getImageTitle()); ?>"
				/>
			</div>
			<?$this->renderPartial('//store/product/_images', ['product' => $data, 'width' => 300, 'height' => 192])?>
		</a>
	</div>

	<div class="product-item__info">
		<div class="product-item__title">
			<a href="<?= ProductHelper::getUrl($data); ?>" title="<?= CHtml::encode($data->getNameInList()); ?>"><?= CHtml::encode($data->getNameInList()); ?></a>
		</div>

		<div class="product-item__characteristic">
			<div class="product-item__characteristic-left">
				<dl class="product-spec-item">
					<dt class="product-spec-item__name">
						<span class="product-spec-item__name-inner">Площадь:</span>
					</dt>
					<dd class="product-spec-item__value">
						<span class="product-spec-item__value-inner"><?= Helper::getFormatPrice($data->square)?> м<sup>2</sup></span>
					</dd>
				</dl>
				<dl class="product-spec-item">
					<dt class="product-spec-item__name">
						<span class="product-spec-item__name-inner">Этажность:</span>
					</dt>
					<dd class="product-spec-item__value">
						<span class="product-spec-item__value-inner"><?=DictionaryData::getFloor($data->floor_id)?></span>
					</dd>
				</dl>
			</div>

			<div class="product-item__characteristic-right">
				<dl class="product-spec-item">
					<dt class="product-spec-item__name">
						<span class="product-spec-item__name-inner">Готовность:</span>
					</dt>
					<dd class="product-spec-item__value">
						<span class="product-spec-item__value-inner"><?=DictionaryData::getReadiness($data)?></span>
					</dd>
				</dl>
				<dl class="product-spec-item">
					<dt class="product-spec-item__name">
						<span class="product-spec-item__name-inner">Участок:</span>
					</dt>
					<dd class="product-spec-item__value">
						<span class="product-spec-item__value-inner"><?= Helper::getFormatPrice($data->square_area)?> соток</span>
					</dd>
				</dl>
			</div>
		</div>

		<?if($attributes):?>
			<div class="product-item__attributes">
				<?=$attributes;?>
			</div>
		<?endif?>
	</div>

	<div class="product-item__buttons">

		<div class="product-item__price">
			<input type="hidden" class="base-price" value="<?= round($data->getResultPrice(), 2); ?>"/>

			<div class="product-item__price-actual">
				<span class="result-price"><span class="result-price-value"><?= $data->getFormatPrice() ?></span> <span class="currency"><?=$currency;?></span></span>
			</div>
		</div>

		<div class="product-item__widgets">
			<?$this->widget('compare', ['product' => $data])?>
			<?$this->widget('favorite', ['product' => $data])?>
			<?$this->widget('content', ['code' => 'share', 'view' => 'share'])?>
		</div>

		<div class="product-item-quantity-buy">
			<div class="product-item__buy">
				<div class="product-item__buy-one-click">
					<span class="buy-one-click__button" data-text="<?=CHtml::encode($data->getNameInList());?>">
						Записаться на&nbsp;просмотр
					</span>
				</div>
			</div>

			<div class="product-item__calc">
				<span class="product-calc__button" data-price="<?=$data->getResultPrice()?>">
					Рассчитать ипотеку
				</span>
			</div>
		</div>
	</div>
</div>
