<?php
$mainAssets = Yii::app()->getTheme()->getAssetsUrl();

/* @var CActiveDataProvider $dataProvider */

$searchQuery = trim(Yii::app()->getRequest()->getParam('q'));
$title = 'Результаты поиска'.($searchQuery ? ' по запросу «'.$searchQuery.'»' : '');
$this->title = Helper::prepareSeoTitle($title);
$this->breadcrumbs = [
    'Результаты поиска'
];
$this->setTitle($title);
$this->setTemplateVar('bodyClass', 'catalog-page');

$this->layout = '//layouts/onecol';
echo '<div class="search-in-page">';
$this->widget('searchProduct');

if(mb_strlen($searchQuery) >= 3){
    if($category){
        echo '<div class="search-block">';
        echo '<div class="h3">Категории</div>';
        foreach ($category as $item){
            echo '<p class="search-item"><a href="'.Yii::app()->createUrl('/store/category/view', ['path' => $item->getPath()]).'">'.$item->name.'</a></p>';
        }
        echo '</div>';
    }
    if($producerLine){
        echo '<div class="search-block">';
        echo '<div class="h3">Линии</div>';
        foreach ($producerLine as $item){
            echo '<p class="search-item"><a href="'.Yii::app()->createUrl('/store/producer/line', ['slug' => $item->producer->slug, 'line' => $item->slug]).'">'.$item->name.'</a></p>';
        }
        echo '</div>';
    }

    if($producer):?>
        <div class="search-block">
            <div class="h3">Бренды</div>
            <div class="brands__box">
            <?foreach ($producer as $item):?>
                <?$link = Yii::app()->createUrl('/store/producer/view', ['slug' => $item->slug]);?>
                <div class="brands__box-item">
                    <div class="brand-item__image">
                        <a href="<?=$link?>">
                            <img src="<?=$item->getImageUrl(140, 140, false)?>" class="brand-line-item__img">
                        </a>
                    </div>
                    <div class="brand-item__title">
                        <a href="<?=$link?>" class="brand-line-item__link"><?=$item->getName()?></a>
                    </div>
                </div>
            <?endforeach;?>
            </div>
        </div>
    <?endif;

    if($dataProvider->getTotalItemCount() > 0 || ($dataProvider->getTotalItemCount() == 0 && !$category && !$producerLine && !$producer)) {
		echo '<div class="search-block"><div class="h3">Товары</div>';
        $this->renderPartial('/category/_product_list', [
            'dataProvider' => $dataProvider,
            'emptyText' => 'К сожалению не нашлось ни одного товара, пожалуйста, попробуйте переформулировать запрос.',
        ]);
		echo '</div>';
    }
} else {
    echo '<div class="catalog__product-items"><div class="empty">Пожалуйста, введите более 3х символов</div></div>';
}
echo '</div>';
