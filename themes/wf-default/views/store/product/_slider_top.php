<div class="forotama-top-bar-wrapper hidden">
	<div class="forotama-top-bar">

		<div class="fr-title"><?=$product->getTitle()?></div>
		<div class="fr-phone">
			<?$this->widget('content', ['code' => 'phone'])?>
		</div>
		<div class="fr-buttons">
			<div class="lh_feedback modal-show close_fotorama" data-modal="callback"></div>
			<div class="product-detail__widgets">
				<?$this->widget('compare', ['product' => $product])?>
				<?$this->widget('favorite', ['product' => $product])?>
			</div>
			<div class="product-detail__order">
				<span class="buy-one-click__button close_fotorama" data-text="<?=CHtml::encode($product->getNameInList());?>">
					Записаться на&nbsp;просмотр
				</span>
			</div>
		</div>

	</div>
</div>