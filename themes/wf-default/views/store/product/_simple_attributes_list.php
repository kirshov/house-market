<?
/**
 * @var Product $product
 * @var string $sku
 */
$links = false;
$material = $otdelka = false;
$dop = $list = [];
?>

<?php foreach ($product->getAttributeGroupsWithId() as $groupName => $items){
	foreach ($items as $attribute){
        if($attribute->hide_in_site || $product->hasVariantByAttribute($attribute->id)){
            continue;
        }
		$value = AttributeRender::renderValue($attribute, $product->attribute($attribute),true, '{item}');
        $origValue = $value;
		if(!$attribute->title || !$value){
			continue;
		}

		$value = '<li class="product-item__attribute-item">'.CHtml::encode($attribute->title).': '.$value.'</li>';

		if($attribute->name == 'material-sten'){
			$material = $value;
		} elseif($attribute->name == 'vid-vnutrenney-otdelki'){
			$otdelka = '<li class="product-item__attribute-item">Отделка: '.$origValue.'</li>';
		} elseif(in_array($attribute->name, ['otdelka-sten', 'kolichestvo-komnat', 'otoplenie'])) {
			$dop[] = $value;
		} else {
			if($groupName != 2){
				continue;
			}
			$value = AttributeRender::renderValue($attribute, $product->attribute($attribute),2, '{item}');
			if($value[0]){
				$list[] = '<li class="product-item__attribute-item">'.trim($value[0]).'</li>';
			}
			/*$exp = explode(',', $origValue);
			if($exp){
				foreach ($exp as $_item){
					$list[] = '<li class="product-item__attribute-item">'.trim($_item).'</li>';
				}
			}*/
		}


	}
}
$result = [];
if($material){
	$result[] = $material;
}
if($otdelka){
	$result[] = $otdelka;
}
$result = Helper::mergeArray($result, $dop);
$result = Helper::mergeArray($result, $list);
$result = array_slice($result, 0, 6);
?>
<ul class="product-item__attributes-list">
	<?=implode('', $result)?>
</ul>