<?
/**
 * @var Product $product
 * @var string $sku
 */
$links = false;
?>
<?php foreach ($product->getAttributeGroupsWithId() as $groupName => $items): ?>
	<?php
	if(!in_array($groupName, [1, 4, 6])){
		continue;
	}
	?>
	<?php foreach ($items as $attribute): ?>
		<?
		if($attribute->name == 'material-sten' || $attribute->name == 'vid-vnutrenney-otdelki'){
			continue;
		}

		if($attribute->hide_in_site || $product->hasVariantByAttribute($attribute->id)){
			continue;
		}
		$value = AttributeRender::renderValue($attribute, $product->attribute($attribute),true, '{item}');
		if(!$attribute->title || !$value){
			continue;
		}
		?>
		<tr class="product-spec-item">
			<td class="product-spec-item__name">
				<span class="product-spec-item__name-inner"><?= CHtml::encode($attribute->title); ?></span>
			</td>
			<td class="product-spec-item__value">
				<span class="product-spec-item__value-inner"><?= AttributeRender::formatSting($value); ?></span>
			</td>
		</tr>
	<?php endforeach; ?>
<?php endforeach; ?>
