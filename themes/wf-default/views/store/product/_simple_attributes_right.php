<?
/**
 * @var Product $product
 * @var string $sku
 */
$links = false;
?>
<?php foreach ($product->getAttributeGroups() as $groupName => $items): ?>
	<?php foreach ($items as $attribute): ?>
		<?
        if(!($attribute->name == 'material-sten' || $attribute->name == 'vid-vnutrenney-otdelki')){
			continue;
		}

        if($attribute->hide_in_site || $product->hasVariantByAttribute($attribute->id)){
            continue;
        }
		$value = AttributeRender::renderValue($attribute, $product->attribute($attribute),true, '{item}');

		if(!$attribute->title || !$value){
			continue;
		}
		$title = ($attribute->name == 'vid-vnutrenney-otdelki') ? 'Отделка' : CHtml::encode($attribute->title);
		?>
		<div class="params-item">
            <div class="params-item__title"><?=$title?>:</div>
            <div class="params-item__value"><?= AttributeRender::formatSting($value)?></div>
        </div>
	<?php endforeach; ?>
<?php endforeach; ?>
