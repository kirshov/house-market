<?
/**
 * @var Product $product
 * @var string $sku
 */
$links = false;
?>
<?php foreach ($product->getAttributeGroupsWithId() as $groupId => $items): ?>
	<?
		$_items = [];

		if($groupId == 2){
			$groupName = 'Дополнительно';
		} elseif ($groupId == 3){
			$groupName = 'Планировка';
		} else {
			continue;
		}
	?>
	<?php foreach ($items as $attribute): ?>
		<?
		if($attribute->hide_in_site || $product->hasVariantByAttribute($attribute->id)){
			continue;
		}

		if($groupId == 3){
			$value = AttributeRender::renderValue($attribute, $product->attribute($attribute),true, '{item}');
			if(!$attribute->title || !$value){
				continue;
			}

			$_items[] = $attribute->title.' - '.AttributeRender::formatSting($value);
		} elseif ($groupId == 2){
			$value = AttributeRender::renderValue($attribute, $product->attribute($attribute),2, '{item}');
			if(!$attribute->title || !$value){
				continue;
			}

			$_items = Helper::mergeArray($_items, $value);
		}

		?>
	<?php endforeach; ?>

	<?if($_items):?>
		<tr class="product-spec-item product-spec-item-scope">
			<td colspan="2">
				<div class="product-spec-item-center"><?=$groupName?></div>
				<div class="product-spec-item__value">
					<ul>
						<?foreach ($_items as $_item):?>
							<li><?=$_item?></li>
						<?endforeach;?>
					</ul>
				</div>
			</td>
		</tr>
	<?endif;?>
<?php endforeach;?>