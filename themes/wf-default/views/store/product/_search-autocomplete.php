<?php
/**
 * @var string $term
 * @var Product[] $products
 * @var StoreCategory[] $categories
 * @var Producer[] $brands
 * @var ProducerLine[] $lines
 */
$count = sizeof($products) + sizeof($categories) + sizeof($brands) + sizeof($lines);
?>
<?if($count):?>
	<div class="search-result__wrapper">
		<div class="search-result__group">
			<div class="search-result__item search-result__item-site">
				<a href="<?=Yii::app()->createUrl('/store/product/search', ['q' => $term])?>">Искать «<?=$term?>» по всему сайту</a>
			</div>
		</div>

		<?if($categories):?>
			<div class="search-result__group">
				<div class="search-result__group">Категории</div>
				<?foreach ($categories as $category):?>
					<div class="search-result__item search-result__item-category">
						<a href="<?=StoreCategoryHelper::getUrl($category)?>"><?=$category->name?></a>
					</div>
				<?endforeach;?>
			</div>
		<?endif;?>

		<?if($products):?>
			<div class="search-result__group">
				<div class="search-result__group">Товары</div>
				<?foreach ($products as $product):?>
					<div class="search-result__item search-result__item-product">
						<a href="<?=ProductHelper::getUrl($product)?>"><?=$product->getNameInList()?></a>
					</div>
				<?endforeach;?>
				<div class="search-result__item search-result__item-site">
					<a href="<?=Yii::app()->createUrl('/store/product/search', ['q' => $term])?>">Поиск всех товаров, содержащих «<?=$term?>»</a>
				</div>
			</div>
		<?endif;?>
	</div>
<?endif?>