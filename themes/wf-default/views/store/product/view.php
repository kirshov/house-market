<?php

/**
 * @var Product $product
 * @var ProductVariant $variant
 * @var integer $variantId
 * @var \webforma\components\controllers\FrontController $this
 */

$this->title = $product->getMetaTitle();
$this->description = $product->getMetaDescription();
$this->keywords = $product->getMetaKeywords();

$this->canonical = $product->getMetaCanonical();
$this->setTitle($product->getTitle());
if($product->image){
	$this->setMetaImage($product->getImageUrl(250, 250, false));
	$this->setMetaSrcImage($product->getImageUrl(180, 120, false));
}
$this->setTemplateVar(['bodyClass', 'product-page']);
$this->setTemplateVar('bodyClass', 'page-onepos');
$producer = $product->getProducer();
if($producer && $producer->retargeting_code){
	$this->setTemplateVar(['retargeting-code', $producer->retargeting_code]);
}

/*$this->breadcrumbs = array_merge(
	$product->category ? $product->category->getBreadcrumbs(true) : [],
	[CHtml::encode($product->name)]
);*/

$marker = $product->getMarker();
$webforma = Yii::app()->getModule('webforma');
$store = Yii::app()->getModule('store');
$currency = Yii::t("StoreModule.store", $store->currency);
$productObject = $variant !== null ? $variant : $product;
$price = $productObject->getFormatPrice();
$variants = $this->renderPartial('_variants', ['product' => $product, 'variantId' => $variantId], true);

$_pVariant = $this->getTemplateVar('product_variant_'.$product->id);
if(!$variant && $_pVariant){
	$productObject = $_pVariant;
	$price = $_pVariant->getResultPagePrice();
}

Yii::app()->getModule('dictionary');

if($product->place_id){
	$placeMap = DictionaryDataVillage::getValue($product->place_id, 'map');
}
$reviews = '';
if($product->producer_id){
	$reviews = $this->widget('reviews', ['producerId' => $product->producer_id], true);
}
?>
<link  href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>

<div class="product-page">
	<div class="wrapper">
		<div class="product-page__left">
			<div class="product-detail product-wrapper" data-id="<?=$product->id?>">
                <div class="product-detail__main">
                    <?if($product->search_name):?>
                        <div class="product-detail-subtitle"><?=$product->search_name?></div>
                    <?endif?>
                    <div class="product-detail-top">
                        <div class="product-gallery">
                            <div class="product-gallery__body">
                                <?//if($product->getImages()):?>
                                    <div class="product-images">
                                        <div class="fotorama" data-nav="thumbs" data-auto="false" data-allowfullscreen="native">
                                            <a href="<?= $product->getImageUrl(785, 512, 2); ?>" >
                                                <img src="<?= StoreImage::product($product, 202, 132, 2); ?>" class="product-gallery__main-img" alt="<?= CHtml::encode($product->getImageAlt()); ?>" title="<?= CHtml::encode($product->getImageTitle()); ?>" itemprop="image">
                                            </a>

                                            <?php foreach ($product->getImages() as $key => $image): ?>
                                                <a href="<?= $image->getImageUrl(785, 512, 2); ?>">
                                                    <img src="<?= $image->getImageUrl(202, 132, 2); ?>" alt="" class="product-gallery__nav-img">
                                                </a>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                <?//endif;?>
                                <?/*
                                <div class="product-gallery__img-wrap">
                                    <a href="<?= $product->getImageUrl(); ?>" data-index="0" data-fancybox="product">
                                        <img src="<?= StoreImage::product($product, $store->productImageWidth, $store->productImageHeight, $store->productImageCrop); ?>" class="product-gallery__main-img" alt="<?= CHtml::encode($product->getImageAlt()); ?>" title="<?= CHtml::encode($product->getImageTitle()); ?>" itemprop="image">
                                    </a>
                                </div>
                                */?>
                            </div>
                        </div>
                    </div>

                    <div class="product-detail__params">
                        <table class="product-detail__table">
                            <?$this->renderPartial('//store/product/_simple_attributes_main', ['product' => $product])?>
                            <?$this->renderPartial('//store/product/_simple_attributes_advanced', ['product' => $product])?>
                        </table>
                    </div>
                </div>
				<div class="product-detail__ipoteka">
					<div class="product-detail__title">Ипотека с нами</div>

					<div class="product-detail__text">
						Этот дом <?= Helper::getFormatPrice($product->square)?> м<sup>2</sup> на участке <?= Helper::getFormatPrice($product->square_area)?> соток возможно приобрести в ипотеку.<br/>
						Помощь в получении ипотеки при покупке дома оказывается бесплатно.
					</div>

					<div class="product-item__calc">
						<span class="product-calc__button" data-price="<?=$product->getResultPrice()?>">
							Рассчитать ипотеку
						</span>
					</div>
				</div>


				<?if($reviews):?>
					<div class="product-detail__review">
						<div class="product-detail__title">Отзывы о застройщике</div>
						<div class="product-detail__value">
							<?=$reviews?>
						</div>
					</div>
				<?endif?>

				<?if($placeMap):?>
					<div class="product-detail__infrastructure">
						<div class="product-detail__title">Инфраструктура наших поселков</div>
						<div class="product-detail__value">
							<?=$placeMap?>
						</div>
					</div>
				<?endif;?>
			</div>
		</div>

		<div class="product-page__right">
			<div class="product-detail__price">
				<div class="price-title params-title">Стоимость</div>
				<div class="result-price-value"><?= $productObject->getFormatPrice() ?> <span class="currency"><?=$currency;?></span></div>
			</div>

			<div class="product-detail__params">
				<div class="params-title">Параметры</div>
				<div class="params-items">
                    <div class="params-item">
                        <div class="params-item__title">Площадь:</div>
                        <div class="params-item__value"> <?= Helper::getFormatPrice($product->square)?> м<sup>2</sup></div>
                    </div>
					<div class="params-item">
                        <div class="params-item__title">Этажность: </div>
                        <div class="params-item__value"><?=DictionaryData::getFloor($product->floor_id)?></div>
                    </div>
					<div class="params-item">
                        <div class="params-item__title">Готовность: </div>
                        <div class="params-item__value"><?=DictionaryData::getReadiness($product)?></div>
                    </div>
					<div class="params-item">
                        <div class="params-item__title">Участок: </div>
                        <div class="params-item__value"><?= Helper::getFormatPrice($product->square_area)?> соток</div>
                    </div>
					<?$this->renderPartial('//store/product/_simple_attributes_right', ['product' => $product])?>
				</div>
			</div>

			<div class="product-detail__widgets">
				<?$this->widget('compare', ['product' => $product])?>
				<?$this->widget('favorite', ['product' => $product])?>
				<?$this->widget('content', ['code' => 'share', 'view' => 'share'])?>
			</div>

			<div class="product-detail__order">
				<span class="buy-one-click__button" data-text="<?=CHtml::encode($product->getNameInList());?>">
					Записаться на&nbsp;просмотр
				</span>
			</div>
		</div>
	</div>
</div>

<?php
$this->renderPartial('_slider_top', ['product' => $product]);
?>

<script type="text/javascript">
	$('.fotorama').fotorama({
		clicktransition: 'dissolve',
		width: '100%',
		//ratio: 1,
		hash: true,
		maxheight: '560px',
		nav: 'thumbs',
		margin: 20,
		//shuffle: true,
		thumbmargin: 2,
		thumbwidth: 202,
		thumbheight: 132,
		allowfullscreen: true,
		keyboard: {space: true},
		shadows: false,
		click: false,
		//fit: 'none'
	});
	$('.fotorama__img').off('click');
	$('.fotorama').on('fotorama:ready', function (e, fotorama) {
		$('.fotorama__wrap').prepend($('.forotama-top-bar-wrapper').html());

		$('body').on('click', '.fotorama__stage .fotorama__img', function(e){
			e.preventDefault();
			e.stopPropagation();
			fotorama.requestFullScreen();
			return false;
		});

		$('body').on('click', '.close_fotorama', function(){
			fotorama.cancelFullScreen();
		});
	});

    $(".reviews__items").slick({
        infinite: true,
        slidesToShow: 1,
        dots: true
    });
</script>