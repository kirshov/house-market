<?php
/**
 * @var \webforma\components\controllers\FrontController $this
 */
$storeModule = Yii::app()->getModule('store');
$title = Yii::t('StoreModule.store', $type);
$typeName = Helper::mb_ucfirst($type);
$this->title = $storeModule->{'meta'.$typeName.'Title'} ?: Helper::prepareSeoTitle($title);

$this->description = $storeModule->{'meta'.$typeName.'Description'};
$this->keywords =  $storeModule->{'meta'.$typeName.'Keywords'};

$this->breadcrumbs = [
	$title
];

$this->layout = '//layouts/onecol';
$this->setTemplateVar('bodyClass', 'catalog-page');

$showBy = $this->renderPartial('/category/_show_by', null, true);

$this->setTitle($title);

$this->setTemplateVar([
	'descriptionsAfterPos' => $this->widget('content', ['code' => $type.'-text'], true),
]);
?>

<div class="catalog">
	<?$this->renderPartial('/category/_product_list', [
		'dataProvider' => $dataProvider,
		'type' => $type,
	]);?>
</div>
