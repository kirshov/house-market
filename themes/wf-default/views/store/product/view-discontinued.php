<?php

/**
 * @var Product $product
 * @var ProductVariant $variant
 * @var \webforma\components\controllers\FrontController $this
 */

$this->layout = '//layouts/onecol';

$this->title = $product->getMetaTitle();
$this->description = $product->getMetaDescription();
$this->keywords = $product->getMetaKeywords();

$this->canonical = $product->getMetaCanonical();
$this->setTitle(false);
if($product->image){
	$this->setMetaImage($product->getImageUrl(250, 250, false));
	$this->setMetaSrcImage($product->getImageUrl(180, 120, false));
}
$this->setTemplateVar(['bodyClass', 'product-page']);
$this->setTemplateVar('bodyClass', 'page-onepos');
$producer = $product->getProducer();
if($producer && $producer->retargeting_code){
	$this->setTemplateVar(['retargeting-code', $producer->retargeting_code]);
}

$this->breadcrumbs = array_merge(
	$product->category ? $product->category->getBreadcrumbs(true) : [],
	[CHtml::encode($product->name)]
);

$marker = $product->getMarker();
$webforma = Yii::app()->getModule('webforma');
$store = Yii::app()->getModule('store');
$currency = Yii::t("StoreModule.store", $store->currency);
$minSumCart = Yii::app()->getModule('store')->minCost;
$productObject = $variant !== null ? $variant : $product;
?>
	<div class="product-page" itemscope itemtype="http://schema.org/Product">
		<div class="wrapper">
			<div class="product-detail product-wrapper" data-id="<?=$product->id?>">
				<div class="product-detail-top">
					<div class="product-gallery">
						<div class="product-gallery__body">
							<?if($product->getImages()):?>
								<div class="product-images">
									<a href="<?= $product->getImageUrl(); ?>" data-id="0" data-fancybox="product">
										<img src="<?= StoreImage::product($product, 76, 76, false); ?>" class="product-gallery__main-img" alt="<?= CHtml::encode($product->getImageAlt()); ?>" title="<?= CHtml::encode($product->getImageTitle()); ?>" itemprop="image">
									</a>

									<?php foreach ($product->getImages() as $key => $image): ?>
										<a href="<?= $image->getImageUrl(); ?>" data-id="<?=$image->id?>" data-fancybox="product">
											<img src="<?= $image->getImageUrl(76, 76, false); ?>" alt="" class="product-gallery__nav-img">
										</a>
									<?php endforeach; ?>
								</div>
							<?endif;?>

							<div class="product-gallery__img-wrap">
								<a href="<?= $product->getImageUrl(); ?>" data-id="0" data-fancybox="product">
									<img src="<?= StoreImage::product($product, 460, 460, false); ?>" class="product-gallery__main-img" alt="<?= CHtml::encode($product->getImageAlt()); ?>" title="<?= CHtml::encode($product->getImageTitle()); ?>" itemprop="image">
								</a>
							</div>
						</div>
					</div>

					<form action="<?= Yii::app()->createUrl('cart/cart/add'); ?>" method="post">
						<div class="product-detail-info">
							<div class="product-detail-attr">
								<div class="product-detail-producer-sku">
									<?if($product->getProducer()):?>
										<div class="product-detail__producer">
											<a href="<?=Yii::app()->createUrl('/store/producer/view', ['slug' => $product->getProducer()->slug])?>" title="<?=Yii::t('StoreModule.store', 'Producer');?>">
												<?if($product->getProducer() && $product->getProducer()->image):?>
													<span itemprop="brand" class="hidden"><?= $product->getProducerName() ?></span>
													<img src="<?=$product->getProducer()->getImageUrl(120, 40, false)?>" class="product-detail__producer-image" alt="<?=$product->getProducerName();?>" title="<?=$product->getProducerName();?>"/>
												<?else:?>
													<span itemprop="brand"><?=$product->getProducerName();?></span>
												<?endif;?>
											</a>
										</div>
									<?endif;?>

									<div class="product-detail-sku-availability">
										<?if($product->sku):?>
											<div class="product-detail__sku">
												Артикул: <span itemprop="sku"><?=$product->sku;?></span>
												<meta itemprop="mpn" content="<?=$product->sku;?>">
											</div>
										<?endif;?>

										<div class="product-detail__availability">
											<link itemprop="availability" href="http://schema.org/OutOfStock"/>
										</div>
									</div>
								</div>

								<h1 itemprop="name"><?=$product->getTitle();?></h1>

								<div class="product-detail-form-question">
									<div class="product-detail__buy">
										<input type="hidden" name="Product[id]" value="<?= $product->id; ?>"/>
										<?= CHtml::hiddenField(Yii::app()->getRequest()->csrfTokenName,Yii::app()->getRequest()->csrfToken); ?>

										<div class="product-detail__price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
											<meta itemprop="priceCurrency" content="<?=Yii::app()->getModule('store')->currency?>" />
											<span itemprop="price" class="hidden">0</span>
											<div class="product-item__not-available">Снят с производства!</div>
										</div>
									</div>

									<div class="product-detail__question">
										Остались вопросы?
										<span class="modal-show" data-modal="popup-feedback">Напишите нам</span>
									</div>
								</div>
							</div>

							<?if($product->short_description):?>
								<div class="product-detail__short_description">
									<?=$product->short_description;?>
								</div>
							<?endif;?>
						</div>
					</form>
				</div>
			</div>

			<div class="product-detail__description-area">
				<?if($this->getModule()->productTabs):
					if($this->mobileDetect(true, false)){
						$this->renderPartial('_tabs_mobile', ['product' => $product]);
					} else {
						$this->renderPartial('_tabs', ['product' => $product]);
					}
				else:?>
					<?if($product->description):?>
						<?if($store->notProductIndexDescription && !$product->force_index):?>
							<!--noindex-->
						<?endif;?>

						<div class="product-description" itemprop="description">
							<?= $product->description ?>
						</div>

						<?if($store->notProductIndexDescription && !$product->force_index):?>
							<!--/noindex-->
						<?endif;?>
					<?endif;?>
				<?endif;?>
			</div>
		</div>
	</div>

<?php
$this->widget('similarProduct', [
	'product' => $product,
	'limit' => 10,
	'code' => 'similar',
	'options' => [
		'isSlider' => true,
		'countBySlider' => 1,
		'wrapper-class' => 'similar-box-wrapper',
		'title' => 'Рекомендуем вам',
	]
]);
?>