<?php
/**
 * @var $dataProvider CArrayDataProvider
 * @var StoreCategory[] $data
 * @var StoreModule $module
 */
//$this->layout = '//layouts/inner-catalog';
$module = Yii::app()->getModule('store');

$title = $module->title;
if(!$title){
	$title = Yii::t('StoreModule.store', 'Catalog');
}
$this->breadcrumbs = [$title];

$this->title = $title;//$module->metaTitle ?: $title;
$this->description = $module->metaDescription;
$this->keywords = $module->metaKeyWords;
$this->setTitle($title);
$this->setTemplateVar('bodyClass', 'catalog-page');

$this->setTemplateVar('filter', $this->renderPartial('_filter', ['category' => $category], true));
?>

<div class="page page-catalog page-store">
	<?if($module->storeTopPageText):?>
		<div class="catalog-top-description">
			<?=$module->storeTopPageText?>
		</div>
	<?endif;?>

	<div class="lh_store">
		<?$this->renderPartial('_product_list', ['dataProvider' => $dataProvider, 'showActionBar' => true]);?>
    </div>

	<?if($module->storePageText):?>
		<div class="catalog-bottom-description">
			<?=$module->storePageText?>
		</div>
	<?endif;?>
</div>
