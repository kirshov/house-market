<?php
/**
 * @var CActiveDataProvider $dataProvider
 */

$this->widget(
	'webforma.widgets.WListView', [
		'dataProvider' => $dataProvider,
		'itemView' => '//store/category/_item',
		'template' => '{items}',
		'summaryText' => '',
		'itemsCssClass' => 'catalog__category-items',
	]
);
