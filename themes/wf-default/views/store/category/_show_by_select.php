<?
$currentPerPage = Yii::app()->getModule('store')->getItemsPerPage();
$itemsPerPage = Yii::app()->getModule('store')->itemsPerPage;
$pagesList = [];
for($i = 1; $i <= 4; $i++){
	$value = $itemsPerPage * $i;
	$pagesList[$value] = $value;
}
$paramsExclude = ['path', 'slug', 'show-by'];
list($action) = explode('?', Yii::app()->getRequest()->getRequestUri());


?>
<form action="<?=$action?>">
	<div class="show_by_title">Выводить по: </div>
	<?=CHtml::dropDownList('show-by', $currentPerPage, $pagesList, ['onChange' => 'this.form.submit()', 'class' => 'styler'])?>
	шт.
	<?echo Helper::getInputFieldsFromArray($_GET, $paramsExclude);?>
</form>
