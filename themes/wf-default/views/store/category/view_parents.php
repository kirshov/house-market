<?php
/**
 * @var CategoryController $this
 * @var StoreCategory $category
 * @var CActiveDataProvider $dataProvider
 */

$cs = Yii::app()->getClientScript();
$cs->registerCoreScript('jquery.ui');
$cs->registerCssFile($cs->getCoreScriptUrl(). '/jui/css/base/jquery-ui.css');

$this->title =  $category->getMetaTitle();
$this->description = $category->getMetaDescription();
$this->keywords =  $category->getMetaKeywords();
$this->canonical = $category->getMetaCanonical();

if($category->image){
	$this->setMetaImage($category->getImageUrl(250, 250, false));
	$this->setMetaSrcImage($category->getImageUrl(180, 120, false));
}

$this->breadcrumbs = $category->getBreadcrumbs();
$showBy = $this->renderPartial('_show_by', null, true);


$this->setTemplateVar('bodyClass', 'catalog-parent-page');
$this->setTitle($category->getTitle());
$this->layout = '//layouts/onecol';

$children = $category->children;
?>
<div class="catalog">
	<?$this->widget('storeCategory', ['onlyDepth' => true])?>
	<div class="catalog-parent-content">
		<div class="catalog-list">

			<div class="catalog-subitems">
				<?if($children):?>
					<?foreach ($children as $child):?>
						<div class="catalog-subitems__item">
							<div class="catalog-subitems__item-image">
								<?if($child->image):?>
									<img src="<?=$child->getImageUrl(158, 126)?>">
								<?endif;?>
							</div>
							<div class="catalog-subitems__item-info">
								<div class="catalog-subitems__item-title">
									<a href="<?=Yii::app()->createUrl('store/category/view', ['path' => $child->getPath()])?>"><?=$child->getTitle()?></a>
								</div>
								<?$products = Product::getProductsByCategory($child->id);?>
								<?if($products):?>
									<div class="catalog-subitems__item-list">
										<ul>
											<?foreach ($products as $product):?>
												<li><a href="<?=ProductHelper::getUrl($product)?>"><?=$product->getName()?></a></li>
											<?endforeach;?>
										</ul>
									</div>
								<?endif;?>
							</div>
						</div>
					<?endforeach;?>
				<?endif;?>
			</div>
		</div>

		<div class="catalog-products-widget">
			<div class="h3">Новинки</div>
			<?$this->widget('products', ['type' => 'new', 'limit' => 4])?>
		</div>
	</div>

	<?if(!$isGetQuery && $category->short_description):?>
		<div class="catalog-bottom-description">
			<?=$category->short_description?>
		</div>
	<?endif;?>
</div>
