<?php
/**
 * @var CActiveDataProvider $dataProvider
 */
$showBy = $this->renderPartial('//store/category/_show_by', null, true);
//$view_type = $this->renderPartial('//store/category/_view_type', null, true);
//$favorites = $this->widget('favoriteWidget', [], true);
//$compares = $this->widget('compareWidget', [], true);

$productActions = '';
if($showActionBar) {
	$productActions = '<div class="catalog-controls">
	    <div class="catalog-controls__sorter">
	        {sorter}
	    </div>
	</div>';
}
$template = $productActions.'
{items}
<div class="bottom_control">
{pager}
'.$showBy.'</div>';

if($dataProvider->getItemCount() == 0){
    $template = '{items}';
}
list($resetLink) = explode('?', Yii::app()->getRequest()->getUrl());
$this->widget(
	'webforma.widgets.WListView', [
		'dataProvider' => $dataProvider,
		'itemView' => '//store/product/_item',
		'template' => $template,
		'summaryText' => '',
		'itemsCssClass' => 'catalog__product-items',
		'sorterHeader' => '<div class="sorter__description">Сортировать по:</div>',
		'sortableAttributes' => [
			'price',
			'square',
			//'create_time',
			//'name',
		],
		'sorterFooter' => '<div class="reset-filter__wrapper"><a href="'.$resetLink.'" class="reset-filter">Сбросить фильтр</a></div>',
		'pagerCssClass' => 'catalog__pagination',
		'pager' => [
			'header' => '',
			'prevPageLabel' => '&larr;',
			'nextPageLabel' => '&rarr;',
			'firstPageLabel' => '&laquo;',
			'lastPageLabel' => '&raquo;',
			'htmlOptions' => [
				'class' => 'pagination'
			]
		],
		'emptyText' => $emptyText ?: null,
		'viewData' => [
			'markerTitles' => Product::getMarkerTitles(),
			'in_favorite' => $in_favorite,
		]
	]
);
