<?
/**
 * @var \webforma\components\controllers\FrontController $this
 */
$currentType = Yii::app()->getModule('store')->getViewType();
$this->setTemplateVar('bodyClass', 'view-type-'.$currentType, true, ' ');
$requestUri = Yii::app()->getRequest()->getRequestUri();

list($address, $params) = explode('?', $requestUri);

parse_str($params, $linkData);
unset($linkData['view-type']);

if($linkData){
	$delimiter = '&';
	$link = $address;
	if($linkData){
		$link .= '?'.http_build_query($linkData);
	}
} else {
	$delimiter = '?';
	$link = $address;
}

$active[$currentType] = ' active';
foreach (['block', 'list'] as $_item){
	$href[$_item] = ' href="'.$link.$delimiter.'view-type='.$_item.'"';
	$dataHref[$_item] = ' data-href="'.$link.$delimiter.'view-type='.$_item.'"';
}
$href[$currentType] = '';
?>

<a <?=$href['block'].$dataHref['block']?> class="catalog-view-type view-block<?=$active['block']?>" data-type="block" title="Выодить блоками">Блоками</a>
<a <?=$href['list'].$dataHref['list']?> class="catalog-view-type view-list<?=$active['list']?>" data-type="list" title="Выводить списком">Списком</a>
