<?php
/**
 * @var CategoryController $this
 * @var StoreCategory $category
 * @var CActiveDataProvider $dataProvider
 * @var CActiveDataProvider $categoryDataProvider
 */

$cs = Yii::app()->getClientScript();
$cs->registerCoreScript('jquery.ui');
$cs->registerCssFile($cs->getCoreScriptUrl(). '/jui/css/base/jquery-ui.css');

$this->title =  $category->getMetaTitle();
$this->description = $category->getMetaDescription();
$this->keywords =  $category->getMetaKeywords();
$this->canonical = $category->getMetaCanonical();

if($category->image){
	$this->setMetaImage($category->getImageUrl(250, 250, false));
	$this->setMetaSrcImage($category->getImageUrl(180, 120, false));
}

$this->breadcrumbs = CMap::mergeArray(
	//['Каталог' => '/store/'],
	[],
	$category->getBreadcrumbs()
);
$showBy = $this->renderPartial('_show_by', null, true);

$isGetQuery = Yii::app()->getModule('store')->isGetFilterParams();

$this->setTemplateVar('bodyClass', 'catalog-page');
$this->setTitle($category->getTitle());

if(!$category->not_filter){
	$this->setTemplateVar('filter', $this->renderPartial('_filter', ['category' => $category], true));
}
?>
<div class="catalog">
	<?if(!$isGetQuery && $category->description):?>
		<div class="catalog-top-description">
			<?=$category->description?>
		</div>
	<?endif;?>

	<?$this->widget('productGroups', ['category' => $category->id])?>

	<?$this->renderPartial('_product_list', ['dataProvider' => $dataProvider, 'showActionBar' => !$category->not_filter]);?>

	<?if(!$isGetQuery && $category->short_description):?>
		<div class="catalog-bottom-description">
			<?=$category->short_description?>
		</div>
	<?endif;?>
</div>
