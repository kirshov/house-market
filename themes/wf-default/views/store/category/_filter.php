<? /**
 * @var StoreCategory $category
 */

Yii::import('application.modules.dictionary.models.*');

$attributeMaterial = Attribute::model()->findByPk(2);
$attributeType = Attribute::model()->findByPk(16);

list($resetLink) = explode('?', Yii::app()->getRequest()->getUrl());
?>
<div class="catalog-filter">
	<div class="view-type">
		<?=$this->renderPartial('_view_type');?>
	</div>

	<form class="store-filter" name="store-filter" method="get">
		<div class="filter-block-1">
			<div class="filter-block-item">
				<div class="filter-block__title">Цена за домовладение</div>
				<div class="filter-block__body">
					<div class="filter-block__range">
						<span class="filter-input filter-input_range">
							<span class="filter-input__box">
								<?= CHtml::textField('price[from]', Yii::app()->attributesFilter->getMainSearchParamsValue('price', 'from', Yii::app()->getRequest()), ['class' => 'filter-input__control', 'placeholder' => 'от ']); ?>
							</span>
						</span>

						<span class="filter-input filter-input_range">
							<span class="filter-input__box">
								<?= CHtml::textField('price[to]', Yii::app()->attributesFilter->getMainSearchParamsValue('price', 'to', Yii::app()->getRequest()),['class' => 'filter-input__control', 'placeholder' => 'до ']); ?>
							</span>
						</span>
					</div>
				</div>
			</div>

			<div class="filter-block-item">
				<div class="filter-block__title">Площадь дома</div>
				<div class="filter-block__body">
					<div class="filter-block__range">
						<span class="filter-input filter-input_range">
							<span class="filter-input__box">
								<?= CHtml::textField('square[from]', Yii::app()->attributesFilter->getMainSearchParamsValue('square', 'from', Yii::app()->getRequest()), ['class' => 'filter-input__control', 'placeholder' => 'от ']); ?>
							</span>
						</span>

						<span class="filter-input filter-input_range">
							<span class="filter-input__box">
								<?= CHtml::textField('square[to]', Yii::app()->attributesFilter->getMainSearchParamsValue('square', 'to', Yii::app()->getRequest()),['class' => 'filter-input__control', 'placeholder' => 'до ']); ?>
							</span>
						</span>
					</div>
				</div>
			</div>
		</div>

		<div class="filter-block-2">
			<div class="filter-block-item">
				<div class="filter-block__title">Этажность</div>
				<div class="filter-block__body">
					<div class="filter-block__select">
						<?=CHtml::dropDownList('etazhnost',
							Yii::app()->getRequest()->getParam('etazhnost'),
							Helper::mergeArray(['' => ''], DictionaryGroup::getHtmlList('etazhnost')),
							['class' => 'filter-select__control']
						)?>
					</div>
				</div>
			</div>

			<div class="filter-block-item">
				<div class="filter-block__title">Материал стен</div>
				<div class="filter-block__body">
					<div class="filter-block__select">
						<?=CHtml::dropDownList($attributeMaterial->name,
							Yii::app()->getRequest()->getParam($attributeMaterial->name),
							Helper::mergeArray(['' => ''], CHtml::listData($attributeMaterial->getOptionsList(), 'id', 'value')),
							['class' => 'filter-select__control']
						)?>
					</div>
				</div>
			</div>
		</div>

		<div class="filter-block-3">
			<div class="filter-block-item">
				<div class="filter-block__title">Готовность</div>
				<div class="filter-block__body">
					<div class="filter-block__select">
						<?=CHtml::dropDownList('gotovnost',
							Yii::app()->getRequest()->getParam('gotovnost'),
							Helper::mergeArray(['' => ''], DictionaryGroup::getHtmlList('gotovnost')),
							['class' => 'filter-select__control']
						)?>
					</div>
				</div>
			</div>

			<div class="filter-block-item">
				<div class="filter-block__title">Внутренняя отделка</div>
				<div class="filter-block__body">
					<div class="filter-block__select">
						<?=CHtml::dropDownList($attributeType->name,
							Yii::app()->getRequest()->getParam($attributeType->name),
							Helper::mergeArray(['' => ''], CHtml::listData($attributeType->getOptionsList(), 'id', 'value')),
							['class' => 'filter-select__control']
						)?>
					</div>
				</div>
			</div>
		</div>

		<div class="filter-block-4">
			<div class="filter-block-item">
				<div class="filter-block__title">Поселок</div>
				<div class="filter-block__body">
					<div class="filter-block__select">
						<?=CHtml::dropDownList('poselki[]',
							Yii::app()->getRequest()->getParam('poselki'),
							DictionaryGroup::getHtmlList('poselki'),
							[
								'class' => 'multi-select filter-select__control',
								'data-placeholder' => '[все]',
								'multiple' => true,
								'size' => 1,
								'options' => DictionaryGroup::getVillageOptionsList('poselki'),
							]
						)?>
					</div>
				</div>
			</div>
            <div class="bottom_side">
                <div class="catalog-filter__button">
                    <input type="submit" value="Найти" class="btn do-filter"/>
                </div>
            </div>
		</div>

	</form>
</div>

<?
Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/multiple-select.min.js', CClientScript::POS_HEAD);
$script = "$(document).ready(function(){
		if($('select.multi-select').length){
            $.each($('select.multi-select'), function () {
                $(this).multipleSelect({
                    placeholder: $(this).data('placeholder'),
                    filter: false,
                    selectAll: false,
                    allSelected: 'Выбраны все',
                    countSelected: $(this).data('placeholder') + ': # из %',
                    noMatchesFound: 'Ничего не найдено',
                });
            });
		}
	});";

Yii::app()->getClientScript()->registerScript('multi-select', $script, CClientScript::POS_END);