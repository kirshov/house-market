<?php
/**
 * @var CategoryController $this
 * @var StoreCategory $category
 * @var CActiveDataProvider $dataProvider
 */

$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
$cs = Yii::app()->getClientScript();
$cs->registerCoreScript('jquery.ui');
$cs->registerCssFile($cs->getCoreScriptUrl(). '/jui/css/base/jquery-ui.css');

$this->title =  $category->getMetaTitle();
$this->description = $category->getMetaDescription();
$this->keywords =  $category->getMetaKeywords();
$this->canonical = $category->getMetaCanonical();

$this->breadcrumbs = [Yii::t("StoreModule.store", "Catalog") => ['/store/category/index']];

$showBy = $this->renderPartial('_show_by', null, true);

$this->setTemplateVar([
	/*'brandInCats' => $this->widget('producers', [
			'view' => 'main',
			'category' => $category,
			'title' => sprintf('Бренды категории «%s»', $category->name)
		], true),*/
	'descriptionsAfterPos' => $category->short_description,
	'categorySelectId' => $category->id,
]);
$this->setTitle($category->name);
?>
<div class="catalog">
	<?$this->renderPartial('_product_list', ['dataProvider' => $dataProvider]);?>
</div>