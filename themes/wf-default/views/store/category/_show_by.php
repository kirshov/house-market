<?
$currentPerPage = Yii::app()->getModule('store')->getItemsPerPage();
$itemsPerPage = Yii::app()->getModule('store')->itemsPerPage;
$requestUri = Yii::app()->getRequest()->getRequestUri();

list($address, $params) = explode('?', $requestUri);

parse_str($params, $linkData);
unset($linkData['show-by'], $linkData['page']);

if($linkData){
	$delimiter = '&';
	$link = $address;
	if($linkData){
		$link .= '?'.http_build_query($linkData);
	}
} else {
	$delimiter = '?';
	$link = $address;
}

?>
<div class="show_by_title-wrap">
    <div class="show_by_title">
    Домов на странице:
	<?
	for($i = 1; $i <= 3; $i++){
		$value = $itemsPerPage * $i;
		$active = $currentPerPage == $value ? ' active' : '';
		echo CHtml::link($value, $link.$delimiter.'show-by='.$value, ['class' => 'showBy__item'.$active]);
	}
	?>
    </div>
</div>