<?
/**
 * @var StoreCategory $data;
 */
$url = Yii::app()->createUrl('/store/category/view', ['path' => $data->getPath()]);
?>
<div class="catalog__category-item">
    <?php if ($data->image): ?>
        <div class="catalog__category-item-img">
            <a href="<?= $url; ?>">
                <img src="<?= $data->getImageUrl($store->categoryListImageWidth, $store->categoryListImageHeight, $store->categoryListImageCrop); ?>" alt="<?= $data->getImageAlt(); ?>" title="<?= $data->getTitle(); ?>"/>
            </a>
        </div>
    <?php endif; ?>
    <div class="catalog__category-item-title">
        <a href="<?= $url; ?>"><?= CHtml::encode($data->name); ?></a>
    </div>
</div>