<?php
/**
 * @var \webforma\components\controllers\FrontController $this
 */

$this->layout = '//layouts/email/onecolumn';

?>

<h2 style="font-weight:normal; text-align: center;">
	Заказ №<?=$order->getNumberForUser()?> успешно оплачен!
</h2>

<br/>
<p style="text-align: center;">
Вы всегда можете посмотреть состав заказа по ссылке:<br>
<?= CHtml::link(
    Yii::app()->createAbsoluteUrl('/order/order/view', ['url' => $order->url]),
    Yii::app()->createAbsoluteUrl('/order/order/view', ['url' => $order->url])
); ?>
</p>
<br/>
