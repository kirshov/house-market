<?php
Yii::setPathOfAlias('application', __DIR__ . '/../');
Yii::setPathOfAlias('webforma', __DIR__ . '/../modules/webforma/');
Yii::setPathOfAlias('vendor', __DIR__ . '/../../vendor/');
Yii::setPathOfAlias('themes', __DIR__ . '/../../themes/');

return [
    // У вас этот путь может отличаться. Можно подсмотреть в config/main.php.
    'basePath' => dirname(__DIR__),
    'name' => 'Cron',
    'preload' => ['log'],
    'commandMap' => [
        'migrate' => [
            'class' => 'vendor.yiiext.migrate-command.EMigrateCommand',
            'migrationPath' => 'application.modules.webforma.install.migrations',
            'migrationTable' => '{{migrations}}',
            'applicationModuleName' => 'webforma',
            'migrationSubPath' => 'install.migrations',
            'connectionID' => 'db',
            'templateFile' => 'application.modules.webforma.migrations.migration-template',
        ],
    ],
    'import' => [
        'application.commands.*',
        'application.components.*',
        'application.models.*',
    ],
    'aliases' => [
        'webroot' => dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'public',
    ],
    // Перенаправляем журнал для cron-а в отдельные файлы
    'components' => [
        'uploadManager' => ['class' => 'webforma\components\UploadManager'],
        'moduleManager' => ['class' => 'webforma\components\ModuleManager'],
        'configManager' => ['class' => 'webforma\components\ConfigManager'],
        // Работа с миграциями, обновление БД модулей
        'migrator' => ['class' => 'webforma\components\Migrator'],
        'themeManager' => [
            'class' => 'CThemeManager',
            'basePath' => dirname(__DIR__) . '/../themes',
            'themeClass' => 'webforma\components\Theme'
        ],
        'request' => [
            'class' => 'webforma\components\HttpRequest',
            'enableCsrfValidation' => true,
            'csrfCookie' => ['httpOnly' => true],
            'csrfTokenName' => 'WEBFORMA_TOKEN',
            'enableCookieValidation' => true,
            // подробнее: http://www.yiiframework.com/doc/guide/1.1/ru/topics.security#sec-4
        ],
        // компонент для отправки почты
        'mail' => [
            'class' => 'webforma\components\Mail',
        ],
        'log' => [
            'class' => 'CLogRouter',
            'routes' => [
                [
                    'class' => 'CFileLogRoute',
                    'logFile' => 'cron.log',
                    'levels' => 'error, warning, info',
                ],
                [
                    'class' => 'CFileLogRoute',
                    'logFile' => 'cron_trace.log',
                    'levels' => 'trace',
                ],
            ],
        ],
	    'cache' => [
		    'class' => 'CFileCache',
	    ],
        // параметры подключения к базе данных, подробнее http://www.yiiframework.ru/doc/guide/ru/database.overview
        'db' => file_exists(__DIR__ . '/db.php') ? require_once __DIR__ . '/db.php' : []
    ],
	'modules' => [
		'webforma' => [
			'class' => 'application.modules.webforma.WebformaModule',
			'cache' => true,
		],
	],
];
