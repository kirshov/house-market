<?php
return [
    'module' => [
        'class' => 'application.modules.articles.ArticlesModule',
    ],
    'import' => [
        'application.modules.articles.events.*',
        'application.modules.articles.listeners.*',
        'application.modules.articles.helpers.*',
    ],
    'component' => [
        'eventManager' => [
            'class' => 'webforma\components\EventManager',
            'events' => [
                'sitemap.before.generate' => [
                    ['\ArticlesSitemapGeneratorListener', 'onGenerate']
                ],
                'articles.after.save' => [
                    ['\ArticlesListener', 'onAfterSave']
                ],
                'articles.after.delete' => [
                    ['\ArticlesListener', 'onAfterDelete']
                ],

            ]
        ]
    ],
    'params' => [
        'widgets' => [
            'articles' => 'application.modules.articles.widgets.LastArticlesWidget',
        ]
    ],
    'rules' => [
        '/articles/' => 'articles/articles/index',
        //'/articles/categories' => 'articles/articlesCategory/index',
        [
            'articles/articles/view',
            'pattern' => '/articles/<slug>',
            //'urlSuffix' => '.html'
        ],
        //'/articles/<slug>' => 'articles/articlesCategory/view',
    ],
];
