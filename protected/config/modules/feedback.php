<?php
/**
 * Файл конфигурации модуля
 **/
return [
    'module' => [
        'class' => 'application.modules.feedback.FeedbackModule',
        'panelWidgets' => [
            'application.modules.feedback.widgets.PanelFeedbackStatWidget' => [
            	'position' => 10,
            ],
        ],
    ],
    'import' => [
        'application.modules.webforma.WebformaModule',
        'application.modules.feedback.models.*',
        'application.modules.feedback.components.*',
    ],
    'component' => [
        'feedback' => [
            'class' => 'application.modules.feedback.components.FeedbackService'
        ],
	    'feedbackNotifyService' => [
		    'class' => 'application.modules.feedback.components.FeedbackNotifyService',
	    ],
	    'feedbackMailMessage' => [
		    'class' => 'application.modules.feedback.components.FeedbackMailMessage',
	    ],
    ],
	'params' => [
		'widgets' => [
			'feedback' => 'application.modules.feedback.widgets.FeedbackWidget'
		]
	],
    'rules' => [
	    '/send-feedback' => '/feedback/contact/index',
        '/feedback/contact/captcha/refresh/<v>' => '/feedback/contact/captcha/refresh',
        '/feedback/contact/captcha/<v>' => '/feedback/contact/captcha/'
    ],
];
