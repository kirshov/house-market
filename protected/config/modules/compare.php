<?php
return [
	'module' => [
		'class' => 'application.modules.compare.CompareModule',
	],
	'import' => [
		'application.modules.compare.components.CompareService',
		'application.modules.compare.listeners.TemplateCompareListener',
		'application.modules.compare.events.*'
	],
	'component' => [
		'compare' => [
			'class' => 'application.modules.compare.components.CompareService',
		],
		'compareDb' => [
			'class' => 'application.modules.compare.components.CompareDbService',
		],
		'eventManager'   => [
			'class'  => 'webforma\components\EventManager',
			'events' => [
				'template.head.start' => [
					['TemplateCompareListener', 'js']
				]
			]
		]
	],
	'params' => [
		'widgets' => [
			'compare' => 'application.modules.compare.widgets.CompareControl',
			'compareWidget' => 'application.modules.compare.widgets.CompareWidget',
		]
	],
	'rules' => [
		'/compare/add' => '/compare/compare/add',
		'/compare/remove' => '/compare/compare/remove',
		'/compare' => '/compare/compare/index',
	],
];
