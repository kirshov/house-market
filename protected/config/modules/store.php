<?php

return [
    'module' => [
        'class' => 'application.modules.store.StoreModule',
    ],
    'import' => [
        'application.modules.store.models.*',
        'application.modules.store.events.*',
        'application.modules.store.listeners.*',
        'application.modules.store.components.helpers.*',
    ],
    'component' => [
        'eventManager' => [
            'class' => 'webforma\components\EventManager',
            'events' => [
                'sitemap.before.generate' => [
                    ['\StoreSitemapGeneratorListener', 'onGenerate']
                ],
                'category.after.save' => [
                    ['\StoreCategoryListener', 'onAfterSave']
                ],
                'category.after.delete' => [
                    ['\StoreCategoryListener', 'onAfterDelete']
                ],
                'store.change' => [
                    ['\StoreListener', 'onChange']
                ],
				'store.product.open' => [
					['\StoreListener', 'onOpening']
				],
            ]
        ],
        'money' => [
            'class' => 'application.modules.store.components.Money',
        ],
        'productRepository' => [
            'class' => 'application.modules.store.components.repository.ProductRepository'
        ],
        'producerRepository' => [
            'class' => 'application.modules.store.components.repository.ProducerRepository'
        ],
        'categoryRepository' => [
            'class' => 'application.modules.store.components.repository.StoreCategoryRepository'
        ],
	    'providerRepository' => [
		    'class' => 'application.modules.store.components.repository.ProviderRepository'
	    ],
	    'attributesRepository' => [
		    'class' => 'application.modules.store.components.repository.AttributesRepository'
	    ],
        'attributesFilter' => [
            'class' => 'application.modules.store.components.AttributeFilter'
        ],
        'session' => [
            'class' => 'CHttpSession',
            'timeout' => 86400,
            'cookieParams' => ['httponly' => true]
        ]
    ],
	'params'    => [
		'widgets'   => [
			'searchProduct' => 'application.modules.store.widgets.SearchProductWidget',
			'storeCategory' => 'application.modules.store.widgets.CategoryWidget',
			'storeMainCategory' => 'application.modules.store.widgets.CategoryMainWidget',
			'products' => 'application.modules.store.widgets.ProductsWidget',
			'producers' => 'application.modules.store.widgets.ProducersWidget',
			'attributesFilter' => 'application.modules.store.widgets.filters.AttributesFilterWidget',
			'priceFilter' => 'application.modules.store.widgets.filters.PriceFilterWidget',
			'producerFilter' => 'application.modules.store.widgets.filters.ProducerFilterWidget',
			'availableFilter' => 'application.modules.store.widgets.filters.AvailableFilterWidget',
			'similarProduct' => 'application.modules.store.widgets.LinkedProductsWidget',
			'productInLine' => 'application.modules.store.widgets.ProductsOnLineWidget',
			'productAdminInfo' => 'application.modules.store.widgets.ProductAdminInfoWidget',
			'productGroups' => 'application.modules.store.widgets.ProductGroupsWidget',
			'storePriceList' => 'application.modules.store.widgets.PriceListWidget',
		],
	],
    'rules' => [
        //'/store' => 'store/category/index',
        //'/price' => 'store/pricelist/index',
       // '/search' => 'store/product/search',
        [
            'class' => 'application.modules.store.components.StoreUrlRule',
        ],
    ],
];
