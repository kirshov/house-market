<?php
/**
 *
 * Файл конфигурации модуля
 */
return [
    'module' => [
        'class' => 'application.modules.help.HelpModule'
    ],
    'import' => [
        'application.modules.help.models.*',
    ],
];
