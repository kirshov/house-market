<?php
/**
 * Файл конфигурации модуля
 **/
return [
    'module'    => [
        'class' => 'application.modules.contentblock.ContentBlockModule',
    ],
    'import'    => [],
    'component' => [],
	'params'    => [
		'widgets'   => [
			'content' => 'application.modules.contentblock.widgets.ContentBlockWidget',
			'contentGroup' => 'application.modules.contentblock.widgets.ContentBlockGroupWidget',
		],
	],
    'rules'     => [],
];
