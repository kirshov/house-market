<?php
return [
    'module' => [
        'class' => 'application.modules.favorite.FavoriteModule',
    ],
    'import' => [
        'application.modules.favorite.components.FavoriteService',
        'application.modules.favorite.listeners.TemplateListener',
        'application.modules.favorite.events.*'
    ],
    'component' => [
        'favorite' => [
            'class' => 'application.modules.favorite.components.FavoriteService',
        ],
	    'favoriteDb' => [
		    'class' => 'application.modules.favorite.components.FavoriteDbService',
	    ],
        'eventManager'   => [
            'class'  => 'webforma\components\EventManager',
            'events' => [
                'template.head.start' => [
                    ['TemplateListener', 'js']
                ]
            ]
        ]
    ],
    'params' => [
        'widgets' => [
            'favorite' => 'application.modules.favorite.widgets.FavoriteControl',
            'favoriteWidget' => 'application.modules.favorite.widgets.FavoriteWidget',
        ]
    ],
    'rules' => [
        '/favorite/add' => '/favorite/favorite/add',
        '/favorite/remove' => '/favorite/favorite/remove',
        '/favorite' => '/favorite/favorite/index',
    ],
];
