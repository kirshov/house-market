<?php
/**
 * Файл настроек для модуля target
 */
return [
    'module'    => [
        'class' => 'application.modules.target.TargetModule',
    ],
    'import'    => [
		'application.modules.target.TargetModule',
		'application.modules.target.listeners.TargetTemplateListener',
	],
	'component' => [
		'eventManager' => [
			'class' => 'webforma\components\EventManager',
			'events' => [
				'template.body.end' => [
					['TargetTemplateListener', 'js'],
				],
			],
		],
	],
];