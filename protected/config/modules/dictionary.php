<?php
return [
    'module'    => [
        'class' => 'application.modules.dictionary.DictionaryModule',
    ],
    'import'    => [],
    'component' => [],
    'rules'     => [],
	'params'    => [
		'widgets' => [
			'dictionary' => 'application.modules.dictionary.widgets.DictionaryWidget',
		],
	],
];
