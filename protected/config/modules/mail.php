<?php

/**
 * Файл конфигурации модуля
 **/

return [
    'module'    => [
        'class' => 'application.modules.mail.MailModule',
    ],
    'import'    => [],
    'component' => [
        // компонент для отправки почты
        'mail'        => [
            'class' => 'webforma\components\Mail',
        ],
        'mailMessage' => [
            'class' => 'application.modules.mail.components.YMailMessage'
        ],
    ],
    'rules'     => [],
];
