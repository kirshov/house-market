<?php
/**
 *
 * Файл конфигурации модуля
 **/
return [
    'module' => [
        'class' => 'application.modules.review.ReviewModule',
	    'mailMessages' => 'orderMailMessage',
        /*'panelWidgets' => [
            'application.modules.review.widgets.PanelReviewStatWidget' => [
                'limit' => 5
            ]
        ],*/
    ],
    'import' => [
        'application.modules.review.models.*',
    ],
	'component' => [
		'eventManager' => [
			'class' => 'webforma\components\EventManager',
			'events' => [
				'review.add.success' => [
					['ReviewListener', 'onSuccessAddComment']
				],
			]
		],
		'reviewNotifyService' => [
			'class' => 'application.modules.review.components.ReviewNotifyService',
		],
		'reviewMailMessage' => [
			'class' => 'application.modules.review.components.ReviewMailMessage',
		],
	],
	'params' => [
		'widgets' => [
			'reviews' => 'application.modules.review.widgets.ReviewsWidget',
		],
	],
    'rules' => [
	    '/reviews/captcha/<v>' => 'review/review/captcha/',
	    '/reviews/captcha/refresh/<refresh:\d+>'=>'review/review/captcha',

	    '/reviews/add/' => 'review/review/add/',
	    '/reviews/' => 'review/review/index/',
    ],
];
