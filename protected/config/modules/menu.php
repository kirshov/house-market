<?php
/**
 * Файл конфигурации модуля
 **/
return [
    'module'    => [
        'class' => 'application.modules.menu.MenuModule',
    ],
    'import'    => [],
    'component' => [],
	'params'    => [
		'widgets'   => [
			'menu' => 'application.modules.menu.widgets.MenuWidget',
		],
	],
    'rules'     => [],
];
