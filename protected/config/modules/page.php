<?php
/**
 *
 * Файл конфигурации модуля
 */
return [
    'module' => [
        'class' => 'application.modules.page.PageModule'
    ],
    'import' => [
        'application.modules.page.events.*',
        'application.modules.page.listeners.*',
        'application.modules.page.models.*',
        'application.modules.page.components.*',
    ],
    'component' => [
        'eventManager' => [
            'class' => 'webforma\components\EventManager',
            'events' => [
                'sitemap.before.generate' => [
                    ['\PageSitemapGeneratorListener', 'onGenerate'],
                ],
                'page.after.save' => [
                    ['\PageListener', 'onAfterSave']
                ],
            ],
        ],
    ],
	'params' => [
		'widgets' => [
			'pages' => 'application.modules.page.widgets.PagesWidget',
		]
	],
    'rules' => [
        [
            'class' => 'application.modules.page.components.PageUrlRule',
        ],
    ],
];
