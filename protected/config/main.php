<?php
/**
 * Main configurations for application
 **/

// Set aliases:
Yii::setPathOfAlias('application', __DIR__ . '/../');
Yii::setPathOfAlias('public', dirname($_SERVER['SCRIPT_FILENAME']));
Yii::setPathOfAlias('webforma', __DIR__ . '/../modules/webforma/');
Yii::setPathOfAlias('vendor', __DIR__ . '/../../vendor/');
Yii::setPathOfAlias('themes', __DIR__ . '/../../themes/');

$config = [
    'basePath' => __DIR__ . '/..',
    // Default controller
    'defaultController' => 'home',
    // Application name
    'name' => 'FORMA.cms',
    // Default language
    'language' => 'ru',
    'sourceLanguage' => 'en',
    // Default theme
    'theme' => null,
    'layout' => 'inner',
    'charset' => 'UTF-8',
    'controllerNamespace' => 'application\controllers',
	'preload' => [
		'debug',
	],
    'import' => [
        'application.modules.webforma.extensions.TaggedCache.*',
        'vendor.yiiext.migrate-command.EDbMigration',
    ],
    /**
     * Enabling and configuration of modules
     * @link http://www.yiiframework.ru/doc/guide/ru/basics.module
     */
    'modules' => [
        'webforma' => [
            'class' => 'application.modules.webforma.WebformaModule',
            'cache' => true,
        ],
    ],
    'params' => CMap::mergeArray(
        require __DIR__ . '/params.php',
        is_file(__DIR__ . '/params-local.php') ? require __DIR__ . '/params-local.php' : []
    ),
    /**
     * Configuration base components
     * @link http://www.yiiframework.ru/doc/guide/ru/basics.component
     */
    'components' => [
    	'assetManager' => [
    	    'forceCopy' => defined('YII_DEBUG') && YII_DEBUG,
	    ],
	    /**
         * Database settings be used only after Webforma install         *
         * @link http://www.yiiframework.ru/doc/guide/ru/database.overview
         */
        'db' => file_exists(__DIR__ . '/db.php') ? require_once __DIR__ . '/db.php' : [],
        'moduleManager' => ['class' => 'webforma\components\ModuleManager'],
        'eventManager' => ['class' => 'webforma\components\EventManager'],
        'configManager' => ['class' => 'webforma\components\ConfigManager'],
        // Migrations and update DB modules
        'migrator' => ['class' => 'webforma\components\Migrator'],
        'uploadManager' => ['class' => 'webforma\components\UploadManager'],
        'themeManager' => [
            'class' => 'CThemeManager',
            'basePath' => dirname(__DIR__) . '/../themes',
            'themeClass' => 'webforma\components\Theme'
        ],
        'cache' => [
            'class' => 'CFileCache',
        ],
	    'clientScript' => [
		    'scriptMap' => defined('YII_DEBUG') && YII_DEBUG
			    ? []
			    : [
				    'jquery.js'=>'https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js',
				    'jquery.min.js'=>'https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js',
				    'jquery-ui.css' => 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.min.css',
				    'jquery-ui.min.css' => 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.min.css',
				    'jquery-ui.js' => 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js',
				    'jquery-ui.min.js' => 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js',
			    ],
	    ],
        /**
         * Configuration of urlManager
         * @link http://www.yiiframework.ru/doc/guide/ru/topics.url
         */
        'urlManager' => [
            'class' => 'webforma\components\WUrlManager',
            'urlFormat' => 'path',
            'urlSuffix' => '/',
            /**
             * Removing index.php from url
             * @link http://yiiframework.ru/doc/guide/ru/quickstart.apache-nginx-config
             */
            'showScriptName' => false,
            'cacheID' => 'cache',
            'useStrictParsing' => true,
            'rules' => [ // Main rules
                //'/' => '/home/index',
                '/' => '/store/category/index',

                // For correct work of installer
                '/install/default/<action:\w+>' => '/install/default/<action>',
            ]
        ],
        /**
         * Configuration of CHttpRequest component for secure from CSRF attacks
         * @link http://www.yiiframework.ru/doc/guide/ru/topics.security
         *
         * RECOMMENDED USE OWN VALUE FOR `csrfTokenName`
         *
         * CHttpRequest class be extended for use AJAX
         * @link http://www.yiiframework.com/forum/index.php/topic/8689-disable-csrf-verification-per-controller-action/
         *
         * @link http://www.yiiframework.com/doc/guide/1.1/ru/topics.security#sec-4
         */
        'request' => [
            'class' => 'webforma\components\HttpRequest',
            'enableCsrfValidation' => true,
            'csrfCookie' => ['httpOnly' => true],
            'csrfTokenName' => 'CSRF_TOKEN',
            'enableCookieValidation' => true
        ],
	    'session' => [
	    	'cookieParams' => [
				'path' => '/',
	    		'httponly' => true,
				'domain' => defined('COOKIE_DOMAIN') ? COOKIE_DOMAIN : null,
		    ],
	    ],
		/*'geoLocateManager' => [
			'class' => 'webforma\components\geoLocate\GeoLocateManager',
			'service' => 'application.modules.ipgeobase.components.IpgeobaseGeoLocate',
		],*/
        /**
         * Configuration of logging
         * @link http://www.yiiframework.ru/doc/guide/ru/topics.logging
         */
        'log' => [
            'class' => 'CLogRouter',
            'routes' => [
                [
                    'class' => 'CFileLogRoute',
                    // On production recommended use only `error, warning`
                    'levels' => 'error, warning, info, trace, profile',
                ]
            ]
        ],
	    'debug' => array(
		    'class' => 'vendor.zhuravljov.yii2-debug.Yii2Debug',
		    'allowedIPs' => ['127.0.0.1', '::1'],
		    'enabled' => defined('YII_DEBUG') && YII_DEBUG,
	    ),
        'errorHandler' => [
            'errorAction' => 'home/error'
        ]
    ],
    'rules' => []
];

/**
 * подключаем gii
 */
if(YII_DEBUG){
	session_start();
	if(isset($_GET['gii'])){
		if($_GET['gii'] == 'on'){
			$_SESSION['gii'] = true;
		} elseif($_GET['gii'] == 'off'){
			unset($_SESSION['gii']);
		}
	}

	if($_SESSION['gii']){
		$config['modules']['gii'] = [
			'class' => 'system.gii.GiiModule',
			'password' => 'password',
			'generatorPaths' => [
				'application.modules.webforma.extensions.gii',
				'application.gii',
			],
		];
		$config['components']['urlManager']['rules'] = array_merge($config['components']['urlManager']['rules'], [
			'gii' => 'gii',
			'gii/<controller:\w+>' => 'gii/<controller>',
			'gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',
		]);
	}
}

return $config;