<?php
return [
    'class'                 => 'CDbConnection',
    'connectionString'      => 'mysql:host=localhost;port=3306;dbname=<db.name>',
    'username'              => '<db.user>',
    'password'              => '<db.pass>',
    'emulatePrepare'        => true,
    'charset'               => 'utf8',
    'enableParamLogging'    => defined('YII_DEBUG') && YII_DEBUG ? true : 0,
    'enableProfiling'       => defined('YII_DEBUG') && YII_DEBUG ? true : 0,
    'schemaCachingDuration' => 108000,
    'tablePrefix'           => 'webforma_',
    'pdoClass'              => 'webforma\extensions\NestedPDO',
];
