<?php
return [
	'aliases' => [
	   'bootstrap' => realpath(Yii::getPathOfAlias('vendor') . '/clevertech/yii-booster/src')
    ],
	'modules' => [
		'webforma' => [
			'panelWidgets' => [
				'application.modules.webforma.widgets.PanelErrorWidget' => [
					'position' => 1,
				],
				'application.modules.webforma.widgets.PanelNotifyWidget' => [
					'position' => 30,
				],
			],
			'visualEditors' => [
				'TinyMCE' => [
					'class' => 'webforma\widgets\editors\TinyMCE',
				],
			],
			'controllerMap' => [
				'tinyMce' => 'application.modules.webforma.extensions.tinymce.controllers.TinyMceBackendController',
			],
		],
	],
	'components' => [
		'assetManager' => [
			//'forceCopy' => false,
			//'linkAssets' => defined('YII_DEBUG') && YII_DEBUG,
			'forceCopy' => defined('YII_DEBUG') && YII_DEBUG,
		],
		'urlManager' => [
			'rules' => [
				'/backend' => '/webforma/backend/index',
				'/backend/login' => '/user/account/backendlogin',
				'/backend/tinyMce/<action>/<js:\d+>'=>'/webforma/tinyMce/<action>',
				'/backend/tinyMce/<action>/'=>'/webforma/tinyMce/<action>',
				'/backend/<action:(AjaxFileUpload|AjaxImageUpload|RemoveFile|index|settings|flushDumpSettings|modulesettings|saveModulesettings|modupdate|help|ajaxflush|transliterate)>' => '/webforma/backend/<action>',
				'/backend/<module:\w+>/<controller:\w+>' => '/<module>/<controller>Backend/index',
				'/backend/<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '/<module>/<controller>Backend/<action>',
				'/backend/<module:\w+>/<controller:\w+>/<action:\w+>' => '/<module>/<controller>Backend/<action>',
			],
		],
		'errorHandler' => [
			'errorAction' => '/webforma/backend/error'
		],
	],
];