<?php
 return array (
  'class' => 'CDbConnection',
  'connectionString' => 'mysql:host=localhost;port=3306;dbname=house-market',
  'username' => 'root',
  'password' => '',
  'emulatePrepare' => true,
  'charset' => 'utf8',
  'enableParamLogging' => defined('YII_DEBUG') && YII_DEBUG ? true : 0,
  'enableProfiling' => defined('YII_DEBUG') && YII_DEBUG ? true : 0,
  'schemaCachingDuration' => 108000,
  'tablePrefix' => 'hm_',
  'pdoClass' => 'webforma\\extensions\\NestedPDO',
);
