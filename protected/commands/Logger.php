<?php
class Logger{

	/**
	 * @var
	 */
	public $fileName = 'log';

	public function __construct($fileName = null) {
		if($fileName){
			$this->fileName = $fileName;
		}
	}

	/**
	 * @param $value
	 */
	public function log($value){
		$dir = Yii::app()->getRuntimePath().DIRECTORY_SEPARATOR.'command'.DIRECTORY_SEPARATOR;
		if(!is_dir($dir)){
			CFileHelper::createDirectory($dir);
		}

		$value = date('d.m.Y H:i:s')."\n".$value;
		file_put_contents($dir.$this->fileName.'.log', $value."\n\n", FILE_APPEND);
	}
}