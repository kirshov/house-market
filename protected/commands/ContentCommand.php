<?php
class ContentCommand extends \webforma\components\ConsoleCommand
{
	public $contentPath;

	public function init()
	{
		$this->contentPath = Yii::getPathOfAlias('application.modules.webforma.content');
		parent::init();
	}


	public function actionIndex(){
		$this->createDefaultPages();
	}

	protected function createDefaultPages(){
		Yii::import('application.modules.page.components.PageUrlRule');
		Yii::import('system.web.CUrlManager', true);

		//О компании
		$page = Page::model()->findBySlug('about');
		if(!$page){
			$page = new Page();
			$page->setAttributes([
				'title' => 'О компании',
				'slug' => 'about',
				'body' => file_get_contents($this->contentPath.'/about.txt'),
				'user_id' => 1,
				'status' => 1,
			]);
			$page->change_user_id = 1;
			$page->save();
		}


		//Контакты
		$page = Page::model()->findBySlug('contacts');
		if(!$page) {
			$page = new Page();
			$page->setAttributes([
				'title' => 'Контакты',
				'slug' => 'contacts',
				'body' => file_get_contents($this->contentPath.'/contacts.txt'),
				'user_id' => 1,
				'status' => 1,
			]);
			$page->change_user_id = 1;
			$page->save();
		}

		//Оплата и доставка
		$page = Page::model()->findBySlug('payment-delivery');
		if(!$page) {
			$page = new Page();
			$page->setAttributes([
				'title' => 'Оплата и доставка',
				'slug' => 'payment-delivery',
				'body' => file_get_contents($this->contentPath.'/payment-delivery.txt'),
				'user_id' => 1,
				'status' => 1,
			]);
			$page->change_user_id = 1;
			$page->save();
		}

		//Политика конфиденциальности
		$page = Page::model()->findBySlug('privacy-policy');
		if(!$page) {
			$page = new Page();
			$page->setAttributes([
				'title' => 'Политика конфиденциальности',
				'slug' => 'privacy-policy',
				'body' => file_get_contents($this->contentPath.'/privacy-policy.txt'),
				'user_id' => 1,
				'status' => 1,
			]);
			$page->change_user_id = 1;
			$page->save();
		}
	}
}