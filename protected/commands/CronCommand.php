<?php
if (!ini_get('date.timezone')) {
    date_default_timezone_set('Asia/Yekaterinburg');
}

ini_set('memory_limit', -1);

Yii::import('application.commands.*');
Yii::import('application.commands.Logger');
class CronCommand extends \webforma\components\ConsoleCommand {

	/**
	 * @var
	 */
	protected $schedule;

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		$this->schedule = CronSchedule::model()->findAll();
	}

	/**
	 *
	 */
	public function actionIndex(){

		foreach (Yii::app()->getModules() as $module => $config){
			if(!isset($config['cron'])){
				continue;
			}

			foreach ($config['cron'] as $name => $data){
				if(empty($data['class'])){
					continue;
				}

				try{
					$command = new $data['class'];
					$command->run();

				} catch (Exception $exception){
					$this->log($exception->getMessage());
				}
			}
		}
	}

	/**
	 * @param $module
	 * @param $command
	 * @param $frequency
	 */
	protected function needRun($module, $command, $frequency){

	}

}