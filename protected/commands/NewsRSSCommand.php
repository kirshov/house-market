<?php

class NewsRSSCommand extends \webforma\components\ConsoleCommand {
	/**
	 * @var
	 */
	protected $url = 'https://www.mzso.info/blog-feed.xml';

	/**
	 * @var array
	 */
	protected $news = [];

	/**
	 * @var
	 */
	protected $path;

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		Yii::import('application.modules.news.NewsModule');
		Yii::import('application.modules.news.models.News');
		Yii::import('application.modules.user.models.User');

		$news = Yii::app()->db->createCommand([
			'select' => 'id, external_id',
			'from' => '{{news_news}}',
			'where' => 'external_id IS NOT NULL',
		])->queryAll();

		if(!empty($news)){
			foreach ($news as $item){
				$this->news[$item['external_id']] = $item['id'];
			}
		}

		$this->path = Yii::getPathOfAlias('webroot').'/uploads/'.Yii::app()->getModule('news')->uploadPath.'/';
	}

	/**
	 *
	 */
	public function actionIndex(){
		$xml = simplexml_load_file($this->url);
		if($xml){
			$user = User::model()->admin()->find();
			foreach ($xml->xpath('//item') as $new){
				//$date = date('Y-d-m H:i:s', strtotime((string) $new->pubDate));
				$pubDate = date_create_from_format(DateTime::RSS, (string) $new->pubDate);
				$date = date_format($pubDate, 'Y-m-d H:i:s');

				$externalId = $this->getHash($date);
				if(isset($this->news[$externalId])){
					continue;
				}

				$slug = mb_substr((string) $new->title, 0, 64);
				$slug = $this->prepareSlug($slug);
				$model = new News();
				$model->setAttributes([
					'title' => (string) $new->title,
					'user_id' => $user ? $user->id : null,
					'slug' => $this->getUniqueSlug($slug),
					'short_text' => $this->getAnons((string) $new->description),
					'full_text' => '<p>'.(string) $new->description.'</p>',
					'date' => $date,
					'status' => News::STATUS_PUBLISHED,
					'external_id' => $externalId,
				]);

				if($model->save()){
					$this->news[$externalId] = $model->id;

					if($new->enclosure){
						$attributes = $new->enclosure->attributes();
						if($attributes['url']){
							$filename = (string) $attributes['url'];
							$extension = CFileHelper::getExtension($filename);
							$newFileName = $this->getUniqueFileName($this->path, 'rss-news-'.$model->id.'.'.$extension);

							$file = file_get_contents($filename);
							if($file){
								file_put_contents($this->path.$newFileName, $file);
								$model->image = $newFileName;
								$model->save();
							}
						}
					}
				} else {
					$this->addLog(print_r($model->getErrors(), true));
				}
			}
		}
		Yii::app()->cache->delete(NewsHelper::CACHE_NEWS_LIST.'::Widget::lastnewswidget::1');
		Yii::app()->cache->delete(NewsHelper::CACHE_NEWS_LIST.'::Widget::lastnewswidget::0');
	}

	/**
	 * @param $key
	 * @return mixed|string
	 */
	protected function getHash($key){
		return md5($key);
	}

	/**
	 * @param $description
	 * @param int $length
	 * @return string
	 */
	protected function getAnons($description, $length = 200){
		if(mb_strlen($description) > $length + 3){
			return '<p>'.mb_substr($description, 0, $length).'...</p>';
		}

		return '<p>'.$description.'</p>';
	}

	/**
	 * @param $slug
	 * @return string
	 */
	protected function getUniqueSlug($slug){
		$linkCount = 0;
		$linkPostfix = '';

		$command = Yii::app()->db->createCommand();

		do {
			$newSlug = $slug.$linkPostfix;

			$command
				->reset()
				->select('slug')
				->from('{{news_news}}')
				->where('slug = :slug');
			$command->params = [':slug' => $newSlug];

			$hasExist = $command->queryScalar();

			$linkCount++;
			$linkPostfix = '-' . $linkCount;
		} while ($hasExist);

		return $newSlug;
	}

	/**
	 * @param $path
	 * @param $name
	 * @return mixed|string|string[]|null
	 */
	protected function getUniqueFileName($path, $name){
		$extension = CFileHelper::getExtension($name);

		$name = preg_replace('/\s{2,}/iu', ' ', $name);
		$name = \dosamigos\yii\helpers\TransliteratorHelper::process($name, '');
		$name = str_replace(' ', '-', $name);
		$tempName = basename($name, '.' . $extension);

		$suffix = 1;
		$fileExist = file_exists($path.DIRECTORY_SEPARATOR.$name);
		while ($fileExist && $suffix < 100){
			$name = $tempName.'-'.$suffix.'.'.$extension;
			$fileExist = file_exists($path.DIRECTORY_SEPARATOR.$name);
			$suffix++;
		}

		return $name;
	}

	/**
	 * @param string $slug
	 * @return string mixed
	 */
	protected function prepareSlug($slug){
		$slug = trim($slug);
		$slug = preg_replace('/\s{2,}/',' ',$slug);
		$slug = str_replace(' ', '-', $slug);
		$slug = preg_replace('/[^a-zA-Zа-яА-Я0-9-_.]/ui', '', $slug);
		$slug = mb_strtolower($slug);
		$slug = \dosamigos\yii\helpers\TransliteratorHelper::process($slug);
		$slug = preg_replace(\webforma\components\validators\YSLugValidator::$pattern, '', $slug);
		$slug = mb_substr($slug, 0, 145);
		return $slug;
	}

	/**
	 * @param string $message
	 */
	protected function addLog($message){
		$dir = Yii::app()->getRuntimePath().DIRECTORY_SEPARATOR.'rss_news'.DIRECTORY_SEPARATOR;
		if(!is_dir($dir)){
			CFileHelper::createDirectory($dir, null, true);
		}

		file_put_contents($dir.date('dmY_Hi').'__rss_news.log', $message, FILE_APPEND);
	}
}