<?php

ini_set('memory_limit', -1);

Yii::import('application.modules.import.components.*');
Yii::import('application.modules.import.forms.*');
Yii::import('application.modules.import.models.Import');
Yii::import('application.modules.sitemap.components.*');

class ImportCommand extends \webforma\components\ConsoleCommand {

	/**
	 * @var
	 */
	public $path;

	/**
	 * @var
	 */
	public $runtimePath;

	public function init()
	{
		parent::init();
		$this->path = Yii::getPathOfAlias('webroot').'/1c_import/';
		//$this->path = '/home/u19631/sportek.su/www/!1c/';

		$this->runtimePath = Yii::app()->getRuntimePath().'/';
	}

	public function action1c(){
		$file = $this->path.'data_update.txt';

		if(!file_exists($file)){
			return;
		}

		$flag = file_exists($this->runtimePath.'update.flg') ? file_get_contents($this->runtimePath.'update.flg') : false;

		if($flag && $flag + 10 * 60 > time()){
			file_put_contents($this->runtimePath.'import/logs/import.log', date('d.m.Y')." Процесс уже запущен\n\n", FILE_APPEND);
			return;
		}
		file_put_contents($this->runtimePath.'update.flg', time());

		$handler = new ImportHandler1C();
		$handler->updateType = ImportForm::UPDATE_TYPE_FULL;
		$handler->init();
		$handler->settings['missing_product'] = Import::MISSING_POS_SKIP;
		$handler->run($file);

		Yii::app()->getCache()->flush();

		file_put_contents($this->runtimePath.'import/logs/import.log', date('d.m.Y')."Выполнено\n\n", FILE_APPEND);
		file_put_contents($this->runtimePath.'update.flg', '');
	}

	public function action1cFull(){
		$file = $this->path.'data.txt';

		if(!file_exists($file)){
			return;
		}

		$flag = file_exists($this->runtimePath.'update.flg') ? file_get_contents($this->runtimePath.'update.flg') : false;

		if($flag && $flag + 10 * 60 > time()){
			file_put_contents($this->runtimePath.'import/logs/import.log', date('d.m.Y')." Процесс уже запущен\n\n", FILE_APPEND);
			return;
		}
		file_put_contents($this->runtimePath.'update.flg', time());

		$handler = new ImportHandler1C();
		$handler->updateType = ImportForm::UPDATE_TYPE_FULL;
		$handler->init();
		$handler->settings['missing_product'] = Import::MISSING_POS_DELETE;
		$handler->run($file);

		Yii::app()->getCache()->flush();

		file_put_contents($this->runtimePath.'import/logs/import.log', date('d.m.Y')."Выполнено full\n\n", FILE_APPEND);
		file_put_contents($this->runtimePath.'update.flg', '');
	}

	public function actionImages(){
		$flag = file_exists($this->runtimePath.'update_images.flg') ? file_get_contents($this->runtimePath.'update_images.flg') : false;

		if($flag && $flag + 10 * 60 > time()){
			return;
		}
		file_put_contents($this->runtimePath.'update_images.flg', time());

		$sourcePath = Yii::getPathOfAlias('webroot').'/temp/images/';
		$copyPath = Yii::getPathOfAlias('webroot').'/uploads/store/product/';
		$thumbsPath = Yii::getPathOfAlias('webroot').'/uploads/thumbs/store/product/';

		$images = glob($sourcePath.'*.jpg');
		if(!$images){
			$zip = new ZipArchive();
			$res = $zip->open($this->path.'foto.zip');
			if ($res === true) {
				$zip->extractTo($sourcePath);
				$zip->close();
			} else {
				return false;
			}
		}

		$images = glob($sourcePath.'*.jpg');
		if(!empty($images)){
			$productImages = [];
			$currentImages = Yii::app()->db->createCommand(array(
				'select' => 'image',
				'from' => Product::model()->tableName(),
				'where' => 'image != ""'
			))->queryAll();

			if(!empty($currentImages)){
				foreach ($currentImages as $image){
					$productImages[$image['image']] = true;
				}
			}

			$currentImages = Yii::app()->db->createCommand([
				'select' => 'name',
				'from' => '{{store_product_image}}',
			])->queryAll();

			if(!empty($currentImages)){
				foreach ($currentImages as $image){
					$productImages[$image['name']] = true;
				}
			}

			$images = array_slice($images, 0, 1000);

			foreach ($images as $image){
				$needClearThumb = false;
				$image = basename($image);
				if($productImages[$image]){
					if(file_exists($copyPath.$image)){
						$needClearThumb = filesize($copyPath.$image) != filesize($copyPath.$image);
					}
					if(copy($sourcePath.$image, $copyPath.$image)){
						if($needClearThumb){
							Helper::deleteFiles($thumbsPath, $image);
						}
					}
				}
				@unlink($sourcePath.$image);
			}

			//Helper::clearDir($thumbsPath);
		}

		file_put_contents($this->runtimePath.'update_images.flg', '');
		file_put_contents($this->runtimePath.'import/logs/import.log', date('d.m.Y')."Выполнено images\n\n", FILE_APPEND);
	}
}