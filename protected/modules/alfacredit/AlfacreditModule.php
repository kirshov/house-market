<?php

use webforma\components\WebModule;

class AlfacreditModule extends WebModule
{
    /**
     *
     */
    public $adminMenuOrder = 83;

    public function getDependencies()
    {
        return ['payment'];
    }

    public function getNavigation()
    {
        return false;
    }

    public function getAdminPageLink()
    {
        return false;
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getEditableParamsGroups()
    {
        return [];
    }

    public function getIsShowInAdminMenu()
    {
        return false;
    }

    public function getCategory()
    {
        return Yii::t('AlfacreditModule.alfacredit', 'Store');
    }

    public function getName()
    {
        return Yii::t('AlfacreditModule.alfacredit', 'AlfaCredit');
    }

    public function getDescription()
    {
        return Yii::t('AlfacreditModule.alfacredit', 'AlfaCredit payment module');
    }

    public function getIcon()
    {
        return 'fa fa-rub';
    }

    public function init()
    {
        parent::init();
    }
}
