<?php

/**
 * Class AlfaCreditPaymentSystem
 */

Yii::import('application.modules.tinkoff.TinkoffModule');
Yii::import('application.modules.tinkoff.components.TinkoffMerchantAPI');

class AlfaCreditPaymentSystem extends PaymentSystem
{
	protected $paymentSettings;

	public function renderCheckoutForm(Payment $payment, Order $order, $return = false)
	{
		$this->paymentSettings = $payment->getPaymentSystemSettings();
		if(!$this->paymentSettings['inn']){
			return false;
		}

		return Yii::app()->getController()->renderPartial('application.modules.alfacredit.views.form', [
			'settings' => $payment->getPaymentSystemSettings(),
			'order' => $order,
			'payment' => $payment,
			'xml' => $this->createXml($order),
		], $return);
	}

	/**
	 * @param Order $order
	 * @return string
	 * @throws Exception
	 */
	protected function createXml(Order $order){
		$fio = explode(' ', $order->name);
		$phone = $order->phone;
		if($phone){
			$phone = str_replace('+7', '', $phone);
			$phone = preg_replace("/[^0-9]/", '', $phone);
		}

		$clientInfo = [];
		if($fio[0]){
			$clientInfo['lastname'] = $fio[0];
		}
		if($fio[1]){
			$clientInfo['firstname'] = $fio[1];
		}
		if($fio[2]){
			$clientInfo['middlename'] = $fio[2];
		}
		if($order->email){
			$clientInfo['email'] = $order->email;
		}
		if($phone){
			$clientInfo['mobphone'] = $phone;
		}

		$data = [
			'brokerInfo' => [
				'logotype' => Yii::app()->getModule('webforma')->getLogo(),
			],
			'companyInfo' => [
				'inn' => $this->paymentSettings['inn'],
				'referer' => Yii::app()->createAbsoluteUrl('/order/order/view', ['url' => $order->url]),
			],
			'creditInfo' => [
				'reference' => $order->getNumberForUser(),
			],
			'clientInfo' => $clientInfo,
			'specificationList' => $this->getProductList($order),
		];

		$xml = Array2XML::createXML('inParams', $data);

		$path = Yii::app()->getRuntimePath().'/logs/alfacredit/';
		if(!is_dir($path)){
			CFileHelper::createDirectory($path, null, true);
		}
		$xml->save($path.$order->getNumberForUser().'.xml');

		$resultXml = $xml->saveXML();

		$resultXml = preg_replace('/<\?.+\?>/', '', $resultXml);
		return $resultXml;
	}

	/**
	 * @param Order $order
	 * @return array
	 */
	public function getProductList(Order $order){
		$products = [];
		foreach ((array)$order->products as $productItem){
			$productData = $productItem->product;
			$products['specificationListRow'][] = [
				'category' => $this->paymentSettings['category'],
				'code' => $productItem->sku ?: $productItem->id,
				'description' => $productItem->product_name,
				'amount' => $productItem->quantity,
				'price' => ($productItem->price == (int) $productItem->price) ? (int) $productItem->price : $productItem->price,
				'image' => $productData ? $productData->getImageUrl(90, 90, false) : '',
			];
		}
		return $products;
	}
}
