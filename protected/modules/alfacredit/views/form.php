<?php
/**
 * @var array $settings
 * @var Order $order
 * @var Payment $payment
 * @var string $xml
 */

echo CHtml::form($settings['api_url'], 'post', ['enctype' => 'application/x-www-form-urlencoded']);
echo CHtml::hiddenField('InXML', $xml);
echo CHtml::submitButton('Подать заявку на кредит', ['class' => 'btn']);
echo CHtml::endForm();
