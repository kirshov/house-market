<?php

return [
    'module' => [
        'class' => 'application.modules.alfacredit.AlfacreditModule',
    ],
    'component' => [
        'paymentManager' => [
            'paymentSystems' => [
                'alfacredit' => [
                    'class' => 'application.modules.alfacredit.components.payments.AlfaCreditPaymentSystem',
                ]
            ],
        ],
    ],
];
