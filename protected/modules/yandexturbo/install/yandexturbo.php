<?php

return [
    'module' => [
        'class' => 'application.modules.yandexturbo.YandexturboModule'
    ],
    'import' => [
        'application.modules.store.listeners.StoreTurboGeneratorListener'
    ],
    'component' => [
        'yandexturboGenerator' => [
            'class' => 'application.modules.yandexturbo.components.YandexturboGenerator'
        ]
    ],
    'rules' => [
        'yandexturbo.xml' => 'yandexturbo/yandexturbo/index'
    ]
];
