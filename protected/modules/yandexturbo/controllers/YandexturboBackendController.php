<?php

use webforma\components\controllers\BackController;
use webforma\widgets\WFlashMessages;

/**
 * Class YandexturboBackendController
 */
class YandexturboBackendController extends BackController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['deny'],
        ];
    }

    /**
     *
     */
    public function actionIndex()
    {
	    $this->render('index');
    }

    /**
     * @throws CHttpException
     */
    public function actionRegenerate()
    {
        if (!Yii::app()->getRequest()->getIsPostRequest() || !Yii::app()->getRequest()->getPost('do')) {
            throw new CHttpException(404);
        }

        if (YandexturboHelper::remove()) {
            Yii::app()->getUser()->setFlash(WFlashMessages::SUCCESS_MESSAGE, 'Источник успешно обновлен');
            Yii::app()->ajax->success();
        }

        Yii::app()->getUser()->setFlash(WFlashMessages::ERROR_MESSAGE, 'Произошла ошибка при обновлении источника');
        Yii::app()->ajax->failure();
    }
}
