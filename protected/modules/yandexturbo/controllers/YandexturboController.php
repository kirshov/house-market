<?php

use webforma\components\controllers\FrontController;


/**
 * Class YandexturboController
 */
class YandexturboController extends FrontController
{
    /**
     * @var
     */
    protected $generator;

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->generator = Yii::app()->getComponent('yandexturboGenerator');

    }

    /**
     *
     */
    public function actionIndex()
    {
	    $path = Yii::app()->getModule('yandexturbo')->getYandexturboPath();
	    if(!is_dir($path)){
		    CFileHelper::createDirectory($path, null, true);
	    }
	    $yandexturboFile = $path.$_SERVER['HTTP_HOST'].'_yandexturbo.xml';

	    if (!file_exists($yandexturboFile)) {

		    $staticPages = YandexturboPage::model()->getData();

		    $this->generator->generate($yandexturboFile, $staticPages);
	    }

	    header('Content-type: text/xml');
	    echo file_get_contents($yandexturboFile);
	    Yii::app()->end();
    }
}
