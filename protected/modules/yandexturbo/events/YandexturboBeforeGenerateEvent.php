<?php

/**
 * Class YandexturboBeforeGenerateEvent
 */
class YandexturboBeforeGenerateEvent extends \webforma\components\Event
{
    /**
     * @var
     */
    protected $generator;

    /**
     * @return mixed
     */
    public function getGenerator()
    {
        return $this->generator;
    }

    /**
     * @param YandexturboGenerator $generator
     */
    public function __construct(YandexturboGenerator $generator)
    {
        $this->generator = $generator;
    }
} 
