<?php

$this->breadcrumbs = [
    $this->getModule()->name,
];
?>

<?php $this->widget('bootstrap.widgets.TbButton', [
	'buttonType' => 'submit',
	'context' => 'primary',
	'label' => 'Обновить источник',
	'id' => 'regenerate-site-map',
]); ?>

<script type="text/javascript">
    $('#regenerate-site-map').on('click', function (event) {
        event.preventDefault();
        $.post('<?= Yii::app()->createUrl('/yandexturbo/yandexturboBackend/regenerate');?>', {
            'do': true,
            '<?= Yii::app()->getRequest()->csrfTokenName?>': '<?= Yii::app()->getRequest()->csrfToken;?>'
        }, function (response) {
            window.location.reload();
        }, 'json')
    });
</script>
