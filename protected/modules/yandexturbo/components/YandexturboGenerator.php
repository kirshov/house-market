<?php
/**
 * Class YandexturboGenerator
 */
class YandexturboGenerator extends CApplicationComponent
{
    /**
     * @var array
     */
    protected $data = [];

	/**
	 * @var string
	 */
    protected $headerMenu;

	/**
	 * @param $url
	 * @param $title
	 * @param $content
	 */
    public function addItem($url, $title, $content)
    {
        $this->data[] = [
	        'url' => $url,
	        'title' => $title,
	        'content' => $content,
        ];
    }

    /**
     * @param $yandexturboFile
     */
    public function generate($yandexturboFile, array $data)
    {
        if (!empty($data)) {
            $this->data = array_merge($this->data, $data);
        }

        $yandexturbo = new TurboRSS($yandexturboFile);

        Yii::app()->eventManager->fire(YandexturboEvents::BEFORE_GENERATE, new YandexturboBeforeGenerateEvent($this));

        foreach ($this->data as $item) {
            $yandexturbo->addItem($this->getContentHeader($item['title']), $item['url'], $item['content']);
        }

        $yandexturbo->write();
    }


    protected function getContentHeader($title){
    	if(!$this->headerMenu){
    		$this->headerMenu = $this->getContentHeader();
	    }
		$html = '';

		return $html;
    }

	/**
	 * @return string
	 */
    protected function getHeaderMenu(){
        return '';
    }
} 
