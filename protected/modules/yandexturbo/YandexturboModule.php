<?php

use webforma\components\WebModule;

/**
 * Class YandexturboModule
 */
class YandexturboModule extends WebModule
{
    /**
     * @var
     */
    public $data;

    /**
     * @var string
     */
    public $filePath = 'yandexturbo.xml';

    /**
     * @return string
     */
    public function getYandexturboPath()
    {
        return Yii::getPathOfAlias('webroot').'/uploads/yandexturbo/';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Yandex.Turbo';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-rocket';
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return false;
    }


    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'yandexturbo.components.*',
                'yandexturbo.events.*',
            ]
        );
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return false;
    }

	/**
	 * @return array
	 */
	public function getNavigation()
	{
		return [];
	}


	/**
	 * @return bool
	 */
	public function getIsInstallDefault()
	{
		return false;
	}
}
