<?php
/**
 * Конфигурационный файл модуля
 */
return [
    'module' => [
        'class' => 'application.modules.forms.FormsModule',
    ],
    'import' => [
        'application.modules.forms.models.*',
    ],
    'params' => [
    	'widgets' => [
    	    'form' => 'application.modules.forms.widgets.FormWidget'
	    ],
    ],
    'rules' => [],
];
