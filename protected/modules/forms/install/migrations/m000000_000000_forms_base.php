<?php

class m000000_000000_forms_base extends webforma\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        $this->createTable(
            '{{forms}}',
            [
                'id'                => 'pk',
                'code'              => 'varchar(150) NOT NULL',
                'name'              => 'varchar(250) default null',
                'theme'             => 'varchar(250) default null',
                'view'              => 'varchar(250) default null',
                'description'       => 'text',
	            'after_description' => 'text',
	            'status'            => "boolean NOT NULL DEFAULT '1'",
            ],
            $this->getOptions()
        );

        //ix
        $this->createIndex("ux_{{forms}}_code", '{{forms}}', "code", true);
        $this->createIndex("ix_{{forms}}_status", '{{forms}}', "status", false);
    }

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{forms}}');
    }
}
