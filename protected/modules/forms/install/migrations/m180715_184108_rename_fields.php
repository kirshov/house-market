<?php

class m180715_184108_rename_fields extends webforma\components\DbMigration
{
	public function safeUp()
	{
        $this->renameColumn('{{forms}}', 'after_description', 'message');
	}
}