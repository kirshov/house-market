<?php
use webforma\widgets\WWidget;

/**
 * Class FeedbackWidget
 */
class FormWidget extends WWidget
{
    /**
     * @var string
     */
    public $code;

	/**
	 * @throws CException
	 */
	public function registerScript()
	{
		Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getAssetManager()->publish(
			Yii::getPathOfAlias('application.modules.feedback.views.assets.js') . '/feedbackAjax.js'
		), CClientScript::POS_END);
	}

    /**
     * @throws CException
     */
    public function run()
    {
	    $formModel = Forms::model()->published()->findByAttributes(['code' => $this->code]);
	    if(!$formModel){
	    	return '';
	    }

	    $view = 'default';

        $model = new FeedBackForm();
        $module = Yii::app()->getModule('feedback');

        if (Yii::app()->getUser()->isAuthenticated()) {
            $model->email = Yii::app()->getUser()->getProFileField('email');
            $model->name = Yii::app()->getUser()->getProFileField('nick_name');
        }

        $this->render($view, [
            'model' => $model,
            'module' => $module,
	        'formModel' => $formModel,
        ]);
    }
}