<?php

/**
 * Модель Forms
 *
 * @property string $id
 * @property string $name
 * @property string $theme
 * @property string $view
 * @property string $description
 * @property string $message
 * @property string $code
 * @property integer $status
 *
 * @method Forms published()
 */

use webforma\components\Event;
use webforma\widgets\WPurifier;

class Forms extends webforma\models\WModel
{
    const STATUS_PUBLISHED = 1;
    const STATUS_UNPUBLISHED = 2;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{forms}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * @return Forms the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['name, description, message, code, view, theme', 'filter', 'filter' => 'trim'],
            ['name, code', 'filter', 'filter' => [new WPurifier(), 'purify']],
            ['code', 'required'],
            ['status', 'numerical', 'integerOnly' => true],
            ['name, view', 'length', 'max' => 250],
            ['code', 'length', 'max' => 150],
            [
                'code',
                'webforma\components\validators\YSLugValidator',
                'message' => Yii::t('FormsModule.forms', 'Bad characters in {attribute} field')
            ],
            ['code', 'unique'],
            ['status', 'in', 'range' => array_keys($this->statusList)],
            ['id, code, name, status', 'safe', 'on' => 'search'],
        ];
    }

    public function scopes()
    {
        return [
            'published' => [
                'condition' => 'status = :status',
                'params' => [':status' => self::STATUS_PUBLISHED],
            ],
        ];
    }

    public function beforeValidate()
    {
        if (!$this->code) {
            $this->code = webforma\helpers\WText::translit($this->code);
        }
        return parent::beforeValidate();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('FormsModule.forms', 'Id'),
            'name' => Yii::t('FormsModule.forms', 'Title'),
            'view' => Yii::t('FormsModule.forms', 'View'),
            'message' => Yii::t('FormsModule.forms', 'Message'),
            'description' => Yii::t('FormsModule.forms', 'Description'),
            'code' => Yii::t('FormsModule.forms', 'Code'),
            'theme' => Yii::t('FormsModule.forms', 'Theme'),
            'status' => Yii::t('FormsModule.forms', 'Status'),
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria();

        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider(get_class($this), ['criteria' => $criteria]);
    }

    /**
     * Returns available status list
     *
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_UNPUBLISHED => Yii::t('FormsModule.forms', 'Draft'),
            self::STATUS_PUBLISHED => Yii::t('FormsModule.forms', 'Published'),
        ];
    }

    /**
     * Returns current status name
     *
     * @return string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('FormsModule.forms', '*unknown*');
    }
}
