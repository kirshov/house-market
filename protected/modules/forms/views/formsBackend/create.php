<?php
$this->breadcrumbs = [
    Yii::t('FormsModule.forms', 'Forms') => ['/forms/formsBackend/index'],
    Yii::t('FormsModule.forms', 'Create'),
];

$this->pageTitle = Yii::t('FormsModule.forms', 'Forms - create');

$this->menu = [
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('FormsModule.forms', 'Form manage'),
        'url' => ['/forms/formsBackend/index']
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('FormsModule.forms', 'Create form'),
        'url' => ['/forms/formsBackend/create']
    ],
];
?>

<?$this->title = Yii::t('FormsModule.forms', 'Form').'<br><small>'.Yii::t('FormsModule.forms', 'create').'</small>';?>

<?= $this->renderPartial('_form', ['model' => $model]); ?>
