<?php
$this->breadcrumbs = [
    Yii::t('FormsModule.forms', 'Forms') => ['/forms/formsBackend/index'],
    Yii::t('FormsModule.forms', 'Manage'),
];

$this->pageTitle = Yii::t('FormsModule.forms', 'Forms - manage');

$this->menu = [
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('FormsModule.forms', 'Form manage'),
        'url' => ['/forms/formsBackend/index']
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('FormsModule.forms', 'Create form'),
        'url' => ['/forms/formsBackend/create']
    ],
];
?>

<?php $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'form-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => [
            [
                'name' => 'id',
                'htmlOptions' => ['style' => 'width:20px'],
                'type' => 'raw',
                'value' => 'CHtml::link($data->id, array("/forms/formsBackend/update", "id" => $data->id))'
            ],
            [
                'class' => 'webforma\widgets\WEditableColumn',
                'name' => 'name',
                'editable' => [
                    'url' => $this->createUrl('/forms/formsBackend/inline'),
                    'mode' => 'popup',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter' => CHtml::activeTextField($model, 'name', ['class' => 'form-control']),
            ],
            [
                'class' => 'webforma\widgets\WEditableColumn',
                'name' => 'code',
                'editable' => [
                    'url' => $this->createUrl('/forms/formsBackend/inline'),
                    'mode' => 'popup',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter' => CHtml::activeTextField($model, 'code', ['class' => 'form-control']),
            ],
            [
                'class' => 'webforma\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('/forms/formsBackend/inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    Forms::STATUS_PUBLISHED => ['class' => 'label-success'],
	                Forms::STATUS_UNPUBLISHED => ['class' => 'label-warning'],
                ],
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
            ],
        ],
    ]
);
