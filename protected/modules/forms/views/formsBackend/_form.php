<?php
/**
 * @var $this FormsBackendController
 * @var $form \webforma\widgets\ActiveForm
 * @var $model Forms
 */

$form = $this->beginWidget(
    '\webforma\widgets\ActiveForm',
    [
        'id' => 'form-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => 'vertical',
		'htmlOptions' => ['class' => 'well sticky'],
		'clientOptions' => [
			'validateOnSubmit' => true,
		],
    ]
); ?>
<div class="alert alert-info">
    <?= Yii::t('FormsModule.forms', 'Fields with'); ?>
    <span class="required">*</span>
    <?= Yii::t('FormsModule.forms', 'are required'); ?>
</div>

<?= $form->errorSummary($model); ?>

<div class="row">
	<div class="col-sm-6">
		<div class='row'>
			<div class="col-sm-12">
				<?= $form->slugFieldGroup($model, 'code', ['sourceAttribute' => 'name']); ?>
				<small class="help-block" style="margin-top: -10px;">Используйте конструкцию <b>[form:код]</b>, чтобы вывести форму на странице</small>
			</div>
		</div>

		<div class='row'>
			<div class="col-sm-12">
				<?= $form->textFieldGroup($model, 'name'); ?>
			</div>
		</div>

		<div class='row'>
			<div class="col-sm-12">
				<?= $form->textFieldGroup($model, 'description'); ?>
			</div>
		</div>

		<div class='row'>
			<div class="col-sm-12">
				<?= $form->textFieldGroup($model, 'theme'); ?>
			</div>
		</div>
	</div>
</div>

<div class='row'>
    <div class="col-sm-12">
        <div class="form-group">
            <?= $form->labelEx($model, 'message'); ?>
            <?php $this->widget(
                $this->module->getVisualEditor(),
                [
                    'model' => $model,
                    'attribute' => 'message',
                ]
            ); ?>
			<small class="help-block">Если на заполнено, сообщение будет следующее: <b><?=Yii::app()->getModule('feedback')->message?></b></small>
        </div>
    </div>
</div>

<div class="row">
	<div class='col-sm-6'>
		<?= $form->dropDownListGroup($model, 'status', [
			'widgetOptions' => [
				'data' => $model->getStatusList(),
			],
		]); ?>
	</div>

	<div class="col-sm-6">

	</div>
</div>

<div class="buttons">
	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context' => 'primary',
		'label' => $model->isNewRecord
			? Yii::t('FormsModule.forms', 'Create form and continue')
			: Yii::t('FormsModule.forms', 'Save form and continue'),
	]); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label' => $model->isNewRecord
			? Yii::t('FormsModule.forms', 'Create form and close')
			: Yii::t('FormsModule.forms', 'Save form and close'),
	]); ?>
</div>

<?php $this->endWidget(); ?>
