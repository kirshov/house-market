<?php
$this->breadcrumbs = [
    Yii::t('FormsModule.forms', 'Forms') => ['/forms/formsBackend/index'],
    Yii::t('FormsModule.forms', 'Change').' "'.$model->code.'"',
];

$this->pageTitle = Yii::t('FormsModule.forms', 'Forms - edit');

$this->menu = [
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('FormsModule.forms', 'Form manage'),
        'url' => ['/forms/formsBackend/index']
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('FormsModule.forms', 'Create form'),
        'url' => ['/forms/formsBackend/create']
    ],
    ['label' => Yii::t('FormsModule.forms', 'Form') . ' «' . mb_substr($model->name, 0, 32) . '»'],
    [
        'icon' => 'fa fa-fw fa-pencil',
        'label' => Yii::t('FormsModule.forms', 'Change form'),
        'url' => [
            '/forms/formsBackend/update',
            'id' => $model->id
        ]
    ],
    [
        'icon' => 'fa fa-fw fa-trash-o',
        'label' => Yii::t('FormsModule.forms', 'Remove form'),
        'url' => '#',
        'linkOptions' => [
            'submit' => ['/forms/formsBackend/delete', 'id' => $model->id],
            'params' => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
            'confirm' => Yii::t('FormsModule.forms', 'Do you really want to remove form?'),
            'csrf' => true,
        ]
    ],
];
?>

<?= $this->renderPartial(
    '_form',
    ['model' => $model]
);
