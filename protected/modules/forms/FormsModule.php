<?php

use webforma\components\WebModule;

/**
 * Class FormsModule
 */
class FormsModule extends WebModule
{
	/**
	 * @var int порядок следования модуля в меню панели управления (сортировка)
	 */
	public $adminMenuOrder = 25;

	/**
	 * @return array
	 */
	public function getDependencies()
	{
		return ['feedback'];
	}

	/**
	 * @return string
	 */
	public function getCategory()
	{
		return 'Сервисы';
	}

    /**
     * @return string
     */
    public function getForm()
    {
        return Yii::t('FormsModule.forms', 'Structure');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('FormsModule.forms', 'Forms feedback');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-wpforms';
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'forms.models.*',
            ]
        );
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('FormsModule.forms', 'Forms list'),
                'url' => ['/forms/formsBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('FormsModule.forms', 'Create form'),
                'url' => ['/forms/formsBackend/create'],
            ],
        ];
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/forms/formsBackend/index';
    }
}
