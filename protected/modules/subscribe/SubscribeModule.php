<?php

/**
 * SubscribeModule основной класс модуля install
 */
class SubscribeModule extends webforma\components\WebModule
{
	/**
	 * @var int
	 */
	public $adminMenuOrder = 15;

	/**
	 * @var string
	 */
	public $useSendpulse;

	/**
	 * @var string
	 */
	public $sendpulseApiUserId;

	/**
	 * @var string
	 */
	public $sendpulseApiUserSecret;

	/**
	 * @var string
	 */
	public $sendpulseApiBookId;
	
	/**
     * Метод получения категории:
     *
     * @return string category
     **/
    public function getCategory()
    {
        return Yii::t('SubscribeModule.subscribe', 'Services');
    }

    /**
     * Метод получения названия модуля:
     *
     * @return string name
     **/
    public function getName()
    {
        return Yii::t('SubscribeModule.subscribe', 'Subscribers');
    }

    /**
     * Метод получения описвния модуля:
     *
     * @return string description
     **/
    public function getDescription()
    {
        return Yii::t('SubscribeModule.subscribe', 'Module for subscribe message management');
    }

    /**
     * Метод получения иконки:
     *
     * @return string icon
     **/
    public function getIcon()
    {
        return 'fa fa-fw fa-envelope-square';
    }

    /**
     * Метод получения адреса модуля в админ панели:
     *
     * @return string admin url
     **/
    public function getAdminPageLink()
    {
        return '/subscribe/subscriberBackend/index';
    }

    /**
     * Метод получения меню модуля (для навигации):
     *
     * @return mixed navigation of module
     **/
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('SubscribeModule.subscribe', 'Subscribers'),
                'url' => ['/subscribe/templateBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('SubscribeModule.subscribe', 'Create subscriber'),
                'url' => ['/subscribe/templateBackend/create'],
            ],
        ];
    }

    /**
     * Метод инициализации модуля:
     **/
    public function init()
    {
        $this->setImport(
            [
                'subscribe.models.*',
                'subscribe.components.*',
	            'subscribe.events.*',
	            'subscribe.listeners.*',
            ]
        );

        parent::init();
    }

	/**
	 * @return array
	 */
	public function getParamsLabels()
	{
		return [
			'useSendpulse' => 'Использовать интеграцию с Sendpulse',
			'sendpulseApiUserId' => 'Sendpulse API UserId',
			'sendpulseApiUserSecret' => 'Sendpulse API Secret',
			'sendpulseApiBookId' => 'Sendpulse Id адресной книги',
		];
	}

	/**
	 * @return array
	 */
	public function getEditableParams()
	{
		$params = [
			'sendPulseLink' => [
				'input-type' => 'html',
				'html' => '<a href="https://sendpulse.com/ru/?ref=7102056" target="_blank">Регистрация в SendPulse</a>',
			],
			'useSendpulse' => [
				'input-type' => 'checkbox',
			],
			'sendpulseApiUserId',
			'sendpulseApiUserSecret',
			'sendpulseApiBookId',
			/*'hint' => [
				'input-type' => 'html',
				'html' => 'Чтобы отметить отписавшихся через рассылки SendPulse - создайте соответсвующий webhook с адресом <b>'.Yii::app()->createAbsoluteUrl('/subscribe/subscribe/unsubscribeHook').'</b>',
			]*/
		];

		return $params;
	}

	/**
	 * @return array
	 */
	public function getEditableParamsGroups()
	{
		$groups =  [
			'sendpulse' => [
				'label' => 'Интеграция с сервисом SendPulse',
				'items' => [
					'sendPulseLink',
					'useSendpulse',
					'sendpulseApiUserId',
					'sendpulseApiUserSecret',
					'sendpulseApiBookId',
					//'hint',
				],
			],
		];

		return $groups;
	}


	/**
     * @return array
     */
    public function getDependencies()
    {
        return [];
    }
}
