<?php
class SubscribeMailMessage extends WMailMessage{

	/**
	 * @var string
	 */
	public $icon = 'fa-envelope-square';

	/**
	 * @var Subscribers
	 */
	protected $model;

	/**
	 * @var string
	 */
	protected $module = 'subscribe';

	/**
	 * @return array
	 */
	public function getDefaultMessages()
	{
		return [
			'confirmSubscribe' => [
				'name' => 'Сообщение пользователю о необходимости подтвердить подписку',
				'subject' => 'Подтвердите Вашу подписку на сайте "%siteName%"',
				'body' => '<h2 style="'.$this->getStyle('title').'">Подтвердите Вашу подписку</h2>
					<h3 style="'.$this->getStyle('subtitle').'">на сайте "%siteName%"</h3>
					<br/>
					<p style="text-align: center">%link-subscribe%</p>
					<br/>
					<p style="font: %font%; text-align: center;">
						Если вы получили это письмо по ошибке и не хотите получать уведомления, просто проигнорируйте его.
					</p>
				',
			],

			'adminNotifySubscribe' => [
				'name' => 'Сообщение администратору о новом подписчике',
				'subject' => 'Новый подписчик на сайте "%siteName%"',
				'body' => '<h2 style="'.$this->getStyle('title').'">Новый подписчик</h2>
					<h3 style="'.$this->getStyle('subtitle').'">на сайте "%siteName%"</h3>
					<p style="font: %font%; text-align: center;">
						Email: %email%
					</p>
				',
			],
		];
	}

	/**
	 * @param $code
	 * @return mixed|string
	 */
	public function getContentBlock($code)
	{
		switch ($code){
			case '%link-subscribe%':
				return '<a style="'.$this->getStyle('btn').'" href="'.Yii::app()->createAbsoluteUrl('/subscribe/subscribe/confirm', ['token' => $this->model->token]).'">Подтвердить подписку &rarr;</a>';
			case '%email%':
				return $this->model->email;
			default:
				return parent::getContentBlock($code);
		}
	}
}
