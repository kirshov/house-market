<?php

class SubscribeNotifyService extends NotifyService
{

	/**
	 * @var string
	 */
	protected $templatePath  = 'subscribe/email';

	/**
	 * @var string
	 */
	protected $messageComponent = 'subscribeMailMessage';

	/**
	 * @param SubscribersEvent $event
	 * @return bool
	 */
	public function sendSubscribeConfirm(SubscribersEvent $event)
	{
		$model = $event->getSubscriber();
		$webforma = Yii::app()->getModule('webforma');
		$to = $model->email;
		$from = $webforma->notifyEmailFrom;

		if(!$to || !$from){
			return false;
		}

		$mailContent = $this->getMailContent($model, 'confirmSubscribe');

		if($mailContent['subject'] && $mailContent['body']){
			foreach (explode(',', $to) as $email) {
				$this->mail->send($from, $email, $mailContent['subject'], $mailContent['body']);
			}
		}

		return true;
	}

	/**
	 * @param SubscribersEvent $event
	 * @return bool
	 */
	public function sendAdminNotify(SubscribersEvent $event)
	{
		/**
		 * @var WebformaModule $webforma
		 */
		$webforma = Yii::app()->getModule('webforma');
		$model = $event->getSubscriber();
		$from = $webforma->notifyEmailFrom;
		$to = $webforma->notifyEmailTo;

		if(!$to || !$from){
			return false;
		}

		$mailContent = $this->getMailContent($model, 'adminNotifySubscribe', true);

		if($mailContent['subject'] && $mailContent['body']){
			foreach (explode(',', $to) as $email) {
				$this->mail->send($from, $email, $mailContent['subject'], $mailContent['body']);
			}
		}

		return true;
	}

}