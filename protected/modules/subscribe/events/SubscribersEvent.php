<?php

class SubscribersEvent extends \webforma\components\Event
{
    private $subscriber;

    public function __construct(Subscribers $subscriber)
    {
        $this->subscriber = $subscriber;
    }

    public function getSubscriber()
    {
        return $this->subscriber;
    }
}