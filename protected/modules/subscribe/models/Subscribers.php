<?php
use webforma\widgets\WPurifier;

/**
 *
 * @property string $id
 * @property string $email
 * @property integer $status
 * @property string $token
 * @property string $ip
 * @property string $referrer
 * @property integer $create_time
 * @property integer $update_time
 *
 * Subscribers model class
 * Класс модели Subscribers
 *
 **/
class Subscribers extends webforma\models\WModel
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 0;

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return Subscribers the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Получаем имя таблицы:
     *
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{subscribers}}';
    }

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'CTimestampBehavior' => [
				'class' => 'zii.behaviors.CTimestampBehavior',
				'setUpdateOnCreate' => true,
			],
		];
	}

    /**
     * Получаем правила валидации полей таблицы:
     *
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['email', 'filter', 'filter' => [$obj = new WPurifier(), 'purify']],
	        ['email', 'required', 'message' => 'Необходимо ввести E-mail'],
	        ['email', 'email', 'message' => 'Введите правильный E-mail'],
	        ['status', 'in', 'range' => array_keys($this->getStatusList())],
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            ['id, email, status, create_time', 'safe', 'on' => 'search'],
        ];
    }


    /**
     * Получаем атрибуты меток полей данной таблицы:
     *
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('SubscribeModule.subscribe', 'ID'),
            'status'      => Yii::t('SubscribeModule.subscribe', 'Status'),
	        'email'       => 'Email',
	        'create_time' => 'Дата подписки',
	        'update_time' => 'Дата обновления',
	        'ip' => 'IP-адрес',
	        'referer' => 'Реферер',
        ];
    }

	/**
     * Retrieves a list of models based on the current search/filter conditions.
     *
	 * @var int $limit
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria();

        $criteria->compare('id', $this->id, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('status', $this->status, true);
        if($this->create_time){
        	$criteria->addCondition('(create_time >= :create_time AND create_time <= :create_time)');
        	$criteria->params[':create_time'] = date('Y-m-d', strtotime($this->create_time));
		}

        return new CActiveDataProvider(get_class($this), [
        	'criteria' => $criteria,
	        'sort' => ['defaultOrder' => 't.create_time DESC'],
        ]);
    }


    /**
     * Получаем массив статусов:
     *
     * @return array status
     **/
    public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE     => 'Подтвержден',
            self::STATUS_NOT_ACTIVE => 'Не подтвержден',
        ];
    }

    /**
     * Получаем статусов записи:
     *
     * @return string status
     **/
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('SubscribeModule.subscribe', '--unknown--');
    }

	protected function beforeSave()
	{
		if($this->getIsNewRecord()){
			$this->token = Yii::app()->getSecurityManager()->generateRandomString(32);
			$this->ip = Yii::app()->getRequest()->getUserHostAddress();
			$this->referrer = \webforma\components\WCookie::getCookie('referrer');
		}
		return parent::beforeSave();
	}
}
