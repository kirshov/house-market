<?php

/**
 *
 * Файл конфигурации модуля
 **/

return [
    'module'    => [
        'class' => 'application.modules.subscribe.SubscribeModule',
    ],
    'import'    => [],
	'rules' => [
		'/subscribe' => '/subscribe/subscribe/index',
		'/subscribe-confirm/<token>' => '/subscribe/subscribe/confirm',
		//'/unsubscribeHook' => '/subscribe/subscribe/unsubscribeHook',
	],
	'params'    => [
		'widgets'   => [
			'subscribe' => 'application.modules.subscribe.widgets.SubscribeWidget',
		],
	],
	'component' => [
		'eventManager' => [
			'class' => 'webforma\components\EventManager',
			'events' => [
				'subscribe.confirm' => [
					['\SubscribeListener', 'onSubscriberNeedConfirm']
				],
				'subscribe.new.subscriber' => [
					['\SubscribeListener', 'onNewSubscriber']
				],
			]
		],
		'subscribeNotifyService' => [
			'class' => 'application.modules.subscribe.components.SubscribeNotifyService',
		],
		'subscribeMailMessage' => [
			'class' => 'application.modules.subscribe.components.SubscribeMailMessage',
		],
	]
];
