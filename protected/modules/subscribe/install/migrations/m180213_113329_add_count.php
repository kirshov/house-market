<?php

class m180213_113329_add_count extends webforma\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{subscribe_template}}', 'last_send_user_id', 'integer(11) default "0"');
	}

	public function safeDown()
	{

	}
}