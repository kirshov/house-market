<?php

class m180131_182652_change extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{subscribe_template}}', 'type_id', 'integer(11) default "0"');
		$this->addColumn('{{subscribe_template}}', 'subscribe_status', 'integer(1) default "0"');
		$this->addColumn('{{subscribe_template}}', 'percent', 'integer(3) default "0"');
		$this->addColumn('{{subscribe_template}}', 'finish_date', 'datetime');
	}

	public function safeDown()
	{

	}
}