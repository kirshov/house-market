<?php

class m180211_132353_add_fields extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->renameColumn('{{subscribe_template}}', 'subscribe_status', 'status');
	}

	public function safeDown()
	{

	}
}