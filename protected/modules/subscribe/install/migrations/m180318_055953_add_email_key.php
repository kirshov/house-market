<?php

class m180318_055953_add_email_key extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->createIndex("ux_{{subscribers}}_email", "{{subscribers}}", "email", true);
	}

	public function safeDown()
	{

	}
}