<?php

class m180311_111328_add_subscriber_token extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{subscribers}}', 'token', 'varchar(32) not null');
		$this->addColumn('{{subscribers}}', 'ip', 'varchar(15) not null');
		$this->addColumn('{{subscribers}}', 'referrer', 'varchar(255) not null');
	}

	public function safeDown()
	{

	}
}