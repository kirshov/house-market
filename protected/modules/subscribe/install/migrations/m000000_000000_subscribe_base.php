<?php

/**
 *
 * Subscribe install migration
 * Класс миграций для модуля Subscribe
 **/
class m000000_000000_subscribe_base extends webforma\components\DbMigration
{

    public function safeUp()
    {
        /**
         * subscribe_event:
         **/
        $this->createTable(
            '{{subscribe_queue}}',
            [
                'id'            => 'pk',
                'subscribe_id'  => 'integer(11) NOT NULL default "0"',
                'email'         => 'varchar(150) NOT NULL',
                'flag'          => 'integer(1) NOT NULL default "0"',
            ],
            $this->getOptions()
        );


        /**
         * subscribe_template:
         **/
        $this->createTable(
            '{{subscribe_template}}',
            [
                'id'          => 'pk',
                'name'        => 'varchar(150) NOT NULL',
                'description' => 'text',
                'theme'       => 'text NOT NULL',
                'body'        => 'text NOT NULL',
                "create_time" => "datetime not null",
                "update_time" => "datetime not null",
            ],
            $this->getOptions()
        );


        $this->createTable(
            '{{subscribers}}',
            [
                'id'            => 'pk',
                'email'         => 'varchar(150) NOT NULL',
                'status'        => 'tinyint(1) NOT NULL default "0"',
                "create_time"   => "datetime not null",
                "update_time"   => "datetime not null",
            ],
            $this->getOptions()
        );

    }

    public function safeDown()
    {
    }
}
