<?php

class m180131_174131_add_from extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{subscribe_template}}', 'from', 'varchar(50) DEFAULT NULL');
	}

	public function safeDown()
	{

	}
}