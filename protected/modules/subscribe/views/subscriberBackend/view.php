<?php
$this->breadcrumbs = [
    Yii::t('SubscribeModule.subscribe', 'Subscribers')    => ['/subscribe/subscriberBackend/index'],
	$model->email => ['view', 'id' => $model->id],
	Yii::t('SubscribeModule.subscribe', 'View'),
];
$this->pageTitle = Yii::t('SubscribeModule.subscribe', 'View subscriber');
$this->menu = [
	['label' => Yii::t('SubscribeModule.subscribe', 'Subscribers')],
	[
		'icon'  => 'fa fa-fw fa-list-alt',
		'label' => Yii::t('SubscribeModule.subscribe', 'Subscribers'),
		'url'   => ['/subscribe/subscriberBackend/index']
	],
	[
		'icon'  => 'fa fa-fw fa-plus-square',
		'label' => Yii::t('SubscribeModule.subscribe', 'Create subscriber'),
		'url'   => ['/subscribe/subscriberBackend/create']
	],
	[
		'icon'  => 'fa fa-fw fa-pencil',
		'label' => Yii::t('SubscribeModule.subscribe', 'Edit subscriber'),
		'url'   => ['/subscribe/subscriberBackend/update', 'id' => $model->id]
	],
	[
		'icon'  => 'fa fa-fw fa-eye',
		'label' => Yii::t('SubscribeModule.subscribe', 'View subscriber'),
		'url'   => ['/subscribe/subscriberBackend/view', 'id' => $model->id]
	],
	[
		'icon'        => 'fa fa-fw fa-trash-o',
		'label'       => Yii::t('SubscribeModule.subscribe', 'Remove subscriber'),
		'url'         => '#',
		'linkOptions' => [
			'submit'  => ['/subscribe/subscriberBackend/delete', 'id' => $model->id],
			'confirm' => Yii::t('SubscribeModule.subscribe', 'Do you really want to remove?'),
			'csrf'    => true,
		]
	],
];
?>

<?php $this->widget(
    'bootstrap.widgets.TbDetailView',
    [
        'data'       => $model,
        'attributes' => [
            'id',
            'email',
            'update_time',
            'create_time',
            'ip',
            [
                'name' => 'referer',
                'type' => 'raw'
            ],
	        [
		        'name' => 'status',
				'value' => function ($data){
    				return $data->getStatus();
				},
	        ],
        ],
    ]
); ?>
