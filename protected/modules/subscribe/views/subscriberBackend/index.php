<?php
$this->breadcrumbs = [
    Yii::t('SubscribeModule.subscribe', 'Subscribers') => ['index'],
    Yii::t('SubscribeModule.subscribe', 'Management'),
];
$this->pageTitle = Yii::t('SubscribeModule.subscribe', 'Subscribers');

$this->menu = [
    ['label' => Yii::t('SubscribeModule.subscribe', 'Subscribers')],
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('SubscribeModule.subscribe', 'Subscribers'),
        'url'   => ['/subscribe/subscriberBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('SubscribeModule.subscribe', 'Create subscriber'),
        'url'   => ['/subscribe/subscriberBackend/create']
    ],
];
?>

<?php
$this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id'             => 'subscribe-grid',
        'dataProvider'   => $model->search(),
        'filter'         => $model,
        'actionsButtons' => [
            CHtml::link(Yii::t('WebformaModule.webforma', 'Add'), ['/subscribe/subscriberBackend/create'], ['class' => 'btn btn-success pull-right btn-sm']),
			'<span class="pull-right">'.CHtml::link('Экспортировать в CSV', ['/subscribe/subscriberBackend/exportCSV'], ['class' => 'btn btn-success btn-sm']).'&nbsp&nbsp</span>',
        ],
        'columns'        => [
            [
                'name'        => 'id',
                'type'        => 'raw',
                'value'       => 'CHtml::link($data->id, array("/subscribe/subscriberBackend/update", "id" => $data->id))',
                'htmlOptions' => ['style' => 'width:20px'],
            ],
			[
				'name'        => 'email',
				'type'        => 'raw',
				'value'       => 'CHtml::link($data->email, array("/subscribe/subscriberBackend/update", "id" => $data->id))',
				'htmlOptions' => ['style' => 'width:33%'],
			],
			[
				'name' => 'create_time',
				'type' => 'html',
				'filter' => $this->widget('booster.widgets.TbDatePicker', [
					'model' => $model,
					'attribute' => 'create_time',
					'options' => [
						'format' => 'dd.mm.yyyy',
						'autoclose' => true,
					],
					'htmlOptions' => [
						'class' => 'form-control',
					],
				], true),
				'value' => function ($data) {
					return date('d.m.Y H:i:s', strtotime($data->create_time));
				},
				'htmlOptions' => ['style' => 'width:33%'],
			],
	        [
		        'class'   => 'webforma\widgets\EditableStatusColumn',
		        'name'    => 'status',
		        'url'     => $this->createUrl('/subscribe/subscriberBackend/inline'),
		        'source'  => $model->getStatusList(),
		        'options' => [
			        Subscribers::STATUS_ACTIVE     => ['class' => 'label-success'],
					Subscribers::STATUS_NOT_ACTIVE => ['class' => 'label-warning'],
		        ],
				'htmlOptions' => ['style' => 'width:33%'],
	        ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
            ],
        ],
    ]
); ?>
