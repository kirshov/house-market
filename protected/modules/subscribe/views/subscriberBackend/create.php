<?php
$this->breadcrumbs = [
    Yii::t('SubscribeModule.subscribe', 'Subscribers') => ['index'],
    Yii::t('SubscribeModule.subscribe', 'Create'),
];

$this->menu = [
    ['label' => Yii::t('SubscribeModule.subscribe', 'Subscribers')],
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('SubscribeModule.subscribe', 'Subscribers'),
        'url'   => ['/subscribe/subscriberBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('SubscribeModule.subscribe', 'Create subscriber'),
        'url'   => ['/subscribe/subscriberBackend/create']
    ],
];
?>
<?=  $this->renderPartial('_form', ['model' => $model]); ?>
