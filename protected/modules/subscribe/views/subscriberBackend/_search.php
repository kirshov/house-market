<?php
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'action'      => Yii::app()->createUrl($this->route),
        'method'      => 'get',
        'htmlOptions' => ['class' => 'well search-form'],
    ]
);
?>
<fieldset>
    <div class="row">
        <div class="col-sm-3">
            <?=  $form->textFieldGroup($model, 'description'); ?>
        </div>
        <div class="col-sm-3">
            <?=  $form->textFieldGroup($model, 'from'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <?=  $form->textFieldGroup($model, 'theme'); ?>
        </div>
        <div class="col-sm-3">
            <?=  $form->textFieldGroup($model, 'body'); ?>
        </div>
    </div>
    <div class="form-actions">
        <?php
        $this->widget(
            'bootstrap.widgets.TbButton',
            [
                'buttonType'  => 'submit',
                'context'     => 'primary',
                'encodeLabel' => false,
                'label'       => '<i class="fa fa-search"></i> ' . Yii::t('SubscribeModule.subscribe', 'Find')
            ]
        );
        ?>
    </div>
</fieldset>

<?php $this->endWidget(); ?>
