<?php
/**
 * @var $this SubscriberBackendController
 * @var $model Subscribers
 * @var $form \webforma\widgets\ActiveForm
 */
$form = $this->beginWidget(
    '\webforma\widgets\ActiveForm',
    [
        'id'                     => 'subscribe-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'type'                   => 'vertical',
        'htmlOptions'            => ['class' => 'well sticky'],
    ]
); ?>

<div class="alert alert-info">
    <?=  Yii::t('SubscribeModule.subscribe', 'Fields, with'); ?>
    <span class="required">*</span>
    <?=  Yii::t('SubscribeModule.subscribe', 'are required.'); ?>
</div>

<?=  $form->errorSummary($model); ?>

<div class='row'>
    <div class="col-sm-3">
        <?=  $form->textFieldGroup($model, 'email'); ?>
    </div>

    <div class="col-sm-3 form-group">
        <?= $form->dropDownListGroup($model, 'status', [
			'widgetOptions' => [
				'data'        => $model->getStatusList(),
			],
		]); ?>
    </div>
</div>

<div class="buttons">
	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context'    => 'primary',
		'label'      => $model->isNewRecord
			? Yii::t('SubscribeModule.subscribe', 'Create and continue')
			: Yii::t('SubscribeModule.subscribe', 'Save and continue'),
	]); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType'  => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label'       => $model->isNewRecord
			? Yii::t('SubscribeModule.subscribe', 'Create and close')
			: Yii::t('SubscribeModule.subscribe', 'Save and close'),
	]); ?>
</div>

<?php $this->endWidget(); ?>
