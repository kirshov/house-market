<?php

/**
 * Class CallbackController
 */
class SubscribeController extends \webforma\components\controllers\FrontController
{
    /**
     * @throws CHttpException
     */
    public function actionIndex()
    {
        $request = Yii::app()->getRequest();
	    $post = $request->getPost('Subscribers');
        if (!$request->getIsAjaxRequest() || !$request->getIsPostRequest() || !$post) {
	        throw new CHttpException(404, Yii::t('WebformaModule.webforma', 'Page not found!'));
        }

        $model = Subscribers::model()->find('email = :email', [':email' => $post['email']]);

        if($model) {
        	if($model->status == Subscribers::STATUS_ACTIVE){
		        Yii::app()->ajax->success('Вы уже подписаны на наши новости');
	        }
        } else {
	        $model = new Subscribers();
	        $model->setAttributes($post);
	        if(!$model->save()){
		        Yii::app()->ajax->rawText(CActiveForm::validate($model));
	        }
        }

	    Yii::app()->eventManager->fire(SubscribeEvents::SUBSCRIBE_CONFIRM, new SubscribersEvent($model));
	    Yii::app()->ajax->success('На Ваш e-mail адрес отправлено письмо с подтверждением подписки');
    }

    public function actionConfirm($token){
	    $model = Subscribers::model()->find('token = :token', [':token' => $token]);
	    if($model){
	    	$model->status = Subscribers::STATUS_ACTIVE;
	    	$model->token = '';
	    	$status = $model->save();
		    Yii::app()->eventManager->fire(SubscribeEvents::SUBSCRIBE_NEW_SUBSCRUBER, new SubscribersEvent($model));
	    } else {
	    	$status = false;
	    }

	    $this->render('confirm', [
	    	'status' => $status,
	    ]);
    }

    public function actionUnsubscribeHook()
	{
		$file = Yii::getPathOfAlias('public').'/un.log';
		$json_string = file_get_contents('php://input');
		$data_array = json_decode($json_string, true);

		Yii::log(print_r($data_array, true));
		file_put_contents($file, print_r($data_array, true), FILE_APPEND);
	}
}