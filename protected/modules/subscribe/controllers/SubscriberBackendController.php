<?php

/**
 * SubscriberBackendController
 **/
class SubscriberBackendController extends webforma\components\controllers\BackController
{
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Subscribe.SubscriberBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Subscribe.SubscriberBackend.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['Subscribe.SubscriberBackend.Create']],
            ['allow', 'actions' => ['update', 'inline'], 'roles' => ['Subscribe.SubscriberBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Subscribe.SubscriberBackend.Delete']],
            ['deny']
        ];
    }

    public function actions()
    {
        return [
            'inline' => [
                'class'           => 'webforma\components\actions\WInLineEditAction',
                'model'           => 'Subscribers',
                'validAttributes' => ['email', 'status']
            ]
        ];
    }

    /**
     * @param integer $id
     *
     * @return void
     */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }

    /**
     * @return void
     */
    public function actionCreate()
    {
        $model = new Subscribers();

        if (($data = Yii::app()->getRequest()->getPost('Subscribers')) !== null) {
            $model->setAttributes($data);

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('SubscribeModule.subscribe', 'Record was created!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['create']
                    )
                );
            }
        }

        $this->render('create', ['model' => $model]);
    }

    /**
     * @return void
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (($data = Yii::app()->getRequest()->getPost('Subscribers')) !== null) {

            $model->setAttributes($data);

            if ($model->save()) {
				Yii::app()->user->setFlash(
					webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
					Yii::t('SubscribeModule.subscribe', 'Record was updated!')
				);

				$this->redirect((array)Yii::app()->getRequest()->getPost('submit-type', ['update', 'id' => $model->id]));
            }
        }

        $this->render('update', ['model' => $model]);
    }

    /**
     * @param integer $id
     *
     * @return void
     *
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                Yii::t('SubscribeModule.subscribe', 'Record was removed!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            Yii::app()->getRequest()->getParam('ajax') !== null || $this->redirect(
                (array)Yii::app()->getRequest()->getPost('returnUrl', 'index')
            );
        } else {
            throw new CHttpException(
                400,
                Yii::t('SubscribeModule.subscribe', 'Bad request. Please don\'t repeate similar request anymore')
            );
        }
    }

    /**
     * @return void
     */
    public function actionIndex()
    {
        $model = new Subscribers('search');

        $model->unsetAttributes(); // clear any default values

        $model->setAttributes(Yii::app()->getRequest()->getParam('Subscribers', []));

        $this->render('index', ['model' => $model]);
    }

    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param integer $id - идентификатор нужной модели
     *
     * @return \webforma\models\WModel $model
     *
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Subscribers::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(
                404,
                Yii::t('SubscribeModule.subscribe', 'Requested page was not found')
            );
        }

        return $model;
    }

	/**
	 *
	 */
    public function actionExportCSV(){
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"".date('Подписчики_'.Yii::app()->name.'_d-m-Y_His').".csv\"");
		header("Pragma: no-cache");
		header("Expires: 0");

		$out = fopen('php://output', 'w');
		fputcsv($out, ['ID','E-mail','Дата подписки', 'Подтвержден'], ';');

		$list = new CDataProviderIterator(Subscribers::model()->search(), 100);
		foreach ($list as $subscriber) {
			fputcsv($out, [$subscriber->id, $subscriber->email, $subscriber->create_time, $subscriber->getStatus()], ';');
		}
		fclose($out);
		Yii::app()->end();
	}
}
