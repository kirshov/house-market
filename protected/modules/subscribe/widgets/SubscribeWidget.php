<?php
Yii::import('application.modules.subscribe.SubscribeModule');
Yii::import('application.modules.subscribe.models.Subscribers');
/**
 * Class SubscribeWidget
 *
 * @property string $view Widget view file
 */
class SubscribeWidget extends \webforma\widgets\WWidget
{
    /**
     * @var string
     */
    public $view = 'index';

	/**
	 * @throws CException
	 */
	public function init()
	{
		Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getAssetManager()->publish(
			Yii::getPathOfAlias('application.modules.subscribe.views.web') . '/subscribe.js'
		), CClientScript::POS_END);

		parent::init();
	}

	/**
     * @return bool
     * @throws CException
     */
    public function run()
    {
	    $model = new Subscribers();
        $this->render(
            $this->view,
            [
                'model' => $model,
            ]
        );
    }
}