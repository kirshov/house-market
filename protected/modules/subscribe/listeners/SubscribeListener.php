<?php
use webforma\components\Event;

/**
 * Class SubscribeListener
 */
class SubscribeListener extends \webforma\components\BaseListener
{
    /**
     * @param SubscribersEvent $event
     */
    public static function onSubscriberNeedConfirm(SubscribersEvent $event)
    {
    	Yii::app()->getComponent('subscribeNotifyService')->sendSubscribeConfirm($event);
    }

	/**
	 * @param SubscribersEvent $event
	 * @return bool
	 */
	public static function onNewSubscriber(SubscribersEvent $event)
	{
		Yii::app()->getComponent('subscribeNotifyService')->sendAdminNotify($event);

		/**
		 * @var SubscribeModule $module
		 */
		$module = Yii::app()->getModule('subscribe');
		if($module->useSendpulse){
			if(!$module->sendpulseApiUserId || !$module->sendpulseApiUserSecret || !$module->sendpulseApiBookId){
				return false;
			}


			$SPApiClient = new \Sendpulse\RestApi\ApiClient($module->sendpulseApiUserId, $module->sendpulseApiUserSecret);

			$data = [
				[
					'email' => $event->getSubscriber()->email,
					/*'variables' => [
						'name' => $form->getName(),
					]*/
				]
			];
			$SPApiClient->addEmails($module->sendpulseApiBookId, json_encode($data));
		}
	}
}