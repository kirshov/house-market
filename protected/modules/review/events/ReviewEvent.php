<?php

use webforma\components\Event;

/**
 * Class CommentEvent
 */
class ReviewEvent extends Event
{
    /**
     * @var Review
     */
    protected $review;

    /**
     * @param Review $review
     */
    public function __construct(Review $review)
    {
        $this->review = $review;
    }

    /**
     * @param mixed $review
     */
    public function setReview($review)
    {
        $this->review = $review;
    }

    /**
     * @return mixed
     */
    public function getReview()
    {
        return $this->review;
    }
}
