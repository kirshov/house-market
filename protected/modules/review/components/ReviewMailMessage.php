<?php
class ReviewMailMessage extends WMailMessage{
	/**
	 * @var Review
	 */
	protected $model;

	/**
	 * @var string
	 */
	protected $module = 'review';

	public function getDefaultMessages()
	{
		return [
			'newReviewAdmin' => [
				'name' => 'Сообщение администратору о новом отзыве',
				'subject' => 'Оставлен новый отзыв на сайте "%siteName%"',
				'body' => '<h2 style="'.$this->getStyle('title').'">Новый отзыв</h2>
					<h3 style="'.$this->getStyle('subtitle').'">на сайте "%siteName%"</h3>
					<p style="font: %font%">
						Отправитель: %autor%<br/>
						Оценка: %rating%<br/>
						Текст: %text%<br/>
					</p>
				',
			],

			'replyReviewUser' => [
				'name' => 'Сообщение пользователю об ответе на комментарий',
				'subject' => 'Получен ответ на ваш отзыв на сайте "%siteName%"',
				'body' => '<h2 style="'.$this->getStyle('title').'">Ответ на Ваш отзыв</h2>
					<h3 style="'.$this->getStyle('subtitle').'">на сайте "%siteName%"</h3>
					<p style="font: %font%">
						Текст ответа: %answer%<br/>
					</p>
					<div align="center">%link%</div>
				',
			],
		];
	}

	/**
	 * @param $code
	 * @return mixed|string
	 */
	public function getContentBlock($code)
	{
		switch ($code){
			case '%autor%':
				return CHtml::encode($this->model->name);
			case '%rating%':
				return CHtml::encode($this->model->getRatingTitle());
			case '%text%':
				return CHtml::encode($this->model->text);
			case '%answer%':
				return strip_tags($this->model->admin_text);
			case '%link%':
				return $this->getModuleLink();
			default:
				return parent::getContentBlock($code);
		}
	}


	/**
	 * @return string
	 */
	public function getModuleLink(){
		return '<a href="'.Yii::app()->createAbsoluteUrl('/review/review/index/').'" style="'.$this->getStyle('btn').'">
			Перейти к отзывам
		</a>';
	}
}