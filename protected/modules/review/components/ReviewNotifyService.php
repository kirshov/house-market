<?php

class ReviewNotifyService extends NotifyService
{

	/**
	 * @var string
	 */
	protected $templatePath  = 'review/review/email';

	/**
	 * @var string
	 */
	protected $messageComponent = 'reviewMailMessage';

	/**
	 * @param Review $model
	 * @return bool
	 */
	public function sendCreatedAdminNotify(Review $model)
	{
		$from = Yii::app()->getModule('webforma')->notifyEmailFrom;
		$to = Yii::app()->getModule('webforma')->notifyEmailTo;

		if(!$to || !$from){
			return false;
		}
		$code = 'newReviewAdmin';

		$mailContent = $this->getMailContent($model, $code, true);

		if($mailContent['subject'] && $mailContent['body']){
			foreach (explode(',', $to) as $email) {
				$this->mail->send($from, $email, $mailContent['subject'], $mailContent['body']);
			}
		}

		return true;
	}


	/**
	 * @param Review $model
	 * @return bool
	 */
	public function sendReplyUserNotify(Review $model)
	{
		if(!$model->email){
			return false;
		}
		$from = Yii::app()->getModule('webforma')->notifyEmailFrom;
		$to = $model->email;

		if(!$to || !$from){
			return false;
		}

		$mailContent = $this->getMailContent($model, 'replyReviewUser', true);

		if($mailContent['subject'] && $mailContent['body']){
			foreach (explode(',', $to) as $email) {
				$this->mail->send($from, $email, $mailContent['subject'], $mailContent['body']);
			}
		}

		return true;
	}
}