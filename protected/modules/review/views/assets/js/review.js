function reviewSendForm(form, data, hasError) {
    if (hasError) return false;

    var resultField = $('.result-message');
    resultField
        .html('')
        .removeClass('bs-callout-danger')
        .removeClass('bs-callout-success')
        .addClass('hidden');

    $.ajax({
        url: form.attr('action'),
        type: 'POST',
        data: form.serialize(),
        dataType: 'json',
        success: function (response) {
            if (response.result) {
                form.trigger('reset');
            }
            if(response.result){
                resultField.html('<p>' + response.data.message + '</p>');
                resultField.addClass('bs-callout-success');
            } else {
                if(response.data){
                    resultField.append(response.data);
                } else if(typeof response == 'object' && Object.keys(response).length){
                    $.each(response, function(index, value){
                        $.each(value, function(key, item){
                            resultField.append('<p>' + item + '</p>');
                        });
                    });
                }
                resultField.addClass('bs-callout-danger');
            }
            resultField.removeClass('hidden');
        },
        error: function () {
            resultField
                .html('Извините, при обработке запроса возникла ошибка')
                .addClass('bs-callout-danger')
                .removeClass('hidden');
        }
    });
    form.find("button[type='submit']").prop("disabled", false);
    return false;
}