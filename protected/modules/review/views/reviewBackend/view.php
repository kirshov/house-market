<?php
$this->breadcrumbs = [
    Yii::t('ReviewModule.review', 'Reviews') => ['/review/reviewBackend/index'],
	$model->name.' '.date('d.m.Y', strtotime($model->create_time)),
];

$this->pageTitle = Yii::t('ReviewModule.review', 'Reviews - show');

$this->menu = [
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('ReviewModule.review', 'Manage reviews'),
        'url'   => ['/review/reviewBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('ReviewModule.review', 'Create review'),
        'url'   => ['/review/reviewBackend/create']
    ],
    [
        'icon'  => 'fa fa-fw fa-pencil',
        'label' => Yii::t('ReviewModule.review', 'Edit review'),
        'url'   => [
            '/review/reviewBackend/update',
            'id' => $model->id
        ]
    ],
    [
        'icon'  => 'fa fa-fw fa-eye',
        'label' => Yii::t('ReviewModule.review', 'View review'),
        'url'   => [
            '/review/reviewBackend/view',
            'id' => $model->id
        ]
    ],
    [
        'icon'        => 'fa fa-fw fa-trash-o',
        'label'       => Yii::t('ReviewModule.review', 'Delete review'),
        'url'         => '#',
        'linkOptions' => [
            'submit'  => ['/review/reviewBackend/delete', 'id' => $model->id],
            'params'  => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
            'confirm' => Yii::t('ReviewModule.review', 'Do you really want do remove review?'),
        ]
    ],
];
?>

<?php $this->widget(
    'bootstrap.widgets.TbDetailView',
    [
        'data'       => $model,
        'attributes' => [
            'id',
            [
                'name'  => 'create_time',
                'value' => Yii::app()->getDateFormatter()->formatDateTime($model->create_time, "short", "short"),
            ],
            'name',
            'phone',
            'email',
            [
                'name' => 'text',
                'type' => 'raw'
            ],
            [
                'name'  => 'status',
                'value' => $model->getStatus(),
            ],
            'ip',
        ],
    ]
); ?>
