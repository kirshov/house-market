<?php
$this->breadcrumbs = [
    Yii::t('ReviewModule.review', 'Reviews') => ['/review/reviewBackend/index'],
    Yii::t('ReviewModule.review', 'Manage'),
];

$this->pageTitle = Yii::t('ReviewModule.review', 'Reviews - management');

$this->menu = [
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('ReviewModule.review', 'Reviews list'),
        'url'   => ['/review/reviewBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('ReviewModule.review', 'Create review'),
        'url'   => ['/review/reviewBackend/create']
    ],
    [
        'icon' => 'fa fa-fw fa-external-link-square',
        'label' => Yii::t('WebformaModule.webforma', 'To the section on the site'),
        'url' => ['/review/review/index'],
    ],
];
?>

<?php $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id'           => 'review-grid',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'actionsButtons' => [
            'approve' => CHtml::link(Yii::t('ReviewModule.review', 'Approve'), '#', [
                    'id' => 'approve-reviews',
                    'class' => 'btn btn-sm btn-info pull-right disabled bulk-actions-btn',
                    'style' => 'margin-left: 4px;'
                ]
            ),
            'add' => CHtml::link(
                Yii::t('ReviewModule.review', 'Add'),
                ['/review/reviewBackend/create'],
                ['class' => 'btn btn-sm btn-success pull-right']
            ),
        ],
        'columns'      => [
           /* [
                'name'  => 'text',
                'value' => 'webforma\helpers\WText::characterLimiter($data->getText(), 5)',
                'type'  => 'html'
            ],*/

            [
                'type' => 'raw',
                'value' => function ($data) {
                    if(!$data->image){
                        return '';
                    }
                    return CHtml::image($data->getImageUrl(100, 100), $data->title, ["width" => 100, "height" => 100, "class" => "img-thumbnail"]);
                },
                'htmlOptions' => [
                    'style' => 'width: 80px;',
                ],
                //'visible' => $module->useImage,
                'filter' => CHtml::activeTextField($model, 'id', ['class' => 'form-control']),
            ],
            'name',
            [
                'name'  => 'text',
                'value' => 'webforma\helpers\WText::characterLimiter($data->getText(), 50)',
                'type'  => 'html'
            ],
            [
                'class' => 'webforma\widgets\EditableStatusColumn',
                'name' => 'producer_id',
                'url' => $this->createUrl('/store/productBackend/inline'),
                'source' => CMap::mergeArray(
                    ['' => '---'],
                    Producer::model()->getFormattedList()
                ),
                'editable' => [
                    'mode' => 'popup',
                    'emptytext' => '---',
                ],
            ],
            [
                'class'   => 'webforma\widgets\EditableStatusColumn',
                'name'    => 'status',
                'url'     => $this->createUrl('/review/reviewBackend/inline'),
                'source'  => $model->getStatusList(),
                'options' => [
                    Review::STATUS_APPROVED   => ['class' => 'label-success'],
                    Review::STATUS_DELETED    => ['class' => 'label-default'],
                    Review::STATUS_NEED_CHECK => ['class' => 'label-warning'],
                    Review::STATUS_SPAM       => ['class' => 'label-danger'],
                ],
            ],
            [
                'name'  => 'create_time',
                'value' => 'Yii::app()->getDateFormatter()->formatDateTime($data->create_time, "short", "short")',
				'htmlOptions' => [
					'style' => 'width:130px;',
				],
            ],
            /*'email',*/
	        /*[
		        'class' => 'webforma\widgets\WEditableColumn',
		        'editable' => [
			        'url' => $this->createUrl('/review/reviewBackend/inline'),
			        'type' => 'select',
			        'source' => $model->getRatingRanges(),
			        'params' => [
				        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
			        ]
		        ],
		        'name' => 'rating',
		        'type' => 'raw',
		        'value' => '$data->getRatingTitle()',
		        'filter' => CHtml::activeDropDownList($model, 'rating', $model->getRatingRanges(),[
					'class' => 'form-control',
					'empty' => '',
				]),
	        ],*/
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
            ],
        ],
    ]
);

$url = Yii::app()->createUrl('/review/reviewBackend/approve');
$tokenName = Yii::app()->getRequest()->csrfTokenName;
$token = Yii::app()->getRequest()->csrfToken;
$confirmMessage = Yii::t('ReviewModule.review', 'Do you really want to approve selected elements?');
$noCheckedMessage = Yii::t('ReviewModule.review', 'No items are checked');
$errorMessage = Yii::t('ReviewModule.review', 'Error!');

Yii::app()->getClientScript()->registerScript(
    __FILE__,
    <<<JS
    $('body').on('click', '#approve-reviews', function (e) {
        e.preventDefault();
        var checked = $.fn.yiiGridView.getCheckedRowsIds('review-grid');
        if (!checked.length) {
            alert("$noCheckedMessage");
            return false;
        }
        var url = "$url";
        var data = {};
        data['$tokenName'] = "$token";
        data['items'] = checked;
        if(confirm("$confirmMessage")){
            $.ajax({
                url: url,
                type: "POST",
                dataType: "json",
                data: data,
                success: function (data) {
                    if (data.result) {
                        $.fn.yiiGridView.update("review-grid");
                    } else {
                        alert(data.data);
                    }
                },
                error: function (data) {alert("$errorMessage")}
            });
        }
    });
JS
, CClientScript::POS_READY
);

