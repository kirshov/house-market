<?php
$this->breadcrumbs = [
    Yii::t('ReviewModule.review', 'Reviews') => ['/review/reviewBackend/index'],
    $model->name.' '.date('d.m.Y', strtotime($model->create_time)) => ['/review/reviewBackend/view', 'id' => $model->id],
    Yii::t('ReviewModule.review', 'Edit'),
];

$this->pageTitle = Yii::t('ReviewModule.review', 'Reviews - edit');

$this->menu = [
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('ReviewModule.review', 'Manage reviews'),
        'url'   => ['/review/reviewBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('ReviewModule.review', 'Create review'),
        'url'   => ['/review/reviewBackend/create']
    ],
    [
        'icon'  => 'fa fa-fw fa-pencil',
        'label' => Yii::t('ReviewModule.review', 'Edit review'),
        'url'   => [
            '/review/reviewBackend/update',
            'id' => $model->id
        ]
    ],
    [
        'icon'  => 'fa fa-fw fa-eye',
        'label' => Yii::t('ReviewModule.review', 'View review'),
        'url'   => [
            '/review/reviewBackend/view',
            'id' => $model->id
        ]
    ],
    [
        'icon'        => 'fa fa-fw fa-trash-o',
        'label'       => Yii::t('ReviewModule.review', 'Delete review'),
        'url'         => '#',
        'linkOptions' => [
            'submit'  => ['/review/reviewBackend/delete', 'id' => $model->id],
            'params'  => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
            'confirm' => Yii::t('ReviewModule.review', 'Do you really want do remove review?'),
        ]
    ],
];
?>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>
