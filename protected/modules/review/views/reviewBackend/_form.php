<?php
$form = $this->beginWidget(
	'\webforma\widgets\ActiveForm',
    [
        'id' => 'review-form',
	    'enableAjaxValidation' => false,
	    'enableClientValidation' => false,
	    'type' => 'vertical',
		'htmlOptions' => ['class' => 'well sticky', 'enctype' => 'multipart/form-data'],
	    'clientOptions' => [
		    'validateOnSubmit' => true,
	    ],
    ]
); ?>
<div class="alert alert-info">
    <?=  Yii::t('ReviewModule.review', 'Fields with'); ?>
    <span class="required">*</span>
    <?=  Yii::t('ReviewModule.review', 'are require.'); ?>
</div>

<?=  $form->errorSummary($model); ?>

<div class='row'>
    <div class="col-sm-6">
		<div class='row'>
			<div class="col-sm-6">
				<?=  $form->textFieldGroup($model, 'name'); ?>
			</div>
			<div class="col-sm-6">
				<?=  $form->dropDownListGroup($model, 'status', [
					'widgetOptions' => [
						'data' => $model->getStatusList(),
					],
				]); ?>
			</div>
		</div>
		<?php/*
		<div class='row'>
			<div class="col-sm-6">
				<?=  $form->textFieldGroup($model, 'phone'); ?>
			</div>
			<div class="col-sm-6">
				<?=  $form->textFieldGroup($model, 'email'); ?>
			</div>
		</div>*/?>
		<div class='row'>
			<div class="col-sm-6">
				<?= $form->dropDownListGroup($model, 'producer_id', [
					'widgetOptions' => [
						'data' => Producer::model()->getFormattedList(),
						'htmlOptions' => [
							'empty' => '---',
						],
					],
				]); ?>
			</div>

			<div class="col-sm-6">
				<?= $form->dateTimePickerGroup($model, 'create_time', [
					'widgetOptions' => [
						'options' => [
							'format' => 'dd.mm.yyyy hh:ii:ss',
							'weekStart' => 1,
							'autoclose' => true,
						],
					],
					'prepend' => '<i class="fa fa-calendar"></i>',
				]);?>
			</div>
		</div>
	</div>


	<div class="col-sm-6">
		<div class='row'>
			<div class="col-sx-8 col-sm-8 col-sm-offset-2">
				<?php $this->widget('webforma\widgets\WInputFile', [
					'model' => $model,
					'attribute' => 'image',
				]); ?>
			</div>
		</div>
	</div>
</div>
<div class='row'>
    <div class="col-sm-12">
        <?=  $form->labelEx($model, 'text'); ?>
        <?php $this->widget($this->module->getVisualEditor(), [
			'model'     => $model,
			'attribute' => 'text',
		]); ?>
    </div>

</div>

<br/>
<div class='row'>
	<div class="col-sm-12">
		<?=  $form->textAreaGroup($model, 'admin_text'); ?>
		<div class="help-block small">Если заполен код видео, то текст отзыва выводиться не будет</div>
	</div>
</div>

<div class="buttons">
	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context'    => 'primary',
		'label'      => $model->isNewRecord
			? Yii::t('ReviewModule.review', 'Create review and continue')
			: Yii::t('ReviewModule.review','Save review and continue'),
	]); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType'  => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label'       => $model->isNewRecord
			? Yii::t('ReviewModule.review','Create review and close')
			: Yii::t('ReviewModule.review','Save review and close'),
	]); ?>
</div>
<?php $this->endWidget(); ?>
