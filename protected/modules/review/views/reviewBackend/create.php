<?php
$this->breadcrumbs = [
    Yii::t('ReviewModule.review', 'Reviews') => ['/review/reviewBackend/index'],
    Yii::t('ReviewModule.review', 'Create'),
];

$this->pageTitle = Yii::t('ReviewModule.review', 'Review - create');

$this->menu = [
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('ReviewModule.review', 'Reviews list'),
        'url'   => ['/review/reviewBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('ReviewModule.review', 'Create review'),
        'url'   => ['/review/reviewBackend/create']
    ],
];
?>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>
