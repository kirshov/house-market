<?php

/**
 * ReviewBackendController контроллер для управления комментариями в панели управления
 */
class ReviewBackendController extends webforma\components\controllers\BackController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Review.ReviewBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Review.ReviewBackend.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['Review.ReviewBackend.Create']],
            ['allow', 'actions' => ['update', 'inline', 'approve'], 'roles' => ['Review.ReviewBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Review.ReviewBackend.Delete']],
            ['deny']
        ];
    }

    /**
     * @throws CHttpException
     */
    public function actionInline()
    {
        if (!Yii::app()->getRequest()->getIsAjaxRequest() || !Yii::app()->getRequest()->getIsPostRequest()) {
            throw new CHttpException(404);
        }

        $name = Yii::app()->getRequest()->getPost('name');
        $value = Yii::app()->getRequest()->getPost('value');
        $pk = (int)Yii::app()->getRequest()->getPost('pk');

        if (!isset($name, $value, $pk)) {
            throw new CHttpException(404);
        }

        if (!in_array($name, ['status', 'rating'])) {
            throw new CHttpException(404);
        }

        $model = Review::model()->findByPk($pk);

        if (null === $model) {
            throw new CHttpException(404);
        }

        $model->$name = $value;

        if ($model->save()) {
            Yii::app()->ajax->success();
        }

        throw new CHttpException(500, $model->getError($name));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Review();

        if (($data = Yii::app()->getRequest()->getPost('Review')) !== null) {

            $model->setAttributes($data);

            // Если указан parent_id просто добавляем новый комментарий.
            $saveStatus = $model->save();

            if ($saveStatus) {
                Yii::app()->getUser()->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('ReviewModule.review', 'Review was created!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['create']
                    )
                );
            }
        } else {
        	$model->status = Review::STATUS_APPROVED;
        }
        $this->render('create', ['model' => $model]);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
		$oldAdminText = $model->admin_text;
        if (($data = Yii::app()->getRequest()->getPost('Review')) !== null) {

            $model->setAttributes($data);

            if ($model->save()) {
                Yii::app()->getUser()->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('ReviewModule.review', 'Review was updated!')
                );

				if(!$oldAdminText && $model->admin_text){
					Yii::app()->getComponent('reviewNotifyService')->sendReplyUserNotify($model);
				}

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['update', 'id' => $model->id]
                    )
                );
            }
        }
        $this->render('update', ['model' => $model]);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            $model = $this->loadModel($id);

            $model->delete();

            Yii::app()->getRequest()->getParam('ajax') !== null || $this->redirect(
                (array)Yii::app()->getRequest()->getPost('returnUrl', 'index')
            );
        } else {
            throw new CHttpException(400, Yii::t(
                'ReviewModule.review',
                'Bad request. Please don\'t repeate similar requests anymore'
            ));
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $model = new Review('search');

        $model->unsetAttributes(); // clear any default values

        $model->setAttributes(
            Yii::app()->getRequest()->getParam(
                'Review',
                []
            )
        );

        $this->render('index', ['model' => $model]);
    }

    /**
     * @throws CHttpException
     */
    public function actionMultiaction()
    {
        if (!Yii::app()->getRequest()->getIsAjaxRequest() || !Yii::app()->getRequest()->getIsPostRequest()) {
            throw new CHttpException(404);
        }

        $items = Yii::app()->getRequest()->getPost('items');

        if (!is_array($items) || empty($items)) {
            Yii::app()->ajax->success();
        }

	    $models = Review::model()->findAllByPk($items);
        if ($models) {
	        $count = 0;

	        foreach ($models as $model) {
		        $count += (int)$model->delete();
	        }

            Yii::app()->ajax->success(
                Yii::t(
                    'WebformaModule.webforma',
                    'Removed {count} records!',
                    [
                        '{count}' => $count
                    ]
                )
            );
        } else {
            Yii::app()->ajax->failure(
                Yii::t('WebformaModule.webforma', 'There was an error when processing the request')
            );
        }
    }

    /**
     * @throws CHttpException
     */
    public function actionApprove()
    {
        if (!Yii::app()->getRequest()->getIsAjaxRequest() || !Yii::app()->getRequest()->getIsPostRequest()) {
            throw new CHttpException(404);
        }

        if ($items = Yii::app()->getRequest()->getPost('items')) {
            if (Review::multiApprove($items)) {
                Yii::app()->ajax->success();
            } else {
                Yii::app()->ajax->failure();
            }
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = Review::model()->findByPk((int)$id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('ReviewModule.review', 'Requested page was not found!'));
        }

        return $model;
    }
}
