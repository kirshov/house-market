<?php

/**
 * Review controller class
 * Класс для обработки комментариев на фронт-части.
 **/
class ReviewController extends \webforma\components\controllers\FrontController
{
    /**
     * Объявляем действия:
     *
     * @return mixed actions
     **/
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'webforma\components\actions\WCaptchaAction',
            ]
        ];
    }

	/**
	 * @throws CException
	 */
    public function actionIndex(){
    	Yii::app()->getModule('video');
        Yii::app()->getUser()->setState('spamField', md5(Yii::app()->userManager->hasher->generateRandomToken(8)));
        Yii::app()->getUser()->setState('spamFieldValue', md5(Yii::app()->userManager->hasher->generateRandomToken(8)));

        $user = Yii::app()->getUser();
        $model = new Review();
        if ($user->isAuthenticated($model)) {
            $model->setAttributes([
                'user_id' => $user->getId(),
                'name' => $user->getName(),
                'email' => $user->getProfileField('email'),
                'phone' => $user->getState('phone'),
            ]);
        }

        $dbCriteria = new CDbCriteria([
            'condition' => 't.status = :status',
            'params' => [
                ':status' => Review::STATUS_APPROVED,
            ],
            'order' => 't.create_time DESC',
        ]);

        $type = '';
        /*if(Yii::app()->getRequest()->getParam('type') == 'video'){
			$type = 'video';
			$dataProvider = new CActiveDataProvider(
				'VideoItem', [
					'criteria' => [
						'condition' => 't.gallery_id = :id AND t.status = :status',
						'params' => [
							':id' => 1,
							':status' => VideoItem::STATUS_ACTIVE,
						],
					],
					'sort' => [
						'defaultOrder' => 't.sort DESC',
					],
					'pagination' => [
						'pageSize' => (int)$this->getModule()->perPage,
					],
				]
			);
		} else {
			$dataProvider = new CActiveDataProvider('Review', [
				'criteria' => $dbCriteria,
				'pagination' => [
					'pageSize' => (int)$this->getModule()->perPage,
					'pageVar' => 'page',
				],
			]);
		}*/

		$dbCriteria->addCondition('t.producer_id IS NULL');
		if(Yii::app()->getRequest()->getParam('type') == 'video'){
			$type = 'video';
			$dbCriteria->addCondition('t.admin_text != ""');
		} else {
			$dbCriteria->addCondition('t.admin_text = ""');
		}

		$dataProvider = new CActiveDataProvider('Review', [
			'criteria' => $dbCriteria,
			'pagination' => [
				'pageSize' => (int)$this->getModule()->perPage,
				'pageVar' => 'page',
			],
		]);

        $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model,
            'type' => $type,
        ]);
    }

    /**
     * Action добавления комментария
     *
     *
     **/
    public function actionAdd()
    {
        if (!Yii::app()->getRequest()->getIsAjaxRequest() || !Yii::app()->getRequest()->getIsPostRequest() || !Yii::app()->getRequest()->getPost('Review')) {
            throw new CHttpException(404);
        }

        $module = Yii::app()->getModule('review');

        if (!$module->allowGuestReview && !Yii::app()->getUser()->isAuthenticated()) {
            throw new CHttpException(404);
        }

        if (Yii::app()->getRequest()->getIsAjaxRequest() && Yii::app()->getRequest()->getPost('ajax') == 'review-form') {
            echo CActiveForm::validate(new Review());
            Yii::app()->end();
        }

        $redirect = Yii::app()->getRequest()->getPost('redirectTo', Yii::app()->getUser()->getReturnUrl());

	    try {
            $review = new Review();

            $review->setAttributes(Yii::app()->getRequest()->getPost('Review'));

            $review->status = (int)$module->defaultReviewStatus;

            if ($module->autoApprove && Yii::app()->getUser()->isAuthenticated()) {
                $review->status = Review::STATUS_APPROVED;
            }

            if ($review->save()) {
                $message = $review->isApproved()
                    ? 'Ваш отзыв успешно добавлен!'
                    : 'Ваш отзыв успешно добавлен! Он будет размещен после одобрения администратором.';

	            Yii::app()->eventManager->fire(ReviewEvents::REVIEW_ADD_SUCCESS, new ReviewEvent($review));

                if (Yii::app()->getRequest()->getIsAjaxRequest()) {

                    Yii::app()->ajax->success([
                        'message' => $message,
                        /*'review' => [
                            'parent_id' => $review->parent_id,
                        ],*/
                        //'reviewContent' => $review->isApproved() ? $this->_renderReview($review) : '',
                    ]);
                }

                Yii::app()->getUser()->setFlash(webforma\widgets\WFlashMessages::SUCCESS_MESSAGE, $message);

            } else {
                Yii::app()->ajax->rawText(CActiveForm::validate($review));
            }
        } catch (Exception $e) {
            Yii::app()->ajax->failure(['message' => Yii::t('ReviewModule.review', $e->getMessage()),]);
        }
    }

    /**
     * Отрисовка комментария:
     *
     * @param Review $review - комментарий
     *
     * @return string html отрисованного комментария
     **/
    private function _renderReview(Review $review)
    {
        $review->refresh();

        return $this->renderPartial('_review', ['review' => $review], true);
    }
}
