<?php

class m181105_153001_reviews_add extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{review}}', 'rating', 'tinyint(3) default null');
		$this->addColumn('{{review}}', 'admin_text', 'text default null');
	}

	public function safeDown()
	{

	}
}