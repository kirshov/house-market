<?php

class m191105_160618_image extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{review}}', 'image', 'varchar(255) default null');
	}

	public function safeDown()
	{

	}
}