<?php

class m181105_160618_reviews_yandex_id extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{review}}', 'yandex_id', 'int(11) default null');
	}

	public function safeDown()
	{

	}
}