<?php

class m191106_160618_producer extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{review}}', 'producer_id', 'int(11) default null');

		$this->addForeignKey(
			"fk_{{review}}_producer_id",
			'{{review}}',
			'producer_id',
			'{{store_producer}}',
			'id',
			'CASCADE',
			'NO ACTION'
		);
	}

	public function safeDown()
	{

	}
}