<?php

/**
 * Review install migration
 * Класс миграций для модуля Review
 **/
class m000000_000000_review_base extends webforma\components\DbMigration
{

    public function safeUp()
    {
        $this->createTable(
            '{{review}}',
            [
                'id'            => 'pk',
                'parent_id'     => 'integer DEFAULT NULL',
                'user_id'       => 'integer DEFAULT NULL',
                'create_time' => 'datetime NOT NULL',
                'name'          => 'varchar(150) NOT NULL',
                'email'         => 'varchar(150) NOT NULL',
                'phone'         => 'varchar(150) NOT NULL',
                'text'          => 'text NOT NULL',
                'status'        => "integer NOT NULL DEFAULT '0'",
                'ip'            => 'varchar(20) DEFAULT NULL'
            ],
            $this->getOptions()
        );

        $this->createIndex("ix_{{review}}_status", '{{review}}', "status", false);
        $this->createIndex("ix_{{review}}_user_id", '{{review}}', "user_id", false);
        $this->createIndex("ix_{{review}}_parent_id", '{{review}}', "parent_id", false);

        $this->addForeignKey(
            "fk_{{review}}_user_id",
            '{{review}}',
            'user_id',
            '{{user_user}}',
            'id',
            'CASCADE',
            'NO ACTION'
        );
        $this->addForeignKey(
            "fk_{{review}}_parent_id",
            '{{review}}',
            "parent_id",
            '{{review}}',
            "id",
            'CASCADE',
            'NO ACTION'
        );
    }

    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{review}}');
    }
}
