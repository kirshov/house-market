<?php

/**
 * Class ReviewListener
 */
class ReviewListener
{

	/**
	 * @param ReviewEvent $event
	 */
	public static function onSuccessAddComment(ReviewEvent $event){
		if(Yii::app()->getModule('review')->notify){
			Yii::app()->getComponent('reviewNotifyService')->sendCreatedAdminNotify($event->getReview());
		}
	}
}
