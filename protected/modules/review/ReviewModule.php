<?php

/**
 * ReviewModule основной класс модуля review
 */

use webforma\components\WebModule;

/**
 * Class ReviewModule
 */
class ReviewModule extends WebModule
{
	/**
	 *
	 */
	public $adminMenuOrder = 80;

    /**
     * @var
     */
    public $defaultReviewStatus;

    /**
     * @var bool
     */
    public $autoApprove = true;

    /**
     * @var bool
     */
    public $notify = true;

    /**
     * @var int
     */
    public $perPage = 20;

    /**
     * @var int
     */
    public $yandexShopLink;

    /**
     * @var int
     */
    public $showCaptcha = 1;

    /**
     * @var int
     */
    public $minCaptchaLength = 3;

    /**
     * @var int
     */
    public $maxCaptchaLength = 6;

    /**
     * @var int
     */
    public $antiSpamInterval = 3;

    /**
     * @var string
     */
    public $allowedTags = 'p,br,b,img[src],a[href]';

    /**
     * @var int
     */
    public $allowGuestReview = 0;

    /**
     * @var int
     */
    public $stripTags = 1;

    /**
     * @var string
     */
    public $assetsPath = "application.modules.review.views.assets";

    /**
     * @var string
     */
    public $h1 = 'Отзывы';

    /**
     * @var string
     */
    public $meta_title;

    /**
     * @var string
     */
    public $meta_keywords;

    /**
     * @var string
     */
    public $meta_description;

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [
            'defaultReviewStatus' => Yii::t('ReviewModule.review', 'Default reviews status'),
            'autoApprove' => Yii::t('ReviewModule.review', 'Automatic review confirmation'),
            'notify' => Yii::t('ReviewModule.review', 'Notify about review?'),
            'showCaptcha' => Yii::t('ReviewModule.review', 'Show captcha for guests'),
            'minCaptchaLength' => Yii::t('ReviewModule.review', 'Minimum captcha length'),
            'maxCaptchaLength' => Yii::t('ReviewModule.review', 'Maximum captcha length'),
            'allowedTags' => Yii::t('ReviewModule.review', 'Accepted tags'),
            'antiSpamInterval' => Yii::t('ReviewModule.review', 'Antispam interval'),
            'allowGuestReview' => Yii::t('ReviewModule.review', 'Guest can review ?'),
            'stripTags' => Yii::t(
                'ReviewModule.review',
                'Remove tags in the derivation review using strip_tags() ?'
            ),
            'perPage' => 'Отзывов на странице',
            'yandexShopLink' => 'Ссылка на магазин на Яндекс.Маркете',
            'h1' => 'Заголовок на странице',
            'meta_title' => 'Meta title',
            'meta_keywords' => 'Meta keywords',
            'meta_description' => 'Meta description',
        ];
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
        return [
            'allowGuestReview' => $this->getChoice(),
            'defaultReviewStatus' => Review::model()->getStatusList(),
            'autoApprove' => $this->getChoice(),
            'notify' => $this->getChoice(),
            'showCaptcha' => $this->getChoice(),
            'perPage',
            'yandexShopLink',
            'h1',
            'meta_title',
            'meta_keywords',
            'meta_description',
        ];
    }

    /**
     * @return array
     */
    public function getEditableParamsGroups()
    {
        return [
            'main' => [
                'label' => Yii::t('ReviewModule.review', 'Module general settings'),
                'items' => [
                    'defaultReviewStatus',
                    'autoApprove',
                    'allowGuestReview',
	                'showCaptcha',
                    'notify',
                    'yandexShopLink',
                    'perPage'
                ],
            ],
            'meta' => [
                'label' => 'Мета информация',
                'items' => [
                    'h1',
                    'meta_title',
                    'meta_keywords',
                    'meta_description',
                ]
            ]
        ];
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('ReviewModule.review', 'Content');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('ReviewModule.review', 'Reviews');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('ReviewModule.review', 'Module for simple reviews support');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return "fa fa-fw fa-comments";
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ReviewModule.review', 'Reviews list'),
                'url' => ['/review/reviewBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ReviewModule.review', 'Create review'),
                'url' => ['/review/reviewBackend/create'],
            ],
        ];
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/review/reviewBackend/index';
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $import = [
        	'application.modules.review.models.*',
        	'application.modules.review.components.*',
        	'application.modules.review.events.*',
        	'application.modules.review.listeners.*',
		];

        $this->setImport($import);

        $this->defaultReviewStatus = Review::STATUS_NEED_CHECK;
    }
}
