<?php
namespace review\widgets\editors;

use Yii;
use webforma\widgets\editors\Redactor as Redactor;

/**
 * Class ReviewRedactor
 * @package review\widgets\editors
 */
class ReviewRedactor extends Redactor
{
    /**
     * @var array
     */
    public $options = [];

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'buttonsHide'           => [
                'html',
                'formatting',
                'horizontalrule',
                'italic',
                'deleted',
                'unorderedlist',
                'orderedlist',
                'outdent',
                'indent',
                'alignment'
            ],
            'imageUpload'           => Yii::app()->createUrl('review/review/ajaxImageUpload'),
            'toolbarFixedTopOffset' => 53,
            'lang'                  => strtolower(substr(Yii::app()->getLanguage(), -2)),
            'minHeight'             => 150,
            'uploadImageFields'     => [
                Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
            ]
        ];
    }

    /**
     * @return array
     */
    public function getPlugins()
    {
        return [];
    }
}
