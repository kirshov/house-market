<?php $box = $this->beginWidget(
    'bootstrap.widgets.TbCollapse', [
        'htmlOptions' => [
            'id' => 'panel-review-stat'
        ]
    ]
);?>


<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#<?= $this->getId(); ?>">
                    <i class="fa fa-comments"></i> <?=  Yii::t(
                        'ReviewModule.review',
                        'Reviews'
                    ); ?>
                </a>
                <span class="badge alert-success"><?=  $reviewsCount; ?></span>
                <span class="badge alert-info"><?=  $allReviewsCnt; ?></span>
                <span class="badge alert-danger"><?=  $newCnt; ?></span>
            </h4>
        </div>


        <div id="<?= $this->getId(); ?>" class="panel-collapse collapse">
            <div class="panel-body">


                <div class="row">
                    <div class="col-sm-8">
                        <?php $this->widget(
                            'bootstrap.widgets.TbExtendedGridView',
                            [
                                'id'           => 'post-grid',
                                'type'         => 'striped condensed',
                                'dataProvider' => $dataProvider,
                                'template'     => '{items}',
                                'htmlOptions'  => [
                                    'class' => false
                                ],
                                'columns'      => [
                                    [
                                        'name'  => 'text',
                                        'value' => 'CHtml::link(webforma\helpers\WText::characterLimiter($data->text, 100), array("/review/reviewBackend/update","id" => $data->id))',
                                        'type'  => 'html'
                                    ],
                                    'create_time',
                                    [
                                        'name'  => 'status',
                                        'value' => '$data->getStatus()',
                                    ],
                                ],
                            ]
                        ); ?>
                    </div>
                    <div class="col-sm-4">
                        <div class="row">
                            <table>
                                <tbody>
                                <tr>
                                    <td>
                                        <?=  Yii::t('ReviewModule.review', 'Reviews (last day / all)'); ?>:

                                    </td>
                                    <td>
                                        <span class="badge alert-success"><?=  $reviewsCount; ?></span>
                                        <span class="badge alert-info"><?=  $allReviewsCnt; ?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?=  Yii::t('ReviewModule.review', 'Moderation'); ?>:
                                    </td>
                                    <td>
                                        <span class="badge alert-danger"><?=  $newCnt; ?></span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>
