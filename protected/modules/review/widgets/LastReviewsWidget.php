<?php

/**
 * Виджет для вывода последних отзывов
 **/
class LastReviewsWidget extends webforma\widgets\WWidget
{
    /**
     * @var
     */
    public $model;
    /**
     * @var
     */
    public $reviewStatus;
    /**
     * @var int
     */
    public $limit = 10;
    /**
     * @var bool
     */
    public $onlyWithAuthor = true;
    /**
     * @var string
     */
    public $view = 'lastreviews';

    /**
     *
     */
    public function init()
    {
        if ($this->model) {
            $this->model = is_object($this->model) ? get_class($this->model) : $this->model;
        }

        $this->reviewStatus || ($this->reviewStatus = Review::STATUS_APPROVED);
    }

    /**
     * @throws CException
     */
    public function run()
    {
        $criteria = new CDbCriteria([
            'condition' => 'status = :status AND id<>root',
            'params' => [':status' => $this->reviewStatus],
            'limit' => $this->limit,
            'order' => 'id DESC',
        ]);

        if ($this->model) {
            $criteria->addCondition('model = :model');
            $criteria->params[':model'] = $this->model;
        }

        if ($this->onlyWithAuthor) {
            $criteria->addCondition('user_id is not null');
        }

        $reviews = Review::model()->findAll($criteria);

        $this->render($this->view, ['models' => $reviews]);
    }
}
