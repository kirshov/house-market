<?php
Yii::import('application.modules.review.ReviewModule');

/**
 * Review Widget
 **/
class ReviewsWidget extends webforma\widgets\WWidget
{
    /**
     * @var string
     */
    public $view = 'reviews';

	/**
	 * @var null
	 */
    public $producerId = null;

    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->render($this->view, [
        	'reviews' => $this->getReviews(),
        ]);
    }

    /**
     * @return array
     */
    protected function getReviews()
    {
    	//$key = 'ReviewWidget';
    	$criteria = null;
    	if($this->producerId){
    		$criteria = new CDbCriteria();
    		$criteria->condition = 't.producer_id = :producer_id';
    		$criteria->params[':producer_id'] = $this->producerId;
		}
        //if (false === ($reviews = Yii::app()->getCache()->get($key))) {
		$reviews = Review::model()->approved()->findAll($criteria);
            //Yii::app()->getCache()->set($key, $reviews);
        //}

        return $reviews;
    }
}
