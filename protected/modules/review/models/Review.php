<?php
use webforma\widgets\WPurifier;

/**
 * Review model class:
 *
 * This is the model class for table "Review".
 *
 * @const    int STATUS_APPROVED    - Принят
 * @const    int STATUS_DELETED     - Удален
 * @const    int STATUS_NEED_CHECK  - Проверка
 * @const    int STATUS_SPAM        - Спам
 *
 * @var      public $verifyCode - капча
 *
 * The followings are the available columns in table 'Review':
 * @property string $id
 * @property string $create_time
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $text
 * @property integer $status
 * @property string $ip
 * @property string $user_id
 * @property integer $parent_id
 * @property integer $rating
 * @property integer $admin_text
 *
 * @property User author
 * @method getImageUrl($width = null, $height = null, $crop = false)
 *
 * @method $this approved
 */
class Review extends webforma\models\WModel
{
    /**
     *
     */
    const STATUS_NEED_CHECK = 0;

    /**
     *
     */
    const STATUS_APPROVED = 1;

    /**
     *
     */
    const STATUS_SPAM = 2;

    /**
     *
     */
    const STATUS_DELETED = 3;

    /**
     * @var
     */
    public $verifyCode;

    /**
     * @var
     */
    public $spamField;

    /**
     * @var
     */
    public $review;

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className - инстанс модели
     *
     * @return Review the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Имя таблицы в БД:
     *
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{review}}';
    }

    /**
     * Список правил для валидации полей модели:
     *
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        $module = Yii::app()->getModule('review');

        return [
            ['name, email, phone, text, create_time, admin_text, yandex_id', 'filter', 'filter' => 'trim'],
            ['name, email, text', 'filter', 'filter' => [$obj = new WPurifier(), 'purify']],
	        [
		        'phone',
		        'match',
		        'pattern' => Yii::app()->getModule('webforma')->phonePattern,
		        'message' => 'Неверный формат телефона',
	        ],
	        ['rating', 'in', 'range' => [1, 2, 3, 4, 5], 'allowEmpty' => true],
            ['text', 'purifyText'],
            ['name', 'required'],
            ['status, user_id, parent_id, producer_id', 'numerical', 'integerOnly' => true],
            ['name, email, review', 'length', 'max' => 150],
            ['ip', 'length', 'max' => 20],
            ['email', 'email'],
            ['status', 'in', 'range' => array_keys($this->getStatusList()), 'allowEmpty' => true],
            [
                'verifyCode',
                'webforma\components\validators\YRequiredValidator',
                'allowEmpty' => !$module->showCaptcha || Yii::app()->getUser()->isAuthenticated()
            ],
            [
                'verifyCode',
                'captcha',
                'allowEmpty' => !$module->showCaptcha || Yii::app()->getUser()->isAuthenticated(),
                'captchaAction' => '/review/review/captcha',
            ],
            [
                'id, create_time, name, email, text, status, ip, parent_id',
                'safe',
                'on' => 'search'
            ],
        ];
    }

	public function behaviors()
	{
		return array(
			'uploads' => [
				'class' => 'webforma\components\behaviors\ImageUploadBehavior',
				'attributeName' => 'image',
				'uploadPath' => 'review',
			],
		);
	}


	/**
     * @param $attribute
     * @param $params
     */
    public function purifyText($attribute, $params)
    {
        $module = Yii::app()->getModule('review');
        $p = new CHtmlPurifier();
        $p->options = [
            'HTML.Allowed' => $module->allowedTags,
        ];
        $this->$attribute = $p->purify($this->$attribute);
    }

    /**
     * Список атрибутов для меток формы:
     *
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('ReviewModule.review', 'ID'),
            'create_time' => Yii::t('ReviewModule.review', 'Created at'),
            'name' => Yii::t('ReviewModule.review', 'Name'),
            'email' => Yii::t('ReviewModule.review', 'Email'),
            'phone' => Yii::t('ReviewModule.review', 'Phone'),
            'text' => Yii::t('ReviewModule.review', 'Review'),
            'status' => Yii::t('ReviewModule.review', 'Status'),
            'verifyCode' => Yii::t('ReviewModule.review', 'Verification code'),
            'ip' => Yii::t('ReviewModule.review', 'IP address'),
            'parent_id' => Yii::t('ReviewModule.review', 'Parent'),
	        'rating' => 'Оценка',
	        'admin_text' => 'Код видео',
	        'yandex_id' => 'ID отзыва в Яндекс.Маркет',
	        'image' => 'Изображение',
	        'producer_id' => 'Застройщик',
        ];
    }

    /**
     * Список связей данной таблицы:
     *
     * @return mixed список связей
     **/
    public function relations()
    {
        return [
            'author' => [self::BELONGS_TO, 'User', 'user_id'],
        ];
    }

    /**
     * Получение группы условий:
     *
     * @return mixed список условий
     **/
    public function scopes()
    {
        return [
            'new' => [
                'condition' => 't.status = :status',
                'params' => [':status' => self::STATUS_NEED_CHECK],
            ],
            'approved' => [
                'condition' => 't.status = :status',
                'params' => [':status' => self::STATUS_APPROVED],
                'order' => 't.create_time DESC',
            ],
            'authored' => [
                'condition' => 't.user_id is not null',
            ],
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria();

        $criteria->compare('id', $this->id, true);
        $criteria->compare('parent_id', $this->parent_id);
        $criteria->compare('producer_id', $this->producer_id);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('ip', $this->ip, true);

        return new CActiveDataProvider(get_class($this), [
            'criteria' => $criteria,
            'sort' => [
                'defaultOrder' => 'id DESC',
            ]
        ]);
    }

    /**
     * Событие выполняемое перед сохранением модели
     *
     * @return parent::beforeSave()
     **/
    public function beforeSave()
    {
        if ($this->getIsNewRecord()) {
            $this->create_time = new CDbExpression('NOW()');
            $this->ip = Yii::app()->getRequest()->userHostAddress;
        } else {
        	if($this->create_time){
		        $this->create_time = date('Y-m-d H:i:s', strtotime($this->create_time));
	        } else {
		        $this->create_time = new CDbExpression('NOW()');
	        }
        }

        return parent::beforeSave();
    }

	/**
	 *
	 */
	protected function afterSave()
	{
		Yii::app()->cache->delete('ReviewWidget');
		parent::afterSave();
	}

	/**
	 *
	 */
	protected function afterDelete()
	{
		Yii::app()->cache->delete('ReviewWidget');
		parent::afterDelete();
	}

	/**
	 *
	 */
	public function afterFind()
	{
		$this->create_time = date('d.m.Y H:i:s', strtotime($this->create_time));

		return parent::afterFind();
	}

    /**
     * Получение списка статусов:
     *
     * @return mixed список статусов
     **/
    public function getStatusList()
    {
        return [
            self::STATUS_APPROVED => Yii::t('ReviewModule.review', 'Accepted'),
            self::STATUS_DELETED => Yii::t('ReviewModule.review', 'Deleted'),
            self::STATUS_NEED_CHECK => Yii::t('ReviewModule.review', 'Check'),
            self::STATUS_SPAM => Yii::t('ReviewModule.review', 'Spam'),
        ];
    }

    /**
     * Получение статуса по заданному:
     *
     * @return string текст статуса
     **/
    public function getStatus()
    {
        $list = $this->getStatusList();

        return isset($list[$this->status]) ? $list[$this->status] : Yii::t('ReviewModule.review', 'Unknown status');
    }

    /**
     * Получаем автора:
     *
     * @return Review->author || bool false
     **/
    public function getAuthorName()
    {
        return ($this->author) ? $this->author->nick_name : $this->name;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return (Yii::app()->getModule('review')->stripTags)
            ? strip_tags($this->text)
            : $this->text;
    }

    /**
     * @return bool
     */
    public function isApproved()
    {
        return $this->status == self::STATUS_APPROVED;
    }

    /**
     * @return mixed
     */
    public function approve()
    {
        $this->status = self::STATUS_APPROVED;
        return $this->save();
    }

    /**
     * @param array $items
     * @return bool
     */
    public static function multiApprove(array $items)
    {
        $transaction = Yii::app()->getDb()->beginTransaction();

        try {
            $models = Review::model()->findAllByPk($items);

            foreach ($models as $model) {
                $model->approve();
            }

            $transaction->commit();

            return true;

        } catch (Exception $e) {
            $transaction->rollback();

            Yii::log($e->__toString(), CLogger::LEVEL_ERROR);

            return false;
        }
    }

    /**
     * @return string
     */
    public function getDate(){
        return date('d.m.Y', strtotime($this->create_time));
    }

	/**
	 * @return array
	 */
    public static function getRatingRanges(){
    	return [
    		5 => 'Отлично',
    		4 => 'Хорошо',
    		3 => 'Средне',
    		2 => 'Плохо',
    		1 => 'Ужасно',
	    ];
    }

	/**
	 * @return bool|mixed
	 */
    public function getRatingTitle(){
    	if(!$this->rating){
    		return 'Нет оценки';
	    }
    	$data = self::getRatingRanges();
	    return isset($data[$this->rating]) ? $data[$this->rating] : 'Нет оценки';
    }

	/**
	 * @return int
	 */
    public function getRating(){
    	return $this->rating;
    }
}
