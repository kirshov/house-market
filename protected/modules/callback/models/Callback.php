<?php

/**
 * Class Callback
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $time
 * @property string $comment
 * @property integer $status
 * @property string $create_time
 * @property string $url
 */
class Callback extends \webforma\models\WModel
{
    /**
     *
     */
    const STATUS_NEW = 0;

    /**
     *
     */
    const STATUS_PROCESSED = 1;

    /**
     * @return string
     */
    public function tableName()
    {
        return '{{callback}}';
    }

    /**
     * @param null|string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['name, phone', 'required'],
            ['name, comment', 'filter', 'filter' => 'trim'],
            ['name', 'length', 'max' => 30],
	        [
		        'phone',
		        'match',
		        'pattern' => Yii::app()->getModule('webforma')->phonePattern,
		        'message' => Yii::t('WebformaModule.webforma', 'Incorrect phone value'),
	        ],
            ['comment', 'length', 'max' => 255],
            ['status', 'numerical', 'integerOnly' => true],
            //['url', 'url'],
            ['id, name, phone, time, comment, status, create_time, url', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('CallbackModule.callback', 'Name'),
            'phone' => Yii::t('CallbackModule.callback', 'Phone'),
            'time' => Yii::t('CallbackModule.callback', 'Time'),
            'comment' => Yii::t('CallbackModule.callback', 'Comment'),
            'status' => Yii::t('CallbackModule.callback', 'Status'),
            'create_time' => Yii::t('CallbackModule.callback', 'Created At'),
            'url' => Yii::t('CallbackModule.callback', 'Url'),
        ];
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'new' => [
                'condition' => 'status = :status',
                'params' => [':status' => self::STATUS_NEW],
            ],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'CTimestampBehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => null,
            ],
        ];
    }

    /**
     * @return CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->compare('comment', $this->comment, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('time', $this->time, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('url', $this->url, true);

        return new CActiveDataProvider(
            $this, [
                'criteria' => $criteria,
                'sort' => ['defaultOrder' => 'id DESC'],
            ]
        );
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_NEW => 'Новый',
            self::STATUS_PROCESSED => 'Обработан',
        ];
    }

    /**
     * @return array
     */
    public function getStatusLabelList()
    {
        return [
            self::STATUS_NEW => ['class' => 'label-danger'],
            self::STATUS_PROCESSED => ['class' => 'label-success'],
        ];
    }

	/**
	 * @return Order|mixed
	 */
    public function getLastOrderByPhone(){
	    if(!Yii::app()->hasModule('order') || !$this->phone){
		    return false;
	    }

	    Yii::import('application.modules.order.models.Order');
    	return Order::model()->find([
    		'condition' => 't.phone = :phone',
		    'params' => [
		    	':phone' => $this->phone
		    ],
		    'order' => 't.date DESC',
	    ]);
    }
}