<?php

/**
 * Class PanelCallbackStatWidget
 */
class PanelCallbackStatWidget extends \webforma\widgets\WWidget
{
    /**
     * @throws CException
     */
    public function run()
    {
        $cacheTime = Yii::app()->getController()->webforma->coreCacheTime;

        $this->render('panel-callback-stat', [
            'allCallbackCount' => Callback::model()->cache($cacheTime)->count(),
            'newCallbackCount'  => Callback::model()->new()->cache($cacheTime)->count(),
        ]);
    }
}
