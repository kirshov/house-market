<?php

Yii::import('application.modules.callback.models.*');

/**
 * Class CallbackWidget
 */
class CallbackWidget extends \webforma\widgets\WWidget
{
    /**
     * @var string
     */
    public $view = 'default';

    /**
     * @throws CException
     */
    public function init()
    {
        Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getAssetManager()->publish(
            Yii::getPathOfAlias('application.modules.callback.views.web') . '/callback.js'
        ), CClientScript::POS_END);

        parent::init();
    }

    /**
     * @throws CException
     */
    public function run()
    {
	    $model = Callback::model();
	    if (Yii::app()->getUser()->isAuthenticated()) {
		    $user = Yii::app()->getUser()->getProfile();
		    $model->setAttributes([
			    'name' => $user->getFullName(),
			    'phone' => $user->phone,
		    ]);
	    }
        $this->render($this->view, [
            'model' => $model,
        ]);
    }
}