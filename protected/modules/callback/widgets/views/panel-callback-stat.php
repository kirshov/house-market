<?/**
 * @var int $newCallbackCount
 * @var int $allCallbackCount
 */?>
<div class="col-md-4 col-xs-12 col-sm-6">
	<div class="panel panel-info panel-dark">
		<div class="panel-heading">
			<h3 class="widget-profile-header">
				Обратный звонок
			</h3>
			<i class="panel-icon fa fa-fw fa-phone"></i>
		</div>
		<div class="widget-profile-counters">
			<a href="<?=Yii::app()->createUrl('/backend/callback/callback/')?>?Callback[status]=<?=Callback::STATUS_NEW?>" class="col-xs-6">
				<span class="widget-profile-counter<?=($newCallbackCount ? ' text-danger' : '')?>"><?=$newCallbackCount?></span>
				Ждут звонка
			</a>
			<a href="<?=Yii::app()->createUrl('/backend/callback/callback/')?>" class="col-xs-6">
				<span class="widget-profile-counter"><?=$allCallbackCount?></span>
				Всего
			</a>
		</div>
		<div class="panel-footer">
			<a href="<?=Yii::app()->createUrl('/backend/callback/callback/')?>">Перейти к звонкам</a>
		</div>
	</div>
</div>