<?php
/**
 * Модуль "Обратный звонок"
 *
 * @package  webforma.modules.callback
 * @author   Oleg Filimonov <olegsabian@gmail.com>
 * @license  BSD http://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D1%86%D0%B5%D0%BD%D0%B7%D0%B8%D1%8F_BSD
 * @version  1.1
 **/
use webforma\components\WebModule;

/**
 * Class CallbackModule
 */
class CallbackModule extends WebModule
{
    /**
     * @var
     */
    public $notifyEmailFrom;

    /**
     * @var
     */
    public $notifyEmailsTo;

	/**
	 * @var int
	 */
	public $adminMenuOrder = 10;

	/**
	 * @var string
	 */
	public $message = 'Спасибо за Ваше обращения, мы свяжемся с Вами в ближайшее время';


	/**
     * @return array
     */
    public function getDependencies()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
		$module = Yii::app()->getModule('webforma');
        return [
			'notifyEmailFrom' => [
				'input-type' => 'text',
				'htmlOptions' => [
					'placeholder' => $module->notifyEmailFrom
				],
			],
			'notifyEmailsTo' => [
				'input-type' => 'text',
				'htmlOptions' => [
					'placeholder' => $module->notifyEmailTo
				],
			],
			'message' => [
				'input-type' => 'textarea',
			]
        ];
    }

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [
            'notifyEmailFrom' => Yii::t('CallbackModule.callback', 'Notification email'),
            'notifyEmailsTo'  => Yii::t('CallbackModule.callback', 'Recipients of notifications (comma separated)'),
			'message' => 'Сообщение об успешной отправке',
        ];
    }

    /**
     * @return array
     */
    public function getEditableParamsGroups()
    {

        return [
            '1.notify' => [
                'label' => Yii::t('CallbackModule.callback', 'Params'),
                'items' => [
                    'notifyEmailFrom',
                    'notifyEmailsTo',
                    'message',
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('CallbackModule.callback', 'Services');
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-phone',
                'label' => Yii::t('CallbackModule.callback', 'Messages'),
                'url' => ['/callback/callbackBackend/index']
            ],
        ];
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/callback/callbackBackend/index';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('CallbackModule.callback', 'Callback');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('CallbackModule.callback', 'Callback messages management module');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-phone';
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport([
            'callback.models.*',
            'callback.events.*',
        ]);
    }

	/**
	 * @return bool
	 */
	public function getIsInstallDefault()
	{
		return true;
	}
}