<?php
return [
    'module' => [
        'class' => 'application.modules.callback.CallbackModule',
	    'panelWidgets' => [
		    'application.modules.callback.widgets.PanelCallbackStatWidget' => [
			    'position' => 15,
		    ],
	    ],
    ],
    'import' => [
        'application.modules.callback.CallbackModule',
        'application.modules.callback.listeners.CallbackTemplateListener',
    ],
    'component' => [
        'callbackManager' => [
            'class' => 'application.modules.callback.components.CallbackManager',
        ],
        'eventManager' => [
            'class' => 'webforma\components\EventManager',
        ],
	    'callbackNotifyService' => [
		    'class' => 'application.modules.callback.components.CallbackNotifyService',
	    ],
	    'callbackMailMessage' => [
		    'class' => 'application.modules.callback.components.CallbackMailMessage',
	    ],
    ],
	'params'    => [
		'widgets'   => [
			'callback' => 'application.modules.callback.widgets.CallbackWidget',
		],
	],
    'rules' => [
        '/callback' => '/callback/callback/send',
    ],

];
