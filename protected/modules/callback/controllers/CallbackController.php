<?php

/**
 * Class CallbackController
 */
class CallbackController extends \webforma\components\controllers\FrontController
{
    /**
     * @throws CHttpException
     */
    public function actionSend()
    {
        $request = Yii::app()->getRequest();

        if (!$request->getIsAjaxRequest() || !$request->getIsPostRequest() || !$request->getPost('Callback')) {
            throw new CHttpException(404);
        }

		$model = new Callback();
		$model->setAttributes($request->getPost('Callback'));
		$model->url = $request->getUrlReferrer();

        $result = Yii::app()->callbackManager->add($model);
        if($result === true) {
            Yii::app()->ajax->success(['message' => Yii::app()->getModule('callback')->message]);
        } else {
			Yii::app()->ajax->rawText(CActiveForm::validate($model));
            //Yii::app()->ajax->failure(Yii::t('CallbackModule.callback', 'Sorry, an error has occurred.'));
        }
    }
}