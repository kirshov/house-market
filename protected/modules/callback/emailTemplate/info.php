<?
/**
 * @var Callback $model
 */
$lastOrder = $model->getLastOrderByPhone();
$c=1;
?>
<table cellpadding="6" cellspacing="0" style="border-collapse: collapse; width: 100%;">
	<?php if ($model->name): ?>
		<tr>
			<td style="%info-table-<?=Helper::getParity($c);?>% width: 173px;">
				Имя и фамилия
			</td>
			<td style="%info-table-<?=Helper::getParity($c);?>%">
				<?= CHtml::encode($model->name); ?>
			</td>
		</tr>
		<?$c++;?>
	<?php endif; ?>
	<?php if ($model->phone): ?>
		<tr>
			<td style="%info-table-<?=Helper::getParity($c);?>% width: 173px;">
				Телефон
			</td>
			<td style="%info-table-<?=Helper::getParity($c);?>%">
				<?= CHtml::encode($model->phone); ?>
			</td>
		</tr>
		<?$c++;?>
	<?php endif; ?>
	<?php if ($model->url): ?>
		<tr>
			<td style="%info-table-<?=Helper::getParity($c);?>% width: 173px;">
				Страница
			</td>
			<td style="%info-table-<?=Helper::getParity($c);?>%">
				<?= CHtml::link($model->url, $model->url); ?>
			</td>
		</tr>
		<?$c++;?>
	<?php endif; ?>
	<tr>
		<td style="%info-table-<?=Helper::getParity($c);?>% width: 173px;">
			IP-адрес
		</td>
		<td style="%info-table-<?=Helper::getParity($c);?>%">
			<?= Yii::app()->getRequest()->getUserHostAddress(); ?>
		</td>
		<?$c++;?>
	</tr>
	<?if($lastOrder):?>
		<tr>
			<td style="%info-table-<?=Helper::getParity($c);?>% width: 173px;">
				Последний заказ
			</td>
			<td style="%info-table-<?=Helper::getParity($c);?>%">
				<a href="<?=Yii::app()->createAbsoluteUrl('/order/order/view', ['url' => $lastOrder->url])?>" style="font: %fond%; color: %linkColor%"><?= $lastOrder->getNumberForUser(); ?></a>
				от <?=date('Y.m.d', strtotime($lastOrder->date))?>
			</td>
			<?$c++;?>
		</tr>
	<?endif;?>
	<?if(Yii::app()->hasModule('utm') && Yii::app()->getComponent('utm')->has()):?>
		<?foreach (Yii::app()->getComponent('utm')->get() as $title => $value):?>
			<tr>
				<td style="%info-table-<?=Helper::getParity($c);?>% width: 173px;">
					<?=$title?>
				</td>
				<td style="%info-table-<?=Helper::getParity($c);?>%">
					<?= CHtml::encode($value); ?>
				</td>
			</tr>
			<?$c++;?>
		<?endforeach;?>
	<?endif?>
</table>
