<?php

class CallbackNotifyService extends NotifyService
{

	/**
	 * @var string
	 */
	protected $templatePath  = 'callback/email';

	/**
	 * @var string
	 */
	protected $messageComponent = 'callbackMailMessage';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		$this->module = Yii::app()->getModule('callback');
	}

	/**
	 * @param Callback $model
	 * @return bool
	 */
	public function sendCreatedAdminNotify(Callback $model)
	{
		$webforma = Yii::app()->getModule('webforma');
		$from = $this->module->notifyEmailFrom ?: $webforma->notifyEmailFrom;
		$to = $this->module->notifyEmailsTo ?: $webforma->notifyEmailTo;

		if(!$to || !$from){
			return false;
		}

		$mailContent = $this->getMailContent($model, 'newMessage', true);

		if($mailContent['subject'] && $mailContent['body']){
			foreach (explode(',', $to) as $email) {
				$this->mail->send($from, $email, $mailContent['subject'], $mailContent['body']);
			}
		}

		return true;
	}

}