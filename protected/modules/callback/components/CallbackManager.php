<?php

/**
 * Class CallbackManager
 */
class CallbackManager extends CApplicationComponent
{
    /** @var CallbackController */
    private $view;

    /**
     * @var
     */
    private $mailer;

    /** @var CallbackModule */
    private $module;

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->view = Yii::app()->getController();
        $this->mailer = Yii::app()->mail;
        $this->module = Yii::app()->getModule('callback');
    }

    /**
     * Add callback request to DB
     *
     * @param Callback $model
     * @return bool
     */
    public function add($model)
    {
        if ($model->save()) {
            Yii::app()->eventManager->fire(CallbackEvents::ADD, new CallbackAddEvent($model));

	        Yii::app()->getComponent('callbackNotifyService')->sendCreatedAdminNotify($model);
            return true;
        }

        return false;
    }

    /**
     * Send notification to managers
     *
     * @param Callback $model
     * @return bool
     */
    private function sendNotification(Callback $model)
    {
    	$module = Yii::app()->getModule('webforma');
        $from = $this->module->notifyEmailFrom ?: $module->notifyEmailFrom;
        $to = $this->module->notifyEmailsTo ?: $module->notifyEmailTo;

        $theme = Yii::t('CallbackModule.callback', 'Callback request');
        $body = $this->view->renderPartial('/callback/email/request', ['model' => $model], true);

        foreach (explode(',', $to) as $email) {
            $this->mailer->send($from, $email, $theme, $body);
        }

        return true;
    }
}