<?php
class CallbackMailMessage extends WMailMessage{

	/**
	 * @var string
	 */
	public $icon = 'fa-phone';

	/**
	 * @var Callback
	 */
	protected $model;

	/**
	 * @var string
	 */
	protected $module = 'callback';

	/**
	 * @return array
	 */
	public function getDefaultMessages()
	{
		return [
			'newMessage' => [
				'name' => 'Запрос на обратный звонок',
				'subject' => 'Получен запрос на обратный звонок на сайте "%siteName%"',
				'body' => '<h2 style="'.$this->getStyle('title').'">Получен запрос на обратный звонок</h2>
					<h3 style="'.$this->getStyle('subtitle').'">на сайте "%siteName%"</h3>
					<br/>
					%info%
				',
			],
		];
	}

	/**
	 * @param $code
	 * @param bool $isAdmin
	 * @return mixed|string
	 */
	public function getContentBlock($code, $isAdmin = false)
	{
		switch ($code){
			case '%info%':
				return $this->includeContent('callback.emailTemplate.info', ['model' => $this->model, 'isAdmin' => $isAdmin]);
			default:
				return parent::getContentBlock($code, $isAdmin);
		}
	}
}
