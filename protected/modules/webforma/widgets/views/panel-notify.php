<?/**
 * @var int $allCommentsCnt
 * @var int $newCommentsCnt
 * @var int $allReviewsCnt
 * @var int $newReviewsCnt
 */?>
<div class="col-md-4 col-xs-12 col-sm-6">
	<div class="panel panel-warning panel-dark widget-profile notify-panel">
		<div class="panel-heading">
			<h3 class="widget-profile-header">
				Уведомления
			</h3>
			<i class="panel-icon fa fa-fw fa-bullhorn"></i>
		</div>
		<div class="list-group">
			<?if(Yii::app()->hasModule('comment')):?>
				<a href="<?=Yii::app()->createUrl('/backend/comment/comment/')?>" class="list-group-item">
					<i class="<?=Yii::app()->getModule('comment')->getIcon()?> list-group-icon"></i>
					&nbsp;Комментарии
					<span class="badge badge-default"><?=(int) $comments['allCommentsCnt']?></span>
					<span class="badge-separator">/</span>
					<span class="badge badge-<?=$comments['newCommentsCnt'] ? 'danger' : 'default'?>"><?=(int) $comments['newCommentsCnt']?></span>
				</a>
			<?endif;?>
			<?if(Yii::app()->hasModule('review')):?>
				<a href="<?=Yii::app()->createUrl('/backend/review/review/')?>" class="list-group-item">
					<i class="<?=Yii::app()->getModule('review')->getIcon()?> list-group-icon"></i>
					&nbsp;Отзывы
					<span class="badge badge-default"><?=(int) $reviews['allReviewsCnt']?></span>
					<span class="badge-separator">/</span>
					<span class="badge badge-<?=$reviews['newReviewsCnt'] ? 'danger' : 'default'?>"><?=(int) $reviews['newReviewsCnt']?></span>
				</a>
			<?endif;?>
			<a href="<?=Yii::app()->createUrl('/backend/user/user/')?>" class="list-group-item">
				<i class="<?=Yii::app()->getModule('user')->getIcon()?> list-group-icon"></i>
				&nbsp;Пользователей
				<span class="badge badge-default"><?=(int) $users['allUsersCnt']?></span>
				<?/*<span class="badge-separator">/</span>
				<span class="badge badge-<?=$newReviewsCnt ? 'danger' : 'default'?>"><?=(int) $newReviewsCnt?></span>*/?>
			</a>
			<a href="<?=Yii::app()->createUrl('/webforma/backend/modulesettings/', ['module' => 'webforma'])?>" class="list-group-item">Настройки сайта</a>
		</div>
	</div>
</div>