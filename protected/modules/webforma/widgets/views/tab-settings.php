<?
/**
 * @var string $name
 * @var array $options
 */
?>
<div class="tab-settings-wrap">
	<div class="row">
		<div class="col-xs-12">
			<div id="tabs-settings">
				<div class="tab-template hidden form-group tab-item">
					<div class="panel simple">
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-6 col-sm-8">
									<div class="form-group">
										<label class="control-label" for="">Название</label>
										<input class="form-control tab-name" placeholder="Название" type="text" maxlength="250">
										<input class="tab-sort" type="hidden">
									</div>

								</div>
								<div class="col-xs-6 col-sm-4" style="padding-top: 24px">
									<button class="button-down-tab btn btn-default" type="button" style="margin-top: -2px">
										<i class="fa fa-fw fa-arrow-down"></i>
									</button>
									<button class="button-up-tab btn btn-default" type="button" style="margin-top: -2px">
										<i class="fa fa-fw fa-arrow-up"></i>
									</button>
									<button class="button-delete-tab not-exist btn btn-default" type="button" style="margin-top: -2px">
										<i class="fa fa-fw fa-trash-o"></i>
									</button>
								</div>
							</div>
							<?if(!empty($options['presets'])):?>
								<div class="row">
									<div class="col-xs-6">
										<div class="form-group">
											<label class="control-label" for="">Тип контента</label>
											<?=CHtml::dropDownList('', '', $options['presets'], ['class' => 'form-control tab-preset']);?>
										</div>
									</div>
								</div>
								<div class="row preset-text hidden">
									<div class="col-xs-12">
										<?php $this->widget(Yii::app()->getModule('webforma')->getVisualEditor(), [
											'name' => 'description',
											'htmlOptions' => [
												'class' => 'preset-text',
											]
										]); ?>
									</div>
								</div>
							<?endif;?>
						</div>
					</div>
				</div>

				<?if($value):?>
					<?foreach ($value as $_key => $_item):?>
						<div class="form-group tab-item" data-key="<?=$_key?>">
							<div class="panel simple">
								<div class="panel-body">
									<div class="row">
										<div class="col-xs-6 col-sm-8">
											<div class="form-group">
												<label class="control-label" for="">Название</label>
												<input class="form-control tab-name" placeholder="Название" type="text" maxlength="250" name="<?=$name?>[<?=$_key?>][name]" value="<?=$_item['name']?>">
												<input class="tab-sort" type="hidden" name="<?=$name?>[<?=$_key?>][sort]" value="<?=$_item['sort']?>">
											</div>

										</div>
										<div class="col-xs-6 col-sm-4" style="padding-top: 24px">
											<button class="button-down-tab btn btn-default" type="button" style="margin-top: -2px">
												<i class="fa fa-fw fa-arrow-down"></i>
											</button>
											<button class="button-up-tab btn btn-default" type="button" style="margin-top: -2px">
												<i class="fa fa-fw fa-arrow-up"></i>
											</button>
											<button class="button-delete-tab exist btn btn-default" type="button" style="margin-top: -2px">
												<i class="fa fa-fw fa-trash-o"></i>
											</button>
										</div>
									</div>
									<?if(!empty($options['presets'])):?>
										<div class="row">
											<div class="col-xs-6">
												<div class="form-group">
													<label class="control-label" for="">Тип контента</label>
													<?=CHtml::dropDownList($name.'['.$_key.'][presets]', $_item['presets'], $options['presets'], ['class' => 'form-control tab-preset']);?>
												</div>
											</div>
										</div>

										<div class="row preset-text<?=($_item['presets'] != 'text') ? ' hidden' : '';?>">
											<div class="col-xs-12">
												<?php $this->widget(Yii::app()->getModule('webforma')->getVisualEditor(), [
													'name' => $name.'['.$_key.'][presets-text]',
													'value' => $_item['presets-text'],
												]); ?>
											</div>
										</div>
									<?endif;?>
								</div>
							</div>
						</div>
					<?endforeach;?>
				<?endif;?>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<button id="button-add-tab" type="button" class="btn btn-success">
				<i class="fa fa-fw fa-plus"></i>Добавить вкладку
			</button>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#tabs-settings').on('click', '.button-delete-tab.not-exist', function () {
			$(this).closest('.tab-item').remove();
			updateArrows();
		});

		$('#button-add-tab').on('click', function () {
			var newTab = $("#tabs-settings .tab-template").clone().removeClass('tab-template').removeClass('hidden');
			var key = $.now();

			newTab.appendTo("#tabs-settings");
			newTab.find(".tab-name").attr('name', '<?=$name?>[' + key + '][name]');
			newTab.find(".tab-sort").attr('name', '<?=$name?>[' + key + '][sort]');
			newTab.find(".tab-preset").attr('name', '<?=$name?>[' + key + '][presets]');
			newTab.find(".tab-preset-text").attr('name', '<?=$name?>[' + key + '][preset-text]');

			updateSort();
			updateArrows();
			return false;
		});

		$('body').on('change', '.tab-preset', function () {
			if($(this).val() == 'text'){
				$(this).closest('.tab-item').find('.preset-text').removeClass('hidden');
			} else {
				$(this).closest('.tab-item').find('.preset-text').addClass('hidden');
			}
		});

		$('body').on('click', '.button-delete-tab.exist', function (event) {
			event.preventDefault();
			if(!confirm('Вы действительно хотите удалить вкладку?')){
				return false;
			}
			var blockForDelete = $(this).closest('.tab-item');
			$.ajax({
				type: "POST",
				data: {
					key: blockForDelete.data('key'),
					'<?= Yii::app()->getRequest()->csrfTokenName;?>': '<?= Yii::app()->getRequest()->csrfToken;?>'
				},
				url: '<?= Yii::app()->createUrl($options['delete-route']);?>',
				success: function () {
					blockForDelete.remove();
					updateArrows();
				}
			});
		});

		$('body').on('click', '.button-up-tab', function () {
			var wrapper = $(this).closest('.tab-item');
			if(wrapper.prev('.tab-item').not('.hidden').length){
				wrapper.prev('.tab-item').not('.hidden').before(wrapper);
				updateSort();
				updateArrows();
			}
		});

		$('body').on('click', '.button-down-tab', function () {
			var wrapper = $(this).closest('.tab-item');
			if(wrapper.next('.tab-item').not('.hidden').length){
				wrapper.next('.tab-item').not('.hidden').after(wrapper);
				updateSort();
				updateArrows();
			}
		});
	});

	updateArrows();

	function updateSort() {
		var i = 0;
		$.each($('input.tab-sort'), function(){
			if(typeof $(this).attr('name') !== 'undefined'){
				$(this).val(i);
				i++;
			}
		});
	}

	function updateArrows() {
		$('.tab-item:not(.hidden)').find('.button-up-tab, .button-down-tab').removeClass('disabled');
		$('.tab-item:not(.hidden):first .button-up-tab').addClass('disabled');
		$('.tab-item:not(.hidden):last .button-down-tab').addClass('disabled');
	}
</script>