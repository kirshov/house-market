<?/**
 * @var int $modules
 * @var int $allCommentsCnt
 * @var int $newCommentsCnt
 * @var int $allReviewsCnt
 * @var int $newReviewsCnt
 * @var array $errors
 */?>
<div class="col-md-12 col-xs-12 col-sm-12">
	<div class="box no-border">
		<span class="box-cell col-xs-2 b-r-1 valign-middle text-xs-center bg-danger widget-panel-error-icon">
			<i class="fa fa-warning font-size-24"></i>&nbsp;<br/>
			<span>Обратите внимание!</span>
		</span>

		<div class="box-cell">
			<div class="box-container widget-panel-error">
				<?foreach ($errors as $item):?>
					<div class="box-row">
						<?if($item['link']):?>
							<a href="<?=$item['link']?>" class="box-cell p-l-2 p-a-05 valign-middle bg-danger">
								<?=$item['message']?>
							</a>
						<?else:?>
							<span class="box-cell p-l-2 p-a-05 valign-middle bg-danger"><?=$item['message']?></span>
						<?endif;?>
					</div>
				<?endforeach;?>
			</div>
		</div>
	</div>
</div>
