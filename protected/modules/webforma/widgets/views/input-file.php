<?
/**
 * @var \webforma\models\WModel $model
 * @var string $attribute
 * @var string $altAttribute
 * @var string $titleAttribute
 * @var int $width
 * @var int $height
 */
$attributeId = CHtml::activeId($model, $attribute);
?>
<div class="upload-widget">
	<div>
		<?=CHtml::activeLabel($model, $attribute, ['class' => 'control-label'])?>
	</div>
	<div class="col-xs-12 preview-image-wrapper<?= !$model->getIsNewRecord() && $model->{$attribute} ? '' : ' hidden' ?>">
		<?if($altAttribute || $titleAttribute):?>
			<div class="btn-group image-settings">
				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="collapse" data-target="#image-settings"><span class="fa fa-gear"></span></button>
				<div id="image-settings" class="dropdown-menu">
					<div class="container-fluid">
						<?if($altAttribute):?>
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<?=CHtml::activeLabel($model, $altAttribute, ['class' => 'control-label']);?>
										<?=CHtml::activeTextField($model, $altAttribute, ['class' => 'form-control', 'maxlength' => '250']);?>
									</div>
								</div>
							</div>
						<?endif?>
						<?if($titleAttribute):?>
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<?=CHtml::activeLabel($model, $titleAttribute, ['class' => 'control-label']);?>
										<?=CHtml::activeTextField($model, $titleAttribute, ['class' => 'form-control', 'maxlength' => '250']);?>
									</div>
								</div>
							</div>
						<?endif;?>
					</div>
				</div>
			</div>
		<?endif;?>
		<?=CHtml::image(!$model->getIsNewRecord() && $model->{$attribute} ? $uploadBehavior->getImageUrl($width, $height, false) : '#', '', [
			'class' => 'preview-image img-thumbnail',
			'style' => !$model->getIsNewRecord() && $model->{$attribute} ? '' : 'display:none',
		]); ?>
	</div>

	<div class="col-xs-12 preview-image-btns<?=!$model->{$attribute} || $model->hasErrors() ? ' empty' : ''?>">
		<div class="row">
			<div class="col-xs-12">
				<div class="form-group">
					<?=CHtml::activeLabel($model, $attribute, ['class' => 'control-label']);?>
					<?=CHtml::activeFileField($model, $attribute, ['class' => 'form-control', 'onchange' => 'readURL(this);', 'data-label' => '<i class="fa fa-upload"></i> '.($model->{$attribute} ? 'Изменить...' : 'Загрузить...')]);?>
				</div>
			</div>
		</div>

		<?php $this->widget('bootstrap.widgets.TbButton', [
			'buttonType' => 'button',
			'context' => 'secondary',
			'icon' => 'fa fa-trash',
			'htmlOptions' => [
				'title' => Yii::t('WebformaModule.webforma','Delete the file'),
				'class' => 'btn-label-icon delete-file'.(!$model->{$attribute} ? ' hidden is-new' : ''),
				'data-widget' => CJavaScript::jsonEncode([
					'model' => get_class($model),
					'pk' => $model->primaryKey,
					'attribute' => $attribute,
					'titleAttribute' => $titleAttribute,
					'altAttribute' => $altAttribute,
					'isNew' => $model->getIsNewRecord(),
					'behavior' => $behavior,
					'view' => $view,
				]),
				'data-link' => Yii::app()->createUrl('/'.$this->getController()->getModule()->getId().'/'.$this->getController()->getId().'/removeFile'),
			]
		]); ?>
	</div>
</div>
<script type="text/javascript">
	$('#<?=$attributeId?>').styler({
		fileBrowse: $('#<?=$attributeId?>').attr('data-label')
	});
</script>