<?
/**
 * @var string $name
 * @var string $value
 * @var array $items
 * @var array $labels
 */

?>
<div class="color-items-wrapper">
	<?if($items):?>
		<div class="color-items">
			<?foreach ($items as $_name => $_color):?>
				<div class="color-item">
					<label>
						<?$checked = $_name == $value ? ' checked' : '';?>
						<input type="radio" name="<?=$name?>" value="<?=$_name?>" <?=$checked?>>
						<div class="color-item__color<?=$checked ? ' active' : ''?>" style="background-color: <?=$_color?>" title="<?=$labels[$_name]?>">&nbsp;</div>
					</label>
				</div>
			<?endforeach;?>
		</div>
	<?endif;?>
</div>
<?/*
<script type="text/javascript">
	$(document).ready(function () {
		var picker = new CP(document.querySelector('input.<?=$name?>-other'), 'click');
		console.log(picker);
		picker.on('change', function(color) {
			this.source.value = '#' + color;
			$('.color-other-item .color-item__color').css('background-color', '#' + color);
		});

	});
</script>
*/