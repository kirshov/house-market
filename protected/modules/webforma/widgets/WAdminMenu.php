<?php
/**
 * Виджет панели быстрого запуска
 *
 **/
namespace webforma\widgets;

use Yii;

/**
 * Class WAdminMenu
 * @package webforma\widgets
 */
class WAdminMenu extends WWidget
{
    /**
     * @var
     */
    public $items;

    /**
     * @var string
     */
    public $view = 'leftmenu';

    /**
     *
     */
    public function run()
    {
		$items = $this->prepareItems($this->items);

        $this->render($this->view, [
        	'items' => $items,
		]);
    }

	protected function prepareItems($items, $depth = 1)
	{
		if(!is_array($items)){
			return [];
		}
		foreach($items as $i => $item){
			if(!is_array($item)){
				unset($items[$i]);
				continue;
			}
			if($depth == 1 && empty($item['url'])){
				$items[$i]['url'] = '#';
			} elseif ($depth > 1 && empty($item['url'])){
				unset($items[$i]);
			}
			if(!empty($item['icon'])){
				$items[$i]['label'] = '<i class="'.$item['icon'].'"></i>'.$item['label'];
				$items[$i]['encodeLabel'] = false;
			}
			unset($items[$i]['submenuOptions']);

			$items[$i]['items'] = $this->prepareItems($item['items'], ($depth + 1));
			if(!empty($item['items'])){
				$items[$i]['itemOptions']['class'] = 'has-submenu';
			}
		}
		return array_values($items);
	}
}
