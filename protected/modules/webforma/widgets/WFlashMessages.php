<?php
/**
 * Виджет для отображения flash-сообщений
 **/
namespace webforma\widgets;

/**
 * Class WFlashMessages
 * @package webforma\widgets
 */
class WFlashMessages extends WWidget
{
    /**
     *
     */
    const SUCCESS_MESSAGE = 'success';
    /**
     *
     */
    const INFO_MESSAGE = 'info';
    /**
     *
     */
    const WARNING_MESSAGE = 'warning';
    /**
     *
     */
    const ERROR_MESSAGE = 'error';

    /**
     * @var array
     */
    public $options = [];

    /**
     * @var string
     */
    public $view = 'flashmessages';

    /**
     * @throws \CException
     */
    public function run()
    {
        $this->render($this->view, ['options' => $this->options]);
    }
}
