<?php
Yii::import('zii.widgets.CBreadcrumbs');

class WBreadcrumbs extends CBreadcrumbs{

	/**
	 * @var string
	 */
	public $activeLinkTemplate='<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="b-link-item"><a href="{url}" itemprop="item"><span itemprop="name">{label}</span></a><meta itemprop="position" content="{position}" /></span>';

	/**
	 * @var string
	 */
	public $inactiveLinkTemplate='<span>{label}</span>';
	/**
	 *
	 */
	/**
	 *
	 */
	public function run()
	{
		if(empty($this->links))
			return;

		$definedLinks = $this->links;

		echo '<' . $this->tagName .' itemscope itemtype="http://schema.org/BreadcrumbList" '.CHtml::renderAttributes($this->htmlOptions).">\n";
		$links=array();
		if($this->homeLink===null)
			$definedLinks=array(Yii::t('zii','Home') => Yii::app()->homeUrl)+$definedLinks;
		elseif($this->homeLink!==false)
			$links[]=$this->homeLink;

		$position = 1;
		foreach($definedLinks as $label=>$url)
		{
			if(is_string($label) || is_array($url))
				$links[]=strtr($this->activeLinkTemplate,array(
					'{url}'=>Yii::app()->createAbsoluteUrl(CHtml::normalizeUrl($url)),
					'{label}'=>$this->encodeLabel ? CHtml::encode($label) : $label,
					'{position}'=> $position,
				));
			else
				$links[]=str_replace('{label}',$this->encodeLabel ? CHtml::encode($url) : $url,$this->inactiveLinkTemplate);

			$position++;
		}
		echo implode($this->separator,$links);
		echo CHtml::closeTag($this->tagName);
	}
}