<?php
/**
 * Class WTabSettings
 * @package webforma\widgets
 */
class WTabSettings extends \webforma\widgets\WWidget
{
	/**
	 * @var
	 */
	public $name;

	/**
	 * @var
	 */
	public $value;

	/**
	 * @var array
	 */
	public $options = [];

    /**
     *
     */
    public function run()
    {
        $this->render('tab-settings', [
        	'name' => $this->name,
	        'value' => $this->value,
	        'options' => $this->options,
        ]);
    }
}
