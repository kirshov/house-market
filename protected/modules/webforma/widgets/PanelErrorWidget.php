<?php

/**
 * Class PanelErrorWidget
 */
class PanelErrorWidget extends \webforma\widgets\WWidget
{
	/**
	 * @throws CException
	 */
	public function run()
	{
		$modules = Yii::app()->moduleManager->getModules(false, false);

		$errors = [];
		if(!$modules['modules']){
			return false;
		}
		foreach ($modules['modules'] as $module){
			if ($module instanceof webforma\components\WebModule === false || !$module->getIsActive()){
				continue;
			}
			$messages = $module->checkSelf();
			if (is_array($messages)){
				foreach ($messages as $key => $value){
					if (!is_array($value) || empty($value)) {
						continue;
					}
					$errors = CMap::mergeArray($errors, $value);
				}
			}
		}

		if(!$errors){
			return false;
		}

		$this->render('panel-error', [
			'errors' => $errors,
		]);
	}
}
