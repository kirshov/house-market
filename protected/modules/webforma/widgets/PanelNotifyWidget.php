<?php

/**
 * Class PanelNotifyWidget
 */
class PanelNotifyWidget extends \webforma\widgets\WWidget
{
	/**
	 * @throws CException
	 */
	public function run()
	{
		$cacheTime = Yii::app()->controller->webforma->coreCacheTime;
		$comments = $reviews = $users = [];


		if(Yii::app()->hasModule('comment')){
			$comments = [
				'allCommentsCnt' => Comment::model()->cache($cacheTime)->count(),
				'newCommentsCnt' => Comment::model()->cache($cacheTime)->new()->count(),
			];
		}

		if(Yii::app()->hasModule('review')){
			$reviews = [
				'allReviewsCnt' => Review::model()->cache($cacheTime)->count(),
				'newReviewsCnt' => Review::model()->cache($cacheTime)->new()->count(),
			];
		}

		$users = [
			'allUsersCnt' => User::model()->cache($cacheTime)->count(),
			//'newReviewsCnt' => Review::model()->cache($cacheTime)->new()->count(),
		];

		$this->render('panel-notify', [
			'comments' => $comments,
			'reviews' => $reviews,
			'users' => $users,
		]);
	}
}
