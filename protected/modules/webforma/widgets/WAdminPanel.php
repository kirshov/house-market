<?php
/**
 * Виджет админ-панели для фронтальной части сайта
 **/

namespace webforma\widgets;

use Yii;
use CHtml;

/**
 * Class WAdminPanel
 * @package webforma\widgets
 */
class WAdminPanel extends WWidget
{
    /**
     * @var string
     */
    public $view = 'adminpanel';

    /**
     * @throws \CException
     */
    public function run()
    {
        $this->render($this->view, [
			'modules' => Yii::app()->moduleManager->getModules(true),
		]);
    }
}
