<?php
namespace webforma\widgets;
use webforma\models\WModel;

/**
 * Class WInputFile
 * @package webforma\widgets
 */
class WInputFile extends WWidget
{
	/**
	 * @var
	 */
	public $model;

	/**
	 * @var
	 */
	public $attribute;

	/**
	 * @var
	 */
	public $behavior;

	/**
	 * @var int
	 */
	public $width = 300;

	/**
	 * @var
	 */
	public $height = 300;

	/**
	 * @var
	 */
	public $altAttribute = 'image_alt';

	/**
	 * @var
	 */
	public $titleAttribute = 'image_title';

	/**
	 * @var string
	 */
	public $view = 'input-file';
    /**
     *
     */
    public function run()
    {
    	if(!$this->model instanceof WModel){
    		return false;
		}
        $this->render($this->view, [
        	'model' => $this->model,
        	'uploadBehavior' => $this->behavior ? $this->model->{$this->behavior} : $this->model,
        	'behavior' => $this->behavior,
        	'attribute' => $this->attribute,
        	'altAttribute' => isset($this->model->{$this->altAttribute}) ? $this->altAttribute : null,
        	'titleAttribute' => isset($this->model->{$this->titleAttribute}) ? $this->titleAttribute : null,
        	'view' => $this->view,
		]);
    }
}
