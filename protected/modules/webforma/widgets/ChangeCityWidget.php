<?php

class ChangeCityWidget extends \webforma\widgets\WWidget
{
    /**
     * @var string
     */
    public $view = 'default';

	/**
	 * @var bool
	 */
	public $force = false;

    /**
     * @throws CException
     */
    public function init()
    {
	    parent::init();
    	if($this->force || Yii::app()->params['needChangeCity']){
            Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getController()->systemAssets . '/js/jquery.auto-complete.min.js', CClientScript::POS_END);
            Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getController()->systemAssets . '/js/change-city.js', CClientScript::POS_END);
            Yii::app()->getClientScript()->registerCssFile(Yii::app()->getController()->systemAssets . '/css/jquery.auto-complete.min.css');
	    }
    }

    /**
     * @throws CException
     */
    public function run()
    {
	    if($this->force || Yii::app()->params['needChangeCity']){
		    $this->render($this->view);
	    }
    }
}