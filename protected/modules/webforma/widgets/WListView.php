<?php
Yii::import('zii.widgets.CListView');

class WListView extends CListView
{

	/**
	 * @var bool
	 */
	public $ajaxUpdate = false;

	/**
	 * @var bool
	 */
	public $cssFile = false;

	/**
	 *
	 */
	public function init()
	{
		parent::init();
		if($this->pager !== null){
			$this->pager['cssFile'] = false;
		}
	}

	/**
	 * Renders the sorter.
	 */
	public function renderSorter2()
	{
		if($this->dataProvider->getItemCount()<=0 || !$this->enableSorting || empty($this->sortableAttributes)){
			return;
		}

		echo CHtml::openTag('div',array('class'=>$this->sorterCssClass))."\n";
		echo $this->sorterHeader===null ? Yii::t('zii','Sort by: ') : $this->sorterHeader;
		echo "<ul>\n";
		$sort=$this->dataProvider->getSort();
		foreach($this->sortableAttributes as $name=>$label) {
			echo "<li>";

			if(is_integer($name)){

				$directions = $sort->getDirections();
				if(isset($directions[$label])){
					$linkLabel = $sort->resolveLabel($label);
					echo '<span class="active">'.$linkLabel.'</span>';
				} else {
					echo $sort->link($label);
				}
			} else{
				echo $sort->link($name,$label);
			}

			echo "</li>\n";
		}
		echo "</ul>";
		echo $this->sorterFooter;
		echo CHtml::closeTag('div');
	}

	/**
	 * Renders the sorter.
	 */
	public function renderSorter()
	{
		if($this->dataProvider->getItemCount()<=0 || !$this->enableSorting || empty($this->sortableAttributes)){
            return;
        }

		echo CHtml::openTag('div',array('class'=>$this->sorterCssClass))."\n";
		echo $this->sorterHeader===null ? Yii::t('zii','Sort by: ') : $this->sorterHeader;
		echo "<ul>\n";
		$sort=$this->dataProvider->getSort();
		$params = $sort->params===null ? $_GET : $sort->params;
		/*if(empty($params)){
			$params['sort'] = 'price'.$sort->separators[1].'asc';
		}*/
		$paramsOriginal = $params;
		foreach($this->sortableAttributes as $name=>$label) {
			echo "<li>";
			if(is_integer($name)){
				$options = [];
				if(empty($params['sort']) && $label == 'price'){
					$options['class'] = 'asc';
				}

				echo $sort->link($label, null, $options);
				$params[$sort->sortVar] = $label.$sort->separators[1].'desc';
				$descLink = Yii::app()->getController()->createUrl($sort->route, $params);
				$descActiveClass = ($paramsOriginal['sort'] == $label.$sort->separators[1].'desc') ? ' active' : '';
				echo '<span class="sort_desc'.$descActiveClass.'">'.CHtml::link('', $descLink).'</span>';

				$params[$sort->sortVar] = $label;
				$ascLink = Yii::app()->getController()->createUrl($sort->route, $params);
				$ascActiveClass = ($paramsOriginal['sort'] == $label.$sort->separators[1].'asc' || $paramsOriginal['sort'] == $label || ($label == 'price' && empty($paramsOriginal['sort']))) ? ' active' : '';
				echo '<span class="sort_asc'.$ascActiveClass.'">'.CHtml::link('', $ascLink).'</span>';
			} else{
				echo $sort->link($name, $label);
			}

			echo "</li>\n";
		}
		echo "</ul>";
		echo $this->sorterFooter;
		echo CHtml::closeTag('div');
	}

	/**
	 * Registers necessary client scripts.
	 */
	public function registerClientScript()
	{
		$id=$this->getId();

		if($this->ajaxUpdate===false)
			$ajaxUpdate=array();
		else
			$ajaxUpdate=array_unique(preg_split('/\s*,\s*/',$this->ajaxUpdate.','.$id,-1,PREG_SPLIT_NO_EMPTY));
		$options=array(
			'ajaxUpdate'=>$ajaxUpdate,
			'ajaxVar'=>$this->ajaxVar,
			'pagerClass'=>$this->pagerCssClass,
			'loadingClass'=>$this->loadingCssClass,
			'sorterClass'=>$this->sorterCssClass,
			'enableHistory'=>$this->enableHistory
		);
		if($this->ajaxUrl!==null)
			$options['url']=CHtml::normalizeUrl($this->ajaxUrl);
		if($this->ajaxType!==null)
			$options['ajaxType']=strtoupper($this->ajaxType);
		if($this->updateSelector!==null)
			$options['updateSelector']=$this->updateSelector;
		foreach(array('beforeAjaxUpdate', 'afterAjaxUpdate', 'ajaxUpdateError') as $event)
		{
			if($this->$event!==null)
			{
				if($this->$event instanceof CJavaScriptExpression)
					$options[$event]=$this->$event;
				else
					$options[$event]=new CJavaScriptExpression($this->$event);
			}
		}

		$cs=Yii::app()->getClientScript();
		$cs->registerCoreScript('jquery');
	}
}