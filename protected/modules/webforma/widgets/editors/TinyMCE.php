<?php
namespace webforma\widgets\editors;

use Yii;
use CInputWidget;

/**
 * Class CKEditor
 * @package webforma\widgets\editors
 */
class TinyMCE extends \CInputWidget
{

	/**
	 * @var string
	 */
	public $selector = 'tinymce';

	/**
	 * @var array
	 */
	public $options = [];

	/**
	 * @var array
	 */
	public $settings = [];

    /**
     * @throws \Exception
     */
    public function run()
    {
	    $htmlOptions = [
		    'rows' => 12,
	    ];

	    if(($this->attribute == 'description' || $this->attribute == 'body') && !$this->options['rows']){
			$htmlOptions['rows'] = 25;
		}

		$htmlOptions = \CMap::mergeArray($htmlOptions, $this->options);

        if($this->hasModel()){
	        $this->widget('application.modules.webforma.extensions.tinymce.TinyMce', array(
		        'selector' => $this->selector,
		        'model' => $this->model,
		        'attribute' => $this->attribute,
		        'compressorRoute' => '/backend/webforma/tinyMce/compressor',
		        'fileManager' => true,
		        'htmlOptions' => $htmlOptions,
		        'settings' => $this->settings,
	        ));
        } else {
	        $htmlOptions['name'] = $this->name;
	        $this->widget('application.modules.webforma.extensions.tinymce.TinyMce', array(
				'selector' => $this->selector,
		        'compressorRoute' => '/backend/webforma/tinyMce/compressor',
		        'fileManager' => true,
		        'htmlOptions' => $htmlOptions,
		        'value' => $this->value,
				'settings' => $this->settings,
	        ));
        }
    }
}