<?php
/**
 * Класс webforma\widgets\WWidget - базовый класс для всех виджетов
 *
 * Все виджеты должны наследовать этот класс
 *
 */
namespace webforma\widgets;

use ReflectionClass;
use CWidget;
use Yii;

/**
 * Class WWidget
 * @package webforma\widgets
 */
abstract class WWidget extends CWidget
{
    /**
     * cacheTime - время кэширования выборки в виджете
     * если передано 0 - выборка не кэшируется, если ничего не передано - берется время жизни кэша из ядра:
     * Yii::app()->getModule('webforma')->coreCacheTime
     *
     */
    public $cacheTime;

    /**
     *
     *  limit - кол-во записей для вывода
     *
     */

    public $limit = 5;

    /**
     *  view - название шаблона (view) который используется для отрисовки виджета
     *
     *
     */
    public $view;

    /**
     * @since 0.8.1
     *
     * Модуль к которому относится виджет
     */
    public $module;

    /**
     * @param bool $checkTheme
     * @return null|string
     */
    public function getViewPath($checkTheme = false)
    {
        if (null === Yii::app()->getTheme()) {
            return parent::getViewPath($checkTheme);
        }

        $themeView = null;
        $reflection = new ReflectionClass(get_class($this));
        $path = explode(Yii::app()->getModulePath().DIRECTORY_SEPARATOR, $reflection->getFileName(), 2);

        if (isset($path[1])) {
            $path = explode(DIRECTORY_SEPARATOR, $path[1], 2);
            $themeView = Yii::app()->getThemeManager()->getBasePath().DIRECTORY_SEPARATOR.
                Yii::app()->getTheme()->getName().DIRECTORY_SEPARATOR.
                'views'.DIRECTORY_SEPARATOR.
                $path[0].DIRECTORY_SEPARATOR.
                'widgets'.DIRECTORY_SEPARATOR.
                $reflection->getShortName();

            if ($themeView && file_exists($themeView)) {
                return $themeView;
            }

            $themeView = implode(
                DIRECTORY_SEPARATOR,
                [Yii::app()->getModulePath(), $path[0], 'views', 'widgets', $reflection->getShortName()]
            );

            if ($themeView && file_exists($themeView)) {
                return $themeView;
            }
        }

        return parent::getViewPath($checkTheme);
    }


    /**
     *
     */
    public function init()
    {
        if (!$this->cacheTime && $this->cacheTime !== 0) {
            $this->cacheTime = (int)Yii::app()->getModule('webforma')->coreCacheTime;
        }

        parent::init();
    }

    public function render($view, $data = null, $return = false)
    {
        return parent::render($view, $data, $return);
    }

    public function renderPartial($view, $data = null, $return = false)
    {
        return parent::render($view, $data, $return);
    }

	public function widget($className, $properties = array(), $captureOutput = false)
	{
		if(Yii::app()->params['widgets'][$className]){
			$className = Yii::app()->params['widgets'][$className];
		}
		return parent::widget($className, $properties, $captureOutput);
	}


}
