<?php

namespace webforma\widgets;

use CException;

/**
 * Class RandomDataWidget
 * @package webforma\widgets
 */
class RandomDataWidget extends WWidget
{
    /**
     * @var
     */
    public $data;

    /**
     * @throws CException
     */
    public function init()
    {
        if (!is_array($this->data) || empty($this->data)) {
            throw new CException("'RandomDataWidget' empty data!");
        }
    }

    /**
     * @throws CException
     */
    public function run()
    {
        $this->render('randomdata', ['item' => $this->data[array_rand($this->data)]]);
    }
}
