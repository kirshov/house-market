<?php
/**
 * Виджет панели быстрого запуска
 *
 **/
namespace webforma\widgets;

use Yii;

/**
 * Class WShortCuts
 * @package webforma\widgets
 */
class WShortCuts extends WWidget
{
    /**
     * @var
     */
    public $modules;

    /**
     * @var string
     */
    public $view = 'shortcuts';

    /**
     *
     */
    public function run()
    {
        $this->render(
            $this->view,
            ['modules' => $this->modules, 'updates' => Yii::app()->migrator->checkForUpdates($this->modules)]
        );
    }
}
