<?php
/**
 * Class ActiveForm. Extend yii-booster TbActiveForm
 *
 * @since 0.9.4
 */

namespace webforma\widgets;

use Yii;
use TbActiveForm;
use CHtml;

Yii::import('bootstrap.widgets.TbActiveForm', true);

/**
 * Class ActiveForm
 * @package webforma\widgets
 */
class ActiveForm extends TbActiveForm
{
    /**
     * Output slug field.
     *
     * Set up $options['sourceModel'] and $options['sourceAttribute'] or just $options['sourceName'] with field name which is the source of slug field
     *
     * @since 0.9.4
     *
     * @throws \CException
     *
     * @param \CModel $model The data model.
     * @param string $attribute The attribute.
     * @param array $options
     * @return string
     */
    public function slugFieldGroup($model, $attribute, $options = array())
    {
        $this->initOptions($options);

        if (!isset($options['sourceModel'])) {
            $options['sourceModel'] = $model;
        }

        if (!isset($options['sourceName']) && !isset($options['sourceModel'], $options['sourceAttribute'])) {
            throw new \CException('Set up source field params for slug field!');
        }

        $sourceID = isset($options['sourceName']) ? CHtml::getIdByName($options['sourceName']) : CHtml::getIdByName(
            CHtml::resolveName($options['sourceModel'], $options['sourceAttribute'])
        );
        $targetID = CHtml::getIdByName(CHtml::resolveName($model, $attribute));

        $updateUrl = Yii::app()->createUrl('/webforma/backend/transliterate');

        $JS = <<<JS

        $(function () {
            var timer,
                sourceField = $('#{$sourceID}'),
                targetField = $('#{$targetID}'),
                updateUrl = "{$updateUrl}",
                editable = targetField.val().length == 0,
                value = sourceField.val();

            if (targetField.val().length !== 0) {
                $.get(updateUrl, {data: sourceField.val()}, function (r) {
                    editable = targetField.val() == r;
                });
            }

            sourceField.on('keyup blur copy paste cut start', function () {
                clearTimeout(timer);

                if (editable && value != sourceField.val()) {
                    timer = setTimeout(function () {
                        value = sourceField.val();
                        targetField.attr('disabled', 'disabled');
                        $.get(updateUrl, {data: sourceField.val()}, function (r) {
                            targetField.val(r).removeAttr('disabled');
                        });
                    }, 300);
                }
            });

            targetField.on('change', function () {
                editable = $(this).val().length == 0;
            });
        });
JS;

        Yii::app()->getClientScript()->registerScript($this->getId(), $JS, \CClientScript::POS_END);

        return $this->textFieldGroup($model, $attribute, $options);
    }

	protected function horizontalGroup(&$fieldData, &$model, &$attribute, &$options) {
		/*if($options['cols'] == 2){
			$labelCol = 4;
			$contentCol = 8;
		} elseif($options['cols'] == 3) {
			$labelCol = 3;
			$contentCol = 9;
		} elseif($options['cols'] == 12) {
			$labelCol = 3;
			$contentCol = 12;
		} else {
			$labelCol = 3;
			$contentCol = 12;
		}*/

		$groupOptions = isset($options['groupOptions']) ? $options['groupOptions']: array(); // array('class' => 'form-group');
		self::addCssClass($groupOptions, 'form-group');

		if ($model->hasErrors($attribute))
			self::addCssClass($groupOptions, 'has-error');

		echo CHtml::openTag('div', $groupOptions);

		self::addCssClass($options['labelOptions'], 'col-sm-'.$labelCol.' control-label');
		if (isset($options['label'])) {
			if (!empty($options['label'])) {
				echo CHtml::label($options['label'], CHtml::activeId($model, $attribute), $options['labelOptions']);
			} elseif(!$options['not-label']) {
				echo '<span class="col-sm-'.$labelCol.'"></span>';
			}
		} else {
			echo $this->labelEx($model, $attribute, $options['labelOptions']);
		}

		// TODO: is this good to be applied in vertical and inline?
		if(isset($options['wrapperHtmlOptions']) && !empty($options['wrapperHtmlOptions']))
			$wrapperHtmlOptions = $options['wrapperHtmlOptions'];
		else
			$wrapperHtmlOptions = $options['wrapperHtmlOptions'] = array();
		$this->addCssClass($wrapperHtmlOptions, 'col-sm-'.$contentCol);
		echo CHtml::openTag('div', $wrapperHtmlOptions);

		if (!empty($options['prepend']) || !empty($options['append'])) {
			$this->renderAddOnBegin($options['prepend'], $options['append'], $options['prependOptions']);
		}

		if (is_array($fieldData)) {
			echo call_user_func_array($fieldData[0], $fieldData[1]);
		} else {
			echo $fieldData;
		}

		if (!empty($options['prepend']) || !empty($options['append'])) {
			$this->renderAddOnEnd($options['append'], $options['appendOptions']);
		}

		if ($this->showErrors && $options['errorOptions'] !== false) {
			echo $this->error($model, $attribute, $options['errorOptions'], $options['enableAjaxValidation'], $options['enableClientValidation']);
		}

		if (isset($options['hint'])) {
			self::addCssClass($options['hintOptions'], $this->hintCssClass);
			echo CHtml::tag($this->hintTag, $options['hintOptions'], $options['hint']);
		}

		echo '</div></div>'; // controls, form-group
	}

	public function checkboxGroupFull($model, $attribute, $options = array()) {
		$this->initOptions($options);

		if ($this->type == self::TYPE_INLINE)
			self::addCssClass($options['labelOptions'], 'inline');

		$field = $this->checkbox($model, $attribute, $options['widgetOptions']['htmlOptions']);
		if ((!array_key_exists('uncheckValue', $options['widgetOptions']) || isset($options['widgetOptions']['uncheckValue']))
			&& preg_match('/\<input.*?type="hidden".*?\>/', $field, $matches)
		) {
			$hiddenField = $matches[0];
			$field = str_replace($hiddenField, '', $field);
		}

		$realAttribute = $attribute;
		CHtml::resolveName($model, $realAttribute);

		ob_start();
		echo '<div class="checkbox">';
		if (isset($hiddenField)) echo $hiddenField;
		echo CHtml::tag('label', $options['labelOptions'], false, false);
		echo $field;
		if (isset($options['label'])) {
			if ($options['label'])
				echo $options['label'];
		} else
			echo ' Да';//.$model->getAttributeLabel($realAttribute);
		echo CHtml::closeTag('label');
		echo '</div>';
		$fieldData = ob_get_clean();

		//$options['label'] = '';

		return $this->customFieldGroupInternal($fieldData, $model, $attribute, $options);
	}
}