<?php
namespace webforma\widgets;

use Yii;

Yii::import('booster.widgets.TbEditableField');

class WTbEditableField extends \TbEditableField
{
	/**
	 * @throws \CException
	 */
	public function init()
	{
		$newTitle = '';
		//generate title from attribute label
		if ($this->title === null) {
			$titles = array(
				'Select' => array('select', 'date'),
				'Check' => array('checklist')
			);
			$title = Yii::t('WebformaModule.editable', 'Enter');
			foreach($titles as $t => $types) {
				if(in_array($this->type, $types)) {
					$title = Yii::t('WebformaModule.editable', $t);
				}
			}
			$newTitle = $title . ' ' . $this->model->getAttributeLabel($this->attribute);
		}

		parent::init();

		if($newTitle){
			$this->title = $newTitle;
		}
	}

	/**
	 * @return string
	 */
	public function registerClientScript()
	{
		$selector = "a[rel=\"{$this->htmlOptions['rel']}\"]";
		if($this->liveTarget) {
			$selector = '#'.$this->liveTarget.' '.$selector;
		}
		$script = "$('".$selector."').off()";
		//$script = "$('".$selector."')";

		//attach events
		foreach(array('init', 'shown', 'save', 'hidden') as $event) {
			$eventName = 'on'.ucfirst($event);
			if (isset($this->$eventName)) {
				// CJavaScriptExpression appeared only in 1.1.11, will turn to it later
				//$event = ($this->onInit instanceof CJavaScriptExpression) ? $this->onInit : new CJavaScriptExpression($this->onInit);
				$eventJs = (strpos($this->$eventName, 'js:') !== 0 ? 'js:' : '') . $this->$eventName;
				$script .= "\n.on('".$event."', ".\CJavaScript::encode($eventJs).")";
			}
		}
		//apply editable
		$options = \CJavaScript::encode($this->options);
		$script .= ".editable($options);";
		//wrap in anonymous function for live update
		if($this->liveTarget) {
			if(!Yii::app()->getRequest()->getIsAjaxRequest()) {
				$script .= "\n $(function(){ setTimeout(function(){window['__f'] = undefined;}, 1000);}); if(typeof(window['__f']) === 'undefined') { $('body').off('ajaxUpdate.editable'); window['__f'] = true;}";

				$script .= "\n $('body').on('ajaxUpdate.editable', function(e){ if(e.target.id == '".$this->liveTarget."') yiiEditable_".$this->htmlOptions['rel']."(); });";
			}
			if(!Yii::app()->getRequest()->getIsAjaxRequest()){
				$script = "(function yiiEditable_".$this->htmlOptions['rel']."() {\n".$script."\n}());";
			}
		}

		Yii::app()->getClientScript()->registerScript(__CLASS__ . '-' . $selector, $script);

		return $script;
	}


	public function registerClientScriptOtherFix()
	{
		$selector = "a[rel=\"{$this->htmlOptions['rel']}\"]";
		if($this->liveTarget) {
			$selector = '#'.$this->liveTarget.' '.$selector;
		}
		$script = "$('".$selector."')";

		//check if script is already registered
		if(Yii::app()->getClientScript()->isScriptRegistered(__CLASS__ . '-' . $selector)) {
			return true;
		}

		//attach events
		foreach(array('init', 'shown', 'save', 'hidden') as $event) {
			$eventName = 'on'.ucfirst($event);
			if (isset($this->$eventName)) {
				$eventJs = (strpos($this->$eventName, 'js:') !== 0 ? 'js:' : '') . $this->$eventName;
				$script .= "\n.on('".$event."', ".\CJavaScript::encode($eventJs).")";
			}
		}

		//apply editable
		$options = \CJavaScript::encode($this->options);
		$script .= ".editable($options);";

		//wrap in anonymous function for live update
		if($this->liveTarget) {
			$script2 = "\n$('body').on('ajaxUpdate.editable',function(e){ if(e.target.id == '".$this->liveTarget."') yiiEditable2(); });";
			$script = "(function yiiEditable() {function yiiEditable2() {\n\t$script\n} $script2 yiiEditable2(); }\n());";
		}

		Yii::app()->getClientScript()->registerScript(__CLASS__ . '-' . $selector, $script);

		return $script;
	}
}