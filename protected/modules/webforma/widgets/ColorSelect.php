<?php
class ColorSelect extends \webforma\widgets\WWidget
{
	/**
	* @var
	*/
	public $name;

	/**
	* @var
	*/
	public $value;

	/**
	* @var
	*/
	public $options;

	/**
	 * @throws CException
	 */
	public function init()
	{
		parent::init();

		$mainAssets = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.webforma.views.assets'));
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile($mainAssets . '/plugins/color-picker/color-picker.min.js');
		$cs->registerCssFile($mainAssets . '/plugins/color-picker/color-picker.min.css');
	}


	/**
	 * @throws \Exception
	 */
	public function run()
	{
		$this->render('color-select', [
			'name' => $this->name,
			'items' => $this->options['items'],
			'labels' => $this->options['labels'],
			'value' => $this->value,
		]);
	}
}