<?php
namespace webforma\models;

use Yii;
use CDbExpression;
use CDbCriteria;
use CActiveDataProvider;
use CMap;

/**
 * Class Cities
 * @package webforma\models
 *
 * @property string title_ru
 * @property string area_ru
 * @property string region_ru
 * @property integer city_id
 * @property integer important
 */
class Cities extends WModel
{
    /**
     * Returns the static model of the specified AR class.
     * @return Settings the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{cities}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['title_ru', 'safe', 'on' => 'search'],
        ];
    }
}
