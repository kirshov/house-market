<?php

/**
 *
 * @package  webforma.modules.webforma.models
 *
 */

/**
 * This is the model class for table "Settings".
 *
 * The followings are the available columns in table 'Settings':
 * @property string $id
 * @property string $module_id
 * @property string $param_name
 * @property string $param_value
 * @property string $create_time
 * @property string $update_time
 * @property string $user_id
 *
 * The followings are the available model relations:
 * @property User $user
 */
namespace webforma\models;

use Yii;
use CDbExpression;
use CDbCriteria;
use CActiveDataProvider;
use CMap;

/**
 * Class Settings
 * @package webforma\models
 */
class Settings extends WModel
{
    /**
     *
     */
    const TYPE_TEXT = 1;
    /**
     *
     */
    const TYPE_JSON = 3;

    /**
     * @var array Массив хранящий список валидаторов для определенного параметра модуля
     */
    public $rulesFromModule = [];

    /**
     * Returns the static model of the specified AR class.
     * @return Settings the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{webforma_settings}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return CMap::mergeArray(
            [
                ['module_id, param_name', 'required'],
                ['module_id, param_name', 'length', 'max' => 100],
                ['param_value', 'safe'],
                ['user_id, type', 'numerical', 'integerOnly' => true],
                [
                    'id, module_id, param_name, param_value, create_time, update_time, user_id',
                    'safe',
                    'on' => 'search',
                ],
            ],
            $this->rulesFromModule
        );
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        $this->update_time = new CDbExpression('NOW()');

        if ($this->getIsNewRecord()) {
            $this->create_time = $this->update_time;
        }

        if($this->user_id === null){
	        $this->user_id = Yii::app()->hasComponent('user') ? Yii::app()->getUser()->getId() : null;
        }

        return parent::beforeSave();
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'User', 'user_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('WebformaModule.webforma', 'ID'),
            'module_id' => Yii::t('WebformaModule.webforma', 'Module'),
            'param_name' => Yii::t('WebformaModule.webforma', 'Parameter name'),
            'param_value' => Yii::t('WebformaModule.webforma', 'Parameter value'),
            'create_time' => Yii::t('WebformaModule.webforma', 'Creation date'),
            'update_time' => Yii::t('WebformaModule.webforma', 'Change date'),
            'user_id' => Yii::t('WebformaModule.webforma', 'User'),
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria();

        $criteria->compare('id', $this->id, true);
        $criteria->compare('module_id', $this->module_id, true);
        $criteria->compare('param_name', $this->param_name, true);
        $criteria->compare('param_value', $this->param_value, true);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);
        $criteria->compare('user_id', $this->user_id, true);

        return new CActiveDataProvider(get_class($this), ['criteria' => $criteria]);
    }


    /**
     * @param $moduleId
     * @param array|null $params
     * @return array
     */
    public static function fetchModuleSettings($moduleId, array $params = null)
    {
        $settings = [];

        if ($moduleId) {
            $criteria = new CDbCriteria();

            $criteria->compare("module_id", $moduleId);
            if (!empty($params)) {
                $criteria->addInCondition("param_name", $params);
            }

            $dependency = \TaggedCache\TaggingCacheHelper::getDependency([$moduleId, 'webforma']);

            $q = Settings::model()->cache(Yii::app()->getModule('webforma')->coreCacheTime, $dependency)->findAll(
                $criteria
            );

            if (count($q)) {
                foreach ($q as $s) {
                    $settings[$s->param_name] = $s;
                }
            } elseif (count($params)) {
                foreach ($params as $param) {
                    $settings[$param] = null;
                }
            }
        }

        return $settings;
    }


    /**
     * @param $moduleId
     * @param $paramValues
     * @param $paramTypes
     * @param $refresh
     * @return bool
     */
    public static function saveModuleSettings($moduleId, $paramValues, $paramTypes = array(), $refresh = false)
    {
    	$refreshWatermark = false;
        foreach ($paramValues as $name => $value) {
            // Получаем настройку
            $setting = Settings::model()->find(
                'module_id = :module_id and param_name = :param_name',
                [':module_id' => $moduleId, ':param_name' => $name]
            );

            // Если новая запись
            if ($setting === null) {
                $setting = new Settings();
                $setting->module_id = $moduleId;
                $setting->param_name = $name;

                if($moduleId == 'webforma' && in_array($name, ['watermark', 'watermarkFile', 'watermarkResolution'])){
					$refreshWatermark = true;
				}
            } // Если значение не изменилось то не сохраняем
            elseif ($setting->param_value == $value) {
                continue;
            } else {
				if($moduleId == 'webforma' && in_array($name, ['watermark', 'watermarkFile', 'watermarkResolution']) && $setting->param_value != $value){
					$refreshWatermark = true;
				}
			}


            // Присваиваем новое значение
            $setting->param_value = $value;
            if(isset($paramTypes[$name])){
            	$setting->type = $paramTypes[$name];
            }

            // Добавляем для параметра его правила валидации
            $setting->rulesFromModule = Yii::app()->getModule($moduleId)->getRulesForParam($name);

            //Сохраняем
            if (!$setting->save()) {
                return false;
            }
        }

        if($refresh) {
			Yii::app()->getModule($moduleId)->getSettings(true);
		}

		if($refreshWatermark) {
        	$thumbsPath = Yii::app()->uploadManager->getBasePath().'/thumbs/';
			\Helper::clearDir($thumbsPath.'store/');
			\Helper::clearDir($thumbsPath.'news/');
			\Helper::clearDir($thumbsPath.'articles/');
			\Helper::clearDir($thumbsPath.'images/');
		}

        return true;
    }

}
