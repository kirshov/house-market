<?php
/**
 * Класс базовой модели, в которой определены необходимые методы
 * для работы.
 * Все модели должны наследовать этот класс.
 *
 **/

namespace webforma\models;

use CActiveRecord;
use Yii;

abstract class WModel extends CActiveRecord
{
	/**
	 *
	 */
	const ACTION_TYPE_CREATE = 0;

	/**
	 *
	 */
	const ACTION_TYPE_UPDATE = 1;

	/**
	 * @var bool
	 */
	public $isAddLog = true;

	/**
	 *
	 */
	public $actionCode;

	/**
	 * @var array
	 */
    protected $_oldData = [];

	/**
	 * @var array
	 */
    protected $_oldModel = [];

    /**
     * Получение ссылки на объект модели
     * Это позволяет не писать каждый раз publiс static model в моделях Yii.
     *
     * @author Zalatov A.
     *
     * @param string $className Если необходимо, можно вручную указать имя класса
     * @return $this
     */
    public static function model($className = null)
    {
        if ($className === null) {
            $className = get_called_class();
        }
        return parent::model($className);
    }

    /**
     * Получение имени класса
     *
     * Этот метод необходим, чтобы постараться избежать использования имени класса как строки.
     * Метод get_class() принимает объект, поэтому не годится для статичного вызова.
     * Например, в relations можно теперь вместо 'CatalogItem' указывать CatalogItem::_CLASS_().
     * Это позволит использовать более точно Find Usages в IDE.
     *
     * Начиная с версии PHP > 5.5 есть магическая константа CLASS, которая аналогична.
     * Но в целях совместимости с более старыми версиями PHP, рекомендуется использовать именно этот метод.
     *
     * @author Zalatov A.
     *
     * @return string
     */
    public static function _CLASS_()
    {
        return get_called_class();
    }


    /**
     * Метод хранящий описания атрибутов:
     *
     * @return array описания атрибутов
     **/
    public function attributeDescriptions()
    {
        return [];
    }

    /**
     * Метод получения описания атрибутов
     *
     * @param string $attribute - id-атрибута
     *
     * @return string описания атрибутов
     **/
    public function getAttributeDescription($attribute)
    {
        $descriptions = $this->attributeDescriptions();

        return (isset($descriptions[$attribute])) ? $descriptions[$attribute] : '';
    }

	/**
	 * @return array
	 */
	public function getYesNoList(){
    	return [
    		0 => 'Нет',
		    1 => 'Да',
	    ];
	}

	protected function afterFind()
	{
		//$this->_oldModel = $this->getAttributes();
		parent::afterFind();
	}

	protected function beforeSave()
	{
		if($this->getIsNewRecord() && $this->getIsNeedLog() && $this->primaryKey){
			$this->_oldModel = $this->_loadModel($this->primaryKey);
		}
		return parent::beforeSave();
	}


	protected function afterSave()
	{
		if($this->getIsNeedLog() && !is_array($this->primaryKey)){
			try {
				$description = $type = null;
				$oldData = $currentData = [];
				foreach ($this->getAttributes() as $attribute => $value) {
					if(gettype($value) == 'object'){
						continue;
					}
					if ($this->_oldModel->{$attribute} != $value) {
						if($this->_oldModel->{$attribute} !== null){
							$oldData[$attribute] = $this->_oldModel->{$attribute};
						}

						if($value !== null){
							$currentData[$attribute] = $value;
						}
					}
				}

				if ($oldData || $currentData) {
					if($this->isNewRecord){
						$description = json_encode([
							'current' => $currentData,
						]);
					} else {
						$description = json_encode([
							'old' => $oldData,
							'current' => $currentData,
						]);
					}

					$type = 'json';

					$action = Yii::app()->getController()->getAction();
					$model = self::_CLASS_();
					$pk = $this->primaryKey;

					if($model == 'webforma\models\Settings' && isset($this->module_id)){
						$model = \Helper::mb_ucfirst($this->module_id).'Module';
						$pk = 0;
					}
					\Log::add([
						'model' => $model,
						'code' => $this->actionCode ? : ($this->isNewRecord ? self::ACTION_TYPE_CREATE : self::ACTION_TYPE_UPDATE),
						'model_id' => $pk,
						'type' => $type,
						'action' => isset($action) ? $action->getId() : '',
						'description' => $description,
					]);
				}
			} catch (\Exception $e){}
		}

		parent::afterSave();
	}

	/**
	 * @return bool
	 */
	private function getIsNeedLog(){
		return IS_BACKEND && $this->isAddLog && Yii::app()->hasModule('log');
	}

	/**
	 * @param $pk
	 * @return static
	 */
	private function _loadModel($pk){
		return $this::model()->findByPk($pk);
	}
}
