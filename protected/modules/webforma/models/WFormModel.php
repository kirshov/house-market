<?php
/**
 * webforma\models\WFormModel - базовый класс для всех form-моделей
 *
 * Все модели, должны наследовать этот класс.
 *
 */

namespace webforma\models;

use CFormModel;

/**
 * Class WFormModel
 * @package webforma\models
 */
abstract class WFormModel extends CFormModel
{
    /**
     * @return array
     */
    public function attributeDescriptions()
    {
        return [];
    }

    /**
     * @param $attribute
     * @return string
     */
    public function getAttributeDescription($attribute)
    {
        $descriptions = $this->attributeDescriptions();

        return (isset($descriptions[$attribute])) ? $descriptions[$attribute] : '';
    }
}
