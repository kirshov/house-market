<?php
/**
 * WMailMessage application component
 **/

Yii::import('application.modules.mail.models.*');

/**
 * Class YMailMessage
 */
class WMailMessage extends CApplicationComponent
{

	/**
	 * @var
	 */
	public $icon;

	/**
	 * @var \webforma\models\WModel
	 */
	protected $model;

	/**
	 * @var
	 */
	protected $module;

	/**
	 * @var MailModule $module
	 */
	protected $mailModule;

	/**
	 * @var WebformaModule
	 */
	protected $webforma;

	/**
	 * @var array
	 */
	protected $images = [];

	public function init()
	{
		parent::init();
		$this->mailModule = Yii::app()->getModule('mail');
		$this->webforma = Yii::app()->getModule('webforma');
	}


	/**
	 * @return array
	 */
	public function getDefaultMessages(){
		return [];
	}

	/**
	 * @param $code
	 * @param bool $isAdmin
	 * @return string
	 */
	public function getContentBlock($code, $isAdmin = false){
		switch ($code){
			case '%link-site%':
				return '<a style="'.$this->getStyle('btn').'" target="_blank" href="'.Yii::app()->createAbsoluteUrl('/').'">
					Перейти на сайт &rarr;
				</a>';
		}
		return '';
	}

	/**
	 * @param $code
	 * @return mixed|string
	 */
	public function getStyle($code){
		$styles = [
			'btn' => 'font: bold '.$this->mailModule->font.'; line-height: 50px; height: 50px; display: inline-block; border-radius: 5px; padding: 0 50px 0 60px; background: %btnColor%; color: %btnTextColor%; text-decoration: none; text-transform: uppercase;',
			'a' => 'color: %linkColor%; text-decoration: none;',
			'title' => 'font: bold %fontH2%; color: #222; text-align: center; margin-top: 15px; margin-bottom: 5px;',
			'subtitle' => 'font: bold %fontH3%; color: #222; text-align: center; margin-bottom: 15px;',
		];

		return isset($styles[$code]) ? $styles[$code] : '';
	}

	/**
	 * @param $code
	 * @return string|null
	 */
	public function getDefaultTemplate($code){
		$data = $this->getDefaultMessages();
		return isset($data[$code]) ? $data[$code] : null;
	}

	/**
	 * @param $model
	 * @return $this
	 */
	public function setModel($model){
		$this->model = $model;
		return $this;
	}

	/**
	 * @return \webforma\models\WModel
	 */
	public function getModel(){
		return $this->model;
	}

	/**
	 * @param $code
	 * @param bool $isAdmin
	 * @return bool
	 */
	public function getTemplate($code, $isAdmin = false){
		$template = MailTemplate::getTemplate($this->module, $code);
		if(!$template){
			$template = $this->getDefaultTemplate($code);
		}

		if($template){
			$template['subject'] = $this->parse($template['subject'], $isAdmin);
			$template['body'] = $this->parse($template['body'], $isAdmin);
		}


		return $template;
	}

	public function parse($text, $isAdmin = false){
		preg_match_all('/%[a-z_\-0-9]+%/', $text, $matches);
		if(!empty($matches[0])){
			foreach ($matches[0] as $match){
				if(!$match){
					continue;
				}
				$replace = $this->getContentBlock($match, $isAdmin);
				if($replace !== null){
					$text = str_replace($match, $replace, $text);
				}
			}
		}

		if($text){
			$text = strtr($text, [
				'%btnTextColor%' => $this->mailModule->btnTextColor,
				'%btnColor%' => $this->mailModule->btnColor,
				'%linkColor%' => $this->mailModule->linkColor,
				'%borderColor%' => $this->mailModule->borderColor,
				'%info-table-even%' => 'border: 1px solid #eee; padding: 17px; font: '.$this->mailModule->font.'; color: #32373d; background: #f8f8f8;',
				'%info-table-odd%' => 'border: 1px solid #eee; padding: 17px; font: '.$this->mailModule->font.'; color: #32373d;',
				'%font%' => $this->mailModule->font,
				'%fontH2%' => $this->mailModule->fontH2,
				'%fontH3%' => $this->mailModule->fontH3,
				'%siteName%' => $this->webforma->getShortSiteName(),
			]);
		}

		return $text;
	}

	/**
	 * @param $key
	 * @return mixed|string
	 */
	public function getImage($key){
		if(isset($this->images[$key])){
			return $this->images[$key];
		}

		return '';
	}

	/**
	 * @param $file
	 * @param $data
	 * @return false|string
	 */
	public function includeContent($file, $data){
		extract($data);
		ob_start();
		$path = Yii::getPathOfAlias('application.modules.'.$file).'.php';
		if(file_exists($path)){
			require $path;
		}

		return ob_get_clean();
	}
}
