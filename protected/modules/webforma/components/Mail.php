<?php

/**
 * Mail application component
 * Класс компонента Mail
 **/

namespace webforma\components;

use CApplicationComponent;

/**
 * Class Mail
 * @package webforma\components
 */
class Mail extends CApplicationComponent
{
    /**
     * @var \PHPMailer
     */
    private $_mailer;

    /**
     * Method to send mail: ("mail", "sendmail", or "smtp").
     * @var string
     */
    public $method = 'mail';

    /**
     * Smtp settings. Associative array. Required on method = smtp.
     *
     * array(
     *      'host' => 'example.com',
     *      'username' => 'username',
     *      'password' => '*******'
     * );
     * @var array
     */
    public $smtp;

    /**
     * Layout of the email body
     * @var string
     */
    public $layout;

    /**
     *
     */
    public function init()
    {
        $this->_mailer = new \PHPMailer();

        switch ($this->method) {
            case 'smtp':
                $this->_mailer->isSMTP();
                $this->_mailer->Host = $this->smtp['host'];
                if (!empty($this->smtp['username'])) {
                    $this->_mailer->SMTPAuth = true;
                    $this->_mailer->Username = $this->smtp['username'];
                    $this->_mailer->Password = $this->smtp['password'];
                } else {
                    $this->_mailer->SMTPAuth = false;
                }
                if (isset($this->smtp['port'])) {
                    $this->_mailer->Port = $this->smtp['port'];
                }
                if (isset($this->smtp['secure'])) {
                    $this->_mailer->SMTPSecure = $this->smtp['secure'];
                }
                if (isset($this->smtp['debug'])) {
                    $this->_mailer->SMTPDebug = (int)$this->smtp['debug'];
                }
                break;
            case 'sendmail':
                $this->_mailer->isSendmail();

                break;
            default:
                $this->_mailer->isMail();
        }

        $this->_mailer->CharSet = \Yii::app()->charset;

        parent::init();
    }

    /**
     * @return \PHPMailer
     */
    public function getMailer()
    {
        return $this->_mailer;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->_mailer->Subject;
    }

    /**
     * @param $subject
     */
    public function setSubject($subject)
    {
        $this->_mailer->Subject = $subject;
    }

    /**
     * @param $address
     * @param string $name
     * @throws \phpmailerException
     */
    public function setFrom($address, $name = '')
    {
    	if(!$name){
		    $name = \Yii::app()->getModule('webforma')->getEmailSiteName();
	    }

        $this->_mailer->setFrom($address, $name);
        $this->_mailer->addReplyTo($address, $name);
    }

    /**
     * @param $address
     * @param string $name
     */
    public function addAddress($address, $name = '')
    {
        $this->_mailer->addAddress($address, $name);
    }

    /**
     *
     */
    public function reset()
    {
        $this->init();
    }

    /**
     * Функция отправки сообщения:
     *
     * @param string $from - адрес отправителя
     * @param string|array $to - адрес(-а) получателя
     * @param string $theme - тема письма
     * @param string $body - тело письма
     * @param bool $isText - является ли тело письма текстом
     * @param array $replyTo добавляет заголовок Reply-To, формат [email => имя]
     *
     * @return bool отправилось ли письмо
     **/
    public function send($from, $to, $theme, $body, $isText = false, $replyTo = [])
    {
    	if(!$to){
    		return false;
	    }

    	if(!$from){
    		$from = 'noreply@'.$_SERVER['SERVER_NAME'];
	    }
        $this->_mailer->clearAllRecipients();

        $this->setFrom($from);

        if (is_array($to)) {
            foreach ($to as $email) {
                $this->addAddress($email);
            }
        } else {
			foreach (explode(',', $to) as $email) {
				$this->addAddress($email);
			}
			$this->addAddress($to);
        }

        $this->setSubject($theme);

        if ($isText) {
            $this->_mailer->Body = $body;
            $this->_mailer->isHTML(false);
        } else {
            $this->_mailer->msgHTML($body, \Yii::app()->basePath);
        }

        if (!empty($replyTo)) {
            $this->_mailer->clearReplyTos();
            foreach ($replyTo as $email => $name) {
                $this->_mailer->addReplyTo($email, $name);
            }
        }

	    $attachments = [];
	    if(!empty($_FILES['FeedBackForm'])){
		    foreach ($_FILES['FeedBackForm'] as $field => $_fileInfo) {
			    foreach ($_fileInfo as $_key => $_value) {
				    $attachments[$field] = $_value;
			    }
		    }
	    }

	    if(!empty($attachments)){
		    if(is_array($attachments['name'])){
			    foreach ($attachments['name'] as $_key => $_at){
				    $this->_mailer->addAttachment($attachments['tmp_name'][$_key], $_at);
			    }
		    } else {
			    $this->_mailer->addAttachment($attachments['tmp_name'], $attachments['name']);
		    }
	    }

        try {
            return $this->_mailer->send();
        } catch (\Exception $e) {
            \Yii::log($e->__toString(), \CLogger::LEVEL_ERROR, 'mail');

            return false;
        }
    }
}
