<?php
namespace webforma\components;

use Yii;
use CTheme;

/**
 * Class Theme
 * @package webforma\components
 */
class Theme extends CTheme
{
    /**
     * @var string
     */
    public $resourceFolder = 'web';

    /**
     * @var
     */
    private $_assetsUrl;


	/**
	 * @return string
	 * @throws \CException
	 */
    public function getAssetsUrl()
    {
        if (null === $this->_assetsUrl) {
            $this->_assetsUrl = Yii::app()->getAssetManager()->publish(
                $this->getBasePath().DIRECTORY_SEPARATOR.$this->resourceFolder
            );
        }

        return $this->_assetsUrl;
    }
}
