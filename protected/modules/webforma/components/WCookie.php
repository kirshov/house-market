<?php
namespace webforma\components;

class WCookie{

	/**
	 * @param $key
	 * @param $name
	 * @param array $options
	 */
	public static function setCookie($key, $name, $options = []){
		$cookieParams = \Yii::app()->session->cookieParams;

		$params = [];
		if($cookieParams['lifetime']){
			$params['expire'] = $cookieParams['lifetime'];
		}

		foreach (array('domain', 'path') as $item){
			if(!empty($cookieParams[$item])){
				$params[$item] = $cookieParams[$item];
			}
		}

		foreach ($options as $_key => $_value){
			$params[$_key] = $_value;
		}

		\Yii::app()->request->cookies[$key] = new \CHttpCookie($key, $name, $params);
	}

	/**
	 * @param $key
	 * @return string
	 */
	public static function getCookie($key){
		return \Yii::app()->request->cookies[$key]->value;
	}

	/**
	 * @param $key
	 * @return bool
	 */
	public static function hasCookie($key){
		return isset(\Yii::app()->request->cookies[$key]->value);
	}

	/**
	 * @param $key
	 */
	public static function removeCookie($key){
		unset(\Yii::app()->request->cookies[$key]);
	}
}