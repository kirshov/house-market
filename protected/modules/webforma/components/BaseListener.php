<?php

namespace webforma\components;


class BaseListener
{
	/**
	 * @var string
	 */
	public static $notifyMarker = '• ';

	/**
	 * @var string
	 */
	public static $notifyOrderMarker = '₽ ';

	/**
	 * @var string
	 */
	public static $notifyCallbackMarker = '☎ ';

	/**
	 * @var string
	 */
	public static $notifyQuestionMarker = '? ';
}