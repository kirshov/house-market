<?php

class SetCityAction extends CAction{

	public function run(){
		if(!Yii::app()->getRequest()->getIsAjaxRequest()){
			Yii::app()->end();
		}

		$cityId = (int) Yii::app()->getRequest()->getParam('city');
		if($cityId){
			$cityName = Yii::app()->getComponent('geoLocateManager')->getCityNameById($cityId);
			if($cityName){
				Yii::app()->eventManager->fire(GeoManagerEvents::CHANGE_CITY, new \webforma\components\Event());

				$_SESSION['geo'] = [
					'city' => [
						'id' => $cityId,
						'name_ru' => $cityName,
					]
				];
			}

			$data = ['result' => true];
			$type = Yii::app()->getRequest()->getParam('type');

			if($type == 'widget-cost'){
				$productId = (int) Yii::app()->getRequest()->getParam('product');
				if($productId){
					$productModel = \Product::model()->findByPk($productId);
					if($productModel){
						$data['html'] = $this->getController()->widget('deliveryCost', ['product' => $productModel], true);
					}
				}
			} elseif($type == 'cart') {
				if(Yii::app()->hasModule('delivery')){
					$deliveryTypes = \Delivery::getAll();
					$data['html'] = $this->getController()->renderPartial('//cart/cart/_delivery_v2',[
						'deliveryTypes' => $deliveryTypes,
						'order' => new \Order(),
					], true);
				}
			}

			echo json_encode($data);
			Yii::app()->end();
		}

		echo json_encode(['result' => false]);
		Yii::app()->end();
	}
}