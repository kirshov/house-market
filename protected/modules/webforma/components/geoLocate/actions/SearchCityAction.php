<?php

class SearchCityAction extends CAction{

	/**
	 *
	 */
	public function run()
	{
		if(!Yii::app()->getRequest()->getIsAjaxRequest()){
			Yii::app()->end();
		}

		$term = trim(Yii::app()->getRequest()->getParam('city'));
		if(mb_strlen($term) < 2){
			Yii::app()->end();
		}

		$word = preg_replace('/\s{2,}/iu', ' ', $term);

		$data = Yii::app()->getComponent('geoLocateManager')->getCityList([$word, \Helper::switcher($word)]);
		$list = [];

		if($data){
			foreach ($data as $id => $name){
				$list[] = [$id, $name];
			}
		}

		echo json_encode($list);
		Yii::app()->end();
	}
}