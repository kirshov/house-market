<?php
namespace webforma\components\geoLocate;

interface IGeoLocate{

	/**
	 * @param string $ip
	 * @return null|\webforma\components\geoLocate\GeoLocateCity
	 */
	public function getCityFromIp($ip);

	/**
	 * @param $id
	 * @return mixed
	 */
	public function getCityById($id);

	/**
	 * @param mixed $search
	 * @param int $limit
	 * @return array|null
	 */
	public function getCityList($search, $limit);
}