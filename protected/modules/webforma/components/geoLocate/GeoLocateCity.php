<?php
namespace webforma\components\geoLocate;

class GeoLocateCity{
	/**
	 * @var
	 */
	protected $name;

	/**
	 * @param array $data
	 */
	public function setData($data){
		if(is_array($data)){
			foreach ($data as $key => $value){
				$this->{$key} = $value;
			}
		}
	}

	/**
	 * @return array
	 */
	public function getData(){
		return [
			'name' => $this->name,
		];
	}

	public function __get($name)
	{
		if(isset($this->{$name})){
			return $this->{$name};
		}
	}
}