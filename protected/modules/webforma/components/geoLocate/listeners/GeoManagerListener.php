<?php
use webforma\components\Event;

/**
 * Class GeoManagerListener
 */
class GeoManagerListener
{
    /**
     * onChangeCity event listener
     *
     * @param Event $event
     */
    public static function onChangeCity(Event $event)
    {
        //перенести в модуль сдека
		unset($_SESSION['cdek_city_id']);
    }
}