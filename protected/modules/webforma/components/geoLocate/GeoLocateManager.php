<?php
namespace webforma\components\geoLocate;

class GeoLocateManager extends \CApplicationComponent
{
	public $service;
	public $useCookie = false;

	/**
	 * @var IGeoLocate
	 */
	protected $serviceProvider;
	protected $data;

	/**
	 * @return bool
	 */
    public function init()
    {
    	if(!$this->service){
    		return false;
	    }

	    $service = \Yii::import($this->service);

		$this->serviceProvider = new $service;
	    if(!$this->serviceProvider){
	    	return false;
	    }

	    \Yii::import('webforma.components.geoLocate.listeners.*');
	    \Yii::import('webforma.components.geoLocate.events.*');
    }

	/**
	 * @param null $ip
	 * @return null|GeoLocateCity
	 */
    public function getCityByIp($ip = null){
	    if(!$ip) {
		    $ip = \Yii::app()->getRequest()->getUserHostAddress();
	    }

	    return $this->serviceProvider->getCityFromIp($ip);
    }

	/**
	 * @return null
	 */
    public function getCurrentCity(){

    	if(!empty($_SESSION['geo']['city']['id'])){
			$data = $this->serviceProvider->getCityById($_SESSION['geo']['city']['id']);
		} else {
			$data = $this->getCityByIp();
		}

		if(!$data){
			return null;
		}
		return $data->name;
    }

	/**
	 * @param $id
	 * @return null
	 */
	public function getCityNameById($id){
		$data = $this->serviceProvider->getCityById($id);

		if(!$data){
			return null;
		}
		return $data->name;
	}

	/**
	 * @param null $search
	 * @param int $limit
	 * @return array|null
	 */
    public function getCityList($search = null, $limit = 10){
    	return $this->serviceProvider->getCityList($search, $limit);
	}

}
