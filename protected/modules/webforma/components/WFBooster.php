<?php
\Yii::import('vendor.clevertech.yii-booster.src.components.Booster');

class WFBooster extends Booster
{
	/**
	 * We use the values of $this->responsiveCss, $this->fontAwesomeCss,
	 * $this->minify and $this->enableCdn to construct the proper package definition
	 * and install and register it.
	 * @return array
	 */
	protected function createBootstrapCssPackage() {
		return array('bootstrap.css' => array(
			'baseUrl' => $this->enableCdn ? '//netdna.bootstrapcdn.com/bootstrap/3.1.1/' : \Yii::app()->getAssetManager()->publish(\Yii::getPathOfAlias('application.modules.webforma.views.assets'), false, -1, $this->forceCopyAssets) . '/',
			'css' => array( ($this->minify || $this->enableCdn) ? 'css/bootstrap.min.css' : 'css/bootstrap.css' ),
		));
	}
}