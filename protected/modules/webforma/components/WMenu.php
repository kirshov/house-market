<?php

Yii::import('zii.widgets.CMenu');


class WMenu extends \CMenu
{
	/**
	 * @var bool
	 */
	public $activateParents=true;

	/**
	 * @param array $item
	 * @param string $route
	 * @return bool
	 */
	protected function isItemActive($item, $route)
	{
		if((is_array($item['url']) && trim($item['url'][0], '/') == trim(Yii::app()->request->requestUri, '/')) || (is_string($item['url']) && trim($item['url'], '/') == trim(Yii::app()->request->requestUri,'/'))){
			return true;
		}

		if(isset($item['url']) && is_array($item['url']) && !strcasecmp(trim($item['url'][0],'/'),$route))
		{
			unset($item['url']['#']);
			if(count($item['url'])>1)
			{
				foreach(array_splice($item['url'],1) as $name=>$value)
				{
					if(!isset($_GET[$name]) || $_GET[$name]!=$value)
						return false;
				}
			}
			return true;
		}

		$url = trim(CHtml::normalizeUrl($item['url']), '/');
		$clearRequest = trim(Yii::app()->request->requestUri, '/');
		$expRequest = explode('/', $clearRequest);
		if($url == $expRequest[0]){
			return true;
		}
		return false;
	}
}