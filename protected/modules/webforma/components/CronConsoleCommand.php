<?php

namespace webforma\components;


/**
 * Class CronConsoleCommand
 */
class CronConsoleCommand
{
	/**
	 * @var int
	 */
    public $frequency = 24;

	/**
	 * @var int
	 */
	public $timeInterval = 24;

	/**
	 *
	 */
    public function init(){

	}

	/**
	 *
	 */
	public function run(){

	}
}
