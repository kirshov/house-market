<?php
namespace webforma\components\image;

use CException;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Yii;
use webforma\helpers\WFile;

/**
 * Class Thumbnailer
 * @package webforma\components\image
 */
class Thumbnailer extends \CApplicationComponent
{
    /**
     * @var string
     */
    public $thumbDir = 'thumbs';

    /**
     * @var array
     */
    public $options = [];

    /**
     * @var
     */
    private $_basePath;
    /**
     * @var
     */
    private $_baseUrl;

    /**
     * @param string $file Полный путь к исходному файлу в файловой системе
     * @param string $uploadDir Подпапка в папке с миниатюрами куда надо поместить изображение
     * @param int $width Ширина изображения. Если не указана - будет вычислена из высоты
     * @param int $height Высота изображения. Если не указана - будет вычислена из ширины
     * @param boolean $crop Обрезка миниатюры по размеру
     * @param boolean $isWatermark
     * @param int $minWatermarkResolution
     * @return string
     * @throws CException
     */
    public function thumbnail($file, $uploadDir, $width = 0, $height = 0, $crop = true, $isWatermark = false, $minWatermarkResolution = 0) {
        if (!$width && !$height) {
            throw new CException("Incorrect width/height");
        }

        $prefix = $crop ? 'cropped_' : '';
        $prefix = $crop === 2 ? 'blur_' : $prefix;
        $name = $width . 'x' . $height . '_' . $prefix . basename($file);
        $uploadPath = $this->getBasePath() . DIRECTORY_SEPARATOR . $uploadDir;
        $thumbFile = $uploadPath . DIRECTORY_SEPARATOR . $name;
        $thumbMode = $crop === true ? ImageInterface::THUMBNAIL_OUTBOUND : ImageInterface::THUMBNAIL_INSET;

        if (!file_exists($thumbFile)) {
            if (false === WFile::checkPath($uploadPath)) {
                throw new CException(
                    Yii::t(
                        'WebformaModule.webforma',
                        'Directory "{dir}" is not acceptable for write!',
                        ['{dir}' => $uploadPath]
                    )
                );
            }

            $img = Imagine::getImagine()->open($file);

            $originalWidth = $img->getSize()->getWidth();
            $originalHeight = $img->getSize()->getHeight();

            if (!$width) {
                $width = $height / $originalHeight * $originalWidth;
            }

            if (!$height) {
                $height = $width / $originalWidth * $originalHeight;
            }

			$img = $img->thumbnail(new Box($width, $height), $thumbMode);

			$originalWidthToBlur = $img->getSize()->getWidth();
			$originalHeightToBlur = $img->getSize()->getHeight();

            if($crop === 2 && ($originalWidthToBlur < $width || $originalHeightToBlur < $height)){
            	ini_set('max_execution_time', 0);
				$bgImg = Imagine::getBlur($file, $width, $height, 5);

				$positionX = $positionY = 0;
				if($originalWidthToBlur < $width){
					$positionX = ($width - $originalWidthToBlur) / 2;
				}
				if($originalHeightToBlur < $height){
					$positionY = ($height - $originalHeightToBlur) / 2;
				}
				$position = new Point($positionX, $positionY);
				if($position){
					$img = $bgImg->paste($img, $position);
				}
				unset($bgImg);
			}

			if($isWatermark) {
				$size = $img->getSize();
				if($size->getWidth() * $size->getHeight() > $minWatermarkResolution){
					$maxWmWidth = $width > $originalWidth ? $originalWidth : $width;
					$maxWmHeight = $height > $originalHeight ? $originalHeight : $height;

					$module = Yii::app()->getModule('webforma');
					$watermark = Imagine::getImagine()->open($module->getWatermark());

					$wSize = $watermark->getSize();
					if($module->watermarkResolution){
						$newWidth = $newHeight = 0;

						if(($maxWmWidth / $module->watermarkResolution) < $wSize->getWidth()){
							$newWidth = round($maxWmWidth / $module->watermarkResolution);
							$ratioOrig = $newWidth / $wSize->getWidth();
							$newHeight = $wSize->getHeight() * $ratioOrig;
						}

						if(($maxWmHeight / $module->watermarkResolution) < $wSize->getHeight()){
							$newHeight = round($maxWmHeight / $module->watermarkResolution);
							$ratioOrig = $newHeight / $wSize->getHeight();
							$newWidth  = $wSize->getWidth() * $ratioOrig;
						}

						if($newWidth && $newHeight){
							$watermark->resize(new \Imagine\Image\Box($newWidth, $newHeight));
							$wSize = $watermark->getSize();
						}
					}

					$position = false;
					$padding = 20;
					switch ($module->watermark){
						case 1:
							$position = new Point($size->getWidth() / $padding, $size->getHeight() / $padding);
							break;
						case 2:
							$position = new Point($size->getWidth() - $wSize->getWidth() - ($size->getWidth() / $padding), $size->getHeight() / $padding);
							break;
						case 3:
							$position = new Point($size->getWidth() / $padding, $size->getHeight() - $wSize->getHeight() - ($size->getHeight() / $padding));
							break;
						case 4:
							$position = new Point($size->getWidth() - $wSize->getWidth() - ($size->getWidth() / $padding), $size->getHeight() - $wSize->getHeight() - ($size->getHeight() / $padding));
							break;
						case 5:
							$position = new Point(($size->getWidth() / 2) - ($wSize->getWidth() / 2), ($size->getHeight() / 2) - ($wSize->getHeight() / 2));
							break;
						case 6:
							$positions = [];
							$amountX = self::getTailRatio($size->getWidth(), $wSize->getWidth());
							$amountY = self::getTailRatio($size->getHeight(), $wSize->getHeight());

							$densityX = $densityY = 1;
							if($amountX > 0)
								$densityX = $size->getWidth() / $amountX;

							if($amountY > 0)
								$densityY = $size->getHeight() / $amountY;

							$countY = 0;
							$countX = 0;

							$startPushX = ($size->getWidth() - (($amountX - 1) * $densityX) - $wSize->getWidth()) / 2;
							$startPushY = ($size->getHeight() - (($amountY - 1) * $densityY) - $wSize->getHeight()) / 2;
							if($startPushY <= 0)
								$startPushY = 0;
							$pushY = ($startPushY <= 0) ? 0 : $startPushY;

							$even = true;
							for($i = 1; $i <= ($amountX + 1) * ($amountY + 1); $i++) {
								if($countX >= $amountX + 1) {
									$countX = 0;
									$countY++;

									$even = !$even;
								}
								$pushX = ($even) ? $startPushX : -($wSize->getWidth() / 2);

								$positions[] = new Point(($countX * $densityX) + $pushX, ($countY * $densityY) + $pushY);
								$countX++;
							}

							if($positions){
								foreach ($positions as $_position){
									$img->paste($watermark, $_position);
								}
							}
							break;
					}

					if($position){
						$img->paste($watermark, $position);
					}
				}
			}
			$img->save($thumbFile, $this->options);
			unset($img);
			gc_collect_cycles();
        }

        $url = $this->getBaseUrl() . '/' . $uploadDir . '/' . $name;

        return $url;
    }

	/**
	 * @param $imgSize
	 * @param $wmSize
	 * @return int
	 */
	private function getTailRatio($imgSize, $wmSize){
		if($imgSize - $wmSize == 0){
			return 0;
		}
		$k = round(($wmSize * 100) / ($imgSize - $wmSize));

		if($k <= 0 || $k >= 35){
			$ratio = 1;
		}
		elseif($k >= 25 && $k < 35){
			$ratio = 2;
		}
		elseif($k >= 15 && $k < 25){
			$ratio = 3;
		}
		elseif($k >= 5 && $k < 15){
			$ratio = 4;
		}
		else{
			$ratio = 5;
		}

		return $ratio;
	}
    /**
     * @return string
     */
    public function getBasePath()
    {
        if ($this->_basePath === null) {
            $this->_basePath = Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->thumbDir;
        }

        return $this->_basePath;
    }

    /**
     * @param $value
     */
    public function setBasePath($value)
    {
        $this->_basePath = rtrim($value, DIRECTORY_SEPARATOR);
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        if ($this->_baseUrl === null) {
            $this->_baseUrl = Yii::app()->uploadManager->getBaseUrl() . '/' . $this->thumbDir;
        }

        return $this->_baseUrl;
    }

    /**
     * @param $value
     */
    public function setBaseUrl($value)
    {
        $this->_baseUrl = rtrim($value, '/');
    }
}
