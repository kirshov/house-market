<?php
/**
 * Компонент для управления модулями
 **/

namespace webforma\components;

use CChainedCacheDependency;
use CDirectoryCacheDependency;
use Exception;
use GlobIterator;
use Yii;
use webforma\widgets\WFlashMessages;
use webforma\helpers\WFile;

/**
 * Class ModuleManager
 * @package webforma\components
 */
class ModuleManager extends \CApplicationComponent
{
    /**
     *
     */
    const CORE_MODULE = 'webforma';

    /**
     *
     */
    const INSTALL_MODULE = 'install';

    /**
     * @var
     */
    public $otherCategoryName;
    /**
     * @var
     */
    public $category;
    /**
     * @var
     */
    public $categoryIcon;
    /**
     * @var
     */
    public $categorySort;

    /**
     * Возвращаем список модулей:
     *
     * @param bool $navigationOnly - только навигация
     * @param bool $disableModule - отключённые модули
     *
     * @return mixed
     **/
    public function getModules($navigationOnly = false, $disableModule = false)
    {
        $this->otherCategoryName = Yii::t('WebformaModule.webforma', 'Other');

        $this->categoryIcon = [
            Yii::t('WebformaModule.webforma', 'Services') => 'fa fa-fw fa-briefcase',
            Yii::t('WebformaModule.webforma', 'FORMA.cms') => 'fa fa-fw fa-cog',
            Yii::t('WebformaModule.webforma', 'Content') => 'fa fa-fw fa-file',
            $this->otherCategoryName => 'fa fa-fw fa-cog',
        ];

        $this->categorySort = [
            Yii::t('WebformaModule.webforma', 'Users'),
            Yii::t('WebformaModule.webforma', 'Content'),
            Yii::t('WebformaModule.webforma', 'Structure'),
            Yii::t('WebformaModule.webforma', 'Users'),
            Yii::t('WebformaModule.webforma', 'Services'),
            Yii::t('WebformaModule.webforma', 'FORMA.cms'),
            Yii::t('WebformaModule.webforma', 'Store'),
            $this->otherCategoryName,
        ];

        $modules = $yiiModules = $order = [];
        $modulesExtendedNavigation = [];

        if (count(Yii::app()->getModules())) {
            /**
             *
             * Получаем модули и заполняем основные массивы
             **/
            foreach (Yii::app()->getModules() as $key => $value) {
                $key = strtolower($key);
                $module = Yii::app()->getModule($key);
                if ($module !== null) {
                    if ($module instanceof WebModule) {
                    	$moduleCategory = $module->getCategory();
                    	if($moduleCategory === false){
                    		continue;
	                    }
                        $category = (!$moduleCategory)
                            ? $this->otherCategoryName
                            : $module->getCategory();
                        $modules[$key] = $module;
                        $order[$category][$key] = $module->adminMenuOrder;
                        $moduleExNav = (array)$module->getExtendedNavigation();
                        $modulesExtendedNavigation = array_merge($modulesExtendedNavigation, $moduleExNav);
                    } else {
                        $yiiModules[$key] = $module;
                    }
                }
            }

            $modulesNavigation = Yii::app()->getCache()->get('WebformaModulesNavigation');
            if(DEVMODE){
                $modulesNavigation = false;
            }

            if ($modulesNavigation === false) {

                // Формируем навигационное меню
                $modulesNavigation = [];

                // Сортируем категории модулей
                if (count($order) > 1) {
                    $categorySort = array_reverse($this->categorySort);

                    foreach ($categorySort as $iValue) {
                        if (array_key_exists($iValue, $order)) {
                            $orderValue = $order[$iValue];
                            unset($order[$iValue]);
                            $order = array_merge([$iValue => $orderValue], $order);
                        }
                    }
                }

                $uniqueMenuId = 0;

                $settings['items'] = [];

                // Обходим категории модулей
                foreach ($order as $keyCategory => $valueCategory) {

                    // Шаблон категорий
                    $modulesNavigation[$keyCategory] = [
                        'label' => $keyCategory,
                        //'url' => '#',
                        'items' => [],
                        'submenuOptions' => ["id" => "mainmenu_".$uniqueMenuId],
                    ];
                    $uniqueMenuId++;

                    if (array_key_exists($keyCategory, $this->categoryIcon)) {
                        $modulesNavigation[$keyCategory]['icon'] = $this->categoryIcon[$keyCategory];
                    }

                    // Сортируем модули в категории
                    asort($valueCategory, SORT_NUMERIC);

                    // Обходим модули
                    foreach ($valueCategory as $key => $value) {
                        $modSettings = [];
                        // Собраются подпункты категории "Настройки модулей", кроме пункта системного
                        if ($key !== self::CORE_MODULE && $modules[$key]->editableParams) {
							$modSettings = [
								'---',
								[
									'icon' => 'fa fa-fw fa-cog',
									'label' => Yii::t('WebformaModule.webforma', 'Module settings'),
									'url' => $modules[$key]->getSettingsUrl(),
								],
							];
                        }

                        // Проверка на вывод модуля в категориях, потребуется при отключении модуля
                        if (!$modules[$key]->getIsShowInAdminMenu()) {
                            continue;
                        }

                        // Если нет иконки для данной категории - подставляется иконка первого модуля
                        if (!isset($modulesNavigation[$keyCategory]['icon']) && $modules[$key]->icon) {
                            $modulesNavigation[$keyCategory]['icon'] = $modules[$key]->icon;
                        }

                        // Шаблон модулей
                        $data = [
                            'icon' => $modules[$key]->icon,
                            'label' => $modules[$key]->name,
                            'url' => $modules[$key]->adminPageLinkNormalize,
                            'submenuOptions' => ["id" => "submenu_".$key],
                            'items' => [],
                        ];

                        // Добавляем подменю у модулей
                        $links = $modules[$key]->getNavigation();
                        if (!empty($links)) {
                            $data['items'] = $links;
                        } else {
                            unset($modSettings[0]);
                        }

                        if ($key !== self::CORE_MODULE) {
                            $data['items'] = array_merge(
                                $data['items'],
                                $key === self::CORE_MODULE ? [] : $modSettings
                            );

	                        /*$advList = $modules[$key]->getNavigationInModule();
	                        if($advList){
		                        $data['items'] = array_merge(
			                        $data['items'],
			                        $advList
		                        );
	                        }*/
                        }

                        $appendTo = $modules[$key]->appendNavigationToModule();
                        if(!$appendTo){
                            $appendTo = $modules[$key]->id;
                        } else {
                            $data['label'] = $appendTo;
                        }

                        if(!isset($modulesNavigation[$keyCategory]['items'][$appendTo])){
                            $modulesNavigation[$keyCategory]['items'][$appendTo] = $data;
                        } else {
                            $modulesNavigation[$keyCategory]['items'][$appendTo] = \CMap::mergeArray($modulesNavigation[$keyCategory]['items'][$appendTo], $data);
                        }
                    }
                }

                foreach ($modulesNavigation as $key => $data) {
                    if (count($data['items']) === 1) {
                        $items = array_shift($modulesNavigation[$key]['items']);
                        $modulesNavigation[$key]['items'] = $items['items'];
                    }
                }

                // Цепочка зависимостей:
                $chain = new CChainedCacheDependency();

                // Зависимость на тег:
                $chain->dependencies->add(
	                \TaggedCache\TaggingCacheHelper::getDependency(['webforma', 'navigation', 'installedModules'])
                );

                // Зависимость на каталог 'application.config.modules':
                $chain->dependencies->add(
                    new CDirectoryCacheDependency(
                        Yii::getPathOfAlias('application.config.modules')
                    )
                );

                Yii::app()->getCache()->set(
                    'WebformaModulesNavigation',
                    $modulesNavigation,
                    0,
                    $chain
                );
            }
        }

        // Подгрузка отключенных модулей
        if ($disableModule) {
            $modules = array_merge((array)$this->getModulesDisabled($modules), $modules);
        }

        $modulesNavigation = array_merge($modulesNavigation, $modulesExtendedNavigation);

        return ($navigationOnly === true) ? $modulesNavigation : [
            'modules' => $modules,
            'yiiModules' => $yiiModules,
            'modulesNavigation' => $modulesNavigation,
        ];
    }

    /**
     * Подгружает и выдает список отключенных модулей
     *
     * @param array $enableModule - список активных модулей, по умолчанию array()
     *
     * @since 0.5
     *
     * @return array список отключенных модулей
     */
    public function getModulesDisabled($enableModule = [])
    {
        if (($imports = Yii::app()->getCache()->get('pathForImports')) !== false) {
            Yii::app()->getModule('webforma')->setImport($imports);
        }

        try {

            if ($imports === false || ($modules = Yii::app()->getCache()->get('modulesDisabled')) == false) {
                $modConfigs = Yii::getPathOfAlias('application.config.modules');
                $modPath = Yii::getPathOfAlias('application.modules');
                $cacheFile = Yii::app()->configManager->cacheFileName;

                foreach (new GlobIterator($modConfigs.'/*.php') as $item) {

                    if (is_dir(
                            $modPath.'/'.$item->getBaseName('.php')
                        ) == false && $cacheFile != $item->getBaseName('.php')
                    ) {

                        Yii::app()->getCache()->flush();

                        unlink($modConfigs.'/'.$item->getBaseName());

                        throw new Exception(
                            Yii::t(
                                'WebformaModule.webforma',
                                'There is an error occurred when try get modules from the cache. It seems that module\'s folder was deleted. Module is {module}...',
                                [
                                    'module' => $item->getBaseName(),
                                ]
                            )
                        );
                    }
                }

                $path = $this->getModulesConfigDefault();
                $enableModule = array_keys($enableModule);

                $modules = [];
                $imports = [];

                if ($handler = opendir($path)) {
                    while ($dir = readdir($handler)) {
                        if (!$this->isValidModule($dir)) {
                            continue;
                        }
                        if ($dir != '.' && $dir != '..' && !is_file($dir) && !isset($enableModule[$dir])) {
                            $modules[$dir] = $this->getCreateModule($dir);
                            $imports[] = Yii::app()->getCache()->get('tmpImports');
                        }
                    }
                    closedir($handler);
                }

                $chain = new CChainedCacheDependency();

                // Зависимость на тег:
                $chain->dependencies->add(
	                \TaggedCache\TaggingCacheHelper::getDependency(['webforma', 'modulesDisabled', 'getModulesDisabled', 'installedModules', 'pathForImports'])
                );

                // Зависимость на каталог 'application.config.modules':
                $chain->dependencies->add(
                    new CDirectoryCacheDependency(
                        Yii::getPathOfAlias('application.config.modules')
                    )
                );

                Yii::app()->getCache()->set('modulesDisabled', $modules, 0, $chain);
                Yii::app()->getCache()->set('pathForImports', $imports, 0, $chain);
            }
        } catch (Exception $e) {

            Yii::app()->getCache()->flush();

            Yii::app()->user->setFlash(
                WFlashMessages::ERROR_MESSAGE,
                $e->getMessage()
            );

            Yii::log($e->__toString(), \CLogger::LEVEL_ERROR);

            return false;
        }

        return $modules;
    }

    /**
     * Подгружает модуль
     *
     * @param array $name - имя модуля
     *
     * @since  0.5
     * @return array класс модуля
     */
    public function getCreateModule($name)
    {
        if (Yii::app()->hasModule($name)) {
            return Yii::app()->getModule($name);
        }
        $path = $this->getModulesConfigDefault();
        $module = null;
        if ($path) {
            //посмотреть внутри файл с окончанием Module.php
            $files = glob($path.'/'.$name.'/'.'*Module.php');
            if (count($files) === 1) {
                $className = pathinfo($files[0], PATHINFO_FILENAME);
                Yii::app()->getCache()->set('tmpImports', 'application.modules.'.$name.'.'.$className);
                Yii::import('application.modules.'.$name.'.'.$className);
                $module = Yii::createComponent($className, $name, null, false);
            }
        }

        return $module;
    }

    /**
     * Получаем путь к папке или файлу с конфигурацией модуля(-ей)
     *
     * @param bool $module - Имя модуля
     *
     * @since 0.5
     * @return string путь к папке или файлу с конфигурацией модуля(-ей)
     */
    public function getModulesConfig($module = false)
    {
        return Yii::app()->getBasePath().'/config/modules/'.($module ? $module.'.php' : '');
    }

    /**
     * Получаем путь к папке c дефолтной конфигурацией модуля
     *
     * @param string $module Имя модуля
     *
     * @since 0.5
     * @return string путь к папке c дефолтной конфигурацией модуля или путь к модулям
     */
    public function getModulesConfigDefault($module = '')
    {
        return empty($module) ? Yii::getPathOfAlias('application.modules') :
            Yii::getPathOfAlias('application.modules.'.$module).'/install/'.$module.'.php';
    }

    /**
     * Метод проверяет является ли каталог валидным модулем Yii/Webforma
     *
     * @param string $module - ID модуля
     *
     * @since 0.6
     *
     * @return boolean true - модуль валиде false - нет
     */
    public function isValidModule($module)
    {
        if (!$module) {
            return false;
        }

        $modulePath = Yii::app()->moduleManager->getModulesConfigDefault().DIRECTORY_SEPARATOR.$module;

        if (!is_dir($modulePath)) {
            return false;
        }

        $files = glob($modulePath.DIRECTORY_SEPARATOR.'*Module.php');

        return empty($files) ? false : true;
    }

    /**
     * Обновить конфигурационный файл модуля
     *
     * @param  WebModule $module
     * @return bool
     * @since 0.8
     */
    public function updateModuleConfig(WebModule $module)
    {
        $newConfig = $this->getModulesConfigDefault($module->getId());

        $currentConfig = $this->getModulesConfig($module->getId());

        if ((!file_exists($currentConfig) || WFile::rmFile($currentConfig)) && WFile::cpFile(
                $newConfig,
                $currentConfig
            )
        ) {
            Yii::app()->configManager->flushDump();

            return true;
        }

        return false;
    }
}
