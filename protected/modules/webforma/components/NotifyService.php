<?php

/**
 * Class NotifyService
 */
class NotifyService extends CApplicationComponent
{
    /**
     * @var
     */
    public $mail = 'mail';

    /**
     * @var \webforma\components\controllers\Controller
     */
    protected $view;

    /**
     * @var
     */
    protected $module;

	/**
	 * @var string
	 */
	protected $templatePath;

	/**
	 * @var string
	 */
	protected $messageComponent;

    /**
     *
     */
    public function init()
    {
        parent::init();

        if ($this->mail && Yii::app()->hasComponent($this->mail)) {
            $this->mail = Yii::app()->getComponent('mail');
        } else {
            $this->mail = Yii::app()->mail;
        }

        $this->view = clone Yii::app()->getController();
    }

	/**
	 * @param $code
	 * @return string
	 */
    public function getFilePath($code){
    	if(!$this->templatePath){
    		return false;
		}
	    return Yii::app()->getTheme()->getBasePath().'/views/'.$this->templatePath.'/'.$code.'.php';
    }

	/**
	 * @param $code
	 * @return string
	 */
	public function getRenderFile($code){
		return '//'.$this->templatePath.'/'.$code;
	}

	/**
	 * @param Order $model
	 * @param string $code
	 * @return string
	 */
	protected function getDefaultSubject($model, $code){}

	/**
	 * @param $model
	 * @param $code
	 * @param $isAdmin
	 * @return string
	 */
	protected function getBody($model, $code, $isAdmin = false){
		return '';
	}

	/**
	 * @param $model
	 * @param $code
	 * @param $isAdmin
	 * @return array
	 */
	protected function getMailContent($model, $code, $isAdmin = false){
		$this->view->setTemplateVar('isAdmin', $isAdmin);
		$file = $this->getFilePath($code);
		if($file && is_file($file)){
			$subject = $this->getDefaultSubject($model, $code);
			$body = $this->getBody($model, $code, $isAdmin);
		} else {
			$mailTemplate = Yii::app()->getComponent($this->messageComponent)->setModel($model)->getTemplate($code, $isAdmin);
			$subject = $mailTemplate['subject'];
			$body = $this->view->render('//webforma/mail/index', ['content' => $mailTemplate['body']], true);
		}

		return [
			'subject' => $subject,
			'body' => $body,
		];
	}
} 
