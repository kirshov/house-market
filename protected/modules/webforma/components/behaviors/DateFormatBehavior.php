<?php
namespace webforma\components\behaviors;

use CActiveRecordBehavior;
use Yii;

/**
 * Class DateFormatBehavior
 * @package webforma\components\behaviors
 */
class DateFormatBehavior extends CActiveRecordBehavior
{
    /**
     * @var string
     */
    public $attributeName = 'date';

    /**
     * @var string
     */
    public $dbFormat = 'Y-m-d H:i:s';

    /**
     * @var string
     */
    public $siteFormat = 'd.m.Y H:i:s';

    /**
     * @param \CModelEvent $event
     */
    public function beforeSave($event)
    {
        $value = $this->getOwner()->{$this->attributeName};
        if(\Helper::isDate($value, $this->siteFormat)){
            $this->getOwner()->{$this->attributeName} = date($this->dbFormat, strtotime($value));
        } else {
            $this->getOwner()->{$this->attributeName} = '0000-00-00';
        }

        return parent::beforeSave($event);
    }

    public function afterFind($event)
    {
        $value = $this->getOwner()->{$this->attributeName};
        if(\Helper::isDate($value, $this->dbFormat)){
            $this->getOwner()->{$this->attributeName} = date($this->siteFormat, strtotime($value));
        } else {
            $this->getOwner()->{$this->attributeName} = null;
        }

        return parent::afterFind($event);
    }
} 
