<?php

namespace webforma\components\behaviors;

use Yii;
use webforma\components\image\Imagine;
use webforma\components\image\Thumbnailer;
use webforma\helpers\WFile;

/**
 * Class ImageUploadBehavior
 * @package webforma\components\behaviors
 */
class ImageUploadBehavior extends FileUploadBehavior
{
    /**
     * @var bool
     */
    public $resizeOnUpload = false;
    /**
     * @var array
     */
    public $resizeOptions = [];

	/**
	 * @var int
	 */
    public $minWatermarkResolution = 40000;

    /**
     * @var array
     */
    protected $defaultResizeOptions = [
        'width' => 2000,
        'height' => 2000,
        'quality' => [
            'jpeg_quality' => 100,
            'png_compression_level' => 1,
        ],
    ];

    /**
     * @var null|string Полный путь к изображению по умолчанию в публичной папке
     */
    public $defaultImage = null;

    /**
     * @var Thumbnailer $thumbnailer ;
     */
    protected $thumbnailer;

	/**
	 * @var array
	 */
    protected $watermarkModels = ['Product', 'ProductImage', 'News', 'Articles', 'Image'];

    /**
     * @param \CComponent $owner
     */
    public function attach($owner)
    {
        parent::attach($owner);

        $this->thumbnailer = Yii::app()->thumbnailer;

        if ($this->resizeOnUpload) {
            $this->resizeOptions = array_merge(
                $this->defaultResizeOptions,
                $this->resizeOptions
            );
        }
    }

    /**
     *
     */
    protected function removeFile()
    {
		$this->removeThumbs();
		parent::removeFile();
	}

    /**
     *
     */
    protected function removeThumbs()
    {
        $filename = pathinfo($this->getOldFilePath(), PATHINFO_BASENAME);

        $iterator = new \GlobIterator(
            $this->thumbnailer->getBasePath().'/'.$this->uploadPath.'/'.'*_'.$filename
        );

        foreach ($iterator as $file) {
            @unlink($file->getRealPath());
        }
    }

    /**
     * @throws \CException
     */
    public function saveFile()
    {
        if (!$this->resizeOnUpload || $this->getUploadedFileInstance()->getExtensionName() == 'csv') {
            return parent::saveFile();
        }

        $newFileName = $this->generateFilename();
        $path = $this->uploadManager->getFilePath($newFileName, $this->getUploadPath());

        if (!WFile::checkPath(pathinfo($path, PATHINFO_DIRNAME))) {
            throw new \CException(
                Yii::t(
                    'WebformaModule.webforma',
                    'Directory "{dir}" is not acceptable for write!',
                    ['{dir}' => $path]
                )
            );
        }

        Imagine::resize(
            $this->getUploadedFileInstance()->getTempName(),
            $this->resizeOptions['width'],
            $this->resizeOptions['height']
        )->save(
            $path,
            $this->resizeOptions['quality']
        );

        $this->getOwner()->setAttribute($this->attributeName, $newFileName);
    }

    /**
     * @param int $width
     * @param int $height
     * @param bool|true $crop
     * @param null $defaultImage
     * @return null|string
     */
    public function getImageUrl($width = 0, $height = 0, $crop = true, $defaultImage = null)
    {
    	if($crop === 2 && !function_exists('gd_info')){
    		$crop = true;
		}
        $file = $this->getFilePath();
        $webRoot = Yii::getPathOfAlias('webroot');
        $defaultImage = $defaultImage ?: $this->defaultImage;

        if(!$defaultImage){
			$mainAssets = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.webforma.views.frontend.assets'));
	        $defaultImage = $mainAssets . '/images/nophoto.jpg';
        }

        if($file && \CFileHelper::getExtension($file) == 'svg'){
			return $this->getFileUrl();
		}

        if (null === $file && (null === $defaultImage || !is_file($webRoot.$defaultImage))) {
            return null;
        }

		$isWatermark = false;

        if(in_array(get_class($this->getOwner()), $this->watermarkModels)){
			$module = Yii::app()->getModule('webforma');
			$isWatermark = $file && $module->watermark && $module->watermarkFile && is_file($module->getWatermark());

			if($isWatermark){
				if ($width || $height) {

				} elseif(!$width && !$height) {
					$size = getimagesize($file);
					$width = $size[0];
					$height = $size[1];
				}

				if($width * $height <= $this->minWatermarkResolution){
					$isWatermark = false;
				}
			}
		}

        if ($width || $height) {
            return $this->thumbnailer->thumbnail($file ?: $webRoot.$defaultImage, $this->uploadPath, $width, $height, $crop, $isWatermark, $this->minWatermarkResolution);
        }

        return $file ? $this->getFileUrl() : $defaultImage;
    }


}
