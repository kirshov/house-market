<?php
namespace webforma\components\behaviors;

use CActiveRecordBehavior;
use Yii;

/**
 * Class PrepareContentBehavior
 * @package webforma\components\behaviors
 */
class PrepareContentBehavior extends CActiveRecordBehavior
{
    /**
     * @var string
     */
    public $attributeName = null;

    public function prepareContent(){
        $value = $this->getOwner()->{$this->attributeName};

	    $widgets = [
		    'content' => 'code',
		    'pickup' => 'group',
		    'feedback' => 'view',
		    'products' => 'ids',
		    'discounts' => null,
		    'faq' => null,
		    'dictionary' => 'code',
	    ];
	    $matches = [];
	    preg_match_all('/\[('.implode('|', array_keys($widgets)).')(\:[a-zA-Z0-9\-\_,]+\]){0,1}/', $value, $matches);
	    if(!empty($matches[0])){
		    foreach ($matches[0] as $match){
			    list($widget, $params) = explode(':', trim($match, '[]'));
				if($this->checkWidget($params, $params)){
					if($widgets[$widget] !== null){
						$block = Yii::app()->getController()->widget($widget, [$widgets[$widget] => $params], true);
						$value = str_replace('<p>['.$widget.':'.$params.']</p>', $block, $value);
						$value = str_replace('['.$widget.':'.$params.']', $block, $value);
					} else {
						$block = Yii::app()->getController()->widget($widget, [], true);
						$value = str_replace('<p>['.$widget.']</p>', $block, $value);
						$value = str_replace('['.$widget.']', $block, $value);
					}
				}
		    }
		    $this->getOwner()->{$this->attributeName} = $value;
	    }
	    return $this->getOwner();
    }

	/**
	 * @param $widget
	 * @param $params
	 * @return bool
	 */
    protected function checkWidget($widget, $params){
    	switch ($widget){
		    case 'products':
			    if(!$params){
			    	return false;
			    }
		    	break;
	    }

	    return true;
    }
}