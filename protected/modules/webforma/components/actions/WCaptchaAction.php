<?php

/**
 * Yii Extended Captcha
 * Класс реализующий Капчу, параметры длинны которой извлекаются
 * из настроек модуля.
 **/
namespace webforma\components\actions;

use CCaptchaAction;

/**
 * Class WCaptchaAction
 * @package webforma\components\actions
 */
class WCaptchaAction extends CCaptchaAction
{
    /**
     * @var int|mixed
     */
    public $minLength = 3;
    /**
     * @var int|mixed
     */
    public $maxLength = 6;

	/**
	 * @var mixed
	 */
	public $backColor = 0xFFFFFF;

	/**
	 * @var mixed
	 */
	public $foreColor = 0x000000;

	/**
	 * @var int|mixed
	 */
	//public $testLimit = 1;

    /**
     * @param \CController $controller
     * @param string $id
     */
    public function __construct($controller, $id)
    {
        parent::__construct($controller, $id);

        /*$module = $controller->getModule();

        if ($module && property_exists($module, "minCaptchaLength")) {
            $this->minLength = $module->minCaptchaLength;
        }
        if ($module && property_exists($module, "maxCaptchaLength")) {
            $this->maxLength = $module->maxCaptchaLength;
        }*/
    }

}
