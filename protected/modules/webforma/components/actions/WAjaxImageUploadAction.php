<?php
/**
 * WAjaxImageUploadAction.php file.
 */

namespace webforma\components\actions;

use Yii;
use Image;

/**
 * Class WAjaxImageUploadAction
 * @package webforma\components\actions
 */
class WAjaxImageUploadAction extends WAjaxFileUploadAction
{
    /**
     * @return bool
     */
    protected function uploadFile()
    {
        if (!$this->uploadedFile || !Yii::app()->hasModule('image')) {
            return false;
        }

        if (false === getimagesize($this->uploadedFile->getTempName())) {
            return false;
        }

        $image = new Image();
        $image->setScenario('insert');
        $image->addFileInstanceName('file');
        $image->setAttribute('name', $this->uploadedFile->getName());
        $image->setAttribute('alt', $this->uploadedFile->getName());
        $image->setAttribute('type', Image::TYPE_SIMPLE);

        if ($image->save()) {

            $this->fileLink = $image->getImageUrl();
            $this->fileName = $image->getName();

            return true;
        }

        return false;
    }
}
