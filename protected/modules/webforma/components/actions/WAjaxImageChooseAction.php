<?php
/**
 * WAjaxImageChooseAction.php file.
 */

namespace webforma\components\actions;

use Yii;
use CAction;
use Image;

/**
 * Class WAjaxImageChooseAction
 * @package webforma\components\actions
 */
class WAjaxImageChooseAction extends CAction
{
    /**
     *
     */
    public function run()
    {
        if (Yii::app()->hasModule("image")) {
            $upPath = Yii::app()->getBaseUrl().DIRECTORY_SEPARATOR.Yii::app()->getModule('webforma')->uploadPath.
                DIRECTORY_SEPARATOR.Yii::app()->getModule('image')->uploadPath.
                DIRECTORY_SEPARATOR;

            $images = Image::model()->findAllByAttributes(
                ['category_id' => null, 'parent_id' => null]
            );

            $forJson = [];

            if (!empty($images)) {
                foreach ($images as $img) {
                    $forJson[] = [
                        'thumb' => $upPath.$img->file,
                        'image' => $upPath.$img->file,
                        'title' => $upPath.$img->name,
                    ];
                }
            }

            Yii::app()->ajax->raw($forJson);
        }
    }
}
