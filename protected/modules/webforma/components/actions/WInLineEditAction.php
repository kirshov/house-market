<?php
namespace webforma\components\actions;

use CAction;
use CHttpException;
use CActiveRecord;
use Yii;

/**
 * Class WInLineEditAction
 * @package webforma\components\actions
 */
class WInLineEditAction extends CAction
{
    /**
     * @var
     */
    public $model;

    /**
     * @var
     */
    public $validAttributes;

    /**
     * @var bool
     */
    public $validateModel = true;

	/**
	 * @var
	 */
    protected $_loadModel;

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $value;

	/**
	 * @var integer
	 */
	protected $pk;

    /**
     * @throws \CHttpException
     */
    public function init()
    {
        if (!$this->model || empty($this->validAttributes)) {
            throw new CHttpException(500);
        }

		if (!Yii::app()->getRequest()->getIsAjaxRequest() || !Yii::app()->getRequest()->getIsPostRequest()) {
			throw new CHttpException(404);
		}

		$this->name = Yii::app()->getRequest()->getPost('name');
		$this->value = trim(Yii::app()->getRequest()->getPost('value'));
		$this->pk = Yii::app()->getRequest()->getPost('pk');

		if (!isset($this->name, $this->pk) || !in_array($this->name, $this->validAttributes)) {
			throw new CHttpException(404);
		}

		$model = $this->getModel($this->pk);

		if (null === $model) {
			throw new CHttpException(404);
		}
    }

    /**
     * @throws \CHttpException
     */
    public function run()
    {
    	$this->init();

       	$model = $this->getModel($this->pk);

        $model->{$this->name} = $this->value;

        if ($model->save((bool)$this->validateModel)) {
            Yii::app()->ajax->success();
        } else {
			Yii::app()->ajax->rawText($model->getError($this->name), 500);
		}
    }

	/**
	 * @param $id
	 * @return array|CActiveRecord|mixed|null
	 */
    protected function getModel($id){
    	if($this->_loadModel){
    		return $this->_loadModel;
		}

		$this->_loadModel = CActiveRecord::model($this->model)->resetScope()->findByPk($id);
    	return $this->_loadModel;
	}
}
