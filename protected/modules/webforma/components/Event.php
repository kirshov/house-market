<?php
namespace webforma\components;

use Symfony\Component\EventDispatcher\Event as MainEvent;

/**
 * Class Event
 * @package webforma\components
 */
class Event extends MainEvent
{
    /**
     * @var
     */
    public $owner;

    /**
     * Event constructor.
     * @param $owner
     */
    public function __construct($owner = null)
    {
        $this->owner = $owner;
    }
}
