<?php
/**
 * Валидатор url, корректо воспринимает кирилические адреса
 */

namespace webforma\components\validators;

use CUrlValidator;

/**
 * Class YUrlValidator
 * @package webforma\components\validators
 */
class YUrlValidator extends CUrlValidator
{
    /**
     * @var string
     */
    public $pattern = '/^{schemes}:\/\/(([A-ZА-Я0-9][A-ZА-Я0-9_-]*)(\.[A-ZА-Я0-9][A-ZА-Я0-9_-]*)+)/iu';
    /**
     * @var string
     */
    public $clientPattern = '/^{schemes}:\/\/(([A-ZА-Я0-9][A-ZА-Я0-9_-]*)(\.[A-ZА-Я0-9][A-ZА-Я0-9_-]*)+)/i';

    /**
     * @param \CModel $object
     * @param string $attribute
     */
    public function clientValidateAttribute($object, $attribute)
    {
        $this->pattern = $this->clientPattern;
        parent::clientValidateAttribute($object, $attribute);
    }
}
