<?php
/**
 * Валидатор заполненности поля
 */

namespace webforma\components\validators;

use CValidator;
use Yii;

/**
 * Class YRequiredValidator
 * @package webforma\components\validators
 */
class YRequiredValidator extends CValidator
{
    /**
     * @var
     */
    public $requiredValue;
    /**
     * @var bool
     */
    public $strict = false;
    /**
     * @var bool
     */
    public $allowEmpty = false;

    /**
     * @param \CModel $object
     * @param string $attribute
     */
    protected function validateAttribute($object, $attribute)
    {
        $value = $object->$attribute;

        if ($this->allowEmpty && $this->isEmpty($value)) {
            return;
        }

        if ($this->requiredValue !== null) {
            if (!$this->strict && $value != $this->requiredValue || $this->strict && $value !== $this->requiredValue) {
                $message = ($this->message !== null)
                    ? $this->message
                    : Yii::t(
                        'WebformaModule.webforma',
                        '{attribute} must be {value}',
                        ['{value}' => $this->requiredValue]
                    );

                $this->addError($object, $attribute, $message);
            }
        } elseif ($this->isEmpty($value, true)) {
            $message = ($this->message !== null)
                ? $this->message
                : Yii::t('WebformaModule.webforma', '{attribute} cannot be blank');

            $this->addError($object, $attribute, $message);
        }
    }
}
