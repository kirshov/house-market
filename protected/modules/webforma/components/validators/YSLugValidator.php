<?php
/**
 * Валидатор поля типа slug или alias
 */
namespace webforma\components\validators;

use CValidator;
use Yii;

/**
 * Class YSLugValidator
 * @package webforma\components\validators
 */
class YSLugValidator extends CValidator
{
	static $pattern = '/[^a-zA-Z0-9_\-]/';
    /**
     * @param \CModel $object
     * @param string $attribute
     */
    public function validateAttribute($object, $attribute)
    {
        $value = $object->$attribute;

        if (preg_match(self::$pattern, $value)) {
            $message = ($this->message !== null)
                ? $this->message
                : Yii::t('WebformaModule.webforma', '{attribute} have illegal characters');
            $this->addError($object, $attribute, $message);
        }
    }
}
