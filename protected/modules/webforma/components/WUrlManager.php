<?php

namespace webforma\components;

use CUrlManager;
use Yii;
use WebformaModule;

/**
 * Class WUrlManager
 * @package webforma\components
 */
class WUrlManager extends CUrlManager
{
    /**
     * @var WebformaModule
     */
    protected $webforma;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->webforma = Yii::app()->getModule('webforma');
        parent::init();
    }

    /**
     * @param string $route
     * @param array $params
     * @param string $ampersand
     * @return mixed|string
     */
    public function createUrl($route, $params = [], $ampersand = '&')
    {
        $url = str_replace('%2F', '/', parent::createUrl($route, $params, $ampersand));
        if(Yii::app()->getUrlManager()->urlSuffix){
            list($u1, $u2) = explode('?', $url);
            $u1 = rtrim($u1, Yii::app()->getUrlManager()->urlSuffix).Yii::app()->getUrlManager()->urlSuffix;
            $url = $u1.($u2 ? '?'.$u2 : $u2);
        }
        if(stripos($url, '?') !== false){
        	$url = rtrim($url, '/');
		}
        return $url;
    }
}
