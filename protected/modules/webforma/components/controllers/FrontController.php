<?php
/**
 * Базовый класс для всех контроллеров публичной части
 **/

namespace webforma\components\controllers;

use webforma\components\WCookie;
use Yii;
use webforma\events\WebformaControllerInitEvent;
use webforma\events\WebformaEvents;
use application\components\Controller;

/**
 * Class FrontController
 * @package webforma\components\controllers
 */
abstract class FrontController extends Controller
{
	/**
	 * @var string
	 */
    public $mainAssets;

	/**
	 * @var string
	 */
    public $systemAssets;

	/**
	 * @var bool
	 */
    public $mainpage;

	private $_contentTitle;
	private $_openGraphType;
    private $_metaImage;
    private $_metaSrcImage;

    /**
     * Вызывается при инициализации FrontController
     * Присваивает значения, необходимым переменным
     */
    public function init()
    {
    	if(Yii::app()->hasModule('redirect')){
		    Yii::app()->getComponent('redirectManager')->run();
	    }

		if(Yii::app()->hasModule('utm')){
			Yii::app()->getComponent('utm')->init();
		}

        //Yii::app()->eventManager->fire(WebformaEvents::BEFORE_FRONT_CONTROLLER_INIT, new WebformaControllerInitEvent($this, Yii::app()->getUser()));

        parent::init();


	    /**
	     * theme
	     */
        Yii::app()->theme = $this->webforma->theme ?: 'default';

        $this->mainAssets = Yii::app()->getTheme()->getAssetsUrl();
	    $this->systemAssets = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.webforma.views.frontend.assets'));

        $bootstrap = Yii::app()->getTheme()->getBasePath() . DIRECTORY_SEPARATOR . "bootstrap.php";

        if (is_file($bootstrap)) {
            require $bootstrap;
        }

	    /**
	     * ajax запросы
	     */
        if(!WCookie::hasCookie('referrer')){
        	WCookie::setCookie('referrer', Yii::app()->getRequest()->getUrlReferrer());
        }

        if(Yii::app()->hasModule('store')){
        	$module = Yii::app()->getModule('store');
	        if(Yii::app()->getRequest()->getParam('show-by')) {
		        $showBy = $module->getPrepareShowBy();
		        if ($showBy) {
		        	WCookie::setCookie('store-per-page', $showBy);
		        }
	        }

	        if(Yii::app()->getRequest()->getParam('view-type')) {
		        $viewType = $module->getViewType();
		        if ($viewType) {
					WCookie::setCookie('store-view-type', $viewType);
		        }
		        if(Yii::app()->getRequest()->getIsAjaxRequest()){
		        	Yii::app()->end();
		        }
	        }
        }


		/**
	     * регионы
	     */
	    if(Yii::app()->hasModule('regions')) {
		    Yii::app()->getComponent('region');
	    }
    }
    
    /**
	 *
	 */
    public function registerThemeColor(){
	    $webforma = Yii::app()->getModule('webforma');
	    if($webforma->isTemplate && $webforma->colorTheme){
		    $colorFile = Yii::app()->getTheme()->getBasePath().'/web/css/themes.css';
		    if(is_file($colorFile)){
			    $this->setTemplateVar('bodyClass', 'theme-'.$webforma->colorTheme, true, ' ');
			    Yii::app()->getClientScript()->registerCssFile($this->mainAssets.'/css/themes.css');
            }
	    }
    }

	/**
	 * @return array|mixed
	 */
	public function actions()
	{
		$actions = parent::actions();

		if(Yii::app()->getModule('webforma')->useGeoManager){
			$actions = \CMap::mergeArray($actions, [
				'searchCity' => [
					'class' => 'application.modules.webforma.components.geoLocate.actions.SearchCityAction',
				],
				'setCity' => [
					'class' => 'application.modules.webforma.components.geoLocate.actions.SetCityAction',
				]
			]);
		}

		return $actions;
	}

	/**
	 * @param \CAction $action
	 * @return bool
	 */
	protected function beforeAction($action)
	{
		$this->mainpage = $this->getId() == 'home' && $action->getId() == 'index';
		return parent::beforeAction($action);
	}

	/**
	 * @param string $title
	 */
	public function setTitle($title){
		$this->_contentTitle = $title;
	}

	/**
	 * @return string
	 */
	public function getTitle(){
		if($this->_contentTitle === false){
			return false;
		}
		return $this->_contentTitle ?: $this->title;
	}

	/**
	 * @param string $value
	 */
	public function setOpenGraphType($value){
		$this->_openGraphType = $value;
	}

	/**
	 * @return string
	 */
	public function getOpenGraphType(){
		if($this->mainpage){
			return 'website';
		}

		if($this->_openGraphType){
			return $this->_openGraphType;
		}

		return 'article';
	}

	/**
	 * @param string $image 250x250
	 */
	public function setMetaImage($image){
		$this->_metaImage = $image;
	}

	/**
	 * @param string $image 180x120
	 */
	public function setMetaSrcImage($image){
		$this->_metaSrcImage = $image;
	}

	/**
	 * @return string
	 */
	public function getMetaTitle(){
		$title = $this->title;
		$currentPage = Yii::app()->getRequest()->getParam('page', 0);
		if(is_numeric($currentPage) && $currentPage > 1){
			$title .= ' - страница '.$currentPage;
		}
		return $title;
	}

	/**
	 * @return mixed
	 */
	public function getMetaImage(){
		if($this->_metaImage){
			return $this->_metaImage;
		}
		$uploadPath = Yii::app()->getModule('webforma')->uploadPath;
		$logo = Yii::getPathOfAlias('webroot').DIRECTORY_SEPARATOR.$uploadPath.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'meta-logo.png';

		if(is_file($logo)){
			return Yii::app()->createAbsoluteUrl('/').$uploadPath.'/images/meta-logo.png';
		}
	}

	/**
	 * @return mixed
	 */
	public function getMetaSrcImage(){
		if($this->_metaSrcImage){
			return $this->_metaSrcImage;
		}
		/*$uploadPath = Yii::app()->getModule('webforma')->uploadPath;
		$logo = Yii::getPathOfAlias('webroot').DIRECTORY_SEPARATOR.$uploadPath.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'meta-logo.png';

		if(is_file($logo)){
			return Yii::app()->createAbsoluteUrl('/').$uploadPath.'/images/meta-logo.png';
		}*/
	}

	/**
	 * @throws \CException
	 */
	public function actionError()
	{
		$error = \Yii::app()->errorHandler->error;

		if (empty($error) || !isset($error['code']) || !(isset($error['message']) || isset($error['msg']))) {
			$this->redirect(['index']);
		}

		$module = \Yii::app()->getModule('webforma');
		if($module->send404){
			$request = \Yii::app()->getRequest();

			list($url) = explode('?', $request->getUrl());
			$url = trim($url, '/');

			$excludedString = 'guest_tracker\.map\.js|forum|wp-login.php|admin|phpmyadmin|templates|fckeditor|bea_wls_deployment_internal|console';
			$excludedString .= '|subscribe|send-feedback|callme|mail|feedback|action|callmeback|getcall|index|wordpress|wp|blog';
			$excludedString .= '|forum|phpmyadmin|ads|console|pma|xmlrpc|scripts|phpMyAdmin|license|browserconfig|status|user|fck|reviews\/add';
			$excludedString .= '|elFinder|cookies.js|phpthumb.php|.css.map|apple-touch-icon-precomposed.png|\/login\/contacts\/';

			$allowCodes = [404, 500];

			if(\Yii::app()->getRequest()->getParam('no-send', null) === null
				&& !\Helper::isBot()
				&& in_array($error['code'], $allowCodes)
				&& (($error['code'] == 404 && !preg_match('/('.$excludedString.')/i', $url)) || $error['code'] != 404)
			){
				$link = $request->getHostInfo().$request->getUrl();
				$referrer = $request->getUrlReferrer();

				if(trim($link, '/') != trim($referrer, '/')){
					$linkOriginal = $link;
					$separator = mb_strpos($link, '?') === false ? '?' : '&';
					$link .= $separator . 'no-send';

					\Yii::app()->notify->sendToAdmin('! Ошибка '.$error['code'], $error['message']
						.'<br/>User agent: '.$request->getUserAgent()
						.'<br/>IP: '.$request->getUserHostAddress()
						.'<br/>Реферер: '.($referrer ? '<a href="'.$referrer.'">'.$referrer.'</a>' : ' - ')
						.'<br/>Страница: <a href="'.$link.'">'.$linkOriginal.'</a>'
					);
				}
			}
		}

		if (!\Yii::app()->getRequest()->getIsAjaxRequest()) {
			$product = $category = $producer = $line = null;
			if($error['code'] == 404 && $module->extend404 && Yii::app()->hasModule('store')){
				$url = \Yii::app()->getRequest()->getUrl();
				$exp = explode('/', trim($url, '/'));
				if(!empty($exp)){
					$criteria = new \CDbCriteria();
					$i = 0;
					foreach ($exp as $item) {
						if(!trim($item)){
							continue;
						}
						$criteria->addCondition('slug = :slug_'.$i, 'OR');
						$criteria->params[':slug_'.$i] = $item;
						$i++;
					}

					$product = \Product::model()->published()->find($criteria);
					$category = \StoreCategory::model()->publishedAll()->find($criteria);
					$producer = \Producer::model()->published()->find($criteria);
					$line = \ProducerLine::model()->published()->find($criteria);
					if($line && !$producer){
						$producer = $line->producer;
					}
				}
			}
			$this->render('error', [
				'error' => $error,
				'product' => $product,
				'category' => $category,
				'producer' => $producer,
				'line' => $line,
				'extend404' => $module->extend404,
			]);
		}
	}

	/**
	 *
	 */
	public function actionDisablePrivacyWindow(){
		WCookie::setCookie('disabled-privacy-window', true, ['expire' => strtotime('+1 year')]);
		\Yii::app()->end();
	}

	/**
	 * @return string
	 * @throws \CException
	 */
	public function getBodyClass(){
		$class = [];
		if($this->hasTemplateVar('bodyClass')){
			$class[] = $this->getTemplateVar('bodyClass');
		}

        $isMobile = $this->mobileDetect(true, false);
        $isTablet = $this->mobileDetect(false, true);
        if($isMobile){
            $class[] = 'mobile';
        }

        if($isTablet){
	        $class[] = 'tablet';
        }

        if($isMobile || $isTablet){
	        $class[] = 'gadget';
        }

		return !empty($class) ? implode(' ', $class) : '';
	}

	/**
	 * @param string $view
	 * @param null $data
	 * @param bool $return
	 * @return string
	 * @throws \CException
	 */
	public function render($view,$data=null,$return=false)
	{
		if($this->beforeRender($view))
		{
			$output=$this->renderPartial($view,$data,true);
			if(($layoutFile=$this->getLayoutFile($this->layout))!==false)
				$output=$this->renderFile($layoutFile,array('content'=>$output),true);

			$this->afterRender($view,$output);

			$output=$this->processOutput($output);

			if(Yii::app()->hasModule('regions')){
				$region = Yii::app()->getComponent('region')->getCurrentRegion();
				$defaultRegion = Yii::app()->getComponent('region')->getDefaultRegion();
				$isDiffRegions = $defaultRegion->id != $region->id;

				$dictionaryItems = [];

				if($region && $region->dictionary){
					foreach ($region->dictionary as $dKey => $dValue){
						if(!$dValue && $isDiffRegions && $defaultRegion->dictionary[$dKey]){
							$dValue = $defaultRegion->dictionary[$dKey];
						}

						$dictionaryItems['{' . $dKey . '}'] = $dValue;
					}

					if(!empty($dictionaryItems)){
						$output = strtr($output, $dictionaryItems);
					}
				}
			}

			$replaced = [
				'{Y}' => date('Y'),
			];

			if(!empty($replaced)){
				$output = strtr($output, $replaced);
			}

			if($return)
				return $output;
			else
				echo $output;
		}
	}

	/**
	 * @param $plugin
	 * @throws \CException
	 */
	public function initPlugin($plugin){
		$cs = Yii::app()->getClientScript();
		switch ($plugin){
			case 'slick':
				$cs->registerCssFile($this->systemAssets.'/css/slick.min.css');
				$cs->registerScriptFile($this->systemAssets.'/js/slick.min.js', \CClientScript::POS_HEAD);
				break;

			case 'fancybox':
				$cs->registerCssFile($this->mainAssets.'/plugins/fancybox/jquery.fancybox.css');
				$cs->registerScriptFile($this->mainAssets.'/plugins/fancybox/jquery.fancybox.pack.js', \CClientScript::POS_END);
				$cs->registerScript('fancyboxInit', '$("[data-fancybox]").fancybox({helpers:{title:  null, overlay: {locked: false}},loop: false});');
				break;

			case 'fancybox3':
				$cs->registerCssFile($this->mainAssets.'/plugins/fancybox3/jquery.fancybox.min.css');
				$cs->registerScriptFile($this->mainAssets.'/plugins/fancybox3/jquery.fancybox.min.js', \CClientScript::POS_END);
				$cs->registerScript('fancyboxInit', '$("[data-fancybox]").fancybox({slideShow:false, thumbs:false});');
				break;

			case 'jquery-touchSwipe':
				$cs->registerScriptFile($this->mainAssets.'/plugins/jquery-touchSwipe/jquery.touchSwipe.min.js', \CClientScript::POS_END);
				break;

			case 'lazyload':
				$cs->registerScriptFile($this->mainAssets.'/plugins/lazyload/lazyload.min.js', \CClientScript::POS_END);
				break;
		}
	}

	/**
	 * @param array $plugins
	 * @throws \CException
	 */
	public function initPlugins(array $plugins){
		foreach ($plugins as $plugin){
			$this->initPlugin($plugin);
		}
	}
}
