<?php
/**
 * Базовый класс для всех контроллеров панели управления
 */

namespace webforma\components\controllers;

use webforma\models\WModel;
use Yii;
use webforma\components\WebModule;
//use webforma\events\WebformaBackendControllerInitEvent;
use webforma\events\WebformaControllerInitEvent;
use webforma\events\WebformaEvents;
use webforma\widgets\WFlashMessages;
use application\components\Controller;
use CHttpException;
use CActiveRecord;
use Exception;
use CLogger;

/**
 * Class BackController
 * @package webforma\components\controllers
 */
abstract class BackController extends Controller
{
    /**
     *
     */
    const BULK_DELETE = 'delete';

    // Прятать sidebar или нет:
    /**
     * @var bool
     */
    public $hideSidebar = false;

	/**
	 * @var
	 */
    public $subTitle;

    /**
     * @return array
     */
    public function filters()
    {
        return Yii::app()->getModule('webforma')->getBackendFilters();
    }

    /**
     * По умолчанию для роли admin разрешены все действия.
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['deny']
        ];
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->webforma->getComponent('bootstrap');
        $this->layout = $this->webforma->getBackendLayoutAlias();
        $this->pageTitle = 'Панель управления сайтом';

        if ($this->webforma->hidePanelUrls == WebModule::CHOICE_NO) {
            Yii::app()->getErrorHandler()->errorAction = '/webforma/backend/error';
        }

        Yii::app()->eventManager->fire(
            WebformaEvents::BACKEND_CONTROLLER_INIT,
            new WebformaControllerInitEvent($this, Yii::app()->getUser())
        );

	    Yii::app()->theme = $this->webforma->theme;
    }

    /**
     * @param  \CAction $action
     * @return bool
     */
    protected function beforeAction($action)
    {
        /**
         * $this->module->getId() !== 'install' избавляет от ошибок на этапе установки
         * $this->id !== 'backend' || ($this->id == 'backend' && $action->id != 'modupdate') устраняем проблемы с зацикливанием
         */
        if (($this->id !== 'backend' || ($this->id == 'backend' && $action->id != 'modupdate'))
            && ($updates = Yii::app()->migrator->checkForUpdates(
                [$this->module->getId() => $this->module]
            )) !== null
            && count($updates) > 0
        ) {
            Yii::app()->getUser()->setFlash(
                WFlashMessages::WARNING_MESSAGE,
                Yii::t('WebformaModule.webforma', 'You must install all migration before start working with module.')
            );

            $this->redirect(['/webforma/backend/modupdate', 'name' => $this->module->getId()]);
        }

        return parent::beforeAction($action);
    }

    /**
     * @throws \CHttpException
     */
    public function actionMultiaction()
    {
        if (!Yii::app()->getRequest()->getIsAjaxRequest() || !Yii::app()->getRequest()->getIsPostRequest()) {
            throw new CHttpException(404);
        }

        $modelClass = Yii::app()->getRequest()->getPost('model');
        $action = Yii::app()->getRequest()->getPost('do');

        if (!isset($modelClass, $action)) {
            throw new CHttpException(404);
        }

        $items = Yii::app()->getRequest()->getPost('items');

        if (!is_array($items) || empty($items)) {
            Yii::app()->ajax->success();
        }

        $transaction = Yii::app()->getDb()->beginTransaction();

        try {
            switch ($action) {
                case self::BULK_DELETE:

                    $models = CActiveRecord::model($modelClass)->findAllByPk($items);

                    $count = 0;

                    foreach ($models as $model) {
                        $count += (int)$model->delete();
                    }

                    $transaction->commit();

                    Yii::app()->ajax->success(
                        Yii::t(
                            'WebformaModule.webforma',
                            'Removed {count} records!',
                            [
                                '{count}' => $count
                            ]
                        )
                    );
                    break;

                default:
                    throw new CHttpException(404);
                    break;
            }
        } catch (Exception $e) {
            $transaction->rollback();
            Yii::log($e->__toString(), CLogger::LEVEL_ERROR);
            Yii::app()->ajax->failure($e->getMessage());
        }
    }

    /**
     * @throws \CHttpException
     */
    public function actionSort()
    {
        $id = (int)Yii::app()->getRequest()->getQuery('id');
        $direction = Yii::app()->getRequest()->getQuery('direction');
        $modelClass = Yii::app()->getRequest()->getQuery('model');
        $sortField = Yii::app()->getRequest()->getQuery('sortField');

        if (!isset($direction, $id, $modelClass, $sortField)) {
            throw new CHttpException(404, Yii::t('WebformaModule.webforma', 'Page was not found!'));
        }

        $model = new $modelClass();
        $model_depends = new $modelClass();
        $model = $model->resetScope()->findByPk($id);
        if (!$model) {
            throw new CHttpException(404, Yii::t('WebformaModule.webforma', 'Page was not found!'));
        }

        if ($direction === 'up') {
            $model_depends = $model_depends->findByAttributes([$sortField => ($model->$sortField - 1)]);
            $model_depends->$sortField++;
            $model->$sortField--; // example menu_order column in sql
        } else {
            $model_depends = $model_depends->findByAttributes([$sortField => ($model->$sortField + 1)]);
            $model_depends->$sortField--;
            $model->$sortField++;
        }

        $model->update([$sortField]);
        $model_depends->update([$sortField]);

        if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : ['index']);
        }
    }

	/**
	 * @throws CHttpException
	 */
    public function actionRemoveFile(){
		if (!Yii::app()->getRequest()->getIsPostRequest() || !Yii::app()->getRequest()->getIsAjaxRequest()) {
			throw new CHttpException(404, Yii::t('WebformaModule.webforma', 'Page was not found!'));
		}


		/**
		 * @var WModel $model
		 * @var bool $isNew
		 * @var int $pk
		 * @var string $attribute
		 * @var string $titleAttribute
		 * @var string $altAttribute
		 */
    	extract(\CJavaScript::jsonDecode(Yii::app()->getRequest()->getPost('json')));
    	if(!$isNew){
			if(!$model || !$attribute || !$pk){
				throw new CHttpException(false, Yii::app()->ajax->failure('Ошибка! Недостаточно данных!'));
			}

			$record = $model::model()->findByPk($pk);
			if(!$record){
				throw new CHttpException(false, Yii::app()->ajax->failure('Запись не найдена!'));
			}

			$record->removeFile();
			$record->{$attribute} = null;

			if($titleAttribute && isset($record->{$titleAttribute})){
				$record->{$titleAttribute} = null;
			}

			if($altAttribute && isset($record->{$altAttribute})){
				$record->{$altAttribute} = null;
			}

			if(!$record->save()){
				throw new CHttpException(false, Yii::app()->ajax->failure('Возникла неопределенная ошибка!'));
			}
		} else {
    		$record = new $model();
		}

		Yii::app()->ajax->success($this->widget('webforma\widgets\WInputFile', [
			'model' => $record,
			'attribute' => $attribute,
			'titleAttribute' => $titleAttribute,
			'altAttribute' => $altAttribute,
		], true));
		Yii::app()->end();
	}

	/**
	 * @param $key
	 * @param null $defaultValue
	 * @return mixed|null
	 */
	public function getTemplateVar($key, $defaultValue = null){
		return null;
	}
}
