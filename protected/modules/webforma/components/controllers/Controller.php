<?php
/**
 * Общий контроллер для панели управления и фронтенда
 **/

namespace webforma\components\controllers;

use webforma\components\ContentType;
use CHtml;
use Yii;
use CException;
use CHttpException;

/**
 * Class Controller
 * @package webforma\components\controllers
 *
 */
abstract class Controller extends \CController
{
    /**
     * @var \WebformaModule
     */
    public $webforma;

    /**
     * @var
     */
    public $layout;

    /**
     * Хлебные крошки сайта, меняется в админке
     */
    public $breadcrumbs = [];

    /**
     * Contains data for "CMenu" widget (provides view for menu on the site)
     */
    public $menu = [];

    /**
     * Тип заголовка, подробнее в webforma\components\ContentType
     * @var integer
     */
    public $headerTypeId = ContentType::TYPE_HTML;

	/**
	 * @var
	 */
    public $title;

    /**
     * @var
     */
    public $description;

    /**
     * @var
     */
    public $keywords;

    /**
     * Canonical url
     *
     * @var null
     */
    public $canonical = null;

	/**
	 * @var array
	 */
	private $templateVars = [];

	/**
	 * @param $key
	 * @param $value
	 * @param bool $append
	 * @param string $delimiter
	 */
	public function setTemplateVar($key, $value = null, $append = false, $delimiter = ''){
		if(is_array($key)){
			foreach ($key as $id => $value){
				if($append){
					$oldValue = isset($this->templateVars[$id]) ? $this->templateVars[$id] : '';
					$this->templateVars[$id] = $oldValue.$delimiter.$value;
				} else {
					$this->templateVars[$id] = $value;
				}
			}
		} else {
			if($append){
				$oldValue = isset($this->templateVars[$key]) ? $this->templateVars[$key] : '';
				$this->templateVars[$key] = $oldValue.$delimiter.$value;
			} else {
				$this->templateVars[$key] = $value;
			}
		}
	}

	/**
	 * @param $key
	 * @param null $defaultValue
	 * @return mixed|null
	 */
	public function getTemplateVar($key, $defaultValue = null){
		if($this->templateVars[$key]){
			return $this->templateVars[$key];
		}
		return $defaultValue ? $defaultValue : null;
	}

	/**
	 * @param $key
	 * @return bool
	 */
	public function hasTemplateVar($key){
		return isset($this->templateVars[$key]);
	}

	/**
     * For backward capability
     *
     * @deprecated
     * @param string $value
     */
    public function setDescription($value)
    {
        $this->metaDescription = $value;
    }

    /**
     * For backward capability
     *
     * @deprecated
     * @param string $value
     */
    public function setKeywords($value)
    {
        $this->metaKeywords = $value;
    }

    /**
     * Функция инициализации контроллера:
     *
     * @return void
     **/
    public function init()
    {
        parent::init();
        $this->webforma = Yii::app()->getModule('webforma');

        if($this->webforma->timeZone){
			date_default_timezone_set($this->webforma->timeZone);
			ini_set('date.timezone', $this->webforma->timeZone);
		}

	    if(Yii::app()->getRequest()->getParam('devmode') == 'on'){
		    Yii::app()->getSession()->add('devmode', true);
	    } elseif(Yii::app()->getRequest()->getParam('devmode') == 'off'){
		    Yii::app()->getSession()->remove('devmode');
		    Yii::app()->getCache()->flush();
	    }

	    if(Yii::app()->getRequest()->getParam('editor') == 'on'){
		    Yii::app()->getSession()->add('editor', true);
	    } elseif(Yii::app()->getRequest()->getParam('editor') == 'off'){
		    Yii::app()->getSession()->remove('editor');
	    }

	    define('EDITOR', (bool) Yii::app()->getSession()->get('editor', false));
	    define('DEVMODE', (bool) Yii::app()->getSession()->get('devmode', false));
    }

    /**
     * Функция отрисовки виджета:
     *
     * @param string $className - имя класса
     * @param mixed $properties - параметры
     * @param bool $captureOutput - требуется ли "захват" вывода виджета
     *
     * @return mixed Инстанс виджета в случае, когда $captureOutput является ложным,
     *               или вывод виджета, когда $captureOutput - истина
     **/
    public function widget($className, $properties = [], $captureOutput = false)
    {
        try {

            $modulePath = explode('.', $className);

            $isModule = strpos($className, 'application.modules') !== false
                && !empty($modulePath[2])
                && !Yii::app()->hasModule($modulePath[2]);

            if (false === Yii::getPathOfAlias($className) || $isModule) {

                if ($isModule) {
                    throw new CException(
                        Yii::t(
                            'WebformaModule.webforma',
                            'Widget "{widget}" was not found! Please enable "{module}" module!',
                            [
                                '{widget}' => $className,
                                '{module}' => $modulePath[2],
                            ]
                        ), 1
                    );
                } elseif (class_exists($className) === false) {

                    throw new CException(
                        Yii::t(
                            'WebformaModule.webforma',
                            'Widget "{widget}" was not found!',
                            [
                                '{widget}' => $className,
                            ]
                        )
                    );
                }
            }

            $widget = parent::widget($className, $properties, $captureOutput);

        } catch (CException $e) {

            echo CHtml::tag(
                'p',
                [
                    'class' => 'alert alert-danger',
                ],
                $e->getCode()
                    ? $e->getMessage()
                    : Yii::t(
                    'WebformaModule.webforma',
                    'Error occurred during the render widget ({widget}): {error}',
                    [
                        '{error}' => $e->getMessage(),
                        '{widget}' => $className,
                    ]
                )
            );

            return null;

        }

        return $widget;
    }

    /**
     * Действие обработки вывода:
     *
     * @param string $output - буфер для вывода
     *
     * @return string родительский вызов processOutput
     **/
    public function processOutput($output)
    {
        ContentType::setHeader($this->headerTypeId);

        return parent::processOutput($output);
    }


    /**
     * @param null $message
     * @param int $error
     * @return mixed
     * @throws CHttpException
     */
    protected function badRequest($message = null, $error = 400)
    {
        // Если сообщение не установленно - выставляем
        // дефолтное
        $message = $message
            ?: Yii::t(
                'WebformaModule.webforma',
                'Bad request. Please don\'t use similar requests anymore!'
            );

        if (Yii::app()->getRequest()->getIsAjaxRequest() === true) {
            return Yii::app()->ajax->failure($message);
        }

        throw new CHttpException($error, $message);
    }

    /**
     * Отключение всех профайлеров и логгеров, используется, например при загрузке файлов
     *
     * @since 0.5
     * @see http://allframeworks.ru/blog/Yii/371.html
     **/
    public function disableProfilers()
    {
        if (Yii::app()->getComponent('log')) {
            foreach (Yii::app()->getComponent('log')->routes as $route) {
                if (in_array(
                    get_class($route),
                    [
                        'CFileLogRoute',
                        'CProfileLogRoute',
                        'CWebLogRoute',
                        'YiiDebugToolbarRoute',
                        'DbProfileLogRoute',
                    ]
                )
                ) {
                    $route->enabled = false;
                }
            }
        }
    }

	/**
	 * @param $errno
	 * @param $errstr
	 * @param $errfile
	 * @param $errline
	 * @return bool
	 */
	public function WErrorHandler($errno, $errstr, $errfile, $errline)
	{
		if($errno === E_USER_ERROR){
			$request = \Yii::app()->getRequest();

			$link = $request->getHostInfo().$request->getUrl();
			$referrer = $request->getUrlReferrer();

			if(trim($link, '/') != trim($referrer, '/')){
				$linkOriginal = $link;
				$separator = mb_strpos($link, '?') === false ? '?' : '&';
				$link .= $separator . 'no-send';

				\Yii::app()->notify->sendToAdmin('! Ошибка '.$errno, $errstr
					.'<br/>Фатальная ошибка в строке '.$errline.' файла '.$errfile
					.'<br/>User agent: '.$request->getUserAgent()
					.'<br/>IP: '.$request->getUserHostAddress()
					.'<br/>Реферер: '.($referrer ? '<a href="'.$referrer.'">'.$referrer.'</a>' : ' - ')
					.'<br/>Страница: <a href="'.$link.'">'.$linkOriginal.'</a>'
				);
			}
			echo '!!';
			return true;
		}

		return false;
	}

	/**
	 * @param $key
	 * @param string $module
	 * @return mixed
	 */
	public function getSettingValue($key, $module = 'webforma'){
		return Yii::app()->getModule($module)->{$key};
	}

	/**
	 * @param bool $detectMobile
	 * @param bool $detectPda
	 * @return bool
	 * @throws CException
	 */
	public function mobileDetect($detectMobile = true, $detectPda = true){
		Yii::import('vendor.mobiledetect.mobiledetectlib.Mobile_Detect');

		$md = new \Mobile_Detect();

		$isMobile = false;
		if($detectMobile && $md->isMobile()){
			$isMobile = true;
		}

		if($detectPda && $md->isTablet()){
			$isMobile = true;
		}

		return $isMobile;
	}
}
