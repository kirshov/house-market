<?php
/**
 * WebformaModule файл класса.
 * Модуль webforma - основной модуль
 *
 * Модуль webforma содержит в себе все основные компоненты, которые используются другими модулями
 * Это наше ядрышко.
 */

use webforma\components\WebModule;

/**
 * Class WebformaModule
 */
class WebformaModule extends WebModule
{
	/**
	 *
	 */
	const TARIFF_MIN = 1;

	/*
	 *
	 */
	const TARIFF_MID = 2;

	/**
	 *
	 */
	const TARIFF_MAX = 3;

	/**
	 *
	 */
	public $adminSettingsAsTab = true;

    /**
     * @var
     */
    public $enableAssets;
    /**
     * @var
     */
    public $cache;

    /**
     * @var
     */
    public $siteDescription;

    /**
     * @var
     */
    public $siteName;

	/**
	 * @var
	 */
	public $shortSiteName;

    /**
     * @var
     */
    public $emailSiteName;

	/**
	 * @var
	 */
	public $siteNameSuffix;

    /**
     * @var
     */
    public $siteKeyWords;

    /**
     * @var string
     */
    public $backendLayout = 'column2';
    /**
     * @var string
     */
    public $emptyLayout = 'empty';
    /**
     * @var
     */
    public $theme;

    /**
     * @var int
     */
    public $coreCacheTime = 5;

	/**
	 * @var int
	 */
	public $liteCacheTime = 60;

    /**
     * @var int
     */
    public $coreCacheTimeFrontend = 3600;
    /**
     * @var string
     */
    public $coreModuleId = 'webforma';

    /**
     * @var string
     */
    public $uploadPath = 'uploads';
    /**
     * @var
     */
    public $notifyEmailFrom;

	/**
	 * @var
	 */
	public $notifyEmailTo;

	/**
	 * @var
	 */
	public $notifyEmailAdmin;

    /**
     * @var int
     */
    public $adminMenuOrder = -1;

    /**
     * @var string
     */
    public $profileModel = 'User';

    /**
     * @var
     */
    public $allowedIp;
    /**
     * @var int
     */
    public $hidePanelUrls = 0;

    /**
     * @var string
     */
    public $logo = 'logo.png';

	/**
	 * @var string
	 */
	public $favicon;

    /**
     * @var string
     */
    public $defaultImage;// = '/images/nophoto.jpg';

	/**
	 * @var string
	 */
	public $allowedExtensions = 'jpg,jpeg,png,gif';

	/**
	 * @var int
	 */

	public $minSize = 0;
	/**
	 * @var
	 */
	public $maxSize;
	/**
	 *
	 */
	public $phonePattern = '/^\+7\s\(\d{3}\)\s\d{3}-\d{2}-\d{2}$/';

    /**
     * @var string
     */
	public $phoneMask = '+7 (999) 999-99-99';

    /**
     * @var array
     * @since 0.8
     *
     * Массив фильтров для контроллеров панели управления
     */
    protected $backEndFilters = [['webforma\filters\WBackAccessControl - error']];

	/**
	 * @var int
	 */
    public $send404 = 0;

	/**
	 * @var int
	 */
	public $extend404 = 0;

	/**
	 * @var int
	 */
    public $useCategory = 0;

	/**
	 * @var int
	 */
	public $useGeoManager = 0;

	/**
	 * @var int
	 */
	public $cityIdentify = 0;

	/**
	 * @var string
	 */
	public $mainCity;

	/**
	 * @var
	 */
	public $headTags;

	/**
	 * @var
	 */
	public $counters;

	/**
	 * @var bool
	 */
	public $displayPrivacyPanel = true;

	/**
	 * @var string
	 */
	public $privacyTextPanel = "Для сбора информации технического характера мы используем файлы cookie.\nВ том числе мы обрабатываем ваш IP-адрес для определения региона местоположения.";

	/**
	 * @var string
	 */
	public $privacyTextForm = 'Отправляя данную форму, Вы соглашаетесь на обработку персональных данных в соответствии с <a href="/privacy-policy/" target="_blank" rel="nofollow">политикой конфиденциальности</a>';

	/**
	 * @var string
	 */
	public $tariff = self::TARIFF_MIN;

	/**
	 * @var int
	 */
	public $watermark = 0;

	/**
	 * @var string
	 */
	public $watermarkFile;

	/**
	 * @var int
	 */
	public $watermarkResolution = 5;

	/**
	 * @var bool
	 */
	public $allowUsers = 0;
	
	/**
	 * @var bool
	 */
	public $isTemplate = false;
	/**
	 * @var
	 */
	public $colorTheme = 'yellow';

	/**
	 * @var array
	 */
	public $colorThemeItems = [
		'yellow' => 'Желтый',
		'orange' => 'Оранжевый',
		'red' => 'Красный',
		'purple' => 'Фиолетовый',
		'blue' => 'Синий',
		'green' => 'Зеленый',
	];

	/**
	 * @var array
	 */
	public $colorThemeCodes = [
		'yellow' => '#fecc53',
		'orange' => '#ff6d00',
		'red' => '#de002b',
		'purple' => '#5f58ac',
		'blue' => '#1976d2',
		'green' => '#48a216',
	];

	/**
	 * @var
	 */
	public $timeZone = 'Asia/Yekaterinburg';

	/**
	 * @var array
	 */
	public $timeZoneList = [
		'Europe/Kaliningrad' => '(UTC+02:00) Калининград',
		'Europe/Moscow' => '(UTC+03:00) Москва',
		'Europe/Samara' => '(UTC+04:00) Самара',
		'Europe/Volgograd' => '(UTC+04:00) Волгоград',
		'Asia/Yekaterinburg' => '(UTC+05:00) Екатеринбург',
		'Asia/Omsk' => '(UTC+06:00) Омск',
		'Asia/Krasnoyarsk' => '(UTC+07:00) Красноярск',
		'Asia/Novokuznetsk' => '(UTC+07:00) Новокузнецк',
		'Asia/Novosibirsk' => '(UTC+07:00) Новосибирск',
		'Asia/Irkutsk' => '(UTC+08:00) Иркутск',
		'Asia/Yakutsk' => '(UTC+09:00) Якутск',
		'Asia/Vladivostok' => '(UTC+10:00) Владивосток',
		'Asia/Magadan' => '(UTC+11:00) Магадан',
		'Asia/Sakhalin' => '(UTC+11:00) Сахалин',
		'Asia/Anadyr' => '(UTC+12:00) Анадырь',
		'Asia/Kamchatka' => '(UTC+12:00) Камчатка',
	];

	/**
	 *
	 */
	public function init()
	{
		parent::init();
		if(!IS_BACKEND){
			$this->coreCacheTime = $this->coreCacheTimeFrontend;
		}

		Yii::app()->name = $this->getShortSiteName();
	}


	/**
     * @return array
     * @since 0.8
     *
     * Вернет массив фильтров для контроллеров панели управления
     */
    public function getBackendFilters()
    {
        return $this->backEndFilters;
    }

    /**
     * @since 0.8
     * @param array $filters
     *
     * Устанавливает массив фильтров для контроллеров панели управления
     */
    public function setBackendFilters(array $filters)
    {
        $this->backEndFilters = $filters;
    }

    /**
     * @param $filter
     * @since 0.8
     *
     * Добавить новый фильтр для контроллеров панели управления
     */
    public function addBackendFilter($filter)
    {
        $this->backEndFilters[] = $filter;
    }

    /**
     * @return array
     */
    public function getAllowedIp()
    {
        if (strpos($this->allowedIp, ',')) {
            return explode(',', trim($this->allowedIp));
        }

        return [];
    }

	/**
	 * @return string|null
	 */
    public function getLogo()
    {
	    $module = Yii::app()->getModule('webforma');
	    $logo = $module->logo;
	    $path = Yii::getPathOfAlias('public.'.$module->uploadPath);
	    if($logo && is_file($path.'/'.$logo)){
		    return rtrim(Yii::app()->createAbsoluteUrl('/'), '/').'/'.$module->uploadPath.'/'.$logo;
	    }

	    return null;
    }

    /**
     * Проверка модуля на ошибки:
     *
     * @return bool/mixed - массив сообщений при ошибках или true если всё ок
     **/
    public function checkSelf()
    {
        $messages = [];
		$isDebug = defined('YII_DEBUG') && YII_DEBUG;
        if (Yii::app()->hasModule('install')) {
            $messages[WebModule::CHECK_ERROR][] = [
                'type' => WebModule::CHECK_ERROR,
                'message' => 'Отключите модуль "Установщик"!',
            ];
        }

        if (!$isDebug && Yii::app()->getDb()->enableProfiling) {
            $messages[WebModule::CHECK_NOTICE][] = [
                'type' => WebModule::CHECK_NOTICE,
	            'message' => 'Отключите профайлер запросов',
            ];
        }

        if (!$isDebug && Yii::app()->getDb()->enableParamLogging) {
            $messages[WebModule::CHECK_NOTICE][] = [
                'type' => WebModule::CHECK_NOTICE,
                'message' => 'Отключите логирование запросов',
            ];
        }

        if (!$isDebug && Yii::app()->hasModule('gii')) {
            $messages[WebModule::CHECK_ERROR][] = [
                'type' => WebModule::CHECK_ERROR,
                'message' => 'Отключите модуль "gii"!',
            ];
        }

        $uploadPath = Yii::getPathOfAlias('webroot').'/'.$this->uploadPath;

        if (!is_writable($uploadPath)) {
        	Helper::createDirectoryIfNotExist($uploadPath);

	        if (!is_writable($uploadPath)) {
		        $messages[WebModule::CHECK_ERROR][] = [
			        'type' => WebModule::CHECK_ERROR,
			        'message' => 'Директория "'.$uploadPath.'" не доступна для записи!',
		        ];
	        }
        }


        if (!is_writable(Yii::app()->getRuntimePath())) {
	        Helper::createDirectoryIfNotExist(Yii::app()->getRuntimePath());

	        if (!is_writable(Yii::app()->getRuntimePath())) {
	            $messages[WebModule::CHECK_ERROR][] = [
	                'type' => WebModule::CHECK_ERROR,
		            'message' => 'Директория "'.Yii::app()->runtimePath.'" не доступна для записи!',
	            ];
	        }
        }

        if (!is_writable(Yii::app()->getAssetManager()->basePath)) {
	        Helper::createDirectoryIfNotExist(Yii::app()->getAssetManager()->basePath);

	        if (!is_writable(Yii::app()->getAssetManager()->basePath)) {
	            $messages[WebModule::CHECK_ERROR][] = [
	                'type' => WebModule::CHECK_ERROR,
		            'message' => 'Директория "'.Yii::app()->getAssetManager()->basePath.'" не доступна для записи!',
	            ];
	        }
        }

        if (!DEVMODE && defined('YII_DEBUG') && YII_DEBUG) {
            $messages[WebModule::CHECK_ERROR][] = [
                'type' => WebModule::CHECK_ERROR,
	            'message' => 'Необходимо отключить режим отладки!',
            ];
        }

        if (!Yii::app()->getDb()->schemaCachingDuration) {
            $messages[WebModule::CHECK_ERROR][] = [
                'type' => WebModule::CHECK_ERROR,
                'message' => 'Включите кэширование схемы базы данных!',
            ];
        }

        if(!$this->notifyEmailFrom){
	        $messages[WebModule::CHECK_ERROR][] = [
		        'type' => WebModule::CHECK_ERROR,
		        'message' => 'Не указан адрес исходящей почты!',
		        'link' => '/backend/modulesettings/?module=webforma#tab-tb-c-2',
	        ];
        }

	    if(!$this->notifyEmailTo){
		    $messages[WebModule::CHECK_ERROR][] = [
			    'type' => WebModule::CHECK_ERROR,
			    'message' => 'Не указан адрес для почтовых уведомлений!',
			    'link' => '/backend/modulesettings/?module=webforma#tab-tb-c-2',
		    ];
	    }

	    if(!$this->siteName || $this->siteName == 'Имя вашего сайта'){
		    $messages[WebModule::CHECK_ERROR][] = [
			    'type' => WebModule::CHECK_ERROR,
			    'message' => 'Не заполнено навание сайта!',
			    'link' => '/backend/modulesettings/?module=webforma',
		    ];
	    }

	    $profile = Yii::app()->getUser()->getProfile();
		if($profile->hash == '$2y$13$10Cy5F3Fdw6mB9ulk60nKuk9qVKRdEX7GW2CRCejQos8W/l9dMCSq'){
			$messages[WebModule::CHECK_ERROR][] = [
				'type' => WebModule::CHECK_ERROR,
				'message' => 'Ваш пароль слишком простой, нажмите сюда чтобы его сменить.',
				'link' => Yii::app()->createUrl('/user/userBackend/changepassword/', ['id' => $profile->id]),
			];
		}

        return !empty($messages) ? $messages : true;
    }

    /**
     * Возвращаем названия параметров:
     *
     * @return mixed
     **/
    public function getParamsLabels()
    {
        return [
            'siteDescription' => Yii::t('WebformaModule.webforma', 'Site description'),
            'siteName' => Yii::t('WebformaModule.webforma', 'Site title'),
	        'shortSiteName' => 'Краткое название сайта',
	        'emailSiteName' => 'Название компании в заголовке e-mail "от кого"',
            'siteKeyWords' => Yii::t('WebformaModule.webforma', 'Site keywords'),
            'backendLayout' => Yii::t('WebformaModule.webforma', 'Layout of backend'),
            'theme' => Yii::t('WebformaModule.webforma', 'Frontend theme'),
            'coreCacheTimeFrontend' => Yii::t('WebformaModule.webforma', 'Chacing time (sec.)'),
            'uploadPath' => Yii::t('WebformaModule.webforma', 'File uploads catalog (relative to the site root)'),
            'editor' => Yii::t('WebformaModule.webforma', 'Visual editor'),
            'notifyEmailFrom' => 'Email отправителя',
            'notifyEmailTo' => 'Email для уведомлений',
            'notifyEmailAdmin' => 'Email администратора',
            'allowedIp' => Yii::t('WebformaModule.webforma', 'Allowed IP'),
            'hidePanelUrls' => Yii::t('WebformaModule.webforma', 'Hide panel urls'),
            'logo' => Yii::t('WebformaModule.webforma', 'Logo'),
            'allowedExtensions' => Yii::t('WebformaModule.webforma', 'Allowed extensions (separated by comma)'),
            'mimeTypes' => Yii::t('WebformaModule.webforma', 'Mime types'),
            'maxSize' => Yii::t('WebformaModule.webforma', 'Maximum size (in bytes)'),
			'send404' => 'Уведомлять о 404 ошибке на e-mail',
			'extend404' => 'Расширенная страница 404',
			'useCategory' => 'Использовать категории',
			'useGeoManager' => 'Использовать определение города',
			'cityIdentify' => 'Определять город автоматически',
			'mainCity' => 'Основной город',
	        'headTags' => 'Теги в блоке HEAD',
	        'counters' => 'Счетчики и другие скрипты',
	        'displayPrivacyPanel' => 'Показывать панель о сборе технических данных',
	        'privacyTextPanel' => 'Текст сообщения о сборе технических данных',
	        'privacyTextForm' => 'Текст сообщения о принятии политики конфиденциальности при отправке форм',
	        'favicon' => 'Иконка (favicon)',
	        'tariff' => 'Тариф',
	        'siteNameSuffix' => 'Суффикс для внутренних страниц',
	        'watermark' => 'Расположение водяного знака',
			'watermarkResolution' => 'Размер водяного знака',
			'watermarkFile' => 'Водяной знак',
	        'colorTheme' => 'Цветовая схема',
			'allowUsers' => 'Разрешить личный кабинет пользователей',
			'isTemplate' => 'Это шаблон',
			'timeZone' => 'Часовой пояс',
        ];
    }

    /**
     * Возвращаем редактируемые параметры:
     *
     * @return mixed
     **/
    public function getEditableParams()
    {
        $params = [
            'siteName',
	        'shortSiteName' => [
	        	'input-type' => 'text',
		        'hint' => 'Будет использовано в почтовых уведомлениях и прочих местах, где более предпочтительно использовать краткое название сайта',
	        ],
	        'siteNameSuffix' => [
	        	'input-type' => 'text',
		        'hint' => 'Используется только если не заполнен тег Title<br/>
					Например, для страницы "Доставка" тег Title примет вид: <i>Доставка | суффикс</i> 
				',
	        ],
	        'emailSiteName' => [
		        'input-type' => 'text',
		        'htmlOptions' => [
	                'placeholder' => Yii::app()->getModule('webforma')->siteName,
		        ],
		        'hint' => 'Используется в поле "От кого"',
	        ],
            'siteDescription',
            'siteKeyWords',
            'displayPrivacyPanel' => [
	            'input-type' => 'checkbox',
            ],
			'privacyTextPanel' => [
	            'input-type' => 'textarea',
            ],
			'privacyTextForm' => [
	            'input-type' => 'textarea',
            ],

            'notifyEmailFrom',
            'notifyEmailTo',
            'notifyEmailAdmin',
            'allowedIp',
            'logo' => [
				'input-type' => 'image',
				'path' => $this->uploadPath,
			],
	        'favicon' => [
		        'input-type' => 'image',
		        //'path' => Yii::getPathOfAlias('public'),
		        'default-name' => 'favicon.ico',
	        ],
	        'headTags' => [
		        'input-type' => 'textarea',
	        ],
	        'counters' => [
		        'input-type' => 'textarea',
	        ],

			'watermarkResolution' => [
				'input-type' => 'select',
				'empty' => [],
				'items' => $this->getWatermarkResolution(),
				'hint' => 'Относительно основного изображения',
			],

			'watermark' => [
				1 => 'В левом верхнем углу',
				2 => 'В правом верхнем углу',
				3 => 'В левом нижнем углу',
				4 => 'В правом нижнем углу',
				5 => 'По центру',
				6 => 'Замостить',
			],

			'watermarkFile' => [
				'input-type' => 'image',
				'path' => $this->uploadPath.'/'.$this->getPathWatermark(),
			],
	        'colorTheme' => [
		        'input-type' => 'widget',
		        'widget' => 'application.modules.webforma.widgets.ColorSelect',
		        'needFormWrap' => true,
		        'options' => [
			        'items' => $this->colorThemeCodes,
			        'labels' => $this->colorThemeItems,
		        ],
	        ],
			'timeZone' => $this->timeZoneList,
        ];

        if(DEVMODE){
			$params = CMap::mergeArray($params, [
				'coreCacheTimeFrontend',
				'hidePanelUrls' => $this->getChoice(),
				'theme' => $this->getThemes(),
				'extend404' => [
					'input-type' => 'checkbox',
				],
				'send404' => [
					'input-type' => 'checkbox',
				],
				'useCategory' => [
					'input-type' => 'checkbox',
				],
				'useGeoManager' => [
					'input-type' => 'checkbox',
				],
				'tariff' => $this->getTariffs(),
				'allowUsers' => [
					'input-type' => 'checkbox',
				],
				'isTemplate' => [
					'input-type' => 'checkbox',
				],
			]);
		}
		if($this->useGeoManager){
			$params = CMap::mergeArray($params, [
				'cityIdentify' => [
					'input-type' => 'checkbox',
				],
				'mainCity',
			]);
		}

		return $params;
    }

	/**
	 * @return array
	 */
	public function getEditableParamsTabs(){
		$tabs = [
			'0.site' => [
				'label' => 'Общие настройки',
				'items' => [
					'site',
					'privacy',
					'colorScheme',
					'system',
				],
			],
			'0.seo' => [
				'label' => 'Настройки SEO',
				'items' => [
					'seo',
				],
			],
			'1.notify' => [
				'label' => 'Уведомления',
				'items' => [
					'notify',
				],
			],
			'2.tags' => [
				'label' => 'Теги/счетчики',
				'items' => [
					'tags',
				],
			],
			'watermark' => [
				'label' => 'Водяной знак',
				'items' => [
					'watermark',
				],
			],
		];

		if($this->useGeoManager){
			$tabs['geo'] = [
				'label' => 'ГЕО настройки',
				'items' => [
					'geo',
				],
			];
		}

		return $tabs;
	}

    /**
     * массив групп параметров модуля, для группировки параметров на странице настроек
     *
     * @return array
     */
    public function getEditableParamsGroups()
    {
        $groups = [
            'site' => [
                'label' => Yii::t('WebformaModule.webforma', 'Site settings'),
                'items' => [
					'siteName',
					'shortSiteName',
                    'logo',
                    'favicon',
					'timeZone',
	                'allowedIp',
                ],
            ],
			'privacy' => [
				'label' => Yii::t('WebformaModule.webforma', 'Настройки политики конфиденциальности'),
				'items' => [
					'displayPrivacyPanel',
					'privacyTextPanel',
					'privacyTextForm',
				],
			],
			'seo' => [
                'label' => Yii::t('WebformaModule.webforma', 'Настройки SEO'),
                'items' => [
					'siteDescription',
					'siteKeyWords',
					'siteNameSuffix',
                ],
            ],
            'notify' => [
                'label' => Yii::t('WebformaModule.webforma', 'Main settings admin panel'),
                'items' => [
	                'emailSiteName',
                    'notifyEmailFrom',
                    'notifyEmailTo',
                    'notifyEmailAdmin',
                ],
            ],
			'tags' => [
				'label' => 'Теги/счетчики',
				'items' => [
					'headTags',
					'counters',
				],
			],
			'watermark' => [
				'label' => 'Настройка водяного знака',
				'items' => [
					'watermark',
					'watermarkResolution',
					'watermarkFile',
				],
			]
        ];

		if($this->useGeoManager){
			$groups = CMap::mergeArray($groups, [
				'geo' => [
					'label' => 'Настройки определения города',
					'items' => [
						'cityIdentify',
						'mainCity',
					]
				]
			]);
		}

		if(DEVMODE){
			$groups = CMap::mergeArray($groups, [
				'system' => [
					'label' => 'Системные настройки',
					'items' => [
						'tariff',
						'coreCacheTimeFrontend',
						'hidePanelUrls',
						'theme',
						'extend404',
						'send404',
						'useCategory',
						'isTemplate',
						'useGeoManager',
						'allowUsers',
					],
				],
			]);
		}
		
		if($this->isTemplate){
			$groups = CMap::mergeArray($groups, [
				'colorScheme' => [
					'label' => 'Цветовая схема',
					'items' => [
						'colorTheme',
					],
				],
			]);
		}

        return $groups;
    }

    /**
     * Возвращаем правила валидации для параметров модуля
     *
     * @return array Правила валидации для параметров модуля
     */
    public function rules()
    {
        return [
            [
                'filter',
                'filter' => function ($str) {
                    return preg_replace('/\s+/', '', $str);
                },
            ],
        ];
    }

    /**
     * Возвращаем статус, устанавливать ли галку для установки модуля в инсталяторе по умолчанию:
     *
     * @return bool
     **/
    public function getIsInstallDefault()
    {
        return true;
    }

    /**
     * Возвращаем статус, возможно ли модуль отключать:
     *
     * @return bool
     **/
    public function getIsNoDisable()
    {
        return true;
    }

     /**
     * Возвращаем линк на админ панель:
     *
     * @return mixed
     **/
    public function getAdminPageLink()
    {
        return '/webforma/backend/settings';
    }

    /**
     * Возвращаем массив меню:
     *
     * @return mixed
     **/
    public function getNavigation()
    {
        $data = [
            [
                'label' => Yii::t('WebformaModule.webforma', 'Clean cache'),
                'url' => ['/webforma/backend/ajaxflush', 'method' => 1],
                'linkOptions' => [
                    'class' => 'flushAction',
                    'method' => 'cacheAll',
                ],
                'icon' => 'fa fa-fw fa-trash-o',
                'items' => [
                    [
                        'icon' => 'fa fa-fw fa-trash-o',
                        'label' => Yii::t('WebformaModule.webforma', 'Clean cache'),
                        'url' => ['/webforma/backend/ajaxflush', 'method' => 1],
                        'linkOptions' => [
                            'class' => 'flushAction',
                            'method' => 'cacheFlush',
                        ],
                    ],
                    [
                        'icon' => 'fa fa-fw fa-trash-o',
                        'label' => Yii::t('WebformaModule.webforma', 'Clean settings cache'),
                        'url' => ['/webforma/backend/flushDumpSettings'],
                        'linkOptions' => [
                            'class' => 'flushAction',
                            'method' => 'cacheFlush',
                        ],
	                    'visible' => DEVMODE,
                    ],
                    [
                        'icon' => 'fa fa-fw fa-trash-o',
                        'label' => Yii::t('WebformaModule.webforma', 'Clean assets'),
                        'url' => ['/webforma/backend/ajaxflush', 'method' => 2],
                        'linkOptions' => [
                            'class' => 'flushAction',
                            'method' => 'assetsFlush',
                        ],
                        //'visible' => !Yii::app()->getAssetManager()->linkAssets,
                    ],
                    [
                        'icon' => 'fa fa-fw fa-trash-o',
                        'label' => Yii::t('WebformaModule.webforma', 'Clean cache and assets'),
                        'url' => ['/webforma/backend/ajaxflush', 'method' => 3],
                        'linkOptions' => [
                            'class' => 'flushAction',
                            'method' => 'cacheAssetsFlush',
                        ],
                        //'visible' => !Yii::app()->getAssetManager()->linkAssets,
                    ],
                ],
            ],
            [
                'icon' => 'fa fa-fw fa-wrench',
                'label' => Yii::t('WebformaModule.webforma', 'Site settings'),
                'url' => $this->getSettingsUrl(),
            ],
        ];

        if(DEVMODE){
	        $data = CMap::mergeArray($data, [
		        [
			        'icon' => "fa fa-fw fa-th",
			        'label' => Yii::t('WebformaModule.webforma', 'My modules'),
			        'url' => ['/webforma/backend/settings'],
		        ],
		        [
			        'icon' => "fa fa-fw fa-sticky-note",
			        'label' => 'Редактор шаблонов',
			        'url' => ['/webforma/templateBackend/index'],
		        ],
		        [
			        'icon' => 'fa fa-fw fa-times-rectangle',
			        'label' => 'Выйти из devmode режима',
			        'url' => Yii::app()->createUrl('/backend/').'?devmode=off',
		        ],
            ]);
        }

        return $data;
    }

    /**
     * Возвращаем название категории модуля:
     *
     * @return string
     **/
    public function getCategory()
    {
        return Yii::t('WebformaModule.webforma', 'FORMA.cms');
    }

    /**
     * Возвращаем название модуля:
     *
     * @return string
     **/
    public function getName()
    {
        return Yii::t('WebformaModule.webforma', 'FORMA.cms');
    }

    /**
     * Возвращаем описание модуля:
     *
     * @return string
     **/
    public function getDescription()
    {
        return Yii::t('WebformaModule.webforma', 'Webforma core!');
    }

    /**
     * Возвращаем иконка модуля:
     *
     * @return string
     **/
    public function getIcon()
    {
        return "fa fa-fw fa-cog";
    }

    /**
     * Получает полный алиас нужного лайаута бэкенда с учетом темы
     *
     * @param string $layoutName Название лайаута, если не задан - берется по-умолчанию для бекенда
     *
     * @since 0.4
     * @return string Полный путь к лайауту
     */
    public function getBackendLayoutAlias($layoutName = '')
    {
		return 'application.modules.webforma.views.layouts.'.($layoutName ? $layoutName : $this->backendLayout);
    }

    /**
     * Метод возвращает доступные темы оформления
     *
     * Для добавления новой темы необходимо:
     * Прочитать http://yiiframework.ru/doc/guide/ru/topics.theming
     * Скопировать тему в каталог  WebRoot/themes или аналогичный (настройки themeManager)
     * Название каталога с темой для панели управления должно начинаться с префикса "backend_", например "backend_bootstrap"
     *
     * @param bool $backend - если установлен в true - вернет темы оформления для панели управления, иначе - для публичной части сайта
     *
     * @return array список доступных тем
     * @since 0.4
     * @todo возможно, стоит добавить кэширование чтобы каждый раз не ходить по файловой системе
     */
    public function getThemes($backend = false)
    {
        $themes = [];

        if (isset(Yii::app()->themeManager->basePath) && $handler = opendir(Yii::app()->themeManager->basePath)) {
            while (($file = readdir($handler))) {
                if ($file != '.' && $file != '..' && !is_file($file)) {
                    if ("backend_" == substr($file, 0, 8)) {
                        if ($backend) {
                            $file = str_replace("backend_", "", $file);
                            $themes[$file] = $file;
                        }
                    } else {
                        if (!$backend) {
                            $themes[$file] = $file;
                        }
                    }
                }
            }
            closedir($handler);
        }

        return $themes;
    }

    /**
     * Метод возвращает пункты, содержащие сабменю для заголовок групп
     *
     * @param array $menu - список пунктов
     *
     * @since 0.5
     * @return array преобразованный список пунктов
     */
    public function getSubMenu($menu)
    {
        $items = [];
        $endItemKey = count($menu) ? array_reverse(array_keys($menu))[0] : '';
        foreach ($menu as $key => $item) {
            if ($key === '') {
                continue;
            }
	        if($item['items']){
		        foreach ($item['items'] as $subItem){
			        if($subItem['active']){
				        $menu[$key]['active'] = true;
				        break 1;
			        }
		        }
	        }

            if (!isset($item['not-prepare']) && isset($item['items']) && is_array($item['items']) && !empty($item['items'])) {
                $subItems = $item['items'];
                unset($item['items'], $item['icon'], $item['url']);
                $items[] = $item;
                $items = array_merge($items, $subItems);
                if ($key != $endItemKey) {
                    $items[] = "---";
                }
            } else {
                $items[] = $item;
            }
        }

        foreach ($items as $item => $data) {
            if (empty($data['items']) && !isset($data['url'])) {
                unset($items[$item]);
            }
        }

        return $items;
    }

    protected function getMainSubMenu(){

    }

    /**
     * Выдает путь к стилям, определяет вкелючена тема или нет
     *
     * @since 0.5
     * @return string путь к директории
     */
    public function getThemeBaseUrl()
    {
        return (Yii::app()->theme) ? Yii::app()->theme->baseUrl : Yii::app()->baseUrl;
    }

    /**
     * Генерация анкора PoweredBy
     *
     * @param string $color - цвет
     * @param string $text - текст
     *
     * @return string poweredBy
	 * !!!!!!!!!!!!
     */
    public function poweredBy($color = 'yellow', $text = '')
    {
    	return '';
    }

    /**
     * Получаем массив с именами модулей, от которых зависит работа данного модуля
     *
     * @return array Массив с именами модулей, от которых зависит работа данного модуля
     *
     * @since 0.5
     */
    public function getDependencies()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getLayoutsList()
    {
        $data = [];

        foreach (new GlobIterator(Yii::app()->getTheme()->basePath.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'layouts'.DIRECTORY_SEPARATOR.'*.php') as $item) {
            $name = $item->getBaseName('.php');
            $data[$name] = $name;
        }

        return $data;
    }

	/**
	 * @return mixed
	 */
    public function getShortSiteName(){
    	return $this->shortSiteName ?: $this->siteName;
    }

	/**
	 * @return mixed
	 */
	public function getEmailSiteName(){
		return $this->emailSiteName ?: $this->siteName;
	}

	/**
	 * @return array
	 */
	public function getTariffs(){
		return [
			self::TARIFF_MIN => 'RocketStore',
			self::TARIFF_MID => 'RocketStore+',
		];
	}

	/**
	 * @return string
	 */
	public function getPathWatermark(){
		return 'watermark';
	}

	/**
	 * @return string
	 */
	public function getWatermark(){
		if(!$this->watermarkFile){
			return false;
		}

		return Yii::app()->uploadManager->getBasePath().'/'.$this->getPathWatermark().'/'.$this->watermarkFile;
	}

	/**
	 * @return array
	 */
	protected function getWatermarkResolution(){
		$data = [
			0 => 'Как есть',
		];
		for($i = 1; $i <= 10; $i++){
			$data[$i] = '1:'.$i;
		}
		return $data;
	}

	/**
	 * @return string
	 */
	public function getPrivacyTextPanel(){
		return str_replace("\n", '<br/>', $this->privacyTextPanel);
	}

	/**
	 * @return string
	 */
	public function getPrivacyTextForm(){
		return str_replace("\n", '<br/>', $this->privacyTextForm);
	}
}
