<?php
use webforma\components\Event;
use webforma\components\WCookie;

/**
 * Class WebformaTemplateListener
 */

class WebformaTemplateListener
{
	public static function footer()
	{
		$webforma = Yii::app()->getModule('webforma');
		$controller = Yii::app()->getController();

		echo '<!--noindex-->';

		//Заказать звонок
		if(Yii::app()->hasModule('callback')){
			$controller->widget('callback');
		}

		//Всплывающая форма обратной связи
		if(Yii::app()->hasModule('feedback')){
			$controller->widget('feedback', ['view' => 'popup']);
		}

		//Регионы
		if(Yii::app()->hasModule('regions')){
			$controller->widget('regionsCity');
		}

		//панель о сборе технически данных
		if($webforma->displayPrivacyPanel && !(WCookie::getCookie('disabled-privacy-window') == true)){
			$controller->renderPartial('//service/privacy-panel');
		}

		//Карта из корзины
		if ($controller->hasTemplateVar('pickup-map')) {
			echo '<div class="modal-window delivery-pickup__map"><div class="modal-close">×</div>'
				.$controller->getTemplateVar('pickup-map')
				.'</div>';

		}

		//Панель выбора цвета
		if (Yii::app()->getModule('webforma')->isTemplate) {
			$controller->renderPartial('//service/color-select/default');
		}
		echo '<!--noindex-->';

		//счетчики
		echo $controller->getSettingValue('counters');
	}
}