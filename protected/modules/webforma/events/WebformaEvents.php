<?php
namespace webforma\events;

class WebformaEvents
{
    const BEFORE_BACKEND_CONTROLLER_ACTION = 'webforma.before.backend.controller.action';

    const BACKEND_CONTROLLER_INIT = 'webforma.backend.controller.init';

    const BEFORE_FRONT_CONTROLLER_INIT = 'webforma.before.front.controller.init';
}
