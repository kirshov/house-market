<?php

class Helper
{
	/**
	 * @param $text
	 * @param int $arrow
	 * @return string
	 */
	public static function switcher($text, $arrow = 2)
	{
		$str[0] = array('й' => 'q', 'ц' => 'w', 'у' => 'e', 'к' => 'r', 'е' => 't', 'н' => 'y', 'г' => 'u', 'ш' => 'i', 'щ' => 'o', 'з' => 'p', 'х' => '[', 'ъ' => ']', 'ф' => 'a', 'ы' => 's', 'в' => 'd', 'а' => 'f', 'п' => 'g', 'р' => 'h', 'о' => 'j', 'л' => 'k', 'д' => 'l', 'ж' => ';', 'э' => '\'', 'я' => 'z', 'ч' => 'x', 'с' => 'c', 'м' => 'v', 'и' => 'b', 'т' => 'n', 'ь' => 'm', 'б' => ',', 'ю' => '.', 'Й' => 'Q', 'Ц' => 'W', 'У' => 'E', 'К' => 'R', 'Е' => 'T', 'Н' => 'Y', 'Г' => 'U', 'Ш' => 'I', 'Щ' => 'O', 'З' => 'P', 'Х' => '[', 'Ъ' => ']', 'Ф' => 'A', 'Ы' => 'S', 'В' => 'D', 'А' => 'F', 'П' => 'G', 'Р' => 'H', 'О' => 'J', 'Л' => 'K', 'Д' => 'L', 'Ж' => ';', 'Э' => '\'', '?' => 'Z', 'ч' => 'X', 'С' => 'C', 'М' => 'V', 'И' => 'B', 'Т' => 'N', 'Ь' => 'M', 'Б' => ',', 'Ю' => '.',);
		$str[1] = array('q' => 'й', 'w' => 'ц', 'e' => 'у', 'r' => 'к', 't' => 'е', 'y' => 'н', 'u' => 'г', 'i' => 'ш', 'o' => 'щ', 'p' => 'з', '[' => 'х', ']' => 'ъ', 'a' => 'ф', 's' => 'ы', 'd' => 'в', 'f' => 'а', 'g' => 'п', 'h' => 'р', 'j' => 'о', 'k' => 'л', 'l' => 'д', ';' => 'ж', '\'' => 'э', 'z' => 'я', 'x' => 'ч', 'c' => 'с', 'v' => 'м', 'b' => 'и', 'n' => 'т', 'm' => 'ь', ',' => 'б', '.' => 'ю', 'Q' => 'Й', 'W' => 'Ц', 'E' => 'У', 'R' => 'К', 'T' => 'Е', 'Y' => 'Н', 'U' => 'Г', 'I' => 'Ш', 'O' => 'Щ', 'P' => 'З', '[' => 'Х', ']' => 'Ъ', 'A' => 'Ф', 'S' => 'Ы', 'D' => 'В', 'F' => 'А', 'G' => 'П', 'H' => 'Р', 'J' => 'О', 'K' => 'Л', 'L' => 'Д', ';' => 'Ж', '\'' => 'Э', 'Z' => '?', 'X' => 'ч', 'C' => 'С', 'V' => 'М', 'B' => 'И', 'N' => 'Т', 'M' => 'Ь', ',' => 'Б', '.' => 'Ю',);

		return strtr($text, isset($str[$arrow]) ? $str[$arrow] : array_merge($str[0], $str[1]));
	}

	/**
	 * @param null $var
	 */
	public static function var_dump($var = null){
		echo '<pre style="font-size: 12px; text-align: left;">';
		var_dump($var);
		echo '</pre>';
	}

	/**
	 * возвращает список всех дочерних элементов
	 * @param $parents
	 * @param $id
	 * @return array
	 */
	public static function getChildList($parents, $id){
		if(empty($parents[$id])){
			return array();
		}

		$ids = array();
		foreach($parents[$id] as $pId){
			$ids[] = $pId;
			$p = self::getChildList($parents, $pId);
			$ids = array_merge($ids, $p);
		}
		return $ids;
	}

	/**
	 * @param integer $number
	 * @param array $titles
	 * @return string
	 */
	public static function pluralForm($number, $titles = [])
	{
		$cases = array(2, 0, 1, 1, 1, 2);
		return $titles[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
	}

	/**
	 * @param $string
	 * @return string
	 */
	public static function mb_ucfirst($string) {
		$fc = mb_strtoupper(mb_substr($string, 0, 1));
		return $fc.mb_substr($string, 1);
	}

	/**
	 * @param $fields
	 * @param array $exclude
	 * @param string $namePrefix
	 * @return string
	 */
	public static function getInputFieldsFromArray($fields, $exclude = [], $namePrefix = ''){
		$data = '';
		foreach ($fields as $key => $param){
			if(in_array($key, $exclude, true)) {
				continue;
			}

			if($namePrefix){
				$key = $namePrefix.'['.$key.']';
			}
			if(is_array($param)){
				$data .= self::getInputFieldsFromArray($param, $exclude, $key);
			} else {
				$data .= CHtml::hiddenField($key, $param, ['id' => CHtml::getIdByName($key.'_hidden')]);
			}
		}
		return $data;
	}

	/**
	 * @param mixed $title
	 * @param bool $addSiteName
	 * @param string $separator
	 * @return string
	 */
	public static function prepareSeoTitle($title, $separator = ' | '){
		if(is_array($title)){
			foreach ($title as $key => $item){
				if(!trim($item)){
					unset($title[$key]);
				}
			}
			$title = implode($separator, $title);
		}

		$webforma = Yii::app()->getModule('webforma');
		if($webforma->siteNameSuffix){
			$title .= $separator.$webforma->siteNameSuffix;
		}
		return $title;
	}

    /**
     * @param string $str
     * @param array $replaceArray
     * @param bool $replaceDoubleSpace
     * @return string
     */
    public static function mb_strtr($str, $replaceArray, $replaceDoubleSpace = true) {
        foreach($replaceArray as $key => $value) {
            $str = str_replace($key, $value, $str);
        }

        if($replaceDoubleSpace){
            $str = preg_replace('/\s{2,}/iu', ' ', $str);
            $str = preg_replace('/\-{2,}/iu', '', $str);
            $str = preg_replace('/\(\s*\)/iu', '', $str);
            $str = preg_replace('/-\s*-/iu', ' - ', $str);
            $exp = explode(' - ', $str);
            foreach ($exp as $key => $item) {
                if(!trim($item) || $item == '-'){
                    unset($exp[$key]);
                }
            }
            $str = implode(' - ', (array) $exp);

            $str = trim($str, ' -');
        }
        return $str;
    }

	/**
	 * @return bool
	 */
    public static function isBot(){
        $bots = array(
            'rambler','googlebot','aport','yahoo','msnbot','turtle','mail.ru','omsktele',
            'yetibot','picsearch','sape.bot','sape_context','gigabot','snapbot','alexa.com',
            'megadownload.net','askpeter.info','igde.ru','ask.com','qwartabot','yanga.co.uk',
            'scoutjet','similarpages','oozbot','shrinktheweb.com','aboutusbot','followsite.com',
            'dataparksearch','google-sitemaps','appEngine-google','feedfetcher-google',
            'liveinternet.ru','xml-sitemaps.com','agama','metadatalabs.com','h1.hrn.ru',
            'googlealert.com','seo-rus.com','yaDirectBot','yandeG','yandex', 'AhrefsBot', 'DotBot',
            'yandexSomething','Copyscape.com','AdsBot-Google','domaintools.com', 'MegaIndex.ru', 'SafeDNSBot',
            'Nigma.ru','bing.com','dotnetdotcom','openstat.ru/Bot','mj12bot.com', 'statdom.ru/Bot', 'MauiBot',
	        'Runet-Research-Crawler', 'SemrushBot', 'Dataprovider.com', 'Google-Site-Verification', 'CCBot/2.0',
	        'VelenPublicWebCrawler',
        );

        foreach($bots as $bot){
            if(stripos(Yii::app()->getRequest()->getUserAgent(), $bot) !== false){
                return true;
            }
        }
        return false;
    }

	/**
	 * @param $date
	 * @param string $format
	 * @return bool
	 */
	public static function isDate($date, $format = 'Y-m-d H:i:s'){
		return date($format, strtotime($date)) == $date;
	}

	/**
	 * @param $key
	 * @param $value
	 * @return bool
	 */
	public static function setFlash($key, $value){
		if(!$key){
			return false;
		}

		if($value === null){
			unset($_SESSION['key']);
		}
		$_SESSION[$key] = $value;
	}

	/**
	 * @param $key
	 * @return bool
	 */
	public static function hasFlash($key){
		return isset($_SESSION[$key]);
	}

	/**
	 * @param $key
	 * @param null $default
	 * @return null
	 */
	public function getFlash($key, $default = null){
		if(self::hasFlash($key)){
			$value = $_SESSION[$key];
			self::setFlash($key, null);
			return $value;
		}

		return $default;
	}

	/**
	 * @param $price
	 * @param $decimalsCount
	 * @return string
	 */
	public static function getFormatPrice($price, $decimalsCount = null){
		$decimals = 0;
		if(Yii::app()->hasModule('store') && $decimalsCount === null){
			if(Yii::app()->getModule('store')->roundPrice){
				$decimals = 0;
			} else {
				$decimals = ((int)$price == (float)$price) ? 0 : 2;
			}
		}
		if($decimalsCount){
			$decimals = $decimalsCount;
		}
		return number_format($price, $decimals, '.', ' ');
	}

	/**
	 * @return bool
	 */
	public static function showBacktrace() {
		echo '<pre style="text-align:left;font-size:11px;">';
		$i = -1;
		foreach (debug_backtrace() as $trace){
			if($i === -1){
				$i++;
				continue;
			}
			echo sprintf("#%s %s->%s called as [%s:%s]\n", $i, $trace['class'], $trace['function'], $trace['file'], $trace['line']);
			$i++;
		}
		echo '</pre>';
	}

	/**
	 * @param $day
	 * @return mixed
	 */
	public static function getDayOfWeek($day){
		$short = [
			1 => 'пн',
			2 => 'вт',
			3 => 'ср',
			4 => 'чт',
			5 => 'пт',
			6 => 'сб',
			7 => 'вс',
		];

		return $short[$day];
	}

	/**
	 * @param string $attribute
	 * @param null $value
	 * @return bool
	 */
	public static function isFieldChecked($attribute, $value = null)
	{
		if($value === null){
			return false;
		}
		return Yii::app()->getRequest()->getParam($attribute, null) == $value;
	}

	/**
	 * @param $n
	 * @param array $value
	 * @return mixed
	 */
	public static function getParity($n, $value = ['odd', 'even']){
		return $n % 2 === 0 ? $value[1] : $value[0];
	}

	/**
	 * @return bool
	 */
	public static function hasMidTariff(){
		$tariff = Yii::app()->getModule('webforma')->tariff;
		return  $tariff == WebformaModule::TARIFF_MID || $tariff == WebformaModule::TARIFF_MAX;
	}

	/**
	 * @param $dir
	 * @param $mode
	 * @return bool
	 */
	public static function createDirectoryIfNotExist($dir, $mode = null){
		if(!is_dir($dir)){
			return CFileHelper::createDirectory($dir, $mode);
		}
		return true;
	}

	/**
	 * @param $directory
	 * @param $file
	 * @param array $exclude
	 * @return bool
	 */
	public static function deleteFiles($directory, $file, $exclude = array()){
		if (!$directory || !$file) return false;

		if (is_file($directory.$file)) {
			unlink($directory.$file);
		} else {
			$paths = glob($directory.'*', GLOB_MARK | GLOB_ONLYDIR | GLOB_NOSORT);

			if ($paths) {
				foreach ($paths as $path) {
					if ($exclude){
						$_p = basename($path);
						if(in_array($_p, $exclude)) {
							continue;
						}
					}
					if($file === '*'){
						self::clearDir($path);
						self::deleteFiles($path, $file, $exclude);
					} elseif (is_file($path.$file)) {
						@unlink($path.$file);
					} else {
						self::deleteFiles($path, $file, $exclude);
					}
				}
			}
		}
	}

	/**
	 * @param $pathInfo
	 * @param $urlSuffix
	 * @return bool|string
	 */
	public static function removeUrlSuffix($pathInfo,$urlSuffix)
	{
		if($urlSuffix!=='' && substr($pathInfo,-strlen($urlSuffix))===$urlSuffix)
			return substr($pathInfo,0,-strlen($urlSuffix));
		else
			return $pathInfo;
	}

	/**
	 * @param $directory
	 * @param $recursive
	 * @param $exclude
	 * @return bool
	 */
	public static function clearDir($directory, $recursive = true, $exclude = array()){
		if (!$directory) return false;
		$directory = rtrim($directory, '/').'/';

		$files = glob($directory.'*');

		if ($files) {
			foreach ($files as $file) {
				if ($exclude){
					$_p = basename($file);
					if(in_array($_p, $exclude)) {
						continue;
					}
				}

				if(is_dir($file)){
					if($recursive){
						self::clearDir($file);
					}
					@rmdir($file);
				} else {
					@unlink($file);
				}
			}
		}
	}

	/**
	 * @return int
	 */
	public static function getCacheTime(){
		return Yii::app()->getModule('webforma')->coreCacheTime;
	}

	/**
	 * @param $key
	 * @param $array
	 * @return mixed
	 */
	public static function arrayPopItem($key, &$array){
		$value = $array[$key];
		unset($array[$key]);
		return $value;
	}

	/**
	 * @param $url
	 * @return bool
	 */
	public static function isItemActive($url)
	{
		return (trim($url, '/') == trim(Yii::app()->request->requestUri, '/'));
	}

	/**
	 * Слияние массивов с сохранением ключей
	 * @return array|mixed
	 */
	public static function mergeArray()
	{
		$args = func_get_args();
		$res = array_shift($args);
		while (!empty($args)) {
			$next = array_shift($args);
			foreach ($next as $k => $v) {
				if (is_integer($k))
					isset($res[$k]) ? $res[] = $v : $res[$k] = $v;
				elseif (is_array($v) && isset($res[$k]) && is_array($res[$k]))
					$res[$k] = self::mergeArray($res[$k], $v);
				else
					$res[$k] = $v;
			}
		}
		return $res;
	}

	/**
	 * @param $url
	 * @return bool
	 */
	public static function hasSameURL($url){
		list($currentUri, ) = explode('?', Yii::app()->request->requestUri);
		$currentUri = trim($currentUri, '/');
		$url = trim($url, '/');

		if($url == $currentUri){
			return true;
		}

		$exp = explode('/', $currentUri);
		if(sizeof($exp) > 1){
			while (sizeof($exp) > 0){
				array_pop($exp);
				if(implode('/', $exp) == $url){
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * @param $model
	 * @param $behavior
	 * @param array $htmlOptions
	 * @return false|string
	 */
	public static function getImage($model, $behavior, $htmlOptions = []){
		$attributeName = $model->{$behavior}->attributeName;
		$file = $model->{$behavior}->getOwner()->{$attributeName};

		if($file && \CFileHelper::getExtension($file) == 'svg'){
			$path = $model->{$behavior}->getFilePath();
			if(file_exists($path)){
				$image = file_get_contents($path);
				if($image){
					return $image;
				}
			}
		}

		$data = [];
		foreach (['width', 'height', 'crop', 'alt'] as $item){
			$data[$item] = self::arrayPopItem($item, $htmlOptions);
		}
		extract($data);

		return CHtml::image($model->{$behavior}->getImageUrl($width, $height, $crop), $alt, $htmlOptions);
	}
}