<?php
class DateHelper {

	/**
	 * @param $date
	 * @param int $type
	 * @return false|string
	 */
	public static function getDate($date, $type = 1){
		if(!$date){
			return '';
		}

		switch ($type){
			case 1:
				return date('d.m.Y', strtotime($date));
				break;
			case 2:
				if(!Helper::isDate($date, 'd.m.Y')){
					return '';
				}
				$month = [
					'01' => 'января',
					'02' => 'февраля',
					'03' => 'марта',
					'04' => 'апреля',
					'05' => 'мая',
					'06' => 'июня',
					'07' => 'июля',
					'08' => 'августа',
					'09' => 'сентября',
					'10' => 'октября',
					'11' => 'ноября',
					'12' => 'декабря',
				];
				list($d, $m, $y) = explode('.', $date);
				return $d.' '.$month[$m].($y == date('Y') ? '' : ' '.$y);
				break;
		}
	}
}