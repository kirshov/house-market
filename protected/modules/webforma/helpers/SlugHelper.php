<?php

class SlugHelper{

	/**
	 * @param string $slug
	 * @param int $length
	 * @return string mixed
	 */
	public static function prepareSlug($slug, $length = 145){
		$slug = trim($slug);
		$slug = preg_replace('/\s{2,}/',' ',$slug);
		$slug = str_replace(' ', '-', $slug);
		$slug = preg_replace('/[^a-zA-Zа-яА-Я0-9-_.]/ui', '', $slug);
		$slug = mb_strtolower($slug);
		$slug = \dosamigos\yii\helpers\TransliteratorHelper::process($slug);
		$slug = preg_replace(\webforma\components\validators\YSLugValidator::$pattern, '', $slug);
		$slug = mb_substr($slug, 0, $length);

		return $slug;
	}

	/**
	 * @param $table
	 * @param $slug
	 * @param null $id
	 * @param string $slugField
	 * @return string
	 * @throws CException
	 */
	public static function getUniqueSlug($table, $slug, $id = null, $slugField = 'slug'){
		$linkCount = 0;
		$linkPostfix = '';

		$command = Yii::app()->db->createCommand();

		do {
			$newSlug = $slug.$linkPostfix;

			$command
				->reset()
				->select($slugField)
				->from($table)
				->where($slugField.' = :slug');
			$command->params = [':slug' => $newSlug];

			if($id !== null){
				$command->where .= ' AND id != :id';
				$command->params[':id'] = $id;
			}

			$hasExist = $command->queryScalar();

			$linkCount++;
			$linkPostfix = '-' . $linkCount;
		} while ($hasExist);

		return $newSlug;
	}
}