<?php
	$this->hideSidebar = $this->hideSidebar || !sizeof($this->menu);
	list($requestUri) = explode('?', $_SERVER['REQUEST_URI']);
	$isMain = (Yii::app()->getController()->getId() == 'backend' && Yii::app()->getController()->getModule()->getId() == 'webforma');
	$this->beginContent($this->webforma->getBackendLayoutAlias("main"));
?>
<div class="row">
    <div class="<?= $this->hideSidebar ? 'hidden' : 'left-column col-md-3 col-lg-2 hidden-xs hidden-sm'; ?>">
        <?php if (sizeof($this->menu)): ?>
            <div class="panel panel-default">
				<?php $this->widget('webforma\widgets\WAdminMenu', [
					'items' => $isMain ? Yii::app()->moduleManager->getModules(true, false, false) : $this->webforma->getSubMenu($this->menu),
				]); ?>
            </div>
        <?php endif; ?>
    </div>

	<div class="<?= $this->hideSidebar ? 'col-sm-12' : 'right-column col-sm-12 col-md-9 col-lg-10'; ?>">
		<?php $this->widget('bootstrap.widgets.TbAlert'); ?>
		<!-- breadcrumbs -->
		<?php
		if (count($this->breadcrumbs)) {
			$this->widget('bootstrap.widgets.TbBreadcrumbs', [
				'homeLink' => $isMain ? false : CHtml::link(Yii::t('WebformaModule.webforma', 'Home'), ['/webforma/backend/index']),
				'links'    => $this->breadcrumbs,
			]);
		}
		?>
		<div id="content">
			<?= $content; ?>
		</div>
		<!-- content -->
	</div>
</div>
<?php $this->endContent(); ?>
