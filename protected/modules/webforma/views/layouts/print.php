<?php
$mainAssets = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.webforma.views.assets'));
Yii::app()->getClientScript()->registerCssFile($mainAssets.'/css/fonts.css');
?>
<!DOCTYPE html>
<html lang="<?= Yii::app()->getLanguage(); ?>">
<head>
</head>

<body class="print-page">
	<?= $content; ?>
</body>
</html>
