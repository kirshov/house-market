<?php
/**
 * Отображение для layouts/main
 * @var BackendController $this
 **/
?>
<!DOCTYPE html>
<html lang="<?= Yii::app()->getLanguage(); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= CHtml::encode(Yii::app()->name); ?> <?= ($this->pageTitle) ? (' - ' . CHtml::encode($this->pageTitle)) : ''; ?></title>
    <?php
    $mainAssets = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.webforma.views.assets'));
    $cs = Yii::app()->getClientScript();
    $cs->registerCssFile($mainAssets.'/css/styles.css');
    $cs->registerCssFile($mainAssets.'/css/bootstrap-notify.css');
    $cs->registerCssFile($mainAssets.'/css/font-awesome.min.css');
	$cs->registerCssFile($mainAssets.'/css/select2.css');
	$cs->registerCssFile($mainAssets.'/css/fonts.css');
    $cs->registerScriptFile($mainAssets.'/js/main.js');
    $cs->registerScriptFile($mainAssets.'/js/bootstrap-notify.js');
    $cs->registerScriptFile($mainAssets.'/js/jquery.li-translit.js');
    $cs->registerScriptFile($mainAssets.'/js/jquery.stickyfooterbar.js');
    $cs->registerScriptFile($mainAssets.'/js/app.js');
    $cs->registerScriptFile($mainAssets.'/js/jquery.formstyler.min.js');
    ?>
    <?php
    $cs->registerScript(
        'webformaToken',
        'var actionToken = '.json_encode(
            [
                'token' => Yii::app()->getRequest()->csrfTokenName.'='.Yii::app()->getRequest()->csrfToken,
                'url' => Yii::app()->createAbsoluteUrl('webforma/modulesBackend/moduleStatus'),
                'message' => Yii::t('WebformaModule.webforma', 'Wait please, your request in process...'),
                'error' => Yii::t(
                    'WebformaModule.webforma',
                    'During the processing of your request an unknown error occurred =('
                ),
	            'progressbar' => '<div class="progress"><div class="progress-bar progress-bar-striped active" style="width: 100%"></div></div>',
                'buttons' => [
                    'yes' => Yii::t('WebformaModule.webforma', 'Ok'),
                    'no' => Yii::t('WebformaModule.webforma', 'Cancel'),
                ],
                'messages' => [
                    'confirm_update' => Yii::t(
                        'WebformaModule.webforma',
                        'Do you really want to update configuration file?'
                    ),
                    'confirm_deactivate' => Yii::t(
                        'WebformaModule.webforma',
                        'Do you really want to disable module? We disable all dependent modules!'
                    ),
                    'confirm_activate' => Yii::t('WebformaModule.webforma', 'Do you really want to enable module?'),
                    'confirm_uninstall' => Yii::t(
                            'WebformaModule.webforma',
                            'Do you really want to delete module?'
                        ).'<br />'.Yii::t('WebformaModule.webforma', 'All module parameters will be deleted'),
                    'confirm_install' => Yii::t(
                            'WebformaModule.webforma',
                            'Do you really want to install module?'
                        ).'<br />'.Yii::t('WebformaModule.webforma', 'New module parameters will be added'),
                    'confirm_cacheFlush' => Yii::t('WebformaModule.webforma', 'Do you really want to clean cache?'),
                    'confirm_cacheAll' => Yii::t('WebformaModule.webforma', 'Do you really want to clean cache?'),
                    'confirm_assetsFlush' => Yii::t('WebformaModule.webforma', 'Do you really want to clean assets?'),
                    'confirm_cacheAssetsFlush' => Yii::t(
                            'WebformaModule.webforma',
                            'Do you really want to clean cache and assets?'
                        ).'<br />'.Yii::t('WebformaModule.webforma', 'This process can take much time!'),
                    'unknown' => Yii::t('WebformaModule.webforma', 'Unknown action was selected!'),
                ],
            ]
        ),
        CClientScript::POS_BEGIN
    );

    $bodyClass = '';
	if($this->hideSidebar){
		$bodyClass .= ' onecol';
	}

    if($this->mobileDetect()){
	    $bodyClass .= ' gadget';
    }
    ?>
    <link rel="shortcut icon" href="<?= $mainAssets; ?>/img/favicon.ico"/>

    <script type="text/javascript">
        var webformaTokenName = '<?= Yii::app()->getRequest()->csrfTokenName;?>';
        var webformaToken = '<?= Yii::app()->getRequest()->getCsrfToken();?>';
    </script>

</head>

<body class="admin-panel<?=$bodyClass;?>">
	<div id="overall-wrap">
		<?php $this->widget('webforma\widgets\WAdminPanel'); ?>
		<div class="container-fluid" id="page"><?= $content; ?></div>
		<div id="footer-guard"></div>
	</div>

	<footer>
		&copy; 2012 - <?= date('Y'); ?>
		<?= $this->webforma->poweredBy(); ?>
	</footer>
</body>
</html>
