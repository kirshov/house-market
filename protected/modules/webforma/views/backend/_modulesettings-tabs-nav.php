<?php
	$i = 0;
	Yii::app()->getClientScript()->registerScript('setCurrentTabActive', 'setCurrentTabActive();');
?>
<ul class="nav nav-tabs horizontal-tabs">
	<?php foreach ((array)$tabs as $tabItem): ?>
		<?$i++;?>
		<li<?=($i == 1 ? ' class="active"' : '')?>><a href="#tb-c-<?=$i?>" data-toggle="tab"><?=$tabItem['label']?></a></li>
	<?php endforeach; ?>
</ul>