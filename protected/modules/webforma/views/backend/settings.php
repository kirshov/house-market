<?php
$this->breadcrumbs = [
    Yii::t('WebformaModule.webforma', 'FORMA.cms') => ['settings'],
    Yii::t('WebformaModule.webforma', 'Modules'),
];

$script = "var url = document.location.toString();
    if (url.match('#')) { $('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show'); }
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        if(history.pushState) { history.pushState(null, null, e.target.hash); } else { window.location.hash = e.target.hash; }
    });";

Yii::app()->getClientScript()->registerScript('tabs-remember', $script, CClientScript::POS_END);
?>

<h1><?= Yii::t('WebformaModule.webforma', 'Modules'); ?></h1>

<div class="alert alert-warning">
    <p>
        <?php
        $webformaCount = count($modules);
        $enableCount = 0;
        foreach ($modules as $module) {
            if ('install' === $module->id) {
                $enableCount--;
                $webformaCount--;
            }
            if ($module instanceof webforma\components\WebModule && ($module->getIsActive() || $module->getIsNoDisable())) {
                $enableCount++;
            }
        }
        ?>
        <?= Yii::t('WebformaModule.webforma', 'Installed'); ?>
        <small class="label label-info"><?= $webformaCount; ?></small>
        <?= Yii::t('WebformaModule.webforma', 'module|module|modules', $webformaCount); ?>,
        <?= Yii::t('WebformaModule.webforma', 'enabled'); ?>
        <small class="label label-info"><?= $enableCount; ?></small>
        <?= Yii::t('WebformaModule.webforma', 'module|module|modules', $enableCount); ?>,
        <?= Yii::t('WebformaModule.webforma', 'disabled|disabled', $webformaCount - $enableCount); ?>
        <small class="label label-info"><?= $webformaCount - $enableCount; ?></small>
        <?= Yii::t('WebformaModule.webforma', 'module|module|modules', $webformaCount - $enableCount); ?>
        <br/>
    </p>
</div>

<?= $this->renderPartial('_moduleslist', ['modules' => $modules]); ?>
<script type="text/javascript">
	setCurrentTabActive();
</script>