<?php
/**
 *
 */
$this->breadcrumbs = [
	'Редактор шаблонов' => ['/webforma/templateBackend/index'],
	'Редактирование'
];
$this->menu = $tree;
?>
<div class="template-content">
	<?if($issetFile):?>
		<h3><?=$file?></h3>
		<br/>
		<?
		$form = $this->beginWidget(
			'\webforma\widgets\ActiveForm',
			[
				'enableAjaxValidation' => false,
				'enableClientValidation' => false,
				'type' => 'vertical',
			]
		); ?>

		<div class="ace-content">
			<textarea id="file-content" name="file" style="font-family:monospace;width:100%;height:500px;"><?=$fileContent?></textarea>
		</div>
		<input type="hidden" name="file" class="file"/>

		<div class="buttons">
			<?php $this->widget('bootstrap.widgets.TbButton', [
				'buttonType' => 'submit',
				'context' => 'primary',
				'label' => 'Сохранить'
			]); ?>
		</div>

		<?$this->endWidget();?>
	<?else:?>
		<div class="template-body" style="text-align: center;">
			<h1>Добро пожаловать!</h1>
			<p>&nbsp</p>
			<p>&larr; Выберите интересующий файл слева</p>
		</div>
	<?endif;?>
</div>
