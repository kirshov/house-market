<?
$this->breadcrumbs = [
	'Панель управления сайтом',
];
?>

<div class="main-widgets">
	<div class="row">
		<?
		$allWidgets = [];
		foreach ($modules as $module){
			if ($module instanceof webforma\components\WebModule === false){
				continue;
			}

			if ($module->getIsActive()){
				foreach ($module->getPanelWidgets() as $widget => $params){
					$position = (int) $params['position'];
					unset($params['position']);
					if(!$position || isset($widgets[$position])){
						$allWidgets[][$widget] = $params;
					} else {
						$allWidgets[$position][$widget] = $params;
					}
				}
			}
		}
		if($allWidgets){
			ksort($allWidgets);
			foreach ($allWidgets as $widgets){
				foreach ($widgets as $widget => $params){
					$this->widget($widget, $params);
				}
			}
		}
		?>
	</div>
</div>

<?if(DEVMODE):?>
	<legend><?= Yii::t('WebformaModule.webforma', 'Fast access to modules'); ?> </legend>
	<?php
	$this->widget(
		'webforma\widgets\WShortCuts',
		[
			'modules' => $modules,
		]
	); ?>
<?endif;?>

<?php $this->menu = $modulesNavigation;?>
