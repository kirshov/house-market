<?php $i = 0; ?>
<?php foreach ((array)$groups as $title => $items): ?>
	<?php $i++; ?>
	<div class="row">
		<div class="col-sm-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title"><?= $title ?></h4>
				</div>
				<div class="panel-body">
					<?php foreach ((array)$items as $item): ?>
						<?= $item ?>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
<?php endforeach; ?>