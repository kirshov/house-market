<?php
if(DEVMODE){
	$this->breadcrumbs = [
		Yii::t('WebformaModule.webforma', 'Modules') => ['settings'],
		Yii::t('WebformaModule.webforma', 'Module settings'). ' «'.CHtml::encode($module->name).'»',
	];
} else {
	$this->breadcrumbs = [
		$module->getName() => is_array($module->getAdminPageLink()) ? $module->getAdminPageLink() : [$module->getAdminPageLink()],
		Yii::t('WebformaModule.webforma', 'Module settings'),
	];
}
?>

<?
if($module->renderAdvancedModule && file_exists(Yii::getPathOfAlias('application.modules.'.$module->id.'.views').'/beforeModule.php')){
	$this->renderPartial('application.modules.'.$module->id.'.views.beforeModule');
}
?>

<?php if (is_array($groups) && count($groups)) {
	if($module->adminSettingsAsTab){
		$this->renderPartial('_modulesettings-tabs-nav', ['tabs' => $tabs, 'groups' => $groups]);
	}

	echo CHtml::beginForm(['/webforma/backend/saveModulesettings'], 'post', [
		'class' => 'well sticky form module-settings-form',
		'enctype' => 'multipart/form-data',
	]);

	echo CHtml::hiddenField('module_id', $module->getId());

	if($module->renderAdvancedModule && file_exists(Yii::getPathOfAlias('application.modules.'.$module->id.'.views').'/beforeModuleForm.php')){
		$this->renderPartial('application.modules.'.$module->id.'.views.beforeModuleForm');
	}

	if($module->adminSettingsAsTab){
		$this->renderPartial('_modulesettings-tabs', ['tabs' => $tabs, 'groups' => $groups]);
	} else {
		$this->renderPartial('_modulesettings-list', ['groups' => $groups]);
	}

	if($module->renderAdvancedModule && file_exists(Yii::getPathOfAlias('application.modules.'.$module->id.'.views').'/afterModuleForm.php')){
		$this->renderPartial('application.modules.'.$module->id.'.views.afterModuleForm');
	}
	?>

    <div class="row">
        <div class="col-sm-12">
            <?= CHtml::submitButton('Сохранить настройки модуля', [
				'class' => 'btn btn-primary',
				'id'    => 'saveModuleSettings',
				'name'  => 'saveModuleSettings',
			]); ?>
        </div>
    </div>
    <?= CHtml::endForm(); ?>
<?php } else { ?>
    <b><?= Yii::t('WebformaModule.webforma', 'There is no parameters which you cat change for this module...'); ?></b>
<?php } ?>

<?
if($module->renderAdvancedModule && file_exists(Yii::getPathOfAlias('application.modules.'.$module->id.'.views').'/afterModule.php')){
	$this->renderPartial('application.modules.'.$module->id.'.views.afterModule');
}
?>