<?php $j = 0;?>
<div class="tab-content">
	<?php foreach ((array)$tabs as $title => $tabItems): ?>
		<?php $j++;?>
		<div class="tab-pane<?=$j == 1 ? ' active' : ''?>" id="tb-c-<?=$j?>">
			<?php foreach ((array)$tabItems['items'] as $tabItem): ?>
				<?php foreach ((array)$groups[$tabItem] as $title => $items): ?>
					<?php $i++; ?>
					<div class="row">
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><?= $title ?></h4>
								</div>
								<div class="panel-body">
									<?php foreach ((array)$items as $item): ?>
										<?= $item ?>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			<?php endforeach; ?>
		</div>
	<?php endforeach; ?>
</div>
