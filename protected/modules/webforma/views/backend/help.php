<?php
$this->breadcrumbs = [
    Yii::t('WebformaModule.webforma', 'FORMA.cms') => ['settings'],
    Yii::t('WebformaModule.webforma', 'Help'),
];
?>

<h1><?= Yii::t('WebformaModule.webforma', 'About FORMA.cms'); ?></h1>

<p>
    <?= Yii::t('WebformaModule.webforma', 'You use Yii version'); ?>
    <small class="label label-info" title="<?= Yii::getVersion(); ?>"><?= Yii::getVersion(); ?></small>
    ,
    <?= CHtml::encode(Yii::app()->name); ?>
    <?= Yii::t('WebformaModule.webforma', 'version'); ?>
    <small class="label label-info"
           title="<?= $this->webforma->version; ?>"><?= $this->webforma->version; ?></small>
    ,
    <?= Yii::t('WebformaModule.webforma', 'php version'); ?>
    <small class="label label-info" title="<?= phpversion(); ?>"><?= phpversion(); ?></small>
</p>


<p><b><?= Yii::t('WebformaModule.webforma', 'Interesting resources:'); ?></b></p>

<?= CHtml::link(
    Yii::t('WebformaModule.webforma', 'Read Yii documentation', ['target' => '_blank']),
    'http://yiiframework.com/doc/guide/index'
); ?>
<br/><br/>


<?= CHtml::link(
    Yii::t('WebformaModule.webforma', 'Official FORMA.cms site', ['target' => '_blank']),
    'http://webforma.ru/?form=help'
); ?> - <?= Yii::t('WebformaModule.webforma', 'use most!'); ?>

<br/><br/>

<?= CHtml::link(
    Yii::t('WebformaModule.webforma', 'Community', ['target' => '_blank']),
    'http://webforma.ru/?form=help'
); ?> - <?= Yii::t('WebformaModule.webforma', 'use most!'); ?>

<br/><br/>

<?= CHtml::link(
    Yii::t('WebformaModule.webforma', 'Official FORMA.cms docs', ['target' => '_blank']),
    'http://docs.webforma.ru/?form=help'
); ?> - <?= Yii::t(
    'WebformaModule.webforma',
    ' We are working with it =)'
); ?>

<br/><br/>

<?= CHtml::link(
    Yii::t('WebformaModule.webforma', 'Additional modules and components'),
    'http://webforma.ru/marketplace',
    ['target' => '_blank']
); ?> - <?= Yii::t(
    'WebformaModule.webforma',
    'Submit an extension!'
); ?>

<br/><br/>

<?= CHtml::link(
    Yii::t('WebformaModule.webforma', 'Support FORMA.cms forum'),
    'http://talk.webforma.ru/?form=help',
    ['target' => '_blank']
); ?> - <?= Yii::t('WebformaModule.webforma', 'Lest\'s talk!'); ?>

<br/><br/>

<?= CHtml::link(
    Yii::t('WebformaModule.webforma', 'Sources on Github'),
    'http://github.com/webforma/webforma/',
    ['target' => '_blank']
); ?> - <?= Yii::t(
    'WebformaModule.webforma',
    'Send pull request for us =)'
); ?>

<br/><br/>

<?= CHtml::link(
    Yii::t('WebformaModule.webforma', 'Official FORMA.cms twitter'),
    'https://twitter.com/#!/webforma',
    ['target' => '_blank']
); ?>  - <?= Yii::t('WebformaModule.webforma', 'Follow us =)'); ?>

<br/><br/>

<?= CHtml::link(
    Yii::t('WebformaModule.webforma', 'General sponsor'),
    'http://amylabs.ru?from=webforma-help',
    ['target' => '_blank']
); ?> - <?= Yii::t(
    'WebformaModule.webforma',
    'Just great guys =)'
); ?>

<br/><br/>

<div class="alert alert-warning">
    <?= Yii::t(
        'WebformaModule.webforma',
        'Feedback at <a href="">mail</a> or {link}',
        [
            '{link}' => CHtml::link(
                Yii::t('WebformaModule.webforma', 'feedback form'),
                'http://webforma.ru/contacts?from=help',
                ['target' => '_blank']
            ),
        ]
    ); ?> - <?= Yii::t('WebformaModule.webforma', 'accept any kind of business and any proposals =)'); ?>
</div>

<br/>

<b><?= Yii::t('WebformaModule.webforma', 'FORMA.cms developers team'); ?></b>


