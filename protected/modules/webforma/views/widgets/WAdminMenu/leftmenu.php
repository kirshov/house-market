<?php
$this->widget('zii.widgets.CMenu', [
	'items' => $items,
	'itemCssClass' => 'left-menu__item',
	'htmlOptions' => [
		'class' => 'nav nav-list'
	],
	'submenuHtmlOptions' => [
		'class' => 'dropdown-menu',
	],
	'activateParents' => true,
]);