<?php
/**
 * Отображение для виджета WAdminPanel
 *
 * @category WebformaModuleView
 **/
$mainAssets = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.webforma.views.assets'));
$frontendAssets = Yii::app()->getTheme()->getAssetsUrl();
$logo = Yii::app()->getModule('webforma')->getLogo();

$isAdmin = Yii::app()->getUser()->checkAccess('admin');
$canUpdateUser = Yii::app()->getUser()->checkAccess('User.UserBackend.Update');

foreach ($modules as &$item) {
    $item['linkOptions'] = ['title' => $item['label']];
    $item['label'] = CHtml::tag('span', ['class' => 'hidden-sm'], $item['label']);
}

$icons = [];
$cacheTime = 10;
$messagesLink = '';
if(Yii::app()->hasModule('feedback')){
	$messagesFeedBackCount = FeedBack::model()->cache($cacheTime)->count('status = :status', [':status' => FeedBack::STATUS_NEW]);
	if($messagesFeedBackCount){
		$messagesLink = Yii::app()->createUrl('/backend/feedback/feedback/');
	}
}
if(Yii::app()->hasModule('callback')){
	$messagesCallbackCount = Callback::model()->cache($cacheTime)->count('status = :status', [':status' => Callback::STATUS_NEW]);
	if($messagesCallbackCount && !$messagesFeedBackCount){
		$messagesLink = Yii::app()->createUrl('/backend/callback/callback/');
	}
}

if(!$messagesLink){
	$messagesLink = Yii::app()->createUrl('/backend/feedback/feedback/');
}
$icons = [];

if(Yii::app()->hasModule('order')){
	$orderCount = Order::model()->cache($cacheTime)->count('status_id = :status', [':status' => OrderStatus::STATUS_NEW]);
	$icons['orders'] = [
		'icon' => 'fa fa-fw fa-shopping-basket',
		'label' => CHtml::tag('span', ['class' => 'navbar-label navbar-label-messages'], $orderCount ?: '').CHtml::tag('span', ['class' => 'hidden-sm hidden-md hidden-lg'], 'Заказы'),
		'url' => Yii::app()->createUrl('/backend/order/order/')
	];
}

if(Yii::app()->hasModule('review')){
	$reviewCount = Review::model()->cache($cacheTime)->count('status = :status', [':status' => Review::STATUS_NEED_CHECK]);
	$icons['review'] = [
		'icon' => 'fa fa-fw fa-bullhorn',
		'label' => CHtml::tag('span', ['class' => 'navbar-label navbar-label-notifications'], $reviewCount ?: '').CHtml::tag('span', ['class' => 'hidden-sm hidden-md hidden-lg'], 'Отзывы'),
		'url' => Yii::app()->createUrl('/backend/review/review/')
	];
}

$icons['messages'] = [
	'icon' => 'fa fa-fw fa-envelope',
	'label' => CHtml::tag('span', ['class' => 'navbar-label navbar-label-messages'], ((int) $messagesFeedBackCount + (int) $messagesCallbackCount ?: '')).CHtml::tag('span', ['class' => 'hidden-sm hidden-md hidden-lg'], 'Сообщения'),
	'url' => $messagesLink,
];

$this->widget(
    'bootstrap.widgets.TbNavbar',
    [
        'fluid'    => true,
        'fixed'    => 'top',
        'brand'    => CHtml::image(
			$logo ?: $mainAssets . '/img/logo.png',
            CHtml::encode(Yii::app()->name),
            [
                'title' => CHtml::encode(Yii::app()->name),
				'style' => 'max-height:30px; margin-top:5px;',
            ]
        ),
        'brandUrl' => CHtml::normalizeUrl(["/webforma/backend/index"]),
        'items'    => [
            [
                'class' => 'bootstrap.widgets.TbMenu',
                'type'  => 'navbar',
                'encodeLabel' => false,
                'items' => $modules
            ],
            [
                'class'       => 'bootstrap.widgets.TbMenu',
                'htmlOptions' => ['class' => 'navbar-right visible-xs hidden-sm hidden-md visible-lg'],
                'type'        => 'navbar',
                'encodeLabel' => false,
                'items'       => [
					[
						'icon' => 'fa fa-fw fa-reorder',
						'label' => '',
						'linkOptions' => ['title' => 'Меню'],
						'items' => CMap::mergeArray($icons, [
							[
								'icon' => 'fa fa-fw fa-support',
								'label' => CHtml::tag('span', ['class' => 'hidden-sm hidden-md hidden-lg'], 'Помощь'),
								'url' => Yii::app()->createUrl('/backend/help/help/'),
								'visible' => Yii::app()->hasModule('help'),
								'linkOptions' => ['title' => 'Помощь'],
							],
							[
								'icon' => 'fa fa-fw fa-home',
								'label' => CHtml::tag('span', ['class' => 'hidden-sm hidden-md hidden-lg'], Yii::t('WebformaModule.webforma', 'Go home')),
								'url' => Yii::app()->createAbsoluteUrl('/'),
								'linkOptions' => ['title' => Yii::t('WebformaModule.webforma', 'Go home')],
							],
							[
								'icon' => 'fa fa-fw fa-user',
								'label' => CHtml::tag('span', ['class' => 'hidden-sm hidden-md hidden-lg'], 'Профиль'),
								'items' => [
									[
										'icon' => 'fa fa-fw fa-cog',
										'label' => Yii::t('WebformaModule.webforma', 'Profile'),
										'url' => ($isAdmin || $canUpdateUser)
											? CHtml::normalizeUrl((['/user/userBackend/update', 'id' => Yii::app()->getUser()->getId()]))
											: Yii::app()->createAbsoluteUrl('/user/profile/profile'),
										//'linkOptions' => ['title' => Yii::t('WebformaModule.webforma', 'Profile')],
									],
									[
										'icon' => 'fa fa-fw fa-power-off',
										'label' => Yii::t('WebformaModule.webforma', 'Exit'),
										'url' => CHtml::normalizeUrl(['/user/account/logout']),
										//'linkOptions' => ['title' => Yii::t('WebformaModule.webforma', 'Exit')],
									],
								],
							],
						]),
					],
				],
            ],
        ],
    ]
);?>

<script type="text/javascript">
    $(document).ready(function () {
        var url = window.location.href;
        $('.navbar .nav li').removeClass('active');
        $('.nav a').filter(function () {
            return this.getAttribute("href") != '#' && this.href == url;
        }).parents('li').addClass('active');
    });
</script>
