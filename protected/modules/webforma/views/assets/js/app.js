$(function(){
	var stickyButtons = $('form .buttons');
	if(stickyButtons.length){
		stickyButtons.css({
			left: stickyButtons.closest('form').offset().left + 1,
			width: stickyButtons.closest('form').outerWidth() - 2
		});

		stickyButtons.stickyfooterbar();
	}

	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		var hashName = $(this).attr('href');
		window.location.href = '#tab-' + hashName.replace(/#/, '');
		$(window).resize();
	});

	$('form').on('submit', function () {
		if(window.location.hash){
			$(this).attr('action', $(this).attr('action') + window.location.hash);
		}
		return true;
	});

	if($('.input-phone').length){
		$.each($('.input-phone'), function () {
			setPhoneMask($(this));
		});
	}
});

function setPhoneMask(elem){
	elem.on('paste input', function (event) {
		try {
			var l = elem.val().length,
				lastKey = elem.val()[l - 1],
				prevStr = elem.val().substr(0, l - 1);

			if(lastKey == '8'){
				if(!prevStr || prevStr == '+' || prevStr == '+7' || prevStr == '+7 '){
					elem.val('+7');
					return false;
				}
			}
		} catch (e){}

		var phone = $(this).val().replace(/\D/g, ''),
			phoneFull = $(this).val().replace(/[^\d\+]/g, ''),
			firstChar = phone.substr(0, 1);

		if(phoneFull.substr(0, 1) == '+'){
			if(phoneFull.length > 12){
				$(this).val('+7' + phoneFull.substr(2, 10));
			}
		} else if((firstChar == '8' && phone.length > 10) || ((firstChar == '+' || firstChar == '7') && phone.length > 11)){
			$(this).val('+7' + phone.substr(1, 10));
		}
	});
	elem.mask('+7 (000) 000-00-00');
}

function setCurrentTabActive() {
	var hashName = window.location.hash;
	if(hashName){
		$('a[href="#' + hashName.replace('#tab-', '') + '"]').tab('show');
	}
}
