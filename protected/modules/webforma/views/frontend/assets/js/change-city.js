$(document).ready(function(){
	var _ssaq;
	$('.change-city-form .search-city').autoComplete({
		source: function(term, response){
			try{_ssaq.abort();}catch(e){}
			$('.change-city-form input.search-city').addClass('loader');
			_ssaq = $.getJSON($('.change-city-form').data('search-action'), { city: term }, function(data){
				$('.change-city-form input.search-city').removeClass('loader');
				response(data);
			});
		},
		minChars: 2,
		cache: false,
		delay: 50,
		menuClass: 'cities',
		renderItem: function (item, search){
			search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
			var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi"),
				city = item[1].replace(re, "<b>$1</b>");
			return '<div class="autocomplete-suggestion" data-id="'+item[0]+'" data-val="'+item[1]+'" title="'+item[1]+'">'+city+'</div>';
		},
		onSelect: function(e, term, item){
			$('.change-city-form input.city-id').val(item.data('id'));
			$('.change-city-form .save-city').removeClass('disabled');
			$('.change-city-form input.search-city').blur();
			$('.change-city-form .save-city').trigger('focus');
			return true;
		}
	});

	$('.change-city-form').on('submit', function (e) {
		e.preventDefault();
	});

	$('input.save-city').on('click', function () {
		var data = {city: $('.change-city-form input.city-id').val()};
		if($('.page.page-cart').length){
			data['type'] = 'cart';
		} else if($('.delivery-cost-widget').length){
			data['type'] = 'widget-cost';
			data['product'] = $('.product-detail').data('id');
		}

		$.getJSON($('.change-city-form').data('set-action'), data, function(response){
			if(response.result){
				$('.modal-window.change-city .modal-close').trigger('click');
				if(response.html){
					if($('.page.page-cart').length){
						var checkedId = $('.order-box__inner input.radio__input:checked').attr('id');
						$('.order-box__inner').replaceWith(response.html);
						if($('.order-box__inner #' + checkedId).length){
							$('.order-box__inner #' + checkedId).attr('checked', true).trigger('change');
						} else {
							$('.order-box__inner input.radio__input:enabled:first').attr('checked', true).trigger('change');
						}
						updateDeliveryPrices();
						calculateCartDeliveryCost();
					} else if($('.delivery-cost-widget').length){
						$('.delivery-cost-widget').replaceWith(response.html);
						calculateDeliveryCost();
					}
					try{
						applyStyler();
					} catch (e){}
				}
			} else {
				$('.change-city-form .save-city').removeClass('disabled');
			}
		});
	});

	$('input.search-city').on('click', function () {
		$(this).val('');
		$('.change-city-form input.city-id').val('');
		$('.change-city-form .save-city').addClass('disabled');
	});
});
function afterChangeCityOpen() {
	$('.change-city-form input.search-city').removeClass('loader');
	$('.search-city').val('').trigger('focus');
}