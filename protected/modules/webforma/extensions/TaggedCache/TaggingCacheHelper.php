<?php
namespace TaggedCache;

class TaggingCacheHelper{

	/**
	 * @return \TaggedCache\Dependency|null
	 */
	public static function getDependency($tags){
		$tagsList = [];
		foreach ((array) $tags as $item){
			$tagsList[] = new \TaggedCache\Tag($item);
		}

		if(!$tagsList){
			return null;
		}

		return new \TaggedCache\Dependency($tagsList);
	}

	/**
	 * @param $tags
	 */
	public static function deleteTag($tags){
		foreach ((array) $tags as $item){
			$tag = new \TaggedCache\Tag($item);
			$tag->delete();
		}

	}
}