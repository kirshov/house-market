<?php
class FMorphy {
	const FMORPHY_DEBUG = true;

	/**
	 * @param $lang
	 * @return phpMorphy
	 */
	protected static function getMorphy($lang){
		require_once dirname(__FILE__).'/src/common.php';
		$dir = dirname(__FILE__) . '/dicts/'.$lang;

		$opts = array(
			'storage' => PHPMORPHY_STORAGE_FILE,
			// Enable prediction by suffix
			'predict_by_suffix' => false,
			// Enable prediction by prefix
			'predict_by_db' => true,
			// TODO: comment this
			'graminfo_as_text' => true,
		);

		try {
			$morphy = new phpMorphy($dir, $lang, $opts);
		} catch(phpMorphy_Exception $e) {
			die('Error occured while creating phpMorphy instance: ' . PHP_EOL . $e);
		}

		return $morphy;
	}

	/**
	 * @param array $words
	 * @param string $lang
	 * @return array
	 */
	public static function getRoots($words = [], $lang = 'ru_RU') {
		$morphy = self::getMorphy($lang);

		$roots = [];

		try {
			foreach($words as $word) {
				$root = $morphy->getPseudoRoot(mb_strtoupper($word));
				if($root[0]){
					$roots[] = mb_strtolower($root[0]);
				} else {
					$roots[] = $word;
				}
			}
		} catch(phpMorphy_Exception $e) {
			if(FMORPHY_DEBUG) {
				die('Error occured while text processing: '.$e->getMessage());
			}
		}

		return $roots;
	}

	/**
	 * @param array $words
	 * @param string $lang
	 * @return array|mixed
	 */
	public static function getAllForms($words = [], $lang = 'ru_RU'){
		$morphy = self::getMorphy($lang);
		$forms = [];

		try {
			foreach($words as $word) {
				$form = $morphy->getAllForms(mb_strtoupper($word));
				if($form){
					$form = array_map(function($value){
						return mb_strtolower($value);
					}, $form);
					$forms = CMap::mergeArray($forms, $form);
				} else {
					$forms[] = $word;
				}
			}
		} catch(phpMorphy_Exception $e) {
			if(FMORPHY_DEBUG) {
				die('Error occured while text processing: '.$e->getMessage());
			}
		}

		return $forms;
	}
}
