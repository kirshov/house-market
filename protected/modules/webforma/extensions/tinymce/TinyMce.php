<?php

/**
 * @property
 */
class TinyMce extends CInputWidget
{
	/**
	 * @var string
	 */
	public $selector = 'tinymce';

    /** @var bool|string Route to compressor action */
    public $compressorRoute = false;

    /**
     * @deprecated use spellcheckerUrl instead
     * @var bool|string Route to spellchecker action
     */
    public $spellcheckerRoute = false;

    /**
     * For example here could be url to yandex spellchecker service.
     * http://speller.yandex.net/services/tinyspell
     * More info about it here: http://api.yandex.ru/speller/doc/dg/tasks/how-to-spellcheck-tinymce.xml
     *
     * @var bool|string|array URL or an action route that can be used to create a URL or false if no url
     */
    public $spellcheckerUrl = '//speller.yandex.net/services/tinyspell';

	/** @var array Widget settings will override defaultSettings */
	public $settings = array();

	/**
	 * @var
	 */
    private $assetsDir;

	/**
	 * @var
	 */
    private $webformaTynimceAssetsDir;

    /** @var bool|string Must be set to force widget language */
    public $language = 'ru'; // editor language, if false app language is used

	/**
	 * @var bool
	 */
    public $fileManager = false;

    public $useCss = false;

    /** @var array Supported languages */
    private static $languages = array(
        'ar', 'ar_SA', 'bg_BG', 'bn_BD', 'bs', 'ca', 'cs', 'cy', 'da', 'de', 'de_AT', 'el', 'es', 'et', 'eu', 'fa', 'fi', 'fo',
        'fr_FR', 'gl', 'he_IL', 'hr', 'hu_HU', 'hy', 'id', 'it', 'ja', 'ka_GE', 'ko_KR', 'lb', 'lt', 'lv', 'ml', 'mn_MN', 'nb_NO', 'nl',
        'pl', 'pt_BR', 'pt_PT', 'ro', 'ru', 'si_LK', 'sk', 'sl_SI', 'sr', 'sv_SE', 'ta', 'ta_IN', 'th_TH', 'tr_TR', 'tt', 'ug', 'uk',
        'uk_UA', 'vi', 'vi_VN', 'zh_CN', 'zh_TW', 'en_GB', 'km_KH', 'tg', 'az', 'en_CA', 'is_IS',
        'be', 'dv', 'kk', 'ml_IN', 'gd',
    ); // widget supported languages


    private static $defaultSettings = array(
        'language' => 'ru',
        //image
        'plugins' => array(
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "template paste textcolor hr spellchecker",
        ),
        'toolbar' => "insertfile undo redo | styleselect fontsizeselect | clear_format bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | media | forecolor backcolor | spellchecker code fullscreen",
        'toolbar_items_size' => 'small',
        'image_advtab' => true,
	    'fontsize_formats' => '8px 10px 11px 12px 13px 14px 18px 24px 36px',

	    'valid_elements' => '*[*]',
	    'extended_valid_elements' => '*[*]',

        'relative_urls' => false,
        'spellchecker_languages' => 'Russian=ru,English=en',
        'spellchecker_language' => 'ru',//"+Русский=ru",
		'spellchecker_rpc_url' => '//speller.yandex.net/services/tinyspell',
        'remove_script_host' => false,
        'convert_urls' => false,
    );

    public function init()
    {
    	if($this->settings['use_css']){
		    $this->useCss = true;
		    unset($this->settings['use_css']);
	    }
	    $this->assetsDir = Yii::app()->assetManager->publish(Yii::getPathOfAlias('vendor').'/tinymce/tinymce/');
	    $this->webformaTynimceAssetsDir = Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.modules.webforma.extensions.tinymce.assets'));
        $this->settings = array_merge(self::$defaultSettings, $this->settings);

		$_SESSION['RF_TINYMCE_SYSDIR'] = Yii::app()->getRuntimePath();
		$_SESSION['RF_TINYMCE_FOLDER'] = isset($this->settings['folder']) ? $this->settings['folder'] : 'user/';

	    if ($this->language === false)
		    $this->settings['language'] = Yii::app()->language;
	    else
		    $this->settings['language'] = $this->language;

        if (!in_array($this->settings['language'], self::$languages)) {
            $lang = false;
            foreach (self::$languages as $i) {
                if (strpos($this->settings['language'], $i))
                    $lang = $i;
            }
            if ($lang !== false)
                $this->settings['language'] = $lang;
            else
                $this->settings['language'] = 'en';
        }

        $this->settings['language'] = strtr($this->settings['language'], '_', '-');
	    $this->settings['language_url'] = $this->webformaTynimceAssetsDir.'/langs/'.$this->settings['language'].'.js';

        if ($this->spellcheckerUrl !== false) {
            $this->settings['plugins'][] = 'spellchecker';
            $this->settings['spellchecker_rpc_url'] = CHtml::normalizeUrl($this->spellcheckerUrl);
        }

	    $this->settings['content_css'] = $this->webformaTynimceAssetsDir.'/css/tinymce.css';
	    $assets = Yii::app()->getTheme()->getAssetsUrl();

	    if($this->useCss){
		    $styleFile = Yii::app()->getCache()->get('styleFile');
		    if(!$styleFile){
			    $styleFile = file_exists(Yii::app()->getTheme()->getBasePath().'/web/css/style.min.css') ? 'style.min.css' : 'style.css';
			    Yii::app()->getCache()->set('styleFile', $styleFile);
		    }
		    $this->settings['content_css'] .= ','.$assets.'/css/'.$styleFile;
	    }
    }

    public function run()
    {
        list($name, $id) = $this->resolveNameID();
        if (isset($this->htmlOptions['id']))
            $id = $this->htmlOptions['id'];
        else
            $this->htmlOptions['id'] = $id;
        if (isset($this->htmlOptions['name']))
            $name = $this->htmlOptions['name'];

        if($this->selector){
	        if(!($this->htmlOptions['class'])){
		        $this->htmlOptions['class'] = $this->selector;
	        } else {
		        $this->htmlOptions['class'] .= ' ' . $this->selector;
	        }
        }

        if (isset($this->model)) {
            echo CHtml::textArea($name, CHtml::resolveValue($this->model, $this->attribute), $this->htmlOptions);
        } else {
            echo CHtml::textArea($name, $this->value, $this->htmlOptions);
        }
        $this->registerScripts($id);
    }

    private function registerScripts($id)
    {
        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');

        $cs->registerScriptFile($this->assetsDir . '/tinymce.min.js', CClientScript::POS_HEAD);
        $cs->registerScriptFile($this->webformaTynimceAssetsDir . '/plugins/image/plugin.min.js', CClientScript::POS_HEAD);
        $cs->registerScriptFile($this->assetsDir . '/jquery.tinymce.min.js', CClientScript::POS_HEAD);

        if ($this->fileManager !== false) {
        	$fmDir = Yii::getPathOfAlias('public.responsivefm');
        	if(!is_dir($fmDir)){
		        CFileHelper::createDirectory($fmDir, 655);
	        }
			if(!is_file($fmDir.DIRECTORY_SEPARATOR.'dialog.php')){
				CFileHelper::copyDirectory(Yii::getPathOfAlias('application.modules.webforma.extensions.tinymce.responsivefm'), $fmDir);
				file_put_contents($fmDir.DIRECTORY_SEPARATOR.'.gitignore', "*\n!.gitignore");
			}
            $this->settings = array_merge($this->settings, [
                'external_filemanager_path' => '/responsivefm/',
                'filemanager_title' => 'Responsive Filemanager',
                'external_plugins' => [
                    'filemanager' => '/responsivefm/plugin.min.js',
                    'responsivefilemanager' => $this->webformaTynimceAssetsDir.'/plugins/responsivefilemanager/plugin.min.js',
                ],
                'filemanager_access_key' => $this->getAccessKey(),
	            'file_picker_callback' => 'js:function(cb,value,meta){var width=window.innerWidth-30;var height=window.innerHeight-60;if(width>1800)width=1800;if(height>1200)height=1200;if(width>600){var width_reduce=(width-20)%138;width=width-width_reduce+10;}var urltype=2;if(meta.filetype==\'image\'){urltype=1;}if(meta.filetype==\'media\'){urltype=3;}var title="RESPONSIVE FileManager";if(typeof this.settings.filemanager_title!=="undefined"&&this.settings.filemanager_title){title=this.settings.filemanager_title;}var akey="key";if(typeof this.settings.filemanager_access_key!=="undefined"&&this.settings.filemanager_access_key){akey=this.settings.filemanager_access_key;}var sort_by="";if(typeof this.settings.filemanager_sort_by!=="undefined"&&this.settings.filemanager_sort_by){sort_by="&sort_by="+this.settings.filemanager_sort_by;}var descending="false";if(typeof this.settings.filemanager_descending!=="undefined"&&this.settings.filemanager_descending){descending=this.settings.filemanager_descending;}var fldr="";if(typeof this.settings.filemanager_subfolder!=="undefined"&&this.settings.filemanager_subfolder){fldr="&fldr="+this.settings.filemanager_subfolder;}var crossdomain="";if(typeof this.settings.filemanager_crossdomain!=="undefined"&&this.settings.filemanager_crossdomain){crossdomain="&crossdomain=1";if(window.addEventListener){window.addEventListener(\'message\',filemanager_onMessage,false);}else{window.attachEvent(\'onmessage\',filemanager_onMessage);}}tinymce.activeEditor.windowManager.open({title:title,file:this.settings.external_filemanager_path+\'dialog.php?type=\'+urltype+\'&descending=\'+descending+sort_by+fldr+crossdomain+\'&lang=\'+this.settings.language+\'&akey=\'+akey,width:width,height:height,resizable:true,maximizable:true,inline:1},{setUrl:function(url){cb(url);}});}',
            ]);

        }
	    $this->settings['setup'] = 'js:function(editor){
		    editor.addButton("clear_format", {
			    tooltip: "Очистить форматирование",
	            text: "F",
	            onClick: function() { editor.execCommand("mceToggleFormat", false, "p"); },
	            onPostRender: function() {
	                var self = this, setup = function() {
	                    editor.formatter.formatChanged(name, function(state) {
	                        self.active(state);
	                    });
	                };
	                editor.formatter ? setup() : editor.on("init", setup);
	            }
		    })
	    }';
        $settings = CJavaScript::encode($this->settings);

	    if($this->selector){
		    $cs->registerScript("tinyMce_init", "$('.".$this->selector."').tinymce({$settings});");
	    } else {
		    $cs->registerScript("{$id}_tinyMce_init", "$('#{$id}').tinymce({$settings});");
	    }
    }

	/**
	 * @return bool|string
	 */
	public function getAccessKey(){
		$file = Yii::app()->getRuntimePath().'/key';
		$key = '';
		if(is_file($file)){
			$key = file_get_contents($file);
		}

		if(!$key){
			$key = sha1(Yii::app()->getSecurityManager()->generateRandomString(32));
			file_put_contents($file, $key);
		}

		return $key;
	}
}
