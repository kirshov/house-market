<?php
\Yii::import('application.modules.extensions.tinymce.actions.*');

class TinyMceBackendController extends webforma\components\controllers\BackController
{
	public function actions()
	{
		return array(
			'compressor' => array(
				'class' => 'TinyMceCompressorAction',
				'settings' => array(
					'compress' => true,
					'disk_cache' => true,
				)
			),
			'spellchecker' => array(
				'class' => 'TinyMceSpellcheckerAction',
			),
		);
	}

	public function actionMakePreview(){
		$request = Yii::app()->getRequest();
		$width = $request->getParam('width');
		$height = $request->getParam('height');
		$image = $request->getParam('image');

		$webRoot = Yii::getPathOfAlias('webroot');

		if ($width || $height) {
			$thumbnailer = Yii::app()->thumbnailer;
			$uploadPath = Yii::app()->getModule('webforma')->uploadPath;
			$thumbnailer->setBaseUrl('/'.$uploadPath. '/' . $thumbnailer->thumbDir);
			echo Yii::app()->thumbnailer->thumbnail(
				$webRoot.$image,
				$uploadPath,
				$width,
				$height,
				false
			);
		} else {
			echo Yii::app()->createUrl($image);
		}
		Yii::app()->end();
	}
}