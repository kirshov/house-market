<?php
echo <<<EOF
<?php
\$form = \$this->beginWidget(
    'bootstrap.widgets.TbActiveForm', [
        'id' => '{$this->class2id($this->modelClass)}-form',
	 	'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'type' => 'vertical',
        'htmlOptions' => ['enctype' => 'multipart/form-data', 'class' => 'well sticky'],
        'clientOptions' => [
            'validateOnSubmit' => true,
        ],
    ]
);
?>\n
EOF;
?>

<div class="alert alert-info">
    <?=  "<?=  Yii::t('{$this->getModuleTranslate()}', 'Поля, отмеченные'); ?>\n"; ?>
    <span class="required">*</span>
    <?=  "<?=  Yii::t('{$this->getModuleTranslate()}', 'обязательны.'); ?>\n"; ?>
</div>

<?=  "<?=  \$form->errorSummary(\$model); ?>\n"; ?>

<?php
foreach ($this->tableSchema->columns as $column) {
    if ($column->autoIncrement) {
        continue;
    }

    $activeRow = $this->generateActiveGroup($this->modelClass, $column);
    echo <<<EOF
    <div class="row">
        <div class="col-sm-6">
            <?=  {$activeRow}; ?>
        </div>
    </div>\n
EOF;
}
?>

<?php
echo <<<EOF
<div class="buttons">
    <?php \$this->widget(
        'bootstrap.widgets.TbButton', [
            'buttonType' => 'submit',
            'context'    => 'primary',
            'label'      => Yii::t('{$this->getModuleTranslate()}', 'Сохранить {$this->vin} и продолжить'),
        ]
    ); ?>
    <?php \$this->widget(
        'bootstrap.widgets.TbButton', [
            'buttonType' => 'submit',
            'htmlOptions'=> ['name' => 'submit-type', 'value' => 'index'],
            'label'      => Yii::t('{$this->getModuleTranslate()}', 'Сохранить {$this->vin} и закрыть'),
        ]
    ); ?>\n
</div>
EOF;
?>

<?=  "<?php \$this->endWidget(); ?>"; ?>
