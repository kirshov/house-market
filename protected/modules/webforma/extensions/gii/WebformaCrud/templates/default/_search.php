<?php
echo <<<EOF
<?php
\$form = \$this->beginWidget(
    'bootstrap.widgets.TbActiveForm', [
        'action'      => Yii::app()->createUrl(\$this->route),
        'method'      => 'get',
        'type'        => 'vertical',
        'htmlOptions' => ['class' => 'well'],
    ]
);
?>\n
EOF;
?>

<fieldset>
    <div class="row">
        <?php foreach ($this->tableSchema->columns as $column) {
            $field = $this->generateInputField($this->modelClass, $column);
            if (strpos($field, 'password') !== false) {
                continue;
            }

            $activeRow = $this->generateActiveGroup($this->modelClass, $column);
            echo <<<EOF
<div class="col-sm-3">
            <?=  {$activeRow}; ?>
        </div>\n\t\t
EOF;
        } ?>
    </div>
</fieldset>

<?php
echo <<<EOF
    <?php \$this->widget('bootstrap.widgets.TbButton', [
		'context'     => 'primary',
		'encodeLabel' => false,
		'buttonType'  => 'submit',
		'label'       => '<i class="fa fa-search">&nbsp;</i> ' . Yii::t('{$this->getModuleTranslate()}', 'Искать {$this->vin}'),
	]); ?>\n

	<?php \$this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'reset',
		'context'    => 'danger',
		'encodeLabel' => false,
		'label'       => '<i class="fa fa-times">&nbsp;</i> Очистить форму',
	]); ?>\n
EOF;
?>

<?=  "<?php \$this->endWidget(); ?>"; ?>