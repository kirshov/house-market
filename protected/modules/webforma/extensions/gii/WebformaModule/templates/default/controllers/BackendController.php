<?=  "<?php\n"; ?>
/**
* <?=  ucfirst($this->moduleID); ?>BackendController контроллер для <?=  $this->moduleID; ?> в панели управления
*/

class <?=  ucfirst($this->moduleID); ?>BackendController extends \webforma\components\controllers\BackController
{
    /**
     * Действие "по умолчанию"
     *
     * @return void
     */
	public function actionIndex()
	{
		$this->render('index');
	}
}