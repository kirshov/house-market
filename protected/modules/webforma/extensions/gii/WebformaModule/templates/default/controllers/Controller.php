<?=  "<?php\n"; ?>
/**
* <?=  ucfirst($this->moduleID); ?>Controller контроллер для <?=  $this->moduleID; ?> на публичной части сайта
*/

class <?=  ucfirst($this->moduleID); ?>Controller extends \webforma\components\controllers\FrontController
{
    /**
     * Действие "по умолчанию"
     *
     * @return void
     */
    public function actionIndex()
    {
        $this->render('index');
    }
}