<?=  "<?php\n"; ?>
/**
 * Файл настроек для модуля <?=  $this->moduleID."\n"; ?>
 */
return [
    'module'    => [
        'class' => 'application.modules.<?=  $this->moduleID; ?>.<?=  $this->moduleClass; ?>',
    ],
    'import'    => [],
    'component' => [],
    'rules'     => [
        '/<?=  $this->moduleID; ?>' => '<?=  $this->moduleID; ?>/<?=  $this->moduleID; ?>/index',
    ],
];