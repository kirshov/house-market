<?=  "<?php\n"; ?>
/**
* Отображение для <?=  $this->moduleID; ?>Backend/index
**/
$this->breadcrumbs = [
    Yii::t('<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>', '<?=  $this->moduleID; ?>') => ['/<?=  $this->moduleID; ?>/<?=  $this->moduleID; ?>Backend/index'],
    Yii::t('<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>', 'Index'),
];

$this->pageTitle = Yii::t('<?=  $this->moduleClass; ?>.<?=  $this->moduleID; ?>', '<?=  $this->moduleID; ?> - index');

$this->menu = $this->getModule()->getNavigation();
?>