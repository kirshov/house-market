<?php
/**
 * WBackAccessControl фильтр, контролирующий доступ в панель управления
 */
namespace webforma\filters;

use CAccessControlFilter;
use CHttpException;
use Yii;
use webforma\components\WebModule;

/**
 * Class WBackAccessControl
 * @package webforma\filters
 */
class WBackAccessControl extends CAccessControlFilter
{
    /**
     * @param \CFilterChain $filterChain
     * @return bool
     * @throws CHttpException
     */
    public function preFilter($filterChain)
    {
        $ips = $filterChain->controller->webforma->getAllowedIp();

        if (!empty($ips) && !in_array(Yii::app()->getRequest()->getUserHostAddress(), $ips)) {
            throw new CHttpException(404);
        }

        Yii::app()->getUser()->loginUrl = ['/user/account/backendlogin'];

        if (Yii::app()->getUser()->isGuest) {
            if ($filterChain->controller->webforma->hidePanelUrls == WebModule::CHOICE_YES) {
                throw new CHttpException(404);
            }
            Yii::app()->getUser()->setReturnUrl(Yii::app()->getRequest()->getUrl());
            $filterChain->controller->redirect(['/user/account/backendlogin']);
        }

        if (Yii::app()->getUser()->isSuperUser()) {
            return true;
        }

        // если пользователь авторизован, но не администратор, перекинем его на страницу для разлогиневшегося пользователя
        $filterChain->controller->redirect(Yii::app()->createAbsoluteUrl(Yii::app()->getModule('user')->logoutSuccess));
    }
}
