<?php
/**
 * WFrontAccessControl фильтр, контроллирующий доступ к публичной части сайта
 */
namespace webforma\filters;

use CAccessControlFilter;
use Yii;

/**
 * Class WFrontAccessControl
 * @package webforma\filters
 */
class WFrontAccessControl extends CAccessControlFilter
{
    /**
     * @param \CFilterChain $filterChain
     * @return bool
     */
    public function preFilter($filterChain)
    {
        if (Yii::app()->getUser()->isAuthenticated()) {
            return true;
        }

        Yii::app()->getUser()->setReturnUrl(Yii::app()->getRequest()->getUrl());

        $filterChain->controller->redirect(['/user/account/login']);
    }
}
