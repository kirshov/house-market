<?php

/**
 * Class ModuleBackendController
 * @since 0.8
 */
class ModulesBackendController extends webforma\components\controllers\BackController
{
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['deny',],
        ];
    }

    /**
     *
     */
    public function actionConfigUpdate()
    {
        if (!Yii::app()->getRequest()->getIsPostRequest()) {
            throw new CHttpException(404);
        }

        $module = Yii::app()->getRequest()->getPost('module');

        if (empty($module) || !Yii::app()->hasModule($module)) {
            throw new CHttpException(404);
        }

        if (Yii::app()->moduleManager->updateModuleConfig(Yii::app()->getModule($module))) {
            Yii::app()->ajax->success();
        }

        Yii::app()->ajax->failure();
    }

    /**
     * Действие для управления модулями:
     *
     * @throws CHttpException
     *
     * @return string json-data
     **/
    public function actionModuleStatus()
    {
        if (!Yii::app()->getRequest()->getIsPostRequest() || !Yii::app()->getRequest()->getIsAjaxRequest()) {
            throw new CHttpException(404, Yii::t('WebformaModule.webforma', 'Page was not found!'));
        }

        /**
         * Получаем название модуля и проверяем,
         * возможно модуль необходимо подгрузить
         **/
        if (($name = Yii::app()->getRequest()->getPost('module'))
            && ($status = Yii::app()->getRequest()->getPost('status')) !== null
            && (($module = Yii::app()->getModule($name)) === null || $module->canActivate())
        ) {
            $module = Yii::app()->moduleManager->getCreateModule($name);
        } /**
         * Если статус неизвестен - ошибка:
         **/
        elseif (!isset($status) || !in_array($status, [0, 1, 2])) {
            Yii::app()->ajax->failure(Yii::t('WebformaModule.webforma', 'Status for handler is no set!'));
        }

        /**
         * Если всё в порядке - выполняем нужное действие:
         **/
        if (isset($module) && !empty($module)) {
            $result = false;
            try {
                switch ($status) {
                    case 0:
                        if ($module->getIsActive()) {
                            $module->getDeActivate();
                            $message = Yii::t('WebformaModule.webforma', 'Module disabled successfully!');
                        } else {
                            $module->getUnInstall();
                            $message = Yii::t('WebformaModule.webforma', 'Module uninstalled successfully!');
                        }
                        $result = true;
                        break;

                    case 1:

                        if ($module->getIsInstalled()) {
                            $module->getActivate();
                            $message = Yii::t('WebformaModule.webforma', 'Module enabled successfully!');
                        } else {
                            $module->getInstall();
                            $message = Yii::t('WebformaModule.webforma', 'Module installed successfully!');
                        }
                        $result = true;
                        break;
                    case 2:
                        $message = ($result = Yii::app()->moduleManager->updateModuleConfig($module))
                            ? Yii::t('WebformaModule.webforma', 'Settings file "{n}" updated successfully!', $name)
                            : Yii::t(
                                'WebformaModule.webforma',
                                'There is en error when trying to update "{n}" file module!',
                                $name
                            );
                        Yii::app()->getUser()->setFlash(
                            $result ? webforma\widgets\WFlashMessages::SUCCESS_MESSAGE : webforma\widgets\WFlashMessages::ERROR_MESSAGE,
                            $message
                        );
                        break;
                    default:
                        $message = Yii::t('WebformaModule.webforma', 'Unknown action was checked!');
                        break;
                }
                if (in_array($status, [0, 1, 2])) {
	                \TaggedCache\TaggingCacheHelper::deleteTag($name);
                }
            } catch (Exception $e) {
                $message = $e->getMessage();
            }

            /**
             * Возвращаем ответ:
             **/
            $result === true
                ? Yii::app()->ajax->success($message)
                : Yii::app()->ajax->failure($message);

        } else {
            /**
             * Иначе возвращаем ошибку:
             **/
            Yii::app()->ajax->failure(Yii::t('WebformaModule.webforma', 'Module was not found or it\'s enabling finished'));
        }
    }

}
