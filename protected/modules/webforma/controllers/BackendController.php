<?php
/**
 * Главный контроллер админ-панели:
 **/
use webforma\models\Settings;

/**
 * Class BackendController
 */
class BackendController extends webforma\components\controllers\BackController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow',
				'actions' => ['AjaxFileUpload', 'AjaxImageUpload', 'transliterate', 'FlushDumpSettings', 'Ajaxflush'],
				'roles' => ['editor'],
			],
			['allow',
				'actions' => [
					'modulesettings'
				],
				'roles' => [
					'Target.TargetBackend.Update',
					'Webforma.WebformaBackend.Update',
				],
			],
            ['allow', 'actions' => ['index']],
            ['allow', 'actions' => ['error']],
            ['deny',],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'AjaxFileUpload' => [
                'class' => 'webforma\components\actions\WAjaxFileUploadAction',
                'maxSize' => $this->module->maxSize,
                'mimeTypes' => $this->module->mimeTypes,
                'types' => $this->module->allowedExtensions,
            ],
            'AjaxImageUpload' => [
                'class' => 'webforma\components\actions\WAjaxImageUploadAction',
                'maxSize' => $this->module->maxSize,
                'mimeTypes' => $this->module->mimeTypes,
                'types' => $this->module->allowedExtensions,
            ],
        ];
    }

    /**
     * Экшен главной страницы панели управления:
     *
     * @return void
     **/
    public function actionIndex()
    {
        $this->render('index', Yii::app()->moduleManager->getModules(false, false));
    }

    /**
     * Экшен настройки модулей (список):
     *
     * @return void
     **/
    public function actionSettings()
    {
        $this->hideSidebar = true;
        $this->render('settings', Yii::app()->moduleManager->getModules(false, true));
    }

    /**
     * Экшен сброса кеш-файла настроек:
     *
     * @throws CHttpException
     * @return void
     **/
    public function actionFlushDumpSettings()
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest() == false) {
            throw new CHttpException(404, Yii::t('WebformaModule.webforma', 'Page was not found!'));
        }

        if (!Yii::app()->configManager->isCached()) {
            Yii::app()->ajax->failure(
                Yii::t('WebformaModule.webforma', 'There is no cached settings')
            );
        }

        $message = [
            'success' => Yii::t('WebformaModule.webforma', 'Settings cache was reset successfully'),
            'failure' => Yii::t('WebformaModule.webforma', 'There was an error when processing the request'),
        ];

        try {

            $result = Yii::app()->configManager->flushDump();

        } catch (Exception $e) {
            Yii::app()->ajax->failure(
                Yii::t(
                    'WebformaModule.webforma',
                    'There is an error: {error}',
                    [
                        '{error}' => implode('<br />', (array)$e->getMessage()),
                    ]
                )
            );
        }

        $action = $result == false
            ? 'failure'
            : 'success';

        Yii::app()->ajax->$action($message[$action]);
    }

    /**
     * Формирует поле для редактирование параметра модуля
     * @param \webforma\components\WebModule $module
     * @param $param
     * @return string
     */
    private function getModuleParamRow(\webforma\components\WebModule $module, $param)
    {
        $editableParams = $module->getEditableParams();
        if(empty($editableParams)){
	        throw new CHttpException(404, Yii::t('WebformaModule.webforma', 'Setting page for this module is not available!'));
        }
        $moduleParamsLabels = CMap::mergeArray($module->getParamsLabels(), $module->getDefaultParamsLabels());
        $labelOptions = [];
        if($editableParams[$param]['input-type'] == 'checkbox'){
	        $labelOptions['class'] = 'for-checkbox';
        }

        $res = '';
        if($moduleParamsLabels[$param] !== null){
			$res = CHtml::label($moduleParamsLabels[$param], $param, $labelOptions);
		}

        $needFormWrap = true;

        /* если есть ключ в массиве параметров, то значит этот параметр выпадающий список в вариантами */
        if (array_key_exists($param, $editableParams)) {
        	if(isset($editableParams[$param]['input-type']) && $editableParams[$param]['input-type'] != 'select'){
        		$htmlOptions = (array) $editableParams[$param]['htmlOptions'];
        		switch ($editableParams[$param]['input-type']){
			        case 'html':
			        	$res .= CHtml::tag('div', $htmlOptions, $editableParams[$param]['html']);
			        	break;
					case 'image':
						$res .= '<div class="image-in-settings">';
							$res .= '<div class="form-wrapper">';
								$htmlOptions['class'] .= ' form-control';
								$res .= CHtml::fileField($param, $module->{$param}, $htmlOptions);
								$res .= '<p style="margin-top: 5px;">';
									$res .= CHtml::checkBox($param.'_delete', false, ['id' => $param.'_delete', 'class' => 'one-checkbox']);
									$res .= CHtml::label('Удалить изображение', $param.'_delete', ['class' => 'for-checkbox']);
								$res .= '</p>';
							$res .= '</div>';
							$res .= '<div class="image-wrapper">';
								if($module->{$param}){
									$res .= CHtml::image(Yii::app()->createAbsoluteUrl($editableParams[$param]['path']).$module->{$param}, '', [
										'style' => 'max-height:100px;max-width:100px;',
									]);
								}
							$res .= '</div>';
						$res .= '</div>';
						break;
			        case 'textarea':
				        $htmlOptions['class'] .= ' form-control';
				        $res .= CHtml::textArea($param, $module->{$param}, $htmlOptions);
				        break;
			        case 'checkbox':
				        $htmlOptions['class'] .= ' one-checkbox';
				        if(!isset($htmlOptions['uncheckValue'])){
							$htmlOptions['uncheckValue'] = 0;
						}
				        $res .= CHtml::checkBox($param, $module->{$param}, $htmlOptions);
				        break;
			        case 'editor':
				        $res .= $this->widget(Yii::app()->getModule('webforma')->getVisualEditor(), [
							'name' => $param,
							'value' => $module->{$param},
						], true);
				        break;
			        case 'widget':
				        $needFormWrap = $editableParams[$param]['needFormWrap'] ?: false;
				        $res .= $this->widget($editableParams[$param]['widget'], [
					        'name' => $param,
					        'value' => $module->{$param},
					        'options' => $editableParams[$param]['options']
				        ], true);
				        break;
			        case 'input':
			        case 'text':
			        default:
				        $htmlOptions['class'] .= ' form-control';
				        $res .= CHtml::textField($param, $module->{$param}, $htmlOptions);
		        }

	        } else {
        		$_data = $editableParams[$param]['input-type'] ? $editableParams[$param]['items'] : $editableParams[$param];
		        $res .= CHtml::dropDownList($param, $module->{$param}, $_data, [
		        	'class' => 'form-control',
					'empty' => isset($editableParams[$param]['empty']) ? $editableParams[$param]['empty'] : Yii::t('WebformaModule.webforma', '--choose--')
				]);
	        }
        } else {
            $res .= CHtml::textField($param, $module->{$param}, ['class' => 'form-control']);
        }

        if($editableParams[$param]['hint']){
	        $res .= '<div class="help-block">'.$editableParams[$param]['hint'].'</div>';
        }

        if($needFormWrap){
        	$res = '<div class="row"><div class="col-xs-12"><div class="form-group">'.$res.'</div></div></div>';
        }

        return $res;
    }

    /**
     * Экшен отображения настроек модуля:
     *
     * @throws CHttpException
     *
     * @param string $module - id-модуля
     *
     * @return void
     **/
    public function actionModulesettings($module)
    {
        if (!($module = Yii::app()->getModule($module))) {
            throw new CHttpException(404, Yii::t('WebformaModule.webforma', 'Setting page for this module is not available!'));
        }

        $editableParams = $module->getEditableParams();
        $paramGroups = $module->getEditableParamsGroups();
        $tabs = $module->getEditableParamsTabs();

        $groups = [];
		if($module->adminSettingsAsTab){
			foreach ($paramGroups as $name => $group) {
				$title = isset($group['label']) ? $group['label'] : $name;
				$groups[$name][$title] = [];
				if (isset($group['items'])) {
					foreach ((array)$group['items'] as $item) {
						/*удаляем элементы, которые были в группах*/
						if (($key = array_search($item, $editableParams)) !== false) {
							unset($editableParams[$key]);
						} else {
							unset($editableParams['item']);
						}
						unset($editableParams[$item]);
						$groups[$name][$title][] = $this->getModuleParamRow($module, $item);
					}
				}
			}
		} else {
			foreach ($paramGroups as $name => $group) {
				$title = isset($group['label']) ? $group['label'] : $name;
				$groups[$title] = [];
				if (isset($group['items'])) {
					foreach ((array)$group['items'] as $item) {
						/*удаляем элементы, которые были в группах*/
						if (($key = array_search($item, $editableParams)) !== false) {
							unset($editableParams[$key]);
						} else {
							unset($editableParams['item']);
						}
						unset($editableParams[$item]);
						$groups[$title][] = $this->getModuleParamRow($module, $item);
					}
				}
			}

			/* если остались параметры без групп, то засунем их в одну группу */
			if ($editableParams) {
				$title = Yii::t('WebformaModule.webforma', 'Other');
				$groups[$title] = [];
				foreach ((array)$editableParams as $key => $params) {
					/* из-за формата настроек параметров название атрибута будет или ключом, или значением */
					$groups[$title][] = $this->getModuleParamRow($module, is_string($key) ? $key : $params);
				}
			}
		}



        $this->render('modulesettings', ['module' => $module, 'tabs' => $tabs, 'groups' => $groups,]);
    }

    /**
     * Экшен сохранения настроек модуля:
     *
     * @throws CHttpException
     *
     * @return void
     **/
    public function actionSaveModulesettings()
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            if (!($moduleId = Yii::app()->getRequest()->getPost('module_id'))) {
                throw new CHttpException(404, Yii::t('WebformaModule.webforma', 'Page was not found!'));
            }

            if (!($module = Yii::app()->getModule($moduleId))) {
                throw new CHttpException(
                    404, Yii::t(
                    'WebformaModule.webforma',
                    'Module "{module}" was not found!',
                    ['{module}' => $moduleId]
                )
                );
            }

            if ($this->saveParamsSetting($moduleId, $module->getEditableParamsKey())) {
                Yii::app()->getUser()->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t(
                        'WebformaModule.webforma',
                        'Settings for "{module}" saved successfully!',
                        [
                            '{module}' => $module->getName(),
                        ]
                    )
                );
                $module->getSettings(true);
            } else {
                Yii::app()->getUser()->setFlash(
                    webforma\widgets\WFlashMessages::ERROR_MESSAGE,
                    Yii::t('WebformaModule.webforma', 'There is an error when saving settings!')
                );
            }
            $this->redirect($module->getSettingsUrl());
        }
        throw new CHttpException(404, Yii::t('WebformaModule.webforma', 'Page was not found!'));
    }

    /**
     * Метода сохранения настроек модуля:
     *
     * @param string $moduleId - идетификтор метода
     * @param array $params - массив настроек
     *
     * @return bool
     **/
    public function saveParamsSetting($moduleId, $params)
    {
        $paramValues = [];
        $paramTypes = [];

		$fields = Yii::app()->getModule($moduleId)->getEditableParams();
        // Перебираем все параметры модуля
        foreach ($params as $param_name) {
            $param_value = Yii::app()->getRequest()->getPost($param_name, null);
            // Если параметр есть в post-запросе добавляем его в массив
            if ($param_value !== null) {
            	if(is_array($param_value)){
		            $param_value = json_encode($param_value);
		            $paramTypes[$param_name] = Settings::TYPE_JSON;
	            }
                $paramValues[$param_name] = $param_value;
            }

			if(Yii::app()->getRequest()->getPost($param_name.'_delete') !== null){
				$oldValue = Yii::app()->getModule($moduleId)->{$param_name};
				if($fields[$param_name]['path'] && $oldValue){
					$path = Yii::getPathOfAlias('public.'.$fields[$param_name]['path']);
					if(is_file($path.'/'.$oldValue)){
						unlink($fields[$param_name]['path'].'/'.$oldValue);
					}
				}
				$paramValues[$param_name] = null;
			}
        }

        if($_FILES){
        	foreach ($_FILES as $name => $file){
        		if(Yii::app()->getRequest()->getPost($name.'_delete') !== null){
        			continue;
				}
				$fileInstance = CUploadedFile::getInstanceByName($name);
        		$path = Yii::getPathOfAlias('public.'.$fields[$name]['path']);

        		if(!is_dir($path)){
        			CFileHelper::createDirectory($path);
				}

				if(is_object($fileInstance)){
					$fileName = $fields[$name]['default-name'] ?: $fileInstance->getName();
					if($fileInstance->saveAs($path.'/'.$fileName)){
						$paramValues[$name] = $fileName;
					}
				}
			}
		}

        // Запускаем сохранение параметров
        return Settings::saveModuleSettings($moduleId, $paramValues, $paramTypes);
    }

    /**
     * Обновленик миграций модуля
     *
     * @param string $name - id модуля
     *
     * @return nothing
     */
    public function actionModupdate($name = null)
    {
        if ($name) {
            if (($module = Yii::app()->getModule($name)) == null) {
                $module = Yii::app()->moduleManager->getCreateModule($name);
            }

            if ($module->getIsInstalled()) {

                $updates = Yii::app()->migrator->checkForUpdates([$name => $module]);

                if (Yii::app()->getRequest()->getIsPostRequest()) {

                    Yii::app()->migrator->updateToLatest($name);

                    Yii::app()->getUser()->setFlash(
                        webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                        Yii::t('WebformaModule.webforma', 'Module was updated their migrations!')
                    );
                    $this->redirect(["index"]);
                } else {
                    $this->render('modupdate', ['updates' => $updates, 'module' => $module]);
                }
            } else {
                Yii::app()->getUser()->setFlash(
                    webforma\widgets\WFlashMessages::ERROR_MESSAGE,
                    Yii::t('WebformaModule.webforma', 'Module doesn\'t installed!')
                );
            }
        } else {
            Yii::app()->getUser()->setFlash(
                webforma\widgets\WFlashMessages::ERROR_MESSAGE,
                Yii::t('WebformaModule.webforma', 'Module name is not set!')
            );

            $this->redirect(
                Yii::app()->getRequest()->getUrlReferrer() !== null ? Yii::app()->getRequest()->getUrlReferrer(
                ) : ["/webforma/backend"]
            );
        }
    }

    /**
     * Страничка для отображения ссылок на ресурсы для получения помощи
     *
     * @since 0.4
     * @return nothing
     */
    public function actionHelp()
    {
        $this->render('help');
    }

    /**
     * Метод очистки ресурсов (assets)
     *
     * @return boolean
     **/
    private function _cleanAssets()
    {
        /*if(Yii::app()->getAssetManager()->linkAssets) {
            return true;
        }*/

        try {
            $dirsList = glob(Yii::app()->assetManager->getBasePath().DIRECTORY_SEPARATOR.'*', GLOB_ONLYDIR);
            if (is_array($dirsList)) {
                foreach ($dirsList as $item) {
                    webforma\helpers\WFile::rmDir($item);
                }
            }

            return true;
        } catch (Exception $e) {
            Yii::app()->ajax->failure(
                $e->getMessage()
            );
        }
    }


    /**
     * @throws CHttpException
     */
    public function actionAjaxflush()
    {
        if (!Yii::app()->getRequest()->getIsPostRequest()
            || !Yii::app()->getRequest()->getIsAjaxRequest()
            || ($method = Yii::app()->getRequest()->getPost('method')) === null
        ) {
            throw new CHttpException(404, Yii::t('WebformaModule.webforma', 'Page was not found!'));
        }

        switch ($method) {
            case 'cacheAll':

                try {
                    Yii::app()->getCache()->flush();
                    $this->_cleanAssets();
                    if (Yii::app()->configManager->isCached()) {
                        Yii::app()->configManager->flushDump();
                    }
                    Yii::app()->ajax->success(
                        Yii::t('WebformaModule.webforma', 'Cache cleaned successfully!')
                    );

                } catch (Exception $e) {
                    Yii::app()->ajax->failure(
                        $e->getMessage()
                    );
                }
                break;

            /**
             * Очистка только кеша:
             **/
            case 'cacheFlush':
                try {
                    Yii::app()->getCache()->flush();
                    Yii::app()->ajax->success(
                        Yii::t('WebformaModule.webforma', 'Cache cleaned successfully!')
                    );
                } catch (Exception $e) {
                    Yii::app()->ajax->failure(
                        $e->getMessage()
                    );
                }
                break;
            /**
             * Очистка только ресурсов:
             **/
            case 'assetsFlush':
                if ($this->_cleanAssets()) {
                    Yii::app()->ajax->success(
                        Yii::t('WebformaModule.webforma', 'Assets cleaned successfully!')
                    );
                }
                break;
            /**
             * Очистка ресурсов и кеша:
             **/
            case 'cacheAssetsFlush':
                try {
                    Yii::app()->getCache()->flush();
                    if ($this->_cleanAssets()) {
                        Yii::app()->ajax->success(
                            Yii::t('WebformaModule.webforma', 'Assets and cache cleaned successfully!')
                        );
                    }
                } catch (Exception $e) {
                    Yii::app()->ajax->failure(
                        $e->getMessage()
                    );
                }
                break;
            /**
             * Использован неизвестный системе метод:
             **/
            default:
                Yii::app()->ajax->failure(Yii::t('WebformaModule.webforma', 'Unknown method use in system!'));
                break;
        }
    }

    /**
     *
     */
    public function actionError()
    {
        $error = Yii::app()->getErrorHandler()->error;

        if (empty($error) || !isset($error['code']) || !(isset($error['message']) || isset($error['msg']))) {
            $this->redirect(['index']);
        }

	    $this->render('error', ['error' => $error]);
    }

    /**
     *
     */
    public function actionTransliterate()
    {
        $data = Yii::app()->getRequest()->getParam('data') ?: Yii::app()->getRequest()->getPost('data');

        echo \webforma\helpers\WText::translit($data);
    }
}
