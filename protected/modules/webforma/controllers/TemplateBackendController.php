<?php

class TemplateBackendController extends webforma\components\controllers\BackController
{
	/**
	 * @var array
	 */
	protected $excludedModules = [
		'webforma',
		'notify',
		'social',
		'service',
	];

	/**
	 * @var array
	 */
	protected $excludedFiles = [
		'src',
		'bootstrap.php',
		'ShopThemeEvents.php',
		'views/webforma',
		'views/viewed',
		'web/fonts',
		'web/images',
		'web/plugins',
	];

	/**
	 * @var array
	 */
	protected $labels = [
		'views' => 'Шаблоны',
		'web' => 'Ресурсы',
		'widgets' => 'Виджеты',
		'email' => 'Email',
		'layout' => 'Обший шаблон',
		'home' => 'Обший шаблон',
		'zendsearch' => 'Поиск по сайту',
	];

	/**
	 * @var array
	 */
	protected $tree = [];

	/**
	 * @return array
	 */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['deny',],
        ];
    }

	/**
	 *
	 */
    public function actionIndex(){
		$modules = Yii::app()->moduleManager->getModules();

		$allowedModules = [];
		if($modules['modules']){
			foreach ($modules['modules'] as $name => $item){
				if($item->getCategory() !== false && !isset($this->excludedModules[$name])){
					$allowedModules[$name] = $name;
					$this->labels[$name] = $item->getName();
				}
			}
		}

		$themePath = Yii::app()->getTheme()->getBasePath().'/';
		if($file = Yii::app()->getRequest()->getParam('file')){
			$postFile = Yii::app()->getRequest()->getPost('file');
			$issetFile = false;

			if(is_file($themePath.$file)){
				if($postFile){
					file_put_contents($themePath.$file, $postFile);
					$this->refresh();
				} else {
					$fileContent = file_get_contents($themePath.$file);
				}

				$issetFile = true;
				$assetsDir = Yii::app()->assetManager->publish(Yii::getPathOfAlias('vendor').'/ajaxorg/ace/lib/ace/');
				$mainAssets = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.webforma.views.assets'));
				$cs = Yii::app()->getClientScript();

				$ext = CFileHelper::getExtension($file);
				$modesMap = [
					'js' => 'javascript',
				];
				$mode = $modesMap[$ext] ?: $ext;

				$cs->registerScriptFile($mainAssets.'/js/require.js', CClientScript::POS_END);
				$cs->registerScript('ace-required', 'require.config({paths: {"ace": "'.Yii::app()->createAbsoluteUrl('/').ltrim($assetsDir, '/').'"}});', CClientScript::POS_END);
				$cs->registerScript('act-init', 'require(["ace/ace"], function(ace) {
					var editor = ace.edit("file-content");
					editor.session.setMode("ace/mode/'.$mode.'");
					editor.getSession().on("change", function () {
						$("input.file").val(editor.getSession().getValue());
					});
					editor.setOptions({
						spellcheck: false,
						showPrintMargin: false,
						mergeUndoDeltas: true
					});
				});', CClientScript::POS_END);

			};

		}

    	$this->render('/backend/template-edit', [
			'tree' => $this->getTree(),
			'fileContent' => $fileContent,
			'issetFile' => $issetFile,
			'file' => $file,
		]);
	}

	/**
	 * @return array
	 */
	public function getTree(){
		$themePath = Yii::app()->getTheme()->getBasePath();
		//$tree = Yii::app()->getCache()->get($themePath);
		//$tree = false;
		//if(!$tree){
			$tree = $this->buildTreeFiles($themePath);
		//}
		//Yii::app()->getCache()->set($themePath, $tree, Yii::app()->getModule('webforma')->coreCacheTime);

		return $tree;
	}

	/**
	 * @param $src
	 * @param string $parentDir
	 * @param int $level
	 * @return array
	 * @throws Exception
	 */
	protected function buildTreeFiles($src, $parentDir = '', $level = 1)
	{
		$data = [];
		$folder = opendir($src);
		if ($folder === false){
			throw new Exception('Unable to open directory: '.$src);
		}

		while (($file = readdir($folder)) !== false) {
			if ($file === '.' || $file === '..'){
				continue;
			}

			if($parentDir.$file && in_array($parentDir.$file, $this->excludedFiles)){
				continue;
			}

			if($level == 2 && in_array($file, $this->excludedModules)){
				continue;
			}

			$path = $src.DIRECTORY_SEPARATOR.$file;
			$isFile = is_file($path);

			if(isset($this->labels[$file])){
				$data[$parentDir.$file]['label'] = $this->labels[$file];
			} else {
				$data[$parentDir.$file]['label'] = $file;
			}

			$data[$parentDir.$file]['file'] = $file;
			$data[$parentDir.$file]['not-prepare'] = true;
			if ($isFile) {
				//Helper::var_dump('file = '.$file);
				$data[$parentDir.$file]['type'] = 'file';
				$data[$parentDir.$file]['url'] = Yii::app()->createUrl('/webforma/templateBackend/index', ['file' => $parentDir.$file]);
				$data[$parentDir.$file]['src'] = $src.DIRECTORY_SEPARATOR.$file;
				if(!empty($_GET['file']) && $_GET['file'] == $parentDir.$file){
					$data[$parentDir.$file]['active'] = true;
				}
			} else {
				$data[$parentDir.$file]['url'] = '#';
				$data[$parentDir.$file]['type'] = 'dir';
				$data[$parentDir.$file]['items'] = $this->buildTreeFiles($src.DIRECTORY_SEPARATOR.$file, $parentDir.$file.'/', ($level + 1));

				//$data = CMap::mergeArray($data, $items);
			}
		}
		closedir($folder);
		return $data;
	}
}
