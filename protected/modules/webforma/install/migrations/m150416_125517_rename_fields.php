<?php

class m150416_125517_rename_fields extends webforma\components\DbMigration
{
	public function safeUp()
	{
        $this->renameColumn('{{webforma_settings}}', 'creation_date', 'create_time');
        $this->renameColumn('{{webforma_settings}}', 'change_date', 'update_time');
	}
}