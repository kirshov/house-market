<?php

/**
 * webforma install migration
 * Класс миграций для модуля webforma
 **/
class m130527_154455_webforma_change_unique_index extends webforma\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        //Delete old unique index:
        $this->dropIndex("ux_{{webforma_settings}}_module_id_param_name", '{{webforma_settings}}');

        // Create new unique index:
        $this->createIndex(
            "ux_{{webforma_settings}}_module_id_param_name_user_id",
            '{{webforma_settings}}',
            "module_id,param_name,user_id",
            true
        );
    }

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        //Delete old unique index:
        $this->dropIndex("ux_{{webforma_settings}}_module_id_param_name_user_id", '{{webforma_settings}}');

        // Create new unique index:
        $this->createIndex(
            "ux_{{webforma_settings}}_module_id_param_name",
            '{{webforma_settings}}',
            "module_id,param_name",
            true
        );
    }
}
