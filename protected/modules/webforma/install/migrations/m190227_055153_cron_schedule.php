<?php

class m190227_055153_cron_schedule extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->createTable(
			'{{webforma_cron_schedule}}',
			[
				'id'            => 'pk',
				'module_id'     => 'varchar(100) NOT NULL',
				'task_id'    => 'varchar(100) NOT NULL',
				'frequency'   => 'int(3) NOT NULL',
				'last_run' => 'datetime NOT NULL',
			],
			$this->getOptions()
		);

		$this->createIndex("ix_{{webforma_cron_schedule}}_module_id", '{{webforma_cron_schedule}}', "module_id", false);
		$this->createIndex("ix_{{webforma_cron_schedule}}_task_id", '{{webforma_cron_schedule}}', "task_id", false);
	}

	public function safeDown()
	{

	}
}