<?php

class m160204_195213_change_settings_type extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->alterColumn('{{webforma_settings}}', 'param_value', 'varchar(500) NOT NULL');
	}

	public function safeDown()
	{
		$this->alterColumn('{{webforma_settings}}', 'param_value', 'varchar(255) NOT NULL');
	}
}