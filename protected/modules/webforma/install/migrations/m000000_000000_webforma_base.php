<?php

/**
 * webforma install migration
 * Класс миграций для модуля webforma
 **/
class m000000_000000_webforma_base extends webforma\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        $this->createTable(
            '{{webforma_settings}}',
            [
                'id'            => 'pk',
                'module_id'     => 'varchar(100) NOT NULL',
                'param_name'    => 'varchar(100) NOT NULL',
                'param_value'   => 'varchar(255) NOT NULL',
                'creation_date' => 'datetime NOT NULL',
                'change_date'   => 'datetime NOT NULL',
                'user_id'       => 'integer DEFAULT NULL',
                'type'          => "integer NOT NULL DEFAULT '1'",
            ],
            $this->getOptions()
        );

        //ix
        $this->createIndex(
            "ux_{{webforma_settings}}_module_id_param_name",
            '{{webforma_settings}}',
            "module_id,param_name",
            true
        );
        $this->createIndex("ix_{{webforma_settings}}_module_id", '{{webforma_settings}}', "module_id", false);
        $this->createIndex("ix_{{webforma_settings}}_param_name", '{{webforma_settings}}', "param_name", false);

        //fk
        $this->addForeignKey(
            "fk_{{webforma_settings}}_user_id",
            '{{webforma_settings}}',
            'user_id',
            '{{user_user}}',
            'id',
            'SET NULL',
            'NO ACTION'
        );

    }

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{webforma_settings}}');
    }
}
