<?php

class m190209_130352_change_fields extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->alterColumn('{{webforma_settings}}', 'param_value', 'text');
	}

	public function safeDown()
	{

	}
}