<?php

/**
 * Файл конфигурации модуля
 **/

return [
	'import' => [
		'application.modules.webforma.components.validators.*',
		'application.modules.webforma.components.exceptions.*',
		'application.modules.webforma.extensions.tagcache.*',
		'application.modules.webforma.helpers.*',
		'application.modules.webforma.models.*',
		'application.modules.webforma.widgets.*',
		'application.modules.webforma.controllers.*',
		'application.modules.webforma.components.*',
		'application.modules.webforma.listeners.*',
	],
	'preload' => ['log'],
	'component' => [
		// Массив компонентов, которые требует данный модуль
		// настройки кэширования, подробнее http://www.yiiframework.ru/doc/guide/ru/caching.overview
		'thumbnailer' => [
			'class' => 'webforma\components\image\Thumbnailer',
			'options' => [
				'jpeg_quality' => 100,
				'png_compression_level' => 5,
			],
		],
		// подключение компонента для генерации ajax-ответов
		'ajax' => [
			'class' => 'webforma\components\AsyncResponse',
		],
		'dataContainer' => [
			'class' => 'webforma\components\DataContainer',
		],
		'eventManager' => [
			'events' => [
				'geo.city.change' => [
					['\GeoManagerListener', 'onChangeCity']
				],
				'template.body.end' => [
					['\WebformaTemplateListener', 'footer'],
				],
			],
		],
		'mailMessage' => [
			'class' => 'application.modules.webforma.components.WMailMessage',
		],
	],
	'rules' => [
		'/disable-privacy-window' => '/home/disablePrivacyWindow',
		'/searchCity' => '/home/searchCity',
		'/setCity' => '/home/setCity',
	],
	'module' => [
		'components' => [
			'bootstrap' => [
				'class' => 'application.modules.webforma.components.WFBooster',
				'coreCss' => true,
				'bootstrapCss' => true,
				'responsiveCss' => true,
				'yiiCss' => true,
				'jqueryCss' => true,
				'enableJS' => true,
				'fontAwesomeCss' => false,
				'enableNotifierJS' => false,
				'minify' => false,
				//'forceCopyAssets' => defined('YII_DEBUG') && YII_DEBUG,
			],
		],
	],

	'commandMap' => [
		'webforma' => [
			'class' => 'application.modules.webforma.commands.WebformaCommand',
		],
	],

	'params' => [
		'widgets' => [
			'changeCity' => 'application.modules.webforma.widgets.ChangeCityWidget',
			'socialStatPanel' => 'application.modules.webforma.widgets.PanelSocialStatWidget',
			'tab-settings' => 'application.modules.webforma.widgets.WTabSettings',
		],
	],
];
