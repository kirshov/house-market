<?php

use webforma\components\WebModule;

/**
 * Class CartModule
 */
class CartModule extends WebModule
{
    /**
     * @var string
     */
    public $assetsPath = 'cart.views.assets';

    /**
     * @return array
     */
    public function getDependencies()
    {
        return ['order', 'store'];
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [];
    }

    /**
     * @return bool
     */
    public function getAdminPageLink()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function getIsShowInAdminMenu()
    {
        return false;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('CartModule.cart', 'Cart');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('CartModule.cart', 'Shopping cart in online store');
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('CartModule.cart', 'Store');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-shopping-cart';
    }
}