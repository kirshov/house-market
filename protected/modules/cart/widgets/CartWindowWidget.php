<?php

Yii::import('application.modules.store.models.*');
Yii::import('application.modules.cart.CartModule');

/**
 * Class ShoppingCartWidget
 */
class CartWindowWidget extends webforma\widgets\WWidget
{
    /**
     * @var CartProduct $product
     */
    public $product;

    /**
     * @var string
     */
    public $view = 'default';

	/**
	 * @var
	 */
    public $message;

    /**
     * @throws CException
     */
    public function run()
    {
        $this->render($this->view, ['product' => $this->product]);
    }
}
