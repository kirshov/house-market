<?php

return [
    'module' => [
        'class' => 'application.modules.cart.CartModule',
    ],
    'import' => [
        'application.modules.cart.components.shopping-cart.*',
        'application.modules.cart.events.*',
        'application.modules.cart.models.CartProduct',
    ],
    'component' => [
        'cart' => [
            'class' => 'application.modules.cart.components.shopping-cart.EShoppingCart',
        ],
    ],
	'params'    => [
		'widgets'   => [
			'cart' => 'application.modules.cart.widgets.ShoppingCartWidget',
			'cartWindow' => 'application.modules.cart.widgets.CartWindowWidget',
		],
	],
    'rules' => [
        '/cart' => 'cart/cart/index',
        '/cart/disable-cart-window' => 'cart/cart/disableCartWindow',
        '/cart/<action:\w+>' => 'cart/cart/<action>',
        '/cart/<action:\w+>/<id:\w+>' => 'cart/cart/<action>',
    ],
];
