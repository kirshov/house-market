<?php

use webforma\components\controllers\FrontController;

/**
 * Class CartController
 */
class CartController extends FrontController
{
    /**
     *
     */
    public function actionIndex()
    {
        $request = Yii::app()->getRequest();
        $order = Yii::app()->getRequest()->getPost('Order');
        if($order['is_boc']){
            if(!$request->getIsAjaxRequest() || !$request->getIsPostRequest()){
                throw new CHttpException(404, Yii::t('WebformaModule.webforma', 'Page was not found!'));
            }
            $model = new Order(Order::SCENARIO_BOC);
        } else {
            $model = new Order(Order::SCENARIO_USER);
        }

        if ($request->getIsPostRequest() && $order) {
            $products = Yii::app()->getRequest()->getPost('OrderProduct');
            $coupons = isset($order['couponCodes']) ? $order['couponCodes'] : [];

            if ($model->store($order, $products, Yii::app()->getUser()->getId(), (int)Yii::app()->getModule('order')->defaultStatus)) {
                if(!$order['is_boc']){
                    Yii::app()->cart->clear();
                }

                if (!empty($coupons)) {
                    $model->applyCoupons($coupons);
                }


                Yii::app()->eventManager->fire(OrderEvents::CREATED_HTTP, new OrderEvent($model));

                if($request->getIsAjaxRequest()){
                    Yii::app()->ajax->success('<div class="modal-title">Ваша заявка успешно отправлена!</div> <span>Мы свяжемся с Вами в ближайшее время.</span>');
                } else {
                    Yii::app()->getUser()->setFlash(
                        webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                        Yii::t('OrderModule.order', 'The order created')
                    );

	                Helper::setFlash('orderCreated', 'ok');

                    if (Yii::app()->getModule('order')->showOrder) {
                        $this->redirect(['/order/order/view', 'url' => 'new-' . $model->url]);
                    }

                    $this->redirect(['/store/category/index']);
                }
            } else {
                if($request->getIsAjaxRequest()){
                    Yii::app()->ajax->failure(['errors' => $model->getErrors()]);
                } else {
                    Yii::app()->getUser()->setFlash(
                        webforma\widgets\WFlashMessages::ERROR_MESSAGE,
                        CHtml::errorSummary($model)
                    );
                }
            }
        }
	    Yii::app()->cart->checkProducts();

        $positions = Yii::app()->cart->getPositions();
        if (Yii::app()->getUser()->isAuthenticated()) {
            $user = Yii::app()->getUser()->getProfile();
            $model->setAttributes([
                'name' => $user->getFullName(),
                'email' => $user->email,
                'phone' => $user->phone,
            ]);
        }

        $coupons = [];

        if (Yii::app()->hasModule('coupon')) {

            $couponCodes = Yii::app()->cart->couponManager->coupons;

            foreach ($couponCodes as $code) {
                $coupons[] = Coupon::model()->getCouponByCode($code);
            }
        }

        $returnData = [
	        'positions' => $positions,
	        'order' => $model,
	        'coupons' => $coupons,
	        'uds' => Yii::app()->getUser()->getState('uds-data'),
	        'deliveryTypes' => Yii::app()->hasModule('delivery') ? Delivery::getAll() : [],
	        'paymentSystems' => Yii::app()->hasModule('payment') ? Payment::getAll() : [],
        ];

	    if(Yii::app()->getRequest()->getIsAjaxRequest()){
	    	if(Yii::app()->getRequest()->getParam('refresh')){
			    return $this->renderPartial('_product_table', $returnData);
		    } elseif(Yii::app()->getRequest()->getParam('get-cart')){
			    return $this->renderPartial('_order_form', $returnData);
		    }

        }
        $this->render('index', $returnData);
    }

    /**
     *
     */
	public function actionDisableCartWindow()
	{
		$status = (string) Yii::app()->getRequest()->getParam('status');
		if($status === 'false'){
			\webforma\components\WCookie::removeCookie('disabled-cart-window');
		} else {
			\webforma\components\WCookie::setCookie('disabled-cart-window', $status);
		}
		Yii::app()->end();
	}

    /**
     * @throws CHttpException
     */
    public function actionAdd()
    {
    	if(Yii::app()->getRequest()->getRequestType() == 'GET' && Yii::app()->getRequest()->getParam('offerId') !== null){
    		return $this->addCartFromTurboPage();
		}

        if (!Yii::app()->getRequest()->getIsPostRequest()) {
            throw new CHttpException(404);
        }

        $product = Yii::app()->getRequest()->getPost('Product');

        if (empty($product['id'])) {
            throw new CHttpException(404);
        }

	    /**
	     * @var CartProduct $model
	     */
        $model = CartProduct::model()->published()->findByPk((int)$product['id']);

        if (null === $model) {
            throw new CHttpException(404);
        }

        $variantsId = Yii::app()->getRequest()->getPost('ProductVariant', []);
        $variants = [];
        foreach ((array)$variantsId[$model->id] as $var) {
            if (!$var) {
                continue;
            }
            $variant = ProductVariant::model()->findByPk($var);
            if ($variant && $variant->product_id == $model->id) {
                $variants[] = $variant;
            }
        }
        $model->selectedVariants = $variants;
        $quantity = empty($product['quantity']) ? 1 : (int)$product['quantity'];

		$window = null;
		$message = null;

		if(Yii::app()->getModule('store')->checkCartQuantity){
			if($variant){
				$maxQuantity = $variant->quantity;
				$inStock = $variant->isInStock();
			} else {
				$maxQuantity = $model->quantity;
				$inStock = $model->isInStock();
			}

			if($maxQuantity > 0 || $inStock){
				$cartQuantity = Yii::app()->cart->getProductCount($model);
				if($cartQuantity + $quantity >= $maxQuantity){
					$quantity = $maxQuantity - $cartQuantity;
	                if($quantity > 0){
						//$message = 'Добавлено в количестве '.$quantity.' единиц (макс на складе).';
					} elseif($inStock){
	                    $quantity = 1;
					} else {
						Yii::app()->cart->update($model, $maxQuantity);
						Yii::app()->ajax->failure(['message' => 'Нельзя добавить товара больше, чем есть в наличии']);
					}
				}
			}
		}

        Yii::app()->cart->put($model, $quantity);
        if(!Yii::app()->getRequest()->getIsAjaxRequest()){
        	return $this->redirect(['/cart/cart/index']);
        }

        if(!$window && !Yii::app()->request->cookies['disabled-cart-window']->value && Yii::app()->getRequest()->getParam('no-window', null) === null){
			$window = $this->widget('cartWindow', ['product' => $model, 'message' => $message], true);
        }
        Yii::app()->ajax->success(['window' => $window]);
    }

	/**
	 * @throws CHttpException
	 */
    public function addCartFromTurboPage(){
		$offerId = Yii::app()->getRequest()->getParam('offerId');
		$variants = [];
		if(mb_strpos($offerId, 'var') !== false){
			$variantId = str_replace('var', '', $offerId);
			$variant = ProductVariant::model()->findByPk($variantId);
			if ($variant) {
				$model = CartProduct::model()->published()->findByPk($variant->product_id);
				$variants[] = $variant;
			} else {
				throw new CHttpException(404);
			}
		} else {
			$model = CartProduct::model()->published()->findByPk((int) $offerId);
		}

		if(!$model){
			throw new CHttpException(404);
		}

		if($variants){
			$model->selectedVariants = $variants;
		}
		Yii::app()->cart->put($model, 1);

		$this->actionIndex();
	}

    /**
     * @throws CHttpException
     */
    public function actionUpdate()
    {
        if (!Yii::app()->getRequest()->getIsPostRequest() || !Yii::app()->getRequest()->getPost('id')) {
            throw new CHttpException(404);
        }

        $position = Yii::app()->cart->itemAt(Yii::app()->getRequest()->getPost('id'));
        $quantity = (int)Yii::app()->getRequest()->getPost('quantity');
        Yii::app()->cart->update($position, $quantity);
        Yii::app()->ajax->success(Yii::t("CartModule.cart", 'Quantity changed'));
    }

    /**
     * @throws CHttpException
     */
    public function actionDelete()
    {
        if (!Yii::app()->getRequest()->getIsPostRequest() || !Yii::app()->getRequest()->getPost('id')) {
            throw new CHttpException(404);
        }

        Yii::app()->cart->remove(Yii::app()->getRequest()->getPost('id'));
        Yii::app()->ajax->success(Yii::t("CartModule.cart", 'Product removed from cart'));
    }

    /**
     * @throws CHttpException
     */
    public function actionClear()
    {
        if (!Yii::app()->getRequest()->getIsPostRequest()) {
            throw new CHttpException(404);
        }

        Yii::app()->cart->clear();
        Yii::app()->ajax->success(Yii::t("CartModule.cart", 'Cart is cleared'));
    }

    /**
     *
     */
    public function actionWidget()
    {
    	Yii::app()->cart->checkProducts();

        $this->widget('cart.widgets.ShoppingCartWidget', ['id' => 'shopping-cart-widget']);
    }
}
