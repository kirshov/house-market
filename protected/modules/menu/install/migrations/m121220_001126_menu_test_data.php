<?php

/**
 * File Doc Comment
 * Menu install migration
 * Класс миграций для модуля Menu
 **/
class m121220_001126_menu_test_data extends webforma\components\DbMigration
{

    public function safeUp()
    {
        $this->insert(
            '{{menu_menu}}',
            [
                'id' => 1,
                'name' => 'Верхнее меню',
                'code' => 'top-menu',
                'description' => Yii::t('MenuModule.menu', 'Main site menu. Located at top in "main menu" block.'),
                'status' => 1
            ]
        );

        $items = [
            [
                'parent_id',
                'menu_id',
                'title',
                'href',
                'class',
                'title_attr',
                'before_link',
                'after_link',
                'target',
                'rel',
                'condition_name',
                'condition_denial',
                'sort',
                'status',
                'regular_link'
            ],
            [0, 1, 'Главная', '/', '', '', '', '', '', '', '', 0, 1, 1, 1],
	        [0, 1, 'О компании', '/about/', '', '', '', '', '', '', '', 0, 2, 1, 0],
	        [0, 1, 'Каталог', '/store/product/index', '', '', '', '', '', '', '', 0, 3, 1, 0],
	        [0, 1, 'Оплата и доставка', 'payment-delivery', '', '', '', '', '', '', '', 0, 5, 1, 0],
	        [0, 1, 'Контакты', '/contacts/', '', '', '', '', '', '', '', 0, 7, 1, 0],
        ];

        $columns = array_shift($items);
        /**
         * Как-нибудь описать процесс надо, для большей понятности
         */
        foreach ($items as $i) {
            $item = [];
            $n = 0;

            foreach ($columns as $c) {
                $item[$c] = $i[$n++];
            }
            $this->insert(
                '{{menu_menu_item}}',
                $item
            );
        }
    }
}
