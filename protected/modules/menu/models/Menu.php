<?php
/**
 * Menu основная модель для menu
 */
use webforma\widgets\WPurifier;

/**
 * This is the model class for table "menu".
 *
 * The followings are the available columns in table 'menu':
 * @property string $id
 * @property string $name
 * @property string $code
 * @property string $description
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property MenuItem[] $menuItems
 *
 * @method Menu active()
 */
class Menu extends webforma\models\WModel
{
    /**
     *
     */
    const STATUS_DISABLED = 0;
    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     * Returns the static model of the specified AR class.
     * @param  string $className active record class name.
     * @return Menu   the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{menu_menu}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['name, code', 'required', 'except' => 'search'],
            ['status', 'numerical', 'integerOnly' => true],
            ['name, code', 'filter', 'filter' => [$obj = new WPurifier(), 'purify']],
            ['name, description', 'length', 'max' => 255],
            ['code', 'length', 'max' => 100],
            ['code', 'webforma\components\validators\YSLugValidator'],
            ['code', 'unique'],
            ['status', 'in', 'range' => array_keys($this->statusList)],
            ['id, name, code, description, status', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'menuItems' => [self::HAS_MANY, 'MenuItem', 'menu_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('MenuModule.menu', 'Id'),
            'name' => Yii::t('MenuModule.menu', 'Name'),
            'code' => Yii::t('MenuModule.menu', 'Unified code'),
            'description' => Yii::t('MenuModule.menu', 'Description'),
            'status' => Yii::t('MenuModule.menu', 'Status'),
        ];
    }

    /**
     * @return array customized attribute descriptions (name=>description)
     */
    public function attributeDescriptions()
    {
        return [
            'id' => Yii::t('MenuModule.menu', 'Menu Id'),
            'name' => Yii::t('MenuModule.menu', 'Menu name'),
            'code' => Yii::t(
                'MenuModule.menu',
                'Unified code is using in widget, as identifier for menu printing.'
            ),
            'description' => Yii::t('MenuModule.menu', 'Short description'),
            'status' => Yii::t(
                'MenuModule.menu',
                'Choose menu status: <br /><br /><span class="label label-success">active</span> &ndash; Will be visible on site.<br /><br /><span class="label label-warning">not active</span> &ndash; Will be hidden.'
            ),
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria();

        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider(
            get_class($this), [
                'criteria' => $criteria,
                'sort' => ['defaultOrder' => 'status DESC, id'],
            ]
        );
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'active' => [
                'condition' => 'status = :status',
                'params' => [
                    ':status' => self::STATUS_ACTIVE,
                ],
            ],
        ];
    }

    /**
     *
     */
    protected function afterSave()
    {
	    \TaggedCache\TaggingCacheHelper::deleteTag($this->code);

        return parent::afterSave();
    }

    /**
     *
     */
    protected function afterDelete()
    {
	    \TaggedCache\TaggingCacheHelper::deleteTag($this->code);

        return parent::afterDelete();
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_DISABLED => Yii::t('MenuModule.menu', 'not active'),
            self::STATUS_ACTIVE => Yii::t('MenuModule.menu', 'active'),
        ];
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return array_key_exists($this->status, $data) ? $data[$this->status] : $this->status;
    }

    /**
     * @param $code
     * @param int $parent_id
     * @return array|mixed
     * @throws CDbException
     */
    public function getItems($code, $parent_id = 0)
    {
        $items = Yii::app()->getCache()->get("Menu::{$code}{$parent_id}");

        if ($items === false) {
            $items = [];

            $resultItems = $this->getMenuData($code, (int)$parent_id);
            if (empty($resultItems)) {
                return $items;
            }

            foreach ($resultItems as $result) {
                $childItems = $this->getItems($code, $result->id);

                if ($result->href) {
                    // если адрес надо параметризовать через роутер
                    if (!$result->regular_link) {
                        $url = $result->href;
                        strstr($url, '?') ? list($url, $param) = explode("?", $url) : $param = [];
                        if ($param) {
                            parse_str($param, $param);
                        }
                        $url = ['url' => (array)$url + $param, 'items' => $childItems];
                    } else {
                        // если обычная ссылка
						if(mb_strpos($result->href, 'http://') !== false || mb_strpos($result->href, 'https://') !== false){
							$_href = $result->href;
						} else {
							$_href = mb_stripos($result->href, '#') !== false ? $result->href : Yii::app()->createUrl($result->href);
						}
	                    $url = ['url' => $_href, 'items' => $childItems];
                    }
                } elseif ($childItems) {
                    $url = ['url' => ['#'], 'items' => $childItems];
                } else {
                    $url = [];
                }

                $class = (($childItems) ? ' submenuItem' : '').(($result->class) ? ' '.$result->class : '');
                $title_attr = ($result->title_attr) ? ['title' => $result->title_attr] : [];
                $target = ($result->target && $url) ? ['target' => $result->target] : [];
                $rel = ($result->rel && $url) ? ['rel' => $result->rel] : [];

                $items[] = [
                        'label' => $result->title,
                        'template' => $result->before_link.'{menu}'.$result->after_link,
                        'itemOptions' => ['class' => 'listItem'.$class],
                        'linkOptions' => [
                                'class' => 'listItemLink',
                            ] + $title_attr + $target + $rel,
                        'visible' => MenuItem::model()->getConditionVisible(
                            $result->condition_name,
                            $result->condition_denial
                        ),
                    ] + $url;
            }

            Yii::app()->getCache()->set("Menu::{$code}{$parent_id}", $items, 0, \TaggedCache\TaggingCacheHelper::getDependency(['menu', $code]));
        }

        return $items;
    }

    /**
     * Добавляет новый пункт меню в меню
     * @param $title string - Заголовок
     * @param $href string - Ссылка
     * @param $parentId int - Родитель
     * @param bool $regularLink - Обычная ссылка
     * @return bool
     */
    public function addItem($title, $href, $parentId, $regularLink = false)
    {
        $menuItem = new MenuItem();

        $menuItem->setAttributes([
            'parent_id' => (int)$parentId,
            'menu_id' => $this->id,
            'title' => $title,
            'href' => $href,
            'regular_link' => $regularLink,
        ]);
        if (true === $menuItem->save()) {
	        \TaggedCache\TaggingCacheHelper::deleteTag(['menu', $this->code]);

            return true;
        }

        return false;
    }

    /**
     * Метод изменения пункта меню.
     * @param $oldTitle string Старое название элемента (по нему осуществяется поиск)
     * @param $newTitle string Новое название
     * @param $href string Новая ссылка
     * @param $parentId int id меню
     * @param $regularLink bool Обычная ссылка
     * @return bool статус выполнения
     */
    public function changeItem($oldTitle, $newTitle, $href, $parentId, $regularLink = false)
    {
        $menuItem = MenuItem::model()->findByAttributes(['title' => $oldTitle]);

        if ($menuItem === null) {
            return $this->addItem($newTitle, $href, $parentId, $regularLink);
        }

        $menuItem->setAttributes([
            'parent_id' => (int)$parentId,
            'menu_id' => $this->id,
            'title' => $newTitle,
            'href' => $href,
            'regular_link' => $regularLink,
        ]);

        if ($menuItem->save()) {
	        \TaggedCache\TaggingCacheHelper::deleteTag(['menu', $this->code]);

            return true;
        }

        return false;
    }

    public function getMenuData($code, $parent_id){
        $data = Yii::app()->getCache()->get('menu::'.$code);
        if(!is_array($data)){
            $data = [];

            $criteria = new CDbCriteria();
            $criteria->condition = 't.status = :status AND menu.code = :code';
            $criteria->params = [
                ':status' => MenuItem::STATUS_ACTIVE,
                ':code' => $code,
            ];
            $criteria->join = 'INNER JOIN '.Menu::model()->tableName().' menu ON t.menu_id = menu.id';
            $criteria->order =  't.sort ASC, t.id ASC';

            $results = MenuItem::model()->findAll($criteria);

            if($results){
                foreach ($results as $item){
                    $data[$item->parent_id][] = $item;
                }
            }
            Yii::app()->getCache()->set('menu::'.$code, $data, 0, \TaggedCache\TaggingCacheHelper::getDependency(['menu', $code]));
        }
        return $data[$parent_id] ?: [];
    }
}
