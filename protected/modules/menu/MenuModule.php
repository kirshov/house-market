<?php

/**
 * MenuModule основной класс модуля menu
 */
class MenuModule extends webforma\components\WebModule
{
    /**
     * @var string
     */
    public $defaultController = 'menu';

    /**
     * @var string
     */
    public $menuCache = 'menu.cache';

	/**
	 * @var int порядок следования модуля в меню панели управления (сортировка)
	 */
	public $adminMenuOrder = 10;

    /**
     * @return bool
     */
    public function getIsInstallDefault()
    {
        return true;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('MenuModule.menu', 'Structure');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('MenuModule.menu', 'Menu');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('MenuModule.menu', 'Menu management module');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-list';
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/menu/menuBackend/index';
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            ['label' => Yii::t('MenuModule.menu', 'Menu')],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('MenuModule.menu', 'Manage menu'),
                'url' => ['/menu/menuBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('MenuModule.menu', 'Create menu'),
                'url' => ['/menu/menuBackend/create'],
            ],
            ['label' => Yii::t('MenuModule.menu', 'Menu items')],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('MenuModule.menu', 'Manage menu items'),
                'url' => ['/menu/menuitemBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('MenuModule.menu', 'Create menu item'),
                'url' => ['/menu/menuitemBackend/create'],
            ],
        ];
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'application.modules.menu.models.*',
            ]
        );
    }
}
