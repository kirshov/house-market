<ul class="nav nav-tabs horizontal-tabs">
	<li class="active"><a href="#common" data-toggle="tab">Общее</a></li>
	<li><a href="#advanced" data-toggle="tab">Дополнительные параметры</a></li>
</ul>

<?php
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'id' => 'menu-item-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'type' => 'vertical',
        'htmlOptions' => ['class' => 'well sticky'],
    ]
); ?>

<div class="tab-content">
	<div class="tab-pane active" id="common">
		<div class="alert alert-info">
			<?= Yii::t('MenuModule.menu', 'Fields with'); ?>
			<span class="required">*</span>
			<?= Yii::t('MenuModule.menu', 'are required.'); ?>
		</div>

		<?= $form->errorSummary($model); ?>

		<div class="row">
			<?php
			$menu_id = '#'.CHtml::activeId($model, 'menu_id');
			$parent_id = '#'.CHtml::activeId($model, 'parent_id');
			?>
			<div class="col-sm-3">
				<?= $form->dropDownListGroup($model, 'menu_id', [
					'widgetOptions' => [
						'data' => CHtml::listData(Menu::model()->findAll(), 'id', 'name'),
						'htmlOptions' => [
							'empty' => Yii::t('MenuModule.menu', '--choose menu--'),
							'ajax' => [
								'type' => 'POST',
								'url' => $this->createUrl(
									'/menu/menuitemBackend/dynamicparent',
									(!$model->getIsNewRecord() ? ['id' => $model->id] : [])
								),
								'update' => $parent_id,
								'beforeSend' => "function () {
								$('".$parent_id."').attr('disabled', true);
								if ($('".$menu_id." option:selected').val() == '')
									return false;
							}",
								'complete' => "function () {
								$('".$parent_id."').attr('disabled', false);
							}",
							],
						],
					],
				]); ?>
			</div>
			<div class="col-sm-3">
				<?= $form->dropDownListGroup($model, 'parent_id', [
					'widgetOptions' => [
						'data' => $model->getParentTree(),
						'htmlOptions' => [
							'disabled' => ($model->menu_id) ? false : true,
							'encode' => false,
						],
					],
				]); ?>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<?= $form->textFieldGroup($model, 'title'); ?>
			</div>
			<div class="col-sm-2">
				<?= $form->dropDownListGroup($model, 'status', [
					'widgetOptions' => [
						'data' => $model->getStatusList(),
					],
				]); ?>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<?= $form->textFieldGroup($model, 'href'); ?>
			</div>
		</div>

		<div class="row hidden">
			<div class="col-sm-6">
				<?= $form->textFieldGroup($model, 'sort'); ?>
			</div>
		</div>
	</div>
	<div class="tab-pane" id="advanced">
		<div class="row">
			<div class="col-sm-6">
				<?= $form->checkBoxGroup($model, 'regular_link'); ?>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<?= $form->textFieldGroup($model, 'title_attr', [
					'widgetOptions' => [
						'data' => $model->getStatusList(),
					],
				]); ?>
			</div>

		</div>
		<div class="row">
			<div class="col-sm-6">
				<?= $form->textFieldGroup($model, 'class', [
					'widgetOptions' => [
						'data' => $model->getStatusList(),
					],
				]); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<?= $form->textFieldGroup($model, 'before_link'); ?>
			</div>
			<div class="col-sm-3">
				<?= $form->textFieldGroup($model, 'after_link'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<?= $form->textFieldGroup(
					$model,
					'target',
					[
						'widgetOptions' => [
							'data' => $model->getStatusList(),
							'htmlOptions' => [
								'class' => 'popover-help',
								'data-original-title' => $model->getAttributeLabel('target'),
								'data-content' => $model->getAttributeDescription('target'),
							],
						],
					]
				); ?>
			</div>
			<div class="col-sm-3">
				<?= $form->textFieldGroup(
					$model,
					'rel',
					[
						'widgetOptions' => [
							'data' => $model->getStatusList(),
							'htmlOptions' => [
								'class' => 'popover-help',
								'data-original-title' => $model->getAttributeLabel('rel'),
								'data-content' => $model->getAttributeDescription('rel'),
							],
						],
					]
				); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<?= $form->dropDownListGroup($model, 'condition_name', [
					'widgetOptions' => [
						'data' => $model->getConditionList(),
						'htmlOptions' => [
							'class' => 'popover-help',
							'data-original-title' => $model->getAttributeLabel('condition_name'),
							'data-content' => $model->getAttributeDescription('condition_name'),
							'empty' => '',
						],
					],
				]); ?>
			</div>
			<div class="col-sm-3">
				<?= $form->dropDownListGroup($model, 'condition_denial', [
					'widgetOptions' => [
						'data' => $model->getConditionDenialList(),
						'htmlOptions' => [
							'class' => 'popover-help',
							'data-original-title' => $model->getAttributeLabel('condition_denial'),
							'data-content' => $model->getAttributeDescription('condition_denial'),
						],
					],
				]); ?>
			</div>
	</div>
</div>

<div class="buttons">
	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context' => 'primary',
		'label' => $model->getIsNewRecord()
			? Yii::t('MenuModule.menu', 'Create menu item and continue')
			: Yii::t('MenuModule.menu', 'Save menu item and continue'),
	]); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label' => $model->getIsNewRecord()
			? Yii::t('MenuModule.menu', 'Create menu item and close')
			: Yii::t('MenuModule.menu', 'Save menu item and close'),
	]); ?>
</div>
<?php $this->endWidget(); ?>
