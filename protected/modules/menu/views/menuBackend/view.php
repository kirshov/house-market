<?php
/**
 * Файл представления menu/view
 **/
$this->breadcrumbs = [
    Yii::t('MenuModule.menu', 'Menu') => ['/menu/menuBackend/index'],
    $model->name,
];

$this->pageTitle = Yii::t('MenuModule.menu', 'Menu - show');

$this->menu = [
    [
        'label' => Yii::t('MenuModule.menu', 'Menu'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('MenuModule.menu', 'Manage menu'),
                'url'   => ['/menu/menuBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('MenuModule.menu', 'Create menu'),
                'url'   => ['/menu/menuBackend/create']
            ],
            [
                'icon'  => 'fa fa-fw fa-pencil',
                'label' => Yii::t('MenuModule.menu', 'Change menu'),
                'url'   => ['/menu/menuBackend/update', 'id' => $model->id]
            ],
            [
                'icon'        => 'fa fa-fw fa-eye',
                'encodeLabel' => false,
                'label'       => Yii::t('MenuModule.menu', 'View menu'),
                'url'         => [
                    '/menu/menuBackend/view',
                    'id' => $model->id
                ]
            ],
            [
                'icon'        => 'fa fa-fw fa-trash-o',
                'label'       => Yii::t('MenuModule.menu', 'Remove menu'),
                'url'         => '#',
                'linkOptions' => [
                    'submit'  => ['/menu/menuBackend/delete', 'id' => $model->id],
                    'confirm' => Yii::t('MenuModule.menu', 'Do you really want to delete?'),
                    'params'  => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
                ],
            ],
        ]
    ],
    [
        'label' => Yii::t('MenuModule.menu', 'Menu items'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('MenuModule.menu', 'Manage menu items'),
                'url'   => ['/menu/menuitemBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('MenuModule.menu', 'Create menu item'),
                'url'   => ['/menu/menuitemBackend/create']
            ],
        ]
    ],
];
?>

<?php $this->widget(
    'bootstrap.widgets.TbDetailView',
    [
        'data'       => $model,
        'attributes' => [
            'id',
            'name',
            'code',
            'description',
            [
                'name'  => 'status',
                'value' => $model->getStatus(),
            ],
        ],
    ]
); ?>

<br/>
<div>
    <?=  Yii::t('MenuModule.menu', 'Use next code for inserting menu in view'); ?>
    <p>
        <?=  $example; ?>
    </p>
</div>
