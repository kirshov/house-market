<?php
$form = $this->beginWidget(
    'webforma\widgets\ActiveForm',
    [
        'id'                     => 'menu-form',
	    'enableAjaxValidation' => false,
	    'enableClientValidation' => false,
	    'type' => 'vertical',
	    'htmlOptions' => ['class' => 'well sticky'],
	    'clientOptions' => [
		    'validateOnSubmit' => true,
	    ],
    ]
); ?>
<div class="alert alert-info">
    <?=  Yii::t('MenuModule.menu', 'Fields with'); ?>
    <span class="required">*</span>
    <?=  Yii::t('MenuModule.menu', 'are required.'); ?>
</div>

<?=  $form->errorSummary($model); ?>

<div class="row">
    <div class="col-sm-6">
        <?=  $form->textFieldGroup($model, 'name', [
			'widgetOptions' => [
				'htmlOptions' => [
					'class'               => 'popover-help',
					'data-original-title' => $model->getAttributeLabel('name'),
					'data-content'        => $model->getAttributeDescription('name'),
				],
			],
		]); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <?=  $form->slugFieldGroup($model, 'code', [
			'sourceAttribute' => 'name',
			'widgetOptions'   => [
				'htmlOptions' => [
					'class'               => 'popover-help',
					'data-original-title' => $model->getAttributeLabel('code'),
					'data-content'        => $model->getAttributeDescription('code'),
				],
			],
		]); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <?=  $form->textAreaGroup($model, 'description', [
			'widgetOptions' => [
				'htmlOptions' => [
					'class'               => 'popover-help',
					'data-original-title' => $model->getAttributeLabel('description'),
					'data-content'        => $model->getAttributeDescription('description'),
				],
			],
		]); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-3">
        <?=  $form->dropDownListGroup($model, 'status', [
			'widgetOptions' => [
				'data'        => $model->getStatusList(),
				'htmlOptions' => [
					'class'               => 'popover-help',
					'data-original-title' => $model->getAttributeLabel('status'),
					'data-content'        => $model->getAttributeDescription('status'),
				],
			],
		]); ?>
    </div>
</div>

<div class="buttons">
	<?php
	$this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context'    => 'primary',
		'label'      => $model->getIsNewRecord()
			? Yii::t('MenuModule.menu', 'Create menu and continue')
			: Yii::t('MenuModule.menu', 'Save menu and continue'),
	]); ?>

	<?php
	$this->widget('bootstrap.widgets.TbButton', [
		'buttonType'  => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label'       => $model->getIsNewRecord()
			? Yii::t('MenuModule.menu', 'Create menu and close')
			: Yii::t('MenuModule.menu', 'Save menu and close'),
	]); ?>
</div>
<?php $this->endWidget(); ?>
