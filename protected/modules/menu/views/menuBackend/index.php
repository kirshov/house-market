<?php
/**
 * Файл представления menu/index
 **/
$this->breadcrumbs = [
    Yii::t('MenuModule.menu', 'Menu') => ['/menu/menuBackend/index'],
    Yii::t('MenuModule.menu', 'Manage')
];

$this->pageTitle = Yii::t('MenuModule.menu', 'Menu - manage');

$this->menu = [
    [
        'label' => Yii::t('MenuModule.menu', 'Menu'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('MenuModule.menu', 'Manage menu'),
                'url'   => ['/menu/menuBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('MenuModule.menu', 'Create menu'),
                'url'   => ['/menu/menuBackend/create']
            ],
        ]
    ],
    [
        'label' => Yii::t('MenuModule.menu', 'Menu items'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('MenuModule.menu', 'Manage menu items'),
                'url'   => ['/menu/menuitemBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('MenuModule.menu', 'Create menu item'),
                'url'   => ['/menu/menuitemBackend/create']
            ],
        ]
    ],
];
?>
<?php $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id'           => 'menu-grid',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => [
            [
                'class'    => 'webforma\widgets\WEditableColumn',
                'name'     => 'name',
                'editable' => [
                    'url'    => $this->createUrl('/menu/menuBackend/inline'),
                    'mode'   => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'name', ['class' => 'form-control']),
            ],
            [
                'class'    => 'webforma\widgets\WEditableColumn',
                'name'     => 'code',
                'editable' => [
                    'url'    => $this->createUrl('/menu/menuBackend/inline'),
                    'mode'   => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'code', ['class' => 'form-control']),
            ],
            [
                'class'    => 'webforma\widgets\WEditableColumn',
                'name'     => 'description',
                'editable' => [
                    'url'        => $this->createUrl('/menu/menuBackend/inline'),
                    'mode'       => 'popup',
                    'type'       => 'textarea',
                    'inputclass' => 'input-large',
                    'params'     => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ],
                ],
	            'value' => '($data->description) ? $data->description : "-"',
	            'filter'   => CHtml::activeTextField($model, 'description', ['class' => 'form-control']),
            ],
            [
                'class'   => 'webforma\widgets\EditableStatusColumn',
                'name'    => 'status',
                'url'     => $this->createUrl('/menu/menuBackend/inline'),
                'source'  => $model->getStatusList(),
                'options' => [
                    Menu::STATUS_ACTIVE   => ['class' => 'label-success'],
                    Menu::STATUS_DISABLED => ['class' => 'label-default'],
                ],
            ],
            [
                'header' => Yii::t('MenuModule.menu', 'Items'),
                'type'   => 'raw',
                'value'  => 'CHtml::link(count($data->menuItems), Yii::app()->createUrl("/menu/menuitemBackend/index", array("MenuItem[menu_id]" => $data->id)))',
            ],
            [
                'class'    => 'webforma\widgets\CustomButtonColumn',
                'template' => '{view}{update}{delete}{add}',
                'buttons'  => [
                    'add' => [
                        'icon'  => 'fa fa-fw fa-plus-square',
                        'label' => Yii::t('MenuModule.menu', 'Create menu item'),
                        'url'   => 'Yii::app()->createUrl("/menu/menuitemBackend/create", array("mid" => $data->id))',
                        'options' => ['class' => 'btn btn-sm btn-default']
                    ],
                ],
            ],
        ],
    ]
); ?>
