<?php
use webforma\components\WebModule;

/**
 * Class SocialModule
 */
class SocialModule extends WebModule
{
    /**
     * @var string
     */
    public $controllerNamespace = '\application\modules\social\controllers';

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            'user',
        ];
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getCategory()
    {
    	return false;
        return Yii::t('SocialModule.social', 'Users');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('SocialModule.social', 'Socialization');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('SocialModule.social', 'Module for login and registration via social networks');
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/social/socialBackend/index';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return "fa fa-fw fa-globe";
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            ['label' => Yii::t('SocialModule.social', 'Users')],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('SocialModule.social', 'Accounts'),
                'url' => ['/social/socialBackend/index'],
            ],
        ];
    }

    /**
     *
     */
    public function init()
    {
        parent::init();
    }
}
