<?php
/**
 * Отображение для index
 **/
$this->breadcrumbs = [
    Yii::app()->getModule('social')->getCategory() => [],
    Yii::t('SocialModule.social', 'Accounts')      => ['/social/socialBackend/index'],
    Yii::t('SocialModule.social', 'Manage'),
];

$this->pageTitle = Yii::t('SocialModule.social', 'Accounts - manage');

$this->menu = [
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('SocialModule.social', 'Manage social accounts'),
        'url'   => ['/social/socialBackend/index']
    ],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('SocialModule.social', 'Accounts'); ?>
        <small><?=  Yii::t('SocialModule.social', 'manage'); ?></small>
    </h1>
</div>

<p class="search-wrap">
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?=  Yii::t('SocialModule.social', 'Account search'); ?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php
    Yii::app()->clientScript->registerScript(
        'search',
        "
    $('.search-form form').submit(function () {
        $.fn.yiiGridView.update('social-user-grid', {
            data: $(this).serialize()
        });

        return false;
    });
    "
    );
    $this->renderPartial('_search', ['model' => $model]);
    ?>
</div>

<?php
$this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id'           => 'social-user-grid',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'bulkActions'  => [false],
        'actionsButtons' => false,
        'columns'      => [
            [
                'name'   => 'user_id',
                'value'  => '$data->user->getFullName()',
                'filter' => CHtml::listData(User::model()->findAll(), 'id', 'fullName')
            ],
            'provider',
            'uid',
            [
                'class'    => 'webforma\widgets\CustomButtonColumn',
                'template' => '{view}{delete}'
            ],
        ],
    ]
); ?>
