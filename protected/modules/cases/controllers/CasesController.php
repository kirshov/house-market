<?php
/*
* CasesController контроллер для case на публичной части сайта
*/

class CasesController extends \webforma\components\controllers\FrontController
{
	/**
	 * @param $slug
	 * @throws CHttpException
	 */
	public function actionView($slug)
	{
		$model = Cases::model()->published()->find('slug = :slug', [':slug' => $slug]);

		if (!$model) {
			throw new CHttpException(404, 'Страница не найдена');
		}

		$this->render('view', ['case' => $model]);
	}

	/**
	 *
	 */
	public function actionIndex()
	{
		$dbCriteria = new CDbCriteria([
			'condition' => 't.status = :status',
			'params' => [
				':status' => Cases::STATUS_ACTIVE,
			],
			'order' => 't.sort DESC',
		]);

		$dataProvider = new CActiveDataProvider('Cases', [
			'criteria' => $dbCriteria,
			'pagination' => [
				'pageSize' => (int)$this->getModule()->perPage,
			],
		]);

		$this->render('index', ['dataProvider' => $dataProvider]);
	}
}