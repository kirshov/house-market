<?php
/**
* Класс CasesBackendController:
**/
class CasesBackendController extends \webforma\components\controllers\BackController
{

	/**
	 * @return array
	 */
	public function actions()
	{
		return [
			'inline' => [
				'class' => 'webforma\components\actions\WInLineEditAction',
				'model' => 'Cases',
				'validateModel' => false,
				'validAttributes' => [
					'status',
				],
			],
			'sortable' => [
				'class' => 'webforma\components\actions\SortAction',
				'model' => 'Cases',
				'attribute' => 'sort',
			],
		];
	}

    /**
    * Отображает кейс по указанному идентификатору
    *
    * @param integer $id Идинтификатор кейс для отображения
    *
    * @return void
    */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }
    
    /**
    * Создает новую модель кейсы.
    * Если создание прошло успешно - перенаправляет на просмотр.
    *
    * @return void
    */
    public function actionCreate()
    {
        $model = new Cases;

        if (Yii::app()->getRequest()->getPost('Cases') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('Cases'));
        
            if ($model->save()) {
                Yii::app()->user->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('CasesModule.cases', 'Запись добавлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'create',
                        ]
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }
    
    /**
    * Редактирование кейсы.
    *
    * @param integer $id Идинтификатор кейс для редактирования
    *
    * @return void
    */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (Yii::app()->getRequest()->getPost('Cases') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('Cases'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('CasesModule.cases', 'Запись обновлена!')
                );

	            $this->updateCasesImages($model);

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id
                        ]
                    )
                );
            }
        }
        $this->render('update', ['model' => $model]);
    }
    
    /**
    * Удаляет модель кейсы из базы.
    * Если удаление прошло успешно - возвращется в index
    *
    * @param integer $id идентификатор кейсы, который нужно удалить
    *
    * @return void
    */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                Yii::t('CasesModule.cases', 'Запись удалена!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else
            throw new CHttpException(400, Yii::t('CasesModule.cases', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
    }
    
    /**
    * Управление кейсы.
    *
    * @return void
    */
    public function actionIndex()
    {
        $model = new Cases('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('Cases') !== null)
            $model->setAttributes(Yii::app()->getRequest()->getParam('Cases'));
        $this->render('index', ['model' => $model]);
    }
    
    /**
    * Возвращает модель по указанному идентификатору
    * Если модель не будет найдена - возникнет HTTP-исключение.
    *
    * @param integer идентификатор нужной модели
    *
    * @return void
    */
    public function loadModel($id)
    {
        $model = Cases::model()->with('images')->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('CasesModule.cases', 'Запрошенная страница не найдена.'));

        return $model;
    }

	/**
	 * @param Cases $case
	 */
	protected function updateCasesImages(Cases $case)
	{
		if (Yii::app()->getRequest()->getPost('CasesImage')) {
			foreach (Yii::app()->getRequest()->getPost('CasesImage') as $key => $val) {
				$caseImage = CasesImage::model()->findByPk($key);
				if (null === $caseImage) {
					$caseImage = new CasesImage();
					$caseImage->case_id = $case->id;
					$caseImage->addFileInstanceName('CasesImage['.$key.'][name]');
				}
				$caseImage->setAttributes($_POST['CasesImage'][$key]);
				if (false === $caseImage->save()) {
					exit();
					Yii::app()->getUser()->setFlash(\webforma\widgets\WFlashMessages::ERROR_MESSAGE, 'Возникла ошибка заливки файлов');
				}
			}
		}
	}


	/**
	 * @throws CHttpException
	 */
	public function actionDeleteImage()
	{
		if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getIsAjaxRequest()) {

			$id = (int)Yii::app()->getRequest()->getPost('id');

			$model = CasesImage::model()->findByPk($id);

			if (null !== $model && $model->delete()) {
				Yii::app()->ajax->success();
			}
		}

		throw new CHttpException(404);
	}
}
