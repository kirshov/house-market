<?php
/**
 * Отображение для create:
 **/
$this->breadcrumbs = [
    Yii::t('CasesModule.cases', 'Кейсы') => ['/cases/casesBackend/index'],
    Yii::t('CasesModule.cases', 'Добавление'),
];

$this->pageTitle = Yii::t('CasesModule.cases', 'Кейсы - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('CasesModule.cases', 'Кейсы'), 'url' => ['/cases/casesBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('CasesModule.cases', 'Добавить кейс'), 'url' => ['/cases/casesBackend/create']],
];
?>
<?=  $this->renderPartial('_form', ['model' => $model]); ?>