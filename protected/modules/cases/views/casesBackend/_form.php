<?
/**
 * @var Cases $model
 */
?>

<ul class="nav nav-tabs horizontal-tabs">
	<li class="active"><a href="#common" data-toggle="tab">Основные</a></li>
	<li><a href="#seo" data-toggle="tab">SEO</a></li>
	<li><a href="#images" data-toggle="tab">Дополнительные изображения</a></li>
</ul>

<?php
$form = $this->beginWidget(
    '\webforma\widgets\ActiveForm', [
        'id'                     => 'cases-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => 'vertical',
		'htmlOptions' => ['class' => 'well sticky', 'enctype' => 'multipart/form-data'],
		'clientOptions' => [
			'validateOnSubmit' => true,
		],
    ]
);
?>

<?=  $form->errorSummary($model); ?>

<div class="tab-content">
	<div class="tab-pane active" id="common">

		<div class="alert alert-info">
			<?=  Yii::t('CasesModule.cases', 'Поля, отмеченные'); ?>
			<span class="required">*</span>
			<?=  Yii::t('CasesModule.cases', 'обязательны.'); ?>
		</div>

		<div class="row">
			<div class="col-sm-7">
				<div class="row">
					<div class="col-sm-12">
						<?=  $form->textFieldGroup($model, 'name'); ?>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-12">
						<?if($model->isNewRecord){
							echo $form->slugFieldGroup($model, 'slug', ['sourceAttribute' => 'name']);
						} else {
							echo $form->textFieldGroup($model, 'slug');
						} ?>
					</div>
				</div>

				<div class="row">

					<div class="col-sm-6">
						<?= $form->datePickerGroup(
							$model,
							'date',
							[
								'widgetOptions' => [
									'options' => [
										'format' => 'dd.mm.yyyy',
										'weekStart' => 1,
										'autoclose' => true,
									],
								],
								'prepend' => '<i class="fa fa-calendar"></i>',
							]
						);
						?>
					</div>
				</div>


				<div class="row">
					<div class="col-sm-12">
						<?= $form->labelEx($model, 'short_description'); ?>
						<?php $this->widget($this->module->getVisualEditor(), [
							'model' => $model,
							'attribute' => 'short_description',
						]); ?>
					</div>
				</div>
			</div>

			<div class="col-sm-5">
				<div class='row'>
					<div class="col-sm-12">
						<?php $this->widget('webforma\widgets\WInputFile', [
							'model' => $model,
							'attribute' => 'image',
						]); ?>
					</div>
				</div>
			</div>
		</div>
		<br/>

		<div class="row">
			<div class="col-sm-12">
				<?= $form->labelEx($model, 'description'); ?>
				<?php $this->widget($this->module->getVisualEditor(), [
					'model' => $model,
					'attribute' => 'description',
				]); ?>
			</div>
		</div>
		<br/>

		<div class="row">
			<div class="col-sm-3">
				<?= $form->dropDownListGroup($model, 'status', [
					'widgetOptions' => [
						'data' => $model->getStatusList(),
					],
				]); ?>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="seo">
		<div class="row">
			<div class="col-xs-6">
				<?= $form->textFieldGroup($model,'title_h1'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6">
				<?= $form->textFieldGroup($model,'meta_title'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6">
				<?= $form->textFieldGroup($model, 'meta_keywords'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6">
				<?= $form->textAreaGroup($model,'meta_description'); ?>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="images">
		<?php if ($model->getIsNewRecord()): ?>
			<div class="row">
				<div class="col-lg-12">
					<div class="alert alert-warning">
						Массовая загрузка будет доступна после сохранения кейса
					</div>
				</div>
			</div>
		<?php else: ?>
			<div class="row">
				<div class="col-xs-12">
					<button id="button-add-image" type="button" class="btn btn-success"><i class="fa fa-fw fa-plus"></i> Добавить изображение</button>
				</div>
			</div>
			<p></p>
			<div class="row">
				<div class="col-xs-12">
					<?php $imageModel = new CasesImage(); ?>
					<div id="cases-images">
						<div class="image-template hidden form-group">
							<div class="row">
								<div class="col-xs-6 col-sm-4">
									<label for="">Файл</label>
									<input type="file" class="image-file"/>
									<input type="hidden" class="image-key"/>
								</div>
								<div class="col-xs-2 col-sm-1" style="padding-top: 24px">
									<button class="button-delete-image btn btn-default" type="button">
										<i class="fa fa-fw fa-trash-o"></i>
									</button>
								</div>
							</div>
						</div>
					</div>

					<?php if ($model->images): ?>
						<table class="table table-hover">
							<thead>
							<tr>
								<th></th>
								<th></th>
							</tr>
							</thead>
							<tbody>
							<?php foreach ($model->images as $image): ?>
								<tr>
									<td>
										<img src="<?= $image->getImageUrl(100, 100, false); ?>" alt="" class="img-responsive"/>
									</td>

									<td class="text-center">
										<a data-id="<?= $image->id; ?>" href="<?= Yii::app()->createUrl('/cases/casesBackend/deleteImage', ['id' => $image->id]); ?>" class="btn btn-default case-delete-image">
											<i class="fa fa-fw fa-trash-o"></i>
										</a>
									</td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>
	</div>
</div>

<div class="buttons">
		<?php $this->widget('bootstrap.widgets.TbButton', [
			'buttonType' => 'submit',
			'context'    => 'primary',
			'label'      => Yii::t('CasesModule.cases', 'Сохранить и продолжить'),
		]); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', [
			'buttonType' => 'submit',
			'htmlOptions'=> ['name' => 'submit-type', 'value' => 'index'],
			'label'      => Yii::t('CasesModule.cases', 'Сохранить и закрыть'),
		]); ?>
	</div>
<?php $this->endWidget(); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#cases-images').on('click', '.button-delete-image', function () {
			$(this).closest('.row').remove();
		});

		$('#button-add-image').on('click', function () {
			var newImage = $("#cases-images .image-template").clone().removeClass('image-template').removeClass('hidden');
			var key = $.now();

			newImage.appendTo("#cases-images");
			newImage.find(".image-file").attr('name', 'CasesImage[new_' + key + '][name]');
			newImage.find(".image-key").attr('name', 'CasesImage[new_' + key + '][key]');

			return false;
		});

		$('.case-delete-image').on('click', function (event) {
			event.preventDefault();
			var blockForDelete = $(this).closest('tr');
			$.ajax({
				type: "POST",
				data: {
					'id': $(this).data('id'),
					'<?= Yii::app()->getRequest()->csrfTokenName;?>': '<?= Yii::app()->getRequest()->csrfToken;?>'
				},
				url: '<?= Yii::app()->createUrl('/cases/casesBackend/deleteImage');?>',
				success: function () {
					blockForDelete.remove();
				}
			});
		});

		activateFirstTabWithErrors();
	});

	function activateFirstTabWithErrors() {
		var tab = $('.has-error').parents('.tab-pane').first();
		if (tab.length) {
			var id = tab.attr('id');
			$('a[href="#' + id + '"]').tab('show');
		} else {
			setCurrentTabActive();
		}
	}
</script>
