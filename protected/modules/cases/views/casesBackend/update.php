<?php
/**
 * Отображение для update:
 **/
$this->breadcrumbs = [
    Yii::t('CasesModule.cases', 'Кейсы') => ['/cases/casesBackend/index'],
    $model->name => ['/cases/casesBackend/view', 'id' => $model->id],
    Yii::t('CasesModule.cases', 'Редактирование'),
];

$this->pageTitle = Yii::t('CasesModule.cases', 'Кейсы - редактирование');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('CasesModule.cases', 'Кейсы'), 'url' => ['/cases/casesBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('CasesModule.cases', 'Добавить кейс'), 'url' => ['/cases/casesBackend/create']],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('CasesModule.cases', 'Редактирование кейсы'), 'url' => [
        '/cases/casesBackend/update',
        'id' => $model->id
    ]],
    [
        'icon'  => 'fa fa-fw fa-external-link-square',
        'label' => 'Просмотр на сайте',
        'url'   => Yii::app()->createAbsoluteUrl('cases/cases/view', ['slug' => $model->slug]),
        'linkOptions' => [
            'target' => '_blank',
        ],
        'visible' => $model->status == Cases::STATUS_ACTIVE,
    ],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('CasesModule.cases', 'Удалить кейс'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/cases/casesBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('CasesModule.cases', 'Вы уверены, что хотите удалить кейс?'),
        'csrf' => true,
    ]],
];
?>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>