<?php
/**
 * Отображение для index
 **/
$this->breadcrumbs = [
    Yii::t('CasesModule.cases', 'Кейсы') => ['/cases/casesBackend/index'],
    Yii::t('CasesModule.cases', 'Управление'),
];

$this->pageTitle = Yii::t('CasesModule.cases', 'Кейсы - управление');

$this->menu = [
	['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('CasesModule.cases', 'Кейсы'), 'url' => ['/cases/casesBackend/index']],
	['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('CasesModule.cases', 'Добавить кейс'), 'url' => ['/cases/casesBackend/create']],
	[
		'icon' => 'fa fa-fw fa-external-link-square',
		'label' => Yii::t('WebformaModule.webforma', 'To the section on the site'),
		'url' => ['/cases/cases/index'],
		'linkOptions' => [
			'target' => '_blank',
		]
	],
];
?>

<?php
 $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id'           => 'cases-grid',
	    'sortableRows'      => true,
	    'sortableAjaxSave'  => true,
	    'sortableAttribute' => 'sort',
	    'sortableAction'    => '/cases/casesBackend/sortable',
        'type'         => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => [
	        [
		        'type' => 'raw',
		        'value' => function ($data) {
			        return CHtml::image($data->getImageUrl(100, 100), $data->name, ["width" => 100, "height" => 100, "class" => "img-thumbnail"]);
		        },
		        'htmlOptions' => [
			        'style' => 'width: 100px;',
		        ],
		        'filter' => CHtml::activeTextField($model, 'id', ['class' => 'form-control']),
	        ],
            'name',
	        [
	        	'name' => 'short_description',
		        'type' => 'raw',
		        'value' => function ($data){
			        return strip_tags($data->short_description);
		        }
			],
	        [
		        'class' => 'webforma\widgets\EditableStatusColumn',
		        'name' => 'status',
		        'url' => $this->createUrl('/cases/casesBackend/inline'),
		        'source' => $model->getStatusList(),
		        'options' => [
			        Cases::STATUS_ACTIVE => ['class' => 'label-success'],
			        Cases::STATUS_NOT_ACTIVE => ['class' => 'label-info'],
		        ],
	        ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
	            'template' => '{front_view} {update} {delete}',
            ],
        ],
    ]
); ?>
