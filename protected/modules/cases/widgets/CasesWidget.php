<?php
Yii::import('application.modules.cases.models.*');
Yii::import('application.modules.customer.models.Customer');

/**
 * Class CasesWidget
 */
class CasesWidget extends webforma\widgets\WWidget
{
    /**
     * @var int
     */
    public $limit = 5;
    /**
     * @var string
     */
    public $view = 'default';

	/**
	 * @var bool
	 */
    public $isSlider = false;

	/**
	 * @var
	 */
    public $title;

    /**
     * @throws CException
     */
    public function run()
    {
    	$criteria = new CDbCriteria();
    	$criteria->order = 't.sort DESC';
    	$criteria->scopes = ['published'];
    	if($this->limit){
		    $criteria->limit = $this->limit;
	    }

    	$cases = Cases::model()->findAll($criteria);
    	if(empty($cases)){
    		return false;
	    }

        $this->render($this->view, [
        	'cases' => $cases,
        	'isSlider' => $this->isSlider,
        	'title' => $this->title ?: Yii::app()->getModule('cases')->titleWidget,
        ]);
    }
}
