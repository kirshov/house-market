<?php

/**
 * This is the model class for table "{{case}}".
 *
 * The followings are the available columns in table '{{case}}':
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $image
 * @property string $short_description
 * @property string $description
 * @property integer $status
 * @property integer $sort
 * @property string $date
 * @property string $update_time
 * @property string $create_time
 * @property string $title_h1
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 *
 * @method getImageUrl
 */
class Cases extends webforma\models\WModel
{

	/**
	 *
	 */
	const STATUS_ACTIVE = 1;
	/**
	 *
	 */
	const STATUS_NOT_ACTIVE = 0;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{case}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return [
			['name, slug', 'required'],
			['sort', 'length', 'max'=>255],
			['description, short_description, title_h1, meta_title, meta_keywords, meta_description', 'filter', 'filter' => 'trim'],
			['slug', 'webforma\components\validators\YSLugValidator', 'message' => 'Запрещенные символы в поле {attribute}',],
			['slug', 'unique'],
			['status', 'in', 'range' => array_keys($this->getStatusList())],
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			['id, name, short_description, description, slug', 'safe', 'on'=>'search'],
		];
	}

	public function behaviors()
	{
		return [
			'imageUpload' => [
				'class' => 'webforma\components\behaviors\ImageUploadBehavior',
				'attributeName' => 'image',
				'uploadPath' => 'cases',
			],
			'CTimestampBehavior' => [
				'class' => 'zii.behaviors.CTimestampBehavior',
				'setUpdateOnCreate' => true,
			],
			'sortable' => [
				'class' => 'webforma\components\behaviors\SortableBehavior',
				'attributeName' => 'sort',
			],
		];
	}

	/**
	 * @return array
	 */
	public function scopes()
	{
		return [
			'published' => [
				'condition' => 't.status = :status',
				'params' => [':status' => self::STATUS_ACTIVE],
				'order' => 't.sort DESC',
			],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'images' => [self::HAS_MANY, 'CasesImage', 'case_id'],
		);
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'image' => 'Изображение',
			'name' => 'Название',
			'short_description' => 'Краткое описание',
			'description' => 'Описание',
			'update_time' => 'Update Time',
			'slug' => 'Url',
			'status' => 'Статус',
			'date' => 'Дата',
			'title_h1' => 'Заголовок H1',
			'meta_title' => 'Заголовок title',
			'meta_keywords' => 'Ключевые слова',
			'meta_description' => 'Описание',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('short_description',$this->short_description,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('status', $this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => [
				'defaultOrder' => 't.sort DESC',
			]
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cases the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return bool
	 */
	public function beforeDelete()
	{
		// чтобы удалились файлики
		foreach ((array)$this->images as $image) {
			$image->delete();
		}

		return parent::beforeDelete();
	}

	/**
	 * @return ProductImage[]
	 */
	public function getImages()
	{
		return $this->images;
	}

	/**
	 * @return array
	 */
	public function getStatusList()
	{
		return [
			self::STATUS_ACTIVE => 'Активен',
			self::STATUS_NOT_ACTIVE => 'Не активен',
		];
	}

	/**
	 * @return string
	 */
	public function getStatusTitle()
	{
		$data = $this->getStatusList();

		return isset($data[$this->status]) ? $data[$this->status] : Yii::t('StoreModule.store', '*unknown*');
	}

	/**
	 *
	 */
	protected function afterFind()
	{
		if($this->date){
			$this->date = date('d.m.Y', strtotime($this->date));
		}

		parent::afterFind();
	}

	/**
	 * @return bool
	 */
	protected function beforeSave()
	{
		$this->date = Helper::isDate($this->date, 'd.m.Y') ? date('Y-m-d', strtotime($this->date)) : null;
		return parent::beforeSave();
	}

	/**
	 *
	 */
	public function afterSave()
	{
		parent::afterSave();
	}

	/**
	 * @return string
	 */
	public function getMetaTitle(){
		return $this->meta_title ?: Helper::prepareSeoTitle($this->name);
	}

	/**
	 * @return string
	 */
	public function getTitle(){
		return $this->title_h1 ?: $this->name;
	}
}
