<?php

/**
 *
 * @property integer $id
 * @property integer $case_id
 * @property string $name
 *
 * @property-read Cases $case
 * @method getImageUrl($width = 0, $height = 0, $options = [])
 */
class CasesImage extends \webforma\models\WModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{cases_image}}';
    }


    /**
     * @param null|string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['case_id', 'numerical', 'integerOnly' => true],
	        ['name', 'length', 'max' => 250],
        ];
    }


    /**
     * @return array
     */
    public function relations()
    {
        return [
            'case' => [self::BELONGS_TO, 'Cases', 'case_id'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $module = Yii::app()->getModule('Case');

        return [
            'imageUpload' => [
                'class' => 'webforma\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'name',
                'uploadPath' => $module->uploadPath.'/cases',
            ],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('StoreModule.store', 'Image title'),
        ];
    }

    /**
     * @return array customized attribute descriptions (name=>description)
     */
    public function attributeDescriptions()
    {
        return [];
    }
}
