<?php
/**
 * CasesModule основной класс модуля case
 */

class CasesModule  extends webforma\components\WebModule
{
	/**
	 * @var int
	 */
    public $adminMenuOrder = 200;

	/**
	 * @var
	 */
	public $title = 'Кейсы';

	/**
	 * @var
	 */
	public $titleWidget = 'Кейсы';

	/**
	 * @var
	 */
	public $metaTitle;

	/**
	 * @var
	 */
	public $metaDescription;

	/**
	 * @var
	 */
	public $metaKeyWords;

	/**
	 * @var
	 */
	public $perPage = 10;

	/**
	 * @var
	 */
	public $descriptionText;


    /**
     * Каждый модуль должен принадлежать одной категории, именно по категориям делятся модули в панели управления
     *
     * @return string
     */
    public function getCategory()
    {
        return 'Контент';
    }

    /**
     * массив лейблов для параметров (свойств) модуля. Используется на странице настроек модуля в панели управления.
     *
     * @return array
     */
    public function getParamsLabels()
    {
        return [
        	'perPage' => 'Элементов на странице',
        	'descriptionText' => 'Описание вверху страницы',
        	'title' => 'Заголовок на странице',
        	'titleWidget' => 'Заголовок в виджете',
        	'metaTitle' => 'Title страницы',
        	'metaDescription' => 'Description страницы',
        	'metaKeyWords' => 'Keywords страницы',
        ];
    }

    /**
     * массив параметров модуля, которые можно редактировать через панель управления (GUI)
     *
     * @return array
     */
    public function getEditableParams()
    {
        return [
	        'perPage',
	        'descriptionText' => [
		        'input-type' => 'editor',
	        ],
	        'title',
	        'titleWidget',
	        'metaTitle',
	        'metaDescription',
	        'metaKeyWords',
        ];
    }

    /**
     * массив групп параметров модуля, для группировки параметров на странице настроек
     *
     * @return array
     */
    public function getEditableParamsGroups()
    {
	    return [
		    [
			    'label' => 'Общие настройки',
			    'items' => [
				    'perPage',
				    'descriptionText',
			    ]
		    ],
	    	[
	    		'label' => 'CEO настройки',
			    'items' => [
				    'title',
				    'metaTitle',
				    'metaDescription',
				    'metaKeyWords',
			    ]
		    ]
	    ];
    }

    /**
     * если модуль должен добавить несколько ссылок в панель управления - укажите массив
     *
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => 'Кейсы',
                'url' => ['/cases/casesBackend/index']
            ],
	        [
		        'icon' => 'fa fa-fw fa-plus-square',
		        'label' => 'Добавить кейс',
		        'url' => ['/cases/casesBackend/create']
	        ],
        ];
    }

    /**
     * Возвращает название модуля
     *
     * @return string.
     */
    public function getName()
    {
        return 'Кейсы';
    }

    /**
     * Ссылка, которая будет отображена в панели управления
     * Как правило, ведет на страничку для администрирования модуля
     *
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/cases/casesBackend/index';
    }

    /**
     * Название иконки для меню админки, например 'user'
     *
     * @return string
     */
    public function getIcon()
    {
        return "fa fa-fw fa-briefcase";
    }

    /**
     * Инициализация модуля, считывание настроек из базы данных и их кэширование
     *
     * @return void
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'cases.models.*',
            ]
        );
    }
}
