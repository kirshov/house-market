<?php

class m140814_000018_cases_image_base extends webforma\components\DbMigration
{
    public function safeUp()
    {
        $this->createTable(
            "{{cases_image}}",
            [
                "id" => "pk",
                "case_id" => "integer not null",
                "name" => "varchar(250) not null",
            ],
            $this->getOptions()
        );

        //fk
        $this->addForeignKey("fk_{{cases_image}}_case", "{{cases_image}}", "case_id", "{{case}}", "id", "CASCADE", "CASCADE");
    }

    public function safeDown()
    {
        $this->dropTable("{{cases_image}}");
    }
}
