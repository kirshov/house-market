<?php

class m200224_195240_add_meta extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{case}}', 'title_h1', 'varchar(255) default null');
		$this->addColumn('{{case}}', 'meta_title', 'varchar(255) default null');
		$this->addColumn('{{case}}', 'meta_keywords', 'text default null');
		$this->addColumn('{{case}}', 'meta_description', 'text default null');
	}

	public function safeDown()
	{

	}
}