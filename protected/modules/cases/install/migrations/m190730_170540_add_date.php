<?php

class m190730_170540_add_date extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{case}}', 'date', 'date default null');
	}

	public function safeDown()
	{

	}
}