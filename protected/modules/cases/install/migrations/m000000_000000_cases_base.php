<?php
/**
 * Case install migration
 * Класс миграций для модуля Case:
 **/
class m000000_000000_cases_base extends webforma\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        $this->createTable(
            '{{case}}',
            [
                'id'             => 'pk',
                'name' => 'varchar(255) default null',
                'image' => 'varchar(255) default null',
	            'short_description' => 'varchar(255) default null',
	            'description' => 'text default null',
	            'slug' => 'varchar(150) not null',
	            'sort' => 'integer NOT NULL DEFAULT 1',
	            'status' => 'tinyint NOT NULL DEFAULT 1',

                'create_time'    => 'datetime NOT NULL',
                'update_time'    => 'datetime NOT NULL',
            ],
            $this->getOptions()
        );

	    $this->createIndex("ux_{{case}}_slug", "{{case}}", "slug", true);
	    $this->createIndex("ix_{{case}}_status", "{{case}}", "status", false);
    }


    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{case}}');
    }
}
