<?php
/**
 * Файл настроек для модуля case
 */
return [
    'module'    => [
        'class' => 'application.modules.cases.CasesModule',
    ],
    'import'    => [],
    'component' => [],
	'params' => [
		'widgets' => [
			'cases' => 'application.modules.cases.widgets.CasesWidget',
		]
	],
    'rules'     => [
        '/cases' => 'cases/cases/index',
	    [
		    'cases/cases/view',
		    'pattern' => '/cases/<slug>',
	    ],
    ],
];