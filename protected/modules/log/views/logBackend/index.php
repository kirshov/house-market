<?php
/**
 * @var $this LogBackendController
 * @var $model Log
 */
$this->breadcrumbs = [
    'Журнал действий' => ['/log/logBackend/index'],
   	'Список',
];

$this->pageTitle = 'Журнал действий';

/*$this->menu = [
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('ContentBlockModule.contentblock', 'Content blocks administration'),
        'url' => ['/contentblock/contentBlockBackend/index']
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('ContentBlockModule.contentblock', 'Add content block'),
        'url' => ['/contentblock/contentBlockBackend/create']
    ],
];*/
?>
<?/*
<p class="search-wrap">
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        Найти запись
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php
    Yii::app()->clientScript->registerScript(
        'search',
        "
    $('.search-form form').submit(function () {
        $.fn.yiiGridView.update('log-grid', {
            data: $(this).serialize()
        });

        return false;
    });
"
    );
    $this->renderPartial('_search', ['model' => $model]);
    ?>
</div>
*/?>
<?php $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'log-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'hideBulkActions' => true,
        'columns' => [
	        [
		        'name' => 'creation_date',
		        'type' => 'raw',
		        'value' => function ($data) {
			        return CHtml::link(date('d.m.Y H:i:s', strtotime($data->creation_date)), ["/log/logBackend/view", "id" => $data->id]);
		        },
	        ],
            [
                'name' => 'model',
                'type' => 'raw',
	            'value' => function ($data) {
		            return CHtml::link($data->getTitleModel(), ["/log/logBackend/view", "id" => $data->id]);
	            },
                //'value' => '$data->getType()',
                /*'filter' => CHtml::activeDropDownList(
                    $model,
                    'type',
                    $model->getTypes(),
                    ['class' => 'form-control', 'empty' => '']
                ),*/
            ],
	        [
		        'name' => 'model_id',
		        'type' => 'raw',
		        'value' => function ($data) {
    				return $data->model_id
						? CHtml::link($data->model_id, ['/log/logBackend/index', 'Log[model_id]' => $data->model_id])
						: ' - ';
		        },
	        ],
			[
				'name' => 'user_ip',
				'type' => 'raw',
				'value' => function ($data) {
					return CHtml::link($data->user_ip, ["/log/logBackend/view", "id" => $data->id]);
				},
			],
			[
				'name' => 'user_id',
				'type' => 'raw',
				'value' => function ($data) {
					return CHtml::link($data->user_id, ["/log/logBackend/view", "id" => $data->id]);
				},
			],
	        [
		        'name' => 'action',
		        'type' => 'raw',
		        'value' => function ($data) {
			        return CHtml::link($data->getTitleAction(), ["/log/logBackend/view", "id" => $data->id]);
		        },
	        ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
				'template' => '{view}{delete}',
				'buttons' => [
					'delete' => [
						'visible' => function(){
							return DEVMODE;
						},
					],
				],
            ],
        ],
    ]
); ?>
<style>
	.checkbox-column{display: none;}
</style>