<?php

class LogModule extends webforma\components\WebModule
{
	/**
	 * @var int
	 */
	public $adminMenuOrder = 100;

    /**
     * @return string
     */
    public function getCategory()
    {
    	return 'Сервисы';
    }

    /**
     * @return string
     */
    public function getName()
    {
    	return 'Журнал действий';
        //return Yii::t('ContentBlockModule.contentblock', 'Blocks');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        //return Yii::t('ContentBlockModule.contentblock', 'Module for create simple content blocks');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return "fa fa-fw fa-list-alt";
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'log.models.*',
            ]
        );
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/log/logBackend/index';
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            /*[
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ContentBlockModule.contentblock', 'Blocks list'),
                'url' => ['/contentblock/contentBlockBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ContentBlockModule.contentblock', 'Add block'),
                'url' => ['/contentblock/contentBlockBackend/create'],
            ],*/
        ];
    }
}
