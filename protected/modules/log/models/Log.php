<?php

/**
 * Модель Log
 **/
/**
 * This is the model class for table "Log".
 *
 * The followings are the available columns in table 'Log':
 * @property integer $id
 * @property string $creation_date
 * @property integer $code
 * @property integer $type
 * @property string $model
 * @property integer $model_id
 * @property string $user_ip
 * @property string $description
 * @property integer $user_id
 * @property integer $action
 *
 * @property Category|null $category
 *
 * @method Log active()
 */
class Log extends CActiveRecord
{
	const CODE_PAYMENT_REQUEST = 50;

	//public $isAddLog = false;

    /**
     * Returns the static model of the specified AR class.
     * @param  string $className
     * @return Log the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{logs}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['model, code, type, model_id, description, user_id, action, user_ip', 'filter', 'filter' => 'trim'],
            ['code, model', 'required'],
            ['model, code, model_id, description', 'safe', 'on' => 'search'],
        ];
    }

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'CTimestampBehavior' => [
				'class' => 'zii.behaviors.CTimestampBehavior',
				'setUpdateOnCreate' => true,
				'createAttribute' => 'creation_date',
				'updateAttribute' => null,
			],
		];
	}

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
        	'creation_date' => 'Дата',
        	'model' => 'Тип',
        	'model_id' => 'Id записи',
        	'action' => 'Действие',
        	'user_ip' => 'IP адрес',
        	'user_id' => 'Пользователь',
            /*'id' => Yii::t('LogModule.Log', 'id'),
            'name' => Yii::t('LogModule.Log', 'Title'),
            'code' => Yii::t('LogModule.Log', 'Code'),
            'type' => Yii::t('LogModule.Log', 'Type'),
            'content' => Yii::t('LogModule.Log', 'Content'),
            'description' => Yii::t('LogModule.Log', 'Description'),
            'category_id' => Yii::t('LogModule.Log', 'Category'),
            'status' => Yii::t('LogModule.Log', 'Status'),*/
            ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria();
        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.code', $this->code, true);
        $criteria->compare($this->tableAlias . '.model', $this->model);
        $criteria->compare($this->tableAlias . '.user_ip', $this->user_ip, true);
        $criteria->compare($this->tableAlias . '.user_id', $this->user_id);
        $criteria->compare($this->tableAlias . '.model_id', $this->model_id);
        $criteria->compare($this->tableAlias . '.description', $this->description, true);

        return new CActiveDataProvider(get_class($this), [
        	'criteria' => $criteria,
	        'sort' => [
		        'defaultOrder' => 't.creation_date DESC',//$module->getDefaultSort(),
	        ],
        ]);
    }

	protected function afterFind()
	{
		if($this->type == 'json'){
			$this->description = json_decode($this->description, true);
		}

		parent::afterFind();
	}


	/**
	 * @param array $data
	 * @param bool $addToFile
	 */
    public static function add($data, $addToFile = false){
		$model = new Log();
		$data = CMap::mergeArray($data, [
			'user_id' => Yii::app()->getUser()->getId(),
			'user_ip' => Yii::app()->getRequest()->getUserHostAddress(),
		]);
		$model->setAttributes(self::prepareData($data));

		$result = $model->save();
		if(!$result){
			Log::addToFile(print_r($model->getErrors(), true));
		}

		if($addToFile){
			Log::addToFile(print_r($data, true));
		}
    }

	/**
	 * @param string $data
	 * @param string $fileName
	 */
	public static function addToFile($data, $fileName = 'user_log.log'){
		$path = Yii::app()->getRuntimePath().DIRECTORY_SEPARATOR.'user_logs';
		if(!is_dir($path)){
			CFileHelper::createDirectory($path);
		}
		$file = $path.DIRECTORY_SEPARATOR.$fileName;
		file_put_contents($file, date('d.m.Y H:i:s')."\n".$data."\n\n", FILE_APPEND);
	}

	/**
	 * @param $action
	 * @return mixed
	 */
	public function getTitleAction($action = null){
		if($action === null){
			$action = $this->action;
		}
		$data = [
			'create' => 'Создание',
			'update' => 'Редактирование',
			'saveModulesettings' => 'Изменение настроек модуля',
			'backendlogin' => 'Авторизация',
		];

		return $data[$action] ?: $action;
	}

	/**
	 * @param $model
	 * @return mixed
	 */
	public function getTitleModel($model = null){
		if($model === null){
			$model = $this->model;
		}
		$data = [
			'Page' => 'Страница',
			'Product' => 'Товар',
			'StoreCategory' => 'Категория товара',
			'Article' => 'Статья',
			'News' => 'Новости',
			'Delivery' => 'Способ доставки',
			'Order' => 'Заказ',
			'User' => 'Пользователь',
		];

		return $data[$model] ?: $model;
	}

	/**
	 * @param array $data
	 * @return array
	 */
	public static function prepareData($data){

		if($data['action'] == 'backendlogin' && !$data['user_id']){
			$data['user_id'] = $data['model_id'];
		}
		return $data;
	}
}