<?php
/**
 * Файл конфигурации модуля
 **/
return [
    'module'    => [
        'class' => 'application.modules.log.LogModule',
    ],
	'component' => [
		'diff' => [
			'class' => 'application.modules.log.components.Diff'
		],
	],
    'import'    => [
    	'application.modules.log.models.Log',
    ],
];
