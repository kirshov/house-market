<?php

class m180916_044336_change_fields extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->alterColumn('{{logs}}', 'action_type', 'varchar(255) default null');
		$this->renameColumn('{{logs}}', 'action_type', 'action');
		$this->execute('UPDATE {{logs}} SET action = NULL WHERE action = 0');

		$this->addForeignKey("fk_{{logs}}_user_id", "{{logs}}", "user_id", "{{user_user}}", "id", "SET NULL", "CASCADE");
	}

	public function safeDown()
	{

	}
}