<?php

class m000000_000000_log_base extends webforma\components\DbMigration
{

    public function safeUp()
    {
        $this->createTable(
            '{{logs}}',
            [
                'id'          => 'pk',
	            'creation_date'  => 'datetime NOT NULL',
                'code'        => 'integer(11) NOT NULL DEFAULT 0',
	            'type'        => 'varchar(32) DEFAULT NULL',
	            'model'        => 'varchar(32) DEFAULT NULL',
                'model_id'        => "integer(11) NOT NULL DEFAULT 0",
                'description' => "TEXT DEFAULT NULL"
            ],
            $this->getOptions()
        );

    }

    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{logs}}');
    }
}
