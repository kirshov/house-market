<?php

class m190913_083036_add_ip extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{logs}}', 'user_ip', 'varchar(15) default null');
	}

	public function safeDown()
	{

	}
}