<?php

class m180915_113844_add_fields extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{logs}}', 'action_type','integer(11) not null default 0');
		$this->addColumn('{{logs}}', 'user_id','integer(11) default null');
		$this->alterColumn('{{logs}}', 'model_id', 'integer(11) default null');
	}

	public function safeDown()
	{

	}
}