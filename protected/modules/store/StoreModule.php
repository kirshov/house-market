<?php

use webforma\components\WebModule;

/**
 * Class StoreModule
 */
class StoreModule extends WebModule
{


	/**
	 *
	 */
	const PRICE_TYPE_NORMAL = 1;

	/**
	 *
	 */
	const PRICE_TYPE_CHARGE = 2;

	/**
	 *
	 */
	const PRODUCTS_SPECIAL_TYPE_SELECT = 1;

	/**
	 *
	 */
	const PRODUCTS_SPECIAL_TYPE_DISCOUNT = 2;

	/**
	 *
	 */
	const PRODUCTS_NEW_TYPE_SELECT = 1;

	/**
	 *
	 */
	const PRODUCTS_NEW_TYPE_LAST = 2;


	/**
     *
     */
    public $adminMenuOrder = 1;

	/**
	 *
	 */
	public $adminSettingsAsTab = true;

    /**
     * Общее название магазина
     * @var
     */
    public $title;

    /**
     * @var string
     */
    public $currency = 'RUB';

    /**
     * @var int
     */
    public $minCost = 0;

    /**
     * @var string
     */
    public $uploadPath = 'store';

    /**
     * @var string
     */
    public $assetsPath = 'application.modules.store.views.assets';

    /**
     * @var int
     */
    public $itemsPerPage = 20;

    /**
     * @var string
     */
    public $defaultSort = 'position';

    /**
     * @var string
     */
    public $defaultSortDirection = 'ASC';

	/**
	 * Название магазина в реквизитах
	 * @var string
	 */
	public $shopTitle;

	/**
	 * @var string
	 */
	public $shopRequisites;

    /**
     * @var
     */
    public $metaTitle;
    /**
     * @var
     */
    public $metaDescription;
    /**
     * @var
     */
    public $metaKeyWords;
	/**
	 * @var
	 */
	public $storeTopPageText;
    /**
     * @var
     */
    public $storePageText;
	/**
	 *
	 */
	public $producerUrl;
	/**
	 *
	 */
	public $metaProducerH1;

	/**
	 * @var
	 */
	public $metaProducerTitle;
	/**
	 * @var
	 */
	public $metaProducerDescription;
	/**
	 * @var
	 */
	public $metaProducerKeyWords;

	/**
	 * @var
	 */
	public $producerPageText;

	/**
     *
     */
	public $metaCategoryTitle;

	/**
     *
     */
	public $metaCategoryDescription;

	/**
     *
     */
	public $metaCategoryKeyWords;

    /**
     *
     */
    public $metaProductH1;

	/**
     *
     */
	public $metaProductTitle;

	/**
     *
     */
	public $metaProductDescription;

	/**
     *
     */
	public $metaProductKeyWords;

	/**
	 *
	 */
	public $countryUrl;

    /**
     *
     */
    public $metaCountryTitle;

    /**
     *
     */
    public $metaCountryDescription;

    /**
     *
     */
    public $metaCountryKeyWords;

    /**
     *
     */
    public $metaCountryH1;


    /**
     *
     */
    public $metaProducerItemTitle;

    /**
     *
     */
    public $metaProducerItemDescription;

    /**
     *
     */
    public $metaProducerItemKeyWords;

    /**
     *
     */
    public $metaProducerItemH1;

    /**
     *
     */
    public $metaProducerLineTitle;

    /**
     *
     */
    public $metaProducerLineDescription;

    /**
     *
     */
    public $metaProducerLineKeyWords;

    /**
     *
     */
    public $metaProducerLineH1;

	/**
	 *
	 */
	public $productSpecialType = 1;

	/**
	 *
	 */
	public $productSpecialUrl;

	/**
	 *
	 */
    public $metaSpecialTitle;

	/**
	 *
	 */
    public $metaSpecialDescription;

	/**
	 *
	 */
    public $metaSpecialKeywords;

	/**
	 *
	 */
	public $productNewType = 1;

	/**
	 * @var
	 */
	public $productNewUrl;

	/**
	 *
	 */
	public $metaNewTitle;

	/**
	 *
	 */
	public $metaNewDescription;

	/**
	 *
	 */
	public $metaNewKeywords;

	/**
	 *
	 */
	public $productGroupUrl;

	/**
	 *
	 */
	public $productGroupH1;

	/**
	 *
	 */
	public $metaProductGroupTitle;

	/**
	 *
	 */
	public $metaProductGroupDescription;

	/**
	 *
	 */
	public $metaProductGroupKeywords;

	/**
	 *
	 */
	public $metaProductGroupTitleTemplate;

	/**
	 * @var int
	 */
	public $priceType = self::PRICE_TYPE_NORMAL;

	/**
	 * @var int
	 */
	public $useOriginalName = 0;

	/**
	 * @var int
	 */
	public $useProductGroups = 0;

	/**
	 * @var int
	 */
	public $useProductTabs = 0;

	/**
	 * @var
	 */
	public $productTabs;

	/**
	 * @var int
	 */
	public $useProductUnit = 0;

	/**
	 * @var int
	 */
	public $notProductIndexDescription = 0;

	/**
	 * @var int
	 */
	public $roundPrice = 0;

	/**
	 * @var int
	 */
	public $useSimilar = 1;

	/**
	 * @var int
	 */
	public $showSimilar = 1;

	/**
	 * @var string
	 */
	public $similarTitle;

	/**
	 * @var string
	 */
	public $similarDefaultTitle = 'Похожие товары';

	/**
	 * @var int
	 */
	public $similarCount = 10;

	/**
	 * @var bool
	 */
	public $showProductsSubCats = true;

	/**
	 * @var int
	 */
	public $checkCartQuantity = 0;

	/**
	 * @var string
	 */
	public $zeroPriceTitle;

	/**
	 * @var int
	 */
	public $productListImageWidth = 160;

	/**
	 * @var int
	 */
	public $productListImageHeight = 160;

	/**
	 * @var int
	 */
	public $productListImageCrop = 0;

	/**
	 * @var int
	 */
	public $productImageWidth = 460;

	/**
	 * @var int
	 */
	public $productImageHeight = 460;

	/**
	 * @var int
	 */
	public $productImageCrop = 0;

	/**
	 * @var int
	 */
	public $categoryListImageWidth = 160;

	/**
	 * @var int
	 */
	public $categoryListImageHeight = 160;

	/**
	 * @var int
	 */
	public $categoryListImageCrop = 0;

	/**
	 * @var string
	 */
	public $viewType = 'block';

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getUploadPath()
    {
        return Yii::getPathOfAlias('webroot').'/'.Yii::app()->getModule(
            'webforma'
        )->uploadPath.'/'.$this->uploadPath;
    }

    /**
     * @return array|bool
     */
    public function checkSelf()
    {
        $messages = [];

        return isset($messages[WebModule::CHECK_ERROR]) ? $messages : true;
    }

    /**
     * @return bool
     */
    public function getInstall()
    {
        if (parent::getInstall()) {
            @mkdir($this->getUploadPath(), 0755);
        }

        return true;
    }

    /**
     * @return array
     */
    public function getCurrencyList()
    {
        return [
	        'RUB' => 'RUB',
	        /*'USD' => 'USD',
			'EUR' => 'EUR',
			'KZT' => 'KZT',
			'BYB' => 'BYB',
			'UAH' => 'UAH',*/
        ];
    }

    /**
     * @return array
     */
    public function getSortList()
    {
        return [
            'position' => Yii::t('StoreModule.store', 'Position'),
            'sku' => Yii::t('StoreModule.store', 'SKU'),
            'name' => Yii::t('StoreModule.store', 'Name'),
            'price' => Yii::t('StoreModule.store', 'Price'),
            'create_time' => Yii::t('StoreModule.store', 'Create time'),
            'update_time' => Yii::t('StoreModule.store', 'Update time'),
        ];
    }

    public function getSortDirectionList()
    {
        return [
            'ASC' => Yii::t('StoreModule.store', 'ASC'),
            'DESC' => Yii::t('StoreModule.store', 'DESC'),
        ];
    }

	/**
	 * @return array
	 */
	public function getParamsLabels()
	{
		return [
			'uploadPath' => Yii::t(
				'StoreModule.store',
				'File uploads directory (relative to "{path}")',
				['{path}' => Yii::getPathOfAlias('webroot').'/'.Yii::app()->getModule("webforma")->uploadPath]
			),
			'itemsPerPage' => Yii::t('StoreModule.store', 'Items per page'),
			'title' => Yii::t('StoreModule.store', 'Name'),
			'defaultSort' => Yii::t('StoreModule.store', 'Default sort'),
			'defaultSortDirection' => Yii::t('StoreModule.store', 'Default sort direction'),
			'minCost' => 'Минимальная сумма заказа',

			'shopTitle' => 'Название юр. лица или магазина',
			'shopRequisites' => 'Реквизиты',

			'storeTopPageText' => 'Текст вверху на странице магазина',
			'storePageText' => 'Текст на странице магазина',
			'metaTitle' => 'Тег title для родительского раздела магазина',
			'metaDescription' => 'Тег description для родительского раздела магазина',
			'metaKeyWords' => 'Тег keywords для родительского раздела магазина',

			'metaCategoryTitle' => 'Шаблон title для категории магазина',
			'metaCategoryDescription' => 'Шаблон description для категории магазина',
			'metaCategoryKeyWords' => 'Шаблон keywords для категории магазина',

			'metaProductH1' => 'Шаблон H1 для страницы товара',
			'metaProductTitle' => 'Шаблон title для страницы товара',
			'metaProductDescription' => 'Шаблон description для страницы товара',
			'metaProductKeyWords' => 'Шаблон keywords для страницы товара',

			'productSpecialType' => 'Выводить спецпредложения',
			'productSpecialUrl' => 'URL адрес раздела спецпредложений',
			'metaSpecialTitle' => 'Тег title для раздела спецпредложений',
			'metaSpecialDescription' => 'Тег description для раздела спецпредложений',
			'metaSpecialKeywords' => 'Тег keywords для раздела спецпредложений',

			'productNewType' => 'Выводить новинки',
			'productNewUrl' => 'URL адрес раздела новинок',
			'metaNewTitle' => 'Тег title для раздела новинок',
			'metaNewDescription' => 'Тег description для раздела новинок',
			'metaNewKeywords' => 'Тег keywords для раздела новинок',

			'producerUrl' => 'URL адрес раздела брендов',
			'metaProducerH1' => 'Тег H1 раздела брендов',
			'metaProducerTitle' => 'Тег title раздела брендов',
			'metaProducerDescription' => 'Тег description раздела брендов',
			'metaProducerKeyWords' => 'Тег keywords раздела брендов',
			'producerPageText' => 'Текст раздела брендов',

			'countryUrl' => 'URL адрес раздела стран',
			'metaCountryH1' => 'Тег H1 для раздела стран',
			'metaCountryTitle' => 'Тег title для раздела стран',
			'metaCountryDescription' => 'Тег description для раздела стран',
			'metaCountryKeyWords' => 'Тег keywords для раздела стран',

			'metaProducerItemTitle' => 'Шаблон title для бренда',
			'metaProducerItemDescription' => 'Шаблон description для бренда',
			'metaProducerItemKeyWords' => 'Шаблон keywords для бренда',
			'metaProducerItemH1' => 'Шаблон H1 на странце бренда',

			'metaProducerLineTitle' => 'Шаблон title для линии бренда',
			'metaProducerLineDescription' => 'Шаблон description для линии бренда',
			'metaProducerLineKeyWords' => 'Шаблон keywords для линии бренда',
			'metaProducerLineH1' => 'Шаблон H1 на странце линии бренда',

			'productGroupUrl' => 'URL для основной страницы групп товаров',
			'productGroupH1' => 'H1 для основной страницы групп товаров',
			'metaProductGroupTitle' => 'Title для основной страницы групп товаров',
			'metaProductGroupKeywords' => 'Keywords для основной страницы групп товаров',
			'metaProductGroupDescription' => 'Descriptions для основной страницы групп товаров',
			'metaProductGroupTitleTemplate' => 'Шаблон title для групп товаров',

			'priceType' => 'Способ формирования цен:',
			'useOriginalName' => 'Использовать оригинальное название товара',
			'useProductGroups' => 'Использовать группы товаров',
			'useProductTabs' => 'Использовать дополнительные вкладки в товарах',
			'useProductUnit' => 'Использовать единицы измеения',
			'notProductIndexDescription' => 'Не индексировать описание позиции',
			'roundPrice' => 'Округлять цены',

			'useSimilar' => 'Использовать похожие товары',
			'showSimilar' => 'Выводить похожие товары',
			'similarTitle' => 'Заголовок похожих товаров',
			'similarCount' => 'Количество похожих товаров',

			'showProductsSubCats' => 'Выводить товары из вложенных категорий',
			'checkCartQuantity' => 'Не добавлять в корзину больше товаров, чем есть в наличии',

			'zeroPriceTitle' => 'Текст вместе цены, если цена ровна 0',

			'productListImageWidth' => 'Ширина изображения товара в списке',
			'productListImageHeight' => 'Высота изображения товара в списке',
			'productListImageCrop' => 'Подгонять по размеру изображения товара в списке',
			'productImageWidth' => 'Ширина изображения товара в карточке',
			'productImageHeight' => 'Высота изображения товара в карточке',
			'productImageCrop' => 'Подгонять по размеру изображения товара в карточке',
			'categoryListImageWidth' => 'Ширина изображения категории в списке',
			'categoryListImageHeight' => 'Высота изображения категории в списке',
			'categoryListImageCrop' => 'Подгонять по размеру изображения категории в списке',

			'viewType' => 'Тип вывода домов по умолчанию',
		];
	}

	/**
     * @return array
     */
    public function getEditableParams()
    {
	    $shopName = Yii::t('StoreModule.store', 'Catalog');

        $items = [
            'itemsPerPage',
            //'currency' => $this->getCurrencyList(),
            'title' => [
	            'input-type' => 'text',
	            'htmlOptions' => [
		            'placeholder' => $shopName,
	            ],
	            'hint' => 'Общее название магазина',
            ],
            'minCost',
            'defaultSort' => $this->getSortList(),
            'defaultSortDirection' => $this->getSortDirectionList(),
			'roundPrice' => [
				Product::ROUND_PRICE_NONE => 'Не округлять цены',
				Product::ROUND_PRICE_SIMPLE => 'Округлять до целых',
				Product::ROUND_PRICE_DEC => 'Округлять до 5 рублей у товаров дешевле 1000, и до 10 рублей у товаров дороже 1000',
			],

	        'shopTitle' => [
		        'input-type' => 'text',
		        'htmlOptions' => [
		        	'placeholder' => Yii::app()->getModule('webforma')->getShortSiteName(),
		        ],
		        'hint' => 'Используется при печати заказа',
	        ],

	        'storeTopPageText' => [
	        	'input-type' => 'editor',
	        ],
	        'storePageText' => [
	        	'input-type' => 'editor',
	        ],

	        'shopRequisites' => [
	        	'input-type' => 'textarea',
	        ],
			'notProductIndexDescription' => [
	        	'input-type' => 'checkbox',
	        ],

	        'showProductsSubCats' => [
		        'input-type' => 'checkbox',
	        ],

		    'metaTitle',
            'metaDescription',
            'metaKeyWords',

            'metaCategoryTitle',
            'metaCategoryDescription',
            'metaCategoryKeyWords',
			'metaCategoryNotice' => [
				'input-type' => 'html',
				'html' => 'Используйте следующие конструкции для автоподстановки:<br/>
					<b>{name}</b> - название категории',
				'htmlOptions' => [
					'class' => 'help-block',
				]
			],

            'metaProductH1',
            'metaProductTitle',
            'metaProductDescription',
            'metaProductKeyWords',
			'metaProductNotice' => [
				'input-type' => 'html',
				'html' => 'Используйте следующие конструкции для автоподстановки:<br/>
 					<b>{name}</b> - название товара<br/> 
 					<b>{category}</b> - название категории<br/> 
 					'.($this->useOriginalName ? '<b>{original}</b> - оригинальное название товара<br/>' : '').'  
 					<b>{brand}</b> - название бренда<br/>',
				'htmlOptions' => [
					'class' => 'help-block',
				]
			],

            'producerUrl' => [
				'input-type' => 'text',
				'htmlOptions' => [
					'placeholder' => $this->getProducerUrl(true),
				]
			],
            'metaProducerH1',
            'metaProducerTitle',
            'metaProducerDescription',
            'metaProducerKeyWords',
	        'producerPageText' => [
		        'input-type' => 'editor',
	        ],

	        'metaProducerItemH1',
            'metaProducerItemTitle',
            'metaProducerItemDescription',
            'metaProducerItemKeyWords',
			'metaProducerItemNotice' => [
				'input-type' => 'html',
				'html' => 'Используйте следующие конструкции для автоподстановки:<br/>
 					<b>{name}</b> - название бренда<br/>',
				'htmlOptions' => [
					'class' => 'help-block',
				]
			],


	        'productSpecialType' => [
	        	self::PRODUCTS_SPECIAL_TYPE_SELECT => 'Отмеченные как "Спецпредложение"',
	        	self::PRODUCTS_SPECIAL_TYPE_DISCOUNT => 'Отмеченные как "Спецпредложение" и все товары со скидкой',
			],
	        'productSpecialUrl' => [
				'input-type' => 'text',
				'htmlOptions' => [
					'placeholder' => $this->getProductSpecialUrl(true),
				]
			],
	        'metaSpecialTitle',
	        'metaSpecialDescription',
	        'metaSpecialKeywords',

			'productNewType' => [
				self::PRODUCTS_NEW_TYPE_SELECT => 'Отмеченные как "Новинка"',
				self::PRODUCTS_NEW_TYPE_LAST => 'Отмеченные как "Новинка" и последние добавленные товары',
			],
	        'productNewUrl' => [
				'input-type' => 'text',
				'htmlOptions' => [
					'placeholder' => $this->getProductNewUrl(true),
				]
			],
	        'metaNewTitle',
	        'metaNewDescription',
	        'metaNewKeywords',

			'zeroPriceTitle',

			'viewType' => [
				'block' => 'Блоками',
				'list' => 'Списком',
			],
        ];

        if(Helper::hasMidTariff()){
        	$items = CMap::mergeArray($items, [
		        'countryUrl' => [
					'input-type' => 'text',
					'htmlOptions' => [
						'placeholder' => $this->getCountryUrl(true),
					]
				],
		        'metaCountryH1',
		        'metaCountryTitle',
		        'metaCountryDescription',
		        'metaCountryKeyWords',

		        'metaProducerLineH1',
		        'metaProducerLineTitle',
		        'metaProducerLineDescription',
		        'metaProducerLineKeyWords',

		        'metaProducerLineNotice' => [
			        'input-type' => 'html',
			        'html' => 'Используйте следующие конструкции для автоподстановки:<br/>
 					<b>{name}</b> - название линии<br/>   
 					<b>{brand}</b> - название бренда',
			        'htmlOptions' => [
				        'class' => 'help-block',
			        ]
		        ],
	        ]);
        }

        if($this->useProductGroups){
        	$items = CMap::mergeArray($items, [
				'productGroupUrl' => [
					'input-type' => 'text',
					'htmlOptions' => [
						'placeholder' => $this->getProductGroupUrl(true),
					]
				],
				'productGroupH1',
				'metaProductGroupTitle',
				'metaProductGroupKeywords',
				'metaProductGroupDescription',
		        'metaProductGroupTitleTemplate',
		        'metaProductGroupTitleTemplateHint' => [
			        'input-type' => 'html',
			        'html' => 'Используйте следующие конструкции для автоподстановки:<br/>
 					<b>{name}</b> - название группы',
			        'htmlOptions' => [
				        'class' => 'help-block',
			        ]
		        ],
			]);
		}

	    if($this->useProductTabs){
		    $items = CMap::mergeArray($items, [
			    'productTabs' => [
				    'input-type' => 'widget',
				    'widget' => 'tab-settings',
				    'options' => [
					    'delete-route' => '/store/productBackend/deleteTab',
					    'presets' => [
						    '' => 'Индивидуальный текст',
						    'text' => 'Предустановленный текст',
						    'description' => 'Описание товара',
						    'characteristic' => 'Характеристики',
						    'comments' => 'Комментарии',
					    ],
				    ],
			    ],
		    ]);
	    }

	    if($this->useSimilar){
		    $items = CMap::mergeArray($items, [
			    'showSimilar' => [
			    	ProductLink::SIMILAR_TYPE_NONE => 'Не выводить',
			    	ProductLink::SIMILAR_TYPE_ONLY_SELECT => 'Выводить указанные',
			    	ProductLink::SIMILAR_TYPE_ADVANCED => 'Дополнять указанные товарами из этой же категории',
			    ],
			    'similarTitle' => [
			    	'input-type' => 'text',
				    'htmlOptions' => [
					    'placeholder' => $this->similarDefaultTitle,
				    ]
			    ],
			    'similarCount',
		    ]);
	    }

        if(DEVMODE){
        	$items = CMap::mergeArray($items, [
				'priceType' => $this->getPriceTypes(),
				'useOriginalName' => [
					'input-type' => 'checkbox',
				],
				'useProductGroups' => [
					'input-type' => 'checkbox',
				],
		        'useProductTabs' => [
			        'input-type' => 'checkbox',
		        ],
		        'useProductUnit' => [
			        'input-type' => 'checkbox',
		        ],
		        'useSimilar' => [
			        'input-type' => 'checkbox',
		        ],

				'checkCartQuantity' => [
			        'input-type' => 'checkbox',
		        ],

				'productListImageWidth',
				'productListImageHeight',
				'productImageWidth',
				'productImageHeight',
				'categoryListImageWidth',
				'categoryListImageHeight',

				'productListImageCrop' => [
					'input-type' => 'checkbox',
				],
				'productImageCrop' => [
					'input-type' => 'checkbox',
				],
				'categoryListImageCrop' => [
					'input-type' => 'checkbox',
				],

				'hr' => [
					'input-type' => 'html',
					'html' => '<hr/>',
					'htmlOptions' => [
						'style' => 'margin-top: -15px;',
					]
				]
			]);

		}

		return $items;
    }

	/**
	 * @return array
	 */
	public function getEditableParamsTabs(){
		$tabs = [
			'0.store' => [
				'label' => 'Общие настройки',
				'items' => [
					'0.store',
					'0.similarSettings',
					'0.requisites',
					'0.system',
				],
			],
			'3.seo' => [
				'label' => Yii::t('StoreModule.store', 'SEO setting'),
				'items' => [
					'3.1.1.seo',
					'3.1.2.seo',
					'3.1.3.seo',
				],
			],
			'3.2.seo' => [
				'label' => 'SEO брендов',
				'items' => [
					'3.2.1.seo',
					'3.2.2.seo',
					'3.2.3.seo',
					'3.2.4.seo',
				]
			],
			'4.seoSpecial' => [
				'label' => 'SEO новинок и спецпредложений',
				'items' => [
					'4.2.seoNews',
					'4.1.seoSpecial',
				],
			],
		];

		if($this->useProductGroups){
			$tabs = CMap::mergeArray($tabs, [
				'6.seoGroups' => [
					'label' => 'SEO групп товаров',
					'items' => [
						'6.seoGroups',
					],
				],
			]);
		}

		if($this->useProductTabs){
			$tabs = CMap::mergeArray($tabs, [
				'7.tabs' => [
					'label' => 'Доп. вкладки в товарах',
					'items' => [
						'8.tabsSettings',
					],
				],
			]);
		}

		if(DEVMODE){
			$tabs = CMap::mergeArray($tabs, [
				'8.image_size' => [
					'label' => 'Размеры изображений',
					'items' => [
						'image_size',
					],
				],
			]);
		}

		return $tabs;
	}

    /**
     * @return array
     */
    public function getEditableParamsGroups()
    {

    	$groups = [
            '0.store' => [
                'label' => 'Общие настройки',
                'items' => [
	                'title',
                    //'currency',
					'roundPrice',
                    'minCost',
	                'showProductsSubCats',
                    'itemsPerPage',
                    'defaultSort',
                    'defaultSortDirection',
                    'viewType',
                    'zeroPriceTitle',
                ],
            ],
		    '0.requisites' => [
			    'label' => 'Настройки реквизитов для печати заказа',
			    'items' => [
				    'shopTitle',
				    'shopRequisites',
			    ],
		    ],
		    '3.1.1.seo' => [
                'label' => 'Настройки SEO основной страницы магазина',
                'items' => [
                    'metaTitle',
                    'metaDescription',
                    'metaKeyWords',
                    'storeTopPageText',
                    'storePageText',
                ],
            ],
			'3.1.2.seo' => [
				'label' => 'Настройки SEO шаблона страниц категорий',
				'items' => [
					'metaCategoryTitle',
					'metaCategoryDescription',
					'metaCategoryKeyWords',
					'metaCategoryNotice',
				],
			],
			'3.1.3.seo' => [
				'label' => 'Настройки SEO шаблона страниц товаров',
				'items' => [
					'notProductIndexDescription',
					'metaProductH1',
					'metaProductTitle',
					'metaProductDescription',
					'metaProductKeyWords',
					'metaProductNotice',
				],
			],
            '3.2.1.seo' => [
                'label' => 'Настройки SEO раздела брендов',
                'items' => [
                   	'producerUrl',
                   	'metaProducerH1',
                    'metaProducerTitle',
                    'metaProducerDescription',
                    'metaProducerKeyWords',
                    'producerPageText',
                ]
            ],
			'3.2.2.seo' => [
				'label' => 'Настройки SEO шаблона страниц бренда',
				'items' => [
					'metaProducerItemH1',
					'metaProducerItemTitle',
					'metaProducerItemDescription',
					'metaProducerItemKeyWords',
					'metaProducerItemNotice',
				]
			],

	        '4.1.seoSpecial' => [
		        'label' => 'Настройки спецпредложений',
		        'items' => [
			        'productSpecialType',
			        'productSpecialUrl',
			        'metaSpecialTitle',
			        'metaSpecialDescription',
			        'metaSpecialKeywords',
		        ],
	        ],
	        '4.2.seoNews' => [
		        'label' => 'Настройки новинок',
		        'items' => [
			        'productNewType',
			        'productNewUrl',
			        'metaNewTitle',
			        'metaNewDescription',
			        'metaNewKeywords',
		        ],
	        ],
        ];

    	if(Helper::hasMidTariff()){
		    $groups['3.2.3.seo'] = [
			    'label' => 'Настройка шаблона SEO страниц линейки бренда',
			    'items' => [
				    'metaProducerLineH1',
				    'metaProducerLineTitle',
				    'metaProducerLineDescription',
				    'metaProducerLineKeyWords',
				    'metaProducerLineNotice',
			    ]
		    ];

		    $groups['3.2.4.seo'] = [
			    'label' => 'Настройка шаблона SEO страниц страны',
			    'items' => [
				    'countryUrl',
				    'metaCountryH1',
				    'metaCountryTitle',
				    'metaCountryDescription',
				    'metaCountryKeyWords',
			    ]
		    ];
	    }


    	if($this->useProductGroups){
    		$groups['6.seoGroups'] = [
				'label' => 'Настройки SEO страницы групп товаров',
				'items' => [
					'productGroupUrl',
					'productGroupH1',

					'metaProductGroupTitle',
					'metaProductGroupDescription',
					'metaProductGroupKeywords',
					'metaProductGroupTitleTemplate',
					'metaProductGroupTitleTemplateHint',
				],
			];
		}

	    if($this->useSimilar){
		    $groups['0.similarSettings'] = [
			    'label' => 'Настройки похожих товаров',
			    'items' => [
				    'showSimilar',
				    'similarTitle',
				    'similarCount',
			    ],
		    ];
	    }

    	if($this->useProductTabs){
    		$groups['8.tabsSettings'] = [
				'label' => 'Настройка дополнительных вкладок товара',
				'items' => [
					'productTabs',
				],
			];
		}

    	if(DEVMODE){
			$groups['0.system'] = [
				'label' => 'Системные настройки',
				'items' => [
					'priceType',
					'useSimilar',
					'useOriginalName',
					'useProductGroups',
					'useProductUnit',
					'useProductTabs',
					'checkCartQuantity',
				]
			];

			$groups['image_size'] = [
				'label' => 'Настройки размеров изображений',
				'items' => [
					'productListImageWidth',
					'productListImageHeight',
					'productListImageCrop',
					'hr',
					'productImageWidth',
					'productImageHeight',
					'productImageCrop',
					'hr',
					'categoryListImageWidth',
					'categoryListImageHeight',
					'categoryListImageCrop',
				]
			];
		}

    	return $groups;
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [];
    }

    /**
     * @return bool
     */
    public function getIsShowInAdminMenu()
    {
        return true;
    }

    /**
     * @return array
     */
    public function getExtendedNavigation()
    {
    	$hasMidTariff = Helper::hasMidTariff();
        return [
            [
                'icon' => 'fa fa-fw fa-shopping-cart',
                'label' => Yii::t('StoreModule.store', 'Catalog'),
                'items' => [
                    [
                        'icon' => 'fa fa-fw fa-reorder',
                        'label' => Yii::t('StoreModule.store', 'Products'),
                        'url' => ['/store/productBackend/index'],
                        'items' => [
                            [
                                'icon' => 'fa fa-fw fa-list-alt',
                                'label' => Yii::t('StoreModule.store', 'Product list'),
                                'url' => ['/store/productBackend/index'],
                            ],
                            [
                                'icon' => 'fa fa-fw fa-plus-square',
                                'label' => Yii::t('StoreModule.store', 'Create product'),
                                'url' => ['/store/productBackend/create'],
                            ],
                            [
                                'icon' => 'fa fa-fw fa-link',
                                'label' => Yii::t('StoreModule.store', 'Link types'),
                                'url' => ['/store/linkBackend/typeIndex'],
								'visible' => DEVMODE,
                            ],
                        ],
                    ],
					[
						'icon' => 'fa fa-fw fa-building',
						'label' => Yii::t('StoreModule.store', 'Producers'),
						'url' => ['/store/producerBackend/index'],
						'items' => [
							[
								'label' => 'Производители',
								'visible' => $hasMidTariff,
							],
							[
								'icon' => 'fa fa-fw fa-list-alt',
								'label' => Yii::t('StoreModule.store', 'Producers list'),
								'url' => ['/store/producerBackend/index'],
							],
							[
								'icon' => 'fa fa-fw fa-plus-square',
								'label' => Yii::t('StoreModule.store', 'Create producer'),
								'url' => ['/store/producerBackend/create'],
							],

							[
								'label' => 'Линии',
								'visible' => $hasMidTariff,
							],
							[
								'icon' => 'fa fa-fw fa-list-alt',
								'label' => Yii::t('StoreModule.store', 'Manage producers line'),
								'url' => ['/store/producerLineBackend/index'],
								'visible' => $hasMidTariff,
							],
							[
								'icon' => 'fa fa-fw fa-plus-square',
								'label' => Yii::t('StoreModule.store', 'Create producer line'),
								'url' => ['/store/producerLineBackend/create'],
								'visible' => $hasMidTariff,
							],
							[
								'label' => 'Страны поставщиков',
								'visible' => $hasMidTariff,
							],
							[
								'icon' => 'fa fa-fw fa-list-alt',
								'label' => Yii::t('StoreModule.store', 'Country list'),
								'url' => ['/store/producerCountryBackend/index'],
								'visible' => $hasMidTariff,
							],
							[
								'icon' => 'fa fa-fw fa-plus-square',
								'label' => Yii::t('StoreModule.store', 'Create country'),
								'url' => ['/store/producerCountryBackend/create'],
								'visible' => $hasMidTariff,
							],
						],
					],
                    /*[
                        'icon' => 'fa fa-fw fa-folder-open',
                        'label' => Yii::t('StoreModule.store', 'Categories'),
                        'url' => ['/store/categoryBackend/index'],
                        'items' => [
                            [
                                'icon' => 'fa fa-fw fa-list-alt',
                                'label' => Yii::t('StoreModule.store', 'Categories list'),
                                'url' => ['/store/categoryBackend/index'],
                            ],
                            [
                                'icon' => 'fa fa-fw fa-plus-square',
                                'label' => Yii::t('StoreModule.store', 'Create category'),
                                'url' => ['/store/categoryBackend/create'],
                            ],
                        ],
                    ],
					[
						'icon' => 'fa fa-fw fa-cube',
						'label' => Yii::t('StoreModule.store', 'Groups product'),
						'url' => ['/store/productGroupsBackend/index'],
						'items' => [
							[
								'icon' => 'fa fa-fw fa-list-alt',
								'label' => Yii::t('StoreModule.store', 'Groups list'),
								'url' => ['/store/productGroupsBackend/index'],
							],
							[
								'icon' => 'fa fa-fw fa-plus-square',
								'label' => Yii::t('StoreModule.store', 'Create group'),
								'url' => ['/store/productGroupsBackend/create'],
							],
						],
						'visible' => $this->useProductGroups,
					],

					[
						'icon' => 'fa fa-fw fa-truck',
						'label' => Yii::t('StoreModule.store', 'Providers'),
						'url' => ['/store/providerBackend/index'],
						'items' => [
							[
								'icon' => 'fa fa-fw fa-list-alt',
								'label' => Yii::t('StoreModule.store', 'Providers list'),
								'url' => ['/store/providerBackend/index'],
							],
							[
								'icon' => 'fa fa-fw fa-plus-square',
								'label' => Yii::t('StoreModule.store', 'Create provider'),
								'url' => ['/store/providerBackend/create'],
							],
						],
					],*/
					[
						'icon' => 'fa fa-fw fa-pencil-square-o',
						'label' => Yii::t('StoreModule.store', 'Attributes'),
						'url' => ['/store/attributeBackend/index'],
						'items' => [
							[
								'icon' => 'fa fa-fw fa-list-alt',
								'label' => Yii::t('StoreModule.store', 'Attributes list'),
								'url' => ['/store/attributeBackend/index'],
							],
							[
								'icon' => 'fa fa-fw fa-plus-square',
								'label' => Yii::t('StoreModule.store', 'Create attribute'),
								'url' => ['/store/attributeBackend/create'],
							],
						],
					],
					/*[
						'icon' => 'fa fa-fw fa-list-alt',
						'label' => Yii::t('StoreModule.store', 'Types'),
						'url' => ['/store/typeBackend/index'],
						'items' => [
							[
								'icon' => 'fa fa-fw fa-list-alt',
								'label' => Yii::t('StoreModule.store', 'Types list'),
								'url' => ['/store/typeBackend/index'],
							],
							[
								'icon' => 'fa fa-fw fa-plus-square',
								'label' => Yii::t('StoreModule.store', 'Create type'),
								'url' => ['/store/typeBackend/create'],
							],
						],
					],*/
				],
			],
		];
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
		$params = $this->getSettingsUrl();
		$uri = array_shift($params);
    	return Yii::app()->createUrl($uri, $params);
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('StoreModule.store', 'Store');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('StoreModule.store', 'Store');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('StoreModule.store', 'Store');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-shopping-cart';
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport([
            'application.modules.store.models.*',
            'application.modules.store.forms.*',
            'application.modules.store.components.*',
            'application.modules.sitemap.components.SitemapHelper',
        ]);
    }

	/**
	 * @return string
	 */
    public function getShopTitle(){
    	if($this->shopTitle){
    		return $this->shopTitle;
	    }

	    return Yii::app()->getModule('webforma')->getShortSiteName();
    }

    /**
     * Returns default product sort attribute and direction
     *
     * @param string $table
     * @return string
     */
    public function getDefaultSort($table = 't')
    {
        return $table . '.' . $this->defaultSort . ' ' . $this->defaultSortDirection;
    }

	/**
	 * @return int|string
	 */
    public function getItemsPerPage(){
        $currentPerPage = Yii::app()->getRequest()->getParam('show-by', null);

        if(!$currentPerPage){
            $currentPerPage = Yii::app()->getRequest()->cookies['store-per-page']->value;
        }

	    return is_numeric($currentPerPage) ? $currentPerPage : $this->itemsPerPage;
    }

    /**
     * @return mixed
     */
    public function getPrepareShowBy(){
        $showBy = Yii::app()->getRequest()->getParam('show-by');
        if(is_numeric($showBy) && ($showBy % $this->itemsPerPage == 0) && $showBy < ($this->itemsPerPage * 3)){
            return $showBy;
        }
        return false;
    }

	/**
	 * @return string
	 */
	public function getViewType(){
		$currentViewType = Yii::app()->getRequest()->getParam('view-type', null);

		if(!$currentViewType){
			$currentViewType = Yii::app()->getRequest()->cookies['store-view-type']->value;
		}

		return $currentViewType && in_array($currentViewType, ['list', 'block']) ? $currentViewType : $this->viewType;
	}

	/**
     * @return array
     */
    public function getYesNo(){
    	return [
		    1 => 'Да',
		    0 => 'Нет',
	    ];
    }

	/**
	 * @return bool
	 */
    public function isGetFilterParams(){
	    $attributes = Yii::app()->getComponent('attributesRepository')->getAllAttributes();
	    $attributes = CMap::mergeArray($attributes, Yii::app()->getComponent('attributesFilter')->getMainSearchParams());
	    $attributes = CMap::mergeArray($attributes, [
	    	'page' => true,
		    'sort' => true,
	    ]);

	    foreach ($_GET as $key => $item) {
			if(isset($attributes[$key])){
				return true;
			}
	    }

	    return false;
    }

	/**
	 * @return array
	 */
    public function getPriceTypes(){
    	return [
    		self::PRICE_TYPE_NORMAL => 'Обычный',
		    self::PRICE_TYPE_CHARGE => 'По наценке',
	    ];
    }

	/**
	 * @param bool $getDefault
	 * @return string
	 */
    public function getProducerUrl($getDefault = false){
    	$default = 'brands';
    	if($getDefault){
    		return $default;
		}

    	return $this->producerUrl ?: $default;
	}

	/**
	 * @param bool $getDefault
	 * @return string
	 */
	public function getCountryUrl($getDefault = false){
		$default = 'country';
		if($getDefault){
			return $default;
		}

		return $this->countryUrl ?: $default;
	}

	/**
	 * @param bool $getDefault
	 * @return string
	 */
	public function getProductNewUrl($getDefault = false){
		$default = 'new-product';
		if($getDefault){
			return $default;
		}

		return $this->productNewUrl ?: $default;
	}

	/**
	 * @param bool $getDefault
	 * @return string
	 */
	public function getProductSpecialUrl($getDefault = false){
		$default = 'sales-product';
		if($getDefault){
			return $default;
		}

		return $this->productSpecialUrl ?: $default;
	}

	/**
	 * @param bool $getDefault
	 * @return string
	 */
	public function getProductGroupUrl($getDefault = false){
		$default = 'groups';
		if($getDefault){
			return $default;
		}

		return $this->productGroupUrl ?: $default;
	}
}
