<?php

/**
 * @var $model ProductLinkType
 */

$this->breadcrumbs = [
    Yii::t('StoreModule.store', 'Link types') => ['/store/linkBackend/typeIndex'],
    Yii::t('StoreModule.store', 'Manage'),
];

$this->pageTitle = Yii::t('StoreModule.store', 'Link types - manage');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('StoreModule.store', 'Manage link types'), 'url' => ['/store/linkBackend/typeIndex']],
];
?>

<?php $this->widget('bootstrap.widgets.TbButton', [
	'buttonType' => 'button',
	'context' => 'success',
	'label' => '<i class="glyphicon glyphicon-plus">&nbsp;</i> Добавить тип',
	'id' => 'add-site-map',
	'encodeLabel' => false,
	'htmlOptions' => [
		'data-target' => '#add-toggle',
		'data-toggle' => 'collapse',
	]
]); ?>

<div id="add-toggle" class="collapse <?= $model->hasErrors() ? 'in' : 'out' ?> add-form">
	<p></p>
    <?php
    /* @var $form TbActiveForm */
    $form = $this->beginWidget(
        'bootstrap.widgets.TbActiveForm',
        [
            'id' => 'question-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'action' => ['/store/linkBackend/typeCreate'],
            'type' => 'vertical',
            'htmlOptions' => ['class' => 'well'],
        ]
    ); ?>

    <?= $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->textFieldGroup($model, 'code'); ?>
        </div>
        <div class="col-sm-6">
            <?= $form->textFieldGroup($model, 'title'); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-2">
            <?php $this->widget(
                'bootstrap.widgets.TbButton',
                [
                    'buttonType' => 'submit',
                    'context' => 'primary',
                    'label' => Yii::t('StoreModule.store', 'Create'),
                ]
            ); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>

</div>

<?php $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'question-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'actionsButtons' => false,
        'hideBulkActions' => true,
        'columns' => [
            [
                'class' => 'webforma\widgets\WEditableColumn',
                'name' => 'code',
                'editable' => [
                    'url' => $this->createUrl('/store/linkBackend/typeInline'),
                    'mode' => 'popup',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter' => CHtml::activeTextField($model, 'code', ['class' => 'form-control']),
            ],
            [
                'class' => 'webforma\widgets\WEditableColumn',
                'name' => 'title',
                'editable' => [
                    'url' => $this->createUrl('/store/linkBackend/typeInline'),
                    'mode' => 'popup',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter' => CHtml::activeTextField($model, 'title', ['class' => 'form-control']),
            ],
            [
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'template' => '{delete}',
                'buttons' => [
                    'delete' => [
                        'url' => function ($data) {
                            return Yii::app()->createUrl('/store/linkBackend/typeDelete', ['id' => $data->id]);
                        }
                    ]
                ]
            ],
        ],
    ]
); ?>
