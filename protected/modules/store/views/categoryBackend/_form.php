<?php
/**
 * @var $this CatalogBackendController
 * @var $model StoreCategory
 * @var $form \webforma\widgets\ActiveForm
 */
$form = $this->beginWidget(
    '\webforma\widgets\ActiveForm',
    [
        'id'                     => 'category-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => 'vertical',
		'htmlOptions' => ['class' => 'well sticky', 'enctype' => 'multipart/form-data'],
		'clientOptions' => [
			'validateOnSubmit' => true,
		],
    ]
); ?>
<div class="alert alert-info">
    <?= Yii::t('StoreModule.store', 'Fields with'); ?>
    <span class="required">*</span>
    <?= Yii::t('StoreModule.store', 'are required'); ?>
</div>

<?= $form->errorSummary($model); ?>

<div class='row'>
    <div class="col-sm-6">
		<div class='row'>
			<div class="col-sm-6">
			    <?= $form->dropDownListGroup($model, 'parent_id', [
				    'widgetOptions' => [
					    'data' => StoreCategoryHelper::formattedList(),
					    'htmlOptions' => [
						    'empty' => Yii::t('StoreModule.store', '--no--'),
						    'encode' => false,
					    ],
				    ],
			    ]); ?>
			</div>
			<div class="col-sm-6">
			    <?= $form->dropDownListGroup($model, 'status', [
				    'widgetOptions' => [
					    'data' => $model->getStatusList(),
				    ],
			    ]); ?>
			</div>
		</div>

		<div class='row'>
			<div class="col-sm-12">
			    <?= $form->textFieldGroup($model, 'name'); ?>
			</div>
		</div>

		<div class='row'>
			<div class="col-sm-12">
			    <?if($model->isNewRecord){
				    echo $form->slugFieldGroup($model, 'slug', ['sourceAttribute' => 'name']);
			    } else {
				    echo $form->textFieldGroup($model, 'slug');
			    }?>
			</div>
		</div>
    </div>

	<div class="col-sm-6">
		<div class='row'>
			<div class="col-sx-8 col-sm-8 col-sm-offset-2">
				<?php $this->widget('webforma\widgets\WInputFile', [
					'model' => $model,
					'attribute' => 'image',
				]); ?>
			</div>
		</div>
	</div>
</div>

<div class='row'>
    <div class="col-sm-12 <?= $model->hasErrors('description') ? 'has-error' : ''; ?>">
        <?= $form->labelEx($model, 'description'); ?>
        <?php $this->widget($this->module->getVisualEditor(), [
			'model'     => $model,
			'attribute' => 'description',
		]); ?>
        <p class="help-block"></p>
    </div>
</div>

<div class='row'>
    <div class="col-sm-12 <?= $model->hasErrors('short_description') ? 'has-error' : ''; ?>">
        <?= $form->labelEx($model, 'short_description'); ?>
        <?php $this->widget($this->module->getVisualEditor(), [
			'model'     => $model,
			'attribute' => 'short_description',
		]); ?>
        <p class="help-block"></p>
    </div>
</div>

<?php $collapse = $this->beginWidget('bootstrap.widgets.TbCollapse'); ?>
<div class="panel-group" id="extended-options">
	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="panel-title">
				<a data-toggle="collapse" data-parent="#advanced-options" href="#advanced">
					Дополнительные настройки
				</a>
			</div>
		</div>
		<div id="advanced" class="panel-collapse collapse">
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-6">
				
						<?= $form->checkboxGroup($model, 'not_filter'); ?>
					</div>
				</div>
				<?/*
				<div class="row">
					<div class="col-sm-6">
						<br/>
						<?= $form->checkboxGroup($model, 'on_main'); ?>
					</div>
					<div class="col-sm-6">
						<?= $form->textFieldGroup($model, 'sort_main'); ?>
					</div>
				</div>*/?>
				<div class="row">
					<div class="col-sm-6">
						<?$typeRoot = [-1 => '--- Нет ---', 0 => '--- Как у родительской ---'];?>
						<?= $form->dropDownListGroup($model, 'product_type', [
							'widgetOptions' => [
								'data' => CMap::mergeArray($typeRoot, CHtml::listData(Type::model()->findAll(), 'id', 'name')),
							],
						]); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>

<?php $collapse = $this->beginWidget('bootstrap.widgets.TbCollapse'); ?>
<div class="panel-group" id="extended-options">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">
                <a data-toggle="collapse" data-parent="#extended-options" href="#collapseOne">
                    <?= Yii::t('StoreModule.store', 'Data for SEO'); ?>
                </a>
            </div>
        </div>
        <div id="collapseOne" class="panel-collapse collapse">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->textFieldGroup($model, 'title'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->textFieldGroup($model, 'meta_title'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->textAreaGroup($model, 'meta_description'); ?>
                    </div>
                </div>
				<div class="row">
					<div class="col-sm-6">
			            <?= $form->textFieldGroup($model, 'meta_keywords'); ?>
					</div>
				</div>
                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->textFieldGroup($model, 'meta_canonical'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>

<?if(Yii::app()->getModule('store')->useProductGroups):?>
	<?php $collapse = $this->beginWidget('bootstrap.widgets.TbCollapse'); ?>
	<div class="panel-group" id="extended-groups_meta">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-title">
					<a data-toggle="collapse" data-parent="#extended-groups_meta" href="#groups_meta">
						Данные для поисковой оптимизации в разделе групп товаров
					</a>
				</div>
			</div>
			<div id="groups_meta" class="panel-collapse collapse">
				<div class="panel-body">
					<?if(!$model->parent_id):?>
						<div class="row">
							<div class="col-sm-6">
								<?= $form->textFieldGroup($model, 'groups_alias'); ?>
							</div>
						</div>
					<?endif;?>
					<div class="row">
						<div class="col-sm-6">
							<?= $form->textFieldGroup($model, 'groups_name'); ?>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<?= $form->textFieldGroup($model, 'groups_h1'); ?>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<?= $form->textFieldGroup($model, 'groups_meta_title'); ?>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<?= $form->textAreaGroup($model, 'groups_meta_description'); ?>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<?= $form->textFieldGroup($model, 'groups_meta_keywords'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php $this->endWidget(); ?>
<?endif?>

<div class="buttons">
    <?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context'    => 'primary',
		'label'      => $model->isNewRecord ? Yii::t('StoreModule.store', 'Create category and continue') : Yii::t('StoreModule.store', 'Save category and continue'),
	]); ?>

    <?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType'  => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label'       => $model->isNewRecord ? Yii::t('StoreModule.store', 'Create category and close') : Yii::t('StoreModule.store', 'Save category and close'),
	]); ?>

    <?php $this->endWidget(); ?>
</div>