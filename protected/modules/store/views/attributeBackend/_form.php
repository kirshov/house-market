<?php
/**
 * @var $this AttributeBackendController
 * @var $model Attribute
 * @var $form \webforma\widgets\ActiveForm
 */
?>

<?php
/**
 * @var $model Attribute
 */

$form = $this->beginWidget(
    '\webforma\widgets\ActiveForm',
    [
        'id' => 'attribute-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => 'vertical',
		'htmlOptions' => ['class' => 'well sticky'],
		'clientOptions' => [
			'validateOnSubmit' => true,
		],
    ]
); ?>
<style>
	.checkbox-column{display: none;}
</style>

<div class="alert alert-info">
    <?= Yii::t('StoreModule.store', 'Fields with'); ?>
    <span class="required">*</span>
    <?= Yii::t('StoreModule.store', 'are required'); ?>
</div>

<?= $form->errorSummary($model); ?>
<div class='row'>
	<div class="col-sm-7">
		<div class='row'>
			<div class="col-sm-12">
				<?= $form->textFieldGroup($model, 'title'); ?>
			</div>
			<?/*
			<div class="col-sm-3">
				<?= $form->slugFieldGroup($model, 'name', ['sourceAttribute' => 'title']); ?>
			</div>
			*/?>
		</div>


		<div class='row'>
			<div class="col-sm-6">
				<?= $form->dropDownListGroup($model, 'type', [
					'widgetOptions' => [
						'data' => $model->getTypesList(),
						'htmlOptions' => [
							'empty' => '---',
							'id' => 'attribute-type',
						],
					],
				]); ?>
			</div>

			<div class="col-sm-6">
				<?= $form->dropDownListGroup(
					$model,
					'group_id',
					[
						'widgetOptions' => [
							'data' => AttributeGroup::model()->getFormattedList(),
							'htmlOptions' => [
								'empty' => '---',
							],
						],
					]
				); ?>
			</div>
		</div>

		<div class='row'>
			<div class="col-sm-12">
				<?= $form->textAreaGroup($model, 'description', ['rows' => 30, 'class' => 'form-control']); ?>
			</div>
		</div>

		<div class='row'>
			<div class="col-sm-6">
				<?= $form->textFieldGroup($model, 'unit'); ?>
			</div>
		</div>

		<?= $form->hiddenField($model, 'is_filter', ['value' => $model->getIsNewRecord() ? 0 : $model->is_filter]); ?>
		<?/*
		<div class='row'>
			<div class="col-sm-12">
				<?= $form->checkBoxGroup($model, 'is_general'); ?>
				<div class="help-block small">
					Будет выводится в корзине и письме с деталями заказа
				</div>
			</div>
		</div>

		<div class='row'>
			<div class="col-sm-12">
				<?= $form->checkBoxGroup($model, 'append_name'); ?>
			</div>
		</div>

		<div class='row'>
			<div class="col-sm-6">
				<?= $form->checkBoxGroup($model, 'required'); ?>
			</div>

			<?/*
			<div class="col-sm-6">
				<?= $form->checkBoxGroup($model, 'is_filter'); ?>
			</div>
			* /?>
		</div>
		*/?>
		<?/*
		<div class='row'>
			<div class="col-sm-6">
				<?= $form->checkBoxGroup($model, 'hide_in_site'); ?>
			</div>

			<div class="col-sm-6">
				<?= $form->checkBoxGroup($model, 'use_on_similar'); ?>
			</div>
		</div>


		<div class='row'>
			<div class="col-sm-12">
				<?php $collapse = $this->beginWidget('bootstrap.widgets.TbCollapse'); ?>
				<div class="panel-group" id="extended-options">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="panel-title">
								<a data-toggle="collapse" data-parent="#extended-options" href="#collapseOne">
									Дополнительные опции для Яндекс.Маркет
								</a>
							</div>
						</div>
						<div id="collapseOne" class="panel-collapse collapse">
							<div class="panel-body">
								<div class='row'>
									<div class="col-sm-12">
										<?= $form->textFieldGroup($model, 'yml_field'); ?>
										<div class="help-block small">
											Выгрузить отдельным атрибутом. Подробнее: <a href="https://yandex.ru/support/partnermarket/elements/vendor-name-model.html">https://yandex.ru/support/partnermarket/elements/vendor-name-model.html</a>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<?= $form->checkBoxGroup($model, 'yml_add_name'); ?>
										<div class="help-block small">Добавить значение атрибута к названию товара</div>
									</div>
									<div class="col-sm-6">
										<?= $form->checkBoxGroup($model, 'yml_add_name_on_variant'); ?>
										<div class="help-block small">Добавить значение атрибута к названию товара только если существет несколько вариантов одного товара с этим атрибутом</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<?= $form->checkBoxGroup($model, 'yml_group_variant'); ?>
										<div class="help-block small">Создает в выгрузке несколько товаров в одной группе, если существет несколько вариантов одного товара с этим атрибутом</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<?= $form->checkBoxGroup($model, 'yml_add_unit'); ?>
										<div class="help-block small">
											Последнее слово в характеристикие будет использовано как единица измерения, например, если в списке значение заведено как "100 мл", то в выгрзку(в элемент param) попадет значение параметра: "100", а единица измерения "мл". Более подробно смотрите в документации YML
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php $this->endWidget(); ?>
			</div>
		</div>
		<br>
		*/?>
		<?php if ($model->getIsNewRecord()): ?>
			<div class="row">
				<div id="options" class="<?= !$model->isMultipleValues() ? 'hidden' : ''; ?> col-sm-12">
					<div class="row form-group">
						<div class="col-sm-12">
							<label class="control-label">Список опций</label>
							<div class="help-block" style="margin: 0;"><?= Yii::t("StoreModule.store", "Each option value must be on a new line."); ?></div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<?= CHtml::activeTextArea($model, 'rawOptions', ['rows' => 10, 'class' => 'form-control']); ?>
						</div>
					</div>
				</div>
			</div>
		<?php else: ?>
			<div id="options" class="<?= !$model->isMultipleValues() ? 'hidden' : ''; ?>">
				<div class="row">
					<div class="form-group">
						<div class="col-sm-8">
							<label class="control-label">Список характеристик</label>
						</div>
					</div>

					<div class="col-md-8">
						<?= CHtml::textField('AttributeOption[name]', null, ['class' => 'form-control', 'placeholder' => 'Введите название опции',]); ?>
					</div>
					<div class="col-md-2">
						<?= CHtml::button(Yii::t('StoreModule.store', 'Add'), [
							'class' => 'btn btn-success',
							'id' => 'add-option-btn',
							'data-url' => Yii::app()->createUrl('/store/attributeBackend/addOption', ['id' => $model->id]),
						]); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<?php $this->widget(
							'webforma\widgets\CustomGridView',
							[
								'hideBulkActions' => true,
								'id' => 'attributes-options-grid',
								'sortableRows' => true,
								'sortableAjaxSave' => true,
								'sortableAttribute' => 'position',
								'sortableAction' => '/store/attributeBackend/sortoptions',
								'type' => 'condensed',
								'ajaxUrl' => Yii::app()->createUrl('/store/attributeBackend/update', ['id' => $model->id]),
								'template' => "{items}\n{pager}<br/><br/>",
								'dataProvider' => new CActiveDataProvider('AttributeOption',
									[
										'criteria' => [
											'condition' => 'attribute_id = :id',
											'params' => [':id' => $model->id],
										],
										'pagination' => false,
										'sort' => [
											'defaultOrder' => 'position ASC',
										],
									]
								),
								'columns' => [
									[
										'class' => 'webforma\widgets\WEditableColumn',
										'name' => 'value',
										'editable' => [
											'url' => $this->createUrl('/store/attributeBackend/inline'),
											'mode' => 'popup',
											'params' => [
												Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->getCsrfToken(),
											],
										],
									],
									[
										'class' => 'webforma\widgets\CustomButtonColumn',
										'template' => '{delete}',
										'buttons' => [
											'delete' => [
												'url' => function ($data) {
													return Yii::app()->createUrl('/store/attributeBackend/deleteOption',
														['id' => $data->id]);
												},
												'options' => [
													'class' => 'delete btn-sm btn-default',
												],
											],
										],
									],
								],
							]
						) ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<div class="col-sm-1"></div>
	<div class="col-sm-4">
		<?php if (!empty($types)): ?>
			<div class="row">
				<div class="col-sm-12">
					<label class="control-label"><?= Yii::t('StoreModule.store', 'Use in types'); ?></label>
				</div>
				<div class="col-sm-12">
					<?php foreach ($types as $type): ?>
						<div class="checkbox">
							<label>
								<?= CHtml::checkBox('types[]', array_key_exists($type->id, $model->getTypes()), ['value' => $type->id]) ?>
								<?= CHtml::encode($type->name); ?>
							</label>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		<?php endif; ?>
	</div>
</div>
<script type="text/javascript">
    $(function () {
        $('#add-option-btn').on('click', function (event) {
            event.preventDefault();
            var data = $('#AttributeOption_name').val();
            if (data) {
                $.post($(this).data('url'), {
                    'value': data,
                    '<?= Yii::app()->getRequest()->csrfTokenName;?>': '<?= Yii::app()->getRequest()->getCsrfToken()?>'
                }, function (response) {
                    $.fn.yiiGridView.update('attributes-options-grid');
                }, 'json');
            }
        });

        $('#attribute-type').change(function () {
            if ($.inArray(parseInt($(this).val()), [<?= Attribute::TYPE_DROPDOWN;?>, <?= Attribute::TYPE_RADIO;?>, <?= Attribute::TYPE_CHECKBOX_LIST;?>, <?= Attribute::TYPE_COLOR;?>]) >= 0) {
                $('#options').removeClass('hidden');
            } else {
                $('#options').addClass('hidden');
            }
        });
    });
</script>

<div class="buttons">
    <?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context' => 'primary',
		'label' => $model->getIsNewRecord() ? Yii::t('StoreModule.store',
			'Add attribute and continue') : Yii::t('StoreModule.store', 'Save attribute and continue'),
	]); ?>

    <?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label' => $model->getIsNewRecord() ? Yii::t('StoreModule.store',
			'Add attribute and close') : Yii::t('StoreModule.store', 'Save attribute and close'),
	]); ?>
</div>
<?php $this->endWidget(); ?>