<?php
$this->breadcrumbs = [
    Yii::t('StoreModule.store', 'Providers') => ['/store/providerBackend/index'],
    Yii::t('StoreModule.store', 'Manage'),
];

$this->pageTitle = Yii::t('StoreModule.store', 'Providers - manage');

$this->menu = [
    [
        'icon' => 'fa fa-fw fa-truck',
        'label' => Yii::t('StoreModule.store', 'Manage providers'),
        'url' => ['/store/providerBackend/index'],
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('StoreModule.store', 'Create provider'),
        'url' => ['/store/providerBackend/create'],
    ],
];
?>
<?php
$this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'provider-grid',
        'sortableRows' => true,
        'sortableAjaxSave' => true,
        'sortableAttribute' => 'sort',
        'sortableAction' => '/store/providerBackend/sortable',
        'type' => 'condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => [
	        [
		        'class' => 'webforma\widgets\WEditableColumn',
		        'name' => 'name',
		        'editable' => [
			        'emptytext' => '---',
			        'url' => $this->createUrl('/store/providerBackend/inline'),
			        'mode' => 'popup',
			        'params' => [
				        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
			        ]
		        ],
		        'filter' => CHtml::activeTextField($model, 'name', ['class' => 'form-control']),
	        ],
			[
		        'class' => 'webforma\widgets\WEditableColumn',
		        'name' => 'extra_charge',
		        'editable' => [
			        'emptytext' => '---',
			        'url' => $this->createUrl('/store/providerBackend/inline'),
			        'mode' => 'popup',
			        'params' => [
				        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
			        ]
		        ],
		        'filter' => CHtml::activeTextField($model, 'extra_charge', ['class' => 'form-control']),
	        ],
            /*[
                'class' => 'webforma\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('/store/providerBackend/inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    Provider::STATUS_ACTIVE => ['class' => 'label-success'],
	                Provider::STATUS_NOT_ACTIVE => ['class' => 'label-default'],
                ],
            ],*/
            [
                'header' => Yii::t('StoreModule.store', 'Products'),
                'value' => function ($data) {
                    return CHtml::link(
                        $data->productCount,
                        ['/store/productBackend/index', 'Product[provider_id]' => $data->id],
                        ['class' => 'badge']
                    );
                },
                'type' => 'raw',
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
				'template' => '{update} {delete}',
            ],
        ],
    ]
); ?>
