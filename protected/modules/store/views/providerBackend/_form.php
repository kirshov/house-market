<?php
/**
 * @var $this providerBackendController
 * @var $model Provider
 * @var $form \webforma\widgets\ActiveForm
 */
$form = $this->beginWidget(
    '\webforma\widgets\ActiveForm',
    [
        'id' => 'provider-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => 'vertical',
		'htmlOptions' => ['class' => 'well sticky'],
		'clientOptions' => [
			'validateOnSubmit' => true,
		],
    ]
);
?>

<div class="alert alert-info">
    <?= Yii::t('StoreModule.store', 'Fields with'); ?>
    <span class="required">*</span>
    <?= Yii::t('StoreModule.store', 'are required'); ?>
</div>

<?= $form->errorSummary($model); ?>

<div class="row">
	<div class="col-sm-6">
		<?= $form->textFieldGroup($model, 'name'); ?>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<?= $form->textFieldGroup($model, 'extra_charge'); ?>
	</div>
</div>

<div class='row'>
    <div class="col-sm-12 <?= $model->hasErrors('description') ? 'has-error' : ''; ?>">
        <?= $form->labelEx($model, 'description'); ?>
        <?php $this->widget($this->module->getVisualEditor(), [
			'model' => $model,
			'attribute' => 'description',
		]); ?>
        <p class="help-block"></p>
    </div>
</div>

<div class="buttons">
	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context' => 'primary',
		'label' => $model->getIsNewRecord() ?
			Yii::t('StoreModule.store', 'Add provider and continue') :
			Yii::t('StoreModule.store', 'Save provider and continue'),
	]); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label' => $model->getIsNewRecord() ?
			Yii::t('StoreModule.store', 'Add provider and close') :
			Yii::t('StoreModule.store', 'Save provider and close'),
	]); ?>
</div>
<?php $this->endWidget(); ?>
