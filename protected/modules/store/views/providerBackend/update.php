<?php
$this->breadcrumbs = [
    Yii::t('StoreModule.store', 'Providers') => ['/store/providerBackend/index'],
	$model->name . ' - ' . Yii::t('StoreModule.store', 'Edition'),
];

$this->pageTitle = Yii::t('StoreModule.store', 'Providers - edition');

$this->menu = [
    [
        'icon' => 'fa fa-fw fa-truck',
        'label' => Yii::t('StoreModule.store', 'Manage providers'),
        'url' => ['/store/providerBackend/index'],
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('StoreModule.store', 'Create provider'),
        'url' => ['/store/providerBackend/create'],
    ],
    ['label' => Yii::t('StoreModule.store', 'Provider') . ' «' . mb_substr($model->name, 0, 32) . '»'],
    [
        'icon' => 'fa fa-fw fa-pencil',
        'label' => Yii::t('StoreModule.store', 'Update provider'),
        'url' => [
            '/store/providerBackend/update',
            'id' => $model->id,
        ],
    ],
    [
        'icon' => 'fa fa-fw fa-trash-o',
        'label' => Yii::t('StoreModule.store', 'Delete provider'),
        'url' => '#',
        'linkOptions' => [
            'submit' => ['/store/providerBackend/delete', 'id' => $model->id],
            'confirm' => Yii::t('StoreModule.store', 'Do you really want to remove this provider?'),
            'params' => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
            'csrf' => true,
        ],
    ],
];
?>

<?= $this->renderPartial('_form', ['model' => $model]); ?>
