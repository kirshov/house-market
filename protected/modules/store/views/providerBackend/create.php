<?php
$this->breadcrumbs = [
    Yii::t('StoreModule.store', 'Providers') => ['/store/providerBackend/index'],
    Yii::t('StoreModule.store', 'Creating'),
];

$this->pageTitle = Yii::t('StoreModule.store', 'Providers - create');

$this->menu = [
    [
        'icon' => 'fa fa-fw fa-truck',
        'label' => Yii::t('StoreModule.store', 'Manage providers'),
        'url' => ['/store/providerBackend/index'],
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('StoreModule.store', 'Create provider'),
        'url' => ['/store/providerBackend/create'],
    ],
];
?>

<?= $this->renderPartial('_form', ['model' => $model]); ?>
