<?php
/**
 * @var $this CatalogBackendController
 * @var $model StoreCategory
 * @var $form \webforma\widgets\ActiveForm
 */
$form = $this->beginWidget(
    '\webforma\widgets\ActiveForm',
    [
        'id'                     => 'category-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'htmlOptions'            => ['class' => 'well', 'enctype' => 'multipart/form-data'],
    ]
); ?>

<?= $form->errorSummary($model); ?>

<div class='row'>
	<div class="col-sm-4">
		<?= $form->dropDownListGroup(
			$model,
			'category_id',
			[
				'widgetOptions' => [
					'data' => StoreCategoryHelper::formattedList(),
					'htmlOptions' => [
						'empty' => '---',
						'encode' => false,
					],
				],
			]
		); ?>
	</div>

    <div class="col-sm-3">
        <?= $form->dropDownListGroup(
            $model,
            'status',
            [
                'widgetOptions' => [
                    'data' => $model->getStatusList(),
                ],
            ]
        ); ?>
    </div>
</div>
<div class='row'>
    <div class="col-sm-7">
        <?= $form->textFieldGroup($model, 'name'); ?>
    </div>
</div>
<div class='row'>
    <div class="col-sm-7">
        <?if($model->isNewRecord){
	        echo $form->slugFieldGroup($model, 'slug', ['sourceAttribute' => 'name']);
		} else {
        	echo $form->textFieldGroup($model, 'slug');
        }?>
    </div>
</div>
<div class="row">
	<div class="col-sm-6">
		<?= $form->checkBoxGroup($model, 'show_after_filter'); ?>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="row" id="filters">
			<?=$filters?>
		</div>
	</div>
</div>

<div class='row'>
    <div class="col-sm-12 <?= $model->hasErrors('description') ? 'has-error' : ''; ?>">
        <?= $form->labelEx($model, 'description'); ?>
        <?php $this->widget(
            $this->module->getVisualEditor(),
            [
                'model'     => $model,
                'attribute' => 'description',
            ]
        ); ?>
		<p></p>
    </div>
</div>

<div class='row'>
    <div class="col-sm-12 <?= $model->hasErrors('description_bottom') ? 'has-error' : ''; ?>">
        <?= $form->labelEx($model, 'description_bottom'); ?>
        <?php $this->widget(
            $this->module->getVisualEditor(),
            [
                'model'     => $model,
                'attribute' => 'description_bottom',
            ]
        ); ?>
		<p></p>
    </div>
</div>

<?php $collapse = $this->beginWidget('bootstrap.widgets.TbCollapse'); ?>
<div class="panel-group" id="extended-options">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">
                <a data-toggle="collapse" data-parent="#extended-options" href="#collapseOne">
                    <?= Yii::t('StoreModule.store', 'Data for SEO'); ?>
                </a>
            </div>
        </div>
        <div id="collapseOne" class="panel-collapse collapse">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-7">
                        <?= $form->textFieldGroup($model, 'title'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-7">
                        <?= $form->textFieldGroup($model, 'meta_title'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-7">
                        <?= $form->textFieldGroup($model, 'meta_keywords'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-7">
                        <?= $form->textAreaGroup($model, 'meta_description'); ?>
                    </div>
                </div>
				<div class="row">
					<div class="col-sm-7">
						<?= $form->checkboxGroup($model, 'use_category_url'); ?>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>

<div class="buttons">
	<?php $this->widget(
		'bootstrap.widgets.TbButton',
		[
			'buttonType' => 'submit',
			'context'    => 'primary',
			'label'      => $model->isNewRecord
				? 'Создать группу и продолжить'
				: 'Сохранить группу и продолжить',
		]
	); ?>

	<?php $this->widget(
		'bootstrap.widgets.TbButton',
		[
			'buttonType'  => 'submit',
			'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
			'label'       => $model->isNewRecord
				? 'Создать группу и закрыть'
				: 'Сохранить группу и закрыть',
		]
	); ?>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	$('#ProductGroups_category_id').on('change', function () {
		var categoryId = $(this).val();
		if(!categoryId){
			categoryId = 0;
		}
		$('#filters').load('<?= Yii::app()->createUrl('/store/productGroupsBackend/getFilterCatId');?>' + categoryId + '/');
	});
</script>