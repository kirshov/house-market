<?php
$this->breadcrumbs = [
	'Группы товаров' => ['/store/productGroupsBackend/index'],
    'Редактирование',
];

$this->pageTitle = 'Группы товаров - редактирование';

$this->menu = [
	[
		'icon' => 'fa fa-fw fa-list-alt',
		'label' => Yii::t('StoreModule.store', 'Groups list'),
		'url' => ['/store/productGroupsBackend/index'],
	],
	[
		'icon' => 'fa fa-fw fa-plus-square',
		'label' => Yii::t('StoreModule.store', 'Create group'),
		'url' => ['/store/productGroupsBackend/create'],
	],
	[
		'icon' => 'fa fa-fw fa-external-link-square',
		'label' => 'Посмотреть на сайте',
		'url' => [
			'/store/productGroups/view',
			'slug' => $model->slug
		],
		'linkOptions' => [
			'target' => '_blank',
		]
	],
    [
        'icon' => 'fa fa-fw fa-pencil',
        'label' => Yii::t('StoreModule.store', 'Update group'),
        'url' => [
            '/store/productGroupsBackend/update',
            'id' => $model->id
        ]
    ],
    [
        'icon' => 'fa fa-fw fa-trash-o',
        'label' => Yii::t('StoreModule.store', 'Delete group'),
        'url' => '#',
        'linkOptions' => [
            'submit' => ['/store/productGroupsBackend/delete', 'id' => $model->id],
            'params' => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
            'confirm' => Yii::t('StoreModule.store', 'Do you really want to remove group?'),
            'csrf' => true,
        ]
    ],
];
?>

<?= $this->renderPartial('_form', [
	'model' => $model,
	'filters' => $filters,
]); ?>
