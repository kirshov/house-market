<?php
$this->breadcrumbs = [
    'Группы товаров' => ['/store/productGroupsBackend/index'],
    'Управление',
];

$this->pageTitle = 'Группы товаров';

$this->menu = [
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('StoreModule.store', 'Groups list'),
        'url' => ['/store/productGroupsBackend/index'],
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('StoreModule.store', 'Create group'),
        'url' => ['/store/productGroupsBackend/create'],
    ],
];
?>

<?php $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'category-grid',
        'sortableRows' => true,
        'sortableAjaxSave' => true,
        'sortableAttribute' => 'sort',
        'sortableAction' => '/store/productGroupsBackend/sortable',
        'type' => 'condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'actionsButtons' => [
            CHtml::link(
                Yii::t('WebformaModule.webforma', 'Add'),
                ['/store/productGroupsBackend/create'],
                ['class' => 'btn btn-success pull-right btn-sm']
            ),
        ],
        'columns' => [
            [
                'name' => 'name',
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::link($data->name, array("/store/productGroupsBackend/update", "id" => $data->id));
                },
            ],
            [
                'name' => 'slug',
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::link($data->slug, array("/store/productGroupsBackend/update", "id" => $data->id));
                },
            ],
	        [
		        'class'    => 'webforma\widgets\WEditableColumn',
		        'editable' => [
			        'url'    => $this->createUrl('/store/productGroupsBackend/inline'),
			        'mode'   => 'popup',
			        'type'   => 'select',
			        //'title'  => '',
			        'source' => $model->getYesNoList(),
			        'params' => [
				        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
			        ]
		        ],
		        'name'     => 'show_after_filter',
		        'type'     => 'raw',
		        'value'    => '$data->getIsShowAfterFilters()',
		        'filter'   => CHtml::activeDropDownList(
			        $model,
			        'show_after_filter',
			        $model->getYesNoList(),
			        ['class' => 'form-control', 'empty' => '']
		        ),
	        ],
            [
                'class' => 'webforma\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('/store/productGroupsBackend/inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    StoreCategory::STATUS_PUBLISHED => ['class' => 'label-success'],
                    StoreCategory::STATUS_DRAFT => ['class' => 'label-default'],
                ],
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
                'frontViewButtonUrl' => function($data){
                    return $data->getUrl();
                    //return Yii::app()->createUrl('/store/productGroups/view', ['slug' => $data->slug]);
                },
                'buttons' => [
                    'view' => [
                        'visible' => function ($row, $data) {
                            return false;
                        },
                    ],
                    'front_view' => [
                        'visible' => function ($row, $data) {
                            return $data->status == ProductGroups::STATUS_PUBLISHED;
                        },
                    ],
                ],
            ],
        ],
    ]
); ?>
