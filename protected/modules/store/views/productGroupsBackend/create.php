<?php
$this->breadcrumbs = [
	'Группы товаров' => ['/store/productGroupsBackend/index'],
    Yii::t('StoreModule.store', 'Creating'),
];

$this->pageTitle = Yii::t('StoreModule.store', 'Categories - create');

$this->menu = [
	[
		'icon' => 'fa fa-fw fa-list-alt',
		'label' => Yii::t('StoreModule.store', 'Groups list'),
		'url' => ['/store/productGroupsBackend/index'],
	],
	[
		'icon' => 'fa fa-fw fa-plus-square',
		'label' => Yii::t('StoreModule.store', 'Create group'),
		'url' => ['/store/productGroupsBackend/create'],
	],
];
?>

<?= $this->renderPartial('_form', [
	'model' => $model,
	'filters' => $filters,
]); ?>
