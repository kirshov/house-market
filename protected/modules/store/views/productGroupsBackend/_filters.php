<div>
	<div class="col-sm-4 backend-attr-filter-item">
		<label>Поиск по имени</label>
		<?=CHtml::textField(
			'name',
			Yii::app()->user->getState('pg_name', '')
		)?>
	</div>
<?
$this->widget('application.modules.store.widgets.filters.PriceFilterWidget', [
	'category' => $category,
	'view' => 'in-backend',
]);
?>
</div>
<br/>
<br/>
<div class="row">
	<div class="col-sm-12">
	<?
		$this->widget('application.modules.store.widgets.filters.ProducerFilterWidget', [
			//'category' => $category,
			'view' => 'in-backend',
		]);

		$this->widget('application.modules.store.widgets.filters.AttributesFilterWidget', [
			'category' => $category,
			'attributes' => '**',
			'dropDownView' => 'in-backend',
		]);
	?>
	</div>
</div>
<br/>
<div class="row">
	<div class="col-sm-12">
		<div class="col-sm-12 backend-attr-filter-item">
			<label>Список ID товаров (через запятую)</label>
			<textarea name="ids" style="width: 100%;height: 100px;"><?=Yii::app()->user->getState('pg_ids')?></textarea>
			<small>
				<strong>Внимание!</strong>
				Если вы укажите список ID товаров, то выборка будет состоять <b>только из указанных товаров</b>!<br/>
			</small>
		</div>
	</div>
</div>
<br/>