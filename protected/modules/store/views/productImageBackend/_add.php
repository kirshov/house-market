<?php
$webforma = Yii::app()->getModule('webforma');
$this->widget(
    'bootstrap.widgets.TbFileUpload',
    [
        'id' => 'fileUploader',
        'url' => $this->createUrl("/store/productImageBackend/addImages", ['id' => $product->id]),
        'model' => $model,
        'attribute' => 'name',
        'multiple' => true,
        'formView' => 'store.views.productImageBackend._tform',
        'uploadView' => 'store.views.productImageBackend._upload',
        'options' => [
            'maxFileSize' => $webforma->maxSize,
            'acceptFileTypes' => 'js:/(\.|\/)(' . implode('|', explode(',', $webforma->allowedExtensions)) . ')$/i',
        ]
    ]
);
