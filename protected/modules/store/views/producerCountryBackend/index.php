<?php
$this->breadcrumbs = [
    Yii::t('StoreModule.store', 'Producers') => ['/store/producerBackend/index'],
    Yii::t('StoreModule.store', 'Country list') => ['/store/producerCountryBackend/index'],
    Yii::t('StoreModule.store', 'Manage'),
];

$this->pageTitle = Yii::t('StoreModule.store', 'Country list - manage');

$this->menu = [
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('StoreModule.store', 'Manage producers'),
        'url' => ['/store/producerBackend/index'],
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('StoreModule.store', 'Create producer'),
        'url' => ['/store/producerBackend/create'],
    ],
	[
		'icon' => 'fa fa-fw fa-list-alt',
		'label' => Yii::t('StoreModule.store', 'Country list'),
		'url' => ['/store/producerCountryBackend/index'],
	],
	[
		'icon' => 'fa fa-fw fa-plus-square',
		'label' => Yii::t('StoreModule.store', 'Create country'),
		'url' => ['/store/producerCountryBackend/create'],
	],
	[
		'icon' => 'fa fa-fw fa-list-alt',
		'label' => Yii::t('StoreModule.store', 'Manage producers line'),
		'url' => ['/store/producerLineBackend/index'],
	],
	[
		'icon' => 'fa fa-fw fa-plus-square',
		'label' => Yii::t('StoreModule.store', 'Create producer line'),
		'url' => ['/store/producerLineBackend/create'],
	],
];
?>

<?php
$this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'producer-grid',
        'type' => 'condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => [
            [
                'name' => 'name',
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::link($data->name, ['/store/producerCountryBackend/update', 'id' => $data->id]);
                },
            ],
            [
                'class' => 'webforma\widgets\WEditableColumn',
                'name' => 'slug',
                'editable' => [
                    'url' => $this->createUrl('/store/producerCountryBackend/inline'),
                    'mode' => 'popup',
                    'params' => [
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->request->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($model, 'slug', ['class' => 'form-control']),
            ],
            [
                'class' => 'webforma\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('/store/producerCountryBackend/inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    ProducerCountry::STATUS_ACTIVE => ['class' => 'label-success'],
	                ProducerCountry::STATUS_NOT_ACTIVE => ['class' => 'label-info'],
	                ProducerCountry::STATUS_ZERO => ['class' => 'label-default'],
                ],
            ],
            [
                'header' => Yii::t('StoreModule.store', 'Producers'),
                'value' => function ($data) {
                    return CHtml::link(
                        $data->producerCount,
                        ['/store/producerBackend/index', 'Producer[country_id]' => $data->id],
                        ['class' => 'badge']
                    );
                },
                'type' => 'raw',
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
				'template' => '{front_view} {update} {delete}',
                'frontViewButtonUrl' => function ($data) {
                    return Yii::app()->createUrl('/store/producer/country', ['slug' => $data->slug]);
                },
                'buttons' => [
                    'front_view' => [
                        'visible' => function ($row, $data) {
                            return $data->status == ProducerCountry::STATUS_ACTIVE;
                        },
                    ],
                ],
            ],
        ],
    ]
); ?>
