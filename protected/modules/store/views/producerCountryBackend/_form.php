<?php
/**
 * @var $this ProducerBackendController
 * @var $model Producer
 * @var $form \webforma\widgets\ActiveForm
 */
$form = $this->beginWidget(
    '\webforma\widgets\ActiveForm',
    [
        'id' => 'producer-country-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => 'vertical',
		'htmlOptions' => ['class' => 'well sticky'],
		'clientOptions' => [
			'validateOnSubmit' => true,
		],
    ]
);
?>

<div class="alert alert-info">
    <?= Yii::t('StoreModule.store', 'Fields with'); ?>
    <span class="required">*</span>
    <?= Yii::t('StoreModule.store', 'are required'); ?>
</div>

<?= $form->errorSummary($model); ?>

<div class="row">
    <div class="col-sm-6">
        <?= $form->textFieldGroup($model, 'name'); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?if($model->isNewRecord){
	        echo $form->slugFieldGroup($model, 'slug', ['sourceAttribute' => 'name']);
		} else {
	        echo $form->textFieldGroup($model, 'slug');
        }?>
    </div>
</div>

<div class='row'>
    <div class="col-sm-12 <?= $model->hasErrors('description') ? 'has-error' : ''; ?>">
        <?= $form->labelEx($model, 'description'); ?>
        <?php $this->widget(
            $this->module->getVisualEditor(),
            [
                'model' => $model,
                'attribute' => 'description',
            ]
        ); ?>
        <p class="help-block"></p>
    </div>
</div>


<div class='row'>
	<div class="col-sm-12 <?= $model->hasErrors('description_bottom') ? 'has-error' : ''; ?>">
		<?= $form->labelEx($model, 'description_bottom'); ?>
		<?php $this->widget(
			$this->module->getVisualEditor(),
			[
				'model' => $model,
				'attribute' => 'description_bottom',
			]
		); ?>
		<p class="help-block"></p>
	</div>
</div>

<?php $collapse = $this->beginWidget('bootstrap.widgets.TbCollapse'); ?>
<div class="panel-group" id="extended-options">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">
                <a data-toggle="collapse" data-parent="#extended-options" href="#collapseOne">
                    <?= Yii::t('StoreModule.store', 'Data for SEO'); ?>
                </a>
            </div>
        </div>
        <div id="collapseOne" class="panel-collapse collapse">
            <div class="panel-body">
				<div class="row">
					<div class="col-sm-7">
			            <?= $form->textFieldGroup($model, 'h1'); ?>
					</div>
				</div>

                <div class="row">
                    <div class="col-sm-7">
                        <?= $form->textFieldGroup($model, 'meta_title'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-7">
                        <?= $form->textFieldGroup($model, 'meta_keywords'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-7">
                        <?= $form->textAreaGroup($model, 'meta_description'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>

<div class="buttons">
	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context' => 'primary',
		'label' => $model->getIsNewRecord()
			? Yii::t('StoreModule.store', 'Add country and continue')
			: Yii::t('StoreModule.store', 'Save country and continue'),
	]); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label' => $model->getIsNewRecord()
			? Yii::t('StoreModule.store', 'Add country and close')
			: Yii::t('StoreModule.store', 'Save country and close'),
	]); ?>
</div>

<?php $this->endWidget(); ?>
