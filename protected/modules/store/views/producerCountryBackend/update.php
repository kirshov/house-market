<?php
$this->breadcrumbs = [
    Yii::t('StoreModule.store', 'Producers') => ['/store/producerBackend/index'],
	Yii::t('StoreModule.store', 'Country list') => ['/store/producerCountryBackend/index'],
    $model->name . ' - ' . Yii::t('StoreModule.store', 'Edition'),
];

$this->pageTitle = Yii::t('StoreModule.store', 'Producer country - edition');
$this->menu = [
	[
		'icon' => 'fa fa-fw fa-list-alt',
		'label' => Yii::t('StoreModule.store', 'Country list'),
		'url' => ['/store/producerCountryBackend/index'],
	],
	[
		'icon' => 'fa fa-fw fa-plus-square',
		'label' => Yii::t('StoreModule.store', 'Create country'),
		'url' => ['/store/producerCountryBackend/create'],
	],
];
?>

<?= $this->renderPartial('_form', ['model' => $model]); ?>
