<?php
$this->breadcrumbs = [
    Yii::t('StoreModule.store', 'Producers') => ['/store/producerBackend/index'],
    Yii::t('StoreModule.store', 'Manage'),
];

$this->pageTitle = Yii::t('StoreModule.store', 'Producers - manage');

$isMidTariff = Helper::hasMidTariff();
$this->menu = [
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('StoreModule.store', 'Manage producers'),
        'url' => ['/store/producerBackend/index'],
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('StoreModule.store', 'Create producer'),
        'url' => ['/store/producerBackend/create'],
    ],
	[
		'icon' => 'fa fa-fw fa-list-alt',
		'label' => Yii::t('StoreModule.store', 'Manage producers line'),
		'url' => ['/store/producerLineBackend/index'],
		'visible' => $isMidTariff,
	],
	[
		'icon' => 'fa fa-fw fa-plus-square',
		'label' => Yii::t('StoreModule.store', 'Create producer line'),
		'url' => ['/store/producerLineBackend/create'],
		'visible' => $isMidTariff,
	],
	[
		'icon' => 'fa fa-fw fa-list-alt',
		'label' => Yii::t('StoreModule.store', 'Country list'),
		'url' => ['/store/producerCountryBackend/index'],
		'visible' => $isMidTariff,
	],
	[
		'icon' => 'fa fa-fw fa-plus-square',
		'label' => Yii::t('StoreModule.store', 'Create country'),
		'url' => ['/store/producerCountryBackend/create'],
		'visible' => $isMidTariff,
	],
];
?>

<?php
$this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'producer-grid',
        'sortableRows' => true,
        'sortableAjaxSave' => true,
        'sortableAttribute' => 'sort',
        'sortableAction' => '/store/producerBackend/sortable',
        'type' => 'condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => [
            [
                'name' => 'image',
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::image(
                        StoreImage::producer($data, 80, 80, false),
                        $data->name,
                        [
                            'width' => 80,
                            'height' => 80,
                            'class' => 'img-thumbnail',
                        ]
                    );
                },
                'filter' => false,
            ],
            [
                'name' => 'name',
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::link($data->name, ['/store/producerBackend/update', 'id' => $data->id]);
                },
            ],
            [
                'class' => 'webforma\widgets\WEditableColumn',
                'name' => 'slug',
                'editable' => [
                    'url' => $this->createUrl('/store/producerBackend/inline'),
                    'mode' => 'popup',
                    'params' => [
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->request->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($model, 'slug', ['class' => 'form-control']),
            ],
            [
                'class' => 'webforma\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('/store/producerBackend/inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    Producer::STATUS_ACTIVE => ['class' => 'label-success'],
                    Producer::STATUS_NOT_ACTIVE => ['class' => 'label-info'],
                    Producer::STATUS_ZERO => ['class' => 'label-default'],
                ],
            ],
            [
                'header' => Yii::t('StoreModule.store', 'Products'),
                'value' => function ($data) {
                    return CHtml::link(
                        $data->productCount,
                        ['/store/productBackend/index', 'Product[producer_id]' => $data->id],
                        ['class' => 'badge']
                    );
                },
                'type' => 'raw',
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
                'frontViewButtonUrl' => function ($data) {
                    return Yii::app()->createUrl('/store/producer/view', ['slug' => $data->slug]);
                },
                'buttons' => [
                    'front_view' => [
                        'visible' => function ($row, $data) {
                            return $data->status == Producer::STATUS_ACTIVE;
                        },
                    ],
                ],
            ],
        ],
    ]
); ?>
