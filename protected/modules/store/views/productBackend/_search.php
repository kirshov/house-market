<?php
/**
 * @var StoreModule $module
 */
$module = Yii::app()->getModule('store');
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'action'      => Yii::app()->createUrl($this->route),
        'method'      => 'get',
        'htmlOptions' => ['class' => 'well'],
    ]
); ?>
<fieldset class="inline">
    <div class="row">
        <div class="col-sm-3">
            <?= $form->textFieldGroup($model, 'name'); ?>
        </div>
        <div class="col-sm-3">
            <?= $form->textFieldGroup($model, 'slug'); ?>
        </div>
        <div class="col-sm-3">
            <?= $form->textFieldGroup($model, 'sku'); ?>
        </div>
		<?if($module->useOriginalName):?>
			<div class="col-sm-3">
				<?= $form->textFieldGroup($model, 'original_name'); ?>
			</div>
		<?endif;?>
    </div>

    <div class="row">
        <div class="col-sm-3">
			<div class="row">
				<div class="col-sm-12">
            		<?= $form->checkBoxGroup($model, 'is_new'); ?>
        		</div>
        	</div>
			<div class="row">
				<div class="col-sm-12">
					<?= $form->checkBoxGroup($model, 'is_special'); ?>
				</div>
			</div>
        </div>
		<div class="col-sm-3">
			<div class="row">
				<div class="col-sm-12">
					<?= $form->checkBoxGroup($model, 'is_discount'); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<?= $form->checkBoxGroup($model, 'null_price'); ?>
				</div>
			</div>
		</div>
        <div class="col-sm-3">
			<div class="row">
				<div class="col-sm-12">
			        <?= $form->checkBoxGroup($model, 'discontinued'); ?>
				</div>
			</div>
        </div>

		<?if($module->priceType == StoreModule::PRICE_TYPE_CHARGE):?>
			<div class="col-sm-2">
				<?= $form->checkBoxGroup($model, 'unique_extra_charge'); ?>
			</div>
		<?endif;?>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <?= $form->dropDownListGroup($model, 'category_id', [
                'widgetOptions' => [
                    'data' => StoreCategoryHelper::formattedList(),
                    'htmlOptions' => [
                        'empty' => Yii::t('StoreModule.store', '--choose--'),
                        'encode' => false,
                    ],
                ],
            ]) ?>
        </div>
		<div class="col-sm-3">
		    <?= $form->dropDownListGroup($model, 'provider_id', [
			    'widgetOptions' => [
				    'data' => Provider::model()->getFormattedList(),
				    'htmlOptions' => [
					    'empty' => Yii::t('StoreModule.store', '--choose--'),
					    'encode' => false,
				    ],
			    ],
		    ]) ?>
		</div>

        <div class="col-sm-3">
            <?= $form->dropDownListGroup($model, 'producer_id', [
                'widgetOptions' => [
                    'data' => Producer::model()->getFormattedList(),
                    'htmlOptions' => [
                        'empty' => Yii::t('StoreModule.store', '--choose--'),
                        'encode' => false,
                    ],
                ],
            ]) ?>
        </div>

        <div class="col-sm-3">
            <div class="pareData">
                <?= $form->dropDownListGroup($model, 'producer_line_id', [
                    'widgetOptions' => [
                        'data' => [],
                        'htmlOptions' => [
                            'empty' => Yii::t('StoreModule.store', '--choose--'),
                            'encode' => false,
                        ],
                    ],
                ]) ?>
            </div>
        </div>
    </div>

    
    <div class="row">
        <div class="col-sm-3">
            <?= $form->textFieldGroup($model, 'title'); ?>
        </div>
        <div class="col-sm-3">
            <?= $form->textFieldGroup($model, 'meta_title'); ?>
        </div>
        <div class="col-sm-3">
            <?= $form->textFieldGroup($model, 'meta_keywords'); ?>
        </div>
        <div class="col-sm-3">
            <?= $form->textFieldGroup($model, 'meta_description'); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <?= $form->textFieldGroup($model, 'description'); ?>
        </div>
        <div class="col-sm-3">
            <?= $form->dropDownListGroup(
                $model,
                'status',
                [
                    'widgetOptions' => [
                        'data'        => $model->statusList,
                        'htmlOptions' => [
                            'class' => 'popover-help',
                            'empty' => '-- не выбран --'
                        ],
                    ],
                ]
            ); ?>
        </div>
        <div class="col-sm-3">
            <?= $form->dropDownListGroup(
                $model,
                'in_stock',
                [
                    'widgetOptions' => [
                        'data'        => $model->getInStockList(),
                        'htmlOptions' => [
                            'class' => 'popover-help',
                            'empty' => '-- не выбран --'
                        ],
                    ],
                ]
            ); ?>
        </div>
		<div class="col-sm-3">
		    <?= $form->textFieldGroup($model, 'quantity'); ?>
		</div>
        <?/*<div class="col-sm-3">
            <?= $form->dropDownListGroup(
                $model,
                'category_id',
                [
                    'widgetOptions' => [
                        'data'        => Yii::app()->getComponent('categoriesRepository')->getFormattedList(
                                (int)Yii::app()->getModule('page')->mainCategory
                            ),
                        'htmlOptions' => [
                            'class' => 'popover-help',
                            'empty' => Yii::t('PageModule.page', '- not set -')
                        ],
                    ],
                ]
            ); ?>
        </div>
        <div class="col-sm-3">
            <?= $form->textFieldGroup($model, 'body'); ?>
        </div>
*/?>
    </div>

</fieldset>

<?php $this->widget(
    'bootstrap.widgets.TbButton',
    [
        'buttonType'  => 'submit',
        'context'     => 'primary',
        'encodeLabel' => false,
        'label'       => '<i class="fa fa-search">&nbsp;</i> Найти товары',
    ]
); ?>

<?php $this->widget('bootstrap.widgets.TbButton', [
	'buttonType' => 'reset',
	'context'    => 'danger',
	'encodeLabel' => false,
	'label'       => '<i class="fa fa-times">&nbsp;</i> Очистить форму',
]); ?>
<br/>
<br/>
<?
/*
 * <div><a href="<?=Yii::app()->createUrl($this->route, ['Product[dont_match_cat]' => 1])?>">Показать все товары несоответсвтующие типу товара и категории</a></div>
 * <div><a href="<?=Yii::app()->createUrl('/store/productBackend/refreshPrice')?>">Пересчитать цены (может выполняться несколько минут)</a></div>
 * */
?>

<?php $this->endWidget(); ?>
<?
Yii::app()->getClientScript()->registerScript(
    __FILE__,
    <<<JS
    $(document).ready(function(){
    	$('#Product_producer_id').change(function () {
    		$('#Product_producer_line_id option').filter('[value!=""]').remove();
			var producerId = parseInt($(this).val());
			if (producerId) {
				$.post("$urlLine", {
					'$tokenName': "$token",
					'producerId': producerId
				}, function (response) {
					if (response.result) {
						$.each(response.data, function (index, element) {
							$('#Product_producer_line_id').append(new Option(element, index, false));
						});
						$('#Product_producer_line_id').removeAttr('disabled');
						$('#pareData').show();
					}
				});
			}
		});
    });
JS
); ?>






