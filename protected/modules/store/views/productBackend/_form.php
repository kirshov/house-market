<?php
/**
 * @var $this ProductBackendController
 * @var $model Product
 * @var $form \webforma\widgets\ActiveForm
 * @var ImageGroup $imageGroup
 */
?>
<?php
Yii::app()->getClientScript()->registerCssFile($this->getModule()->getAssetsUrl().'/css/store-backend.css');
//Yii::app()->getClientScript()->registerCoreScript('jquery.ui');
Yii::import('application.modules.dictionary.models.*');
?>

<ul class="nav nav-tabs horizontal-tabs">
    <li class="active"><a href="#common" data-toggle="tab"><?= Yii::t("StoreModule.store", "Common"); ?></a></li>
	<li><a href="#advanced" data-toggle="tab">Дополнительно</a></li>
	<li><a href="#attributes" data-toggle="tab"><?= Yii::t("StoreModule.store", "Attributes"); ?></a></li>
    <li><a href="#images" data-toggle="tab"><?= Yii::t("StoreModule.store", "Images"); ?></a></li>
	<?if(Helper::hasMidTariff()):?>
    	<li><a href="#variants" data-toggle="tab"><?= Yii::t("StoreModule.store", "Variants"); ?></a></li>
	<?endif;?>
    <li><a href="#seo" data-toggle="tab"><?= Yii::t("StoreModule.store", "SEO"); ?></a></li>
    <?/*<li><a href="#linked" data-toggle="tab"><?= Yii::t("StoreModule.store", "Linked products"); ?></a></li>*/?>
	<?if($this->getModule()->useProductTabs && $this->getModule()->productTabs):?>
		<li><a href="#adv-tabs" data-toggle="tab">Дополнительные вкладки</a></li>
	<?endif;?>
</ul>

<?php
$form = $this->beginWidget(
    '\webforma\widgets\ActiveForm',
    [
        'id' => 'product-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'type' => 'vertical',
        'htmlOptions' => ['enctype' => 'multipart/form-data', 'class' => 'well sticky'],
        'clientOptions' => [
            'validateOnSubmit' => true,
        ],
    ]
); ?>

<?= $form->errorSummary($model); ?>


<div class="tab-content">
    <div class="tab-pane active" id="common">
		<div class="row">
			<div class="col-sm-6">
				<div class="row">
					<div class="col-sm-12">
						<?= $form->dropDownListGroup($model, 'place_id', [
							'widgetOptions' => [
								'data' => DictionaryGroup::getHtmlList('poselki'),
								'htmlOptions' => [
									'empty' => '---',
								],
							],
						]); ?>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-12">
						<?= $form->textFieldGroup($model, 'name'); ?>
					</div>

					<div class="col-sm-12">
						<?= $form->textFieldGroup($model, 'search_name'); ?>
					</div>

					<?if($this->getModule()->useOriginalName):?>
						<div class="col-sm-12">
							<?= $form->textFieldGroup($model, 'original_name'); ?>
						</div>
					<?endif;?>
				</div>

				<div class="row">
					<div class="col-sm-12">
						<?if($model->isNewRecord){
							echo $form->slugFieldGroup($model, 'slug', ['sourceAttribute' => 'name']);
						} else {
							echo $form->textFieldGroup($model, 'slug');
						} ?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<?=$form->textFieldGroup($model, 'square');?>
					</div>
					<div class="col-sm-6">
						<?=$form->textFieldGroup($model, 'square_area');?>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-6">
						<?= $form->dropDownListGroup($model, 'producer_id', [
							'widgetOptions' => [
								'data' => Producer::model()->getFormattedList(),
								'htmlOptions' => [
									'empty' => '---',
								],
							],
						]); ?>
					</div>
					<div class="col-sm-6">
						<?= $form->dropDownListGroup($model, 'floor_id', [
							'widgetOptions' => [
								'data' => DictionaryGroup::getHtmlList('etazhnost'),
								'htmlOptions' => [
									'empty' => '---',
								],
							],
						]); ?>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-6">
						<?= $form->dropDownListGroup($model, 'done_id', [
							'widgetOptions' => [
								'data' => DictionaryGroup::getHtmlList('gotovnost'),
								'htmlOptions' => [
									'empty' => '---',
								],
							],
						]); ?>
					</div>
					<div class="col-sm-6">
						<?= $form->datePickerGroup($model, 'date', [
							'widgetOptions' => [
								'options' => [
									'format' => 'dd.mm.yyyy',
									'weekStart' => 1,
									'autoclose' => true,
								],
							],
							'prepend' => '<i class="fa fa-calendar"></i>',
						]);
						?>
					</div>
				</div>

				<?/*<div class="row">
					<div class="col-sm-12">
						<?=$form->textFieldGroup($model, 'address');?>
					</div>
				</div>*/?>

				<?/*
				<div class="row">
					<div class="col-sm-6">
						<?= $form->textFieldGroup($model, 'sku'); ?>
					</div>

					<div class="col-sm-6">
						<?= $form->dropDownListGroup($model, 'provider_id', [
							'widgetOptions' => [
								'data' => Provider::model()->getFormattedList(),
								'htmlOptions' => [
									'empty' => '---',
								],
							],
						]); ?>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-12">
						<?= $form->dropDownListGroup($model,'category_id', [
							'widgetOptions' => [
								'data' => StoreCategoryHelper::formattedList(),
								'htmlOptions' => [
									'empty' => '---',
									'encode' => false,
								],
							],
						]); ?>
					</div>
				</div>


					<div class="col-sm-6">
						<div id="pareData" style='display:none;'>
							<?= $form->dropDownListGroup($model, 'producer_line_id',[
								'widgetOptions' => [
									'data' => [],
									'htmlOptions' => [
										'empty' => '---',
									],
								],
							]); ?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-4 col-xs-4">
						<?= $form->checkBoxGroup($model, 'is_special'); ?>
					</div>
					<div class="col-sm-4 col-xs-4">
						<?= $form->checkBoxGroup($model, 'is_new'); ?>
					</div>
					<div class="col-sm-4 col-xs-4">
						<?= $form->checkBoxGroup($model, 'is_hit'); ?>
					</div>
				</div>

				<?if($this->getModule()->priceType == StoreModule::PRICE_TYPE_CHARGE):?>
					<div class="row">
						<div class="col-sm-6">
							<?= $form->textFieldGroup($model, 'purchase_price'); ?>
						</div>

						<div class="col-sm-6">
							<?= $form->textFieldGroup($model, 'extra_charge', [
								'widgetOptions' => [
									'htmlOptions' => [
										'placeholder' => $model->provider->extra_charge,
										'data-extra-charge' => $model->provider->extra_charge,
									],
								],
							]); ?>
						</div>
					</div>
				<?endif;?>
				*/?>
				<div class="row">
					<div class="col-sm-6">
						<?= $form->textFieldGroup($model, 'price'); ?>
					</div>
					<?/*
					<?if($this->getModule()->priceType == StoreModule::PRICE_TYPE_CHARGE):?>
						<div class="col-sm-6">
							<?= $form->textFieldGroup($model, 'discount'); ?>
						</div>
					<?else:?>
						<div class="col-sm-6">
							<?= $form->textFieldGroup($model, 'discount_price'); ?>
						</div>
					<?endif;?>
					*/?>
				</div>

				<?/*
				<div class='row'>
					<div class="col-sm-6">
						<?= $form->dropDownListGroup($model, 'in_stock', [
							'widgetOptions' => [
								'data' => $model->getInStockList(),
							],
						]); ?>
					</div>
					<div class="col-sm-6">
						<?= $form->numberFieldGroup($model, 'quantity'); ?>
					</div>
				</div>


				<?if($this->getModule()->useProductUnit):?>
					<div class="row">
						<div class="col-sm-6">
							<?= $form->textFieldGroup($model, 'unit'); ?>
						</div>
					</div>
				<?endif;?>
				*/?>
			</div>

			<div class="col-sm-6">
				<div class='row'>
					<div class="col-sx-8 col-sm-8 col-sm-offset-2">
						<?php $this->widget('webforma\widgets\WInputFile', [
							'model' => $model,
							'attribute' => 'image',
						]); ?>
					</div>
				</div>
			</div>
		</div>

		<div class='row'>
			<div class="col-sm-12 <?= $model->hasErrors('description') ? 'has-error' : ''; ?>">
			    <?= $form->labelEx($model, 'description'); ?>
			    <?php $this->widget($this->module->getVisualEditor(), [
				    'model' => $model,
				    'attribute' => 'description',
			    ]); ?>
				<p class="help-block"></p>
			</div>
		</div>

		<?/*
		<div class='row'>
			<div class="col-sm-12 <?= $model->hasErrors('short_description') ? 'has-error' : ''; ?>">
			    <?= $form->labelEx($model, 'short_description'); ?>
			    <?php $this->widget($this->module->getVisualEditor(), [
				    'model' => $model,
				    'attribute' => 'short_description',
			    ]); ?>
				<p class="help-block"></p>
			</div>
		</div>
		*/?>

		<div class="row">
			<div class="col-sm-3">
				<?= $form->dropDownListGroup($model, 'status', [
					'widgetOptions' => [
						'data' => $model->getStatusList(),
					],
				]); ?>
			</div>
		</div>
    </div>

	<div class="tab-pane" id="advanced">
		<div class="row">
			<div class="col-sm-6">
				<?= $form->textFieldGroup($model, 'count_view'); ?>
			</div>
		</div>

		<?/*
		<div class="row">
			<div class="col-sm-6">
				<?= $form->checkBoxGroup($model, 'discontinued', [
					'hint' => 'Выводит страницу товара без цены и возможности заказа',
				]); ?>
			</div>
		</div>
		*/?>

		<div class="row">
			<div class="col-sm-6">
				<?= $form->checkBoxGroup($model, 'show_at_end'); ?>
			</div>
		</div>
	</div>

    <div class="tab-pane" id="images">
        <?php if ($model->getIsNewRecord()): ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-warning">
                        <?= Yii::t("StoreModule.store", "Mass upload alert"); ?>
                    </div>
                </div>
            </div>
	    <?php else: ?>
			<div class="row">
				<div class="col-xs-12">
					<button id="button-add-image" type="button" class="btn btn-success"><i class="fa fa-fw fa-plus"></i> Добавить изображение</button>
					<a href="<?=Yii::app()->createUrl('/store/productImageBackend/index', ['id' => $model->id])?>" class="btn btn-success"><i class="fa fa-fw fa-image"></i> Массовая загрузка</a>
				</div>
			</div>
			<p></p>
        	<div class="row">
				<div class="col-xs-12">
					<?php $imageModel = new ProductImage(); ?>
					<div id="product-images">
						<div class="image-template hidden form-group">
							<div class="row">
								<div class="col-xs-6 col-sm-4">
									<label for=""><?= Yii::t("StoreModule.store", "File"); ?></label>
									<input type="file" class="image-file"/>
								</div>
								<div class="col-xs-5 col-sm-3">
									<label for=""><?= Yii::t("StoreModule.store", "Image title"); ?></label>
									<input type="text" class="image-title form-control"/>
								</div>
								<div class="col-xs-5 col-sm-3">
									<label for=""><?= Yii::t("StoreModule.store", "Image alt"); ?></label>
									<input type="text" class="image-alt form-control"/>
								</div>
								<div class="col-xs-2 col-sm-1" style="padding-top: 24px">
									<button class="button-delete-image btn btn-default" type="button"><i
											class="fa fa-fw fa-trash-o"></i></button>
								</div>
							</div>
						</div>
					</div>

					<?php if ($model->images): ?>
						<table class="table table-hover">
							<thead>
							<tr>
								<th></th>
								<th><?= Yii::t("StoreModule.store", "Image title"); ?></th>
								<th><?= Yii::t("StoreModule.store", "Image alt"); ?></th>
								<?/*<th><?= Yii::t("StoreModule.store", "Group"); ?></th>*/?>
								<th></th>
							</tr>
							</thead>
							<tbody id="product-images-list">
							<?php foreach ($model->images as $key => $image): ?>
								<tr>
									<td>
										<img src="<?= $image->getImageUrl(100, 100, false); ?>" alt="" class="img-responsive"/>
									</td>
									<td>
										<?= CHtml::textField('ProductImage['.$image->id.'][title]', $image->title,
											['class' => 'form-control']) ?>
									</td>
									<td>
										<?= CHtml::textField('ProductImage['.$image->id.'][alt]', $image->alt,
											['class' => 'form-control']) ?>
									</td>
									<?/*<td>
										<?= CHtml::dropDownList(
											'ProductImage['.$image->id.'][group_id]',
											$image->group_id,
											ImageGroupHelper::all(),
											[
												'empty' => Yii::t('StoreModule.store', '--choose--'),
												'class' => 'form-control image-group-dropdown',
											]
										) ?>
									</td>*/?>
									<td class="text-center">
										<a data-id="<?= $image->id; ?>" href="<?= Yii::app()->createUrl(
											'/store/productBackend/deleteImage',
											['id' => $image->id]
										); ?>" class="btn btn-default product-delete-image"><i
												class="fa fa-fw fa-trash-o"></i></a>

										<?= CHtml::hiddenField('ProductImage['.$image->id.'][position]', $key, ['class' => 'image_order']) ?>
									</td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
					<?php endif; ?>
				</div>
			</div>
	    <?php endif; ?>
    </div>

    <div class="tab-pane" id="attributes">
        <div class="row">
            <div class="col-sm-6">
                <?= $form->dropDownListGroup($model, 'type_id', [
					'widgetOptions' => [
						'data' => CHtml::listData(Type::model()->findAll(), 'id', 'name'),
						'htmlOptions' => [
							'empty' => '---',
							'encode' => false,
							'id' => 'product-type',
							'style' => 'display:inline-block; width: 260px; margin-left: 5px',
						],
					],
				]); ?>
            </div>
        </div>
        <div id="attributes-panel">
            <?php $this->renderPartial('_attribute_form', ['groups' => $model->getAttributeGroups(), 'model' => $model]); ?>
        </div>
    </div>

    <div class="tab-pane" id="seo">
        <div class="row">
            <div class="col-sm-6">
                <?= $form->textFieldGroup($model, 'title'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->textFieldGroup($model, 'meta_title'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->textFieldGroup($model, 'meta_keywords'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->textAreaGroup($model, 'meta_description'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->textFieldGroup($model, 'meta_canonical'); ?>
            </div>
        </div>

		<div class="row">
			<div class="col-sm-6">
				<?= $form->checkBoxGroup($model, 'force_index'); ?>
				<div class="help-block">При включенной опции и настройки магазина "Не индексировать описание позиции" - к описанию товара не будет добавлен "noindex"</div>
			</div>
		</div>
    </div>
	<?if(Helper::hasMidTariff()):?>
    	<div class="tab-pane" id="variants">
			<div class="row">
				<div class="col-sm-12 form-group">
					<div class="form-inline">
						<div class="form-group">
							<select id="variants-type-attributes" class="form-control" style="width: 200px; height: 34px;"></select>
							<a href="#" class="btn btn-success" id="add-product-variant">Добавить вариант с атрибутом</a>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="variant-template variant">
							<table class="table table-condensed">
								<thead>
								<tr>
									<th width="80px"><?= Yii::t("StoreModule.store", "Attribute"); ?></th>
									<th width="130px"><?= Yii::t("StoreModule.store", "Value"); ?></th>
									<th width="150px"><?= Yii::t("StoreModule.store", "Price type"); ?></th>
									<th width="90px"><?= Yii::t("StoreModule.store", "SKU"); ?></th>
									<?if($this->getModule()->priceType == StoreModule::PRICE_TYPE_CHARGE):?>
										<th width="120px"><?= Yii::t("StoreModule.store", "Purchase price"); ?></th>
									<?endif;?>
									<th width="90px"><?= Yii::t("StoreModule.store", "Price"); ?></th>
									<?if($this->getModule()->priceType == StoreModule::PRICE_TYPE_CHARGE):?>
										<th width="100px"><?= Yii::t("StoreModule.store", "Discount"); ?>, %</th>
										<th width="100px"><?= Yii::t("StoreModule.store", "Extra Charge"); ?></th>
									<?else:?>
										<th width="150px"><?= Yii::t("StoreModule.store", "Discount price"); ?></th>
									<?endif;?>
									<th width="100px"><?= Yii::t("StoreModule.store", "Stock status"); ?></th>
									<th width="100px"><?= Yii::t("StoreModule.store", "Quantity"); ?></th>
									<th width="80px">Изображение</th>
									<th></th>
								</tr>
								</thead>
								<tbody id="product-variants">
								<?php foreach ((array)$model->getVariants(true) as $variant): ?>
									<?php $this->renderPartial('_variant_row', ['variant' => $variant, 'model' => $model]); ?>
								<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?endif;?>

    <div class="tab-pane" id="linked">
        <?php if ($model->getIsNewRecord()): ?>
			<div class="row">
				<div class="col-lg-12">
					<div class="alert alert-warning">
						<?= Yii::t("StoreModule.store", "First you need to save the product."); ?>
					</div>
				</div>
			</div>
        <?php else: ?>
            <?= $this->renderPartial('_link_form', ['product' => $model, 'searchModel' => $searchModel]); ?>
        <?php endif; ?>
    </div>

	<?if($this->getModule()->useProductTabs && $this->getModule()->productTabs):?>
		<div class="tab-pane" id="adv-tabs">
			<?$currentTabs = $model->getTabsFromKey();?>
			<?$countTabs = 0;?>
			<?foreach ($this->getModule()->productTabs as $_key => $_tab):?>
				<div class='row'>
					<div class="col-sm-12 <?= $model->hasErrors('description') ? 'has-error' : ''; ?>">
						<?if(!$_tab['presets']):?>
							<?= CHtml::label($_tab['name'], ''); ?>
							<?php $this->widget($this->module->getVisualEditor(), [
								'name' => 'ProductTabs['.$_key.']',
								'value' => isset($currentTabs[$_key]->content) ? $currentTabs[$_key]->content : '',
							]); ?>
							<p class="help-block"></p>
							<?$countTabs++;?>
						<?elseif ($_tab['presets'] == 'text'):?>
							<?= CHtml::label($_tab['name'], ''); ?>
							<div class="panel simple">
								<div class="panel-body">
									<?=$_tab['presets-text'];?>
								</div>
							</div>
							<?$countTabs++;?>
						<?endif;?>
					</div>
				</div>
			<?endforeach;?>
			
			<?if(!$countTabs):?>
				Нет вкладок для произвольного наполнения. Настройки вкладок находятся в настройках магазина.
			<?endif;?>
		</div>
	<?endif;?>
</div>

<div class="buttons">
	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context' => 'primary',
		'label' => $model->getIsNewRecord()
			? 'Добавить и создать новый'
			: 'Сохранить и продолжить редактирование',
	]); ?>

	<?php
	if($model->getIsNewRecord()){
		$this->widget('bootstrap.widgets.TbButton', [
			'buttonType' => 'submit',
			'context' => 'success',
			'htmlOptions' => ['name' => 'submit-type', 'value' => 'update'],
			'label' => 'Добавить и продолжить редактирование',
		]);
	}?>

	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label' => $model->getIsNewRecord()
			? 'Добавить и закрыть'
			: 'Сохранить и закрыть',
	]); ?>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(function () {

		$('#product-form').submit(function () {
			var productForm = $(this);
			$('#category-tree a.jstree-clicked').each(function (index, element) {
				productForm.append('<input type="hidden" name="categories[]" value="' + $(element).data('category-id') + '" />');
			});
		});

		var typeAttributes = {};

		function updateVariantTypeAttributes(onChange) {
			onChange = onChange || false;
			var typeId = $('#product-type').val();
			if (typeId) {
				$.getJSON('<?= Yii::app()->createUrl('/store/productBackend/typeAttributes');?>' + typeId + '/', function (data) {
					typeAttributes = data;
					var select = $('#variants-type-attributes');
					select.html("");
					$.each(data, function (key, value) {
						select.append($("<option></option>")
							.attr("value", value.id)
							.text(value.title));
					});

					if(onChange){
						if(Object.keys(attributesData).length){
							var elem;
							$.each(attributesData, function(index, value){
								elem = $('.attributes-items [value="' + value + '"]');
								if(elem.length){
									if(elem.get(0).tagName == 'OPTION'){
										elem.attr('selected', true);
										elem.closest('select').trigger('change');
									} else {
										elem.attr('checked', true).trigger('change');
									}
								}
							});
						}
					}
				});
			}
		}

		saveAttributes();

		updateVariantTypeAttributes();

		$("#add-product-variant").click(function (e) {
			e.preventDefault();
			var attributeId = $('#variants-type-attributes').val();
			if(!attributeId){
				return false;
			}
			var variantAttribute = typeAttributes.filter(function (el) {
				return el.id == attributeId;
			}).pop();
			var tbody = $('#product-variants');
			var modelId = '';
			<?if(!$model->getIsNewRecord()):?>
			modelId = '?modelId=' + <?=$model->id?>;
			<?endif;?>
			$.get('<?= Yii::app()->createUrl('/store/productBackend/variantRow');?>' + attributeId + '/' + modelId, function (data) {
				tbody.append(data);

				sortableVariants();
			});
		});

		$('#product-variants').on('click', '.remove-variant', function (e) {
			e.preventDefault();
			$(this).closest('tr').remove();
		});

		$('#product-type').on('change', function () {
			var typeId = $(this).val();
			if (typeId) {
				$('#attributes-panel').load('<?= Yii::app()->createUrl('/store/productBackend/typeAttributesForm');?>' + typeId + '/');
				updateVariantTypeAttributes(true);
			}
			else {
				$('#attributes-panel').html('');
				$('#variants-type-attributes').html('');
			}
		});

		$('#product-form').on('click', '.how_last_product', function(event){
			event.preventDefault();
			var currentType = $('#product-type').val();

			$.ajax({
				url: '<?= Yii::app()->createUrl('/store/productBackend/getAttrLastProduct');?>',
				data: {id: <?=$model->id ?: 0?>},
				dataType: 'json',
				type: 'get',
				success: function(response){
					if(currentType = response.type_id){
						$('#product-type').val(response.type_id);
						$('#attributes-panel').load('<?= Yii::app()->createUrl('/store/productBackend/typeAttributesForm');?>' + response.type_id + '/', function(){
							setAttributes(response['attributes']);
							saveAttributes();
							//$('#product-type').trigger('change');
						});
					} else {
						setAttributes(response['attributes']);
					}
				}
			});
		});

		function setAttributes(attributes){
			if(typeof attributes == 'undefined'){
				return false;
			}
			$.each(attributes, function(index, value){
				$.each(value, function(i, param){
					var elem = $('[id^=Attribute_'+index+']');
					if(elem.find('[value='+param+']').filter(':checkbox, :radio').length){
						elem.attr('checked', true).trigger('change');
					} else {
						if(elem.is('select')){
							elem.val(param).trigger('change');
						}
					}
				});
			});
		}

		$('#button-add-image').on('click', function () {
			var newImage = $("#product-images .image-template").clone().removeClass('image-template').removeClass('hidden');
			var key = $.now();

			newImage.appendTo("#product-images");
			newImage.find(".image-file").attr('name', 'ProductImage[new_' + key + '][name]');
			newImage.find(".image-title").attr('name', 'ProductImage[new_' + key + '][title]');
			newImage.find(".image-alt").attr('name', 'ProductImage[new_' + key + '][alt]');
			newImage.find(".image-group").attr('name', 'ProductImage[new_' + key + '][group_id]');

			return false;
		});

		$(this).closest('.product-image').remove();

		$('#product-images').on('click', '.button-delete-image', function () {
			$(this).closest('.row').remove();
		});

		$('.product-delete-image').on('click', function (event) {
			event.preventDefault();
			var blockForDelete = $(this).closest('tr');
			$.ajax({
				type: "POST",
				data: {
					'id': $(this).data('id'),
					'<?= Yii::app()->getRequest()->csrfTokenName;?>': '<?= Yii::app()->getRequest()->csrfToken;?>'
				},
				url: '<?= Yii::app()->createUrl('/store/productBackend/deleteImage');?>',
				success: function () {
					blockForDelete.remove();
				}
			});
		});

		$('#product-images-list').sortable({
			update: function(){
				sortableImages();
			}
		});

		$('#product-variants').sortable({
			update: function(){
				sortableVariants();
			}
		});

		$('#Product_producer_id').change(function () {
			$('#Product_producer_line_id option').filter('[value!=""]').remove();
			var producerId = parseInt($(this).val());
			if (producerId) {
				$.post('<?= Yii::app()->createUrl('/store/producerLineBackend/getjsonitems/') ?>', {
					'<?= Yii::app()->getRequest()->csrfTokenName;?>': '<?= Yii::app()->getRequest()->csrfToken;?>',
					'producerId': producerId
				}, function (response) {
					if (response.result && Object.keys(response.data).length) {
						var option = false;
						var current = <?= (int) $model->producer_line_id; ?>;
						$.each(response.data, function (index, element) {
							if (index == current) {
								option = true;
							} else {
								option = false;
							}
							$('#Product_producer_line_id').append(new Option(element, index, option));
						})
						if (current) {
							$('#Product_producer_line_id').val(current);
						}
						$('#Product_producer_line_id').removeAttr('disabled');
						$('#pareData').show();
					} else {
						$('#pareData').hide();
					}
				});
			}
		});

		if ($('#Product_producer_id').val() > 0) {
			$('#Product_producer_id').trigger('change');
		}
	});

	var attributesData = {};
	function saveAttributes(){
		$.each($('.attribute-item input:checked'), function(){
			attributesData[$(this).attr('id')] = $(this).val();
		});

		$.each($('.attributes-items select'), function(){
			attributesData[$(this).attr('id')] = $(this).val();
		});
	}

	function sortableVariants(){
		var i = 1;
		$('#product-variants tr .variant_order').each(function(){
			$(this).val(i);
			i++;
		})
	}

	function sortableImages(){
		var i = 1;
		$('#product-images-list tr .image_order').each(function(){
			$(this).val(i);
			i++;
		})
	}

	function activateFirstTabWithErrors() {
		var tab = $('.has-error').parents('.tab-pane').first();
		if (tab.length) {
			var id = tab.attr('id');
			$('a[href="#' + id + '"]').tab('show');
		} else {
			setCurrentTabActive();
		}
	}

	function calculatePricePlaceHolder(resultElem, purchasePriceElem, extraChargeElem){
		var extraCharge = parseInt(extraChargeElem.val());
		if(!extraCharge){
			var extraCharge = parseInt(extraChargeElem.data('extra-charge'));
		}
		if(isNaN(extraCharge)){
			extraCharge = 0;
		}
		var price = parseInt(purchasePriceElem.val()) + parseFloat(purchasePriceElem.val() * extraCharge / 100);
		if($('#Product_discount').val() > 0){
			price = price - (price * $('#Product_discount').val() / 100);
		}
		if(isNaN(price)){
			price = 0;
		}
		price = Math.round(price, 0);
		resultElem.attr('placeholder', price);
	}

	function calculateAllPricePlaceHolder(){
		calculatePricePlaceHolder($('#Product_price'), $('#Product_purchase_price'), $('#Product_extra_charge'));
		$('#product-variants tr').each(function(){
			var tr = $(this);
			calculatePricePlaceHolder(tr.find('input.amount'), tr.find('input.purchase_price'), tr.find('input.extra_charge'));
		})
	}

	$('#Product_purchase_price, #Product_extra_charge').on('keyup', function(){
		calculatePricePlaceHolder($('#Product_price'), $('#Product_purchase_price'), $('#Product_extra_charge'));
	});

	$('#product-variants').on('keyup', 'input.purchase_price, input.purchase_price, input.extra_charge', function(){
		var tr = $(this).closest('tr');
		calculatePricePlaceHolder(tr.find('input.amount'), tr.find('input.purchase_price'), tr.find('input.extra_charge'));
	});

	$('#Product_discount').on('keyup', function(){
		calculateAllPricePlaceHolder();
	});

	calculateAllPricePlaceHolder();

	activateFirstTabWithErrors();

</script>
