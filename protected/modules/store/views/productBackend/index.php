<?php
/**
 * @var ProductBackendController $this
 * @var Product $model
 * @var Product $data
 * @var ActiveForm $form
 * @var ProductBatchForm $batchModel
 */

use webforma\widgets\ActiveForm;

$this->hideSidebar = true;
$this->breadcrumbs = [
    Yii::t('StoreModule.store', 'Products') => ['/store/productBackend/index'],
    Yii::t('StoreModule.store', 'Manage'),
];

$this->pageTitle = Yii::t('StoreModule.store', 'Products - manage');

$this->title = Yii::t('StoreModule.store', 'Products');
$this->subTitle = Yii::t('StoreModule.store', 'administration');
?>

<?/*
<p class="search-wrap">
	<a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
		<i class="fa fa-search">&nbsp;</i>
		Поиск товаров
		<span class="caret">&nbsp;</span>
	</a>
</p>

<div id="search-toggle" class="collapse out search-form">
	<?php
	Yii::app()->clientScript->registerScript('search', "$('.search-form form').submit(function () { $.fn.yiiGridView.update('product-grid', {data: $(this).serialize()});return false;});");
	$this->renderPartial('_search', ['model' => $model]);
	?>
</div>
*/?>

<?php
$actionsButtons = [
	'add' => CHtml::link(Yii::t('StoreModule.store', 'Add'), ['/store/productBackend/create'],['class' => 'btn btn-sm btn-success pull-right']),
	'copy' => CHtml::link(Yii::t('StoreModule.store', 'Copy'), '#', ['id' => 'copy-products', 'class' => 'btn btn-sm btn-default pull-right', 'style' => 'margin-right: 4px;']),
	'batch' => CHtml::button(Yii::t('StoreModule.store', 'Batch actions'), ['class' => 'btn btn-sm btn-primary pull-right', 'style' => 'margin-right:7px','data-toggle' => 'modal','data-target' => '#batch-actions',]),
];
$typePriceCharge = false;
if(Yii::app()->getModule('store')->priceType == StoreModule::PRICE_TYPE_CHARGE){
	$actionsButtons['refreshPrice'] = CHtml::link('Пересчитать все цены', ['/store/productBackend/refreshPrice'],['class' => 'btn btn-sm btn-warning pull-right', 'style' => 'margin-right:7px', 'title' => 'Может выполняться несколько минут']);
	$typePriceCharge = true;
}
$this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'product-grid',
        'sortableRows'      => true,
        'sortableAjaxSave'  => true,
        'sortableAttribute' => 'position',
        'sortableAction'    => '/store/productBackend/sortable',
        'type' => 'condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'actionsButtons' => $actionsButtons,
        'columns' => [
            [
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::image(StoreImage::product($data, 100, 100), $data->name, ["width" => 100, "height" => 100, "class" => "img-thumbnail"]);
                },
				'htmlOptions' => [
					'style' => 'width: 80px;',
				],
				'filter' => CHtml::activeTextField($model, 'id', ['class' => 'form-control']),
            ],
            [
                'name' => 'name',
                'type' => 'raw',
                'value' => function ($data) use ($typePriceCharge){
					$name = CHtml::link('[<small>id:'.$data->id.'</small>] '.\webforma\helpers\WText::wordLimiter($data->name, 5), ["/store/productBackend/update", "id" => $data->id], ['class' => 'name', 'title' => CHtml::encode($data->name)]);
					if($data->original_name){
						$name .= '<div class="original_name">'.$data->original_name.'</div>';
					}
					if($data->producerLine){
						$lineNameOriginal = $lineName = $data->producerLine->name;
						if(mb_strlen($lineName) > 43){
							$lineName = mb_substr($lineName, 0, 20).'...'.mb_substr($lineName, mb_strlen($lineName) - 20);
						}
						$name .= '<div class="producer-line label label-success" title="'.$lineNameOriginal.'">'.$lineName.'</div>';
					}

	                $variants = '';
	                if($data->variants){
		                $variants = '<hr style="margin-top: 5px;margin-bottom: 3px;">';
		                $variants .= '<div class="variants">';
		                $currency = Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency);
		                foreach ($data->variants as $variantItem){
		                	$purchasePrice = ($typePriceCharge) ? '<span title="Цена закупки">'.($variantItem->purchase_price ? $variantItem->purchase_price . ' ' . $currency : ' - ').'</span>' : '';
			                $temp = [
				                '<span title="Характеристика">'.$variantItem->attribute->title.': '.$variantItem->attributeOption->value.'</span>',
				                '<span title="Артикул">'.($variantItem->sku ?: ' - ').'</span>',
			                ];

			                if($purchasePrice){
				                $temp[] = $purchasePrice;
							}
							$temp[] = '<span title="Цена">'.($variantItem->amount ? $variantItem->amount . ' ' . $currency : ' - ').'</span>';
			                if($variantItem->extra_charge){
				                $temp[] = '<span title="Наценка">'.($variantItem->extra_charge ? $variantItem->extra_charge . '%' : ' - ').'</span>';
			                } elseif($variantItem->discount_price) {
				                $temp[] = '<span title="Цена со скидкой">'.($variantItem->discount_price).'</span>';
							}
			                $variants .= '<div class="label label-success">['.$variantItem->id.'] '.implode(' / ', $temp).'</div> ';
		                }
		                $variants .= '</div>';
	                }

					return $name.$variants;
				},
            ],
			[
				'class' => 'webforma\widgets\EditableStatusColumn',
				'name' => 'producer_id',
				'url' => $this->createUrl('/store/productBackend/inline'),
				'source' => CMap::mergeArray(
					['' => '---'],
					Producer::model()->getFormattedList()
				),
				'editable' => [
					'mode' => 'popup',
					'emptytext' => '---',
				],
			],
           /* [
                'class' => 'webforma\widgets\WEditableColumn',
                'name' => 'sku',
                'editable' => [
                    'emptytext' => '---',
                    'url' => $this->createUrl('/store/productBackend/inline'),
                    'mode' => 'popup',
                    'params' => [
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
                    ]
                ],
                'filter' => CHtml::activeTextField($model, 'sku', ['class' => 'form-control']),
            ],

            [
                'name'  => 'category_id',
                'value' => function($data){
                    $categoryList = '<span class="label label-primary">'. (isset($data->category) ? $data->category->name : '---') . '</span>';

                    foreach ($data->categories as $category) {
                        $categoryList .= '</span> <span class="label label-default">' . $category->name .'</span>';
                    }

                    return $categoryList;
                },
                'type' => 'raw',
                'filter' => CHtml::activeDropDownList($model, 'category_id', StoreCategoryHelper::formattedList(), ['encode' => false, 'empty' => '', 'class' => 'form-control']),
                'htmlOptions' => ['width' => '220px'],
            ],*/
            [
                'name' => 'price',
                'value' => function (Product $data) {
                    return '<span title="Закупочная">'.round($data->purchase_price, 0).' <small>(закуп)</small></span>'.
						'<br/><span title="Продажа">'.round($data->getBasePrice(), 0).' <small>(продажа)</small></span>';
                },
				'type' => 'raw',
                'filter' => CHtml::activeTextField($model, 'price', ['class' => 'form-control']),
				'visible' => $typePriceCharge,
            ],
			[
				'class'  => 'webforma\widgets\WEditableColumn',
				'name' => 'price',
				//'type' => 'raw',
				'editable' => [
					'emptytext' => '---',
					'url' => $this->createUrl('/store/productBackend/inline'),
					'mode' => 'popup',
					'params' => [
						Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
					]
				],
				'filter' => CHtml::activeTextField($model, 'price', ['class' => 'form-control']),
				'visible' => !$typePriceCharge,
			],
            /*[
                'class'  => 'webforma\widgets\WEditableColumn',
                'name'   => 'discount_price',
                //'header' => Yii::t('StoreModule.store', 'New price'),
                'editable' => [
                    'emptytext' => '---',
                    'url' => $this->createUrl('/store/productBackend/inline'),
                    'mode' => 'popup',
                    'params' => [
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
                    ]
                ],
                'filter' => CHtml::activeTextField($model, 'discount_price', ['class' => 'form-control']),
				'visible' => !$typePriceCharge,
            ],*/
            /*[
                'class' => 'webforma\widgets\EditableStatusColumn',
                'name' => 'in_stock',
                'header' => Yii::t('StoreModule.store', 'Availability'),
                'url' => $this->createUrl('/store/productBackend/inline'),
                'source' => $model->getInStockList(),
                'options' => [
                    Product::STATUS_IN_STOCK => ['class' => 'label-success'],
                    Product::STATUS_NOT_IN_STOCK => ['class' => 'label-danger']
                ],
            ],*/
            /*[
                'class' => 'webforma\widgets\WEditableColumn',
                'name' => 'quantity',
                'value' => function (Product $data) {
                    return $data->quantity;
                },
                'header' => Yii::t('StoreModule.store', 'Rest'),
                'editable' => [
                    'url' => $this->createUrl('/store/productBackend/inline'),
                    'mode' => 'inline',
                    'params' => [
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
                    ]
                ],
                'filter' => CHtml::activeTextField($model, 'quantity', ['class' => 'form-control']),
            ],*/
            [
                'class' => 'webforma\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('/store/productBackend/inline'),
	            'editable' => [
					'mode' => 'popup',
				],
                'source' => $model->getStatusList(),
                'options' => [
                    Product::STATUS_ACTIVE => ['class' => 'label-success'],
                    Product::STATUS_NOT_ACTIVE => ['class' => 'label-info'],
                ],
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
                'frontViewButtonUrl' => function($data){
                    return ProductHelper::getUrl($data);
                },
                'template' => '{front_view} {update} {delete}',
                'buttons' => [
                    'front_view' => [
                        'visible' => function ($row, $data) {
                            return $data->status == Product::STATUS_ACTIVE;
                        }
                    ],
                    'images' => [
                        'icon' => 'fa fa-fw fa-picture-o',
                        'label' => Yii::t('StoreModule.store', 'Images'),
                        'url' => function ($data) {
                            return Yii::app()->createUrl('/store/productImageBackend/index', ['id' => $data->id]);
                        },
                        'options' => [
                            'class' => 'images btn btn-sm btn-default',
                        ]
                    ],
                ]
            ],
        ],
    ]
); ?>

<div class="modal fade" id="batch-actions" tabindex="-1" role="dialog" aria-labelledby="batchActions">
    <div class="modal-dialog" role="document">
        <?php
        $form = $this->beginWidget(
            '\webforma\widgets\ActiveForm',
            [
                'id' => 'batch-actions-form',
                'action' => ['/store/productBackend/batch'],
                'enableAjaxValidation' => false,
                'enableClientValidation' => true,
                'type' => 'vertical',
            ]
        ); ?>

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="batchActions">
                    <?= Yii::t('StoreModule.store', 'Batch actions') ?>
                </h4>
            </div>
            <div class="modal-body">
                <div id="no-product-selected" class="alert alert-danger hidden">
                    <?= Yii::t('StoreModule.store', 'No one product selected') ?>
                </div>
                <div id="batch-action-error" class="alert alert-danger hidden">
                    <?= Yii::t('StoreModule.store', 'Something going wrong. Sorry.') ?>
                </div>

				<div class="row">
					<div class="col-xs-12">
			            <?= $form->label($batchModel, 'price') ?>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4">
						<div class="form-group">
				            <?= $form->dropDownList($batchModel, 'price_op', ProductBatchHelper::getPericeOpList(), ['class' => 'form-control']) ?>
						</div>
					</div>
					<div class="col-xs-4">
						<div class="form-group">
				            <?= $form->textField($batchModel, 'price', ['class' => 'form-control']) ?>
				            <?= $form->error($batchModel, 'price') ?>
						</div>
					</div>
					<div class="col-xs-4">
						<div class="form-group">
				            <?= $form->dropDownList($batchModel, 'price_op_unit', ProductBatchHelper::getOpUnits(), ['class' => 'form-control']) ?>
						</div>
					</div>
				</div>

				<?if($this->getModule()->priceType == StoreModule::PRICE_TYPE_CHARGE):?>
					<div class="row">
						<div class="col-xs-12">
							<?= $form->label($batchModel, 'purchase_price') ?>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							<div class="form-group">
								<?= $form->dropDownList($batchModel, 'purchase_price_op', ProductBatchHelper::getPericeOpList(), ['class' => 'form-control']) ?>
							</div>
						</div>
						<div class="col-xs-4">
							<div class="form-group">
								<?= $form->textField($batchModel, 'purchase_price', ['class' => 'form-control']) ?>
								<?= $form->error($batchModel, 'purchase_price') ?>
							</div>
						</div>
						<div class="col-xs-4">
							<div class="form-group">
								<?= $form->dropDownList($batchModel, 'purchase_price_op_unit', ProductBatchHelper::getOpUnits(), ['class' => 'form-control']) ?>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12">
							<?= $form->label($batchModel, 'extra_charge') ?>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							<div class="form-group">
								<?= $form->dropDownList($batchModel, 'extra_charge_op', ProductBatchHelper::getPericeOpList(), ['class' => 'form-control']) ?>
							</div>
						</div>
						<div class="col-xs-4">
							<div class="form-group">
								<?= $form->textField($batchModel, 'extra_charge', ['class' => 'form-control']) ?>
								<?= $form->error($batchModel, 'extra_charge') ?>
							</div>
						</div>
					</div>
				<?endif;?>
                <?/*
                <div class="row">
                    <div class="col-xs-12">
                        <?= $form->label($batchModel, 'discount_price') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <?= $form->dropDownList($batchModel, 'discount_price_op', ProductBatchHelper::getPericeOpList(), ['class' => 'form-control']) ?>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <?= $form->textField($batchModel, 'discount_price', ['class' => 'form-control']) ?>
                            <?= $form->error($batchModel, 'discount_price') ?>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <?= $form->dropDownList($batchModel, 'discount_price_op_unit', ProductBatchHelper::getOpUnits(), ['class' => 'form-control']) ?>
                        </div>
                    </div>
                </div>
                */?>

                <div class="row">
					<?if($this->getModule()->priceType == StoreModule::PRICE_TYPE_CHARGE):?>
						<div class="col-sm-6">
							<?= $form->textFieldGroup($batchModel, 'discount') ?>
						</div>
					<?else:?>
						<div class="col-sm-6">
							<?= $form->textFieldGroup($batchModel, 'discount_price') ?>
						</div>
                    <?endif?>
                    <div class="col-sm-6">
                        <?= $form->textFieldGroup($batchModel, 'quantity') ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->dropDownListGroup($batchModel, 'producer_id', [
                            'widgetOptions' => [
                                'data' => Producer::model()->getFormattedList(),
                                'htmlOptions' => [
                                    'empty' => Yii::t('StoreModule.store', '--choose--'),
                                    'encode' => false,
                                ],
                            ],
                        ]) ?>
                    </div>

					<div class="col-sm-6">
						<div class="pareData">
							<?= $form->dropDownListGroup($batchModel, 'producer_line_id', [
								'widgetOptions' => [
									'data' => [],
									'htmlOptions' => [
										'empty' => Yii::t('StoreModule.store', '--choose--'),
										'encode' => false,
									],
								],
							]) ?>
						</div>
					</div>
				</div>
				<div class="row">
                    <div class="col-sm-6">
                        <?= $form->dropDownListGroup($batchModel, 'category_id', [
                            'widgetOptions' => [
                                'data' => StoreCategoryHelper::formattedList(),
                                'htmlOptions' => [
                                    'empty' => Yii::t('StoreModule.store', '--choose--'),
                                    'encode' => false,
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->dropDownListGroup($batchModel, 'status', [
                            'widgetOptions' => [
                                'data' => $model->getStatusList(),
                                'htmlOptions' => [
                                    'empty' => Yii::t('StoreModule.store', '--choose--'),
                                    'encode' => false,
                                ],
                            ],
                        ]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->dropDownListGroup($batchModel, 'in_stock', [
                            'widgetOptions' => [
                                'data' => $model->getInStockList(),
                                'htmlOptions' => [
                                    'empty' => Yii::t('StoreModule.store', '--choose--'),
                                    'encode' => false,
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->dropDownListGroup($batchModel, 'is_special', [
                            'widgetOptions' => [
                                'data' => [
                                    1 => Yii::t('StoreModule.store', 'Yes'),
                                    0 => Yii::t('StoreModule.store', 'No'),
                                ],
                                'htmlOptions' => [
                                    'empty' => Yii::t('StoreModule.store', '--choose--'),
                                    'encode' => false,
                                ],
                            ],
                        ]) ?>
                    </div>

                    <div class="col-sm-6">
                        <?= $form->dropDownListGroup($batchModel, 'is_new', [
                            'widgetOptions' => [
                                'data' => [
                                    1 => Yii::t('StoreModule.store', 'Yes'),
                                    0 => Yii::t('StoreModule.store', 'No'),
                                ],
                                'htmlOptions' => [
                                    'empty' => Yii::t('StoreModule.store', '--choose--'),
                                    'encode' => false,
                                ],
                            ],
                        ]) ?>
                    </div>
                    <?/*
                    <div class="col-sm-6">
                        <?= $form->textFieldGroup($batchModel, 'view') ?>
                    </div>
                    */?>
                </div>

				<div class="row">
					<div class="col-sm-6">
			            <?= $form->dropDownListGroup($batchModel, 'is_hit', [
				            'widgetOptions' => [
					            'data' => [
						            1 => Yii::t('StoreModule.store', 'Yes'),
						            0 => Yii::t('StoreModule.store', 'No'),
					            ],
					            'htmlOptions' => [
						            'empty' => Yii::t('StoreModule.store', '--choose--'),
						            'encode' => false,
					            ],
				            ],
			            ]) ?>
					</div>
				</div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <?= Yii::t('StoreModule.store', 'Close') ?>
                </button>
                <button type="submit" id="batch-apply" class="btn btn-success">
                    <?= Yii::t('StoreModule.store', 'Apply') ?>
                </button>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>

<?php
$url = Yii::app()->createUrl('/store/productBackend/copy');
$urlLine = Yii::app()->createUrl('/store/producerLineBackend/getjsonitems/');
$tokenName = Yii::app()->getRequest()->csrfTokenName;
$token = Yii::app()->getRequest()->csrfToken;

Yii::app()->getClientScript()->registerScript(
    __FILE__,
    <<<JS
    $(document).ready(function(){
    	$('#ProductBatchForm_producer_id').change(function () {
    		$('#ProductBatchForm_producer_line_id option').filter('[value!=""]').remove();
			var producerId = parseInt($(this).val());
			if (producerId) {
				$.post("$urlLine", {
					'$tokenName': "$token",
					'producerId': producerId
				}, function (response) {
					if (response.result) {
						$.each(response.data, function (index, element) {
							$('#ProductBatchForm_producer_line_id').append(new Option(element, index, false));
						});
						$('#ProductBatchForm_producer_line_id').removeAttr('disabled');
						$('#pareData').show();
					}
				});
			}
		});
    });
    $('body').on('click', '#copy-products', function (e) {
        e.preventDefault();
        var checked = $.fn.yiiGridView.getCheckedRowsIds('product-grid');
        if (!checked.length) {
            alert('No items are checked');
            return false;
        }
        if(confirm("Вы уверены, что хотите дублировать выбранные элементы?")){
            var url = "$url";
            var data = {};
            data['$tokenName'] = "$token";
            data['items'] = checked;
            $.ajax({
                url: url,
                type: "POST",
                dataType: "json",
                data: data,
                success: function (data) {
                    if (data.result) {
                        $.fn.yiiGridView.update("product-grid");
                    } else {
                        alert(data.data);
                    }
                },
                error: function (data) {alert("Ошибка!")}
            });
        }
    });

    $('#batch-actions-form').on('submit', function(e) {
        e.preventDefault();
        
        var checked = $.fn.yiiGridView.getCheckedRowsIds('product-grid');
        $('#batch-action-error').addClass('hidden');
        
        if (checked.length == 0) {
            return false;
        }

        var data = $(this).serialize() + '&ids=' + checked;

        data['$tokenName'] = "$token";

        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(data) {
                if (data.result === true) {
                    //$.fn.yiiGridView.update("product-grid");
                    $('#batch-actions').modal('hide');
                    window.location.reload();
                }
                
                if (data.result === false) {
                    $('#batch-action-error').removeClass('hidden');
                }
            },
            error: function(data) {
              
            }
        });
    });
    
    $('#batch-actions').on('show.bs.modal', function(e) {
        var checked = $.fn.yiiGridView.getCheckedRowsIds('product-grid');
        
        if (checked.length > 0) {
            return true;
        }
        
        $('#no-product-selected').removeClass('hidden');
        $('#batch-apply').attr('disabled', true);
        
    }).on('hidden.bs.modal', function(e) {
        $('#no-product-selected').addClass('hidden');
        $('#batch-apply').attr('disabled', false);
    });
JS
); ?>







