<?php
/* @var $variant ProductVariant */
$new = false;
if (!$variant->id) {
    $new = true;
    $variant->id = mt_rand(10000, 50000);
}
if($variant->amount == 0){
	$variant->amount = '';
}
if($variant->extra_charge !== null){
    $extraCharge = $variant->extra_charge;
} elseif($model) {
    $extraCharge = $model->getExtraCharge();
}
?>
<tr>
	<?/*
	<input type="hidden" name="ProductVariant[<?= $variant->id; ?>][type]" value="<?= $variant->type ? $variant->type : ProductVariant::TYPE_BASE_PRICE; ?>"/>
	*/?>
    <?php if (false === $new): ?>
        <input type="hidden" name="ProductVariant[<?= $variant->id; ?>][id]" value="<?= $variant->id; ?>"/>
    <?php endif; ?>
	<input class="variant_order" type="hidden" name="ProductVariant[<?= $variant->id; ?>][position]" value="<?= $variant->position; ?>">
    <td>
        <?= $variant->attribute->title; ?>
        <input type="hidden" value="<?= $variant->attribute_id; ?>" name="ProductVariant[<?= $variant->id; ?>][attribute_id]"/>
    </td>
    <td>
        <?php //if ($variant->attribute->isType(Attribute::TYPE_DROPDOWN)): ?>
            <?php// $option = AttributeOption::model()->findByAttributes(['attribute_id' => $variant->attribute_id, 'value' => $variant->attribute_value]); ?>
            <?//= AttributeRender::renderField($variant->attribute, ($option ? $option->id : null), 'ProductVariant[' . $variant->id . '][attributeOptionId]', ['class' => 'form-control']); ?>
        <?php //else: ?>
            <?= AttributeRender::renderField($variant->attribute, $variant->attribute_value, 'ProductVariant[' . $variant->id . '][attribute_value]', ['class' => 'form-control']); ?>
        <?php //endif; ?>
    </td>
    <td>
        <?= CHtml::dropDownList('ProductVariant[' . $variant->id . '][type]', $variant->type, $variant->getTypeList(), ['class' => 'form-control']); ?>
    </td>
	<td><input class="form-control" type="text" name="ProductVariant[<?= $variant->id; ?>][sku]" value="<?= $variant->sku; ?>"></td>
	<?if($this->getModule()->priceType == StoreModule::PRICE_TYPE_CHARGE):?>
		<td><input class="form-control purchase_price" type="text" name="ProductVariant[<?= $variant->id; ?>][purchase_price]" value="<?= $variant->purchase_price; ?>"></td>
	<?endif;?>
	<td><input class="form-control amount" type="text" name="ProductVariant[<?= $variant->id; ?>][amount]" value="<?= $variant->amount; ?>"></td>
	<?if($this->getModule()->priceType == StoreModule::PRICE_TYPE_CHARGE):?>
		<td><input class="form-control discount" type="text" name="ProductVariant[<?= $variant->id; ?>][discount]" value="<?= $variant->discount; ?>"></td>
		<td><input class="form-control extra_charge" type="text" name="ProductVariant[<?= $variant->id; ?>][extra_charge]" value="<?= $variant->extra_charge; ?>" placeholder="<?=$extraCharge?>" data-extra-charge="<?=$extraCharge?>"></td>
	<?else:?>
		<td><input class="form-control discount_price" type="text" name="ProductVariant[<?= $variant->id; ?>][discount_price]" value="<?= $variant->discount_price; ?>"></td>
	<?endif;?>
    <td><?= CHtml::dropDownList('ProductVariant['.$variant->id.'][in_stock]', $variant->in_stock, Product::getInStockListData(), ['class' => 'form-control']); ?></td>
	<td><input class="form-control" type="number" name="ProductVariant[<?= $variant->id; ?>][quantity]" value="<?= $variant->quantity; ?>"></td>
	<?if(!$model || $model->getIsNewRecord()):?>
		<td><?= CHtml::dropDownList('ProductVariant['.$variant->id.'][image_id]', $variant->image_id, [], ['class' => 'form-control']); ?></td>
	<?else:?>
		<td><?= CHtml::dropDownList('ProductVariant['.$variant->id.'][image_id]', $variant->image_id, ['' => '-- Не выбрано --'] + CHtml::listData($model->getImages(), 'id', 'name'), ['class' => 'form-control']); ?></td>
	<?endif;?>

    <td><a href="#" class="btn btn-danger btn-sm remove-variant">&times;</a></td>
</tr>
