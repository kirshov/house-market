<?php
/**
 * @var $this ProductBackendController
 * @var $model Product
 */

$this->layout = 'product';

$this->breadcrumbs = [
    Yii::t('StoreModule.store', 'Products') => ['/store/productBackend/index'],
    Yii::t('StoreModule.store', 'Creating'),
];

$this->pageTitle = Yii::t('StoreModule.store', 'Products - creating');

$this->title = Yii::t('StoreModule.store', 'Products');
$this->subTitle = Yii::t('StoreModule.store', 'creating');
?>

<?= $this->renderPartial('_form', ['model' => $model, 'imageGroup' => $imageGroup]); ?>
