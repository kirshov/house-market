<?php
/* @var $model Product - передается при рендере из формы редактирования товара */
/* @var $type Type - передается при генерации формы через ajax */
?>
<a href="javascript:void(0)" class="how_last_product">Как у последнего сохраненного товара</a>
<?php if (!empty($groups)): ?>
    <div class="row">
        <div class="col-sm-12">
            <?php foreach ($groups as $groupName => $items): ?>
                <fieldset>
                    <legend><?= CHtml::encode($groupName); ?></legend>
                    <?php foreach ($items as $attribute): ?>
                        <?php /* @var $attribute Attribute */ ?>
                        <?php $hasError = $model->hasErrors($attribute->name); ?>
                        <?if(!($attribute->isType(Attribute::TYPE_CHECKBOX_LIST) && $attribute->isGroupingItems())):?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="Attribute_<?= $attribute->name ?>" class="<?= $hasError ? 'has-error' : null; ?>">
                                        <?= $attribute->title; ?>
                                        <?php if ($attribute->required): ?>
                                            <span class="required">*</span>
                                        <?php endif; ?>
                                        <?php if ($attribute->unit): ?>
                                            <span>(<?= $attribute->unit; ?>)</span>
                                        <?php endif; ?>
                                    </label>
                                </div>
                            </div>
                        <?endif;?>

						<?php
							$render = '';
							$htmlOptions = $attribute->isType(Attribute::TYPE_CHECKBOX) || $attribute->isType(Attribute::TYPE_CHECKBOX_LIST) ? ['template' => '<span class="attribute-item">{input} {label}</span>', 'separator' => ''] : ['class' => 'form-control'];
							if ($attribute->isType(Attribute::TYPE_FILE)){
								if($model->attributeFile($attribute->name)){
									$render .= CHtml::openTag('div');
									$render .= CHtml::link(Yii::t('StoreModule.store', 'Download'), $model->attributeFile($attribute->name));
									$render .= Yii::t('StoreModule.store', 'or');
									$render .= CHtml::link(Yii::t('StoreModule.store', 'Delete'), null, ['class' => 'rm-file-attr', 'data-product' => $model->id, 'data-attribute' => $attribute->id,]);
									$render .= CHtml::closeTag('div');
								}
							} elseif($attribute->isType(Attribute::TYPE_CHECKBOX_LIST)) {
								$render .= CHtml::hiddenField('Attribute['.$attribute->id.'][]', '');
							}

							if($attribute->isType(Attribute::TYPE_DROPDOWN) || $attribute->isType(Attribute::TYPE_RADIO) || $attribute->isType(Attribute::TYPE_COLOR) || $attribute->isType(Attribute::TYPE_SHORT_TEXT)) {
								$render .= CHtml::openTag('div', ['class' => 'row']);
								$render .= CHtml::openTag('div', ['class' => 'col-sm-6']);
								$render .= AttributeRender::renderField($attribute, $model->attribute($attribute), null, $htmlOptions);
								$render .= CHtml::closeTag('div');
								$render .= CHtml::closeTag('div');
							} else {
								$render .= AttributeRender::renderField($attribute, $model->attribute($attribute), null, $htmlOptions);
							}

	                    ?>
						<div class="row form-group">
                            <div class="col-sm-12 qwqwe<?= $hasError ? 'has-error' : null; ?> attributes-items <?=($attribute->isType(Attribute::TYPE_CHECKBOX_LIST) ? 'checkbox-list' : '')?>">
								<?=$render;?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </fieldset>
            <?php endforeach; ?>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.rm-file-attr').on('click', function (event) {
                event.preventDefault();
                var $this = $(this);
                var product = parseInt($(this).data('product'));
                var attribute = parseInt($(this).data('attribute'));
                $.post('<?= Yii::app()->createUrl('/store/attributeBackend/deleteFile');?>', {
                    'product': product,
                    'attribute': attribute,
                    '<?= Yii::app()->getRequest()->csrfTokenName;?>': '<?= Yii::app()->getRequest()->csrfToken;?>'
                }, function (response) {
                    if (response.result) {
                        $this.parent('div').fadeOut();
                    }
                }, 'json');
            });
        });
    </script>
<?php endif; ?>
