<?php
$this->breadcrumbs = [
    Yii::t('StoreModule.store', 'Producers') => ['/store/producerBackend/index'],
    Yii::t('StoreModule.store', 'Producers line') => ['/store/producerLineBackend/index'],
    Yii::t('StoreModule.store', 'Manage'),
];

$this->pageTitle = Yii::t('StoreModule.store', 'Producers line - manage');

$this->menu = [
	[
		'icon' => 'fa fa-fw fa-list-alt',
		'label' => Yii::t('StoreModule.store', 'Manage producers line'),
		'url' => ['/store/producerLineBackend/index'],
	],
	[
		'icon' => 'fa fa-fw fa-plus-square',
		'label' => Yii::t('StoreModule.store', 'Create producer line'),
		'url' => ['/store/producerLineBackend/create'],
	],
	[
		'icon' => 'fa fa-fw fa-list-alt',
		'label' => Yii::t('StoreModule.store', 'Manage producers'),
		'url' => ['/store/producerBackend/index'],
	],
	[
		'icon' => 'fa fa-fw fa-plus-square',
		'label' => Yii::t('StoreModule.store', 'Create producer'),
		'url' => ['/store/producerBackend/create'],
	],
	[
		'icon' => 'fa fa-fw fa-list-alt',
		'label' => Yii::t('StoreModule.store', 'Country list'),
		'url' => ['/store/producerCountryBackend/index'],
	],
	[
		'icon' => 'fa fa-fw fa-plus-square',
		'label' => Yii::t('StoreModule.store', 'Create country'),
		'url' => ['/store/producerCountryBackend/create'],
	],
];
?>

<?php
$this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'producer-grid',
        'sortableRows' => true,
        'sortableAjaxSave' => true,
        'sortableAttribute' => 'sort',
        'sortableAction' => '/store/producerLineBackend/sortable',
        'type' => 'condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => [
	        [
		        'name' => 'image',
		        'type' => 'raw',
		        'value' => function ($data) {
			        return CHtml::image(
				        StoreImage::producerLine($data, 80, 80, false),
				        $data->name,
				        [
					        'width' => 80,
					        'height' => 80,
					        'class' => 'img-thumbnail',
				        ]
			        );
		        },
		        'filter' => false,
	        ],
            [
                'name' => 'name',
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::link($data->name, ['/store/producerLineBackend/update', 'id' => $data->id]);
                },
            ],
	        [
		        'class' => 'webforma\widgets\EditableStatusColumn',
		        'name' => 'producer_id',
		        'url' => $this->createUrl('/store/producerLineBackend/inline'),
		        'source' => Producer::model()->getFormattedList(),
	        ],
            [
                'class' => 'webforma\widgets\WEditableColumn',
                'name' => 'slug',
                'editable' => [
                    'url' => $this->createUrl('/store/producerLineBackend/inline'),
                    'mode' => 'popup',
                    'params' => [
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->request->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($model, 'slug', ['class' => 'form-control']),
            ],
            [
                'class' => 'webforma\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('/store/producerLineBackend/inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    Producer::STATUS_ACTIVE => ['class' => 'label-success'],
                    Producer::STATUS_NOT_ACTIVE => ['class' => 'label-info'],
                    Producer::STATUS_ZERO => ['class' => 'label-default'],
                ],
				'htmlOptions' => [
					'style' => 'width:150px;',
				],
            ],
            [
                'header' => Yii::t('StoreModule.store', 'Products'),
                'value' => function ($data) {
                    return CHtml::link(
                        $data->productCount,
                        ['/store/productBackend/index', 'Product[producer_line_id]' => $data->id],
                        ['class' => 'badge']
                    );
                },
                'type' => 'raw',
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
				'template' => '{front_view} {update} {delete}',
                'frontViewButtonUrl' => function ($data) {
                    return Yii::app()->createUrl('/store/producer/line', ['slug' => $data->producer->slug, 'line' => $data->slug]);
                },
                'buttons' => [
                    'front_view' => [
                        'visible' => function ($row, $data) {
                            return $data->producer->status == Producer::STATUS_ACTIVE;
                        },
                    ],
                ],
            ],
        ],
    ]
); ?>
