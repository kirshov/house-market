<?php
$this->breadcrumbs = [
    Yii::t('StoreModule.store', 'Producers') => ['/store/producerBackend/index'],
	Yii::t('StoreModule.store', 'Producers line') => ['/store/producerLineBackend/index'],
    $model->name . ' - ' . Yii::t('StoreModule.store', 'Edition'),
];

$this->pageTitle = Yii::t('StoreModule.store', 'Producer line - edition');

$this->menu = [
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('StoreModule.store', 'Manage producers line'),
        'url' => ['/store/producerLineBackend/index'],
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('StoreModule.store', 'Create producer line'),
        'url' => ['/store/producerLineBackend/create'],
    ],
];
?>

<?= $this->renderPartial('_form', ['model' => $model]); ?>
