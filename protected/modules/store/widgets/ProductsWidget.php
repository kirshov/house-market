<?php
Yii::import('application.modules.store.models.Product');

/**
 * Class ProductsFromCategoryWidget
 *
 * Show products from specified category
 *
 * @property string $slug Products category slug
 * @property bool|integer $limit The number of products. Default: false (unlimited)
 * @property string $order The order of products. Default: id DESC
 * @property string $view Widget view file
 */
class ProductsWidget extends \webforma\widgets\WWidget
{
	/**
	 * @var bool
	 */
	public $limit = -1;
	/**
	 * @var string
	 */
	public $view = 'default';

	/**
	 * @var string
	 */
	public $type;

	/**
	 * @var string
	 */
	public $ids;

	/**
	 * @var string
	 */
	public $after;

	/**
	 * @var
	 */
	public $category;

	/**
	 * @var array
	 */
	public $options = [];

	public function init()
	{
		if($this->options['isSlider']){
			$this->getController()->initPlugin('slick');
		}

		parent::init();
	}

	/**
	 * @return bool
	 * @throws CException
	 */
	public function run()
	{
		$module = Yii::app()->getModule('store');

		if($this->type && !in_array($this->type, ['special', 'new', 'popular'])){
			return null;
		}

		$scopes = ['published'];

		if($this->type == 'special'){
			if($module->productSpecialType == StoreModule::PRODUCTS_SPECIAL_TYPE_DISCOUNT){
				$scopes[] = 'specialOfferDiscount';
			} else {
				$scopes[] = 'specialOffer';
			}
		}

		if($this->type == 'new'){
			if($module->productNewType == StoreModule::PRODUCTS_NEW_TYPE_LAST){
				$scopes[] = 'newOfferLast';
			} else {
				$scopes[] = 'newOffer';
			}
		}

		if ($this->type == 'popular') {
			$scopes[] = 'popularOffer';
		}

		$criteria = new CDbCriteria([
			'distinct' => true,
			//'order' => 't.position ASC',
			'scopes' => $scopes,
			'with' => ['variants', 'category', 'type', 'attributesValues', 'type.typeAttributes', 'producer', 'producerLine'],
		]);

		if ($this->category) {
			$categories[] = (int) $this->category;
			$categories = CMap::mergeArray($categories, StoreCategory::model()->getChildsArray($this->category));
			$criteria->addInCondition('t.category_id', array_unique($categories));
		}


		if($this->ids){
			$ids = explode(',', $this->ids);
			$toCriteria = [];
			foreach ($ids as $_id){
				$_id = trim($_id);
				if(is_numeric($_id)){
					$toCriteria[] = $_id;
				}
			}
			if($toCriteria){
				$criteria->addInCondition('t.id', $toCriteria);
			}

			$this->limit = sizeof($toCriteria);
		}


		if(!$this->limit || $this->limit < 1){
			$this->limit = 100;
		}

		$criteria->limit = $this->limit;

		$this->render($this->view, [
			'products' => Product::model()->findAll($criteria),
			'type' => $this->type,
			'after' => $this->after,
			'markerTitles' => Product::getMarkerTitles(),
			'options' => $this->options,
		]);
	}
}