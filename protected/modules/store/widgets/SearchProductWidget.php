<?php

Yii::import('application.modules.store.components.AttributeFilter');

/**
 * Class SearchProductWidget
 */
class SearchProductWidget extends \webforma\widgets\WWidget
{
    /**
     * @var string
     */
    public $view = 'index';

	/**
	 * @var string
	 */
	public $placeholder;

	/**
	 * @var bool
	 */
	public $autocomplete = false;

    public function init()
    {
	    parent::init();
	    if($this->autocomplete){
			Yii::app()->getController()->initPlugin('autocomplete');
			Yii::app()->getClientScript()->registerScript('autocomplete', '$(function(){$("input.autocomplete").autocomplete();});', CClientScript::POS_HEAD);
		}
    }

	/**
     * @throws CException
     */
    public function run()
    {
        $this->render($this->view, [
        	'placeholder' => $this->placeholder,
        	'autocomplete' => $this->autocomplete,
		]);
    }
} 
