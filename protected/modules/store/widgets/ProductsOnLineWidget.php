<?php
Yii::import('application.modules.store.widgets.LinkedProductsWidget');
/**
 * Class ProductsOnLineWidget
 */
class ProductsOnLineWidget extends LinkedProductsWidget
{
    /**
     * @var string
     */
    public $view = 'products-on-line';


    /**
     * @throws CException
     */
    public function run()
    {
        if (!$this->product || !is_numeric($this->product->producer_line_id)) {
            return;
        }

        $criteria = new CDbCriteria();
	    $criteria->with = ['variants', 'producer', 'type', 'attributesValues'];
        $criteria->condition = 't.producer_line_id = :producer_line_id AND t.id != :id';
        $criteria->params = [
            'producer_line_id' => $this->product->producer_line_id,
            'id' => $this->product->id,
        ];
        $criteria->scopes = ['published'];
        $criteria->order = 't.position';

        $products = Product::model()->findAll($criteria);

        $this->render($this->view, [
            'products' => $products,
            'options' => $this->options,
        ]);
    }
} 
