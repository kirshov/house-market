<?php

/**
 * Class ProducerFilterWidget
 */
class ProducerFilterWidget extends \webforma\widgets\WWidget
{
    /**
     * @var int
     */
    public $limit = -1;

    /**
     * @var string
     */
    public $order = 'name ASC';

    /**
     * @var string
     */
    public $view = 'producer-filter';

    /**
     * @var StoreCategory
     */
    public $category;

    /**
     * @var int
     */
    public $ids;

    /**
     * @var Producer[]
     */
    public $producers;

    /**
     * @var ProducerRepository
     */
    protected $producerRepository;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->producerRepository = Yii::app()->getComponent('producerRepository');

        parent::init();
    }

    /**
     * @throws CException
     */
    public function run()
    {
        $criteria = new CDbCriteria([
            'limit' => $this->limit,
            'order' => $this->order,
            'scopes' => ['published']
        ]);

        if($this->category !== null){
            $producers = $this->producerRepository->getForCategory($this->category, $criteria);
        } elseif(!empty($this->ids)) {
            $criteria->addInCondition('id', $this->ids);
            $producers = Producer::model()->findAll($criteria);
        } elseif($this->producers) {
            $producers = $this->producers;
        } else {
            $producers = Producer::model()->findAll($criteria);
        }

        $this->render($this->view, [
            'producers' => $producers
        ]);
    }
} 
