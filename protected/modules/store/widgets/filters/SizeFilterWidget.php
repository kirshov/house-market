<?php
use webforma\widgets\WWidget;

/**
 * Class SizeFilterWidget
 */
class SizeFilterWidget extends WWidget
{
    /**
     * @var string
     */
    public $view = 'size-filter';

    /**
     * @throws CException
     */
    public function run()
    {
        $this->render($this->view);
    }
}