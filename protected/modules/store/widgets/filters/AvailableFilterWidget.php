<?php
use webforma\widgets\WWidget;

/**
 * Class AvailableFilterWidget
 */
class AvailableFilterWidget extends WWidget
{
    /**
     * @var string
     */
    public $view = 'checkbox-filter';

    /**
     * @throws CException
     */
    public function run()
    {
        $this->render($this->view);
    }
}