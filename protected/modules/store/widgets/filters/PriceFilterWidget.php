<?php

/**
 * Class PriceFilterWidget
 */
class PriceFilterWidget extends \webforma\widgets\WWidget
{
    /**
     * @var string
     */
    public $view = 'price-filter';

	/**
	 * @var integer
	 */
    public $max;

	/**
	 * @var integer
	 */
    public $min;

    /**
     * @var StoreCategory $category
     */
    public $category;
    /**
     * @throws CException
     */
    public function run()
    {
        $criteria = new CDbCriteria;

        if($this->category){
            $categories[] = $this->category->id;
            $categories = CMap::mergeArray($categories, StoreCategory::model()->published()->getChildsArray($this->category->id));
			$builder = new CDbCommandBuilder(Yii::app()->getDb()->getSchema());

			$criteria->addInCondition('t.category_id', array_unique($categories));
			$criteria->addCondition(sprintf('t.id IN (SELECT product_id FROM {{store_product_category}} WHERE %s)',
				$builder->createInCondition('{{store_product_category}}', 'category_id', $categories)), 'OR');
            $criteria->scopes = ['published'];
        }
        if($this->max === null){
	        $criteria->select = new CDbExpression('MAX(IF(t.discount_price, t.discount_price, t.price)) as maxPrice');
	        $max = Product::model()->cache(Yii::app()->getModule('webforma')->coreCacheTime)->find($criteria)->maxPrice;

	        $this->max = ceil($max / 100) * 100;
        }

		if($this->min === null){
			$criteria->select = new CDbExpression('MIN(IF(t.discount_price, t.discount_price, t.price)) as minPrice');
			$min = Product::model()->cache(Yii::app()->getModule('webforma')->coreCacheTime)->find($criteria)->minPrice;

			$this->min = floor($min / 100) * 100;
		}


        $this->render($this->view, [
            'max' => $this->max,
            'min' => $this->min,
        ]);
    }
}