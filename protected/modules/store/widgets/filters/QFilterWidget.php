<?php

class QFilterWidget extends \webforma\widgets\WWidget
{
    public $view = 'q-filter';

    public function run()
    {
        $this->render($this->view);
    }
}