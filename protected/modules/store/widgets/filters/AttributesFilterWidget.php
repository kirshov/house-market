<?php
/**
 * Class AttributesFilterWidget
 */
class AttributesFilterWidget extends \webforma\widgets\WWidget
{
    /**
     * @var
     */
    public $attributes;

    /**
     * @var
     */
    public $category;

	/**
	 * @var
	 */
	public $producerIds;

	/**
	 * @var
	 */
	public $lineIds;

	/**
	 * @var
	 */
	public $dropDownView;

    /**
     * @var AttributesRepository
     */
    protected $attributesRepository;


    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->attributesRepository = Yii::app()->getComponent('attributesRepository');
    }

    /**
     * @throws Exception
     */
    public function run()
    {
		$allAttributes = ('**' === $this->attributes);

        if ($this->category) {
            $this->attributes = $this->attributesRepository->getForCategory($this->category, $allAttributes);
        } elseif($this->producerIds){
	        $this->attributes = $this->attributesRepository->getForProducer($this->producerIds, $allAttributes);
        } elseif($this->lineIds){
	        $this->attributes = $this->attributesRepository->getForLine($this->lineIds, $allAttributes);
        }

        if ('*' === $this->attributes) {
            $this->attributes = Attribute::model()->with(['options'])->findAll([
                'condition' => 't.is_filter = 1',
                'order' => 't.sort DESC',
            ]);
        }

		if ('**' === $this->attributes) {
			$this->attributes = Attribute::model()->with(['options'])->findAll([
				'order' => 't.sort DESC',
			]);
		}

        if($this->attributes){
	        foreach ($this->attributes as $attribute) {
				if(is_string($attribute)){
					if($allAttributes){
						$model = Attribute::model()->findByAttributes([
							'name' => $attribute,
							//'is_filter' => \webforma\components\WebModule::CHOICE_YES,
							'order' => 't.sort',
						]);

					} else {
						$model = Attribute::model()->findByAttributes([
							'name' => $attribute,
							'is_filter' => \webforma\components\WebModule::CHOICE_YES,
							'order' => 't.sort',
						]);
					}
				} else {
					$model = $attribute;
				}

	            if ($model) {
	                switch ($model->type) {
	                   /* case Attribute::TYPE_DROPDOWN:
	                        $this->widget(
	                            'application.modules.store.widgets.filters.DropdownFilterWidget',
	                            ['attribute' => $model]
	                        );
	                        break;*/
		                case Attribute::TYPE_RADIO:
		                case Attribute::TYPE_DROPDOWN:
	                    case Attribute::TYPE_CHECKBOX_LIST:
	                        $this->widget(
	                            'application.modules.store.widgets.filters.DropdownFilterWidget', [
	                                'attribute' => $model,
		                            'view' => $this->dropDownView ?: 'dropdown-filter',
	                            ]
	                        );
	                        break;
	                    case Attribute::TYPE_CHECKBOX:
	                        $this->widget(
	                            'application.modules.store.widgets.filters.CheckboxFilterWidget',
	                            ['attribute' => $model]
	                        );
	                        break;
	                    case Attribute::TYPE_NUMBER:
	                        $this->widget(
	                            'application.modules.store.widgets.filters.NumberFilterWidget',
	                            ['attribute' => $model]
	                        );
	                        break;
	                    case Attribute::TYPE_SHORT_TEXT:
	                        $this->widget(
	                            'application.modules.store.widgets.filters.TextFilterWidget',
	                            ['attribute' => $model]
	                        );
	                        break;
	                }
	            }
	        }
        }
    }
} 
