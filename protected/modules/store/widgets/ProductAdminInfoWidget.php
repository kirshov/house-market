<?php

class ProductAdminInfoWidget extends \webforma\widgets\WWidget
{
	/**
	 * @var
	 */
	public $product;

	/**
	 * @var
	 */
	public $options = [];

	/**
	 * @var
	 */
	protected $defaultOptions = [
		'displayEditable' => true,
		'displayPurchasePrice' => false,
	];

	/**
	 * @var
	 */
	public $currency;

	public function init()
	{
		if(!Yii::app()->getModule('store')->priceType == StoreModule::PRICE_TYPE_CHARGE){
			return false;
		}

		if(!$this->currency){
			$this->currency = Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency);
		}

		$this->options = CMap::mergeArray($this->defaultOptions, $this->options);
	}


	public function run()
    {
    	if(!Yii::app()->getUser()->checkAccess('admin') && !Yii::app()->getUser()->isSuperUser()){
    		return false;
	    }
        $this->render('index', [
        	'product' => $this->product,
            'currency' => $this->currency,
	        'options' => $this->options,
	        'purchasePrices' => $this->options['displayPurchasePrice'] ? $this->getPurchasePrice() : [],
        ]);
    }

    protected function getPurchasePrice(){
	    $_pPrices = [];
	    $variantsGroups = $this->product->getVariantsGroup();
	    if ($variantsGroups){
		    foreach ($variantsGroups as $type => $variantsDataGroup){
			    foreach ($variantsDataGroup as $title => $variantsGroup){
				    if(sizeof($variantsGroup) > 1){
					    foreach ($variantsGroup as $item){
						    $_pPrices[] = ProductHelper::getFormatPrice($item->purchase_price);
					    }
				    }
			    }
		    }
	    }
	    if(!$_pPrices){
		    $_pPrices = [ProductHelper::getFormatPrice($this->product->purchase_price)];
	    }
	    return $_pPrices;
    }
}