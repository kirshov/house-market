<?php

Yii::import('application.modules.store.models.StoreCategory');

class CategoryMainWidget extends webforma\widgets\WWidget
{
	/**
	 * @var string
	 */
	public $view = 'default';

	/**
	 * @var int
	 */
    public $width = 100;

	/**
	 * @var int
	 */
	public $height = 100;

	/**
	 * @var int
	 */
	public $parentId;

	/**
	 * @var int
	 */
	public $limit = 5;

	/**
	 * @throws CException
	 */
    public function run()
    {
    	$criteria = new CDbCriteria();
    	if($this->parentId){
		    $criteria->condition = 'parent_id = :parent_id';
		    $criteria->params[':parent_id'] = $this->parentId;
	    }
    	$criteria->limit = $this->limit;
        $this->render($this->view, [
            'categories' => StoreCategory::model()->roots()->published()->findAll($criteria),
	        'width' => $this->width,
	        'height' => $this->height,
        ]);
    }
}
