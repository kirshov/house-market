<?php
Yii::import('application.modules.store.models.ProductGroups');

class ProductGroupsWidget extends \webforma\widgets\WWidget
{
	/**
	 * @var bool
	 */
	public $limit = -1;
	/**
	 * @var string
	 */
	public $view = 'default';

	/**
	 * @var
	 */
	public $category;

	/**
	 * @var array
	 */
	public $options = [];

	/**
	 * @return bool
	 * @throws CException
	 */
	public function run()
	{
		if(!$this->category){
			return false;
		}
		$criteria = new CDbCriteria();
		$criteria->order = 't.sort';
		$criteria->condition = 't.category_id = :categoryId AND show_after_filter = 1';
		$criteria->params = [
			':categoryId' => $this->category
		];

		$this->render($this->view, [
			'groups' => ProductGroups::model()->published()->findAll($criteria),
			'options' => $this->options,
		]);
	}
}