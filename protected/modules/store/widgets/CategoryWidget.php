<?php

Yii::import('application.modules.store.models.StoreCategory');

/**
 * Class CategoryWidget
 *
 * <pre>
 * <?php
 *    $this->widget('application.modules.store.widgets.CategoryWidget');
 * ?>
 * </pre>
 */
class CategoryWidget extends webforma\widgets\WWidget
{
    /**
     * @var int
     */
    public $parent = 0;

    /**
     * @var int
     */
    public $depth = 0;

	/**
	 * @var bool
	 */
	public $onlyDepth = false;

    /**
     * @var string
     */
    public $view = 'category-widget';

    /**
     * @var array
     */
    public $htmlOptions = [];

    /**
     * @var array
     */
    public $excluded = [];

    /**
     * @throws CException
     */
    public function run()
    {
    	$tree = (new StoreCategory())->published()->getMenuList($this->depth, $this->parent, $this->onlyDepth);
    	if(!$tree){
    		return false;
	    }

        $this->render($this->view, [
            'tree' => $tree,
            'htmlOptions' => $this->htmlOptions,
            'excluded' => $this->excluded,
        ]);
    }
}
