<?php

/**
 * Class LinkedProductsWidget
 */
class LinkedProductsWidget extends \webforma\widgets\WWidget
{
	/**
	 * @var string
	 */
	public $view = 'linked-products';

	/**
	 * @var null
	 */
	public $code = null;
	/**
	 * @var Product
	 */
	public $product;

	/**
	 * @var array
	 */
	public $options;

	/**
	 * @var ProductRepository
	 */
	protected $productRepository;

	/**
	 *
	 */
	public function init()
	{
		$this->productRepository = new ProductRepository();

		if($this->options['isSlider']){
			$this->getController()->initPlugin('slick');
		}

		if(!isset($this->options['similar_type'])){
			$this->options['similar_type'] = ProductLink::SIMILAR_TYPE_ADVANCED;
		}
	}

	/**
	 * @throws CException
	 */
	public function run()
	{
		if (!$this->product) {
			return;
		}

		$with = ['variants', 'type', 'attributesValues', 'type.typeAttributes'];
		$criteria = new CDbCriteria();
		$criteria->with = $with;
		$criteria->scopes = ['published'];
		$criteria->order = 'linked.position';
		$criteria->condition = '`t`.discontinued = 0';
		$criteria->join  = ' JOIN {{store_product_link}} linked ON t.id = linked.linked_product_id';
		$criteria->compare('linked.product_id', $this->product->id);
		if (null !== $this->code) {
			$criteria->join .= ' JOIN {{store_product_link_type}} link_type ON link_type.id = linked.type_id';
			$criteria->compare('link_type.code', $this->code);
		}
		if($this->limit){
			$criteria->limit = $this->limit;
		}

		$products = Product::model()->findAll($criteria);

		if($this->options['similar_type'] == ProductLink::SIMILAR_TYPE_ADVANCED && $this->code == 'similar' && $this->limit && sizeof($products) < $this->limit){
			$newCriteria = new CDbCriteria();
			$newCriteria->with = $with;
			$newCriteria->scopes = ['published'];
			$newCriteria->condition = '`t`.category_id = :category_id AND `t`.discontinued = 0';
			$newCriteria->params = [
				':category_id' => $this->product->category_id
			];
			$ids = [$this->product->id];
			foreach ($products as $_item){
				$ids[] = $_item->id;
			}
			$newCriteria->addNotInCondition('`t`.id', $ids);
			$newCriteria->order = 'RAND()';
			$newCriteria->limit = $this->limit - sizeof($products);

			$attributes = [];
			$attributeValue = new AttributeValue();

			foreach ($this->product->attributes() as $attrValue){
				if($attrValue->attribute->use_on_similar){
					$column = $attributeValue->column($attrValue->attribute->type);
					if(!$attributes[$attrValue->attribute->name]){
						$attributes[$attrValue->attribute->name] = [
							'value' => [],
							'attribute_id' => (int)$attrValue->attribute->id,
							'column' => $column,
						];
					}
					$attributes[$attrValue->attribute->name]['value'][] = $attrValue->{$column};
				}
			}

			if($attributes){
				$newCriteria->mergeWith($this->productRepository->buildCriteriaForTypeAttributes($attributes));
			}

			$advancedProducts = Product::model()->findAll($newCriteria);

			$products = array_merge($products, $advancedProducts);
		}

		$this->render($this->view, [
			'products' => $products,
			'options' => $this->options,
		]);
	}
}