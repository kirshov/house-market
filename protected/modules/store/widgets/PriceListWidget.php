<?php
Yii::import('application.modules.store.models.StoreCategory');
Yii::import('application.modules.store.models.Product');

/**
 * Class PriceListWidget
 *
 * Show products from specified category
 *
 * @property bool|integer $limit
 * @property string $categoryId
 * @property string $view
 */
class PriceListWidget extends \webforma\widgets\WWidget
{
	/**
	 * @var string
	 */
	public $view = 'index';

	/**
	 * @var
	 */
	public $categoryId;

	/**
	 * @var StoreCategory
	 */
	public $category;

	/**
	 * @var ProductRepository
	 */
	protected $productRepository;

	/**
	 * @var array
	 */
	protected $fieldList = ['name', 'unit', 'price'];
	/**
	 *
	 */
	public function init()
	{
		parent::init();
		$this->productRepository = Yii::app()->getComponent('productRepository');
	}


	/**
	 * @return bool
	 * @throws CException
	 */
	public function run()
	{
		$categories = $data = [];
		if($this->categoryId){
			$category = StoreCategory::model()->publishedAll()->findByPk($this->categoryId);
			if($category){
				$categories = CMap::mergeArray([$category], $category->children);
			}
		} elseif($this->category) {
			$categories = CMap::mergeArray([$this->category], $this->category->children);
		} else {
			$categories = StoreCategory::model()->publishedAll()->roots()->with(['children'])->findAll();
		}

		if($categories){
			foreach ($categories as $item){
				$products = $this->getProducts($item);
				if(!empty($products)){
					$data[$item->id] = [
						'title' => $item->name,
						'products' => $products,
					];
				}
			}
		}

		if(!$data){
			return false;
		}

		$this->render($this->view, [
			'data' => $data,
		]);
	}

	/**
	 * @param $category
	 * @return array
	 */
	protected function getProducts($category){
		$data = [];
		$dataProvider = $this->productRepository->getListForCategory($category, false, $limit = -1);
		if($dataProvider->getItemCount()){
			foreach ($dataProvider->getData() as $item){
				foreach ($this->fieldList as $field){
					$data[$item->id][$field] = $item->{$field};
				}
			}
		}

		return $data;
	}
}