<?php

class ProductGroupsBackendController extends webforma\components\controllers\BackController
{
	public function init()
	{
		if(!$this->getModule()->useProductGroups){
			throw new CHttpException(404, Yii::t('StoreModule.store', 'Page not found!'));
		}
		parent::init();
	}


	public function actions()
    {
        return [
            'inline' => [
                'class'           => 'webforma\components\actions\WInLineEditAction',
                'model'           => 'ProductGroups',
                'validAttributes' => [
                    'status',
                    'slug',
	                'show_after_filter',
                ]
            ],
            'sortable' => [
                'class' => 'webforma\components\actions\SortAction',
                'model' => 'ProductGroups',
                'attribute' => 'sort'
            ]
        ];
    }

    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin'],],
            ['allow', 'actions' => ['index'], 'roles' => ['Store.CategoryBackend.Index'],],
            ['allow', 'actions' => ['create'], 'roles' => ['Store.CategoryBackend.Create'],],
            ['allow', 'actions' => ['update', 'inline', 'sortable'], 'roles' => ['Store.CategoryBackend.Update'],],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Store.CategoryBackend.Delete'],],
            ['deny',],
        ];
    }

    /**
     *
     * @return void
     */
    public function actionCreate()
    {
        $model = new ProductGroups;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
	    $filters = '';
        if (($data = Yii::app()->getRequest()->getPost('ProductGroups')) !== null) {

            $model->setAttributes($data);

            if ($model->save()) {

                Yii::app()->user->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('StoreModule.store', 'Record was created!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['create']
                    )
                );
            }

	        $category = $model->category_id ? StoreCategory::model()->findByPk($model->category_id) : null;
			$request = Yii::app()->getRequest();

			Yii::app()->user->setState('pg_name', $request->getPost('name', ''));
			Yii::app()->user->setState('pg_brand', $request->getPost('brand', []));
			Yii::app()->user->setState('pg_price', $request->getPost('price', []));
			Yii::app()->user->setState('pg_attributes', $request->getPost('attributes', []));
			Yii::app()->user->setState('pg_ids', $request->getPost('ids', ''));
        } else {
	        Yii::app()->user->setState('pg_name', '');
	        Yii::app()->user->setState('pg_brand', []);
	        Yii::app()->user->setState('pg_price', []);
	        Yii::app()->user->setState('pg_attributes', []);
	        Yii::app()->user->setState('pg_ids', '');
        }

		$filters = $this->renderPartial('_filters', [
			'category' => $category,
		], true);

        $this->render('create', [
        	'model' => $model,
	        'filters' => $filters
        ]);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id the ID of the model to be updated
     *
     * @return void
     */
    public function actionUpdate($id)
    {
        // Указан ID новости страницы, редактируем только ее
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (($data = Yii::app()->getRequest()->getPost('ProductGroups')) !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('ProductGroups'));

            if ($model->save()) {

                Yii::app()->user->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    'Группа успешно сохранена!'
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id,
                        ]
                    )
                );
            }
        }


	    $category = $model->category_id ? StoreCategory::model()->findByPk($model->category_id) : null;

		Yii::app()->user->setState('pg_name', $model->options->name ?: '');
		Yii::app()->user->setState('pg_brand', $model->options->brand ?: []);
		Yii::app()->user->setState('pg_price', $model->options->price ?: []);
		Yii::app()->user->setState('pg_attributes', $model->options->attributes ?: []);
		Yii::app()->user->setState('pg_ids', $model->options->ids ?: '');

		$filters = $this->renderPartial('_filters', [
			'category' => $category,
		], true);

        $this->render(
            'update',
            [
                'model' => $model,
	            'filters' => $filters
            ]
        );
    }

    /**
     * @param integer $id
     *
     * @return void
     *
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            $transaction = Yii::app()->db->beginTransaction();

            try {
                // поддерживаем удаление только из POST-запроса
                $this->loadModel($id)->delete();
                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser

                $transaction->commit();

                if (!isset($_GET['ajax'])) {
                    $this->redirect(
                        (array)Yii::app()->getRequest()->getPost('returnUrl', 'index')
                    );
                }
            } catch (Exception $e) {
                $transaction->rollback();

                Yii::log($e->__toString(), CLogger::LEVEL_ERROR);
            }

        } else {
            throw new CHttpException(
                400,
                Yii::t('StoreModule.store', 'Bad request. Please don\'t use similar requests anymore')
            );
        }
    }

    /**
     * @return void
     */
    public function actionIndex()
    {
        $model = new ProductGroups('search');
        $model->unsetAttributes();  // clear any default values

        if (isset($_GET['ProductGroups'])) {
            $model->attributes = $_GET['ProductGroups'];
        }

        $this->render('index', ['model' => $model]);
    }

    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param integer $id идентификатор нужной модели
     *
     * @return ProductGroups $model
     *
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = ProductGroups::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('StoreModule.store', 'Page not found!'));
        }
        return $model;
    }

    /**
     * Производит AJAX-валидацию
     *
     * @param ProductGroups $model модель, которую необходимо валидировать
     *
     * @return void
     */
    protected function performAjaxValidation(ProductGroups $model)
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest() && Yii::app()->getRequest()->getPost('ajax') === 'category-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

	/**
	 * @param $id
	 * @throws CException
	 */
	public function actionGetFilterCatId($id){
		if(!Yii::app()->getRequest()->getIsAjaxRequest()){
			throw new CHttpException(404, Yii::t('StoreModule.store', 'Page not found!'));
		}
		$category = $id && is_numeric($id) ? StoreCategory::model()->findByPk($id) : null;

		$this->renderPartial('_filters', [
			'category' => $category,
		]);
	}
}
