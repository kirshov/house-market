<?php
Yii::import('application.modules.store.controllers.CategoryController');

/**
 * Class ProductGroupsController
 */
class ProductGroupsController extends CategoryController
{
	public function init()
	{
		if (!$this->getModule()->useProductGroups) {
			throw new CHttpException(404, Yii::t('StoreModule.store', 'Page not found!'));
		}
		parent::init();
	}

	/**
	 * @param $slug
	 * @throws CException
	 * @throws CHttpException
	 */
	public function actionCategory($slug)
	{
		$category = Yii::app()->getComponent('categoryRepository')->getByGroupAlias($slug);
		if (!$category) {
			throw new CHttpException(404);
		}
		$groups = ProductGroups::model()->published()->findAll([
			'condition' => 'category_id = :category_id',
			'params' => [':category_id' => $category->id],
			'order' => 'name',
		]);
		if (!$groups) {
			throw new CHttpException(404);
		}

		$this->render('category', [
			'category' => $category,
			'groups' => $groups,
		]);
	}

	/**
	 *
	 */
	public function actionIndex()
	{
		$groups = ProductGroups::model()->published()->with('category')->findAll('groups_alias != "" AND groups_alias IS NOT NULL');
		$categories = [];
		foreach ($groups as $group) {
			$key = $group->category->sort.'_'.$group->category->name;
			if (!isset($categories[$group->id])) {
				$categories[$key]['category'] = $group->category;
			}
			$categories[$key]['items'][] = $group;
		}

		$this->render('index', [
			'categories' => $categories,
		]);
	}

	/**
	 * @param $slug
	 * @param null $categorySlug
	 * @throws CException
	 * @throws CHttpException
	 */
    public function actionView($slug, $categorySlug = null)
    {
	    /**
	     * @var ProductGroups $group
	     */
        $group = ProductGroups::model()->published()->find('slug = :slug', [':slug' => $slug]);

        if(!$group){
            throw new CHttpException(404);
        } elseif($group->category_id && $group->use_category_url && empty($categorySlug)){
			throw new CHttpException(404);
		} elseif(!$group->category_id && !empty($categorySlug)){
			throw new CHttpException(404);
		}

	    /**
	     * @var AttributeFilter $attributeFilter
	     */
        $attributeFilter = Yii::app()->getComponent('attributesFilter');
        $mainSearchAttributesProducers = $mainSearchAttributes = $typeSearchAttributes = [];

	    $mainSearchAttributes[$attributeFilter::MAIN_SEARCH_PARAM_CATEGORY][] = $group->category_id;
		$request = Yii::app()->getRequest();

	    if($group->options->name){
		    $mainSearchAttributes[$attributeFilter::MAIN_SEARCH_PARAM_NAME_ONLY] = $group->options->name;
	    }

	    if($request->getParam('brand')){
		    $mainSearchAttributes[$attributeFilter::MAIN_SEARCH_PARAM_PRODUCER] = $request->getParam('brand');
	    } elseif($group->options->brand){
		    $mainSearchAttributes[$attributeFilter::MAIN_SEARCH_PARAM_PRODUCER] = (array) $group->options->brand;
	    }

	    if($request->getParam('price')){
		    $mainSearchAttributes[$attributeFilter::MAIN_SEARCH_PARAM_PRICE] = $request->getParam('price');
	    } elseif($group->options->price){
		    $mainSearchAttributes[$attributeFilter::MAIN_SEARCH_PARAM_PRICE] = (array) $group->options->price;
	    }

	    $typeSearchAttributes = $this->attributeFilter->getTypeAttributesForSearchFromQuery((array) $group->options->attributes);

	    if($group->options->ids){
			$typeSearchAttributes['ids'] = $group->options->ids;
		}

	    $mainSearchAttributesProducers[$attributeFilter::MAIN_SEARCH_PARAM_NAME_ONLY] = $group->options->name;
	    $mainSearchAttributesProducers[$attributeFilter::MAIN_SEARCH_PARAM_PRODUCER] = (array) $group->options->brand;
	    $mainSearchAttributesProducers[$attributeFilter::MAIN_SEARCH_PARAM_PRICE] = (array) $group->options->price;

		$products = $this->productRepository->getByFilter($mainSearchAttributes, $typeSearchAttributes, true);

        //$this->productRepository->getProducersForProducts($mainSearchAttributesProducers, $typeSearchAttributes)
		$criteria = $this->productRepository->getCriteriaByFilter($mainSearchAttributes, $typeSearchAttributes);
		$criteria->with = [];
		$criteria->distinct = true;
		$criteria->select = 't.producer_id';
		$producersIds = [];
		$productsProducers = Product::model()->published()->findAll($criteria);
		if($productsProducers){
			foreach ($productsProducers as $item){
				$producersIds[] = $item->producer_id;
			}
		}

        $this->render('view', [
            'dataProvider' => $products,
            'group' => $group,
            'producersIds' => $producersIds,
        ]);
    }

	/**
	 * @param $route
	 * @param array $params
	 * @param string $ampersand
	 * @return mixed
	 */
	public function createUrl($route, $params = array(), $ampersand = '&')
	{
 		if(!$route){
			list($address) = explode('?', Yii::app()->getRequest()->getUrl());
			$path = trim($address, '/');
			unset($params['path']);
			foreach (explode('/', $path) as $param){
				unset($params[$param]);
			}
			$params['slug'] = $path;
		}
		return \webforma\components\controllers\FrontController::createUrl($route, $params, $ampersand);
	}

	/**
	 * @param ProductGroups $group
	 * @return array
	 */
	public function getBreadcrumbs($group){
		$breadcrumbs = [];
		if($group->category_id && $group->use_category_url){
			$breadcrumbs = $group->category->getBreadcrumbs(true);
		}

		$breadcrumbs[] = $group->getTitle();

		return $breadcrumbs;
	}
}