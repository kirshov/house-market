<?php

use webforma\components\controllers\FrontController;

/**
 * Class ProductController
 */
class ProductController extends FrontController
{
    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var AttributeFilter
     */
    protected $attributeFilter;

    /**
     *
     */
    public function init()
    {
        $this->productRepository = Yii::app()->getComponent('productRepository');
        $this->attributeFilter = Yii::app()->getComponent('attributesFilter');

        parent::init();
    }

	/**
	 *
	 */
    public function actionIndex(){

    }

	/**
	 *
	 */
	public function actionNews()
	{
		$this->render('list-type', [
			'dataProvider' => $this->productRepository->getListByType('new'),
			'type' => 'new'
		]);
	}

	/**
	 *
	 */
	public function actionSpecial()
	{
		$this->render('list-type', [
			'dataProvider' => $this->productRepository->getListByType('special'),
			'type' => 'special'
		]);
	}

	/**
     *
     */
    public function actionSearch()
    {
        $term = trim(Yii::app()->getRequest()->getQuery(AttributeFilter::MAIN_SEARCH_QUERY_NAME));

	    if(Yii::app()->getRequest()->getIsAjaxRequest()){
		    if($term && strlen($term) >= 3){
				$this->renderPartial('_search-autocomplete', $this->productRepository->getForAutocomplete($term));
		    }
		    Yii::app()->end();
	    }

	    if(strlen($term) >= 3){
            $typesSearchParam = $this->attributeFilter->getTypeAttributesForSearchFromQuery($_GET);

            $mainSearchParam = $this->attributeFilter->getMainAttributesForSearchFromQuery(
                Yii::app()->getRequest(),
                [
                    AttributeFilter::MAIN_SEARCH_PARAM_NAME => Yii::app()->getRequest()->getQuery(AttributeFilter::MAIN_SEARCH_QUERY_NAME)
                ]
            );

            if (!empty($mainSearchParam) || !empty($typesSearchParam)) {
                $data = $this->productRepository->getByFilter($mainSearchParam, $typesSearchParam);
            }

            if(!isset($_GET['page'])){
                $word = preg_replace('/\s{2,}/iu', ' ', $term);

                $category = $this->productRepository->getCategoryByTerm($word);
                $producer = $this->productRepository->getProducerByTerm($word);
                $producerLine = $this->productRepository->getProducerLineByTerm($word);
            }
        }

        $this->render(
            'search', [
                'dataProvider' => $data,
                'category' => $category,
                'producer' => $producer,
                'producerLine' => $producerLine,
            ]
        );
    }

	/**
	 * @param $name
	 * @param null $category
	 * @throws CException
	 * @throws CHttpException
	 */
    public function actionView($name, $category = null)
    {
        $pathData = explode('/', Yii::app()->getRequest()->getPathInfo());
        if(sizeof($pathData) > 0) {
            $positionSlug = array_pop($pathData);
            if (preg_match('/^var-[0-9]+$/', $positionSlug)) {
                $variant = $positionSlug;
                $name = array_pop($pathData);
            }
        }

        $product = $this->productRepository->getBySlug($name);
        if (
            null === $product ||
            (isset($product->category) && $product->category->path !== $category) ||
            (!isset($product->category) && !is_null($category))
        ) {
            throw new CHttpException(404);
            //throw new CHttpException(404, Yii::t('StoreModule.catalog', 'Product was not found!'));
        }

        Yii::app()->eventManager->fire(StoreEvents::PRODUCT_OPEN, new ProductOpenEvent($product));

        if($variant){
            list(, $variantId) = explode('-', $variant);
            if(!is_numeric($variantId)){
                $variantId = null;
            }
        }

        if($product->discontinued){
        	$product->view = 'view-discontinued';
        }

        $this->render($product->view ?:'view', [
            'product' => $product,
            'variantId' => $variantId,
            'variant' => $variantId ? ProductVariant::model()->cache(0)->findByPk($variantId) : null,
        ]);
    }
}