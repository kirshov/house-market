<?php
use webforma\components\controllers\FrontController;

/**
 * Class CategoryController
 */
class CategoryController extends FrontController
{
    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var StoreCategoryRepository
     */
    protected $categoryRepository;

    /**
     * @var AttributeFilter
     */
    protected $attributeFilter;

	/**
	 * @var StoreCategory
	 */
    public $currentCategory;

    /**
     *
     */
    public function init()
    {
        parent::init();
        $this->productRepository = Yii::app()->getComponent('productRepository');
        $this->attributeFilter = Yii::app()->getComponent('attributesFilter');
        $this->categoryRepository = Yii::app()->getComponent('categoryRepository');
    }

    /**
     *
     */
    public function actionIndex()
    {
		$typesSearchParam = $this->attributeFilter->getTypeAttributesForSearchFromQuery($_GET);
		$mainSearchParam = $this->attributeFilter->getMainAttributesForSearchFromQuery(Yii::app()->getRequest(), []);

		$data = $this->productRepository->getByFilter($mainSearchParam, $typesSearchParam, !empty($params));

		$this->render('index', [
			'dataProvider' => $data,
		]);
    }

	/**
	 * @param $path
	 * @throws CException
	 * @throws CHttpException
	 */
    public function actionView($path)
    {
    	list($path) = explode('?', Yii::app()->getRequest()->getUrl());
        $category = $this->categoryRepository->getByPath($path);

        if (null === $category) {
	        $path = Yii::app()->getRequest()->getPathInfo();
	        $pathData = explode('/', $path);
	        if(sizeof($pathData) > 1) {
		        $positionSlug = array_pop($pathData);
		        if(preg_match('/^var-[0-9]+$/', $positionSlug)){
                    $positionSlug = array_pop($pathData);
                }

		        /**
		         * @var ProductController $controller
		         */
		        list($controller) = Yii::app()->createController('store/product');
		        $controller->init();

		        return $controller->actionView($positionSlug, implode('/', $pathData));

	        } else {
		        throw new CHttpException(404);
	        }
        }

        $typesSearchParam = $this->attributeFilter->getTypeAttributesForSearchFromQuery($_GET);
        $mainSearchParam = $this->attributeFilter->getMainAttributesForSearchFromQuery(Yii::app()->getRequest(), [
			AttributeFilter::MAIN_SEARCH_PARAM_CATEGORY => Yii::app()->getRequest()->getQuery('category', [$category->id]),
		]);

        if ((!empty($mainSearchParam)) || !empty($typesSearchParam)) {
	        $params = CMap::mergeArray($mainSearchParam, $typesSearchParam);
	        unset($params['category']);

            $data = $this->productRepository->getByFilter($mainSearchParam, $typesSearchParam, !empty($params));
        } else {
            $data = $this->productRepository->getListForCategory($category);
        }

	    $this->currentCategory = $category;

        $categoryProvider = new CActiveDataProvider('StoreCategory', [
	        'criteria' => [
	        	'condition' => 'parent_id = :parentId AND status = :status',
		        'params' => [
		        	':parentId' => $category->id,
			        ':status' => StoreCategory::STATUS_PUBLISHED,
		        ],
		        'order' => 'sort ASC',
	        ],
	        'pagination' => false,
        ]);

        $this->render($category->view ?: 'view', [
            'dataProvider' => $data,
            'categoryDataProvider' => $categoryProvider,
            'category' => $category,
        ]);
    }

	public function createUrl($route, $params = array(), $ampersand = '&')
	{
		if(!$route){
			list($address) = explode('?', Yii::app()->getRequest()->getUrl());
			$path = trim($address, '/');
			unset($params['path']);
			foreach (explode('/', $path) as $param){
				unset($params[$param]);
			}
			$params['path'] = $path;
		}

		return parent::createUrl($route, $params, $ampersand);
	}
}