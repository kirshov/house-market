<?php
use webforma\components\controllers\FrontController;

/**
 * Class ProducerController
 */
class ProducerController extends FrontController
{
    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var ProducerRepository
     */
    protected $producerRepository;

    /**
     *
     */
    public function init()
    {
        $this->productRepository = Yii::app()->getComponent('productRepository');

        $this->producerRepository = Yii::app()->getComponent('producerRepository');

        parent::init();
    }

    /**
     *
     */
    public function actionIndex()
    {
	    $brands = Producer::model()->published()->findAll(['order' => 'name']);
	    $letters = [];
		if($brands){
			foreach ($brands as $brand){
				$firstLetter = mb_substr($brand->name, 0, 1);
				if(!preg_match('/[A-Za-z]/', $firstLetter)){
					$firstLetter = 'А-Я';
				}
				$letters[$firstLetter][] = $brand;
			}
		}

        $this->render(
            'index',
            [
                'brandsLetters' => $letters,
            ]
        );
    }

    /**
     *
     */
    public function actionCountryIndex(){
        $this->render('country_list', [
            'countries' => ProducerCountry::model()->published()->findAll(['order' => 'IF(h1 != "", h1, name)']),
        ]);
    }

    public function actionCountry($slug){
        $country = Yii::app()->getComponent('producerRepository')->getCountryBySlug($slug);

        if (null === $country) {
            throw new CHttpException(404, Yii::t('StoreModule.store', 'Page not found!'));
        }
	    $attributeFilter = Yii::app()->getComponent('attributesFilter');
	    $typesSearchParam = $attributeFilter->getTypeAttributesForSearchFromQuery($_GET);
	    $mainSearchParam = $attributeFilter->getMainAttributesForSearchFromQuery(Yii::app()->getRequest());

        $this->render('country', [
            'country' => $country,
            'products' => $this->productRepository->getByCountry($country, $mainSearchParam, $typesSearchParam),
        ]);
    }

    /**
     * @param $slug
     * @throws CHttpException
     */
    public function actionView($slug)
    {
        $producer = $this->producerRepository->getBySlug($slug);

        if (null === $producer) {
            throw new CHttpException(404, Yii::t('StoreModule.store', 'Page not found!'));
        }

	    $attributeFilter = Yii::app()->getComponent('attributesFilter');
	    $typesSearchParam = $attributeFilter->getTypeAttributesForSearchFromQuery($_GET);
	    $mainSearchParam = $attributeFilter->getMainAttributesForSearchFromQuery(Yii::app()->getRequest());

        $this->render($producer->view ?: 'view', [
            'brand' => $producer,
            'products' => $this->productRepository->getByBrand($producer, null, $mainSearchParam, $typesSearchParam),
        ]);
    }

    /**
     * @param $slug
     * @param $slug
     * @throws CHttpException
     */
    public function actionLine($slug, $line)
    {
        $producer = $this->producerRepository->getBySlug($slug, []);
        if (null === $producer) {
            throw new CHttpException(404, Yii::t('StoreModule.store', 'Page not found!'));
        }

        $line = $this->producerRepository->getLineBySlug($line, $producer->id);
        if (null === $line) {
            throw new CHttpException(404, Yii::t('StoreModule.store', 'Page not found!'));
        }

		$attributeFilter = Yii::app()->getComponent('attributesFilter');
		$typesSearchParam = $attributeFilter->getTypeAttributesForSearchFromQuery($_GET);
		$mainSearchParam = $attributeFilter->getMainAttributesForSearchFromQuery(Yii::app()->getRequest());

        $this->render('line', [
			'brand' => $producer,
			'line' => $line,
			'products' => $this->productRepository->getByBrand($producer, $line, $mainSearchParam, $typesSearchParam),
		]);
    }
}
