<?php

/**
 * Class ProductBackendController
 */
class ProductBackendController extends webforma\components\controllers\BackController
{
    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->productRepository = Yii::app()->getComponent('productRepository');
    }

    /**
     * @return array
     */
    public function filters()
    {
        return CMap::mergeArray(
            parent::filters(),
            [
                'postOnly + batch',
            ]
        );
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'inline' => [
                'class' => 'webforma\components\actions\WInLineEditAction',
                'model' => 'Product',
                'validateModel' => false,
                'validAttributes' => [
                    'status',
                    'in_stock',
                    'price',
                    'discount_price',
                    'sku',
                    'type_id',
                    'quantity',
                    'producer_id'
                ],
            ],
            'sortable' => [
                'class' => 'webforma\components\actions\SortAction',
                'model' => 'Product',
            ],
            'sortrelated' => [
                'class' => 'webforma\components\actions\SortAction',
                'model' => 'ProductLink',
            ],
        ];
    }

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin'],],
            ['allow', 'actions' => ['index'], 'roles' => ['Store.ProductBackend.Index'],],
            ['allow', 'actions' => ['view'], 'roles' => ['Store.ProductBackend.View'],],
            ['allow', 'actions' => ['create', 'copy', 'batch'], 'roles' => ['Store.ProductBackend.Create'],],
            [
                'allow',
                'actions' => ['update', 'inline', 'sortable', 'deleteImage', 'batch'],
                'roles' => ['Store.ProductBackend.Update'],
            ],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Store.ProductBackend.Delete'],],
            [
                'allow',
                'actions' => ['typeAttributes'],
                'roles' => ['Store.ProductBackend.Create', 'Store.ProductBackend.Update'],
            ],
            [
                'allow',
                'actions' => ['typeAttributesForm'],
                'roles' => ['Store.ProductBackend.Create', 'Store.ProductBackend.Update'],
            ],
            [
                'allow',
                'actions' => ['variantRow'],
                'roles' => ['Store.ProductBackend.Create', 'Store.ProductBackend.Update'],
            ],
            ['deny',],
        ];
    }

    /**
     * Отображает товар по указанному идентификатору
     * @param integer $id Идентификатор товар для отображения
     */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }

    /**
     * Создает новую модель товара.
     * Если создание прошло успешно - перенаправляет на просмотр.
     */
    public function actionCreate()
    {
        $model = new Product();

        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('Product')) {

            $attributes = Yii::app()->getRequest()->getPost('Product');
            $typeAttributes = Yii::app()->getRequest()->getPost('Attribute', []);
            $variants = Yii::app()->getRequest()->getPost('ProductVariant', []);
            $categories = Yii::app()->getRequest()->getPost('categories', []);
            $tabs = Yii::app()->getRequest()->getPost('ProductTabs', []);

            if ($model->saveData($attributes, $typeAttributes, $variants, $categories, $tabs)) {

                $this->updateProductImages($model);

                $this->uploadAttributesFiles($model);

                Yii::app()->getUser()->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('StoreModule.store', 'Record was created!')
                );

	            $submitType = Yii::app()->getRequest()->getPost('submit-type');

	            if ($submitType == 'update') {
		            $this->redirect(['update', 'id' => $model->id]);
	            }
	            if($submitType == null){
		            $submitType = 'create';
	            }

                $this->redirect([$submitType]);

            } else {
                Yii::app()->getUser()->setFlash(
                    webforma\widgets\WFlashMessages::ERROR_MESSAGE,
                    Yii::t('StoreModule.store', 'Failed to save product!')
                );
            }
        }
        $this->render('create', [
            'model' => $model,
            'imageGroup' => ImageGroup::model(),
        ]);
    }

    /**
     * Редактирование товара.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('Product')) {

            $attributes = Yii::app()->getRequest()->getPost('Product');
            $typeAttributes = Yii::app()->getRequest()->getPost('Attribute', []);
            $variants = Yii::app()->getRequest()->getPost('ProductVariant', []);
            $categories = Yii::app()->getRequest()->getPost('categories', []);
	        $tabs = Yii::app()->getRequest()->getPost('ProductTabs', []);

            if ($model->saveData($attributes, $typeAttributes, $variants, $categories, $tabs)) {

                $this->updateProductImages($model);

                $this->uploadAttributesFiles($model);

                Yii::app()->getUser()->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('StoreModule.store', 'Record was updated!')
                );

	            if(Yii::app()->getRequest()->getPost('submit-type') == 'index'){
					$page = $this->getPageForModel($model);
					if($page > 1) {
						$this->redirect([$_POST['submit-type'], 'Product_page' => $page]);
					}

	            }

                if (!isset($_POST['submit-type'])) {
                    $this->redirect(['update', 'id' => $model->id]);
                } else {
                    $this->redirect([$_POST['submit-type']]);
                }
            } else {
                Yii::app()->getUser()->setFlash(
                    webforma\widgets\WFlashMessages::ERROR_MESSAGE,
                    Yii::t('StoreModule.store', 'Failed to save product!')
                );
            }
        }
        if($model->price == 0){
        	$model->price = '';
        }

        $searchModel = new ProductSearch('search');
        $searchModel->unsetAttributes();
        $searchModel->setAttributes(
            Yii::app()->getRequest()->getQuery('ProductSearch')
        );

        $this->render(
            'update',
            [
                'model' => $model,
                'searchModel' => $searchModel,
                'imageGroup' => ImageGroup::model(),
            ]
        );
    }

    /**
     * @param $model
     */
    protected function uploadAttributesFiles($model)
    {
        if (!empty($_FILES['Attribute']['name'])) {
            foreach ($_FILES['Attribute']['name'] as $key => $file) {
                $value = AttributeValue::model()->find('product_id = :product AND attribute_id = :attribute', [
                    ':product' => $model->id,
                    ':attribute' => $key,
                ]);

                $value = $value ?: new AttributeValue();

                $value->setAttributes([
                    'product_id' => $model->id,
                    'attribute_id' => $key,
                ]);

                $value->addFileInstanceName('Attribute['.$key.'][name]');
                if (false === $value->save()) {
                    Yii::app()->getUser()->setFlash(\webforma\widgets\WFlashMessages::ERROR_MESSAGE,
                        Yii::t('StoreModule.store', 'Error uploading some files...'));
                }
            }
        }
    }

    /**
     * @param Product $product
     */
    protected function updateProductImages(Product $product)
    {
        if (Yii::app()->getRequest()->getPost('ProductImage')) {
            foreach (Yii::app()->getRequest()->getPost('ProductImage') as $key => $val) {
                $productImage = ProductImage::model()->findByPk($key);
                if (null === $productImage) {
                    $productImage = new ProductImage();
                    $productImage->product_id = $product->id;
                    $productImage->addFileInstanceName('ProductImage['.$key.'][name]');
                }
                $productImage->setAttributes($_POST['ProductImage'][$key]);
                if (false === $productImage->save()) {
                    Yii::app()->getUser()->setFlash(\webforma\widgets\WFlashMessages::ERROR_MESSAGE,
                        Yii::t('StoreModule.store', 'Error uploading some images...'));
                }
            }
        }
    }

    /**
     * @throws CHttpException
     */
    public function actionDeleteImage()
    {
        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getIsAjaxRequest()) {

            $id = (int)Yii::app()->getRequest()->getPost('id');

            $model = ProductImage::model()->findByPk($id);

            if (null !== $model && $model->delete()) {
                Yii::app()->ajax->success();
            }
        }

        throw new CHttpException(404);
    }

    /**
     * Удаяет модель товара из базы.
     * Если удаление прошло успешно - возвращется в index
     * @param integer $id идентификатор товара, который нужно удалить
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            $this->loadModel($id)->delete();

            Yii::app()->getUser()->setFlash(
                webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                Yii::t('StoreModule.store', 'Record was removed!')
            );

            if (!isset($_GET['ajax'])) {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : ['index']);
            }
        } else {
            throw new CHttpException(400, Yii::t('StoreModule.store', 'Unknown request. Don\'t repeat it please!'));
        }
    }

    /**
     * Управление товарами.
     */
    public function actionIndex()
    {
        $model = new Product('search');
        $model->unsetAttributes();
        if (Yii::app()->getRequest()->getQuery('Product')) {
            $model->setAttributes(
                Yii::app()->getRequest()->getQuery('Product')
            );
        }

        if(Yii::app()->getRequest()->getIsAjaxRequest()){
			$this->renderPartial('index', [
				'model' => $model,
				'batchModel' => new ProductBatchForm(),
			]);
		} else {
			$this->render('index', [
				'model' => $model,
				'batchModel' => new ProductBatchForm(),
			]);
		}

    }

    /**
     * @param $id
     * @return Product
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Product::model()->with('images')->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, Yii::t('StoreModule.store', 'Page not found!'));
        }

        $this->setProductType($model);
        return $model;
    }

	/**
	 * @param Product $model
	 */
    private function setProductType($model){
    	if(!$model->type_id && $model->category){
    		$category = $model->category;
    		while ($category){
    			if($category->product_type == -1){
    				return false;
			    }
			    if($category->product_type){
			    	$model->type_id = $category->product_type;
			    	return;
			    }
			    $category = $category->parent;
		    }
	    }
    }

    /**
     * @param $id
     * @throws CException
     * @throws CHttpException
     */
    public function actionTypeAttributesForm($id)
    {
        $type = Type::model()->findByPk($id);

        if (null === $type) {
            throw new CHttpException(404);
        }

        $this->renderPartial('_attribute_form', ['groups' => $type->getAttributeGroups(), 'model' => new Product()]);
    }

    /**
     * @param $id
     * @throws CException
     */
    public function actionVariantRow($id)
    {
        $variant = new ProductVariant();
        $variant->setAttributes(
            [
                'attribute_id' => (int)$id,
            ]
        );
        $model = null;

        if($modelId = Yii::app()->getRequest()->getParam('modelId')){
        	$model = Product::model()->findByPk($modelId);
        }

        $this->renderPartial('_variant_row', ['variant' => $variant, 'model' => $model]);
    }

    /**
     * @param $id
     * @throws CHttpException
     */
    public function actionTypeAttributes($id)
    {
        $type = Type::model()->findByPk($id);

        if (null === $type) {
            throw new CHttpException(404);
        }

        $types = [];

        $noSupported = [Attribute::TYPE_FILE, Attribute::TYPE_TEXT, Attribute::TYPE_CHECKBOX_LIST];

        foreach ($type->typeAttributes as $attr) {

            if (in_array($attr->type, $noSupported)) {
                continue;
            }

            if (in_array($attr->type, [Attribute::TYPE_DROPDOWN, Attribute::TYPE_RADIO, Attribute::TYPE_COLOR])) {
                $types[] = array_merge($attr->attributes, ['options' => $attr->options]);
            } else {
                if (in_array($attr->type, [Attribute::TYPE_CHECKBOX, Attribute::TYPE_SHORT_TEXT])) {
                    $types[] = array_merge($attr->attributes, ['options' => []]);
                } else {
                    $types[] = $attr->attributes;
                }
            }
        }

        Yii::app()->ajax->raw($types);
    }

    /**
     *
     */
    public function actionCopy()
    {
        if ($data = Yii::app()->getRequest()->getPost('items')) {
            foreach ($data as $id) {
                $model = Product::model()->findByPk($id);
                if ($model) {
                    $model->copy();
                }
            }

            Yii::app()->ajax->success();
        }
    }

    /**
     *
     */
    public function actionBatch()
    {
        $form = new ProductBatchForm();
        $form->setAttributes(Yii::app()->getRequest()->getPost('ProductBatchForm'));

        if ($form->validate() === false) {
            Yii::app()->ajax->failure(Yii::t('StoreModule.store', 'Wrong data'));
        }

        if ($this->productRepository->batchUpdate($form, explode(',', Yii::app()->getRequest()->getPost('ids')))) {
            Yii::app()->ajax->success('ok');
        }

        Yii::app()->ajax->failure();
    }

	/**
	 *
	 */
    public function actionGetAttrLastProduct(){
    	$id = Yii::app()->getRequest()->getParam('id');
    	if($id){
		    $criteria = [
			    'condition' => 't.id != :id',
			    'params' => [':id' => (int) $id],
		    ];
	    }
	    $criteria['order'] = 'update_time DESC';
    	//$product = Product::model()->with('attributesValues')->find($criteria);
    	$product = Product::model()->find($criteria);

	    $data['type_id'] = $product->type_id;
	    foreach ($product->attributesValues as $p){
	    	$data['attributes'][$p->attribute_id][] = $p->option_value;
	    }
	    echo json_encode($data);
	    Yii::app()->end();
    }

	/**
	 * @param Product $model
	 * @return bool
	 */
	public function getPageForModel($model){
		/**
		 * @var StoreModule $module
		 */
    	$module = Yii::app()->getModule('store');
    	$defaultOrderField = $module->defaultSort;
    	if(!$defaultOrderField){
    		return false;
	    }
	    //$direction = $module->defaultSortDirection == 'ASC' ? '<' : '>';

		$modSettings = Yii::app()->getUser()->getState(\YWebUser::STATE_MOD_SETTINGS, null);
		$pageSize = $modSettings['product']['pageSize'] ?: CPagination::DEFAULT_PAGE_SIZE;
		$count = Product::model()->count([
			//'condition' => 't.'.$defaultOrderField.$direction.$model->{$defaultOrderField},
			'condition' => 't.id <= '.$model->id,
			'order' => $module->getDefaultSort(),
		]);
		if(!$count){
			return false;
		}
		return (ceil($count/$pageSize));
	}

	/**
	 *
	 */
	public function actionRefreshPrice(){
        set_time_limit(0);
        $products = $this->getProducts();
        $offset = 0;
        while(!empty($products)){
            foreach ($products as $product){
                $product->save();
            }
            $offset += 50;
            $products = $this->getProducts($offset);
        }

        $this->redirect(Yii::app()->createUrl('/store/productBackend/index'));
    }

    /**
     * @param $offset
     * @return Product[]
     */
    private function getProducts($offset = 0){
	    return Product::model()->findAll([
	        'order' => 'id',
            'limit' => 50,
            'offset' => $offset
        ]);
    }

	/**
	 *
	 */
    public function actionDeleteTab(){
    	$key = Yii::app()->getRequest()->getPost('key');
    	if(!$key || !Yii::app()->getRequest()->getIsAjaxRequest()){
		    throw new CHttpException(404);
	    }

    	$settings = \webforma\models\Settings::fetchModuleSettings('store', ['productTabs']);
    	$tabs = json_decode($settings['productTabs']->param_value, true);

    	unset($tabs[$key]);

    	\webforma\models\Settings::saveModuleSettings('store', ['productTabs' => json_encode($tabs)], ['productTabs' => 3]);

    	ProductTabs::model()->deleteAll('`key` = :key', [':key' => $key]);

	    Yii::app()->getModule('store')->getSettings(true);
    }
}
