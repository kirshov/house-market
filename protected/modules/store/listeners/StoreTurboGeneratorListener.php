<?php

use webforma\components\Event;

Yii::import('application.modules.store.models.StoreCategory');

/**
 * Class StoreTurboGeneratorListener
 */
class StoreTurboGeneratorListener
{
    /**
     * @param Event $event
     */
    public static function onGenerate(Event $event)
    {
        $generator = $event->getGenerator();

	    $categoryProvider = new CActiveDataProvider(StoreCategory::model()->publishedAll());

	    foreach (new CDataProviderIterator($categoryProvider) as $item) {
		    $generator->addItem(
			    Yii::app()->createAbsoluteUrl('/store/category/view', ['path' => $item->path]),
			    null,
			    SitemapHelper::FREQUENCY_DAILY,
			    0.5
		    );
	    }

	    //$productGroupsProvider = new CActiveDataProvider(ProductGroups::model()->published());

	    /*foreach (new CDataProviderIterator($productGroupsProvider) as $item) {
		    $generator->addItem(
			    Yii::app()->createAbsoluteUrl('/store/productGroups/view', ['slug' => $item->slug]),
			    null,
			    SitemapHelper::FREQUENCY_DAILY,
			    0.5
		    );
	    }*/
    }
} 
