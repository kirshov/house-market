<?php
use webforma\components\Event;

/**
 * Class StoreListener
 */
class StoreListener
{

	/**
	 * @param Event $event
	 */
    public static function onChange(Event $event){
        if($event->owner instanceof StoreCategory){
            self::clearCategoryCache();
        }

        if($event->owner instanceof Provider){
            Yii::app()->cache->delete('providers_backend');
            Yii::app()->cache->delete('providers_frontend');
        }

        self::clearFiles();
    }

	/**
	 *
	 */
    public static function clearCategoryCache(){
	    Yii::app()->cache->delete(StoreUrlRule::CACHE_KEY);
	    \TaggedCache\TaggingCacheHelper::deleteTag(StoreCategoryHelper::CACHE_CATEGORY_TREE);
	    \TaggedCache\TaggingCacheHelper::deleteTag(StoreCategoryHelper::CACHE_CATEGORY_LIST);
	    \TaggedCache\TaggingCacheHelper::deleteTag(StoreCategoryHelper::CACHE_CATEGORY_TAG);
	    \TaggedCache\TaggingCacheHelper::deleteTag('store::category');
    }

	/**
	 *
	 */
    public static function clearFiles(){
    	if(Yii::app()->hasModule('sitemap')){
		    SitemapHelper::remove();
	    }
    }

	/**
	 * @param ProductOpenEvent $event
	 */
	public static function onOpening(ProductOpenEvent $event){
		$product = $event->getProduct();
		Product::model()->updateByPk($product->id, ['count_view' => (int) $product->count_view + 1]);
	}
}