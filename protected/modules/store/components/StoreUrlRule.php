<?php

/**
 * Class StoreUrlRule
 */
class StoreUrlRule extends CBaseUrlRule
{
    const CACHE_KEY = 'store::category::slugs';
    const CACHE_COUNTRY_KEY = 'store::country::slugs';
    const CACHE_CATEGORY_GROUPS_KEY = 'store::category::groups::slugs';
    const CACHE_GROUPS_KEY = 'store::groups::slugs';
    const CACHE_PRODUCERS_KEY = 'store::producers::slugs';
    const CACHE_LINES_KEY = 'store::producers::lines::slugs';

    /**
     * @param CUrlManager $manager
     * @param string $route
     * @param array $params
     * @param string $amp
     * @return bool
     */
    public function createUrl($manager, $route, $params, $amp)
    {
        $suffix = Yii::app()->getUrlManager()->urlSuffix;

		/**
		 * @var StoreModule $storeModule
		 */
		$storeModule = Yii::app()->getModule('store');

		//продукт
		if ($route === 'store/product/view' && isset($params['name'])) {
			$url = 'home/'.$params['name'];
			if($url!==''){
				$url.=$suffix;
			}
			return $url;
		}

        if ($route === 'store/product/view' && isset($params['category']) && isset($params['name'])) {
            $url = $params['category'].'/'.$params['name'];
            if($url!==''){
                $url.=$suffix;
            }
            return $url;
        }

        //категория
        if ($route === 'store/category/view' && isset($params['path'])) {
            $url = $params['path'];
            if($url!==''){
                $url.=$suffix;
            }
            unset($params['path']);
            if($params){
                $url.='?'.$manager->createPathInfo($params,'=',$amp);
            }

            return $url;
        }

		//новинки
		if ($route === 'store/product/news' && $storeModule->getProductNewUrl()) {
			return $storeModule->getProductNewUrl();
		}

		//спецпредложения
		if ($route === 'store/product/special' && $storeModule->getProductSpecialUrl()) {
			return $storeModule->getProductSpecialUrl();
		}

		//бренды
		if ($route === 'store/producer/index' && $storeModule->getProducerUrl()) {
			return $storeModule->getProducerUrl();
		}

		//страница бренда
		if ($route === 'store/producer/view' && $storeModule->getProducerUrl() && isset($params['slug'])) {
			$url = $storeModule->getProducerUrl().'/'.$params['slug'];
			if($url!==''){
				$url.=$suffix;
			}
			unset($params['slug']);
			if($params){
				$url.='?'.$manager->createPathInfo($params,'=',$amp);
			}

			return $url;
		}

		//линии брендов
		if ($route === 'store/producer/line' && $storeModule->getProducerUrl() && isset($params['slug'])) {
			$url = $storeModule->getProducerUrl().'/'.$params['slug'].'/'.$params['line'];
			if($url!==''){
				$url.=$suffix;
			}
			unset($params['slug'], $params['line']);
			if($params){
				$url.='?'.$manager->createPathInfo($params,'=',$amp);
			}

			return $url;
		}

		//Страны производителей
		if ($route === 'store/producer/countryIndex' && $storeModule->getCountryUrl()) {
			return $storeModule->getCountryUrl();
		}

		//Страна производителей
	    if ($route === 'store/producer/country' && isset($params['slug'])) {
		    $url = $params['slug'];
		    if($url!==''){
			    $url.=$suffix;
		    }
		    unset($params['slug']);
		    if($params){
			    $url.='?'.$manager->createPathInfo($params,'=',$amp);
		    }

		    return $url;
	    }

		//группы товаров - главная
		if ($route === 'store/productGroups/index' && $storeModule->getProductGroupUrl()) {
			return $storeModule->getProductGroupUrl();
		}

		//группы товаров - вложенная
		if ($route === 'store/productGroups/category' && $storeModule->getProductGroupUrl() && isset($params['slug'])) {
			$url = $storeModule->getProductGroupUrl().'/'.$params['slug'];
			if($url!==''){
				$url.=$suffix;
			}
			unset($params['slug']);
			if($params){
				$url.='?'.$manager->createPathInfo($params,'=',$amp);
			}

			return $url;
		}

		//группы товаров - группа
		if ($route === 'store/productGroups/view' && isset($params['slug'])) {
			$url = $params['slug'];
			if($url!==''){
				$url.=$suffix;
			}
			unset($params['slug']);
			if($params){
				$url.='?'.$manager->createPathInfo($params,'=',$amp);
			}

			return $url;
		}
        return false;
    }

    /**
     * @param \webforma\components\WUrlManager $manager
     * @param CHttpRequest $request
     * @param string $pathInfo
     * @param string $rawPathInfo
     * @return bool|string
     */
    public function parseUrl($manager, $request, $pathInfo, $rawPathInfo)
    {
		$parts = explode('/', $pathInfo);

		if($parts[0] == 'home'){
			return '/store/product/view/name/'.$parts[1];
		}
		return false;


	    $slugsCategory = Yii::app()->getCache()->get(self::CACHE_KEY);
		/**
		 * @var StoreModule $storeModule
		 */
	    $storeModule = Yii::app()->getModule('store');

	    //Категории
        if (false === $slugsCategory) {
            /* @var $cmd CDbCommand */
            $cmd = Yii::app()->getDb()->createCommand();

	        $slugsCategory = $cmd
                ->setFetchMode(PDO::FETCH_COLUMN, 0)
                ->from(StoreCategory::model()->tableName())
                ->select('slug')
                ->queryAll();

            Yii::app()->getCache()->set(self::CACHE_KEY, $slugsCategory, 0);
        }

		//категории группы товаров
	    $slugsCategoryGroups = Yii::app()->getCache()->get(self::CACHE_CATEGORY_GROUPS_KEY);
	    if (false === $slugsCategoryGroups) {
		    /* @var $cmd CDbCommand */
		    $cmd = Yii::app()->getDb()->createCommand();

		    $slugsCategoryGroups = $cmd
			    ->setFetchMode(PDO::FETCH_COLUMN, 0)
			    ->from(StoreCategory::model()->tableName())
			    ->select('groups_alias')
			    ->where('groups_alias != "" AND groups_alias IS NOT NULL')
			    ->queryAll();

		    Yii::app()->getCache()->set(self::CACHE_CATEGORY_GROUPS_KEY, $slugsCategoryGroups, 0);
	    }

		//страны
	    $slugsCountry = Yii::app()->getCache()->get(self::CACHE_COUNTRY_KEY);
	    if (false === $slugsCountry) {
		    /* @var $cmd CDbCommand */
		    $cmd = Yii::app()->getDb()->createCommand();

		    $slugsCountry = $cmd
			    ->setFetchMode(PDO::FETCH_COLUMN, 0)
			    ->from(ProducerCountry::model()->tableName())
			    ->select('slug')
			    ->queryAll();

		    Yii::app()->getCache()->set(self::CACHE_COUNTRY_KEY, $slugsCountry, 0);
	    }

	    //группы товаров
        $slugsGroups = Yii::app()->getCache()->get(self::CACHE_GROUPS_KEY);
        if (false === $slugsGroups) {
            /* @var $cmd CDbCommand */
            $cmd = Yii::app()->getDb()->createCommand();

            $slugsGroups = $cmd
                ->setFetchMode(PDO::FETCH_COLUMN, 0)
                ->from(ProductGroups::model()->tableName())
                ->select('slug')
                ->queryAll();

            Yii::app()->getCache()->set(self::CACHE_GROUPS_KEY, $slugsGroups, 0);
        }

        //бренды
		$slugsProducers = Yii::app()->getCache()->get(self::CACHE_PRODUCERS_KEY);
		if (false === $slugsProducers) {
			/* @var $cmd CDbCommand */
			$cmd = Yii::app()->getDb()->createCommand();

			$slugsProducers = $cmd
				->setFetchMode(PDO::FETCH_COLUMN, 0)
				->from(Producer::model()->tableName())
				->select('slug')
				->queryAll();

			Yii::app()->getCache()->set(self::CACHE_PRODUCERS_KEY, $slugsProducers, 0);
		}

		//бренды
		$slugsLines = Yii::app()->getCache()->get(self::CACHE_LINES_KEY);
		if (false === $slugsLines) {
			/* @var $cmd CDbCommand */
			$cmd = Yii::app()->getDb()->createCommand();

			$slugsLines = $cmd
				->setFetchMode(PDO::FETCH_COLUMN, 0)
				->from(ProducerLine::model()->tableName())
				->select('slug')
				->queryAll();

			Yii::app()->getCache()->set(self::CACHE_LINES_KEY, $slugsLines, 0);
		}

        $parts = explode('/', $pathInfo);
		$partsSize = sizeof($parts);

        if (!empty($parts)) {
            $slug = $parts[0];

			//группа товара
			if ($partsSize > 1 && in_array($parts[$partsSize - 2], $slugsCategory, true) && in_array($parts[$partsSize - 1], $slugsGroups, true)) {
				return '/store/productGroups/view/slug/'.$parts[$partsSize - 1].'/categorySlug/'.$parts[$partsSize - 2].'/';
			}

			//рубрика, товар
			if (in_array($slug, $slugsCategory, true)) {
				return '/store/category/view/path/'.$pathInfo;
			}

			//новинки
			if ($storeModule->getProductNewUrl() && $slug == $storeModule->getProductNewUrl() && count($parts) == 1) {
				return '/store/product/news/';
			}

			//спецпредложение
			if ($storeModule->getProductSpecialUrl() && $slug == $storeModule->getProductSpecialUrl() && count($parts) == 1) {
				return '/store/product/special/';
			}

	        //группы товаров
	        if ($storeModule->getProductGroupUrl() && $slug == $storeModule->getProductGroupUrl() && count($parts) == 1) {
				return '/store/productGroups/index/';
	        }

			//группы товаров - категория
			if (isset($parts[1]) && in_array($parts[1], $slugsCategoryGroups, true) && $storeModule->getProductGroupUrl() && $slug == $storeModule->getProductGroupUrl()) {
				return '/store/productGroups/category/slug/'.$parts[1];
			}

            //группа товара
            if ($slug == $storeModule->productGroupUrl && in_array($parts[1], $slugsCategoryGroups, true)) {
                return '/store/productGroups/view/slug/'.$slug;
            }

			//страница брендов
			if ($storeModule->getProducerUrl() && $slug == $storeModule->getProducerUrl() && count($parts) == 1) {
				return '/store/producer/index/';
			}

			//страница бренда
			if ($parts[1] && in_array($parts[1], $slugsProducers, true) && $storeModule->getProducerUrl() && $slug == $storeModule->getProducerUrl() && count($parts) == 2) {
				return '/store/producer/view/slug/'.$parts[1];
			}

			//линия бренда
			if ($parts[1] && in_array($parts[1], $slugsProducers, true) && $parts[2] && in_array($parts[2], $slugsLines, true) && $storeModule->getProducerUrl() && $slug == $storeModule->getProducerUrl() && count($parts) == 3) {
				return '/store/producer/line/slug/'.$parts[1].'/line/'.$parts[2];
			}

			//страница стран
			if ($storeModule->getCountryUrl() && $slug == $storeModule->getCountryUrl() && count($parts) == 1) {
				return '/store/producer/countryIndex/';
			}

            //страница страны
	        if (in_array($slug, $slugsCountry, true)) {
		        return '/store/producer/country/slug/'.$slug;
	        }

			//группа товара без категории
			if ($partsSize == 1 && in_array($slug, $slugsGroups, true)) {
				return '/store/productGroups/view/slug/'.$slug.'/';
			}
        }

        return false;
    }
}