<?php

/**
 * Class AttributeRender
 */
class AttributeRender
{
    /**
     * @param $attribute
     * @param null $value
     * @param null $name
     * @param array $htmlOptions
     * @return mixed|null|string
     */
    public static function renderField($attribute, $value = null, $name = null, $htmlOptions = [])
    {
        $name = $name ?: 'Attribute['.$attribute->id.']';
        switch ($attribute->type) {
            case Attribute::TYPE_SHORT_TEXT:
                return CHtml::textField($name, $value, $htmlOptions);
                break;
            case Attribute::TYPE_TEXT:
                return Yii::app()->getController()->widget(
                    Yii::app()->getModule('store')->getVisualEditor(),
                    [
                        'name' => $name,
                        'value' => $value,
                    ],
                    true
                );
                break;
            case Attribute::TYPE_DROPDOWN:
            case Attribute::TYPE_RADIO:
            case Attribute::TYPE_COLOR:
                $data = CHtml::listData($attribute->getOptionsList(), 'id', 'value');
				return CHtml::dropDownList($name, $value, $data, array_merge($htmlOptions, (['empty' => '---'])));
                break;
            case Attribute::TYPE_CHECKBOX_LIST:

                //$data = CHtml::listData($attribute->options, 'id', 'value');
                //foreach ($data as $key())
                foreach ($attribute->getOptionsList() as $item){
                    $data[$item->group_name][$item->id] = $item->value;
                }
                if(sizeof($data) <= 1){
                    $key = key($data);
                    return CHtml::checkBoxList($name.'[]', $value, $data[$key], $htmlOptions);
                }

                ksort($data);
                $html = '';
                $html .= '</div></div>';

                foreach ($data as $group => $items){
                    $html .= '<div class="row"><div class="col-sm-12"><label class="attribute-group-name">'.$attribute->title.' - '.($group ? mb_strtolower($group) : 'Общая').'</label></div></div>';

	                asort($items);
	                $group = str_replace(' ', '_', $group);
	                $htmlOptions['baseID'] = 'Attribute_'.$attribute->id.'_'.\dosamigos\yii\helpers\TransliteratorHelper::process($group);
	                $list = CHtml::checkBoxList($name.'[]', $value, $items, $htmlOptions);

	                if($list){
		                $html .= '<div class="row"><div class="col-sm-12 attributes-items checkbox-list">';
		                $html .= $list;
		                $html .= '</div></div>';
	                }
                }
                $html .= '<div class="row"><div class="col-sm-12 attributes-items checkbox-list">';

                return $html;
                //return CHtml::checkBoxList($name.'[]', $value, $data, $htmlOptions);
                break;
            case Attribute::TYPE_CHECKBOX:
                return CHtml::checkBox($name, $value, CMap::mergeArray(['uncheckValue' => 0], $htmlOptions));
                break;
            case Attribute::TYPE_NUMBER:
                return CHtml::numberField($name, $value, $htmlOptions);
                break;
            case Attribute::TYPE_FILE:
                return CHtml::fileField($name.'[name]', null, $htmlOptions);
                break;
        }

        return null;
    }

    /**
     * @param Attribute $attribute
     * @param string $value
     * @param bool $inline
     * @param string $template
     * @return string
     */
    public static function renderValue(Attribute $attribute, $value, $inline = false, $template = '<p>{item}</p>')
    {
        $unit = $attribute->unit ? ' '.$attribute->unit : '';
        $res = null;
        switch ($attribute->type) {
            case Attribute::TYPE_TEXT:
            case Attribute::TYPE_SHORT_TEXT:
            case Attribute::TYPE_NUMBER:
            	if(!$value){
            		return false;
				}
                $res = $value;
                break;
            case Attribute::TYPE_RADIO:
            case Attribute::TYPE_DROPDOWN:
                $data = CHtml::listData($attribute->getOptionsList(), 'id', 'value');
                if(is_array($value)) {
                	foreach ($value as $_item){
						if (isset($data[$_item])) {
							$res .= $data[$_item];
						}
					}
                } else {
					if (isset($data[$value])) {
						$res .= $data[$value];
					}
				}
                break;
            case Attribute::TYPE_CHECKBOX_LIST:
                $data = CHtml::listData($attribute->getOptionsList(), 'id', 'value');
                if(is_array($value)) {
                	if($inline){
                		$temp = [];
		                foreach (array_intersect(array_keys($data), $value) as $val) {
			                $temp[] = strtr($template, ['{item}' => $data[$val]]);
		                }
						if($inline === 2) {
							return $temp;
						}
		                $res .= implode(', ', $temp);
	                } else {
		                foreach (array_intersect(array_keys($data), $value) as $val) {
			                $res .= strtr($template, ['{item}' => $data[$val]]);
		                }
	                }
                }
                break;
            case Attribute::TYPE_CHECKBOX:
                $res = $value ? Yii::t("StoreModule.store", "Yes") : Yii::t("StoreModule.store", "No");
                break;
	        case Attribute::TYPE_COLOR:
				$colorListClass = [
					'#fff' => 'white',
					'#ffffff' => 'white',
					'#white' => 'white',
				];
		        $data = CHtml::listData($attribute->getOptionsList(), 'id', 'value');
		        if(is_array($value)) {
			        $value = array_shift($value);
		        }

		        if (isset($data[$value])) {
		        	list($_title, $_color) = explode('|', $data[$value]);

		        	if(!$_title || !$_color){
		        		continue;
					}
		        	$advClass = isset($colorListClass[$_color]) ? ' variant-color-'.$colorListClass[$_color] : '';
			        $res .= '<span class="variant-color-item'.$advClass.'" style="background-color: '.$_color.'" title="'.CHtml::encode($_title).'">'.$_title.'</span>';
		        }
		        break;
        }

        return $res.$unit;
    }

	/**
	 * @param string $str
	 * @return string mixed
	 */
    public static function formatSting($str){
    	return Helper::mb_ucfirst(mb_strtolower($str));
	}
}