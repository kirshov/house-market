<?php

/**
 * Class AttributesRepository
 */
class AttributesRepository extends CApplicationComponent
{
    /**
     * @param StoreCategory $category
     * @param bool $allAttributes
     * @return Attribute[]
     */
    public function getForCategory(StoreCategory $category, $allAttributes = false)
    {
        $categories = $category->getChildsArray();
        $categories[] = $category->id;
        $activeCategories = Yii::app()->getComponent('categoryRepository')->getActiveCategories($category->id);
        $categories = CMap::mergeArray($categories, $activeCategories);

        $categories = array_unique($categories);

        $criteria = new CDbCriteria([
            'condition' => 't.type != :type',
            'params' => [
                ':type' => Attribute::TYPE_TEXT,
            ],
            'join' => 'LEFT JOIN {{store_type_attribute}} ON t.id = {{store_type_attribute}}.attribute_id
               LEFT JOIN {{store_type}} ON {{store_type_attribute}}.type_id = {{store_type}}.id',
            'distinct' => true,
            'with' => ['options'],
            'order' => 't.sort',
        ]);
        if(!$allAttributes){
        	$criteria->addCondition('t.is_filter = 1');
		}

        $categoriesIds = implode(', ', $categories);
        $criteria->addCondition('options.id IN (
            SELECT DISTINCT av.option_value
            FROM {{store_product_attribute_value}} av, {{store_product}} p
            WHERE av.product_id = `p`.id 
            	AND p.status = '.Product::STATUS_ACTIVE.' AND NOT ISNULL(av.option_value)
            	AND (
            		`p`.category_id IN ('.$categoriesIds.')
            		OR
            		`p`.id IN (SELECT product_id FROM {{store_product_category}} WHERE category_id IN ('.$categoriesIds.'))
        		)
        	)');

	    return Attribute::model()->cache(Yii::app()->getModule('webforma')->coreCacheTime, \TaggedCache\TaggingCacheHelper::getDependency(['store', 'store::attribute', 'store::filter::attributes']))->findAll($criteria);
    }

	/**
	 * @param integer $producerIds
	 * @param bool $allAttributes
	 * @return Attribute[]
	 */
	public function getForProducer($producerIds, $allAttributes = false)
	{
		$categories = Yii::app()->getComponent('categoryRepository')->getActiveCategories();

		$criteria = new CDbCriteria([
			'condition' => 't.type != :type',
			'params' => [
				':type' => Attribute::TYPE_TEXT,
			],
			'join' => 'LEFT JOIN {{store_type_attribute}} ON t.id = {{store_type_attribute}}.attribute_id
                   LEFT JOIN {{store_type}} ON {{store_type_attribute}}.type_id = {{store_type}}.id',
			'distinct' => true,
			'with' => ['options'],
			'order' => 't.sort',
		]);
		if(!$allAttributes){
			$criteria->addCondition('t.is_filter = 1');
		}
		$criteria->addCondition('options.id IN (
			SELECT DISTINCT av.option_value
			FROM {{store_product_attribute_value}} av, {{store_product}} p
			WHERE av.product_id = `p`.id AND `p`.`producer_id` IN ('.implode(', ', $producerIds).') AND `p`.category_id IN ('.implode(', ', $categories).') AND p.status = '.Product::STATUS_ACTIVE.' AND NOT ISNULL(av.option_value)
		)');

		return Attribute::model()->cache(Yii::app()->getModule('webforma')->coreCacheTime, \TaggedCache\TaggingCacheHelper::getDependency(['store', 'store::attribute']))->findAll($criteria);
	}

	/**
	 * @param integer $lineIds
	 * @param bool $allAttributes
	 * @return Attribute[]
	 */
	public function getForLine($lineIds, $allAttributes = false)
	{
		$categories = Yii::app()->getComponent('categoryRepository')->getActiveCategories();

		$criteria = new CDbCriteria([
			'condition' => 't.type != :type',
			'params' => [
				':type' => Attribute::TYPE_TEXT,
			],
			'join' => 'LEFT JOIN {{store_type_attribute}} ON t.id = {{store_type_attribute}}.attribute_id
                   LEFT JOIN {{store_type}} ON {{store_type_attribute}}.type_id = {{store_type}}.id',
			'distinct' => true,
			'with' => ['options'],
			'order' => 't.sort',
		]);

		if(!$allAttributes){
			$criteria->addCondition('t.is_filter = 1');
		}

		$criteria->addCondition('options.id IN (
			SELECT DISTINCT av.option_value
			FROM {{store_product_attribute_value}} av, {{store_product}} p
			WHERE av.product_id = `p`.id AND `p`.`producer_line_id` IN ('.implode(', ', $lineIds).') AND `p`.category_id IN ('.implode(', ', $categories).') AND p.status = '.Product::STATUS_ACTIVE.' AND NOT ISNULL(av.option_value)
		)');

		return Attribute::model()->cache(Yii::app()->getModule('webforma')->coreCacheTime, \TaggedCache\TaggingCacheHelper::getDependency(['store', 'store::attribute']))->findAll($criteria);
	}

	/**
	 * @return Attribute[]
	 */
	public function getAllAttributes()
	{
		$attributes = Attribute::model()->cache(Yii::app()->getModule('webforma')->coreCacheTime, \TaggedCache\TaggingCacheHelper::getDependency(['store', 'store::attribute']))->findAll();
		return CHtml::listData($attributes, 'name', 'id');
	}

	/**
	 * @return array
	 */
    public function getActualOptionValues(){
    	$cacheName = 'actualAttributes';
	    $attributes = Yii::app()->cache->get($cacheName);
		if($attributes === false) {
			$data = AttributeValue::model()->findAll([
				'distinct' => true,
				'select' => 'option_value',
			]);
			$attributes = CHtml::listData($data, 'option_value', 'option_value');

			Yii::app()->cache->set($cacheName, $attributes, Yii::app()->getModule('webforma')->coreCacheTime, \TaggedCache\TaggingCacheHelper::getDependency(['store', 'store::attribute']));
		}
		return $attributes;
    }

    /**
     * @param integer $id
     * @return string|array
     */
    public function getOptions($id = null){
        $cacheName = 'attributesOptions';
        $options = Yii::app()->cache->get($cacheName);
        if($options === false) {
            $data = AttributeOption::model()->findAll([
                'select' => 'id, value'
            ]);
            $options = CHtml::listData($data, 'id', 'value');

            Yii::app()->cache->set($cacheName, $options, 0, \TaggedCache\TaggingCacheHelper::getDependency(['store', 'store::attribute']));
        }
        if($id !== null){
            return $options[$id];
        }
        return $options;
    }
}