<?php

/**
 * Class ProductRepository
 */
class ProductRepository extends CApplicationComponent
{
    /**
     * @var
     */
    protected $attributeFilter;

	/**
	 * @var Product[]
	 */
	protected $products;

	/**
	 * @var bool
	 */
	protected $forceSubCats = false;
    /**
     *
     */
    public function init()
    {
        $this->attributeFilter = Yii::app()->getComponent('attributesFilter');
    }

	/**
	 * @param array $mainSearchAttributes
	 * @param array $typeSearchAttributes
	 * @param bool $forceSubCats
	 * @return CActiveDataProvider
	 */
    public function getByFilter(array $mainSearchAttributes, array $typeSearchAttributes, $forceSubCats = false)
    {
    	$this->forceSubCats = $forceSubCats;

        /** @var StoreModule $module */
        $module = Yii::app()->getModule('store');

	    /**
	     * @var CDbCriteria $criteria
	     */
        $criteria = $this->getCriteriaByFilter($mainSearchAttributes, $typeSearchAttributes);

        $countCriteria = clone $criteria;
        $countCriteria->with = [];

		$dataProvider = new CActiveDataProvider(
            'Product',
            [
                'criteria' => $criteria,
                'countCriteria' => $countCriteria,
                'pagination' => [
                    'pageSize' => (int)$module->getItemsPerPage(),
                    'pageVar' => 'page',
                ],
                'sort' => $this->getSortSettingsForDataProvider(),
            ]
        );

		$this->setProducts($dataProvider);

		return $dataProvider;
    }

    /**
     * @param array $mainSearchAttributes
     * @param array $typeSearchAttributes
     * @return Producer[]
     */
    public function getProducersForProducts(array $mainSearchAttributes, array $typeSearchAttributes)
    {
        $criteria = $this->getCriteriaByFilter($mainSearchAttributes, $typeSearchAttributes);
        $criteria->with = ['producer'];
        $criteria->select = Producer::model()->tableName().'.id';
        $criteria->distinct = true;

        $products = Product::model()->published()->findAll($criteria);

	    $producers = [];
        foreach ($products as $product){
	        if(isset($product->producer) && $product->producer){
		        $producers[$product->producer->id] = $product->producer;
	        }
        }

        return $producers;
    }

    /**
     * @param array $mainSearchAttributes
     * @param array $typeSearchAttributes
     * @return CDbCriteria
     */
    protected function getCriteriaByFilter(array $mainSearchAttributes, array $typeSearchAttributes){
        $criteria = new CDbCriteria([
            'select' => 't.*',
            'distinct' => true,
            'params' => [],
            'scopes' => ['published']
        ]);

		$criteria->with = ['type', 'attributesValues', 'type.typeAttributes', 'producer', 'producerLine'];

		$ids = Helper::arrayPopItem('ids', $typeSearchAttributes);
		if($ids){
			$idsArray = explode(',', $ids);
			if($idsArray){
				$criteria->addInCondition('t.id', $idsArray);
				return $criteria;
			}
		}

        //поиск по категории, производителю и цене
        foreach ($this->attributeFilter->getMainSearchParams() as $param => $field) {
            if (empty($mainSearchAttributes[$param])) {
                continue;
            }

			if($field === 'floor_id' && $mainSearchAttributes[$param]){
				$criteria->addCondition('t.floor_id = :floor_id');
				$criteria->params[':floor_id'] = (int) $mainSearchAttributes[$param];

				continue;
			}

			if($field === 'done_id' && $mainSearchAttributes[$param]){
				$criteria->addCondition('t.done_id = :done_id');
				$criteria->params[':done_id'] = (int) $mainSearchAttributes[$param];

				continue;
			}

			if($field === 'place_id' && $mainSearchAttributes[$param]){
				$criteria->addInCondition('t.place_id', (array) $mainSearchAttributes[$param]);
				continue;
			}

            if($param === 'available'){
            	$isAvailable = false;
            	$isNotAvailable = false;
            	foreach ($mainSearchAttributes[$param] as $_item){
            		if($_item == 1) {
            			$isAvailable = true;
		            } elseif($_item == 2) {
			            $isNotAvailable = true;
		            }
	            }

				if($isAvailable && $isNotAvailable){
					continue;
				}
				if($isAvailable){
					$criteria->addCondition('t.quantity > 0 OR t.in_stock = 1');
				}
				if($isNotAvailable){
					$criteria->addCondition('t.quantity = 0 OR t.in_stock = 0');
				}

            	continue;
            }

            /*if ('category' === $param && (Yii::app()->getModule('store')->showProductsSubCats || $this->forceSubCats)) {
                $categories = [];

                foreach ($mainSearchAttributes[$param] as $categoryId) {
                    $categories[] = (int)$categoryId;
                    $categories = CMap::mergeArray($categories, StoreCategory::model()->getChildsArray($categoryId));
                }

                $builder = new CDbCommandBuilder(Yii::app()->getDb()->getSchema());

                $criteria->addInCondition('t.category_id', array_unique($categories));
                $criteria->addCondition(sprintf('t.id IN (SELECT product_id FROM {{store_product_category}} WHERE %s)',
                    $builder->createInCondition('{{store_product_category}}', 'category_id', $categories)), 'OR');

                continue;
            }*/


            $_field = $param == 'price' ? 'IF(t.discount_price, t.discount_price, t.price)' : 't.'.$field;
            if (isset($mainSearchAttributes[$param]['from'], $mainSearchAttributes[$param]['to'])) {
                $criteria->addBetweenCondition(
	                $_field,
                    $mainSearchAttributes[$param]['from'],
                    $mainSearchAttributes[$param]['to']
                );
            } elseif (isset($mainSearchAttributes[$param]['from']) && !isset($mainSearchAttributes[$param]['to'])) {
                $criteria->addCondition($_field." >= :attr_{$field}");
                $criteria->params[":attr_{$field}"] = $mainSearchAttributes[$param]['from'];
            } elseif (isset($mainSearchAttributes[$param]['to']) && !isset($mainSearchAttributes[$param]['from'])) {
                $criteria->addCondition($_field." <= :attr_{$field}");
                $criteria->params[":attr_{$field}"] = $mainSearchAttributes[$param]['to'];
            } else {
                $criteria->addInCondition($_field, $mainSearchAttributes[$param]);
            }
        }

        //поиск по названию и артикулу
        if (!empty($mainSearchAttributes[AttributeFilter::MAIN_SEARCH_PARAM_NAME])) {

            $term = trim($mainSearchAttributes[AttributeFilter::MAIN_SEARCH_PARAM_NAME]);

            $word = preg_replace('/\s{2,}/iu', ' ', $term);

            /*Yii::import('webforma.extensions.phpMorphy.FMorphy');
            $a = new FMorphy([$word]);*/
            $criteria->join = 'LEFT JOIN {{store_product_variant}} variants ON variants.product_id = t.id';

            $word = trim($word);
            $criteria->addSearchCondition('t.name', $word, true);
            $criteria->addSearchCondition('t.original_name', $word, true, 'OR');
            //$criteria->addSearchCondition('t.description', $word, true, 'OR');
            $criteria->addSearchCondition('t.sku', $word, true, 'OR');
            $criteria->addSearchCondition('variants.sku', $word, true, 'OR');
            $criteria->addSearchCondition('t.search_name', $word, true, 'OR');

            $word = Helper::switcher($word);
            $criteria->addSearchCondition('t.name', $word, true, 'OR');
            $criteria->addSearchCondition('t.original_name', $word, true, 'OR');
            //$criteria->addSearchCondition('t.description', $word, true, 'OR');
            $criteria->addSearchCondition('t.sku', $word, true, 'OR');
            $criteria->addSearchCondition('variants.sku', $word, true, 'OR');
            $criteria->addSearchCondition('t.search_name', $word, true, 'OR');
        }

        //поиск по названию
        if (!empty($mainSearchAttributes[AttributeFilter::MAIN_SEARCH_PARAM_NAME_ONLY])) {

            $term = trim($mainSearchAttributes[AttributeFilter::MAIN_SEARCH_PARAM_NAME_ONLY]);

            $criteria->addSearchCondition('t.name', $term, true);
        }
        
		$criteria->mergeWith($this->buildCriteriaForTypeAttributes($typeSearchAttributes));
        return $criteria;
    }

    /**
	 * @param array $typeSearchAttributes
	 * @return CDbCriteria
	 */
	public function buildCriteriaForTypeAttributes(array $typeSearchAttributes)
	{
		$criteria = new CDbCriteria();
		$criteria->params = [];

		$i = 0;

		foreach ($typeSearchAttributes as $attribute => $params) {
			if (empty($params['value'])) {
				continue;
			}

			$alias = "attributes_values_{$i}";

			$criteria->join .= " JOIN {{store_product_attribute_value}} {$alias} ON t.id = {$alias}.product_id ";

			//@TODO подумать как улучшить
			if (is_array($params['value'])) {
				if (isset($params['value']['from'], $params['value']['to'])) {
					$between = new CDbCriteria();
					$between->addBetweenCondition(
						"{$alias}.".$params['column'],
						$params['value']['from'],
						$params['value']['to']
					);
					$between->addCondition("{$alias}.attribute_id = :attributeId_{$i}");
					$between->params[":attributeId_{$i}"] = (int)$params['attribute_id'];
					$criteria->mergeWith($between);
				} elseif (isset($params['value']['from']) && !isset($params['value']['to'])) {
					$between = new CDbCriteria();
					$between->addCondition("{$alias}.attribute_id = :attributeId_{$i}");
					$between->addCondition("{$alias}.{$params['column']} >= :attr_{$i}");
					$between->params[":attributeId_{$i}"] = (int)$params['attribute_id'];
					$between->params[":attr_{$i}"] = $params['value']['from'];
					$criteria->mergeWith($between);
				} elseif (isset($params['value']['to']) && !isset($params['value']['from'])) {
					$between = new CDbCriteria();
					$between->addCondition("{$alias}.attribute_id = :attributeId_{$i}");
					$between->addCondition("{$alias}.{$params['column']} <= :attr_{$i}");
					$between->params[":attributeId_{$i}"] = (int)$params['attribute_id'];
					$between->params[":attr_{$i}"] = $params['value']['to'];
					$criteria->mergeWith($between);
				} else {
					$in = new CDbCriteria();
					$in->addInCondition("{$alias}.".$params['column'], $params['value']);
					$criteria->mergeWith($in);
				}
			} else {
				$condition = new CDbCriteria();
				$condition->addCondition("{$alias}.attribute_id = :attributeId_{$i}");
				$condition->params[":attributeId_{$i}"] = (int)$params['attribute_id'];
				$condition->addColumnCondition(["{$alias}.".$params['column'] => $params['value']]);
				$criteria->mergeWith($condition);
			}

			$i++;
		}

		return $criteria;
	}

    /**
     * @param $term
     * @return string
     */
    public function getForAutocomplete($term){
	    $data = [];

    	$criteria = new CDbCriteria();
    	$criteria->limit = 5;
    	$criteria->with = ['category'];

	    //поиск по названию и артикулу
	    $term = trim($term);

        $word = $wordOriginal = preg_replace('/\s{2,}/iu', ' ', $term);

        $criteria->join = 'LEFT JOIN {{store_product_variant}} variants ON variants.product_id = t.id';

        $criteria->addSearchCondition('t.name', $word, true);
        $criteria->addSearchCondition('t.original_name', $word, true, 'OR');
        $criteria->addSearchCondition('t.sku', $word, true, 'OR');
        $criteria->addSearchCondition('variants.sku', $word, true, 'OR');
        $criteria->addSearchCondition('t.search_name', $word, true, 'OR');

        $word = Helper::switcher($word);

        $criteria->addSearchCondition('t.name', $word, true, 'OR');
        $criteria->addSearchCondition('t.original_name', $word, true, 'OR');
        $criteria->addSearchCondition('t.sku', $word, true, 'OR');
        $criteria->addSearchCondition('variants.sku', $word, true, 'OR');
        $criteria->addSearchCondition('t.search_name', $word, true, 'OR');

	    $products = Product::model()->published()->findAll($criteria);
        $categories = $this->getCategoryByTerm($wordOriginal, 5);
	    $brands = $this->getProducerByTerm($wordOriginal, 4);
	    $lines = $this->getProducerLineByTerm($wordOriginal, 5);

	    return [
	    	'term' => $term,
	    	'products' => $products,
	    	'categories' => $categories,
	    	'brands' => $brands,
	    	'lines' => $lines,
		];

	    $data[] = [
	    	'title' => 'Поиск "' . $term . '" по всему сайту',
		    'value' => $term,
	        'type' => 'all',
	    ];

	    if($category){
		    $data[] = [
		    	'title' => 'Категории',
		    	'type' => 'title',
		    ];
		    foreach ($category as $item){
			    $data[] = [
				    'title' => CHtml::encode($item->name),
				    //'image' => StoreImage::category($item, 140, 140),
				    //'description' => $item->description,
				    'link' => Yii::app()->createAbsoluteUrl('/store/category/view', ['path' => $item->path]),
				    'type' => 'category',
			    ];
		    }
	    }

	    if($products){
		    $data[] = [
			    'title' => 'Товары',
			    'type' => 'title',
		    ];
		    $currency = Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency);
		    $toCartTitle = Yii::t('StoreModule.store', 'Into cart');
		    $toCartLink = Yii::app()->createUrl('/cart/cart/add', ['no-window' => 1]);

		    foreach ($products as $item){
			    $title = CHtml::encode($item->name);
			    $title = str_replace($term, '<b>'.$term.'</b>', $title);
		    	$html = '<div class="autocomplete-suggestion__product-image">'.CHtml::image(StoreImage::product($item, 70, 70, false)).'</div>';
		    	$html .= '<div class="autocomplete-suggestion__product-title">'.$title.'</div>';
		    	$html .= '<div class="autocomplete-suggestion__product-price">'.$item->getFormatPrice().' '.$currency.'</div>';
		    	$after = '<div class="autocomplete-suggestion__product-buy">
						<a class="quick-add-product-to-cart" data-product-id="'.$item->id.'" data-cart-add-url="'.$toCartLink.'">'.$toCartTitle.'</a>
					</div>';

		    	$data[] = [
		    	    'title' => CHtml::encode($item->name),
				    'html' => $html,
				    'link' => Yii::app()->createAbsoluteUrl(ProductHelper::getUrl($item)),
				    'type' => 'position',
				    'after' => $after,
			    ];
		    }

            $data[] = [
                'title' => 'Поиск всех товаров, содержащих "' . $term . '"',
                'value' => $term,
                'type' => 'all no-border',
            ];
	    }

	    if($lines){
		    $data[] = [
			    'title' => 'Линии брендов',
			    'type' => 'title',
		    ];
		    foreach ($lines as $item){
			    $data[] = [
				    'title' => CHtml::encode($item->name),
				    'link' => Yii::app()->createAbsoluteUrl('/store/producer/line', ['slug' => $item->producer->slug, 'line' => $item->slug]),
				    'type' => 'line',
			    ];
		    }
	    }

	    if($brands){
		    $data[] = [
			    'title' => 'Бренды',
			    'type' => 'title',
		    ];
		    foreach ($brands as $item){
		    	if(!$item->image){
		    		continue;
			    }
		    	$html = '<div class="autocomplete-suggestion__brand-image">'.CHtml::image($item->getImageUrl(170, 100, false)).'</div>';
			    $data[] = [
				    'title' => CHtml::encode($item->name),
				    'html' => $html,
				    'link' => Yii::app()->createAbsoluteUrl('/store/producer/view', ['slug' => $item->slug]),
				    'type' => 'brand',
			    ];
		    }
	    }

	    return json_encode($data);
    }

    /**
     * @param string $word
     * @param bool $limit
     * @return StoreCategory[]
     */
    public function getCategoryByTerm($word, $limit = false){
        $categoryCriteria = new CDbCriteria();
        if($limit){
            $categoryCriteria->limit = $limit;
        }

        $categoryCriteria->addSearchCondition('t.name', $word, true, 'AND');
        $word = Helper::switcher($word);
        $categoryCriteria->addSearchCondition('t.name', $word, true, 'OR');

        return StoreCategory::model()->publishedAll()->findAll($categoryCriteria);
    }

    /**
     * @param string $word
     * @param bool $limit
     * @return Producer[]
     */
    public function getProducerByTerm($word, $limit = false){
        $brandCriteria = new CDbCriteria();
        if($limit){
            $brandCriteria->limit = $limit;
        }

        $brandCriteria->addSearchCondition('t.name', $word, true, 'AND');
        $brandCriteria->addSearchCondition('t.search_name', $word, true, 'OR');
        $word = Helper::switcher($word);
        $brandCriteria->addSearchCondition('t.name', $word, true, 'OR');
        $brandCriteria->addSearchCondition('t.search_name', $word, true, 'OR');

        return Producer::model()->published()->findAll($brandCriteria);
    }

    /**
     * @param string $word
     * @param bool $limit
     * @return Producer[]
     */
    public function getProducerLineByTerm($word, $limit = false){
        $brandCriteria = new CDbCriteria();
        if($limit){
            $brandCriteria->limit = $limit;
        }

        $brandCriteria->addSearchCondition('t.name', $word, true, 'AND');
        $word = Helper::switcher($word);
        $brandCriteria->addSearchCondition('t.name', $word, true, 'OR');

        return ProducerLine::model()->published()->findAll($brandCriteria);
    }

    /**
     * @return CActiveDataProvider
     */
    public function getListForIndexPage()
    {
        /** @var StoreModule $module */
        $module = Yii::app()->getModule('store');

        $criteria = new CDbCriteria();
        $criteria->select = 't.*';
        $criteria->scopes = ['published'];

        $dataProvider = new CActiveDataProvider(
            'Product',
            [
                'criteria' => $criteria,
                'pagination' => [
                    'pageSize' => (int)$module->getItemsPerPage(),
                    'pageVar' => 'page',
                ],
                'sort' => $this->getSortSettingsForDataProvider(),
            ]
        );

        $this->setProducts($dataProvider);
        return $dataProvider;
    }

    /**
     * @param StoreCategory $category
     * @param bool $withChild
     * @param null $limit
     * @return CActiveDataProvider
     */
    public function getListForCategory(StoreCategory $category, $withChild = true, $limit = null)
    {
        $categories = [];
        /** @var StoreModule $module */
        $module = Yii::app()->getModule('store');

        if (true === $withChild) {
            $categories = $category->getChildsArray();
        }

        $categories[] = $category->id;

        $criteria = new CDbCriteria([
            'scopes' => ['published'],
	        'with' => ['variants', 'category', 'type', 'attributesValues', 'type.typeAttributes', 'producerLine'],
        ]);

        $criteria->addInCondition('t.category_id', $categories);

        $pagination = [
            'pageSize' => (int)$module->getItemsPerPage(),
            'pageVar' => 'page',
        ];

        if ($limit) {
            $pagination = false;
            $criteria->limit = (int)$limit;
        }

        $countCriteria = clone $criteria;
        $countCriteria->with = [];

        $dataProvider = new CActiveDataProvider(Product::model(), [
            'criteria' => $criteria,
            'countCriteria' => $countCriteria,
            'pagination' => $pagination,
            'sort' => $this->getSortSettingsForDataProvider()
        ]);

        $this->setProducts($dataProvider);

        return $dataProvider;
    }

	/**
	 * @param string $type
	 * @param mixed $limit
	 * @return CActiveDataProvider
	 */
	public function getListByType($type, $limit = false)
	{
		/** @var StoreModule $module */
		$module = Yii::app()->getModule('store');

		$criteria = new CDbCriteria([
			'scopes' => ['published'],
			'with' => ['variants', 'category', 'type', 'attributesValues', 'type.typeAttributes', 'producerLine'],
		]);

		if($type == 'special'){
			if($module->productSpecialType == StoreModule::PRODUCTS_SPECIAL_TYPE_DISCOUNT){
				$criteria->scopes[] = 'specialOfferDiscount';
			} else {
				$criteria->scopes[] = 'specialOffer';
			}
		}

		if($type == 'new'){
			if($module->productNewType == StoreModule::PRODUCTS_NEW_TYPE_LAST){
				$criteria->scopes[] = 'newOfferLast';
				if(!$limit){
					$limit = $module->getItemsPerPage();
				}
			} else {
				$criteria->scopes[] = 'newOffer';
			}
		}

		$pagination = [
			'pageSize' => (int)$module->getItemsPerPage(),
			'pageVar' => 'page',
		];

		if ($limit) {
			$pagination = false;
			$criteria->limit = (int)$limit;
		}

		$dataProvider = new CActiveDataProvider(Product::model(), [
			'criteria' => $criteria,
			'pagination' => $pagination,
		]);

		$this->setProducts($dataProvider);

		return $dataProvider;
	}

    /**
     * @param $query
     * @return array
     */
    public function search($query)
    {
        $criteria = new CDbCriteria();
        $criteria->params = [];
        $criteria->with = ['variants', 'category', 'type', 'attributesValues', 'type.typeAttributes'];
        $criteria->addSearchCondition('name', $query, true);
		$products = Product::model()->published()->findAll($criteria);

		$this->setProducts($products);

        return $products;
    }

    /**
     * @param $slug
     * @param array $with
     * @return mixed
     */
    public function getBySlug(
        $slug,
        array $with = ['producer', 'images', 'category', 'variants',  'producerLine', /*'type.typeAttributes', 'attributesValues'*/]
    ) {
        $product = Product::model()->published()->find([
        	'condition' => 't.slug = :slug',
	        'params' => [':slug' => $slug],
	        'with' => $with,
        ]);

        $this->setProducts($product);

        return $product;
    }

    /**
     * @param $id
     * @param array $with
     * @return mixed
     */
    public function getById($id, array $with = ['producer', /*'type.typeAttributes',*/ 'images', 'category', 'variants', 'producerLine'])
    {
	    $product = Product::model()->published()->with($with)->findByPk($id);

	    $this->setProducts($product);

	    return $product;
    }

    /**
     * backend only
     * @param $name
     * @return array|mixed|null
     */
    public function searchByName($name)
    {
        $criteria = new CDbCriteria();
        $criteria->addSearchCondition('t.name', $name);
        $criteria->addSearchCondition('t.sku', $name, true, 'OR');
        $criteria->addSearchCondition('t.search_name', $name, true, 'OR');
        $criteria->addSearchCondition('t.original_name', $name, true, 'OR');
        $provider = new CActiveDataProvider(
            Product::model()->published(), [
                'criteria' => $criteria,
            ]
        );
	    $this->setProducts($provider);

        return new CDataProviderIterator($provider);
    }

    /**
     * Get products by brand
     *
     * @param Producer $producer
     * @param ProducerLine $line
     * @param array $mainSearchAttributes
     * @param array $typeSearchAttributes
     * @return CActiveDataProvider
     */
    public function getByBrand(Producer $producer, $line = null, $mainSearchAttributes = [], $typeSearchAttributes = [])
    {
        /** @var StoreModule $module */
        $module = Yii::app()->getModule('store');
        if($mainSearchAttributes || $typeSearchAttributes){
	        $criteria = $this->getCriteriaByFilter($mainSearchAttributes, $typeSearchAttributes);
        } else {
	        $criteria = new CDbCriteria();
        }

        $criteria->addCondition('t.producer_id = :producer_id');
        $criteria->scopes = ['published'];
        $criteria->params[':producer_id'] = $producer->id;

	    $sort = null;
        if($line){
            $criteria->addCondition('producer_line_id = :producer_line_id');
            $criteria->params[':producer_line_id'] = $line->id;
            $sort = 't.position';
        }

        $criteria->with = ['variants', 'category', 'type', 'attributesValues', 'type.typeAttributes', 'producerLine'];

        $dataProvider = new CActiveDataProvider(
            Product::model(), [
                'criteria' => $criteria,
                'pagination' => [
                    'pageSize' => (int)$module->getItemsPerPage(),
                    'pageVar' => 'page',
                ],
                'sort' => $this->getSortSettingsForDataProvider($sort),
            ]
        );

        $this->setProducts($dataProvider);

        return $dataProvider;
    }

    /**
     * Get products by brand
     *
     * @param ProducerCountry $country
     * @param [] $mainSearchAttributes
     * @param [] $typeSearchAttributes
     * @return CActiveDataProvider
     */
    public function getByCountry(ProducerCountry $country, $mainSearchAttributes = [], $typeSearchAttributes = [])
    {
        /** @var StoreModule $module */
        $module = Yii::app()->getModule('store');
        $producers = [0];

        if($country->producersActive){
            foreach ($country->producersActive as $producer){
                $producers[] = $producer->id;
            }

	        if($mainSearchAttributes || $typeSearchAttributes){
		        $criteria = $this->getCriteriaByFilter($mainSearchAttributes, $typeSearchAttributes);
	        } else {
		        $criteria = new CDbCriteria();
	        }
            $criteria->addInCondition('t.producer_id', $producers);
            $criteria->scopes = ['published'];

            $criteria->with = ['variants', 'category', 'type', 'attributesValues', 'type.typeAttributes', 'producerLine'];

            $countCriteria = clone $criteria;
            $countCriteria->with = [];

            $dataProvider = new CActiveDataProvider(
                Product::model(), [
                    'criteria' => $criteria,
                    'countCriteria' => $countCriteria,
                    'pagination' => [
                        'pageSize' => (int)$module->getItemsPerPage(),
                        'pageVar' => 'page',
                    ],
                    'sort' => $this->getSortSettingsForDataProvider(),
                ]
            );

            $this->setProducts($dataProvider);

            return $dataProvider;
        } else {
            return new CArrayDataProvider([]);
        }
    }

    /**
     * @param array $ids
     * @param int|null $limit
     * @return CActiveDataProvider
     */
    public function getByIds(array $ids, $limit = null)
    {
    	if(empty($ids)){
    		$ids = [0];
	    }
        /** @var StoreModule $module */
        $module = Yii::app()->getModule('store');

        $criteria = new CDbCriteria();
        $criteria->scopes = ['published'];
        $criteria->addInCondition('t.id', $ids);

        $criteria->with = ['variants', 'category', 'type', 'attributesValues', 'type.typeAttributes', 'producerLine'];

		$pagination = [];

        if($limit === null){
        	$pagination = [
				'pageSize' => (int) $module->getItemsPerPage(),
				'pageVar' => 'page',
			];
		} elseif(is_numeric($limit)){
			$pagination = [
				'pageSize' => (int) $limit,
				'pageVar' => 'page',
			];
		}

	    $dataProvider = new CActiveDataProvider(
            Product::model(), [
                'criteria' => $criteria,
                'pagination' => $pagination,
                'sort' => $this->getSortSettingsForDataProvider(),
            ]
        );

	    $this->setProducts($dataProvider);

	    return $dataProvider;
    }

    /**
     * @param Product $product
     * @param null $typeCode
     * @return CActiveDataProvider
     */
    public function getLinkedProductsDataProvider(Product $product, $typeCode = null)
    {
        $criteria = new CDbCriteria();
        $criteria->scopes = ['published'];
        $criteria->order = 'linked.position DESC';
        $criteria->join  = ' JOIN {{store_product_link}} linked ON t.id = linked.linked_product_id';
        $criteria->compare('linked.product_id', $product->id);
        if (null !== $typeCode) {
            $criteria->join .= ' JOIN {{store_product_link_type}} type ON type.id = linked.type_id';
            $criteria->compare('type.code', $typeCode);
        }

        return new CActiveDataProvider(
            'Product', [
                'criteria' => $criteria,
            ]
        );
    }

    /**
     * @param ProductBatchForm $form
     * @param array $ids
     * @return int
     */
    public function batchUpdate(ProductBatchForm $form, array $ids)
    {
        $attributes = $form->loadQueryAttributes();

	    if (null !== $form->price) {
		    $attributes['price'] = $this->getPriceQuery(
			    'price',
			    $form->price,
			    (int)$form->price_op,
			    (int)$form->price_op_unit
		    );

			if(Yii::app()->getModule('store')->priceType == StoreModule::PRICE_TYPE_CHARGE) {
				Product::model()->updateAll($attributes, [
					'condition' => 'purchase_price = 0 OR purchase_price IS NULL',
				]);
			}
		    unset($attributes['price']);
	    }

		if(Yii::app()->getModule('store')->priceType == StoreModule::PRICE_TYPE_CHARGE){
			if(is_numeric($form->purchase_price) || is_numeric($form->extra_charge)){
				$criteria = new CDbCriteria();
				$criteria->addInCondition('id', $ids);
				$products = Product::model()->findAll($criteria);

				foreach ($products as $product){
					if(is_numeric($form->purchase_price)){
						$product->purchase_price = $this->getPriceData($product->purchase_price, $form->purchase_price, (int)$form->purchase_price_op, (int)$form->purchase_price_op_unit);
					}

					if($form->extra_charge == -1){
						$product->extra_charge = new CDbExpression('NULL');
					} elseif(is_numeric($form->extra_charge)){
						$product->extra_charge = $this->getPriceData($product->extra_charge, $form->extra_charge, (int)$form->extra_charge_op, 1);
					}

					if($product->variants){
						foreach ($product->variants as $variant){
							if(is_numeric($form->purchase_price)){
								$variant->purchase_price = $this->getPriceData($variant->purchase_price, $form->purchase_price, (int)$form->purchase_price_op, (int)$form->purchase_price_op_unit);
							}

							if($form->extra_charge == -1){
								$variant->extra_charge = new CDbExpression('NULL');
							} elseif(is_numeric($form->extra_charge)){
								$variant->extra_charge = $this->getPriceData($variant->extra_charge, $form->extra_charge, (int)$form->extra_charge_op, 1);
							}
						}
					}

					$product->save();
				}
			}
		}

        if (count($attributes) === 0) {
            return true;
        }

        $criteria = new CDbCriteria();
        $criteria->addInCondition('id', $ids);

        return Product::model()->updateAll($attributes, $criteria);
    }

    /**
     * @param $price
     * @param $newPrice
     * @param $operation
     * @param $unit
     * @return float
     */
    private function getPriceData($price, $newPrice, $operation, $unit)
    {
        if (ProductBatchHelper::PRICE_EQUAL === $operation) {
            return $newPrice;
        }

        if(ProductBatchHelper::PRICE_ADD === $operation){
            if(ProductBatchHelper::OP_PERCENT === $unit){
                return $price + ($price * $newPrice / 100);
            } else {
                return $price + $newPrice;
            }
        } else {
            if(ProductBatchHelper::OP_PERCENT === $unit){
                return $price - ($price * $newPrice / 100);
            } else {
                return $price - $newPrice;
            }
        }
    }

    /**
     * @param $field
     * @param $price
     * @param $operation
     * @param $unit
     * @return float|CDbExpression
     */
    private function getPriceQuery($field, $price, $operation, $unit)
    {
        if (ProductBatchHelper::PRICE_EQUAL === $operation) {
            return $price;
        }

        $sign = ProductBatchHelper::PRICE_ADD === $operation ? '+' : '-';

        if (ProductBatchHelper::OP_PERCENT === $unit) {
            return new CDbExpression(sprintf('%s %s ((%s / 100) * :percent)', $field, $sign, $field), [
                ':percent' => $price
            ]);
        }

        return new CDbExpression(sprintf('%s %s :price', $field, $sign), [
            ':price' => $price
        ]);
    }

	/**
	 * @param $default
	 * @return array
	 */
	private function getSortSettingsForDataProvider($default = null){
		$module = Yii::app()->getModule('store');
		$order = 'IF(t.discontinued = 1, 1, 0), IF(t.show_at_end = 1, 1, 0), ';
		if($default === null){
			$default = $module->getDefaultSort();
		}

		if($default == 't.price ASC'){
			$default = 'IF(t.discount_price, t.discount_price, t.price)';
		} elseif($default == 't.price DESC'){
			$default = 'IF(t.discount_price, t.discount_price, t.price) DESC';
		}

        return [
            'sortVar' => 'sort',
            'defaultOrder' => $order.$default,
            'attributes' => [
                'price' => [
                    'asc' => $order.'IF(t.discount_price, t.discount_price, t.price)',
                    'desc' => $order.'IF(t.discount_price, t.discount_price, t.price) DESC',
                    'label' => 'Цене',
                ],
				'price_desc' => [
					//'asc' => $order.'IF(t.discount_price, t.discount_price, t.price)',
					'asc' => $order.'IF(t.discount_price, t.discount_price, t.price) DESC',
					'label' => 'Сначала подороже',
				],
                'create_time' => [
                    'asc' => $order.'t.create_time',
                    //'desc' => $order.'t.create_time DESC',
                    'label' => 'Сначала новинки',
                ],
				'square' => [
					'asc' => $order.'t.square',
					'desc' => $order.'t.square DESC',
					'label' => 'Площади',
				],

            ]
        ];
	}

	/**
	 * @param mixed $products
	 */
	public function setProducts($products){
		if(!$products){
			return;
		}

		if($products instanceof CDataProvider){
			foreach ($products->getData() as $product){
				$this->products[$product->id] = $product;
			}
		} else if($products instanceof Product) {
			$this->products[$products->id] = $products;
		} else {
			foreach ($products as $product){
				$this->products[$product->id] = $product;
			}
		}

	}
	/**
	 * @param int $id
	 * @return Product|null
	 */
	public function getProduct($id){
		return isset($this->products[$id]) ? $this->products[$id] : null;
	}
}
