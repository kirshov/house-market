<?php

/**
 * Class ProducerRepository
 */
class ProducerRepository extends CApplicationComponent
{
	protected $producers;
	protected $lines;

	public function init()
	{
		parent::init();
		$type = IS_BACKEND ? 'backend' : 'frontend';
		$this->producers = Yii::app()->getCache()->get('producers_'.$type);
		if(!is_array($this->producers)){
			$producers = [];
			//$producersData = Producer::model()->findAll(!IS_BACKEND ? 'status = '.Producer::STATUS_ACTIVE : '');
			$producersData = Producer::model()->cache(0)->findAll([
			    'condition' => !IS_BACKEND ? 'status = '.Producer::STATUS_ACTIVE : '',
                'order' => 'name',
            ]);
			if($producersData){
				foreach ($producersData as $producer){
					$producers[$producer->id] = $producer;
				}
			}
			Yii::app()->getCache()->set('producers_'.$type, $producers, 0, \TaggedCache\TaggingCacheHelper::getDependency(['store', 'store::producer']));
			$this->producers = $producers;
		}

        parent::init();
        $this->lines = Yii::app()->getCache()->get('producers_line_'.$type);
        if(!is_array($this->lines)){
            $producersLine = [];
            $producersLineData = ProducerLine::model()->cache(0)->findAll([
                'condition' => !IS_BACKEND ? 'status = '.Producer::STATUS_ACTIVE : '',
                'order' => 'name',
            ]);
            if($producersLineData){
                foreach ($producersLineData as $producer){
                    $producersLine[$producer->id] = $producer;
                }
            }
            Yii::app()->getCache()->set('producers_line_'.$type, $producersLine, 0, \TaggedCache\TaggingCacheHelper::getDependency(['store', 'store::producer::line']));
            $this->lines = $producersLine;
        }
	}

	/**
	 * @return Producer[]
	 */
	public function getById(){
		return $this->producers;
	}

    /**
     * @param StoreCategory $category
     * @param CDbCriteria $mergeWith
     * @return array|mixed|null
     */
    public function getForCategory(StoreCategory $category, CDbCriteria $mergeWith)
    {
        $criteria = new CDbCriteria([
            'order' => 't.sort ASC',
            'join' => 'LEFT JOIN {{store_product}} AS products ON products.producer_id = t.id',
            'distinct' => true,
        ]);

	    $categories = Yii::app()->getComponent('categoryRepository')->getActiveCategories($category->id);
		$criteria->addInCondition('products.category_id', $categories);

        $criteria->mergeWith($mergeWith);

        return Producer::model()->findAll($criteria);
    }

    /**
     * @return CActiveDataProvider
     */
    public function getAllDataProvider()
    {
        $criteria = new CDbCriteria();
        $criteria->scopes = ['published'];
        $criteria->order = 'sort';

        return new CActiveDataProvider(
            'Producer', [
                'criteria' => $criteria,
                'pagination' => [
                    'pageSize' => 20,
                    'pageVar' => 'page',
                ],
            ]
        );
    }

    /**
     * @param string $slug
     * @param array $with
     * @return mixed
     */
    public function getBySlug($slug, $with = ['line'])
    {
        return Producer::model()->published()->with($with)->find('t.slug = :slug', [':slug' => $slug]);
    }

    /**
     * @param string $slug
     * @param integer $producerId
     * @return mixed
     */
    public function getLineBySlug($slug, $producerId = null)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 't.slug = :slug';
        $criteria->params = [':slug' => $slug];
        if($producerId){
            $criteria->addCondition('producer_id = :producer_id');
            $criteria->params[':producer_id'] = $producerId;
        }

        return ProducerLine::model()->published()->find($criteria);
    }

	/**
	 * @return array
	 */
	public function getActiveIds(){
		return array_keys($this->producers) ?: [0];
	}

	/**
	 * @param $id
	 * @param $key
	 * @return array|mixed|null
	 */
	public function getCountry($id = null, $key = null)
	{
		$countries = Yii::app()->getCache()->get('countries');
		if(!is_array($countries)){
			$countries = [];
			$countryData = ProducerCountry::model()->published()->findAll();
			if($countryData){
				foreach ($countryData as $country){
					$countries[$country->id] = $country;
				}
			}
			Yii::app()->getCache()->set('countries', $countries, 0, \TaggedCache\TaggingCacheHelper::getDependency(['store']));
		}

		if($id !== null){
			if($countries[$id]){
				return $key ? $countries[$id]->$key : $countries;
			}
			return null;
		}

		return $countries;
	}

    /**
     * @param $slug
     * @return ProducerCountry
     */
    public function getCountryBySlug($slug)
    {
        return ProducerCountry::model()->published()->find('t.slug = :slug', [':slug' => $slug]);
    }

	/**
	 * @param $id
	 * @return mixed
	 */
    public function getProducer($id){
    	return isset($this->producers[$id]) ? $this->producers[$id] : null;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getProducerLine($id){
        return isset($this->lines[$id]) ? $this->lines[$id] : null;
    }
}