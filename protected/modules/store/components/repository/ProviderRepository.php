<?php

/**
 * Class ProviderRepository
 */
class ProviderRepository extends CApplicationComponent
{
    /**
     * @var
     */
    protected $providers;

    /**
     *
     */
    public function init()
    {
    	$type = IS_BACKEND ? 'backend' : 'frontend';
	    $this->providers = Yii::app()->getCache()->get('providers_'.$type);
	    if($this->providers === false){
		    $providers = [];
	    	$providersData = Provider::model()->findAll(!IS_BACKEND ? 'status = '.Provider::STATUS_ACTIVE : '');
		    if($providersData){
			    foreach ($providersData as $provider){
				    $providers[$provider->id] = $provider;
			    }
		    }
		    Yii::app()->getCache()->set('providers_'.$type, $providers, 0, \TaggedCache\TaggingCacheHelper::getDependency(['provider']));
		    $this->providers = $providers;
	    }

    }

	/**
	 * @param integer $id
	 * @return Provider
	 */
    public function getProvider($id){
    	return $this->providers[$id];
    }

	/**
	 * @return array
	 */
	public function getActiveIds(){
		return array_keys($this->providers) ?: [0];
	}
}
