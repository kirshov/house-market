<?php

/**
 * Class StoreCategoryRepository
 */
class StoreCategoryRepository extends CApplicationComponent
{

    /**
     * @param $slug
     * @return StoreCategory
     */
    public function getByAlias($slug)
    {
        return StoreCategory::model()->published()->find('slug = :slug', [
            ':slug' => $slug,
        ]);
    }

	/**
	 * Returns category by alias
	 *
	 * @param $slug
	 * @return mixed
	 */
	public function getByGroupAlias($slug)
	{
		return StoreCategory::model()->published()->find('groups_alias = :slug', [
			':slug' => $slug,
		]);
	}

    /**
     *
     */
    public function getAllDataProvider()
    {
        return new CArrayDataProvider(
            StoreCategory::model()->published()->getMenuList(1), [
                'id' => 'id',
                'pagination' => false,
            ]
        );
    }

    /**
     * @return StoreCategory[]
     */
    public function getAll(){
        return StoreCategory::model()->published()->roots()->with(['children'])->findAll(['order' => 't.sort']);
    }


    /**
     * @param $path
     * @return mixed
     */
    public function getByPath($path)
    {
        return StoreCategory::model()->published()->findByPath($path);
    }

	/**
	 * @param int $parent
	 * @param bool $forceFrontend
	 * @return array|mixed
	 * @throws CException
	 */
	public function getActiveCategories($parent = 0, $forceFrontend = false){
		$isBackend = IS_BACKEND && !$forceFrontend;
		$cacheName = 'store::activeCategories'.($isBackend ? '_backend' : '');

		if(!$parent){
		    $ids = Yii::app()->getCache()->get($cacheName);
		    if($ids !== false){
			    return $ids;
		    }
	    }

        $categoriesData = $this->getAllActiveCategory($forceFrontend);

        $catsParents = [];
		if($categoriesData){
			foreach ($categoriesData as $item){
				$_parent = (int) $item['parent_id'];
				$catsParents[$_parent][] = $item['id'];
			}

			$ids = Helper::getChildList($catsParents, $parent);
		} else {
			$ids = [0];
		}
		if($parent){
			$ids[] = $parent;
		}
		if(!$parent) {
			Yii::app()->getCache()->set($cacheName, $ids, 0, \TaggedCache\TaggingCacheHelper::getDependency(['store', 'store::category']));
		}
		return $ids;
	}

	/**
     * @param $id
     * @return StoreCategory|null
     */
	public function getById($id){

		$dataContainer = Yii::app()->getComponent('dataContainer');
		$cacheKey = 'store::StoreCategory::'.$id;

		if($dataContainer->has($cacheKey)){
			return $dataContainer->get($cacheKey);
		}
		$data = StoreCategory::model()->publishedAll()->findByPk($id);

		$dataContainer->set($cacheKey, $data);
		return $data;
    }

	/**
	 * @param array $ids
	 * @return StoreCategory[]
	 * @throws CException
	 */
	public function getCatsByIds($ids){
		$categoriesData = $this->getAllActiveCategory();
		foreach ($ids as $id){
			if(!isset($categoriesData[$id])){
				unset($ids[$id]);
			}
		}
		if(!$ids){
			return [];
		}

		$criteria = new CDbCriteria();
		$criteria->addInCondition('id', $ids);
		return StoreCategory::model()->published()->findAll($criteria);
	}

	/**
	 * @param bool $forceFrontend
	 * @return array|mixed
	 * @throws CException
	 */
    public function getAllActiveCategory($forceFrontend = false){
        $key = 'store::allActiveCategory'.(IS_BACKEND && !$forceFrontend ? '_backend' : '');

        $categoriesData = Yii::app()->getCache()->get($key);

        if (false === $categoriesData) {
            if(IS_BACKEND && !$forceFrontend){
                $data = Yii::app()->db->createCommand()
                    ->select('id, parent_id, slug, name')
                    ->from('{{store_category}}')
                    ->queryAll();
            } else {
                $data = Yii::app()->db->createCommand()
                    ->select('id, parent_id, slug, name')
                    ->from('{{store_category}}')
                    ->where('status = :status', [':status' => StoreCategory::STATUS_PUBLISHED])
                    ->queryAll();
            }

            $categoriesData = [];
            foreach ($data as $item){
                $categoriesData[$item['id']] = $item;
            }

            Yii::app()->getCache()->set($key, $categoriesData, 0, \TaggedCache\TaggingCacheHelper::getDependency(['store', 'store::category']));
        }

        return $categoriesData;
    }

	/**
	 * @return CActiveDataProvider
	 */
    public function getRootCategories(){
	    return new CActiveDataProvider('StoreCategory', [
	    	'criteria' => [
			    'scopes' => ['published', 'roots'],
			    'order' => 'sort',
		    ],
			'pagination' => false,
	    ]);
    }
}