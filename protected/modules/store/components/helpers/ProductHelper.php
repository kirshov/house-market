<?php

class ProductHelper
{
    /**
     * Get product url
     *
     * @param Product $product
     * @param bool $absolute
     * @return string
     */
    public static function getUrl(Product $product, $absolute = false)
    {
        $route = '/store/product/view';
        $params = [
            'name' => $product->slug,
        ];

        if ($product->category_id) {
            $category = StoreCategory::getById($product->category_id);
            if($category){
                $params['category'] = $category->path;
            }
        }

        return $absolute ? Yii::app()->createAbsoluteUrl($route, $params) : Yii::app()->createUrl($route, $params);
    }

	/**
	 * @param $price
	 * @return string
	 */
	public static function getFormatPrice($price){
		return Helper::getFormatPrice($price);
	}
}