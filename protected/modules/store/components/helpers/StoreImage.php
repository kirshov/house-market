<?php

class StoreImage
{
    /**
     * @param Producer $producer
     * @param int $width
     * @param int $height
     * @param bool|true $crop
     * @return mixed
     */
    public static function producer(Producer $producer, $width = 0, $height = 0, $crop = true)
    {
        return $producer->getImageUrl($width, $height, $crop, static::getDefaultImage());
    }

	/**
	 * @param ProducerLine $producerLine
	 * @param int $width
	 * @param int $height
	 * @param bool|true $crop
	 * @return mixed
	 */
	public static function producerLine(ProducerLine $producerLine, $width = 0, $height = 0, $crop = true)
	{
		return $producerLine->getImageUrl($width, $height, $crop, static::getDefaultImage());
	}

    /**
     * @param StoreCategory $category
     * @param int $width
     * @param int $height
     * @param bool|true $crop
     * @return mixed
     */
    public static function category(StoreCategory $category, $width = 0, $height = 0, $crop = true)
    {
        return $category->getImageUrl($width, $height, $crop, static::getDefaultImage());
    }

    /**
     * @param Product $product
     * @param int $width
     * @param int $height
     * @param bool|true $crop
     * @return mixed
     */
    public static function product(Product $product, $width = 0, $height = 0, $crop = true)
    {
        return $product->getImageUrl($width, $height, $crop, static::getDefaultImage());
    }

	/**
	 * @param ProductVariant $variant
	 * @param int $width
	 * @param int $height
	 * @param bool|true $crop
	 * @return mixed
	 */
	public static function variant(ProductVariant $variant, $width = 0, $height = 0, $crop = true)
	{
		if($variant->image_id){
			$image = $variant->product->getImagesById($variant->image_id);
			if($image){
				return $image->getImageUrl($width, $height, $crop, static::getDefaultImage());
			}
		}

		return $variant->product->getImageUrl($width, $height, $crop, static::getDefaultImage());
	}

    public static function getDefaultImage()
    {
    	$defaultImage = Yii::app()->getModule('webforma')->defaultImage;
    	if(!$defaultImage){
    		return null;
		}
        /* @var $theme \webforma\components\Theme */
        $theme = Yii::app()->getTheme();
        return $theme->getAssetsUrl() . $defaultImage;
    }
}