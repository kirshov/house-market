<?php

Yii::import('zii.behaviors.CTimestampBehavior');

/**
 * @property string $id
 * @property integer $type_id
 * @property integer $producer_id
 * @property integer $category_id
 * @property string $sku
 * @property string $name
 * @property string $slug
 * @property double $price
 * @property double $discount_price
 * @property double $discount
 * @property string $short_description
 * @property string $description
 * @property string $data
 * @property integer $is_special
 * @property integer $is_new
 * @property integer $is_hit
 * @property double $length
 * @property double $height
 * @property double $width
 * @property double $weight
 * @property integer $quantity
 * @property integer $in_stock
 * @property integer $status
 * @property datetime $create_time
 * @property datetime $update_time
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $image
 * @property double $average_price
 * @property double $purchase_price
 * @property double $recommended_price
 * @property integer $extra_charge
 * @property integer $position
 * @property string $external_id
 * @property string $title
 * @property string $meta_canonical
 * @property string $image_alt
 * @property string $image_title
 * @property string $view
 * @property integer $provider_id
 * @property integer $producer_line_id
 * @property integer $discontinued
 * @property integer $show_at_end
 * @property string $original_name
 * @property string $unit
 * @property string $search_name
 * @property integer $force_index
 * @property integer $square
 * @property integer $square_area
 * @property integer $floor_id
 * @property integer $done_id
 * @property integer $place_id
 * @property string $address
 * @property string $coords
 * @property string $date
 *
 * @method Product published
 * @method getImageUrl
 *
 * The followings are the available model relations:
 * @property Type $type
 * @property Producer $producer
 * @property ProducerLine $producerLine
 * @property Provider $provider
 * @property StoreCategory $category
 * @property ProductImage $mainImage
 * @property ProductImage[] $images
 * @property ProductVariant[] $variants
 * @property Comment[] $comments
 * @property StoreCategory[] $categories
 * @property ProductTabs[] $tabs
 *
 */
class Product extends webforma\models\WModel
{
	/**
	 *
	 */
	const SPECIAL_NOT_ACTIVE = 0;
	/**
	 *
	 */
	const SPECIAL_ACTIVE = 1;

	/**
	 *
	 */
	const NEW_NOT_ACTIVE = 0;
	/**
	 *
	 */
	const NEW_ACTIVE = 1;

	/**
	 *
	 */
	const HIT_NOT_ACTIVE = 0;
	/**
	 *
	 */
	const HIT_ACTIVE = 1;

	/**
	 *
	 */
	const STATUS_NOT_ACTIVE = 0;
	/**
	 *
	 */
	const STATUS_ACTIVE = 1;

	/**
	 *
	 */
	const STATUS_NOT_IN_STOCK = 0;
	/**
	 *
	 */
	const STATUS_IN_STOCK = 1;

	/**
	 *
	 */
	const ROUND_PRICE_NONE = 0;

	/**
	 *
	 */
	const ROUND_PRICE_SIMPLE = 1;

	/**
	 *
	 */
	const ROUND_PRICE_DEC = 2;

	/**
	 *
	 */
	const CACHE_TAG = 'store::products';

	/**
	 * @var array
	 */
	public $selectedVariants = [];

	/**
	 * @var integer
	 */
	public $minPrice;

	/**
	 * @var integer
	 */
	public $maxPrice;

    /**
     * @var bool
     */
    public $is_discount;

    /**
     * @var bool
     */
    public $is_new;

    /**
     * @var bool
     */
    public $null_price;

    /**
     * @var bool
     */
    public $unique_extra_charge;

    /**
     * @var bool
     */
    public $dont_match_cat;

    /**
	 * @var
	 */
	protected $_typeAttributes;

	/**
	 * @var array кеш getVariantsOptions
	 */
	protected $_variantsOptions = false;
	protected $_variantsAttributes;
	protected $_characteristics;

	/**
	 * @var array
	 */
	protected $_attributesValues;

	/**
	 * @var Producer
	 */
	protected $_producer;

    /**
     * @var Producer
     */
    protected $_producerLine;

    /**
     * @var bool
     */
    protected $_variantsLoaded = false;
    protected $_variantsData;

	/**
	 * @var ProductImage[]
	 */
    protected $_sortedImages;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Product Static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{store_product}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			[
				'name, original_name, title, description, short_description, slug, price, discount_price, discount, data, status, is_special, extra_charge, is_new, search_name, coords, date',
				'filter',
				'filter' => 'trim',
			],
			['name, slug', 'required'],//, category_id
			['square, square_area, floor_id, done_id, place_id', 'required'],//, address
			[
				'status, is_special, is_new, discontinued, show_at_end, producer_id, producer_line_id, type_id, quantity, in_stock, category_id, provider_id, force_index, count_view',
				'numerical',
				'integerOnly' => true,
			],
			[
				'price, average_price, purchase_price, recommended_price, discount_price, discount, length, height, width, weight, square, square_area',
				'store\components\validators\NumberValidator',
			],
			[
				'name, original_name, title, meta_keywords, meta_title, meta_description, meta_canonical, image, image_alt, image_title',
				'length',
				'max' => 250,
			],
			['discount_price, discount, average_price, purchase_price, recommended_price', 'default', 'value' => null],
			['sku, view', 'length', 'max' => 100],
			['slug, unit', 'length', 'max' => 150],
			['external_id', 'length', 'max' => 100],
			[
				'slug',
				'webforma\components\validators\YSLugValidator',
				'message' => Yii::t('StoreModule.store', 'Illegal characters in {attribute}'),
			],
			['slug', 'unique'],
			['status', 'in', 'range' => array_keys($this->getStatusList())],
			['is_special, is_new, is_hit', 'boolean'],
			['length, height, width, weight', 'default', 'setOnEmpty' => true, 'value' => null],
			['meta_canonical', 'url'],
			['type_id', 'default', 'value' => null],
			[
				'id, type_id, producer_line_id, producer_id, sku, name, original_name, slug, price, discount_price, discount, short_description, description, data, is_special, is_new, 
				length, height, width, weight, quantity, in_stock, status, create_time, update_time, meta_title, meta_description, meta_keywords, category_id, extra_charge,
				unique_extra_charge, is_discount, null_price, dont_match_cat, unit, discontinued',
				'safe',
				'on' => 'search',
			],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		$relations = [
			'type' => [self::BELONGS_TO, 'Type', 'type_id'],
			'producer' => [self::BELONGS_TO, 'Producer', 'producer_id'],
			'producerLine' => [self::BELONGS_TO, 'ProducerLine', 'producer_line_id', 'joinType' => 'LEFT JOIN'],
			'provider' => [self::BELONGS_TO, 'Provider', 'provider_id'],
			'categoryRelation' => [self::HAS_MANY, 'ProductCategory', 'product_id'],
			'categories' => [self::HAS_MANY, 'StoreCategory', ['category_id' => 'id'], 'through' => 'categoryRelation'],
			'category' => [self::BELONGS_TO, 'StoreCategory', ['category_id' => 'id']],
			'images' => [self::HAS_MANY, 'ProductImage', 'product_id', 'order' => 'images.position'],
			'variants' => [
				self::HAS_MANY,
				'ProductVariant',
				['product_id'],
				'with' => ['attribute', 'attributeOption'],
				'order' => 'variants.position ASC',
			],
			'linkedProductsRelation' => [self::HAS_MANY, 'ProductLink', 'product_id'],
			'linkedProducts' => [
				self::HAS_MANY,
				'Product',
				['linked_product_id' => 'id'],
				'through' => 'linkedProductsRelation',
				'joinType' => 'INNER JOIN',
			],
			'attributesValues' => [
				self::HAS_MANY,
				'AttributeValue',
				'product_id',
				'with' => ['attribute' => ['alias' => 'attr']],
			],
		];

		if(Yii::app()->hasModule('comment')){
			$relations['comments'] = [
				self::HAS_MANY,
				'Comment',
				'model_id',
				'on' => 'model = :model AND comments.status = :status',
				'params' => [
					':model' => __CLASS__,
					':status' => Comment::STATUS_APPROVED,
				],
				'order' => 'comments.lft',
			];
		}

		if(Yii::app()->getModule('store')->useProductTabs){
			$relations['tabs'] = [self::HAS_MANY, 'ProductTabs', 'product_id'];
		}
		return $relations;
	}

	/**
	 * @return array
	 */
	public function scopes()
	{
		//$categoryIds = Yii::app()->getComponent('categoryRepository')->getActiveCategories();

		return [
			'published' => [
				'condition' => 't.status = :status',
				//'condition' => 't.status = :status AND t.category_id IN ('.implode(', ', $categoryIds).')',
				'params' => [
					':status' => self::STATUS_ACTIVE,
				],
			],
			'specialOffer' => [
				'condition' => 't.is_special = :is_special',
				'params' => [':is_special' => self::SPECIAL_ACTIVE],
			],
			'specialOfferDiscount' => [
				'condition' => 't.is_special = :is_special OR t.discount > 0 OR (t.discount_price > 0 AND t.price > t.discount_price)',
				'params' => [':is_special' => self::SPECIAL_ACTIVE],
			],
			'newOffer' => [
				'condition' => 't.is_new = :is_new',
				'params' => [':is_new' => self::NEW_ACTIVE],
			],
			'newOfferLast' => [
				'order' => 'IF(t.is_new, 0, 1), t.create_time DESC',
			],
			'popularOffer' => [
				'condition' => 't.is_hit = :is_hit',
				'params' => [':is_hit' => self::HIT_ACTIVE],
			],
		];
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('StoreModule.store', 'ID'),
			'category_id' => Yii::t('StoreModule.store', 'Category'),
			'type_id' => Yii::t('StoreModule.store', 'Type'),
			'name' => Yii::t('StoreModule.store', 'Title'),
			'original_name' => 'Оригинальное название',
			'price' => Yii::t('StoreModule.store', 'Price'),
			'discount_price' => Yii::t('StoreModule.store', 'Discount price'),
			'discount' => Yii::t('StoreModule.store', 'Discount, %'),
			'sku' => Yii::t('StoreModule.store', 'SKU'),
			'image' => Yii::t('StoreModule.store', 'Image'),
			'short_description' => 'Краткое описание',
			'description' => Yii::t('StoreModule.store', 'Description'),
			'slug' => Yii::t('StoreModule.store', 'Alias'),
			'data' => Yii::t('StoreModule.store', 'Data'),
			'status' => Yii::t('StoreModule.store', 'Status'),
			'create_time' => Yii::t('StoreModule.store', 'Added'),
			'update_time' => Yii::t('StoreModule.store', 'Updated'),
			'user_id' => Yii::t('StoreModule.store', 'User'),
			'change_user_id' => Yii::t('StoreModule.store', 'Editor'),
			'is_special' => Yii::t('StoreModule.store', 'Special'),
			'is_new' => Yii::t('StoreModule.store', 'New'),
			'extra_charge' => Yii::t('StoreModule.store', 'Extra Charge'),
			'length' => Yii::t('StoreModule.store', 'Length, m.'),
			'height' => Yii::t('StoreModule.store', 'Height, m.'),
			'width' => Yii::t('StoreModule.store', 'Width, m.'),
			'weight' => Yii::t('StoreModule.store', 'Weight, kg.'),
			'producer_id' => Yii::t('StoreModule.store', 'Producer'),
			'producer_line_id' => Yii::t('StoreModule.store', 'Producer line item'),
			'provider_id' => Yii::t('StoreModule.store', 'Provider'),
			'in_stock' => Yii::t('StoreModule.store', 'Stock status'),
			'quantity' => Yii::t('StoreModule.store', 'Quantity'),
			'meta_title' => Yii::t('StoreModule.store', 'Meta title'),
			'meta_keywords' => Yii::t('StoreModule.store', 'Meta keywords'),
			'meta_description' => Yii::t('StoreModule.store', 'Meta description'),
			'purchase_price' => Yii::t('StoreModule.store', 'Purchase price'),
			'average_price' => Yii::t('StoreModule.store', 'Average price'),
			'recommended_price' => Yii::t('StoreModule.store', 'Recommended price'),
			'position' => Yii::t('StoreModule.store', 'Position'),
			'external_id' => Yii::t('StoreModule.store', 'External id'),
			'title' => Yii::t('StoreModule.store', 'SEO_Title'),
			'meta_canonical' => Yii::t('StoreModule.store', 'Canonical'),
			'image_alt' => Yii::t('StoreModule.store', 'Image alt'),
			'image_title' => Yii::t('StoreModule.store', 'Image title'),
			'view' => Yii::t('StoreModule.store', 'Template'),
			'is_discount' => 'Есть скидки',
			'null_price' => 'Нулевая цена',
			'unique_extra_charge' => 'Своя наценка',
			'discontinued' => 'Снят с производства',
			'show_at_end' => 'Выводить в конце списка',
			'unit' => 'Единица измерения',
			'search_name' => 'Подзаголовок в карточке',
			'force_index' => 'Не добавлять "noindex" к описанию товара',
			'count_view' => 'Просмотров',
			'is_hit' => Yii::t('StoreModule.store', 'Hit'),
			'square' => 'Площадь, м<sup>2</sup>',
			'square_area' => 'Площадь участка, соток',
			'floor_id' => 'Этажность',
			'done_id' => 'Готовность',
			'place_id' => 'Поселок',
			'address' => 'Адрес',
			'coords' => 'Координаты',
			'date' => 'Дата завершения',
		];
	}

	/**
	 * @return array customized attribute descriptions (name=>description)
	 */
	public function attributeDescriptions()
	{
		return [
			'id' => Yii::t('StoreModule.store', 'ID'),
			'category_id' => Yii::t('StoreModule.store', 'Category'),
			'name' => Yii::t('StoreModule.store', 'Title'),
			'price' => Yii::t('StoreModule.store', 'Price'),
			'sku' => Yii::t('StoreModule.store', 'SKU'),
			'image' => Yii::t('StoreModule.store', 'Image'),
			'short_description' => Yii::t('StoreModule.store', 'Short description'),
			'description' => Yii::t('StoreModule.store', 'Description'),
			'slug' => Yii::t('StoreModule.store', 'Alias'),
			'data' => Yii::t('StoreModule.store', 'Data'),
			'status' => Yii::t('StoreModule.store', 'Status'),
			'create_time' => Yii::t('StoreModule.store', 'Added'),
			'update_time' => Yii::t('StoreModule.store', 'Edited'),
			'user_id' => Yii::t('StoreModule.store', 'User'),
			'change_user_id' => Yii::t('StoreModule.store', 'Editor'),
			'is_special' => Yii::t('StoreModule.store', 'Special'),
			'length' => Yii::t('StoreModule.store', 'Length, m.'),
			'height' => Yii::t('StoreModule.store', 'Height, m.'),
			'width' => Yii::t('StoreModule.store', 'Width, m.'),
			'weight' => Yii::t('StoreModule.store', 'Weight, kg.'),
			'quantity' => Yii::t('StoreModule.store', 'Quantity'),
			'producer_id' => Yii::t('StoreModule.store', 'Producer'),
			'purchase_price' => Yii::t('StoreModule.store', 'Purchase price'),
			'average_price' => Yii::t('StoreModule.store', 'Average price'),
			'recommended_price' => Yii::t('StoreModule.store', 'Recommended price'),
			'title' => Yii::t('StoreModule.store', 'SEO_Title'),
			'meta_canonical' => Yii::t('StoreModule.store', 'Canonical'),
			'image_alt' => Yii::t('StoreModule.store', 'Image alt'),
			'image_title' => Yii::t('StoreModule.store', 'Image title'),
			'view' => Yii::t('StoreModule.store', 'Template'),
		];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$module = Yii::app()->getModule('store');
		$criteria = new CDbCriteria;
		$criteria->with = ['category', 'categories', 'producer', 'producerLine'];
		$criteria->group = 't.id';
		if($this->dont_match_cat){
            foreach (StoreCategory::model()->findAll('parent_id IS NULL') as $category){
                if($category->product_type){
                    $newCriteria = new CDbCriteria();
                    $categories = [$category->id];
                    $categories = CMap::mergeArray($categories, $category->getChildsArray());
                    $newCriteria->addInCondition('t.category_id', $categories);
                    $newCriteria->addCondition('t.type_id != :type_id_'.$category->product_type);
                    $newCriteria->params[':type_id_'.$category->product_type] = $category->product_type;

                    $criteria->mergeWith($newCriteria, 'OR');
                }
            }
        } else {

            $criteria->compare('t.id', $this->id);
			$criteria->compare('t.external_id', $this->id, true, 'OR');
            $criteria->compare('t.type_id', $this->type_id);
            $criteria->compare('t.name', $this->name, true);
            $criteria->compare('t.original_name', $this->name, true, 'OR');
            $criteria->compare('t.price', $this->price);
            $criteria->compare('t.discount_price', $this->discount_price);
            $criteria->compare('t.sku', $this->sku, true);
            $criteria->compare('t.short_description', $this->short_description, true);
            $criteria->compare('t.description', $this->description, true);
            $criteria->compare('t.meta_description', $this->meta_description, true);
            $criteria->compare('t.slug', $this->slug, true);
            $criteria->compare('t.data', $this->data, true);
            $criteria->compare('t.extra_charge', $this->extra_charge);
            $criteria->compare('t.status', $this->status);
            $criteria->compare('t.create_time', $this->create_time, true);
            $criteria->compare('t.update_time', $this->update_time, true);
            $criteria->compare('t.producer_id', $this->producer_id);
			$criteria->compare('t.producer_line_id', $this->producer_line_id);
			$criteria->compare('t.provider_id', $this->provider_id);
			$criteria->compare('t.purchase_price', $this->purchase_price);
            $criteria->compare('t.average_price', $this->average_price);
            $criteria->compare('t.recommended_price', $this->recommended_price);
            $criteria->compare('t.in_stock', $this->in_stock);
            $criteria->compare('t.unit', $this->unit);
			if($this->discontinued) {
				$criteria->compare('t.discontinued', $this->discontinued);
			}

            $criteria->join = 'LEFT JOIN {{store_product_variant}} v ON t.id = v.product_id';

            if($this->is_special){
                $criteria->addCondition('t.is_special = 1');
            }
			if($this->quantity){
				//OR v.quantity = :quantity
				$criteria->addCondition('t.quantity = :quantity');
				$criteria->params[':quantity'] = $this->quantity;
            }

            if($this->is_new){
                $criteria->addCondition('t.is_new = 1');
            }

            if($this->is_discount){
                $criteria->addCondition('t.discount > 0');
            }

            if($this->null_price){
            	if($module->priceType == StoreModule::PRICE_TYPE_CHARGE){
					$criteria->addCondition('((t.price = 0 AND t.purchase_price = 0) OR (t.price = 0 AND t.purchase_price > 0) OR v.purchase_price = 0)');
				} else {
					$criteria->addCondition('t.price = 0');
				}
            }

            if($this->unique_extra_charge){
                $criteria->addCondition('t.extra_charge NOT IS NULL OR v.extra_charge NOT IS NULL');
            }

            if ($this->category_id) {
                $categoryCriteria = new CDbCriteria();
                $categoryCriteria->compare('t.category_id', $this->category_id);
                $categoryCriteria->addCondition(sprintf('t.id IN (SELECT product_id FROM {{store_product_category}} WHERE category_id = :category_id)'),
                    'OR');
                $categoryCriteria->params = CMap::mergeArray($categoryCriteria->params,
                    [':category_id' => $this->category_id]);
                $criteria->mergeWith($categoryCriteria);
            }
        }

		return new CActiveDataProvider(
			'Product', [
				'criteria' => $criteria,
				'sort' => [
					'defaultOrder' => 't.position',//$module->getDefaultSort(),
				],
			]
		);
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		$module = Yii::app()->getModule('store');

		return [
			'time' => [
				'class' => 'zii.behaviors.CTimestampBehavior',
				'setUpdateOnCreate' => true,
				'createAttribute' => 'create_time',
				'updateAttribute' => 'update_time',
			],
			'upload' => [
				'class' => 'webforma\components\behaviors\ImageUploadBehavior',
				'attributeName' => 'image',
				'uploadPath' => $module->uploadPath.'/product',
			],
			'sortable' => [
				'class' => 'webforma\components\behaviors\SortableBehavior',
			],
		];
	}

	/**
	 * @return bool
	 */
	public function beforeValidate()
	{
		foreach ($this->getTypeAttributes() as $attribute) {

			if ($attribute->isType(Attribute::TYPE_CHECKBOX)) {
				continue;
			}

			if ($attribute->isRequired() && (!isset($this->_typeAttributes[$attribute->id]) || '' === $this->_typeAttributes[$attribute->id])
			) {
				$this->addError(
					$attribute->title,
					Yii::t("StoreModule.store", "{title} attribute is required", ['title' => $attribute->title])
				);
			}
		}

		if (!$this->isInStock()) {
			$this->setEmptyQuantity();
		}

		return parent::beforeValidate();
	}

	/**
	 * @return $this
	 */
	public function setEmptyQuantity()
	{
		$this->quantity = 0;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getStatusList()
	{
		return [
			self::STATUS_ACTIVE => Yii::t('StoreModule.store', 'Active'),
			self::STATUS_NOT_ACTIVE => Yii::t('StoreModule.store', 'Not active'),
		];
	}

	/**
	 * @return string
	 */
	public function getStatusTitle()
	{
		$data = $this->getStatusList();

		return isset($data[$this->status]) ? $data[$this->status] : Yii::t('StoreModule.store', '*unknown*');
	}

	/**
	 * @return array
	 */
	public function getSpecialList()
	{
		return [
			self::SPECIAL_NOT_ACTIVE => Yii::t('StoreModule.store', 'No'),
			self::STATUS_ACTIVE => Yii::t('StoreModule.store', 'Yes'),
		];
	}

	/**
	 * @return int
	 */
	public function isSpecial()
	{
		return $this->is_special;
	}

	/**
	 * @return int
	 */
	public function isNew()
	{
		return $this->is_new;
	}

	/**
	 * @return string
	 */
	public function getSpecial()
	{
		$data = $this->getSpecialList();

		return isset($data[$this->is_special]) ? $data[$this->is_special] : Yii::t('StoreModule.store', '*unknown*');
	}

	/**
	 * @return array
	 */
	public function getInStockList()
	{
		return self::getInStockListData();
	}

	/**
	 * @return array
	 */
	public static function getInStockListData(){
		return [
			self::STATUS_IN_STOCK => Yii::t('StoreModule.store', 'In stock'),
			self::STATUS_NOT_IN_STOCK => Yii::t('StoreModule.store', 'Not in stock'),
		];
	}

	/**
	 * Устанавливает дополнительные категории товара
	 *
	 * @param array $categoriesId - список id категорий
	 * @return bool
	 *
	 */
	public function saveCategories(array $categoriesId)
	{
		$categoriesId = array_diff($categoriesId, (array)$this->category_id);

		$currentCategories = Yii::app()->getDb()->createCommand()
			->select('category_id')
			->from('{{store_product_category}}')
			->where('product_id = :id', [':id' => $this->id])
			->queryColumn();

		if ($categoriesId == $currentCategories) {
			return true;
		}

		$transaction = Yii::app()->getDb()->beginTransaction();

		try {

			Yii::app()->getDb()->createCommand()
				->delete('{{store_product_category}}', 'product_id = :id', [':id' => $this->id]);

			if (!empty($categoriesId)) {

				$data = [];

				foreach ($categoriesId as $id) {
					$data[] = [
						'product_id' => $this->id,
						'category_id' => (int)$id,
					];
				}

				Yii::app()->getDb()->getCommandBuilder()
					->createMultipleInsertCommand('{{store_product_category}}', $data)
					->execute();
			}

			$transaction->commit();

			return true;
		} catch (Exception $e) {
			$transaction->rollback();

			return false;
		}
	}

	/**
	 * @return mixed
	 */
	public function getCategoriesId()
	{
		return Yii::app()->getDb()->createCommand()
			->select('category_id')
			->from('{{store_product_category}}')
			->where('product_id = :id', [':id' => $this->id])
			->queryColumn();
	}

	/**
	 * @param array $attributes
	 * @return bool
	 */
	public function saveTypeAttributes(array $attributes)
	{
		$transaction = Yii::app()->getDb()->beginTransaction();
		try {
			AttributeValue::model()->deleteAll('product_id = :product', [
				':product' => $this->id,
			]);

			foreach ($attributes as $attribute => $value) {
				if (null === $value) {
					continue;
				}

				$model = AttributeValue::model()->find('product_id = :product AND attribute_id = :attribute', [
					':product' => $this->id,
					':attribute' => $attribute,
				]);

				//множественные значения

				if (is_array($value)) {
					foreach ($value as $val) {
						if(!$val && !is_numeric($val)){
							continue;
						}
						$model = new AttributeValue();
						if (false === $model->store($attribute, $val, $this)) {
							throw new InvalidArgumentException('Error store attribute!');
						}
					}

				} else {
					$model = $model ?: new AttributeValue();

					if (false === $model->store($attribute, $value, $this)) {
						throw new InvalidArgumentException('Error store attribute!');
					}
				}
			}

			$transaction->commit();

			return true;
		} catch (Exception $e) {
			$transaction->rollback();

			return false;
		}
	}

	/**
	 * @param array $tabs
	 * @return bool
	 */
	public function saveTabs(array $tabs)
	{
		$transaction = Yii::app()->getDb()->beginTransaction();

		try {
			$currentTabs = $this->getTabsFromKey();

			foreach ($tabs as $_key => $_tabItem) {
				if($currentTabs[$_key]){
					$currentTabs[$_key]->content = $_tabItem;
					$currentTabs[$_key]->save();
				} else {
					$tabModel = new ProductTabs();
					$tabModel->product_id = $this->id;
					$tabModel->key = $_key;
					$tabModel->content = $_tabItem;
					$tabModel->save();
				}
			}

			$transaction->commit();

			return true;
		} catch (Exception $e) {
			$transaction->rollback();

			return false;
		}
	}


	/**
	 * @param $attribute
	 * @return string
	 */
	public function attributeFile($attribute)
	{
		$value = $this->attribute($attribute);

		if (null === $value) {
			return null;
		}

		return Yii::app()->getRequest()->getBaseUrl(true).'/'.Yii::app()->getModule('webforma')->uploadPath.'/'.Yii::app()->getModule('store')->uploadPath.'/product/'.$value;
	}

	/**
	 * @param $attribute
	 * @param null $default
	 * @return bool|float|int|null|string|array
	 */
	public function attribute($attribute, $default = null)
	{
		if ($this->getIsNewRecord()) {
			return null;
		}

		$this->loadAttributes();

		$attributeName = $attribute instanceof Attribute ? $attribute->name : $attribute;

		if (!array_key_exists($attributeName, $this->_attributesValues)) {
			return $default;
		}

		//если атрибут имеет множество значений - вернем их массив
		if (is_array($this->_attributesValues[$attributeName])) {
			$values = [];
			foreach ($this->_attributesValues[$attributeName] as $attribute) {
				$value = $attribute->value($default);

				if (is_null($value)) {
					continue;
				}

				$values[] = $value;
			}

			return $values;
		}

		return $this->_attributesValues[$attributeName]->value($default);
	}

	/**
	 *
	 */
	protected function loadAttributes()
	{
		if (null === $this->_attributesValues) {

			$this->_attributesValues = [];

			foreach ($this->attributesValues as $attribute) {

				//собираем массив multiple values attributes
				if ($attribute->attribute->isMultipleValues()) {
					$this->_attributesValues[$attribute->attribute->name][] = $attribute;
				} else {
					$this->_attributesValues[$attribute->attribute->name] = $attribute;
				}
			}
		}
	}


	/**
	 *
	 */
	public function attributes()
	{
		return $this->attributesValues;
	}


	/**
	 * @return array
	 */
	public function getTypesAttributesValues()
	{
		$data = [];

		foreach ($this->attributesValues as $attribute) {
			if($attribute->getStoreAttribute()->isMultipleValues()){
				$data[$attribute->attribute_id][] = $attribute->value();
			} else {
				$data[$attribute->attribute_id] = $attribute->value();
			}

		}

		return $data;
	}

	/**
	 * @return bool
	 */
	public function beforeDelete()
	{
		// чтобы удалились файлики
		foreach ((array)$this->images as $image) {
			$image->delete();
		}

		return parent::beforeDelete();
	}

	/**
	 * @param array $attributes
	 * @param array $typeAttributes
	 * @param array $variants
	 * @param array $categories
	 * @param array $tabs
	 * @return bool
	 */
	public function saveData(array $attributes, array $typeAttributes, array $variants, array $categories = [], array $tabs = [])
	{
		$transaction = Yii::app()->getDb()->beginTransaction();

		try {
			$this->setAttributes($attributes);
			$this->setTypeAttributes($typeAttributes);

			if ($this->save()) {

				$this->saveVariants($variants);
				$this->saveCategories($categories);
				$this->saveTypeAttributes($typeAttributes);
				$this->saveTabs($tabs);

				$transaction->commit();

				return true;
			}

			return false;
		} catch (Exception $e) {
			$transaction->rollback();

			return false;
		}
	}

	/**
	 * @param array $variants
	 * @return bool
	 */
	private function saveVariants(array $variants)
	{
		$transaction = Yii::app()->getDb()->beginTransaction();

		try {
			$productVariants = [];
			foreach ($variants as $var) {
				$variant = null;
				if (isset($var['id'])) {
					$variant = ProductVariant::model()->findByPk($var['id']);
				}
				$variant = $variant ?: new ProductVariant();
				$variant->setAttributes($var);
				$variant->product_id = $this->id;
				if ($variant->save()) {
					$productVariants[] = $variant->id;
				}
			}

			$criteria = new CDbCriteria();
			$criteria->params = [':product_id' => $this->id];
			$criteria->addCondition('product_id = :product_id');
			$criteria->addNotInCondition('id', $productVariants);
			ProductVariant::model()->deleteAll($criteria);
			$transaction->commit();

			return true;
		} catch (Exception $e) {
			$transaction->rollback();

			return false;
		}
	}

	/**
     * @var bool $allowedRound
	 * @return float
	 */
	public function getBasePrice($allowedRound = true)
	{
		$module = Yii::app()->getModule('store');
		if($module->priceType == StoreModule::PRICE_TYPE_CHARGE){
			if((float) $this->purchase_price == 0){
				return $this->price;
			}
			$price = $this->price;
			if($price == 0){
				$extraCharge = $this->getExtraCharge();
				$price = $this->purchase_price;
				if($extraCharge){
					$price = $price + ($this->purchase_price * $extraCharge / 100);
				}
			}

			$price = round((float)$price, 0);

			if($allowedRound && $module->roundPrice && ((isset($extraCharge) && $extraCharge > 0) || !isset($extraCharge))){
				$price = self::roundPrice($price);
			}

			return $price;
		} else {
			$price = $this->price;
		}

		//if($module->priceType == StoreModule::PRICE_TYPE_NORMAL){
		if($allowedRound && $module->roundPrice){
			$price = self::roundPrice($price);
		}
		return $price;
		//}
	}

	/**
	 * @return float
	 */
	public function getResultPrice()
	{
		if($this->hasDiscount()){
			if(Yii::app()->getModule('store')->priceType == StoreModule::PRICE_TYPE_CHARGE){
				$price = (float)$this->getBasePrice(false);
				if($this->getDiscountValue() > 0){
					$price = $price * (1 - ((float)$this->getDiscountValue() ?: 0) / 100);
				}
			} else {
				if($this->discount_price){
					$price = $this->discount_price;
				}
			}
		} else {
			$price = (float)$this->getBasePrice(false);
		}

        if(Yii::app()->getModule('store')->roundPrice){
			$price = round((float)$price, 0);
            $price = self::roundPrice($price);
        }

		return $price;
	}

	/**
	 * @return float
	 */
	public function getDiscountPrice()
	{
	    if(Yii::app()->getModule('store')->priceType == StoreModule::PRICE_TYPE_CHARGE){
			$price = $this->hasDiscount() ? (float)$this->getBasePrice(false) * (1 - ((float)$this->getDiscountValue() ?: 0) / 100) : (float)$this->getBasePrice(false);

			if((float) $this->purchase_price == 0){
				return $price;
			}
		} else {
	    	$price = $this->discount_price;
		}

	    if(!$this->hasDiscount() && Yii::app()->getModule('store')->roundPrice){
            $price = self::roundPrice($price);
        }
		return $price;
	}

	/**
	 * @return bool
	 */
	public function hasDiscount()
	{
		if ($this->getDiscountValue() > 0) {
			return true;
		}

		return false;
	}

	/**
	 * @return integer
	 */
	public function getDiscountValue(){
		$discount = 0;
		if(!IS_BACKEND && !Yii::app()->getUser()->getIsGuest()){
			$userDiscount = Yii::app()->getUser()->getDiscount();
			if(is_numeric($userDiscount) && $userDiscount > 0){
				$discount = $userDiscount;
			}
		}
		if(Yii::app()->getModule('store')->priceType == StoreModule::PRICE_TYPE_CHARGE) {
			if($this->discount > $discount){
				$discount = $this->discount;
			}
		} else {
			if ($this->discount_price > 0){
				$temp = 100 - ((float) $this->discount_price * 100 / $this->price);
				if($temp > $discount){
					$discount = round($temp);
				}
			}
		}

		return $discount;
	}

	/**
	 * @return integer
	 */
	public function hasProductClearDiscount(){
		$discount = 0;
		if(Yii::app()->getModule('store')->priceType == StoreModule::PRICE_TYPE_CHARGE) {
			if($this->discount > $discount){
				$discount = $this->discount;
			}
		} else {
			if ($this->discount_price > 0){
				$temp = 100 - ((float) $this->discount_price * 100 / $this->price);
				if($temp > $discount){
					$discount = round($temp);
				}
			}
		}

		return $discount > 0;
	}

	/**
	 *
	 */
	public function getDiscount(){
		if($this->getDiscountValue() <= 0){
			return false;
		}
		return number_format($this->getDiscountValue(), 0, '.', '');
	}

	/**
	 * @return int
	 */
	public function getExtraCharge(){
		$extraCharge = 0;
		if(is_numeric($this->extra_charge)){
			$extraCharge = $this->extra_charge;
		} else {
			/**
			 * @var providerRepository $providerRepository
			 */
			$providerRepository = Yii::app()->getComponent('providerRepository');
			$provider = $providerRepository->getProvider($this->provider_id);
			if($provider){
				$extraCharge = $provider->extra_charge;
			}
		}

		return $extraCharge;
	}

	/**
	 * @return mixed id
	 */
	public function getId()
	{
		$variantIds = $this->getSelectedVariantsAsArray();
		sort($variantIds);

		return 'product_'.$this->id.'_'.implode('_', $variantIds);
	}

	/**
	 * @return array
	 */
	public function getSelectedVariantsAsArray(){
		return array_map(
			function ($var) {
				return $var->id;
			},
			$this->selectedVariants
		);
	}

	/**
	 * @param array $variantsIds
	 * @param mixed $type
	 * @return float|mixed
	 */
	public function getPrice(array $variantsIds = [], $type = null)
	{
		$variants = [];
		if (!empty($variantsIds)) {
			$criteria = new CDbCriteria();
			$criteria->addInCondition("id", $variantsIds);
			$variants = ProductVariant::model()->findAll($criteria);
		} else {
			$variants = $this->selectedVariants;
		}

		if($type == 'base'){
            $basePrice = $this->getBasePrice();
        } else {
            $basePrice = $this->getResultPrice();
        }

		/* выбираем вариант, который меняет базовую цену максимально */
		/* @var $variants ProductVariant[] */

		$hasBasePriceVariant = false;
		foreach ($variants as $variant) {
			if ($variant->type == ProductVariant::TYPE_BASE_PRICE) {
				if (!$hasBasePriceVariant) {
					$hasBasePriceVariant = true;
                    if($type == 'base') {
                        $basePrice = $variant->getBasePrice();
                    } else {
                        $basePrice = $variant->getResultPrice();
                    }
				} else {
                    if($type == 'base'){
                        if ($basePrice < $variant->getBasePrice()) {
                            $basePrice = $variant->getBasePrice();
                        }
                    } else {
                        if ($basePrice < $variant->getResultPrice()) {
                            $basePrice = $variant->getResultPrice();
                        }
                    }

				}
			}
		}
		$newPrice = $basePrice;
		foreach ($variants as $variant) {
			switch ($variant->type) {
                case ProductVariant::TYPE_SUM:
                    if ($type == 'base') {
                        $newPrice += $variant->getBasePrice();
                    } else {
                        $newPrice += $variant->getResultPrice();
                    }
                    break;
                case ProductVariant::TYPE_PERCENT:
                    if ($type == 'base') {
                        $newPrice += $basePrice * ($variant->getBasePrice() / 100);
                    } else {
                        $newPrice += $basePrice * ($variant->getResultPrice() / 100);
                    }
					break;
				case ProductVariant::TYPE_REDUCE:
					if ($type == 'base') {
						$newPrice -= $variant->getBasePrice();
					} else {
						$newPrice -= $variant->getResultPrice();
					}
					break;
			}
		}

		return $newPrice;
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
        if($template = Yii::app()->getModule('store')->metaProductH1){
        	//$producer = $this->getProducer();
            return Helper::mb_strtr($template, [
                '{name}' => $this->name,
                '{original}' => $this->original_name,
                '{category}' => $this->category->getTitle(),
                /*'{brand}' => $producer->name,
                '{country}' => $producer->country,*/
                '{line}' => $this->getProducerLineName(),
            ]);
        }

		return $this->title ?: $this->name;
	}

	/**
	 * @return mixed
	 */
	public function getLink()
	{
		return ProductHelper::getUrl($this);
	}

	/**
	 * @return null|string
	 */
	public function getCategoryId()
	{
		return is_object($this->category) ? $this->category->id : null;
	}

	/**
	 * @return array
	 */
	public function getTypeAttributes()
	{
		if (empty($this->type)) {
			return [];
		}

		return (array)$this->type->typeAttributes;
	}

	/**
	 * @return null|string
	 */
	public function getProducerName()
	{
		return null;
		$producer = $this->getProducer();
		if (empty($producer)) {
			return null;
		}

		return $producer->name;
	}

	/**
	 * @return null|string
	 */
	public function getProducerLineName()
	{
		if (!$this->producer_line_id || empty($this->producerLine)) {
			return null;
		}

		return $this->producerLine->name;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

    /**
     * @return string
     */
    public function getNameInList()
    {
        $value = '';
        $list = [];
        if($this->variants){
            foreach ($this->variants as $variant){
				$_attribute = $variant->getStoreAttribute();
                if($_attribute->append_name){
	                $attrValue = AttributeRender::renderValue($_attribute, $variant->attribute_value,true, '{item}');
                    $attrValue = str_replace(' мл', 'мл', $attrValue);
                    $attrValue = str_replace(' гр', 'гр', $attrValue);
                    $list[] = $attrValue;
                }
            }
            if(!empty($list)){
                $value = ' '.implode('/', $list);
            }
        } else {
        	if($this->attributes()){
        		foreach ($this->attributes() as $_attr){
					$_attribute = $_attr->getStoreAttribute();
        			if($_attribute->append_name){
        				$attrValue = AttributeRender::renderValue($_attribute, $_attr->option_value,true, '{item}');
				        $attrValue = str_replace(' мл', 'мл', $attrValue);
				        $attrValue = str_replace(' гр', 'гр', $attrValue);
				        $list[] = $attrValue;
			        }
		        }
		        if(!empty($list)){
                    $list = array_unique($list);
			        $value = ' '.implode('/', $list);
		        }
	        }
        }

	    //$producer = $this->getProducer();
        return $this->name./*($producer ? ' '.$producer->name.' ' : '').*/$value;
    }

	/**
	 * @return string
	 */
	public function getFullName()
	{
		$name = $this->name;
		if($this->original_name){
			$name .= ' ('.$this->original_name.')';
		}
		return $name;
	}

	/**
	 * @return int
	 */
	public function isInStock()
	{
		return $this->in_stock;
	}

	/**
	 * @return int
	 */
	public function isAvailable()
	{
		return $this->in_stock && $this->quantity;
	}

	/**
	 * @return string
	 */
	public function getMetaTitle()
	{
		if($this->meta_title){
			return $this->meta_title;
		}

        //$producer = $this->getProducer();
		if($template = Yii::app()->getModule('store')->metaProductTitle){
			return Helper::mb_strtr($template, [
				'{name}' => $this->name,
                '{original}' => $this->original_name,
				'{category}' => $this->category->getTitle(),
				/*'{brand}' => $producer->name,
				'{country}' => $producer->country,
				'{line}' => $this->getProducerLineName(),*/
			]);
		}

		return Helper::prepareSeoTitle([
		    $this->name,
			//$producer->name,
            //$this->category->getTitle(),
        ]);
	}

	/**
	 * @return string
	 */
	public function getMetaDescription()
	{
		if($this->meta_description){
			return $this->meta_description;
		}

		if($template = Yii::app()->getModule('store')->metaProductDescription){
			//$producer = $this->getProducer();
			return Helper::mb_strtr($template, [
				'{name}' => $this->name,
				'{original}' => $this->original_name,
				'{category}' => $this->category->getTitle(),
                /*'{brand}' => $producer->name,
                '{country}' => $producer->country,
                '{line}' => $this->getProducerLineName(),*/
			]);
		}
	}

	/**
	 * @return string
	 */
	public function getMetaKeywords()
	{
		if($this->meta_keywords){
			return $this->meta_keywords;
		}

		if($template = Yii::app()->getModule('store')->metaProductKeyWords){
			//$producer = $this->getProducer();
			return Helper::mb_strtr($template, [
				'{name}' => $this->name,
                '{original}' => $this->original_name,
				'{category}' => $this->category->getTitle(),
                /*'{brand}' => $producer->name,
                '{country}' => $producer->country,
                '{line}' => $this->getProducerLineName(),*/
			]);
		}
	}

	/**
	 * Get canonical url
	 *
	 * @return string
	 */
	public function getMetaCanonical()
	{
		return $this->meta_canonical;
	}

	/**
	 * @return ProductImage[]
	 */
	public function getImages()
	{
		return $this->images;
	}

	/**
	 * @param $id
	 * @return ProductImage
	 */
	public function getImagesById($id)
	{
		if($this->_sortedImages === null){
			foreach ($this->getImages() as $item){
				$this->_sortedImages[$item->id] = $item;
			}
		}
		return isset($this->_sortedImages[$id]) ? $this->_sortedImages[$id] : false;
	}

	/**
	 * @return array
	 */
	public function getAttributeGroups()
	{
		if (empty($this->type)) {
			return [];
		}

		return $this->type->getAttributeGroups();
	}

	/**
	 * @return array
	 */
	public function getAttributeGroupsWithId()
	{
		if (empty($this->type)) {
			return [];
		}

		return $this->type->getAttributeGroupsWithId();
	}

	/**
	 * @param bool $original
	 * @return array|mixed|ProductVariant[]
	 */
	public function getVariants($original = false)
	{
	    if($this->_variantsLoaded){
            return $this->_variantsData;
        }

		$this->_variantsLoaded = true;

		$usedAttributes = [];
		$variants = $this->variants;
		foreach ((array) $variants as $key => &$variant){
			$variant->setProduct($this);
			$usedAttributes[$variant->attribute_id] = $variant->attribute_id;
		}

	    if(!$original && !empty($usedAttributes) && !empty($variants)){
		    $characteristics = $this->getCharacteristicsByType();
		    $currentVariants = [];
		    foreach ($characteristics as $item){
		    	if(isset($usedAttributes[$item->attribute_id]) && isset($item->option_value)){
				    $currentVariant = new ProductVariant();
				    $currentVariant->setAttributes([
					    'id' => 'original_'.$this->id,
					    'product_id' => $this->id,
					    'attribute_id' => $item->attribute_id,
					    'attribute_value' => $item->option_value,
					    'amount' => $this->price,
					    'type' => ProductVariant::TYPE_BASE_PRICE,
					    'sku' => $this->sku,
					    'position' => $this->position,
					    'extra_charge' => $this->extra_charge,
					    'purchase_price' => $this->purchase_price,
					    'in_stock' => $this->in_stock,
					    'quantity' => $this->quantity,
					    'discount' => $this->discount,
					    'discount_price' => $this->discount_price,
					    'image_id' => null,
				    ]);

				    $currentVariants[] = $currentVariant;
			    }
		    }
	    	if($currentVariants){
			    $variants = array_merge($variants, $currentVariants);
		    }
	    }

		$this->_variantsData = $variants;
		return $variants;
	}

	/**
	 * @return array
	 */
	public function getVariantsGroup()
	{
		$variantsGroups = [];
		foreach ((array) $this->getVariants() as $variant) {
			$_attribute = $variant->getStoreAttribute();
			if($_attribute){
				$value = AttributeRender::renderValue($_attribute, $variant->attribute_value,true, '{item}');
				$variantsGroups[$_attribute->type][$_attribute->title][$value] = $variant;
			}
		}

		/*if(!empty($variantsGroups[$variant->attribute->type][$variant->attribute->title])){
			ksort($variantsGroups[$variant->attribute->type][$variant->attribute->title]);
		}*/

		return $variantsGroups;
	}

	/**
	 * @return array
	 */
	public function getVariantsAttributes()
	{
		if($this->_variantsAttributes !== null){
			return $this->_variantsAttributes;
		}
		$this->_variantsAttributes = [];

		foreach ((array)$this->getVariants() as $variant) {
			$this->_variantsAttributes[$variant->attribute_id] = true;
		}

		return $this->_variantsAttributes;
	}

	/**
	 * @param $attributeId
	 * @return bool
	 */
	public function hasVariantByAttribute($attributeId){
		if($this->_variantsAttributes === null){
			$this->getVariantsAttributes();
		}

		return isset($this->_variantsAttributes[$attributeId]);
	}

	/**
	 * Функция для подготовки специфичных настроек элементов option в select при выводе вариантов, которые будут использоваться в js при работе с вариантами
	 * @return array
	 */
	public function getVariantsOptions()
	{
		if ($this->_variantsOptions !== false) {
			return $this->_variantsOptions;
		}
		$options = [];
        $currency = Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency);
        $attributes = [];
		foreach ($this->getVariants() as $variant) {
			$attributes[$variant->attribute_id] = $variant->attribute_id;

			$options[$variant->id] = [
				'data-type' => $variant->type,
				'data-old-price' => $variant->hasDiscount() ? $variant->getBasePrice() : false,
				'data-amount' => $variant->getResultPrice(),
                'data-currency' => $currency,
                'data-discount' => $variant->hasDiscount() ? $variant->getDiscountValue() : false,
				'data-sku' => $variant->sku,
				'data-image' => $variant->image_id,
				'data-quantity' => $variant->quantity,
				'data-stock' => $variant->in_stock,
				//'disabled' => $variant->quantity > 0 ? null : true,
			];
		}

		$this->_variantsOptions = $options;
		return $this->_variantsOptions;
	}

	/**
	 * @return AttributeValue[]
	 */
	protected function getCharacteristicsByType(){
		if($this->_characteristics){
			return $this->_characteristics;
		}
		$this->_characteristics = [];
		if($this->attributesValues){
			foreach ($this->attributesValues as $value){
				$this->_characteristics[$value->attribute_id] = $value;
			}
		}
		return $this->_characteristics;
	}

	/**
	 * @return null|Product
	 * @throws CDbException
	 */
	public function copy()
	{
		$transaction = Yii::app()->getDb()->beginTransaction();
		$model = new Product();
		try {
			$model->setAttributes($this->getAttributes());
			$model->slug = null;

			$similarNamesCount = Yii::app()->getDb()->createCommand()
				->select('count(*)')
				->from($this->tableName())
				->where("name like :name", [':name' => $this->name.' [%]'])
				->queryScalar();

			$model->name = $this->name.' ['.($similarNamesCount + 1).']';
			$model->slug = \webforma\helpers\WText::translit($model->name);
			$model->image = $this->image;

			$attributes = $model->attributes;
			$typeAttributes = $this->getTypesAttributesValues();
			$variantAttributes = $categoriesIds = [];

			if ($variants = $this->variants) {
				foreach ($variants as $variant) {
					$variantAttributes[] = $variant->getAttributes(
						['attribute_id', 'attribute_value', 'amount', 'type', 'sku']
					);
				}
			}

			if ($categories = $this->categories) {
				foreach ($categories as $category) {
					$categoriesIds[] = $category->id;
				}
			}

			if (!$model->saveData($attributes, $typeAttributes, $variantAttributes, $categoriesIds)) {
				throw new CDbException('Error copy product!');
			}

			$transaction->commit();

			return $model;
		} catch (Exception $e) {
			$transaction->rollback();
		}

		return null;
	}


	/**
	 * @param $product
	 * @param null $typeId
	 * @return bool
	 */
	public function link($product, $typeId = null)
	{
		$link = new ProductLink();

		$link->setAttributes([
			'product_id' => $this->id,
			'linked_product_id' => ($product instanceof Product ? $product->id : $product),
			'type_id' => $typeId,
		]);

		return $link->save();
	}


	/**
	 * @param array $attributes
	 */
	public function setTypeAttributes(array $attributes)
	{
		$this->_typeAttributes = $attributes;
	}

	/**
	 * Get image alt tag text
	 *
	 * @return string
	 */
	public function getImageAlt()
	{
		return $this->image_alt ?: $this->getTitle();
	}

	/**
	 * Get image title tag text
	 *
	 * @return string
	 */
	public function getImageTitle()
	{
		return $this->image_title ?: $this->getTitle();
	}

	/**
	 * @param null $price
	 * @return string
	 */
	public function getFormatPrice($price = null){
		if($price === null){
			$price = $this->getResultPrice();
		}

		return ProductHelper::getFormatPrice($price);
	}

	/**
	 * @return array
	 */
	public static function getMarkerTitles(){
		return [
			'special' => Yii::t('StoreModule.store', 'Special'),
			'new' => Yii::t('StoreModule.store', 'New'),
			'popular' => Yii::t('StoreModule.store', 'Hit'),
			'hit' => Yii::t('StoreModule.store', 'Hit'),
		];
	}

	/**
	 * @return bool|string
	 */
	public function getType(){
		if($this->is_special || $this->hasProductClearDiscount()){
			return 'special';
		} elseif ($this->is_hit) {
			return 'hit';
		} elseif ($this->is_new) {
			return 'new';
		}

		return false;
	}

	/**
	 * @param null $type
	 * @return mixed
	 */
	public function getMarker($type = null){
		if(!$type){
			$type = $this->getType();
		}

		$markerTitles = self::getMarkerTitles();
		return $markerTitles[$type];
	}

	/**
	 * @return bool
	 */
	protected function beforeSave()
	{
		if(Yii::app()->getModule('store')->priceType == StoreModule::PRICE_TYPE_CHARGE){
			if($this->purchase_price > 0){
				$this->price = 0;
				$this->price = $this->getBasePrice();
			}
		}

		if($this->date){
			$this->date = date('Y-m-d', strtotime($this->date));
		}

		return parent::beforeSave();
	}

	protected function afterFind()
	{
		if($this->date){
			if($this->date == '0000-00-00'){
				$this->date = '';
			} else {
				$this->date = date('d.m.Y', strtotime($this->date));
			}
		}
		parent::afterFind();
	}


	/**
	 *
	 */
	protected function afterSave()
	{
		parent::afterSave();

		if($this->variants){
		    foreach ($this->variants as $variant){
		        $variant->save();
            }
        }

		Yii::app()->eventManager->fire(StoreEvents::STORE_CHANGE, new \webforma\components\Event($this));
	}

	/**
	 *
	 */
	protected function afterDelete()
	{
		parent::afterDelete();
		Yii::app()->eventManager->fire(StoreEvents::STORE_CHANGE, new \webforma\components\Event($this));
	}

    /**
     * @param $price
     * @return integer
     */
    public static function roundPrice($price){
	    $rounded = $price;

    	$roundType = Yii::app()->getModule('store')->roundPrice;
    	if($roundType == Product::ROUND_PRICE_SIMPLE) {
		    $rounded = round((float)$price, 0);
	    }

	    if($roundType == Product::ROUND_PRICE_DEC) {
		    $rounded = (floor($price/10)) * 10;
		    if($price < 1000 && $price - $rounded >= 5){
			    $rounded += 5;
		    }
	    }

        return $rounded;
    }

	/**
	 * @param int $id
	 * @return Product|null
	 */
    public static function getFromRepository($id){
    	return Yii::app()->getComponent('productRepository')->getProduct($id);
    }

	/**
	 * @return Producer
	 */
	public function getProducer(){
	    if(!$this->producer_id){
            return null;
        }
		if($this->_producer !== null){
			return $this->_producer;
		}
		$producer = Yii::app()->getComponent('producerRepository')->getProducer($this->producer_id);
		if($producer instanceof Producer){
			$this->_producer = $producer;
			return $producer;
		}

		return $this->producer;
	}

    /**
     * @return ProducerLine
     */
    public function getProducerLine(){
        if(!$this->producer_line_id){
            return null;
        }

        if($this->_producerLine !== null){
            return $this->_producerLine;
        }
        $producerLine = Yii::app()->getComponent('producerRepository')->getProducerLine($this->producer_line_id);
        if($producerLine instanceof ProducerLine){
            $this->_producerLine = $producerLine;
            return $producerLine;
        }

        return $this->producerLine;
    }

	/**
	 * @return ProductTabs[]
	 */
    public function getTabs(){
    	return $this->tabs;
    }

	/**
	 * @return ProductTabs[]
	 */
    public function getTabsFromKey(){
    	$tabs = [];
	    foreach((array) $this->getTabs() as $_tab){
		    $tabs[$_tab->key] = $_tab;
	    }
	    return $tabs;
    }

	/**
	 * @param $categoryId
	 * @return Product[]
	 */
	public static function getProductsByCategory($categoryId){
		$criteria = new CDbCriteria();
		$criteria->scopes = ['published'];
		$criteria->condition = 'category_id = :category_id';
		$criteria->params = [
			':category_id' => $categoryId
		];

		return self::model()->cache(0, \TaggedCache\TaggingCacheHelper::getDependency(['store', 'products']))->findAll($criteria);
	}
}
