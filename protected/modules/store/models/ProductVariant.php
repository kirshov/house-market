<?php

/**
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $attribute_id
 * @property string $attribute_value
 * @property double $amount
 * @property integer $type
 * @property string $sku
 * @property integer $position
 * @property integer $extra_charge
 * @property integer $purchase_price
 * @property integer $in_stock
 * @property integer $quantity
 * @property integer $discount
 * @property integer $discount_price
 * @property integer $image_id
 * @property string $external_id
 *
 * @property-read Attribute $attribute
 * @property-read Product $product
 */
class ProductVariant extends \webforma\models\WModel
{
    /**
     *
     */
    const TYPE_SUM = 0;
    /**
     *
     */
    const TYPE_PERCENT = 1;
    /**
     *
     */
    const TYPE_BASE_PRICE = 2;
	/**
	 *
	 */
	const TYPE_REDUCE = 3;

    /**
     * @var int
     */
    public $amount = 0;

    /**
     * @var
     */
    public $attributeOptionId;

    private $_optionValue;
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{store_product_variant}}';
    }

    /**
     * @param null|string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['attribute_id, product_id, type', 'required'],
	        ['extra_charge, external_id', 'filter', 'filter' => 'trim'],
            ['id, attribute_id, product_id, type, attributeOptionId, position, discount', 'numerical', 'integerOnly' => true],
            ['type', 'in', 'range' => [self::TYPE_SUM, self::TYPE_PERCENT, self::TYPE_BASE_PRICE, self::TYPE_REDUCE]],
            ['amount, purchase_price, discount_price, quantity, in_stock, image_id', 'numerical'],
            ['sku', 'length', 'max' => 50],
            ['attribute_value', 'length', 'max' => 255],
            ['id, attribute_id, attribute_value, product_id, amount, type, sku', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if ($this->attributeOptionId) {
            $option = AttributeOption::model()->findByPk($this->attributeOptionId);
            $this->attribute_value = $option ? $option->value : null;
        }
        if (!$this->attribute_value) {
            $this->addErrors(
                [
                    'attribute_value' => Yii::t('StoreModule.store', 'You must specify the attribute value'),
                ]
            );
        }

        return parent::beforeValidate();
    }

    /**
     * @return array
     */
    public function relations()
    {
        return [
	        'product' => [self::BELONGS_TO, 'Product', 'product_id'],
            'attribute' => [self::BELONGS_TO, 'Attribute', 'attribute_id'],
            'attributeOption' => [self::BELONGS_TO, 'AttributeOption', 'attribute_value'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('StoreModule.store', 'Id'),
            'product_id' => Yii::t('StoreModule.store', 'Product'),
            'attribute_id' => Yii::t('StoreModule.store', 'Attribute'),
            'attribute_value' => Yii::t('StoreModule.store', 'Attribute value'),
            'type' => Yii::t('StoreModule.store', 'Price type'),
            'amount' => Yii::t('StoreModule.store', 'Price'),
            'discount' => Yii::t('StoreModule.store', 'Discount'),
            'sku' => Yii::t('StoreModule.store', 'SKU'),
            'position' => Yii::t('StoreModule.store', 'Order'),
            'in_stock' => Yii::t('StoreModule.store', 'Stock status'),
            'quantity' => Yii::t('StoreModule.store', 'Quantity'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'sortable' => [
                'class' => 'webforma\components\behaviors\SortableBehavior',
            ],
        ];
    }

    /**
     * @return array
     */
    public function getTypeList()
    {
        return [
			self::TYPE_SUM => Yii::t('StoreModule.store', 'Increase in the amount of'),
			self::TYPE_BASE_PRICE => Yii::t('StoreModule.store', 'Changing base rates'),
			self::TYPE_REDUCE => 'Уменьшить на',
			//self::TYPE_PERCENT => Yii::t('StoreModule.store', 'Increase in %'),
        ];
    }


    /**
     * @return null|string
     */
    public function getOptionValue()
    {
        if($this->_optionValue !== null){
            return $this->_optionValue;
        }
        $value = null;

        if(!$this->attribute_value){
        	return null;
		}

		$attribute = $this->getStoreAttribute();
        switch ($attribute->type) {
            case Attribute::TYPE_CHECKBOX:
                $value = $this->attribute_value ? Yii::t('StoreModule.store', 'Yes') : Yii::t(
                    'StoreModule.store',
                    'No'
                );
                break;
            case Attribute::TYPE_DROPDOWN:
            case Attribute::TYPE_RADIO:
            case Attribute::TYPE_SHORT_TEXT:
            case Attribute::TYPE_NUMBER:
            case Attribute::TYPE_CHECKBOX_LIST:
				$value = Yii::app()->getComponent('attributesRepository')->getOptions($this->attribute_value);
                break;
			case Attribute::TYPE_COLOR:
				$temp = Yii::app()->getComponent('attributesRepository')->getOptions($this->attribute_value);
				list($value, ) = explode('|', $temp);
				break;
        }

        return $value;
    }

    /**
     * @param $value
     */
    public function setOptionValue($value){
        $this->_optionValue = $value;
    }

    /**
     * @return int
     */
    public function isInStock()
    {
        return $this->in_stock;
    }

    /**
     * @return int
     */
    public function isAvailable()
    {
        return $this->in_stock && $this->quantity;
    }

	/**
     * @var bool $allowedRound
	 * @return float
	 */
    public function getBasePrice($allowedRound = true){
        $price = $this->amount;
		$module = Yii::app()->getModule('store');
        if($module->priceType == StoreModule::PRICE_TYPE_CHARGE){
			if($price == 0){
				$extraCharge = $this->getExtraCharge();
				$price = $this->purchase_price;
				if($extraCharge){
					$price = $price + ($this->purchase_price * $extraCharge / 100);
				}
			}

			$price = round((float)$price, 0);

			if($allowedRound && $module->roundPrice && ((isset($extraCharge) && $extraCharge > 0) || !isset($extraCharge))){
				$price = Product::roundPrice($price);
			}
		} else {
			if($allowedRound && $module->roundPrice){
				$price = Product::roundPrice($price);
			}
		}

        return $price;
    }

    public function getExtraCharge(){
        if(is_numeric($this->extra_charge)){
            $extraCharge = $this->extra_charge;
        } else {
            $extraCharge = $this->getProduct()->getExtraCharge();
        }
        return (int) $extraCharge;
    }
	/**
	 * @return float
	 */
    public function getResultPrice(){
	    if($this->hasDiscount()){
		    $price = $this->discount_price;
	    } else {
		    $price = $this->getDiscountPriceClear();
	    }

	    if($this->getProduct()){
	    	$product = $this->getProduct();
	    } else {
		    $product = $this->product;
	    }

        if(!$this->hasDiscount() && (!$product || !$product->getDiscountValue()) && Yii::app()->getModule('store')->roundPrice){
            $price = round((float)$price, 0);
        }

        $price = Product::roundPrice($price);
	    return $price;
    }

	public function getResultPagePrice(){
		if($this->getProduct()){
			$product = $this->getProduct();
		} else {
			$product = $this->product;
		}

		$hasDiscount = false;
		if($this->type == self::TYPE_BASE_PRICE && $this->hasDiscount()){
			$hasDiscount = true;
			$price = $this->discount_price;
		} else {
			switch ($this->type){
				case self::TYPE_SUM:
					$price = $product->getBasePrice(false) + $this->amount;
					break;

				case self::TYPE_REDUCE:
					$price = $product->getBasePrice(false) - $this->amount;
					break;
				default:
					$price = $this->getDiscountPriceClear();
			}
		}


		if(!$hasDiscount && (!$product || !$product->getDiscountValue()) && Yii::app()->getModule('store')->roundPrice){
			$price = round((float)$price, 0);
		}

		$price = Product::roundPrice($price);
		return $price;
	}

    /**
     * @return float
     */
    public function getDiscountPrice()
    {
		$module = Yii::app()->getModule('store');
        if($module->priceType == StoreModule::PRICE_TYPE_CHARGE){
	        $price = $this->getDiscountPriceClear();

			if((float) $this->purchase_price == 0){
				return $price;
			}
		} else {
	        $price = $this->discount_price;
        }

        if(!$this->hasDiscount() && $module->roundPrice){
            $price = Product::roundPrice($price);
        }
        return $price;
    }

    /**
     * @return float
     */
    private function getDiscountPriceClear(){
	    if(Yii::app()->getModule('store')->priceType == StoreModule::PRICE_TYPE_CHARGE) {
		    if ($this->hasDiscount()) {
			    $price = (float) $this->getBasePrice(false) * (1 - ((float) $this->getDiscountValue() ? : 0) / 100);
		    } elseif ($this->sku == $this->getProduct()->sku && $this->getProduct()->hasDiscount()) {
			    $price = (float) $this->getBasePrice(false) * (1 - ((float) $this->getProduct()->getDiscountValue() ? : 0) / 100);
		    } else {
			    $price = (float) $this->getBasePrice(false);
		    }
	    } else {
		    if ($this->hasDiscount()) {
		    	$price = $this->getDiscountPrice();
		    } else {
			    $price = (float) $this->getBasePrice(false);
		    }
	    }
        return $price;
    }

    /**
     * @return bool
     */
    public function hasDiscount()
    {
        if ($this->getDiscountValue() > 0) {
            return true;
        }

        return false;
    }

    /**
     * @return integer
     */
    public function getDiscountValue(){
	    $discount = $this->discount;
	    if(!IS_BACKEND && !Yii::app()->getUser()->getIsGuest()){
		    $userDiscount = Yii::app()->getUser()->getDiscount();
		    if(is_numeric($userDiscount) && $userDiscount > 0){
			    $discount = $userDiscount;
		    }
	    }

	    if(Yii::app()->getModule('store')->priceType == StoreModule::PRICE_TYPE_CHARGE){
		    if($this->discount > 0) {
			    if($this->discount > $discount){
				    $discount = $this->discount;
			    }
		    }
	    } elseif ($this->discount_price > 0){
		    $temp = 100 - ((float) $this->discount_price * 100 / $this->amount);
		    if($temp > $discount){
			    $discount = round($temp);
		    }
	    }

	    return $discount;
    }

    /**
     *
     */
    public function getDiscount(){
	    if($this->getDiscountValue() <= 0){
		    return false;
	    }

        return number_format($this->getDiscountValue(), 0, '.', '');
    }

    protected function beforeSave()
    {
    	if(Yii::app()->getModule('store')->priceType == StoreModule::PRICE_TYPE_CHARGE){
			if($this->purchase_price > 0 && $this->getExtraCharge() > 0){
				$this->amount = null;
				$this->amount = $this->getBasePrice();
			}

		    if(!$this->discount){
			    $this->discount = new CDbExpression('NULL');
		    }
		}

        return parent::beforeSave();
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product){
        $this->product = $product;
    }

    /**
     * @return null|Product
     */
    public function getProduct(){
    	$product = Product::getFromRepository($this->product_id);
    	if($product instanceof Product){
    		return $product;
	    }

	    return $this->product;
    }

	/**
	 * @param null $price
	 * @return string
	 */
	public function getFormatPrice($price = null){
		if($price === null){
			$price = $this->getResultPrice();
		}
		return ProductHelper::getFormatPrice($price);
	}

	/**
	 *
	 */
	public function getStoreAttribute(){
		$dataContainer = Yii::app()->getComponent('dataContainer');
		$key = 'variantsAttribute__'.$this->attribute_id;

		if($dataContainer->has($key)){
			return $dataContainer->get($key);
		}
		$attributes = $this->attribute;

		$dataContainer->set($key, $attributes);

		return $attributes;
	}
}
