<?php

/**
 * @property string $id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property string $description_bottom
 * @property string $title
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property integer $status
 * @property integer $category_id
 * @property integer $sort
 * @property integer $show_after_filter
 * @property string $options
 * @property string $use_category_url
 *
 * @property StoreCategory $category
 *
 * @method ProductGroups published
 *
 */
class ProductGroups extends \webforma\models\WModel
{
    /**
     *
     */
    const STATUS_DRAFT = 0;
    /**
     *
     */
    const STATUS_PUBLISHED = 1;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{store_product_groups}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className
     * @return StoreCategory the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            [
                'name, title, description, description_bottom, slug, meta_title, meta_keywords, meta_description, category_id',
                'filter',
                'filter' => 'trim',
            ],
            ['name, slug', 'required'],//, category_id
            ['status, sort', 'numerical', 'integerOnly' => true],
            ['status, category_id, show_after_filter', 'length', 'max' => 11],
            ['status, use_category_url', 'numerical', 'integerOnly' => true],
            ['name, title, meta_title, meta_keywords, meta_description', 'length', 'max' => 250],
            ['slug', 'length', 'max' => 150],
            [
                'slug',
                'webforma\components\validators\YSLugValidator',
                'message' => Yii::t('StoreModule.store', 'Bad characters in {attribute} field'),
            ],
            ['slug', 'unique'],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            ['id, name, description, sort, description_bottom, slug, status, category_id', 'safe', 'on' => 'search'],
        ];
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'category' => [self::BELONGS_TO, 'StoreCategory', ['category_id' => 'id']],
		];
	}

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'sortable' => [
                'class' => 'webforma\components\behaviors\SortableBehavior',
                'attributeName' => 'sort',
            ],
        ];
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'published' => [
                'condition' => 't.status = :status',
                'params' => [':status' => self::STATUS_PUBLISHED],
            ],
        ];
    }

	protected function beforeSave()
	{
		$request = Yii::app()->getRequest();

		$price = $request->getPost('price');
		if(!$price['from']){
			unset($price['from']);
		}
		if(!$price['to']){
			unset($price['to']);
		}

		$brand = $request->getPost('brand');
		if($brand[0] == 0){
			unset($brand[0]);
		}

		$attributes = [];
		foreach ($request->getPost('attributes', []) as $key => $attr){
			if($attr[0] == 0){
				continue;
			}
			$attributes[$key] = $attr;
		}

		$ids = [];
		$idsPost = $request->getPost('ids', '');
		if($idsPost){
			foreach (explode(',', $idsPost) as $_id){
				$_id = trim($_id);
				if(is_numeric($_id)){
					$ids[] = $_id;
				}
			}
		}

		$this->options = json_encode([
			'price' => (array) $price,
			'name' => $request->getPost('name'),
			'brand' => (array) $brand,
			'attributes' => (array) $attributes,
			'ids' => !empty($ids) ? implode(',', $ids) : '',
		]);
		return parent::beforeSave();
	}

	protected function afterFind()
	{
		parent::afterFind();
		$this->options = json_decode($this->options);
	}


	/**
     *
     */
    protected function afterSave()
    {
	    Yii::app()->cache->delete(StoreUrlRule::CACHE_GROUPS_KEY);
	    SitemapHelper::remove();

        return parent::afterSave();
    }

    /**
     *
     */
    public function afterDelete()
    {
	    Yii::app()->cache->delete(StoreUrlRule::CACHE_GROUPS_KEY);
	    SitemapHelper::remove();

        parent::afterDelete();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('StoreModule.store', 'Id'),
            'category_id' => Yii::t('StoreModule.store', 'Category'),
            'name' => Yii::t('StoreModule.store', 'Name'),
            'description' => Yii::t('StoreModule.store', 'Description'),
	        'description_bottom' => 'Описание под списком товаров',
	        'slug' => Yii::t('StoreModule.store', 'Alias'),
            'meta_title' => Yii::t('StoreModule.store', 'Meta title'),
            'meta_keywords' => Yii::t('StoreModule.store', 'Meta keywords'),
            'meta_description' => Yii::t('StoreModule.store', 'Meta description'),
            'status' => Yii::t('StoreModule.store', 'Status'),
            'sort' => Yii::t('StoreModule.store', 'Order'),
            'title' => Yii::t('StoreModule.store', 'SEO_Title'),
            'show_after_filter' => 'Показывать на странице категории',
            'use_category_url' => 'Использовать как продолжение URL адреса базовой категории',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('slug', $this->slug, true);
        $criteria->compare('meta_title', $this->meta_title, true);
        $criteria->compare('meta_keywords', $this->meta_keywords, true);
        $criteria->compare('meta_description', $this->meta_description, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('sort', $this->sort);

        return new CActiveDataProvider(
            ProductGroups::_CLASS_(),
            [
                'criteria' => $criteria,
                'sort' => ['defaultOrder' => 't.sort'],
            ]
        );
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_DRAFT => Yii::t('StoreModule.store', 'Draft'),
            self::STATUS_PUBLISHED => Yii::t('StoreModule.store', 'Published'),
        ];
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('StoreModule.store', '*unknown*');
    }

    /**
     * @return string
     */
    public function getParentName()
    {
        return $this->parent ? $this->parent->name : '---';
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title ?: $this->name;
    }

    /**
     * @return string
     */
    public function getMetaTitle()
    {
        if($this->meta_title){
            return $this->meta_title;
        }

	    if($template = Yii::app()->getModule('store')->metaProductGroupTitleTemplate){
		    return Helper::mb_strtr($template, ['{name}' => $this->name]);
	    }

        return Helper::prepareSeoTitle($this->getTitle());
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
        if($this->meta_description){
            return $this->meta_description;
        }
    }

    /**
     * @return string
     */
    public function getMetaKeywords()
    {
        if($this->meta_keywords){
            return $this->meta_keywords;
        }
    }

    /**
     * Get canonical url
     *
     * @return string
     */
    public function getMetaCanonical()
    {
        list($address, $params) = explode('?', Yii::app()->getRequest()->getUrl());

        if($params){
            return $address;
        }
    }

	/**
	 * @return mixed
	 */
    public function getIsShowAfterFilters(){
    	$data = $this->getYesNoList();
    	return isset($data[$this->show_after_filter]) ? $data[$this->show_after_filter] : $data[0];
    }

	/**
	 * @return string
	 */
    public function getUrl(){
		if($this->use_category_url && $this->category_id){
			$url = Yii::app()->createUrl('store/category/view', ['path' => $this->category->getPath().'/'.$this->slug]);
		} else {
			$url = Yii::app()->createUrl('store/productGroups/view', ['slug' => $this->slug]);
		}

		return $url;
	}
}
