<?php

/**
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property integer $status
 * @property string $description
 * @property string $description_bottom
 * @property string $h1
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property Producer[] $producers
 * @property Producer[] $producersActive
 *
 * @method getImageUrl
 */
class ProducerCountry extends webforma\models\WModel
{
    /**
     *
     */
    const STATUS_ZERO = 0;
    /**
     *
     */
    const STATUS_ACTIVE = 1;
    /**
     *
     */
    const STATUS_NOT_ACTIVE = 2;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{store_producer_country}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['name, slug, status', 'required'],
            ['name, slug, h1, description, description_bottom', 'filter', 'filter' => 'trim'],
            ['name, meta_title, meta_keywords, meta_description', 'length', 'max' => 250],
            ['description, description_bottom', 'safe'],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            [
                'slug',
                'webforma\components\validators\YSLugValidator',
                'message' => Yii::t('StoreModule.store', 'Illegal characters in {attribute}'),
            ],
            ['slug', 'unique'],
            [
                'id, name, slug, status, country, description, description_bottom, meta_title, meta_keywords, meta_description',
                'safe',
                'on' => 'search',
            ],
        ];
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'published' => [
                'condition' => 't.status = :status',
                'params' => [':status' => self::STATUS_ACTIVE],
                'order' => 'name ASC',
            ],
        ];
    }

    /**
     * @return array
     */
    public function relations()
    {
        return [
            'producerCount' => [self::STAT, 'Producer', 'country_id'],
	        'producers' => [self::HAS_MANY, 'Producer', 'country_id'],
	        'producersActive' => [self::HAS_MANY, 'Producer', 'country_id', 'condition' => 'producersActive.status = '.Product::STATUS_ACTIVE],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('StoreModule.store', 'ID'),
            'name' => Yii::t('StoreModule.store', 'Title'),
            'slug' => Yii::t('StoreModule.store', 'URL'),
            'status' => 'Статус',
            'h1' => 'Заголовок H1',
            'description' => Yii::t('StoreModule.store', 'Description'),
	        'description_bottom' => 'Описание под списком брендов',
	        'meta_title' => Yii::t('StoreModule.store', 'Meta title'),
            'meta_keywords' => Yii::t('StoreModule.store', 'Meta keywords'),
            'meta_description' => Yii::t('StoreModule.store', 'Meta description'),
        ];
    }


    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('slug', $this->slug, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('description_bottom', $this->description, true);

        return new CActiveDataProvider(
            $this, [
                'criteria' => $criteria,
                'sort' => ['defaultOrder' => 'name'],
            ]
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     * @return Producer the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('StoreModule.store', 'Active'),
            self::STATUS_NOT_ACTIVE => Yii::t('StoreModule.store', 'Not active'),
        ];
    }

    /**
     * @return string
     */
    public function getStatusTitle()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('StoreModule.store', '*unknown*');
    }

    /**
     * @return array
     */
    public function getFormattedList()
    {
        return CHtml::listData(ProducerLine::model()->findAll(), 'id', 'name');
    }

	/**
	 *
	 */
	protected function afterSave()
	{
		Yii::app()->cache->delete('countries');
		Yii::app()->cache->delete(StoreUrlRule::CACHE_COUNTRY_KEY);
		parent::afterSave();
	}

	/**
	 * @return bool
	 */
	protected function beforeDelete()
	{
		Yii::app()->cache->delete('countries');
		Yii::app()->cache->delete(StoreUrlRule::CACHE_COUNTRY_KEY);

		return parent::beforeDelete();
	}

	/**
	 * @return array
	 */
	public function getProducersAsArray(){
    	$data = [];
		foreach ($this->producers as $producer) {
			if($producer->status != Producer::STATUS_ACTIVE){
				continue;
			}
			$data[] = $producer->id;
    	}
    	return $data;
	}

	/**
     * @return string
     */
    public function getMetaTitle()
    {
        if($this->meta_title){
            return $this->meta_title;
        }

        return Helper::prepareSeoTitle($this->getName());
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
        if($this->meta_description){
            return $this->meta_description;
        }
    }

    /**
     * @return string
     */
    public function getMetaKeywords()
    {
        if($this->meta_keywords){
            return $this->meta_keywords;
        }
    }

    /**
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * @return string
     */
    public function getH1(){
	    if($this->h1){
		    return $this->h1;
	    }

	    return $this->getName();
    }

	/**
	 * Get canonical url
	 *
	 * @return string
	 */
	public function getMetaCanonical()
	{
		list($address, $params) = explode('?', Yii::app()->getRequest()->getUrl());

		if($params){
			return $address;
		}
	}

}
