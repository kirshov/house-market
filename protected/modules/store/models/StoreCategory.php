<?php
use webforma\components\Event;
use webforma\widgets\WPurifier;

/**
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string $short_description
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $slug
 * @property integer $status
 * @property integer $parent_id
 * @property integer $sort
 * @property integer $product_type
 * @property string $external_id
 * @property string $title
 * @property string $meta_canonical
 * @property string $image_alt
 * @property string $image_title
 * @property string $view
 * @property string $groups_name
 * @property string $groups_h1
 * @property string $groups_meta_title
 * @property string $groups_meta_description
 * @property string $groups_meta_keywords
 * @property string $groups_alias
 * @property integer $not_filter
 * @property integer $on_main
 * @property integer $sort_main
 *
 * @property-read StoreCategory $parent
 * @property-read StoreCategory[] $children
 *
 * @method StoreCategory published
 * @method StoreCategory child
 * @method StoreCategory roots
 * @method getImageUrl
 * @method string getPath
 *
 */
class StoreCategory extends \webforma\models\WModel
{
    /**
     *
     */
    const STATUS_DRAFT = 0;
    /**
     *
     */
    const STATUS_PUBLISHED = 1;

	/**
	 * @var
	 */
    private $activeCategories;

	/**
	 * @var
	 */
    private $activeObjectCategories;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{store_category}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className
     * @return StoreCategory the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            [
                'name, title, description, short_description, slug, meta_title, meta_keywords, meta_description,
                groups_h1, groups_meta_title, groups_meta_description, groups_meta_keywords, groups_alias, groups_name, 
                not_filter',
                'filter',
                'filter' => 'trim',
            ],
            ['name, slug', 'filter', 'filter' => [$obj = new WPurifier(), 'purify']],
            ['name, slug', 'required'],
            ['parent_id, status, sort, on_main, sort_main', 'numerical', 'integerOnly' => true],
            ['parent_id, status', 'length', 'max' => 11],
            ['parent_id', 'default', 'setOnEmpty' => true, 'value' => null],
            ['status, product_type', 'numerical', 'integerOnly' => true],
            ['status', 'length', 'max' => 11],
            ['name, title, image, image_alt, image_title, meta_title, meta_keywords, meta_description, meta_canonical', 'length', 'max' => 250],
            ['groups_h1, groups_meta_title, groups_meta_description, groups_meta_keywords', 'length', 'max' => 250],
            ['slug', 'length', 'max' => 150],
            ['external_id, view', 'length', 'max' => 100],
            [
                'slug',
                'webforma\components\validators\YSLugValidator',
                'message' => Yii::t('StoreModule.store', 'Bad characters in {attribute} field'),
            ],
            ['slug', 'unique'],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            ['meta_canonical', 'url'],
            ['id, parent_id, name, description, sort, short_description, slug, status, product_type', 'safe', 'on' => 'search'],
        ];
    }

	protected function afterValidate()
	{
		if($this->groups_alias) {
			$slugValidator = new webforma\components\validators\YSLugValidator();
			$slugValidator->validateAttribute($this, 'groups_alias');

			$uniqueValidator = CValidator::createValidator('unique', $this,['groups_alias']);
			$uniqueValidator->validate($this);
		}
		parent::afterValidate();
	}


	/**
     * @return array
     */
    public function behaviors()
    {
        $module = Yii::app()->getModule('store');

        return [
            'imageUpload' => [
                'class' => 'webforma\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'image',
                'uploadPath' => $module !== null ? $module->uploadPath.'/category' : null,
            ],
            'tree' => [
                'class' => 'webforma\components\behaviors\DCategoryTreeBehavior',
                'aliasAttribute' => 'slug',
                'requestPathAttribute' => 'path',
                'parentAttribute' => 'parent_id',
                'parentRelation' => 'parentCategory',
                //'parentRelation' => 'parent',
                //'statAttribute' => 'productCount',
                'defaultCriteria' => [
                    'order' => 't.sort',
                    //'with' => 'productCount',
                ],
                'titleAttribute' => 'name',
                /*'iconAttribute' => function(StoreCategory $item){
                    return $item->getImageUrl(150, 150);
                },
                'iconAltAttribute' => function(StoreCategory $item){
                    return $item->getImageAlt();
                },
                'iconTitleAttribute' => function(StoreCategory $item){
                    return $item->getImageTitle();
                },*/
                'useCache' => true,
            ],
            'sortable' => [
                'class' => 'webforma\components\behaviors\SortableBehavior',
                'attributeName' => 'sort',
            ],
        ];
    }

    /**
     * @return array
     */
    public function relations()
    {
        return [
            'parent' => [self::BELONGS_TO, 'StoreCategory', 'parent_id'],
            'children' => [self::HAS_MANY, 'StoreCategory', 'parent_id'],
            'productCount' => [self::STAT, 'Product', 'category_id'],
        ];
    }

	/**
	 * @return $this
	 */
	public function publishedAll()
	{
		$categoryIds = Yii::app()->getComponent('categoryRepository')->getActiveCategories();
		$this->getDbCriteria()->mergeWith(array(
			'condition' => 't.status = :status AND t.id IN ('.implode(', ', $categoryIds).')',
			'params' => [':status' => self::STATUS_PUBLISHED],
		));
		return $this;
	}

    /**
     * @return array
     */
    public function scopes()
    {
        //$categoryIds = Yii::app()->getComponent('categoryRepository')->getActiveCategories();
        return [
            'published' => [
                //'condition' => 't.status = :status AND t.id IN ('.implode(', ', $categoryIds).')',
                'condition' => 't.status = :status',
                'params' => [':status' => self::STATUS_PUBLISHED],
            ],
            'roots' => [
                'condition' => 't.parent_id IS NULL',
            ],
            'child' => [
                'condition' => 't.parent_id = :id',
                'params' => [':id' => $this->id],
            ]
        ];
    }

    protected function afterFind()
    {
        parent::afterFind();
        //$this->_oldData['path'] = $this->getPath();
        $this->_oldData['status'] = $this->status;
    }

    /**
     *
     */
    protected function afterSave()
    {
        //Yii::app()->eventManager->fire(StoreEvents::CATEGORY_AFTER_SAVE, new Event($this));
        Yii::app()->eventManager->fire(StoreEvents::STORE_CHANGE, new Event($this));

        return parent::afterSave();
    }

    /**
     *
     */
    public function afterDelete()
    {
        //Yii::app()->eventManager->fire(StoreEvents::CATEGORY_AFTER_DELETE, new Event($this));
        Yii::app()->eventManager->fire(StoreEvents::STORE_CHANGE, new Event($this));

        parent::afterDelete();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('StoreModule.store', 'Id'),
            'parent_id' => Yii::t('StoreModule.store', 'Parent'),
            'name' => Yii::t('StoreModule.store', 'Name'),
            'image' => Yii::t('StoreModule.store', 'Image'),
            'short_description' => Yii::t('StoreModule.store', 'Short description'),
            'description' => 'Описание над списком товаров',
            'slug' => Yii::t('StoreModule.store', 'Alias'),
            'meta_title' => Yii::t('StoreModule.store', 'Meta title'),
            'meta_keywords' => Yii::t('StoreModule.store', 'Meta keywords'),
            'meta_description' => Yii::t('StoreModule.store', 'Meta description'),
            'status' => Yii::t('StoreModule.store', 'Status'),
            'sort' => Yii::t('StoreModule.store', 'Order'),
            'external_id' => Yii::t('StoreModule.store', 'External id'),
            'title' => Yii::t('StoreModule.store', 'SEO_Title'),
            'meta_canonical' => Yii::t('StoreModule.store', 'Canonical'),
            'image_alt' => Yii::t('StoreModule.store', 'Image alt'),
            'image_title' => Yii::t('StoreModule.store', 'Image title'),
            'view' => Yii::t('StoreModule.store', 'Template'),
            'product_type' => 'Тип товаров',
	        'groups_name' => 'Название в группах',
	        'groups_h1' => 'Заголовок H1',
	        'groups_meta_title' => 'Заголовок страницы',
	        'groups_meta_description' => 'Meta description',
	        'groups_meta_keywords' => 'Meta keywords',
	        'groups_alias' => 'URL в группах товаров',
	        'not_filter' => 'Не показывать блок фильтров в этой категории',
	        'on_main' => 'Показывать на главной',
	        'sort_main' => 'Сортировка на главной',
        ];
    }

    /**
     * @return array customized attribute descriptions (name=>description)
     */
    public function attributeDescriptions()
    {
        return [
            'id' => Yii::t('StoreModule.store', 'Id'),
            'parent_id' => Yii::t('StoreModule.store', 'Parent'),
            'name' => Yii::t('StoreModule.store', 'Title'),
            'image' => Yii::t('StoreModule.store', 'Image'),
            'short_description' => Yii::t('StoreModule.store', 'Short description'),
            'description' => Yii::t('StoreModule.store', 'Description'),
            'slug' => Yii::t('StoreModule.store', 'Alias'),
            'meta_title' => Yii::t('StoreModule.store', 'Meta title'),
            'meta_keywords' => Yii::t('StoreModule.store', 'Meta keywords'),
            'meta_description' => Yii::t('StoreModule.store', 'Meta description'),
            'status' => Yii::t('StoreModule.store', 'Status'),
            'sort' => Yii::t('StoreModule.store', 'Order'),
            'title' => Yii::t('StoreModule.store', 'SEO_Title'),
            'meta_canonical' => Yii::t('StoreModule.store', 'Canonical'),
            'image_alt' => Yii::t('StoreModule.store', 'Image alt'),
            'image_title' => Yii::t('StoreModule.store', 'Image title'),
            'view' => Yii::t('StoreModule.store', 'Template'),
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        if(is_numeric($this->parent_id)){
            if($this->parent_id > 0){
                $criteria->compare('parent_id', $this->parent_id);
            } else {
                $criteria->addCondition('parent_id IS NULL');
            }
        }

        $criteria->compare('name', $this->name, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('slug', $this->slug, true);
        $criteria->compare('meta_title', $this->meta_title, true);
        $criteria->compare('meta_keywords', $this->meta_keywords, true);
        $criteria->compare('meta_description', $this->meta_description, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('sort', $this->sort);

        return new CActiveDataProvider(
            StoreCategory::_CLASS_(),
            [
                'criteria' => $criteria,
                'sort' => ['defaultOrder' => 't.sort'],
            ]
        );
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_DRAFT => Yii::t('StoreModule.store', 'Draft'),
            self::STATUS_PUBLISHED => Yii::t('StoreModule.store', 'Published'),
        ];
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('StoreModule.store', '*unknown*');
    }

    /**
     * @return string
     */
    public function getParentName()
    {
        return $this->parent ? $this->parent->name : '---';
    }

    /*public function __get($name)
    {
        if($name == 'parent'){
            return $this->getParent();
        }

        return parent::__get($name);
    }


    /**
     * @return StoreCategory|null
     * /
    public function getParent(){
    	return $this->parent;
    }*/

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title ?: $this->name;
    }

    /**
     * @return string
     */
    public function getMetaTitle()
    {
        if($this->meta_title){
            return $this->meta_title;
        }

        if($template = Yii::app()->getModule('store')->metaCategoryTitle){
            return Helper::mb_strtr($template, ['{name}' => $this->getTitle()]);
        }

        return Helper::prepareSeoTitle($this->name);
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
        if($this->meta_description){
            return $this->meta_description;
        }

        if($template = Yii::app()->getModule('store')->metaCategoryDescription){
            return Helper::mb_strtr($template, ['{name}' => $this->getTitle()]);
        }
    }

    /**
     * @return string
     */
    public function getMetaKeywords()
    {
        if($this->meta_keywords){
            return $this->meta_keywords;
        }

        if($template = Yii::app()->getModule('store')->metaCategoryKeyWords){
            return Helper::mb_strtr($template, ['{name}' => $this->getTitle()]);
        }
    }

    /**
     * Get canonical url
     *
     * @return string
     */
    public function getMetaCanonical()
    {
    	if($this->meta_canonical){
		    return $this->meta_canonical;
	    } else {
    		list($address, $params) = explode('?', Yii::app()->getRequest()->getUrl());

    		if($params){
    			return $address;
		    }
	    }

    }

    /**
     * Get image alt tag text
     *
     * @return string
     */
    public function getImageAlt()
    {
        return $this->image_alt ?: $this->getTitle();
    }

    /**
     * Get image title tag text
     *
     * @return string
     */
    public function getImageTitle()
    {
        return $this->image_title ?: $this->getTitle();
    }

	/**
	 * @return string
	 */
	public function getGroupName(){
		return $this->groups_name ? $this->groups_name : $this->name;
	}

	/**
	 * @return string
	 */
	public function getGroupsTitle()
	{
		return $this->groups_h1 ?: 'Группы товаров';
	}

	/**
	 * @return string
	 */
	public function getGroupsMetaTitle()
	{
		if($this->groups_meta_title){
			return $this->groups_meta_title;
		}
		return Helper::prepareSeoTitle($this->getGroupsTitle());
	}

	/**
	 * @return string
	 */
	public function getGroupsMetaDescription()
	{
		return $this->groups_meta_description;
	}

	/**
	 * @return string
	 */
	public function getGroupsMetaKeywords()
	{
		return $this->groups_meta_keywords;
	}

    /**
     * @param $id
     * @return StoreCategory|null
     */
	public static function getById($id){
        return Yii::app()->getComponent('categoryRepository')->getById($id);
    }

    /**
     * @return static[]
     */
    public function getActiveChildren(){

        return self::model()->published()->cache(Yii::app()->getModule('webforma')->coreCacheTime)->findAll([
            'condition' => 'parent_id = :pid',
            'params' => [
                ':pid' => $this->id,
            ],
            'order' => 'sort'
        ]);
    }

	/**
	 * @param int $limit
	 * @return StoreCategory[]
	 */
	public static function getMainCats($limit = 4){
		return self::model()->findAll([
			'condition' => 'on_main = 1',
			'order' => 't.sort_main, t.sort',
			'limit' => $limit,
		]);
	}

	/**
	 * @return StoreCategory
	 */
	public function getParentCategory(){
		if(!$this->parent_id){
			return null;
		}

		$dataContainer = Yii::app()->getComponent('dataContainer');
		if(!$dataContainer->has('loadStoreCategory')){
			$cats = self::model()->publishedAll()->findAll();
			foreach ($cats as $item){
				$cacheKeyItem = 'models::StoreCategory::'.$item->id;
				$dataContainer->set($cacheKeyItem, $item);
			}
			$dataContainer->set('loadStoreCategory', true);
		}

		$cacheKey = 'models::'.get_class($this).'::'.$this->parent_id;

		if($dataContainer->has($cacheKey)){
			return $dataContainer->get($cacheKey);
		}

		$parent = $this->parent;

		$dataContainer->set($cacheKey, $parent);
		return $parent;
	}
}
