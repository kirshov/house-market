<?php

/**
 * @property integer $id
 * @property string $name
 * @property integer $status
 * @property integer $sort
 * @property string $description
 * @property integer $extra_charge
 * @property datetime $create_time
 * @property datetime $update_time
 */
class Provider extends webforma\models\WModel
{
    /**
     *
     */
    const STATUS_ACTIVE = 1;
    /**
     *
     */
    const STATUS_NOT_ACTIVE = 2;

	/**
	 * @var int
	 */
    public $oldExtraCharge;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{store_provider}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['name, status', 'required'],
            ['name, description, extra_charge', 'filter', 'filter' => 'trim'],
            ['sort', 'numerical', 'integerOnly' => true],
            ['extra_charge', 'numerical', 'integerOnly' => true],
            ['name', 'length', 'max' => 250],
            ['extra_charge', 'numerical'],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            [
                'id, name, status, sort, description',
                'safe',
                'on' => 'search',
            ],
        ];
    }

	/**
	 * @return array
	 */
	public function relations()
	{
		return [
			'productCount' => [self::STAT, 'Product', 'provider_id'],
		];
	}

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('StoreModule.store', 'ID'),
            'name' => Yii::t('StoreModule.store', 'Title'),
            'status' => Yii::t('StoreModule.store', 'Status'),
            'sort' => Yii::t('StoreModule.store', 'Order'),
            'description' => Yii::t('StoreModule.store', 'Description'),
	        'extra_charge' => Yii::t('StoreModule.store', 'Extra Charge'),
        ];
    }


    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('sort', $this->sort);
        $criteria->compare('description', $this->description, true);

        return new CActiveDataProvider(
            $this, [
                'criteria' => $criteria,
                'sort' => ['defaultOrder' => 'sort'],
            ]
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     * @return Producer the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'sortable' => [
                'class' => 'webforma\components\behaviors\SortableBehavior',
                'attributeName' => 'sort',
            ],
        ];
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('StoreModule.store', 'Active'),
            self::STATUS_NOT_ACTIVE => Yii::t('StoreModule.store', 'Not active'),
        ];
    }

    /**
     * @return string
     */
    public function getStatusTitle()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('StoreModule.store', '*unknown*');
    }

    /**
     * @return array
     */
    public function getFormattedList()
    {
		return CHtml::listData(self::model()->cache(0)->findAll(), 'id', 'name');
    }

	protected function afterFind()
	{
		parent::afterFind();
		$this->oldExtraCharge = $this->extra_charge;
	}

	protected function afterSave()
	{
		parent::afterSave();

        Yii::app()->eventManager->fire(StoreEvents::STORE_CHANGE, new \webforma\components\Event($this));

		if(Yii::app()->getModule('store')->priceType == StoreModule::PRICE_TYPE_CHARGE && $this->extra_charge != $this->oldExtraCharge){
			$criteria = new CDbCriteria();
			$criteria->condition = 'provider_id = :provider_id AND purchase_price > 0 AND extra_charge IS NULL';
			$criteria->params = [
				':provider_id' => $this->id,
			];
			$expression = new CDbExpression('`purchase_price` + (`purchase_price` * '.$this->extra_charge.' / 100)');

			Product::model()->updateAll(['price' => $expression], $criteria);

            $variantCriteria = new CDbCriteria();
            $variantCriteria->condition = 'p.provider_id = :provider_id AND {{store_product_variant}}.purchase_price > 0 AND {{store_product_variant}}.extra_charge IS NULL';
            $variantCriteria->join = 'INNER JOIN {{store_product}} p ON {{store_product_variant}}.product_id = p.id';
            $variantCriteria->params = [
                ':provider_id' => $this->id,
            ];
            $expression = new CDbExpression('ROUND(IF(`{{store_product_variant}}`.`purchase_price` > 0, `{{store_product_variant}}`.`purchase_price`, `p`.`purchase_price`) + (IF(`{{store_product_variant}}`.`purchase_price` > 0, `{{store_product_variant}}`.`purchase_price`, `p`.`purchase_price`) * '.$this->extra_charge.' / 100), 0)');

            ProductVariant::model()->updateAll(['amount' => $expression], $variantCriteria);
		}
	}

    protected function afterDelete()
    {
        parent::afterDelete();
        Yii::app()->eventManager->fire(StoreEvents::STORE_CHANGE, new Event($this));
    }
}
