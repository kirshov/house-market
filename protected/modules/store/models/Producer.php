<?php

/**
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $slug
 * @property integer $status
 * @property integer $sort
 * @property integer $country_id
 * @property string $image
 * @property string $short_description
 * @property string $description
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $view
 * @property string $search_name
 * @property string $retargeting_code
 * @property ProducerCountry $country
 * @property ProducerLine[] $line
 *
 * @method getImageUrl
 * @method Producer published
 */
class Producer extends webforma\models\WModel
{
    /**
     *
     */
    const STATUS_ZERO = 0;
    /**
     *
     */
    const STATUS_ACTIVE = 1;
    /**
     *
     */
    const STATUS_NOT_ACTIVE = 2;

    private $country;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{store_producer}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['name, slug, status', 'required'],
            ['name, slug, search_name, short_description, description, retargeting_code', 'filter', 'filter' => 'trim'],
            ['sort, country_id', 'numerical', 'integerOnly' => true],
            ['view', 'length', 'max' => 150],
            ['name, image, title, meta_title, meta_keywords, meta_description', 'length', 'max' => 250],
            ['short_description, description', 'safe'],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            [
                'slug',
                'webforma\components\validators\YSLugValidator',
                'message' => Yii::t('StoreModule.store', 'Illegal characters in {attribute}'),
            ],
            ['slug', 'unique'],
            [
                'id, name, slug, status, sort, image, country_id, short_description, description, meta_title, meta_keywords, meta_description',
                'safe',
                'on' => 'search',
            ],
        ];
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'published' => [
                'condition' => 't.status = :status',
                'params' => [':status' => self::STATUS_ACTIVE],
                'order' => 't.sort ASC',
            ],
        ];
    }

    /**
     * @return array
     */
    public function relations()
    {
        return [
            //'country' => [self::BELONGS_TO, 'ProducerCountry', 'country_id'],
            'productCount' => [self::STAT, 'Product', 'producer_id'],
            'line' => [self::HAS_MANY, 'ProducerLine', 'producer_id', 'on' => 'line.status = '.self::STATUS_ACTIVE, 'joinType'=>'LEFT JOIN', 'order' => 'line.sort'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('StoreModule.store', 'ID'),
            'name_short' => Yii::t('StoreModule.store', 'Short title'),
            'name' => Yii::t('StoreModule.store', 'Title'),
            'slug' => Yii::t('StoreModule.store', 'URL'),
            'status' => Yii::t('StoreModule.store', 'Status'),
            'sort' => Yii::t('StoreModule.store', 'Order'),
            'image' => Yii::t('StoreModule.store', 'Image'),
            'short_description' => Yii::t('StoreModule.store', 'Short description'),
            'description' => Yii::t('StoreModule.store', 'Description'),
            'meta_title' => Yii::t('StoreModule.store', 'Meta title'),
            'meta_keywords' => Yii::t('StoreModule.store', 'Meta keywords'),
            'meta_description' => Yii::t('StoreModule.store', 'Meta description'),
            'view' => Yii::t('StoreModule.store', 'Template'),
            'country_id' => Yii::t('StoreModule.store', 'Country'),
            'title' => 'Заголовок H1',
            'search_name' => 'Названия для поиска',
            'retargeting_code' => 'Код для ретаргетинга',
        ];
    }


    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name_short', $this->name_short, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('slug', $this->slug, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('sort', $this->sort);
        $criteria->compare('country_id', $this->country_id);
        $criteria->compare('image', $this->image, true);
        $criteria->compare('short_description', $this->short_description, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('search_name', $this->search_name, true);

        return new CActiveDataProvider(
            $this, [
                'criteria' => $criteria,
                'sort' => ['defaultOrder' => 't.sort'],
            ]
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     * @return Producer the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $module = Yii::app()->getModule('store');

        return [
            'imageUpload' => [
                'class' => 'webforma\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'image',
                'uploadPath' => $module !== null ? $module->uploadPath.'/producer' : null,
            ],
            'sortable' => [
                'class' => 'webforma\components\behaviors\SortableBehavior',
                'attributeName' => 'sort',
            ],
        ];
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_ZERO => Yii::t('StoreModule.store', 'Not available'),
            self::STATUS_ACTIVE => Yii::t('StoreModule.store', 'Active'),
            self::STATUS_NOT_ACTIVE => Yii::t('StoreModule.store', 'Not active'),
        ];
    }

    /**
     * @return string
     */
    public function getStatusTitle()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('StoreModule.store', '*unknown*');
    }

    /**
     * @return array
     */
    public function getFormattedList()
    {
        return CHtml::listData(Producer::model()->findAll(['order' => 'name']), 'id', 'name');
    }

	/**
	 *
	 */
	protected function afterSave()
	{
		parent::afterSave();
		\TaggedCache\TaggingCacheHelper::deleteTag('store::producer');
		Yii::app()->cache->delete(StoreUrlRule::CACHE_PRODUCERS_KEY);
	}

	/**
	 *
	 */
    protected function afterDelete()
    {
        parent::afterDelete();

	    \TaggedCache\TaggingCacheHelper::deleteTag('store::producer');
	    Yii::app()->cache->delete(StoreUrlRule::CACHE_PRODUCERS_KEY);
    }

    /**
     * @return string
     */
    public function getMetaTitle()
    {
        if($this->meta_title){
            return $this->meta_title;
        }

        if($template = Yii::app()->getModule('store')->metaProducerItemTitle){
            return Helper::mb_strtr($template, ['{name}' => $this->name]);
        }

        return Helper::prepareSeoTitle($this->name);
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
        if($this->meta_description){
            return $this->meta_description;
        }

        if($template = Yii::app()->getModule('store')->metaProducerItemDescription){
            return Helper::mb_strtr($template, ['{name}' => $this->name]);
        }
    }

    /**
     * @return string
     */
    public function getMetaKeywords()
    {
        if($this->meta_keywords){
            return $this->meta_keywords;
        }

        if($template = Yii::app()->getModule('store')->metaProducerItemKeyWords){
            return Helper::mb_strtr($template, ['{name}' => $this->name]);
        }
    }

    /**
     * @return string
     */
    public function getFullName(){
        return $this->name.
            ($this->country_id ? ' ('.$this->getCountry().')' : '');
    }

    /**
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * @return string
     */
    public function getH1(){
    	if($this->title){
    		return $this->title;
	    }

        if($template = Yii::app()->getModule('store')->metaProducerItemH1){
            return Helper::mb_strtr($template, [
                '{name}' => $this->name,
                '{country}' => $this->country,
            ]);
        }

        return $this->getFullName();
    }

    public function getCountry(){
    	if($this->country_id){
    		return Yii::app()->getComponent('producerRepository')->getCountry($this->country_id, 'name');
	    }
    }

	/**
	 * Get canonical url
	 *
	 * @return string
	 */
	public function getMetaCanonical()
	{
		list($address, $params) = explode('?', Yii::app()->getRequest()->getUrl());

		if($params){
			return $address;
		}
	}

}
