<?php

/**
 * @property integer $id
 * @property integer $producer_id
 * @property string $name
 * @property string $slug
 * @property integer $status
 * @property integer $sort
 * @property string $short_description
 * @property string $description
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property Producer $producer
 *
 * @method getImageUrl
 */
class ProducerLine extends webforma\models\WModel
{
    /**
     *
     */
    const STATUS_ZERO = 0;
    /**
     *
     */
    const STATUS_ACTIVE = 1;
    /**
     *
     */
    const STATUS_NOT_ACTIVE = 2;

	/**
	 * @var
	 */
    protected $oldProducer;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{store_producer_line}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['name, producer_id, slug, status', 'required'],
            ['name, slug, short_description, description', 'filter', 'filter' => 'trim'],
            ['sort', 'numerical', 'integerOnly' => true],
            ['name, meta_title, meta_keywords, meta_description', 'length', 'max' => 250],
            ['short_description, description', 'safe'],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            [
                'slug',
                'webforma\components\validators\YSLugValidator',
                'message' => Yii::t('StoreModule.store', 'Illegal characters in {attribute}'),
            ],
            ['slug', 'unique'],
            [
                'id, name, slug, status, sort, country, short_description, description, meta_title, meta_keywords, meta_description',
                'safe',
                'on' => 'search',
            ],
        ];
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'published' => [
                'condition' => 't.status = :status',
                'params' => [':status' => self::STATUS_ACTIVE],
                'order' => 't.sort ASC',
            ],
        ];
    }

    /**
     * @return array
     */
    public function relations()
    {
        return [
            'productCount' => [self::STAT, 'Product', 'producer_line_id'],
            'producer' => [self::BELONGS_TO, 'Producer', 'producer_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('StoreModule.store', 'ID'),
            'name' => Yii::t('StoreModule.store', 'Title'),
            'slug' => Yii::t('StoreModule.store', 'URL'),
            'status' => 'На стр. брендов',
            'sort' => Yii::t('StoreModule.store', 'Order'),
            'short_description' => 'Описание под списком товаров',
            'description' => Yii::t('StoreModule.store', 'Description'),
            'meta_title' => Yii::t('StoreModule.store', 'Meta title'),
            'meta_keywords' => Yii::t('StoreModule.store', 'Meta keywords'),
            'meta_description' => Yii::t('StoreModule.store', 'Meta description'),
            'producer_id' => 'Производитель',
            'image' => 'Изображение',
        ];
    }


    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('slug', $this->slug, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('sort', $this->sort);
        $criteria->compare('producer_id', $this->producer_id);
        $criteria->compare('short_description', $this->short_description, true);
        $criteria->compare('description', $this->description, true);

        return new CActiveDataProvider(
            $this, [
                'criteria' => $criteria,
                'sort' => ['defaultOrder' => 'sort'],
            ]
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     * @return Producer the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $module = Yii::app()->getModule('store');

        return [
	        'imageUpload' => [
		        'class' => 'webforma\components\behaviors\ImageUploadBehavior',
		        'attributeName' => 'image',
		        'uploadPath' => $module !== null ? $module->uploadPath.'/producer_line' : null,
	        ],
            'sortable' => [
                'class' => 'webforma\components\behaviors\SortableBehavior',
                'attributeName' => 'sort',
            ],
        ];
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => 'Показывать',
            self::STATUS_NOT_ACTIVE => 'Не показывать',
        ];
    }

    /**
     * @return string
     */
    public function getStatusTitle()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('StoreModule.store', '*unknown*');
    }

    /**
     * @return array
     */
    public function getFormattedList()
    {
        return CHtml::listData(ProducerLine::model()->findAll(), 'id', 'name');
    }

	/**
	 *
	 */
	protected function afterSave()
	{
		if(!$this->getIsNewRecord() && $this->producer_id != $this->oldProducer){
			Product::model()->updateAll(['producer_line_id' => new CDbExpression('NULL')], 'producer_id = '.$this->oldProducer.' AND producer_line_id = '.$this->id);
		}

		\TaggedCache\TaggingCacheHelper::deleteTag('store::producer::line');
		Yii::app()->cache->delete(StoreUrlRule::CACHE_LINES_KEY);
		parent::afterSave();
	}

	/**
	 * @return bool
	 */
	protected function beforeDelete()
	{
		\TaggedCache\TaggingCacheHelper::deleteTag('store::producer::line');
		Yii::app()->cache->delete(StoreUrlRule::CACHE_LINES_KEY);
		return parent::beforeDelete();
	}


    /**
     * @return string
     */
    public function getMetaTitle()
    {
        if($this->meta_title){
            return $this->meta_title;
        }

        if($template = Yii::app()->getModule('store')->metaProducerLineTitle){
            return Helper::mb_strtr($template, [
                '{name}' => $this->name,
                '{brand}' => $this->getProducer()->getName(),
            ]);
        }

        return Helper::prepareSeoTitle([
            $this->getName(),
            $this->getProducer()->getName(),
        ]);
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
        if($this->meta_description){
            return $this->meta_description;
        }

        if($template = Yii::app()->getModule('store')->metaProducerLineDescription){
            return Helper::mb_strtr($template, [
                '{name}' => $this->name,
                '{brand}' => $this->getProducer()->getName(),
            ]);
        }
    }

    /**
     * @return string
     */
    public function getMetaKeywords()
    {
        if($this->meta_keywords){
            return $this->meta_keywords;
        }

        if($template = Yii::app()->getModule('store')->metaProducerLineKeyWords){
            return Helper::mb_strtr($template, [
                '{name}' => $this->name,
                '{brand}' => $this->getProducer()->getName(),
            ]);
        }
    }

    /**
     * @return string
     */
    public function getFullName(){
        return $this->name . ' ' . $this->getProducer()->getFullName();
    }

    /**
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     *
     */
	protected function afterFind()
	{
		$this->oldProducer = $this->producer_id;
		parent::afterFind();
	}

    /**
     * @return string
     */
    public function getH1(){
        if($template = Yii::app()->getModule('store')->metaProducerLineH1){
            return Helper::mb_strtr($template, [
                '{name}' => $this->name,
                '{brand}' => $this->getProducer()->getName(),
                '{country}' => $this->getProducer()->country,
            ]);
        }

        return $this->getName().' '.$this->getProducer()->getFullName();
    }

	/**
	 * @return Producer
	 */
	public function getProducer(){
		$producer = Yii::app()->getComponent('producerRepository')->getProducer($this->producer_id);
		if($producer instanceof Producer){
			return $producer;
		}

		return $this->producer;
	}
}
