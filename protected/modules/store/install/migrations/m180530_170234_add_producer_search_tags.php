<?php

class m180530_170234_add_producer_search_tags extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_category}}', 'search_name', 'varchar(250) default null');
	}

	public function safeDown()
	{

	}
}