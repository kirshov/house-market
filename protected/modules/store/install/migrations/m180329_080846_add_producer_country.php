<?php

class m180329_080846_add_producer_country extends webforma\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_producer}}', 'country', "char(32) null");
	}

	public function safeDown()
	{

	}
}