<?php

class m180906_181511_add_fields extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn("{{store_product_groups}}", "show_after_filter_group", "tinyint(1) not null default 0");
	}

	public function safeDown()
	{

	}
}