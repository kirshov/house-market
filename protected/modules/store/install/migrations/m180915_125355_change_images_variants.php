<?php

class m180915_125355_change_images_variants extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->dropForeignKey('fk_{{store_product_variant}}_image_id', '{{store_product_variant}}');
		$this->addForeignKey('fk_{{store_product_variant}}_image_id', '{{store_product_variant}}', 'image_id', '{{store_product_image}}', 'id', 'SET NULL', 'CASCADE');
	}

	public function safeDown()
	{

	}
}