<?php

class m210922_182118_add_fields extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_product}}', 'square', 'decimal(19,2) not null default 0');
		$this->addColumn('{{store_product}}', 'square_area', 'decimal(19,2) not null default 0');
		$this->addColumn('{{store_product}}', 'floor_id', 'int(11) DEFAULT null');
		$this->addColumn('{{store_product}}', 'done_id', 'int(11) DEFAULT null');
		$this->addColumn('{{store_product}}', 'place_id', 'int(11) DEFAULT null');
		$this->addColumn('{{store_product}}', 'address', 'varchar(128) DEFAULT null');
		$this->addColumn('{{store_product}}', 'coords', 'varchar(128) DEFAULT null');
		$this->addColumn('{{store_product}}', 'date', 'date');

		$this->createIndex('ux_{{store_product}}_square', '{{store_product}}', 'square');

		$this->addForeignKey('fk_{{store_product}}_floor_id', '{{store_product}}', 'floor_id', '{{dictionary_data}}', 'id', 'SET NULL', 'CASCADE');
		$this->addForeignKey('fk_{{store_product}}_done_id', '{{store_product}}', 'done_id', '{{dictionary_data}}', 'id', 'SET NULL', 'CASCADE');
		$this->addForeignKey('fk_{{store_product}}_place_id', '{{store_product}}', 'place_id', '{{dictionary_data}}', 'id', 'SET NULL', 'CASCADE');

	}

	public function safeDown()
	{

	}
}