<?php

class m190402_165721_category_add_system_name extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_category}}', 'system_name', 'varchar(255) default null');
	}

	public function safeDown()
	{

	}
}