<?php

class m180711_103308_add_not_filter extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_category}}', 'not_filter', 'tinyint(1) default 0');
	}

	public function safeDown()
	{

	}
}