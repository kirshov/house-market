<?php

class m180906_054708_stock_add_fields extends webforma\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_stock}}', 'yandex_type', 'tinyint(1) DEFAULT NULL');
        $this->addColumn('{{store_stock}}', 'text_on_yandex', 'text DEFAULT NULL');
        $this->addColumn('{{store_stock}}', 'condition', 'text DEFAULT NULL');
	}

	public function safeDown()
	{

	}
}