<?php

class m190209_145548_add_product_tabs extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->createTable(
			"{{store_product_tabs}}",
			[
				"id" => "pk",
				"product_id" => "integer default null",
				"key" => "varchar(100) default null",
				"content" => "text default null",
			],
			$this->getOptions()
		);

		$this->createIndex("ix_{{store_product_tabs}}_key", "{{store_product_tabs}}", "product_id, key", true);

		//fk
		$this->addForeignKey("fk_{{store_product_tabs}}_type", "{{store_product_tabs}}", "product_id", "{{store_product}}", "id", "CASCADE", "CASCADE");
	}

	public function safeDown()
	{

	}
}