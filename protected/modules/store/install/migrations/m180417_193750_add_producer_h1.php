<?php

class m180417_193750_add_producer_h1 extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn("{{store_producer}}", "title", "varchar(255) not null default ''");
	}

	public function safeDown()
	{

	}
}