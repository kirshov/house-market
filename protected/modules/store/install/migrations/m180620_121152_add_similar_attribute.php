<?php

class m180620_121152_add_similar_attribute extends webforma\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_attribute}}', 'use_on_similar', 'tinyint(1) default 0');
	}

	public function safeDown()
	{

	}
}