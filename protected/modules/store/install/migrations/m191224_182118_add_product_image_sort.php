<?php

class m191224_182118_add_product_image_sort extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_product_image}}', 'position', 'int(11) DEFAULT 0');

		Yii::app()->getDb()->createCommand("UPDATE {{store_product_image}} SET position = id")->execute();
		
		$this->createIndex("ix_{{store_product_image}}_position", "{{store_product_image}}", "position", false);
	}

	public function safeDown()
	{

	}
}