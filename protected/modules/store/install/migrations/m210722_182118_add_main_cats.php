<?php

class m210722_182118_add_main_cats extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_category}}', 'on_main', 'tinyint(1) DEFAULT 0');
		$this->addColumn('{{store_category}}', 'sort_main', 'int(11) DEFAULT 1');

		$this->createIndex("ux_{{store_category}}_on_main", "{{store_category}}", "on_main");
	}

	public function safeDown()
	{

	}
}