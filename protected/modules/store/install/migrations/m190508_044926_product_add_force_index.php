<?php

class m190508_044926_product_add_force_index extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn("{{store_product}}", "force_index", "tinyint(1) default 0");
	}

	public function safeDown()
	{

	}
}