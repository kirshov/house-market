<?php

class m180223_084222_add_variant_price extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_product_variant}}', 'extra_charge', 'integer(3) DEFAULT NULL');
		$this->addColumn('{{store_product_variant}}', 'purchase_price', 'integer(3) DEFAULT NULL');
	}

	public function safeDown()
	{

	}
}