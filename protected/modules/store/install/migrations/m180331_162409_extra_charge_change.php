<?php

class m180331_162409_extra_charge_change extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->alterColumn('{{store_product}}', 'extra_charge', 'int(3) not null default "0"');
	}

	public function safeDown()
	{

	}
}