<?php

class m180516_174714_add_options_in_group extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_product_groups}}', 'options', 'text default null');
	}

	public function safeDown()
	{

	}
}