<?php

class m180503_125717_variants_extra_change extends webforma\components\DbMigration
{
	public function safeUp()
	{
        $this->alterColumn('{{store_product_variant}}', 'extra_charge', 'int(3) not null default "0"');
	}

	public function safeDown()
	{

	}
}