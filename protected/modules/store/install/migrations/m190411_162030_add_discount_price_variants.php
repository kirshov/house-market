<?php

class m190411_162030_add_discount_price_variants extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_product_variant}}', 'discount_price', 'float null');
	}

	public function safeDown()
	{

	}
}