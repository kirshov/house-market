<?php

class m180602_075211_add_tags_fields extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_category}}', 'groups_name', 'varchar(255) default null');
	}

	public function safeDown()
	{

	}
}