<?php

class m180605_110746_add_option_group extends webforma\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_attribute_option}}', 'group_name', 'varchar(32) default null');
	}

	public function safeDown()
	{

	}
}