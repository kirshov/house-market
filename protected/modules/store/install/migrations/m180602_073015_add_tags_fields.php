<?php

class m180602_073015_add_tags_fields extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_category}}', 'groups_alias', 'varchar(255) default null');
		$this->createIndex("ux_{{store_category}}_groups_alias", "{{store_category}}", "groups_alias", false);
	}

	public function safeDown()
	{

	}
}