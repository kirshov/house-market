<?php

class m190427_054922_add_pos_fields extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn("{{store_product}}", "search_name", "text default null");
	}

	public function safeDown()
	{

	}
}