<?php

class m180218_151307_add_category_type extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_category}}', 'product_type', 'integer(11) DEFAULT "0"');
	}

	public function safeDown()
	{

	}
}