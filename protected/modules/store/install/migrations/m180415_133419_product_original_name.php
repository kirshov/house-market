<?php

class m180415_133419_product_original_name extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn("{{store_product}}", "original_name", "varchar(255) not null default ''");
	}

	public function safeDown()
	{

	}
}