<?php

class m190617_105141_add_url_option extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_product_groups}}', 'use_category_url', 'tinyint(1) default 1');
	}

	public function safeDown()
	{

	}
}