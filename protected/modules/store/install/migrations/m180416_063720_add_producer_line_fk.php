<?php

class m180416_063720_add_producer_line_fk extends webforma\components\DbMigration
{
	public function safeUp()
	{
        $this->addForeignKey("fk_{{store_producer_line}}_producer_id", "{{store_producer_line}}", "producer_id", "{{store_producer}}", "id", "CASCADE", "CASCADE");
	}

	public function safeDown()
	{

	}
}