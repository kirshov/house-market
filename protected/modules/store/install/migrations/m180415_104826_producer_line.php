<?php

class m180415_104826_producer_line extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->createTable(
			"{{store_producer_line}}",
			[
				"id" => "pk",
				"producer_id" => "int(11) not null",
				"name" => "varchar(250) not null",
				"slug" => "varchar(150) not null",
				"short_description" => "text",
				"description" => "text",
				"meta_title" => "varchar(250) default null",
				"meta_keywords" => "varchar(250) default null",
				"meta_description" => "varchar(250) default null",
				"status" => "integer not null default '1'",
				"order" => "integer not null default '0'",
			],
			$this->getOptions()
		);

		$this->createIndex("ix_{{store_producer_line}}_slug", "{{store_producer_line}}", "slug", false);
		$this->createIndex("ix_{{store_producer_line}}_producer_id", "{{store_producer_line}}", "producer_id", false);
	}

	public function safeDown()
	{

	}
}