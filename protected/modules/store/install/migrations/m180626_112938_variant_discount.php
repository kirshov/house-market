<?php

class m180626_112938_variant_discount extends webforma\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_product_variant}}', 'discount', 'int(3) default null');
	}

	public function safeDown()
	{

	}
}