<?php

class m180918_185428_add_attr_field extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_attribute}}', 'hide_in_site', 'tinyint(1) default 0');
	}

	public function safeDown()
	{

	}
}