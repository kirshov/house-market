<?php

class m180821_161855_product_discontinued extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn("{{store_product}}", "discontinued", "tinyint(1) not null default 0");
	}

	public function safeDown()
	{

	}
}