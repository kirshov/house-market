<?php

class m190530_051405_product_hit extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn("{{store_product}}", "is_hit", "tinyint(1) default 0");
	}

	public function safeDown()
	{

	}
}