<?php

class m180220_183217_rename_prduct_provider extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->renameColumn('{{store_product}}', 'provider', 'provider_id');
	}

	public function safeDown()
	{

	}
}