<?php

class m180516_163934_add_cat_in_group extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_product_groups}}', 'category_id', 'int(11) default null');
	}

	public function safeDown()
	{

	}
}