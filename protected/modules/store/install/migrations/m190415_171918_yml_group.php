<?php

class m190415_171918_yml_group extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_attribute}}', 'yml_group_variant', 'tinyint(1) default 0');
	}

	public function safeDown()
	{

	}
}