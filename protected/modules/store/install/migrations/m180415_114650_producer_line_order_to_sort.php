<?php

class m180415_114650_producer_line_order_to_sort extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->renameColumn('{{store_producer_line}}', 'order', 'sort');
	}

	public function safeDown()
	{

	}
}