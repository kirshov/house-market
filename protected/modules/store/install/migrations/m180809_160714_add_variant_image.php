<?php

class m180809_160714_add_variant_image extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_product_variant}}', 'image_id', 'int(11) default null');

		$this->addForeignKey("fk_{{store_product_variant}}_image_id", "{{store_product_variant}}", "image_id", "{{store_product_image}}", "id", "CASCADE", "CASCADE");
	}

	public function safeDown()
	{

	}
}