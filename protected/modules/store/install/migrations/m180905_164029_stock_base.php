<?php

class m180905_164029_stock_base extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->createTable(
			"{{store_stock}}",
			[
				"id" => "pk",
				"name" => "varchar(250) not null",
				"slug" => "varchar(150) not null",
				"title" => "varchar(250) not null",
				"description" => "text",
				"description_bottom" => "text",
				"meta_title" => "varchar(250) default null",
				"meta_keywords" => "varchar(250) default null",
				"meta_description" => "varchar(250) default null",
				"status" => "tinyint not null default '1'",
				"sort" => "int(11) not null default '0'",
				'date_start' => 'date NOT NULL',
				'date_end' => 'date NOT NULL',
				'ids' => 'text',
			],
			$this->getOptions()
		);

		$this->createIndex("ux_{{store_stock}}_slug", "{{store_stock}}", "slug", true);
		$this->createIndex("ix_{{store_stock}}_status", "{{store_stock}}", "status", false);
	}

	public function safeDown()
	{

	}
}