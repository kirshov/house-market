<?php

class m180618_121608_yml_field_name_onvar extends webforma\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_attribute}}', 'yml_add_name_on_variant', 'tinyint(1) default 0');
	}

	public function safeDown()
	{

	}
}