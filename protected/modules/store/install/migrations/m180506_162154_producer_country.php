<?php

class m180506_162154_producer_country extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->createTable(
			"{{store_producer_country}}",
			[
				"id" => "pk",
				"name" => "varchar(250) not null",
				"slug" => "varchar(150) not null",
				"description" => "text",
				"description_bottom" => "text",
				"h1" => "varchar(250) default null",
				"meta_title" => "varchar(250) default null",
				"meta_keywords" => "varchar(250) default null",
				"meta_description" => "varchar(250) default null",
				"status" => "integer not null default '1'",
			],
			$this->getOptions()
		);

		$this->dropColumn('{{store_producer}}', 'country');
		$this->createIndex("ix_{{store_producer_country}}_slug", "{{store_producer_country}}", "slug", false);

		$this->addColumn("{{store_producer}}", "country_id", "integer default null");
		$this->createIndex("ix_{{store_producer}}_country_id", "{{store_producer}}", "country_id", false);
		$this->addForeignKey("fk_{{store_producer}}_country_id", "{{store_producer}}", "country_id", "{{store_producer_country}}", "id", "SET NULL", "CASCADE");
	}

	public function safeDown()
	{

	}
}