<?php

class m180219_190442_create_provider extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->createTable(
			"{{store_provider}}",
			[
				"id" => "pk",
				"name" => "varchar(250) not null",
				"description" => "text",
				"status" => "integer not null default '1'",
				"order" => "integer not null default '0'",
				"create_time" => "datetime not null",
				"update_time" => "datetime not null",
			],
			$this->getOptions()
		);

		$this->addColumn('{{store_product}}', 'provider', "integer default null");
	}

	public function safeDown()
	{

	}
}