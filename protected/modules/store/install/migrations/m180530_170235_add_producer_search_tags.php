<?php

class m180530_170235_add_producer_search_tags extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->dropColumn('{{store_category}}', 'search_name');
		$this->addColumn('{{store_producer}}', 'search_name', 'varchar(250) default null');
	}

	public function safeDown()
	{

	}
}