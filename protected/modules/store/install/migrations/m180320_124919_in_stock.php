<?php

class m180320_124919_in_stock extends webforma\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_product_variant}}', 'in_stock', 'integer(1) DEFAULT 1');
        $this->addColumn('{{store_product_variant}}', 'quantity', 'integer(11) null');
	}

	public function safeDown()
	{

	}
}