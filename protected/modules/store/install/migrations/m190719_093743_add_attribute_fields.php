<?php

class m190719_093743_add_attribute_fields extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_attribute}}', 'append_name', 'tinyint(1) default 0');
		$this->addColumn('{{store_attribute}}', 'yml_add_unit', 'tinyint(1) default 0');
	}

	public function safeDown()
	{

	}
}