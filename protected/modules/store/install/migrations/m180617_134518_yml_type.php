<?php

class m180617_134518_yml_type extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_attribute}}', 'yml_field', 'varchar(32) default null');
	}

	public function safeDown()
	{

	}
}