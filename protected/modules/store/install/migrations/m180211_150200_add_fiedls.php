<?php

class m180211_150200_add_fiedls extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_product}}', 'extra_charge', 'integer(3) DEFAULT NULL');
		$this->addColumn('{{store_product}}', 'is_new', 'tinyint(1) DEFAULT "0"');
	}

	public function safeDown()
	{

	}
}