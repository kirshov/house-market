<?php

class m180919_183313_producer_retarget_code extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_producer}}', 'retargeting_code', 'text default null');
	}

	public function safeDown()
	{

	}
}