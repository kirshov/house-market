<?php

class m180219_190959_add_fields extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->alterColumn('{{store_product}}', 'price', 'decimal(19,2) not null default "0"');
		$this->alterColumn('{{store_product}}', 'discount_price', 'decimal(19,2) not null default "0"');
		$this->alterColumn('{{store_product}}', 'discount', 'decimal(19,2) not null default "0"');
		$this->alterColumn('{{store_product}}', 'average_price', 'decimal(19,2) not null default "0"');
		$this->alterColumn('{{store_product}}', 'purchase_price', 'decimal(19,2) not null default "0"');
		$this->alterColumn('{{store_product}}', 'recommended_price', 'decimal(19,2) not null default "0"');

		$this->addColumn('{{store_provider}}', 'extra_charge', 'integer(3) DEFAULT NULL');
	}

	public function safeDown()
	{

	}
}