<?php

class m180521_121406_extra_charget_default_null extends webforma\components\DbMigration
{
	public function safeUp()
	{
        $this->alterColumn('{{store_product}}', 'extra_charge', 'int(3) default null');
        $this->alterColumn('{{store_product_variant}}', 'extra_charge', 'int(3) default null');

        $this->update('{{store_product}}', ['extra_charge' => new CDbExpression('NULL')], 'extra_charge = 0');
        $this->update('{{store_product_variant}}', ['extra_charge' => new CDbExpression('NULL')], 'extra_charge = 0');
	}

	public function safeDown()
	{

	}
}