<?php

class m190608_060052_add_variant_import_id extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_product_variant}}', 'external_id', 'VARCHAR(100) DEFAULT NULL');
		$this->dropColumn('{{store_category}}', 'system_name');
	}

	public function safeDown()
	{

	}
}