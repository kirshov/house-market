<?php

class m190508_051405_product_view extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn("{{store_product}}", "count_view", "int(11) default 0");
	}

	public function safeDown()
	{

	}
}