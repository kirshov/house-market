<?php

class m180617_140715_yml_add_name extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_attribute}}', 'yml_add_name', 'tinyint(1) default 0');
	}

	public function safeDown()
	{

	}
}