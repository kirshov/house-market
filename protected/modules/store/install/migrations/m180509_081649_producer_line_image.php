<?php

class m180509_081649_producer_line_image extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_producer_line}}', 'image', 'varchar(255) default null');
	}

	public function safeDown()
	{

	}
}