<?php

class m180415_132302_producer_line_fk extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn("{{store_product}}", "producer_line_id", "integer default null");
		$this->createIndex("ix_{{store_product}}_producer_line_id", "{{store_product}}", "producer_line_id", false);
		$this->addForeignKey("fk_{{store_product}}_producer_line", "{{store_product}}", "producer_line_id", "{{store_producer_line}}", "id", "SET NULL", "CASCADE");
	}

	public function safeDown()
	{

	}
}