<?php

class m180223_120204_variants_change extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->alterColumn('{{store_attribute_option}}', 'position', 'integer(11) not null default "0"');
		$this->alterColumn('{{store_product_variant}}', 'attribute_value', 'integer(11) default null');
	}

	public function safeDown()
	{

	}
}