<?php

class m180220_175626_add_provider extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->renameColumn('{{store_provider}}', 'order', 'sort');
	}

	public function safeDown()
	{

	}
}