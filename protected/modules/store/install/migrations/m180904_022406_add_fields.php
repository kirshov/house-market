<?php

class m180904_022406_add_fields extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn("{{store_product}}", "show_at_end", "tinyint(1) not null default 0");
		$this->addColumn("{{store_product_groups}}", "show_after_filter", "tinyint(1) not null default 0");
	}

	public function safeDown()
	{

	}
}