<?php

class m190210_064110_add_unit extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_product}}', 'unit', 'varchar(255) default null');
	}

	public function safeDown()
	{

	}
}