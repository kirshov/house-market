<?php

class m190712_032030_add_general_option extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_attribute}}', 'is_general', 'tinyint(1) default 0');
	}

	public function safeDown()
	{

	}
}