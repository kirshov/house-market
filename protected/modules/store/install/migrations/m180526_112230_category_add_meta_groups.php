<?php

class m180526_112230_category_add_meta_groups extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_category}}', 'groups_h1', 'varchar(250) default null');
		$this->addColumn('{{store_category}}', 'groups_meta_title', 'varchar(250) default null');
		$this->addColumn('{{store_category}}', 'groups_meta_description', 'varchar(250) default null');
		$this->addColumn('{{store_category}}', 'groups_meta_keywords', 'varchar(250) default null');
	}

	public function safeDown()
	{

	}
}