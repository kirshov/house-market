<?php

class m180516_064320_store_product_groups extends webforma\components\DbMigration
{
	public function safeUp()
	{

        $this->createTable(
            "{{store_product_groups}}",
            [
                "id" => "pk",
                "name" => "varchar(250) not null",
                "slug" => "varchar(150) not null",
                "title" => "varchar(250) not null",
                "description" => "text",
                "description_bottom" => "text",
                "meta_title" => "varchar(250) default null",
                "meta_keywords" => "varchar(250) default null",
                "meta_description" => "varchar(250) default null",
                "status" => "tinyint not null default '1'",
                "sort" => "int(11) not null default '0'",
            ],
            $this->getOptions()
        );

        $this->createIndex("ux_{{store_product_groups}}_slug", "{{store_product_groups}}", "slug", true);
        $this->createIndex("ix_{{store_product_groups}}_status", "{{store_product_groups}}", "status", false);
	}

	public function safeDown()
	{

	}
}