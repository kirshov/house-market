<?php

use webforma\components\controllers\FrontController;


class ShorterController extends FrontController
{
    public function actionGet($model, $id)
    {
    }

    public function actionCreate($model, $id)
    {
        if(!$model || !$id || !in_array($model, Shorter::$availableModels)){
            Yii::app()->end();
        }

        $link = Shorter::getShortLink($model, $id);
        if(!$link){
            $isExist = true;
            $infinityLoop = 0;
            while ($isExist && $infinityLoop < 20){
                $hash = Shorter::createHash();
                $isExist = Shorter::model()->find([
                    'condition' => 'BINARY `from` = :from',
                    'params' => ['from' => $hash],
                ]);
            }

            $to = $this->getLinkTo($model, $id);
            if($to){
                $shorter = new Shorter();
                $shorter->model_name = $model;
                $shorter->model_id = $id;
                $shorter->from = $hash;
                $shorter->to = $to;
                if($shorter->save()){
                    $link = $shorter->getUrl();
                }
            }
        }
        if($link){
            echo CHtml::link($link, $link, ['target' => '_blank']);
        }

        Yii::app()->end();
    }


    protected function getLinkTo($model, $id){
        switch ($model){
            case 'Product':
                $product = Product::model()->findByPk($id);
                if($product){
                    return ProductHelper::getUrl($product, true);
                }
                break;
        }

        return false;
    }
}
