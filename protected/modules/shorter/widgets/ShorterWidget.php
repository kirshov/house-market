<?php

Yii::import('application.modules.shorter.models.*');

/**
 * Class PagesWidget
 */
class ShorterWidget extends webforma\widgets\WWidget
{
    /**
     * @var string
     */
    public $model;

    /**
     * @var int
     */
    public $id;


    /**
     * @throws CException
     */
    public function init()
    {
        Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getAssetManager()->publish(
            Yii::getPathOfAlias('application.modules.shorter.views.web') . '/shorter.js'
        ), CClientScript::POS_END);

        parent::init();
    }

    /**
     * @throws CException
     */
    public function run()
    {
        if($this->model && $this->id && in_array($this->model, Shorter::$availableModels)){
            $link = Shorter::getShortLink($this->model, $this->id);

            echo CHtml::openTag('p', ['class' => 'short-wrap']);
            if($link) {
                echo CHtml::link($link, $link, ['target' => '_blank']);
            } else {
                echo CHtml::link('Получить короткую ссылку', ['/shorter/shorter/create', 'model' => $this->model, 'id' => $this->id], ['class' => 'get-short']);
            }
            CHtml::closeTag('p');
        }
    }
}
