<?php

/**
 * Class Shorter
 *
 * @property int $id
 * @property string $from
 * @property string $to
 * @property string $model_name
 * @property int $model_id
 */

class Shorter extends webforma\models\WModel
{
    public static $availableModels = ['Product'];

    /**
     * Returns the static model of the specified AR class.
     * @param  string $className
     * @return Shorter the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{shorter}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['from, to', 'filter', 'filter' => 'trim'],
            ['from, to', 'required'],
            ['from', 'length', 'max' => 32],
            ['to', 'length', 'max' => 255],
            [
                'from',
                'webforma\components\validators\YSLugValidator',
                'message' => Yii::t(
                    'ShorterModule.shorter',
                    'Unknown field format "{attribute}" only alphas, digits and _, from 2 to 50 characters'
                )
            ],
            ['from', 'unique'],
            ['id, from, to', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('ShorterModule.shorter', 'id'),
            'from' => 'Откуда',
            'to' => 'Куда',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria();
        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.from', $this->from, true);
        $criteria->compare($this->tableAlias . '.to', $this->to, true);
        $criteria->order = 'id DESC';

        return new CActiveDataProvider(get_class($this), ['criteria' => $criteria]);
    }

    /**
     * @return string
     */
    public function getUrl(){
    	$useSSL = Yii::app()->getModule('shorter')->useSSL;
    	switch ($useSSL){
		    case 1:
			    $ssl = Yii::app()->getRequest()->getIsSecureConnection();
		    	break;
		    case 2:
			    $ssl = true;
			    break;
		    case 3:
			    $ssl = false;
			    break;
	    }
        $domain = Yii::app()->getRequest()->getServerName();

        return ($ssl ? 'https://' : 'http://').'go.'.$domain.'/'.$this->from;
    }

    /**
     * @param $model
     * @param $id
     * @return null
     */
    public static function getShortLink($model, $id){
        $model = Shorter::model()->find('model_name = :model_name AND model_id = :model_id', ['model_name' => $model, 'model_id' => $id]);
        if($model){
            return $model->getUrl();
        }

        return null;
    }

    public static function createHash(){
        $chars="qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";

        $length = 5;
        $size = strlen($chars) - 1;
        $hash = '';
        while($length--){
            $hash .= $chars[mt_rand(0, $size)];
        }

        return $hash;
    }
}