<?php

class ShorterModule extends webforma\components\WebModule
{
    public $useSSL = 1;
    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('ShorterModule.shorter', 'Services');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('ShorterModule.shorter', 'Short links');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Модуль сокращения ссылок';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return "fa fa-fw fa-anchor";
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'shorter.models.*',
            ]
        );
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/shorter/shorterBackend/index';
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ShorterModule.shorter', 'Short links'),
                'url' => ['/shorter/shorterBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ShorterModule.shorter', 'Add short link'),
                'url' => ['/shorter/shorterBackend/create'],
            ],
        ];
    }

	/**
	 * @return array
	 */
	public function getEditableParams()
	{
		return [
			'useSSL' => [
				1 => 'Протокол текущего сервера',
				2 => 'Да',
				3 => 'Нет',
			],
		];
	}

	/**
	 * @return array
	 */
	public function getParamsLabels()
	{
		return [
			'useSSL' => 'Использовать SSL',
		];
	}

	/**
	 * @return array
	 */
	public function getEditableParamsGroups()
	{
		return [
			'0.main' => [
				'label' => 'Общие настройки',
				'items' => [
					'useSSL',
				],
			],
		];
	}
}
