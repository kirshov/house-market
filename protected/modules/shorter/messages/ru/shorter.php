<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yiic message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE, this file must be saved in UTF-8 encoding.
 */
return [
    'Services' => 'Сервисы',
    'Page was not found!' => 'Запрошенная страница не найдена!',
    'Add and close' => 'Добавить и закрыть',
    'Add and continue' => 'Добавить и продолжить',
    'Save and close' => 'Сохранить и закрыть',
    'Save and continue' => 'Сохранить и продолжить',
    'Short links' => 'Короткие ссылки',
    'Administration' => 'Управление',
    'administration' => 'управление',
    'Short links administration' => 'Управление ссылками',
    'Add short link' => 'Добавить короткую ссылку',
    'Short link' => 'Короткая ссылка',
    'add' => 'создание',
    'Unknown field format "{attribute}" only alphas, digits and _, from 2 to 50 characters' => 'Неверный формат поля "{attribute}" допустимы только буквы, цифры и символ подчеркивания, от 2 до 50 символов',
    'Fields with' => 'Поля отмеченные',
    'are required.' => 'обязательны для заполнения',
    'New shorter was added!' => 'Коротка ссылка добавлена!',
    'Create' => 'Добавление',
];
