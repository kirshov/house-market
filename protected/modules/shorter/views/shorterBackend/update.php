<?php
$this->breadcrumbs = [
    Yii::t('ShorterModule.shorter', 'Short links') => ['/shorter/shorterBackend/index'],
    Yii::t('ShorterModule.shorter', 'Editing'),
];

$this->pageTitle = Yii::t('ShorterModule.shorter', 'Short links - edit');

$this->menu = [
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('ShorterModule.shorter', 'Short links administration'),
        'url'   => ['/shorter/shorterBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('ShorterModule.shorter', 'Add short link'),
        'url'   => ['/shorter/shorterBackend/create']
    ],

    [
        'icon'  => 'fa fa-fw fa-pencil',
        'label' => Yii::t('ShorterModule.shorter', 'Edit short link'),
        'url'   => [
            '/shorter/shorterBackend/update',
            'id' => $model->id
        ]
    ],
    [
        'icon'        => 'fa fa-fw fa-trash-o',
        'label'       => Yii::t('ShorterModule.shorter', 'Remove short link'),
        'url'         => '#',
        'linkOptions' => [
            'submit'  => ['/shorter/shorterBackend/delete', 'id' => $model->id],
            'params'  => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
            'confirm' => Yii::t('ShorterModule.shorter', 'Do you really want to remove short link?'),
        ]
    ],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('ShorterModule.shorter', 'Editing short links'); ?>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>
