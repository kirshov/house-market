<?php
$this->breadcrumbs = [
    Yii::t('ShorterModule.shorter', 'Short links') => ['/shorter/shorterBackend/index'],
    Yii::t('ShorterModule.shorter', 'Create'),
];

$this->pageTitle = Yii::t('ShorterModule.shorter', 'Short links - add');

$this->menu = [
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('ShorterModule.shorter', 'Short links administration'),
        'url'   => ['/shorter/shorterBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('ShorterModule.shorter', 'Add short link'),
        'url'   => ['/shorter/shorterBackend/create']
    ],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('ShorterModule.shorter', 'Short link'); ?>
        <small><?=  Yii::t('ShorterModule.shorter', 'add'); ?></small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>
