<?php
/**
 * @var $this ContentBlockBackendController
 * @var $model ContentBlock
 */

$this->breadcrumbs = [
    Yii::t('ShorterModule.shorter', 'Short links') => ['/shorter/shorterBackend/index'],
    Yii::t('ShorterModule.shorter', 'Administration'),
];

$this->pageTitle = Yii::t('ShorterModule.shorter', 'Short links - admin');

$this->menu = [
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('ShorterModule.shorter', 'Short links administration'),
        'url' => ['/shorter/shorterBackend/index']
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('ShorterModule.shorter', 'Add short link'),
        'url' => ['/shorter/shorterBackend/create']
    ],
];
?>
<div class="page-header">
    <h1>
        <?= Yii::t('ShorterModule.shorter', 'Short links'); ?>
        <small><?= Yii::t('ShorterModule.shorter', 'administration'); ?></small>
    </h1>
</div>

<?php $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'shorter-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => [
            [
                'name' => 'from',
                'type' => 'raw',
                'value' => function($data){
                    return CHtml::link($data->getUrl(), $this->createUrl('/shorter/shorterBackend/update', ['id' => $data->id]));
                },
                'filter' => CHtml::activeTextField($model, 'from', ['class' => 'form-control']),
            ],
            [
                'class' => 'webforma\widgets\WEditableColumn',
                'name' => 'to',
                'editable' => [
                    'url' => $this->createUrl('/shorter/shorterBackend/inline'),
                    'mode' => 'popup',
                    'params' => [
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
                    ]
                ],
                'filter' => CHtml::activeTextField($model, 'to', ['class' => 'form-control']),
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
                'frontViewButtonUrl' => function($data){
                    return $data->getUrl();
                },
                'buttons' => [
                    'front_view' => [
                        'visible' => function ($row, $data) {
                            return true;
                        }
                    ],
                    'view' => [
                        'visible' => function ($row, $data) {
                            return false;
                        }
                    ]
                ]
            ],
        ],
    ]
); ?>
