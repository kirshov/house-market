<?php
/**
 * @var $model Shorter
 * @var $this ShorterBackendController
 * @var $form \webforma\widgets\ActiveForm
 */
?>
<?php
$form = $this->beginWidget(
    'webforma\widgets\ActiveForm',
    [
        'id'                     => 'shorter-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'type'                   => 'vertical',
        'htmlOptions'            => ['class' => 'well'],
    ]
); ?>
<div class="alert alert-info">
    <?=  Yii::t('ShorterModule.shorter', 'Fields with'); ?>
    <span class="required">*</span>
    <?=  Yii::t('ShorterModule.shorter', 'are required.'); ?>
</div>

<?=  $form->errorSummary($model); ?>

<div class="row">
    <div class="col-sm-7">
        <?=  $form->textFieldGroup($model, 'from'); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-7">
        <?=  $form->textFieldGroup($model, 'to'); ?>
    </div>
</div>

<?php $this->widget(
    'bootstrap.widgets.TbButton',
    [
        'buttonType' => 'submit',
        'context'    => 'primary',
        'label'      => $model->isNewRecord
            ? Yii::t('ShorterModule.shorter', 'Add and continue')
            : Yii::t('ShorterModule.shorter', 'Save and continue'),
    ]
); ?>

<?php $this->widget(
    'bootstrap.widgets.TbButton',
    [
        'buttonType'  => 'submit',
        'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
        'label'       => $model->isNewRecord
            ? Yii::t('ShorterModule.shorter', 'Add and close')
            : Yii::t('ShorterModule.shorter', 'Save and close'),
    ]
); ?>

<?php $this->endWidget(); ?>
