$(document).ready(function () {
    $('.get-short').on('click', function(event){
        event.preventDefault();
        var _this = $(this);
        $.get(_this.attr('href'), function(response){
            if(response){
                _this.replaceWith(response);
            }
        });
    });
});