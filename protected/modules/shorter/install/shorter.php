<?php
return [
    'module'    => [
        'class' => 'application.modules.shorter.ShorterModule',
    ],
    'import'    => [],
    'component' => [],
	'params'    => [
        'widgets'   => [
            'shorter' => 'application.modules.shorter.widgets.ShorterWidget',
        ],
    ],
    'rules'     => [
        '/get-short-link/<model>/<id>' => 'shorter/shorter/create',
    ],
];
