<?php

class m000000_000000_shorter_base extends webforma\components\DbMigration
{

    public function safeUp()
    {
        $this->createTable(
            '{{shorter}}',
            [
                'id'          => 'pk',
                'from'        => 'varchar(32) NOT NULL',
                'to'        => 'varchar(255) NOT NULL',
            ],
            $this->getOptions()
        );

        $this->createIndex("ux_{{shorter}}_code", '{{shorter}}', "from", true);
    }

    public function safeDown()
    {
    }
}
