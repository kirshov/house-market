<?php

class m180615_103740_add_f extends webforma\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{shorter}}', 'model_name', 'varchar(32)');
        $this->addColumn('{{shorter}}', 'model_id', 'varchar(32)');
	}

	public function safeDown()
	{
        $this->createIndex("ux_{{shorter}}_model_id", '{{shorter}}', "model_name, model_id", true);
	}
}