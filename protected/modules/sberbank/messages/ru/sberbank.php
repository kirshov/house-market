<?php
return [
    'Pay' => 'Оплатить',
    'Payment order #{id} on "{site}" website' => 'Оплата заказа №{id} на сайте "{site}"',
    'Sberbank payment module' => 'Модуль для приема оплаты через Сбербанк',
    'Sberbank' => 'Сбербанк',
    'Store' => 'Магазин',
];
