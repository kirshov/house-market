<?php

Yii::import('application.modules.sberbank.SberbankModule');

class SberbankPaymentSystem extends PaymentSystem
{
	/**
	 * @var
	 */
	public $urlApi;

	/**
	 * @var array
	 */
	public $settings = [];

	/**
	 * @var string
	 */
	public $currency = 643;

	/**
	 * @var array
	 */
	public $errorCodes = [
		1 => 'Заказ с таким номером уже обработан/Неверный номер заказа',
		3 => 'Неизвестная валюта',
		4 => 'Номер заказа не может быть пуст/Отсутствует сумма/Отсутствует сумма/URL возврата не может быть пуст/Пароль не может быть пуст',
		5 => 'Неверно указано значение одного из параметров/Доступ запрещён/Пользователь отключён',
		7 => 'Системная ошибка',
		8 => 'Отсутствие обязательного параметра Корзины/Сумма товарных позиций в Корзине не совпадает с общей суммой заказа/Неверный формат Корзины',
	];

	/**
	 * @param Payment $payment
	 * @param Order $order
	 * @param bool $return
	 * @return false|string|string[]|void|null
	 * @throws CException
	 */
    public function renderCheckoutForm(Payment $payment, Order $order, $return = false)
    {
	    $orderPaymentData = json_decode($order->payment_details, true);
	    if($order->isPaid()){
		    return true;
	    }
	    $getPage = Yii::app()->getRequest()->getParam('result');
	    if($getPage && $orderPaymentData['orderId'] && !$orderPaymentData['orderStatus']){
			$this->receiver($payment, $order);
	    }

	    $html = '';
	    if($getPage == 'fail' || $orderPaymentData['orderStatus']){
	    	if(!$orderPaymentData['orderStatus']){
	    		$data = $this->receiver($payment, $order, true);
			    if(!$data){
				    Order::model()->updateByPk($order->id, ['payment_details' => null]);
			    }
			    $html .= '<div class="bs-callout bs-callout-danger error-block">Произошла ошибка в процессе оплаты. Пожалуйста, повторите попытку позднее!</div>';
		    }
	    }

	    if(!$orderPaymentData['orderStatus']){
		    $html .= Yii::app()->getController()->renderPartial('application.modules.sberbank.views.form', [
			    'settings' => $payment->getPaymentSystemSettings(),
			    'order' => $order,
			    'payment' => $payment,
			    'orderPaymentData' => $orderPaymentData,
		    ], $return);
	    } else {
	    	if(!$order->isPaid()){
	    		$link = Yii::app()->createUrl( '/order/order/view', ['url' => $order->url, 'action' => 'refresh']);
			    $html .= '<div class="bs-callout bs-callout-danger error-block">'.$orderPaymentData['errorMessage'].' <a href="'.$link.'">Попробовать еще раз.</a></div>';
		    }
	    }

	    return $html;
    }

	/**
	 * @param integer $id
	 * @return string
	 */
    public function getError($id){
		return $this->errorCodes[$id];
    }

	public function initPayment(Payment $payment, Order $order)
	{
		$orderPaymentData = json_decode($order->payment_details, true);
		if($order->isPaid()){
			return true;
		}

		if(Yii::app()->getRequest()->getParam('action') == 'refresh'){
			$orderPaymentData = [];
			Order::model()->updateByPk($order->id, ['payment_details' => '']);
		}

		if($orderPaymentData['orderId'] || $orderPaymentData['paymentURL']){
			return true;
		}

		parent::initPayment($payment, $order);

		$order->phone = preg_replace('/\D+/', '', $order->phone);

		$this->settings = $payment->getPaymentSystemSettings();

		$data = [
			'orderNumber' => $order->getNumberForUser() . '#' . time(),
			'amount' => round($order->getTotalPriceWithDelivery() * 100),
			'description' => 'Оплата заказа ' . $order->getNumberForUser(),
			'userName' => $this->settings['userName'],
			'password' => $this->settings['password'],
			'returnUrl' => Yii::app()->createAbsoluteUrl('/order/order/view', ['url' => $order->url, 'result' => 'success']),
			'failUrl' => Yii::app()->createAbsoluteUrl('/order/order/view', ['url' => $order->url, 'result' => 'fail']),
			'currency' => $this->currency,
		];

		if($this->settings['enabledCredit']){
			if($this->settings['mode'] != 'work') {
				$data['dummy'] = true;
			}
			$data['jsonParams'] = json_encode([
				'email' => $order->email,
				'phone' => $order->phone,
			]);
		}

		if($this->settings['enabledCredit'] || $this->settings['enabledTaxation']){
			$data['orderBundle'] = json_encode($this->getOrderBundle($order));
		}

		if($this->settings['enabledTaxation']){
			$data['taxSystem'] = $this->settings['taxations'];
		}

		$result = $this->_sendRequest($this->getUrlApi($payment), 'register.do', $data);

		return [
			'Success' => empty($result['errorCode']),
			'paymentURL' => $result['formUrl'],
			'orderId' => $result['orderId'],
			'errorMessage' => $result['errorMessage'],
			'errorCode' => $result['errorCode'],
		];
	}

	protected function getUrlApi(Payment $payment){
    	if($this->urlApi === null){
		    $this->settings = $payment->getPaymentSystemSettings();

			if($this->settings['enabledCredit']){
				if($this->settings['mode'] == 'work'){
					$this->urlApi = 'https://securepayments.sberbank.ru/sbercredit/';
				} else {
					$this->urlApi = ' https://3dsec.sberbank.ru/sbercredit/';
				}
			} else {
				if($this->settings['mode'] == 'work'){
					$this->urlApi = 'https://securepayments.sberbank.ru/payment/rest/';
				} else {
					$this->urlApi = 'https://3dsec.sberbank.ru/payment/rest/';
				}
			}
	    }

    	return $this->urlApi;
	}

	/**
	 * @param Order $order
	 * @return array
	 */
	protected function getOrderBundle(Order $order){
		$cartItems = [];
		$orderDiscount = $order->getDiscountPrice();

		$discountToPos = 0;
		if($orderDiscount){
			$discountToPos = $orderDiscount / sizeof($order->products);
		}
		foreach ((array)$order->products as $product){
			$_item = [
				'positionId' => $product->id,
				'name' => mb_substr($product->product_name, 0, 99),
				'quantity' => [
					'value' => $product->quantity,
					'measure' => 'шт',
				],
				'itemPrice' => ($product->price - $discountToPos) * 100,
				'itemAmount' => ($product->price - $discountToPos) * $product->quantity * 100,
				'itemCurrency' => $this->currency,
				'itemCode' => $product->sku ?: $product->product_id,
			];
			/*if($orderDiscount){
				$_item['discount'] = [
					'discountType' => '',
					'discountValue' => $discountToPos,
				];
			}*/

			if($this->settings['taxType']){
				$_item['tax'] = [
					'taxType' => $this->settings['taxType'],
				];
			}

			$cartItems[] = $_item;
		}

		if($order->delivery_price > 0){
			$deliveryName = $order->delivery ? $order->delivery->name : 'Доставка';
			$_item = [
				'positionId' => 'delivery',
				'name' => mb_substr($deliveryName, 0, 99),
				'quantity' => [
					'value' => 1,
					'measure' => 'шт',
				],
				'itemPrice' => $order->delivery_price * 100,
				'itemAmount' => $order->delivery_price * 100,
				'itemCurrency' => $this->currency,
				'itemCode' => 'delivery',
			];
			if($this->settings['taxType']){
				$_item['tax'] = [
					'taxType' => $this->settings['taxType'],
				];
			}

			$cartItems[] = $_item;
		}

    	$data = [
		    'customerDetails' => [
			    'email' => $order->email,
			    'phone' => $order->phone,
		    ],
    		'cartItems' => [
    			'items' => $cartItems,
		    ],
	    ];

    	if($this->settings['enabledCredit']){
    		$data['installments'] = [
			    'productType' => 'CREDIT',
		        'productID' => 10,
		    ];
	    }

    	return $data;
	}

	/**
	 * @param $url
	 * @param $method
	 * @param $data
	 * @return bool|mixed|string
	 */
	protected function _sendRequest($url, $method, $data)
	{
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url . $method,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => http_build_query($data),
			//CURLOPT_SSLVERSION => 6
		));

		$response = curl_exec($curl);
		$this->log($response);
		$this->log(http_build_query($data));
		$response = json_decode($response, true);

		curl_close($curl);

		unset($data['login'], $data['password']);
		$this->log('Метод ' . $this->urlApi . $method . '  gateway request: ' . print_r($data, 1) . ' gateway response:' . print_r($response, 1));

		return $response;
	}


	/**
	 * @param Payment $payment
	 * @param Order $order
	 * @param bool $return
	 * @return bool|mixed|string
	 */
	public function receiver(Payment $payment, Order $order, $return = false)
	{
		$this->settings = $payment->getPaymentSystemSettings();

		$orderPaymentData = json_decode($order->payment_details, true);
		$data = [
			'orderId' => $orderPaymentData['orderId'],
			'userName' => $this->settings['userName'],
			'password' => $this->settings['password'],
		];

		$response = $this->_sendRequest($this->getUrlApi($payment), 'getOrderStatusExtended.do', $data);

		if($return){
			return $response;
		}

		if(is_numeric($response['orderStatus']) && $response['orderStatus'] != 0){
			unset($orderPaymentData['paymentURL']);
			if (($response['errorCode'] == 0) && (($response['orderStatus'] == 1) || ($response['orderStatus'] == 2))) {
				$order->pay($payment);

				$orderPaymentData['errorCode'] = null;
				$orderPaymentData['errorMessage'] = null;
				$orderPaymentData['orderStatus'] = $response['orderStatus'];
			} else {
				$orderPaymentData['orderStatus'] = $response['orderStatus'];
				$orderPaymentData['errorCode'] = $response['errorCode'];
				$orderPaymentData['errorMessage'] = $response['actionCodeDescription'] ?: $response['errorMessage'];
			}
			$orderData = [
				'payment_details' => json_encode($orderPaymentData, JSON_UNESCAPED_UNICODE),
			];
			Order::model()->updateByPk($order->id, $orderData);

			Yii::app()->getController()->refresh();
		}
	}
}
