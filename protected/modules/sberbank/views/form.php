<?php
/**
 * @var array $settings
 * @var array $orderPaymentData
 * @var Order $order
 * @var Payment $payment
 */

if($orderPaymentData['paymentURL']){
	echo CHtml::form($orderPaymentData['paymentURL'], 'get');
	echo CHtml::submitButton($settings['enabledCredit'] ? 'Купить в кредит' : 'Оплатить онлайн', ['class' => 'btn']);
	echo CHtml::endForm();
}