<?php

use webforma\components\WebModule;

class SberbankModule extends WebModule
{
    /**
     *
     */
    public $adminMenuOrder = 81;

    public function getDependencies()
    {
        return ['payment'];
    }

    public function getNavigation()
    {
        return false;
    }

    public function getAdminPageLink()
    {
        return false;
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getEditableParamsGroups()
    {
        return [];
    }

    public function getIsShowInAdminMenu()
    {
        return false;
    }

    public function getCategory()
    {
        return Yii::t('SberbankModule.sberbank', 'Store');
    }

    public function getName()
    {
        return Yii::t('SberbankModule.sberbank', 'Tinkoff');
    }

    public function getDescription()
    {
        return Yii::t('SberbankModule.sberbank', 'Sberbank payment module');
    }

    public function getIcon()
    {
        return 'fa fa-rub';
    }

    public function init()
    {
        parent::init();
    }
}
