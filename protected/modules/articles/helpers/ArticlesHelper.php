<?php

/**
 * Class ArticlesHelper
 */
class ArticlesHelper
{
    const CACHE_NEWS_LIST = 'Articles::ArticlesList';
    const CACHE_NEWS_TAG = 'webforma::articles';
}