<?php
use webforma\components\Event;

/**
 * Class ArticlesListener
 */
class ArticlesListener
{
    /**
     * onAfterSave event listener
     *
     * @param Event $event
     */
    public static function onAfterSave(Event $event)
    {
	    \TaggedCache\TaggingCacheHelper::deleteTag([ArticlesHelper::CACHE_NEWS_TAG]);
    }

    /**
     * @param Event $event
     */
    public static function onAfterDelete(Event $event)
    {
	    \TaggedCache\TaggingCacheHelper::deleteTag([ArticlesHelper::CACHE_NEWS_TAG]);
    }

}