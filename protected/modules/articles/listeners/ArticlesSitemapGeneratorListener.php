<?php

use webforma\components\Event;

Yii::import('application.modules.articles.models.Articles');

/**
 * Class SitemapGeneratorListener
 */
class ArticlesSitemapGeneratorListener
{
    /**
     * @param Event $event
     */
    public static function onGenerate(Event $event)
    {
        $generator = $event->getGenerator();

        $provider = new CActiveDataProvider(Articles::model()->published()->public());

        foreach (new CDataProviderIterator($provider) as $item) {
            $generator->addItem(
                Yii::app()->createAbsoluteUrl('/articles/articles/view', ['slug' => $item->slug]),
                strtotime($item->update_time),
                SitemapHelper::FREQUENCY_WEEKLY,
                0.5
            );
        }
    }
} 
