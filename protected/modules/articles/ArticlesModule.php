<?php

/**
 * ArticlesModule основной класс модуля articles
 */

use webforma\components\WebModule;

/**
 * Class ArticlesModule
 */
class ArticlesModule extends WebModule
{
    /**
     * @var string
     */
    public $uploadPath = 'articles';

	/**
	 *
	 */
	public $adminMenuOrder = 45;

     /**
     * @var int
     */
    public $perPage = 10;
    /**
     * @var
     */
    public $metaTitle;
    /**
     * @var
     */
    public $metaDescription;
    /**
     * @var
     */
    public $metaKeyWords;

	/**
	 * @var int
	 */
	public $useTags = 0;

	/**
	 * @var int
	 */
	public $useCategory = 0;

	/**
	 * @var
	 */
	public $mainCategory;

	/**
     * @return array
     */
    public function getDependencies()
    {
        return [];
    }

    /**
     * @return bool
     */
    public function getInstall()
    {
        if (parent::getInstall()) {
            @mkdir(Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath, 0755);
        }

        return false;
    }

    /**
     * @return array|bool
     */
    public function checkSelf()
    {
    	return true;
        $messages = [];

        $uploadPath = Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath;

        if (!is_writable($uploadPath)) {
            $messages[WebModule::CHECK_ERROR][] = [
                'type' => WebModule::CHECK_ERROR,
                'message' => Yii::t(
                    'ArticlesModule.articles',
                    'Directory "{dir}" is not accessible for write! {link}',
                    [
                        '{dir}' => $uploadPath,
                        '{link}' => CHtml::link(
                            Yii::t('ArticlesModule.articles', 'Change settings'),
                            [
                                '/webforma/backend/modulesettings/',
                                'module' => 'articles',
                            ]
                        ),
                    ]
                ),
            ];
        }

        return (isset($messages[WebModule::CHECK_ERROR])) ? $messages : true;
    }

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [
            'uploadPath' => Yii::t(
                'ArticlesModule.articles',
                'Uploading files catalog (relatively {path})',
                [
                    '{path}' => Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . Yii::app()->getModule(
                            "webforma"
                        )->uploadPath,
                ]
            ),
			'useTags' => 'Разрешить теги',
            'perPage' => Yii::t('ArticlesModule.articles', 'Articles per page'),
            'metaTitle' => Yii::t('ArticlesModule.articles', 'Title tag for the articles section'),
            'metaDescription' => Yii::t('ArticlesModule.articles', 'Description for the articles section'),
            'metaKeyWords' => Yii::t('ArticlesModule.articles', 'KeyWords for the articles section'),
			'useCategory' => 'Использовать категории',
			'mainCategory' => 'Главная категория статей',
        ];
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
		$params = [
			'perPage',
			'metaTitle',
			'metaDescription',
			'metaKeyWords',
		];

		if(DEVMODE){
			$params = CMap::mergeArray($params, [
				'useTags' => [
					'input-type' => 'checkbox',
				],
				'useCategory' => [
					'input-type' => 'checkbox',
				],
				'mainCategory' => CHtml::listData($this->getCategoryList(), 'id', 'name'),
			]);
		}
		return $params;
    }

    /**
     * @return array
     */
    public function getEditableParamsGroups()
    {
		$groups = [
			'main' => [
				'label' => Yii::t('ArticlesModule.articles', 'General'),
				'items' => [
					'perPage',
				],
			],
			'seo' => [
				'label' => Yii::t('ArticlesModule.articles', 'SEO'),
				'items' => [
					'metaTitle',
					'metaDescription',
					'metaKeyWords',
				],
			],
		];

		if(DEVMODE){
			$groups = CMap::mergeArray($groups, [
				'system' => [
					'label' => 'Системные настройки',
					'items' => [
						'useTags',
						'useCategory',
						'mainCategory',
					]
				]
			]);
		}

		return $groups;
    }

    /**
     * @return bool
     */
    public function getIsInstallDefault()
    {
        return false;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('ArticlesModule.articles', 'Content');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('ArticlesModule.articles', 'Articles');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('ArticlesModule.articles', 'Module for creating and management articles');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-align-left';
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/articles/articlesBackend/index';
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ArticlesModule.articles', 'Articles list'),
                'url' => ['/articles/articlesBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ArticlesModule.articles', 'Create articles'),
                'url' => ['/articles/articlesBackend/create'],
            ],
        ];
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport([
			'articles.models.*',
			'vendor.yiiext.taggable-behavior.ETaggableBehavior',
			'vendor.yiiext.taggable-behavior.EARTaggableBehavior',
		]);
    }
}
