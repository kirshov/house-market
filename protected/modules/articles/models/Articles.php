<?php
/**
 * Articles основная модель для статей
 */

/**
 * This is the model class for table "Articles".
 *
 * The followings are the available columns in table 'Articles':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property string $title
 * @property string $slug
 * @property string $short_text
 * @property string $full_text
 * @property integer $user_id
 * @property integer $status
 * @property integer $is_protected
 * @property string $link
 * @property string $image
 * @property string $meta_title
 * @property string $description
 * @property string $keywords
 * @property int $category_id
 * @property int $sort_id
 */

use webforma\components\Event;
use webforma\widgets\WPurifier;

/**
 * Class Articles
 */
class Articles extends webforma\models\WModel
{
    /**
     *
     */
    const STATUS_DRAFT = 0;
    /**
     *
     */
    const STATUS_PUBLISHED = 1;
    /**
     *
     */
    const STATUS_MODERATION = 2;

    /**
     *
     */
    const PROTECTED_NO = 0;
    /**
     *
     */
    const PROTECTED_YES = 1;

	/**
	 * @var  string|array tags list
	 */
	public $tags;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{articles}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * @param  string $className
     * @return Articles   the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['title, slug, short_text, full_text, meta_title, keywords, description', 'filter', 'filter' => 'trim'],
            ['title, slug, meta_title, keywords, description', 'filter', 'filter' => [new WPurifier(), 'purify']],
            ['title, slug, full_text', 'required', 'on' => ['update', 'insert']],
            ['status, is_protected, category_id, sort_id', 'numerical', 'integerOnly' => true],
            ['title, slug', 'length', 'max' => 150],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            ['slug', 'unique'],
            ['meta_title, keywords, description', 'length', 'max' => 250],
			['category_id', 'default', 'setOnEmpty' => true, 'value' => null],
			['tags', 'safe'],
			['tags', 'default', 'value' => []],
            ['link', 'length', 'max' => 250],
            ['link', 'webforma\components\validators\YUrlValidator'],
            [
                'slug',
                'webforma\components\validators\YSLugValidator',
                'message' => Yii::t('ArticlesModule.articles', 'Bad characters in {attribute} field')
            ],
            [
                'id, meta_title, keywords, description, create_time, update_time, title, slug, short_text, full_text, user_id, status, is_protected, category_id',
                'safe',
                'on' => 'search'
            ],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $module = Yii::app()->getModule('articles');

		$behaviors = [
			'imageUpload' => [
				'class' => 'webforma\components\behaviors\ImageUploadBehavior',
				'attributeName' => 'image',
				'uploadPath' => $module->uploadPath,
			],
			'sortable' => [
				'class' => 'webforma\components\behaviors\SortableBehavior',
				'attributeName' => 'sort_id',
			],
		];
		if($module->useTags){
			$behaviors['tags'] = [
				'class' => 'vendor.yiiext.taggable-behavior.EARTaggableBehavior',
				'tagTable' => Yii::app()->getDb()->tablePrefix.'articles_tag',
				'tagBindingTable' => Yii::app()->getDb()->tablePrefix.'articles_to_tag',
				'tagModel' => 'ArticlesTag',
				'modelTableFk' => 'article_id',
				'tagBindingTableTagId' => 'tag_id',
				'cacheID' => 'cache',
			];
		}

		return $behaviors;
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
			'category' => [self::BELONGS_TO, 'Category', 'category_id'],
            'user' => [self::BELONGS_TO, 'User', 'user_id'],
        ];
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'published' => [
                'condition' => 't.status = :status',
                'params' => [':status' => self::STATUS_PUBLISHED],
            ],
            'protected' => [
                'condition' => 't.is_protected = :is_protected',
                'params' => [':is_protected' => self::PROTECTED_YES],
            ],
            'public' => [
                'condition' => 't.is_protected = :is_protected',
                'params' => [':is_protected' => self::PROTECTED_NO],
            ],
            'recent' => [
                'order' => 'create_time DESC',
                'limit' => 5,
            ]
        ];
    }

    /**
     * @param $num
     * @return $this
     */
    public function last($num)
    {
        $this->getDbCriteria()->mergeWith(
            [
                'order' => 'create_time DESC',
                'limit' => $num,
            ]
        );

        return $this;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('ArticlesModule.articles', 'Id'),
            'create_time' => Yii::t('ArticlesModule.articles', 'Created at'),
            'update_time' => Yii::t('ArticlesModule.articles', 'Updated at'),
            'title' => Yii::t('ArticlesModule.articles', 'Title'),
            'slug' => Yii::t('ArticlesModule.articles', 'Alias'),
            'image' => Yii::t('ArticlesModule.articles', 'Image'),
            'link' => Yii::t('ArticlesModule.articles', 'Link'),
            'short_text' => Yii::t('ArticlesModule.articles', 'Short text'),
            'full_text' => Yii::t('ArticlesModule.articles', 'Full text'),
            'user_id' => Yii::t('ArticlesModule.articles', 'Author'),
            'status' => Yii::t('ArticlesModule.articles', 'Status'),
            'is_protected' => Yii::t('ArticlesModule.articles', 'Access only for authorized'),
            'meta_title' => Yii::t('ArticlesModule.articles', 'Page title (SEO)'),
            'keywords' => Yii::t('ArticlesModule.articles', 'Keywords (SEO)'),
            'description' => Yii::t('ArticlesModule.articles', 'Description (SEO)'),
			'tags' => 'Тэги',
			'category_id' => Yii::t('ArticlesModule.articles', 'Category'),
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (!$this->slug) {
            $this->slug = webforma\helpers\YText::translit($this->title);
        }

        return parent::beforeValidate();
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        $this->update_time = new CDbExpression('NOW()');

        if ($this->getIsNewRecord()) {
            $this->create_time = $this->update_time;
            $this->user_id = Yii::app()->getUser()->getId();
        }

        if(Yii::app()->getModule('articles')->useTags){
	        $this->setTags($this->tags);
        }

        return parent::beforeSave();
    }

    /**
     *
     */
    public function afterSave()
    {
        Yii::app()->eventManager->fire(ArticlesEvents::ARTICLES_AFTER_SAVE, new Event($this));

        return parent::afterSave();
    }

    /**
     *
     */
    public function afterDelete()
    {
        Yii::app()->eventManager->fire(ArticlesEvents::ARTICLES_AFTER_DELETE, new Event($this));

        parent::afterDelete();
    }

    /**
     *
     */
    public function afterFind()
    {
	    if(Yii::app()->getModule('articles')->useTags) {
		    $this->tags = $this->getTags();
	    }

        return parent::afterFind();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria();

        $criteria->compare('t.id', $this->id);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('t.slug', $this->slug, true);
        $criteria->compare('short_text', $this->short_text, true);
        $criteria->compare('full_text', $this->full_text, true);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('is_protected', $this->is_protected);
		$criteria->compare('category_id', $this->category_id, true);

        return new CActiveDataProvider(get_class($this), [
            'criteria' => $criteria,
            'sort' => ['defaultOrder' => 'sort_id ASC'],
        ]);
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_DRAFT => Yii::t('ArticlesModule.articles', 'Draft'),
            self::STATUS_PUBLISHED => Yii::t('ArticlesModule.articles', 'Published'),
            //self::STATUS_MODERATION => Yii::t('ArticlesModule.articles', 'On moderation'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('ArticlesModule.articles', '*unknown*');
    }

    /**
     * @return array
     */
    public function getProtectedStatusList()
    {
        return [
            self::PROTECTED_NO => Yii::t('ArticlesModule.articles', 'no'),
            self::PROTECTED_YES => Yii::t('ArticlesModule.articles', 'yes'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getProtectedStatus()
    {
        $data = $this->getProtectedStatusList();

        return isset($data[$this->is_protected]) ? $data[$this->is_protected] : Yii::t('ArticlesModule.articles', '*unknown*');
    }

    /**
     * @return bool
     */
    public function isProtected()
    {
        return $this->is_protected == self::PROTECTED_YES;
    }

	/**
	 * @param $tag
	 * @param array $with
	 * @return mixed
	 */
	public function getByTag($tag, array $with = ['articles'])
	{
		return Articles::model()->with($with)
			->published()
			->public()
			->taggedWith($tag)->findAll();
	}

	/**
	 * @param $category_id
	 * @return $this
	 */
	public function category($category_id)
	{
		$this->getDbCriteria()->mergeWith(
			[
				'condition' => 'category_id = :category_id',
				'params' => [':category_id' => $category_id],
			]
		);

		return $this;
	}

	/**
	 * @return string
	 */
	public function getCategoryName()
	{
		return ($this->category === null) ? '---' : $this->category->name;
	}
}
