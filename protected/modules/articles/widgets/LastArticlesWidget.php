<?php

/**
 * Виджет вывода последних статей
 **/
Yii::import('application.modules.articles.models.*');

/**
 * Class LastArticlesWidget
 */
class LastArticlesWidget extends webforma\widgets\WWidget
{
    /**
     * @var string
     */
    public $view = 'last-articles';

	/**
	 * @var int
	 */
    public $limit = 3;

	/**
	 * @var int
	 */
	public $width = 100;

	/**
	 * @var int
	 */
	public $height = 100;

	/**
	 * @var
	 */
	public $category_id;

    /**
     * @throws CException
     */
    public function run()
    {
        $criteria = new CDbCriteria();
        $criteria->limit = (int)$this->limit;
        $criteria->order = 'sort_id ASC';

        if($this->category_id){
        	$criteria->addCondition('t.category_id = '.(int) $this->category_id);
        }

        $articles = Articles::model()->published()->findAll($criteria);

        $this->render($this->view, [
        	'models' => $articles,
	        'width' => $this->width,
	        'height' => $this->height,
        ]);
    }
}
