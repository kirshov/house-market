<?php

/**
 * Articles install migration
 * Класс миграций для модуля Articles
 **/
class m000000_000000_articles_base extends webforma\components\DbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{articles}}',
            [
                'id'            => 'pk',
                'create_time'   => 'datetime NOT NULL',
                'update_time'   => 'datetime NOT NULL',
                'title'         => 'varchar(250) NOT NULL',
                'slug'          => 'varchar(150) NOT NULL',
                'short_text'    => 'text',
                'full_text'     => 'text NOT NULL',
                'image'         => 'varchar(300) DEFAULT NULL',
                'link'          => 'varchar(300) DEFAULT NULL',
                'user_id'       => 'integer DEFAULT NULL',
                'status'        => "integer NOT NULL DEFAULT '0'",
                'is_protected'  => "boolean NOT NULL DEFAULT '0'",
                'keywords'      => 'varchar(250) NOT NULL',
                'description'   => 'varchar(250) NOT NULL',
	            'meta_title'    => 'varchar(250) NOT NULL',
            ],
            $this->getOptions()
        );

        $this->createIndex("ix_{{articles}}_status", '{{articles}}', "status", false);
        $this->createIndex("ix_{{articles}}_user_id", '{{articles}}', "user_id", false);

        //fk
        $this->addForeignKey(
            "fk_{{articles}}_user_id",
            '{{articles}}',
            'user_id',
            '{{user_user}}',
            'id',
            'SET NULL',
            'NO ACTION'
        );
    }

    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{articles}}');
    }
}
