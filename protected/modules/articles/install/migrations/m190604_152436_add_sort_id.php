<?php

class m190604_152436_add_sort_id extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{articles}}', 'sort_id', 'int(11) default 0');
		$this->update('{{articles}}', ['sort_id' => 'id']);
	}

	public function safeDown()
	{

	}
}