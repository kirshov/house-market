<?php

class m190208_053432_add_category_id extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{articles}}', 'category_id', 'integer DEFAULT NULL');

		$this->addForeignKey(
			"fk_{{articles}}_category_id",
			'{{articles}}',
			'category_id',
			'{{category_category}}',
			'id',
			'SET NULL',
			'NO ACTION'
		);
	}

	public function safeDown()
	{

	}
}