<?php

class m190205_115530_add_tags extends webforma\components\DbMigration
{
	public function safeUp()
	{
		// tags
		$this->createTable('{{articles_tag}}', [
				'id'   => 'pk',
				'name' => 'varchar(255) NOT NULL',
			], $this->getOptions()
		);

		//ix
		$this->createIndex("ux_{{articles_tag}}_tag_name", '{{articles_tag}}', "name", true);

		// post to tag
		$this->createTable('{{articles_to_tag}}', [
				'article_id' => 'integer NOT NULL',
				'tag_id'  => 'integer NOT NULL',
				'PRIMARY KEY (article_id, tag_id)'
			],
			$this->getOptions()
		);

		//ix
		$this->createIndex("ix_{{articles_to_tag}}_article_id", '{{articles_to_tag}}', "article_id", false);
		$this->createIndex("ix_{{articles_to_tag}}_tag_id", '{{articles_to_tag}}', "tag_id", false);

		//fk
		$this->addForeignKey("fk_{{articles_to_tag}}_article_id", '{{articles_to_tag}}', 'article_id', '{{articles}}', 'id', 'CASCADE', 'NO ACTION');
		$this->addForeignKey("fk_{{articles_to_tag}}_tag_id", '{{articles_to_tag}}', 'tag_id', '{{articles_tag}}', 'id', 'CASCADE', 'NO ACTION' );
	}

	public function safeDown()
	{

	}
}