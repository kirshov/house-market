<?php

/**
 * ArticlesController контроллер для работы со статьями в публичной части сайта
 *
 */
class ArticlesController extends \webforma\components\controllers\FrontController
{
    /**
     * @param $slug
     * @throws CHttpException
     */
    public function actionView($slug)
    {
        $model = Articles::model()->published();

        $model = $model->find('slug = :slug', [':slug' => $slug]);

        if (!$model) {
            throw new CHttpException(404, Yii::t('ArticlesModule.articles', 'Articles article was not found!'));
        }

        // проверим что пользователь может просматривать эту статью
        if ($model->isProtected() && !Yii::app()->getUser()->isAuthenticated()) {
            Yii::app()->getUser()->setFlash(
                webforma\widgets\WFlashMessages::ERROR_MESSAGE,
                Yii::t('ArticlesModule.articles', 'You must be an authorized user for view this page!')
            );

            $this->redirect([Yii::app()->getModule('user')->accountActivationSuccess]);
        }

        $this->render('view', ['model' => $model]);
    }

    /**
     *
     */
    public function actionIndex()
    {
        $dbCriteria = new CDbCriteria([
            'condition' => 't.status = :status',
            'params' => [
                ':status' => Articles::STATUS_PUBLISHED,
            ],
            'order' => 't.create_time DESC',
            'with' => ['user'],
        ]);

        if (!Yii::app()->getUser()->isAuthenticated()) {
            $dbCriteria->mergeWith(
                [
                    'condition' => 'is_protected = :is_protected',
                    'params' => [
                        ':is_protected' => Articles::PROTECTED_NO,
                    ],
                ]
            );
        }

        $categoryId = Yii::app()->getRequest()->getParam('category');
	    $categoryModel = false;
        if(is_numeric($categoryId)){
	        $categoryModel = Category::model()->findByPk($categoryId);
	        $categoryId = (int) $categoryId;
	        if (!$categoryModel) {
		        throw new CHttpException(404, Yii::t('ArticlesModule.articles', 'Requested page was not found!'));
	        }

	        $dbCriteria->addCondition('category_id = '.$categoryId);
        }

        $dataProvider = new CActiveDataProvider('Articles', [
            'criteria' => $dbCriteria,
            'pagination' => [
                'pageSize' => (int)$this->getModule()->perPage,
            ],
        ]);

        $this->render('index', [
        	'dataProvider' => $dataProvider,
	        'categoryName' => $categoryModel ? $categoryModel->name : false,
        ]);
    }
}
