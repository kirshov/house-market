<?php
class ArticlesEvents
{
    const ARTICLES_AFTER_SAVE = 'articles.after.save';
    const ARTICLES_AFTER_DELETE = 'articles.after.delete';
}