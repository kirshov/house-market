<?php
/**
 * @var $this ArticlesBackendController
 * @var $model Articles
 * @var $form \webforma\widgets\ActiveForm
 */
if($model->getIsNewRecord() && !Yii::app()->getRequest()->getIsPostRequest()){
	$model->status = Articles::STATUS_PUBLISHED;
}
?>
<ul class="nav nav-tabs horizontal-tabs">
    <li class="active"><a href="#common" data-toggle="tab"><?= Yii::t("ArticlesModule.articles", "General"); ?></a></li>
    <li><a href="#seo" data-toggle="tab"><?= Yii::t("ArticlesModule.articles", "SEO"); ?></a></li>
</ul>

<?php
$form = $this->beginWidget(
    '\webforma\widgets\ActiveForm',
    [
        'id' => 'articles-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'type' => 'vertical',
        'htmlOptions' => ['class' => 'well', 'enctype' => 'multipart/form-data'],
    ]
); ?>


<?= $form->errorSummary($model); ?>

<div class="tab-content">
    <div class="tab-pane active" id="common">

        <div class="alert alert-info">
            <?= Yii::t('ArticlesModule.articles', 'Fields with'); ?>
            <span class="required">*</span>
            <?= Yii::t('ArticlesModule.articles', 'are required'); ?>
        </div>

		<div class="row">
			<div class="col-sm-6">
				<div class="row">
					<div class="col-sm-12">
						<?= $form->textFieldGroup($model, 'title'); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<?if($model->isNewRecord){
							echo $form->slugFieldGroup($model, 'slug', ['sourceAttribute' => 'title']);
						} else {
							echo $form->textFieldGroup($model, 'slug');
						}?>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-6">
						<?= $form->dropDownListGroup($model, 'status', [
							'widgetOptions' => [
								'data' => $model->getStatusList(),
							],
						]); ?>
					</div>

					<?if($this->getModule()->useCategory):?>
						<div class="row">
							<div class="col-sm-6">
								<?=  $form->dropDownListGroup($model, 'category_id', [
									'widgetOptions' => [
										'data'        => Yii::app()->getComponent('categoriesRepository')->getFormattedList($this->getModule()->mainCategory),
										'htmlOptions' => ['empty' => Yii::t('ArticlesModule.articles', '--choose--'), 'encode' => false],
									],
								]); ?>
							</div>
						</div>
					<?endif;?>
				</div>

				<?if($this->getModule()->useTags):?>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<?= $form->labelEx($model, 'tags', ['control-label']); ?>
								<div class="input-group">
									<?php $this->widget('booster.widgets.TbSelect2', [
										'asDropDownList' => false,
										'name' => 'tags',
										'options' => [
											'tags' => array_values(CHtml::listData(ArticlesTag::model()->findAll(), 'id', 'name')),
											'placeholder' => 'тэги',
										],
										'htmlOptions' => [
											'class' => '',
										],
										'value' => implode(", ", $model->getTags()),
									]); ?>
								</div>
							</div>
						</div>
					</div>
				<?endif;?>
			</div>

			<div class="col-sm-6">
				<div class='row'>
					<div class="col-sx-8 col-sm-8 col-sm-offset-2">
						<?php $this->widget('webforma\widgets\WInputFile', [
							'model' => $model,
							'attribute' => 'image',
						]); ?>
					</div>
				</div>
			</div>
		</div>

        <div class="row">
            <div class="col-sm-12 <?= $model->hasErrors('full_text') ? 'has-error' : ''; ?>">
                <?= $form->labelEx($model, 'full_text'); ?>
                <?php $this->widget($this->module->getVisualEditor(), [
					'model' => $model,
					'attribute' => 'full_text',
				]); ?>
                <span class="help-block"><?= Yii::t('ArticlesModule.articles', 'Full text articles which will be shown on articles article page'); ?></span>
                <?= $form->error($model, 'full_text'); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?= $form->labelEx($model, 'short_text'); ?>
                <?php $this->widget($this->module->getVisualEditor(), [
					'model' => $model,
					'attribute' => 'short_text',
				]); ?>
                <span class="help-block"><?= Yii::t('ArticlesModule.articles', 'Articles anounce text. Usually this is the main idea of the article.'); ?></span>
                <?= $form->error($model, 'short_text'); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <?= $form->checkBoxGroup($model, 'is_protected', $model->getProtectedStatusList()); ?>
            </div>
        </div>
    </div>

    <div class="tab-pane" id="seo">
        <div class="row">
            <div class="col-sm-6">
                <?= $form->textFieldGroup($model, 'meta_title'); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <?= $form->textFieldGroup($model, 'keywords'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->textAreaGroup($model, 'description'); ?>
            </div>
        </div>
    </div>
</div>

<div class="buttons">
	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context' => 'primary',
		'label' => $model->isNewRecord ? Yii::t('ArticlesModule.articles', 'Create article and continue') : Yii::t(
			'ArticlesModule.articles',
			'Save articles article and continue'
		),
	]); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label' => $model->isNewRecord ? Yii::t('ArticlesModule.articles', 'Create article and close') : Yii::t(
			'ArticlesModule.articles',
			'Save articles article and close'
		),
	]); ?>
</div>

<?php $this->endWidget(); ?>
