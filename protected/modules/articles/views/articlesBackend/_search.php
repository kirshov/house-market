<?php
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
        'type' => 'vertical',
        'htmlOptions' => ['class' => 'well'],
    ]
); ?>

<div class="row">
    <div class="col-sm-4">
        <?= $form->textFieldGroup($model, 'title'); ?>
    </div>
    <div class="col-sm-4">
        <?= $form->textFieldGroup($model, 'slug'); ?>
    </div>
	<div class="col-sm-4">
		<?= $form->dropDownListGroup($model, 'status', [
			'widgetOptions' => [
				'data' => $model->getStatusList(),
				'htmlOptions' => [
					'empty' => Yii::t('ArticlesModule.articles', '-no matter-'),
				],
			],
		]); ?>
	</div>
</div>

<div class="row">
    <div class="col-sm-4">
        <?= $form->textFieldGroup($model, 'short_text'); ?>
    </div>
    <div class="col-sm-4">
        <?= $form->textFieldGroup($model, 'full_text'); ?>
    </div>
	<div class="col-sm-4">
		<?= $form->dropDownListGroup($model, 'is_protected', [
			'widgetOptions' => [
				'data' => $model->getProtectedStatusList(),
				'htmlOptions' => [
					'empty' => '---',
				],
			],
		]); ?>
	</div>
</div>

<?php $this->widget('bootstrap.widgets.TbButton', [
	'context' => 'primary',
	'encodeLabel' => false,
	'buttonType' => 'submit',
	'label' => '<i class="fa fa-search">&nbsp;</i> ' . Yii::t('ArticlesModule.articles', 'Find article'),
]); ?>

<?php $this->widget('bootstrap.widgets.TbButton', [
	'buttonType' => 'reset',
	'context'    => 'danger',
	'encodeLabel' => false,
	'label'       => '<i class="fa fa-times">&nbsp;</i> Очистить форму',
]); ?>

<?php $this->endWidget(); ?>
