<?php

/**
 * @var $model Articles
 * @var $this ArticlesBackendController
 */

$this->breadcrumbs = [
    Yii::t('ArticlesModule.articles', 'Articles') => ['/articles/articlesBackend/index'],
    Yii::t('ArticlesModule.articles', 'Management'),
];

$this->pageTitle = Yii::t('ArticlesModule.articles', 'Articles - management');

$this->menu = [
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('ArticlesModule.articles', 'Articles management'),
        'url' => ['/articles/articlesBackend/index'],
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('ArticlesModule.articles', 'Create articles'),
        'url' => ['/articles/articlesBackend/create'],
    ],
	[
		'icon' => 'fa fa-fw fa-external-link-square',
		'label' => Yii::t('WebformaModule.webforma', 'To the section on the site'),
		'url' => ['/articles/articles/index'],
		'linkOptions' => [
			'target' => '_blank',
		],
	],
];
?>
<p class="search-wrap">
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?= Yii::t('ArticlesModule.articles', 'Find articles'); ?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php
    Yii::app()->clientScript->registerScript(
        'search',
        "
    $('.search-form form').submit(function () {
        $.fn.yiiGridView.update('articles-grid', {
            data: $(this).serialize()
        });

        return false;
    });
"
    );
    $this->renderPartial('_search', ['model' => $model]);
    ?>
</div>

<?php
$useCategory = $this->getModule()->useCategory;
$mainCategory = $this->getModule()->mainCategory;

$this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'articles-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
	    'sortableRows'      => true,
	    'sortableAjaxSave'  => true,
	    'sortableAttribute' => 'sort_id',
	    'sortableAction'    => '/articles/articlesBackend/sortable',
        'columns' => [
			[
				'type' => 'raw',
				'value' => function ($data) {
					if($data->image) {
						return CHtml::image($data->getImageUrl(100, 100), $data->title, ["width" => 100, "height" => 100, "class" => "img-thumbnail"]);
					}
				},
				'htmlOptions' => [
					'style' => 'width: 120px;',
				],
				'filter' => CHtml::activeTextField($model, 'id', ['class' => 'form-control']),
			],
            [
                'class' => 'webforma\widgets\WEditableColumn',
                'name' => 'title',
                'editable' => [
                    'url' => $this->createUrl('/articles/articlesBackend/inline'),
                    'mode' => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($model, 'title', ['class' => 'form-control']),
            ],
            [
                'class' => 'webforma\widgets\WEditableColumn',
                'name' => 'slug',
                'editable' => [
                    'url' => $this->createUrl('/articles/articlesBackend/inline'),
                    'mode' => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($model, 'slug', ['class' => 'form-control']),
            ],
			[
				'name'   => 'category_id',
				'value'  => '$data->getCategoryName()',
				'filter' => CHtml::activeDropDownList(
					$model,
					'category_id',
					Yii::app()->getComponent('categoriesRepository')->getFormattedList($mainCategory),
					['encode' => false, 'empty' => '', 'class' => 'form-control']
				),
				'visible' => $useCategory,
			],
            [
                'class' => 'webforma\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('/articles/articlesBackend/inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    Articles::STATUS_PUBLISHED => ['class' => 'label-success'],
                    Articles::STATUS_MODERATION => ['class' => 'label-warning'],
                    Articles::STATUS_DRAFT => ['class' => 'label-default'],
                ],
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
                'frontViewButtonUrl' => function ($data) {
                    return Yii::app()->createUrl('/articles/articles/view', ['slug' => $data->slug]);
                },
                'buttons' => [
                    'front_view' => [
                        'visible' => function ($row, $data) {
                            return $data->status == Articles::STATUS_PUBLISHED;
                        },
                    ],
                ],
            ],
        ],
    ]
); ?>
