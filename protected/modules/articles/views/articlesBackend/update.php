<?php
$this->breadcrumbs = [
    Yii::t('ArticlesModule.articles', 'Articles') => ['/articles/articlesBackend/index'],
    $model->title => ['/articles/articlesBackend/view', 'id' => $model->id],
    Yii::t('ArticlesModule.articles', 'Edit'),
];

$this->pageTitle = Yii::t('ArticlesModule.articles', 'Articles - edit');

$this->menu = [
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('ArticlesModule.articles', 'Articles management'),
        'url' => ['/articles/articlesBackend/index'],
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('ArticlesModule.articles', 'Create articles'),
        'url' => ['/articles/articlesBackend/create'],
    ],
    ['label' => Yii::t('ArticlesModule.articles', 'Articles Article') . ' «' . mb_substr($model->title, 0, 32) . '»'],
    [
        'icon' => 'fa fa-fw fa-pencil',
        'label' => Yii::t('ArticlesModule.articles', 'Edit articles'),
        'url' => [
            '/articles/articlesBackend/update/',
            'id' => $model->id,
        ],
    ],
    [
        'icon' => 'fa fa-fw fa-eye',
        'label' => Yii::t('ArticlesModule.articles', 'View articles article'),
        'url' => [
            '/articles/articlesBackend/view',
            'id' => $model->id,
        ],
    ],
    [
        'icon' => 'fa fa-fw fa-trash-o',
        'label' => Yii::t('ArticlesModule.articles', 'Removing articles'),
        'url' => '#',
        'linkOptions' => [
            'submit' => ['/articles/articlesBackend/delete', 'id' => $model->id],
            'params' => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
            'confirm' => Yii::t('ArticlesModule.articles', 'Do you really want to remove the article?'),
            'csrf' => true,
        ],
    ],
];
?>

<?= $this->renderPartial(
    '_form',
    ['model' => $model]
); ?>
