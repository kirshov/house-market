<?php
$this->breadcrumbs = [
    Yii::t('ArticlesModule.articles', 'Articles') => ['/articles/articlesBackend/index'],
    Yii::t('ArticlesModule.articles', 'Create'),
];

$this->pageTitle = Yii::t('ArticlesModule.articles', 'Articles - create');

$this->menu = [
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('ArticlesModule.articles', 'Articles management'),
        'url' => ['/articles/articlesBackend/index'],
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('ArticlesModule.articles', 'Create articles'),
        'url' => ['/articles/articlesBackend/create'],
    ],
];
?>

<?= $this->renderPartial('_form', ['model' => $model]); ?>
