<?php
$this->breadcrumbs = [
    Yii::t('ArticlesModule.articles', 'Articles') => ['/articles/articlesBackend/index'],
    $model->title,
];

$this->pageTitle = Yii::t('ArticlesModule.articles', 'Articles - show');

$this->menu = [
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('ArticlesModule.articles', 'Articles management'),
        'url' => ['/articles/articlesBackend/index'],
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('ArticlesModule.articles', 'Create articles'),
        'url' => ['/articles/articlesBackend/create'],
    ],
    ['label' => Yii::t('ArticlesModule.articles', 'Articles Article') . ' «' . mb_substr($model->title, 0, 32) . '»'],
    [
        'icon' => 'fa fa-fw fa-pencil',
        'label' => Yii::t('ArticlesModule.articles', 'Edit articles'),
        'url' => [
            '/articles/articlesBackend/update',
            'id' => $model->id,
        ],
    ],
    [
        'icon' => 'fa fa-fw fa-eye',
        'label' => Yii::t('ArticlesModule.articles', 'View articles article'),
        'url' => [
            '/articles/articlesBackend/view',
            'id' => $model->id,
        ],
    ],
    [
        'icon' => 'fa fa-fw fa-trash-o',
        'label' => Yii::t('ArticlesModule.articles', 'Remove articles'),
        'url' => '#',
        'linkOptions' => [
            'submit' => ['/articles/articlesBackend/delete', 'id' => $model->id],
            'params' => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
            'confirm' => Yii::t('ArticlesModule.articles', 'Do you really want to remove the article?'),
            'csrf' => true,
        ],
    ],
];
?>

<div class="page-header">
    <h1>
        <?= Yii::t('ArticlesModule.articles', 'Show articles article'); ?><br/>
        <small>&laquo;<?= $model->title; ?>&raquo;</small>
    </h1>
</div>

<ul class="nav nav-tabs">
    <li class="active"><a href="#anounce" data-toggle="tab"><?= Yii::t(
                'ArticlesModule.articles',
                'Short articles article example'
            ); ?></a></li>
    <li><a href="#full" data-toggle="tab"><?= Yii::t('ArticlesModule.articles', 'Full articles article example'); ?></a></li>
</ul>
<div class="tab-content">
    <div id="anounce" class="tab-pane fade active in">
        <div style="margin-bottom: 20px;">
            <h6>
                <?= CHtml::link($model->title, ['/articles/articles/view', 'slug' => $model->slug]); ?>
            </h6>

            <p>
                <?= $model->short_text; ?>
            </p>
            <i class="fa fa-fw fa-globe"></i><?= CHtml::link(
                Yii::app()->createAbsoluteUrl('/articles/articles/view', ['slug' => $model->slug]),
                Yii::app()->createAbsoluteUrl('/articles/articles/view', ['slug' => $model->slug])
            ); ?>
        </div>
    </div>
    <div id="full" class="tab-pane fade">
        <div style="margin-bottom: 20px;">
            <h3><?= CHtml::link(CHtml::encode($model->title), ['/articles/articles/view', 'slug' => $model->slug]); ?></h3>
            <?php if ($model->image): ?>
                <?= CHtml::image($model->getImageUrl(), $model->title); ?>
            <?php endif; ?>
            <p><?= $model->full_text; ?></p>
            <i class="fa fa-fw fa-user"></i><?= CHtml::link(
                $model->user->fullName,
                ['/user/people/' . $model->user->nick_name]
            ); ?>
            <i class="fa fa-fw fa-globe"></i><?= CHtml::link(
                Yii::app()->createAbsoluteUrl('/articles/articles/view', ['slug' => $model->slug]),
                Yii::app()->createAbsoluteUrl('/articles/articles/view', ['slug' => $model->slug])
            ); ?>
        </div>
    </div>
</div>
