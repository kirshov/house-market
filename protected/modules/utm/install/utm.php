<?php
/**
 * Файл настроек для модуля utm
 */
return [
    'module'    => [
        'class' => 'application.modules.utm.UtmModule',
    ],
    'import'    => [],
    'component' => [
    	'utm' => [
    		'class' => 'application.modules.utm.components.Utm',
		],
	],
    'rules'     => [],
];