<?php
/**
 * Utm install migration
 * Класс миграций для модуля Utm:
 **/
class m000000_000000_utm_base extends webforma\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        $this->createTable(
            '{{utm}}',
            [
                'id' => 'pk',
                'type' => 'varchar(32) not null',
                'key' => 'varchar(128) not null',
                'value' => 'varchar(128) not null',
            ],
            $this->getOptions()
        );

        $this->createIndex('ix_{{utm}}_type_key', '{{utm}}', ['type', 'key'], true);

    }

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{utm}}');
    }
}
