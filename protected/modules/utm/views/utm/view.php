<?php
/**
 * Отображение для view
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('UtmModule.utm', 'Utm метки') => ['/utm/utm/index'],
    $model->id,
];

$this->pageTitle = Yii::t('UtmModule.utm', 'Utm метки - просмотр');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('UtmModule.utm', 'Управление UTM метками'), 'url' => ['/utm/utm/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('UtmModule.utm', 'Добавить UTM метку'), 'url' => ['/utm/utm/create']],
    ['label' => Yii::t('UtmModule.utm', 'Utm метка') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('UtmModule.utm', 'Редактирование UTM метку'), 'url' => [
        '/utm/utm/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('UtmModule.utm', 'Просмотреть UTM метку'), 'url' => [
        '/utm/utm/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('UtmModule.utm', 'Удалить UTM метку'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/utm/utm/delete', 'id' => $model->id],
        'confirm' => Yii::t('UtmModule.utm', 'Вы уверены, что хотите удалить UTM метку?'),
        'csrf' => true,
    ]],
];
?>
<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data'       => $model,
    'attributes' => [
        'id',
        'type',
        'key',
        'value',
    ],
]); ?>
