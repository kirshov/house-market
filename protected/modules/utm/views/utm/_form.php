<?php
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm', [
        'id' => 'utm-form',
	 	'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'type' => 'vertical',
        'htmlOptions' => ['enctype' => 'multipart/form-data', 'class' => 'well sticky'],
        'clientOptions' => [
            'validateOnSubmit' => true,
        ],
    ]
);
?>

<div class="alert alert-info">
    <?=  Yii::t('UtmModule.utm', 'Поля, отмеченные'); ?>
    <span class="required">*</span>
    <?=  Yii::t('UtmModule.utm', 'обязательны.'); ?>
</div>

<?=  $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-sm-6">
            <?=  $form->textFieldGroup($model, 'type'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?=  $form->textFieldGroup($model, 'key'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?=  $form->textFieldGroup($model, 'value'); ?>
        </div>
    </div>

<div class="buttons">
    <?php $this->widget(
        'bootstrap.widgets.TbButton', [
            'buttonType' => 'submit',
            'context'    => 'primary',
            'label'      => Yii::t('UtmModule.utm', 'Сохранить UTM метку и продолжить'),
        ]
    ); ?>
    <?php $this->widget(
        'bootstrap.widgets.TbButton', [
            'buttonType' => 'submit',
            'htmlOptions'=> ['name' => 'submit-type', 'value' => 'index'],
            'label'      => Yii::t('UtmModule.utm', 'Сохранить UTM метку и закрыть'),
        ]
    ); ?>

</div>
<?php $this->endWidget(); ?>