<?php
/**
 * Отображение для create:
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('UtmModule.utm', 'Utm метки') => ['/utm/utm/index'],
    Yii::t('UtmModule.utm', 'Добавление'),
];

$this->pageTitle = Yii::t('UtmModule.utm', 'Utm метки - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('UtmModule.utm', 'Управление UTM метками'), 'url' => ['/utm/utm/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('UtmModule.utm', 'Добавить UTM метку'), 'url' => ['/utm/utm/create']],
];
?>
<?=  $this->renderPartial('_form', ['model' => $model]); ?>