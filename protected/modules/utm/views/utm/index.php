<?php
/**
 * Отображение для index
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('UtmModule.utm', 'Utm метки') => ['/utm/utm/index'],
    Yii::t('UtmModule.utm', 'Управление'),
];

$this->pageTitle = Yii::t('UtmModule.utm', 'Utm метки - управление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('UtmModule.utm', 'Управление UTM метками'), 'url' => ['/utm/utm/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('UtmModule.utm', 'Добавить UTM метку'), 'url' => ['/utm/utm/create']],
];
?><p class="search-wrap">
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?=  Yii::t('UtmModule.utm', 'Поиск UTM меток');?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
        <?php Yii::app()->clientScript->registerScript('search', "
        $('.search-form form').submit(function () {
            $.fn.yiiGridView.update('utm-grid', {
                data: $(this).serialize()
            });

            return false;
        });
    ");
    $this->renderPartial('_search', ['model' => $model]);
?>
</div>

<?php
 $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id'           => 'utm-grid',
        'type'         => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => [
            'id',
            'type',
            'key',
            'value',
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
            ],
        ],
    ]
); ?>
