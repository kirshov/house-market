<?php
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm', [
        'action'      => Yii::app()->createUrl($this->route),
        'method'      => 'get',
        'type'        => 'vertical',
        'htmlOptions' => ['class' => 'well'],
    ]
);
?>

<fieldset>
    <div class="row">
        <div class="col-sm-3">
            <?=  $form->textFieldGroup($model, 'id'); ?>
        </div>
		<div class="col-sm-3">
            <?=  $form->textFieldGroup($model, 'type'); ?>
        </div>
		<div class="col-sm-3">
            <?=  $form->textFieldGroup($model, 'key'); ?>
        </div>
		<div class="col-sm-3">
            <?=  $form->textFieldGroup($model, 'value'); ?>
        </div>
		    </div>
</fieldset>

    <?php $this->widget('bootstrap.widgets.TbButton', [
		'context'     => 'primary',
		'encodeLabel' => false,
		'buttonType'  => 'submit',
		'label'       => '<i class="fa fa-search">&nbsp;</i> ' . Yii::t('UtmModule.utm', 'Искать UTM метку'),
	]); ?>


	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'reset',
		'context'    => 'danger',
		'encodeLabel' => false,
		'label'       => '<i class="fa fa-times">&nbsp;</i> Очистить форму',
	]); ?>

<?php $this->endWidget(); ?>