<?php
class Utm{
	/**
	 *
	 */
	const UTM_SOURCE  = 'utm_source';

	/**
	 *
	 */
	const UTM_MEDIUM  = 'utm_medium';

	/**
	 *
	 */
	const UTM_CAMPAIGN  = 'utm_campaign';

	/**
	 *
	 */
	const UTM_CONTENT  = 'utm_content';

	/**
	 *
	 */
	const UTM_TERM  = 'utm_term';

	/**
	 *
	 */
	const REFERER = 'referer';

	/**
	 *
	 */
	const DATE = 'date';

	/**
	 * @var string
	 */
	protected $cookieKey = 'utm';

	/**
	 *
	 */
	public function init(){
		$request = Yii::app()->getRequest();

		$utm = [];
		foreach (self::getMarks() as $mark){
			if($request->getParam($mark)){
				$utm[$mark] = $request->getParam($mark);
			}
		}

		if(!empty($utm)){
			if($request->getUrlReferrer()){
				$utm[self::REFERER] = $request->getUrlReferrer();
			}

			$utm[self::DATE] = date('d.m.Y H:i:s');
			\webforma\components\WCookie::setCookie($this->cookieKey, json_encode($utm));
		}
	}

	/**
	 * @return bool
	 */
	public function has(){
		return !empty($this->getFromCookie());
	}

	/**
	 * @return array
	 */
	public function get(){
		$utm = $this->getFromCookie();
		if(!$utm){
			return [];
		}

		$data = [];
		$list = $this->getFullList();
		foreach ($utm as $key => $value){
			$data[$list[$key]] = $value;
		}

		return $data;
	}

	/**
	 * @return array
	 */
	protected function getFromCookie(){
		if(!\webforma\components\WCookie::hasCookie($this->cookieKey)){
			return [];
		}

		$utm = json_decode(\webforma\components\WCookie::getCookie($this->cookieKey), true);

		if(!is_array($utm)){
			return [];
		}

		return $utm;
	}

	/**
	 * @return array
	 */
	public function getList(){
		return [
			self::UTM_SOURCE => 'Источник кампании',
			self::UTM_MEDIUM => 'Тип трафика',
			self::UTM_CAMPAIGN => 'Название кампании',
			self::UTM_CONTENT => 'Идентификатор объявления',
			self::UTM_TERM => 'Ключевое слово',
		];
	}

	/**
	 * @return array
	 */
	public function getFullList(){
		$data = $this->getList();

		foreach ($data as $key => $item){
			$data[$key] = $item.' ('.$key.')';
		}
		$data[self::REFERER] = 'Источник запроса ('.self::REFERER.')';
		$data[self::DATE] = 'Дата перехода';

		return $data;
	}

	/**
	 * @return array
	 */
	public function getMarks(){
		return array_flip($this->getList());
	}
}