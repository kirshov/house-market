<?php
/**
* UtmBackendController контроллер для utm в панели управления
*/

class UtmBackendController extends \webforma\components\controllers\BackController
{
	/**
	 * Отображает UTM метку по указанному идентификатору
	 *
	 * @param integer $id Идинтификатор UTM метку для отображения
	 *
	 * @return void
	 */
	public function actionView($id)
	{
		$this->render('view', ['model' => $this->loadModel($id)]);
	}

	/**
	 * Создает новую модель UTM метку.
	 * Если создание прошло успешно - перенаправляет на просмотр.
	 *
	 * @return void
	 */
	public function actionCreate()
	{
		$model = new Utm;

		if (Yii::app()->getRequest()->getPost('Utm') !== null) {
			$model->setAttributes(Yii::app()->getRequest()->getPost('Utm'));

			if ($model->save()) {
				Yii::app()->user->setFlash(
					webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
					Yii::t('UtmModule.utm', 'Запись добавлена!')
				);

				$this->redirect(
					(array)Yii::app()->getRequest()->getPost(
						'submit-type',
						[
							'update',
							'id' => $model->id
						]
					)
				);
			}
		}
		$this->render('create', ['model' => $model]);
	}

	/**
	 * Редактирование UTM метку.
	 *
	 * @param integer $id Идинтификатор UTM метку для редактирования
	 *
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		if (Yii::app()->getRequest()->getPost('Utm') !== null) {
			$model->setAttributes(Yii::app()->getRequest()->getPost('Utm'));

			if ($model->save()) {
				Yii::app()->user->setFlash(
					webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
					Yii::t('UtmModule.utm', 'Запись обновлена!')
				);

				$this->redirect(
					(array)Yii::app()->getRequest()->getPost(
						'submit-type',
						[
							'update',
							'id' => $model->id
						]
					)
				);
			}
		}
		$this->render('update', ['model' => $model]);
	}

	/**
	 * Удаляет модель UTM метку из базы.
	 * Если удаление прошло успешно - возвращется в index
	 *
	 * @param integer $id идентификатор UTM метку, который нужно удалить
	 *
	 * @return void
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			// поддерживаем удаление только из POST-запроса
			$this->loadModel($id)->delete();

			Yii::app()->user->setFlash(
				webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
				Yii::t('UtmModule.utm', 'Запись удалена!')
			);

			// если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
			if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
				$this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
			}
		} else
			throw new CHttpException(400, Yii::t('UtmModule.utm', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
	}

	/**
	 * Управление UTM метками.
	 *
	 * @return void
	 */
	public function actionIndex()
	{
		$model = new Utm('search');
		$model->unsetAttributes(); // clear any default values
		if (Yii::app()->getRequest()->getParam('Utm') !== null)
			$model->setAttributes(Yii::app()->getRequest()->getParam('Utm'));
		$this->render('index', ['model' => $model]);
	}

	/**
	 * Возвращает модель по указанному идентификатору
	 * Если модель не будет найдена - возникнет HTTP-исключение.
	 *
	 * @param integer идентификатор нужной модели
	 *
	 * @return void
	 */
	public function loadModel($id)
	{
		$model = Utm::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, Yii::t('UtmModule.utm', 'Запрошенная страница не найдена.'));

		return $model;
	}
}