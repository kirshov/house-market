<?php
/**
* Отображение для udsBackend/index
**/
$this->breadcrumbs = [
    Yii::t('UdsModule.uds', 'uds') => ['/uds/udsBackend/index'],
    Yii::t('UdsModule.uds', 'Index'),
];

$this->pageTitle = Yii::t('UdsModule.uds', 'uds - index');

$this->menu = $this->getModule()->getNavigation();
?>