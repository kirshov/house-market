<?php
/**
* Отображение для uds/index
**/
$this->pageTitle = Yii::t('UdsModule.uds', 'uds');
$this->description = Yii::t('UdsModule.uds', 'uds');
$this->keywords = Yii::t('UdsModule.uds', 'uds');

$this->breadcrumbs = [Yii::t('UdsModule.uds', 'uds')];
?>

<h1>
    <small>
        <?php echo Yii::t('UdsModule.uds', 'uds'); ?>
    </small>
</h1>