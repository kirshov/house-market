<?php
class uds extends CApplicationComponent {

	/**
	 * @var string
	 */
	protected $urlApi = 'https://udsgame.com/v1/';

	/**
	 *
	 */
	public function init()
	{
		parent::init();
	}

	/**
	 * @param $code
	 * @return array|bool
	 */
	public function getUserData($code){
		$data = $this->request($this->urlApi.'partner/customer?code='.$code);

		if(!$data){
			return false;
		}

		return [
			'name' => $data['name'].($data['surname'] ? ' '.$data['surname'] : ''),
			'scores' => $data['scores'],
			'userId' => 'id'
		];
	}

	/**
	 * @param $url
	 * @param string $method
	 * @param null $data
	 * @return bool|mixed
	 * @throws Exception
	 */
	private function request($url, $method = 'GET', $data = null){
		$date = new DateTime();
		$module = Yii::app()->getModule('uds');
		if(!$module->apiKey || !$module->UUID){
			return false;
		}

		$opts = [
			'http' => [
				'method' => $method,
				'header' => "Accept: application/json\r\n" .
					"Accept-Charset: utf-8\r\n" .
					"Content-Type: application/json\r\n" .
					"X-Api-Key: ".$module->apiKey."\r\n" .
					"X-Origin-Request-Id: ".$module->UUID."\r\n" .
					"X-Timestamp: ".$date->format(DateTime::ATOM),
			]
		];

		if($data){
			$opts['http']['content'] = $data;
			$opts['http']['ignore_errors'] = true;
		}

		$context = stream_context_create($opts);

		$result = @file_get_contents($url, false, $context);
		if(!is_dir(Yii::app()->getRuntimePath().'/logs/')){
			CFileHelper::createDirectory(Yii::app()->getRuntimePath().'/logs/', null, true);
		}
		file_put_contents(Yii::app()->getRuntimePath().'/logs/uds.log', date('d.m.Y').' '.$url."\n".print_r($opts, true)."\n".$result."\n\n", FILE_APPEND);
		return json_decode($result, true);
	}

	public function getCompanyData(){
		return $this->request( 'https://udsgame.com/v1/partner/company');
	}

	/**
	 * @param $orderId
	 * @param $code
	 * @param $total
	 * @param $score
	 * @return bool
	 * @throws Exception
	 */
	public function purchase($orderId, $code, $total, $score){
		$data = json_encode([
			'total' => round($total, 1),
			'cash' => round($total - $score, 1),
			'scores'=> round($score, 1),
			'code'=> $code,
			'invoiceNumber' => $orderId,
		]);

		$data = $this->request('https://udsgame.com/v1/partner/purchase', 'POST', $data);
		if($data['id']){
			return true;
		}

		return false;
	}
}