<?php
/**
 * UdsModule основной класс модуля uds
 */

class UdsModule extends webforma\components\WebModule
{

	public $adminMenuOrder = 300;

	/**
	 * @var
	 */
	public $apiKey;

	/**
	 * @var
	 */
	public $UUID;

    /**
     * Массив с именами модулей, от которых зависит работа данного модуля
     *
     * @return array
     */
    public function getDependencies()
    {
        return ['order'];
    }

    /**
     * Каждый модуль должен принадлежать одной категории, именно по категориям делятся модули в панели управления
     *
     * @return string
     */
    public function getCategory()
    {
        return 'Магазин';
    }

    /**
     * массив лейблов для параметров (свойств) модуля. Используется на странице настроек модуля в панели управления.
     *
     * @return array
     */
    public function getParamsLabels()
    {
        return [
	        'apiKey' => 'API Key',
	        'UUID' => 'UUID',
        ];
    }

    /**
     * массив параметров модуля, которые можно редактировать через панель управления (GUI)
     *
     * @return array
     */
    public function getEditableParams()
    {
        return [
        	'apiKey',
        	'UUID',
        ];
    }

    /**
     * массив групп параметров модуля, для группировки параметров на странице настроек
     *
     * @return array
     */
    public function getEditableParamsGroups()
    {
        return [
        	'main' => [
        		'label' => 'Общие настройки',
		        'items' => [
		        	'apiKey',
		        	'UUID',
		        ]
	        ]
        ];
    }

    /**
     * если модуль должен добавить несколько ссылок в панель управления - укажите массив
     *
     * @return array
     */
    public function getNavigation()
    {
        return [];
    }

    /**
     * Возвращает название модуля
     *
     * @return string.
     */
    public function getName()
    {
        return 'UDS Game';
    }

    /**
     *
     * @return string
     */
    public function getIcon()
    {
        return "fa fa-fw fa-circle-o-notch";
    }

    /**
     * Инициализация модуля, считывание настроек из базы данных и их кэширование
     *
     * @return void
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'uds.models.*',
                'uds.components.*',
            ]
        );
    }

	/**
	 * @return string
	 */
	public function getAdminPageLink()
	{
		$params = $this->getSettingsUrl();
		$uri = array_shift($params);
		return Yii::app()->createUrl($uri, $params);
	}
}
