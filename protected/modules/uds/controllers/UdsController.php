<?php
/**
* UdsController контроллер для uds на публичной части сайта
*/

class UdsController extends \webforma\components\controllers\FrontController
{
	/**
	 * @param $id
	 */
    public function actionGet($id)
    {
    	if(!$id){
    		Yii::app()->ajax->failure();
	    }

	    /**
	     * @var uds $uds
	     */
    	$uds = Yii::app()->getComponent('udsComponent');

    	$data = $uds->getUserData($id);

	    if(!$data){
		    Yii::app()->ajax->failure();
	    }

	    Yii::app()->ajax->success($data);
    }

	/**
	 *
	 */
    public function actionSet(){
    	$request = Yii::app()->getRequest();
    	$code = $request->getParam('code');
    	$discount = (int) $request->getParam('discount');
    	if(!$code || !$discount){
		    Yii::app()->ajax->failure();
	    }

	    $uds = Yii::app()->getComponent('udsComponent');

	    $data = $uds->getUserData($code);

	    if(!$data){
		    Yii::app()->ajax->failure();
	    }

	    $scores = $data['scores'];
	    if($discount > $scores){
		    $discount = $scores;
	    }

	    $result = [
		    'code' => $code,
		    'userId' => $data['userId'],
		    'discount' => $discount,
	    ];

    	Yii::app()->getUser()->setState('uds-data', $result);

	    Yii::app()->ajax->success($result);
    }
}