<?php
/**
* UdsBackendController контроллер для uds в панели управления
*/

class UdsBackendController extends \webforma\components\controllers\BackController
{
    /**
     * Действие "по умолчанию"
     *
     * @return void
     */
	public function actionIndex()
	{
		$this->render('index');
	}
}