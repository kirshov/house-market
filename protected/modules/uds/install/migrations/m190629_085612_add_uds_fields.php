<?php

class m190629_085612_add_uds_fields extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_order}}', 'uds_code', 'varchar(255) default null');
		$this->addColumn('{{store_order}}', 'uds_user', 'varchar(255) default null');
		$this->addColumn('{{store_order}}', 'uds_discount', 'varchar(255) default null');
		$this->addColumn('{{store_order}}', 'uds_apply', 'tinyint(1) default 0');
	}

	public function safeDown()
	{

	}
}