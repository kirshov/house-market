<?php
/**
 * Файл настроек для модуля uds
 */
return [
    'module'    => [
        'class' => 'application.modules.uds.UdsModule',
    ],
    'import'    => [],
    'component' => [
	    'udsComponent' => [
		    'class' => 'application.modules.uds.components.uds',
	    ],
    ],
    'rules'     => [
	    '/uds-game/<action:\w+>' => 'uds/uds/<action>',
	    '/uds-game/<action:\w+>/<id:\w+>' => 'uds/uds/<action>',
    ],
];