<?php
use webforma\components\Event;
use webforma\widgets\WPurifier;

/**
 * Page модель
 */

/**
 * This is the model class for table "Page".
 *
 * The followings are the available columns in table 'Page':
 *
 * @property integer $id
 * @property string $parent_id
 * @property integer $category_id
 * @property string $create_time
 * @property string $update_time
 * @property string $title
 * @property string $title_short
 * @property string $slug
 * @property string $body
 * @property string $short_body
 * @property string $keywords
 * @property string $description
 * @property integer $status
 * @property integer $is_protected
 * @property integer $user_id
 * @property integer $change_user_id
 * @property integer $order
 * @property string $layout
 * @property string $view
 * @property string $title_h1
 * @property string $meta_title
 * @property Page $parent
 *
 * @method string getPath
 * @method Page findByPath
 * @method Page published
 * @method Page publishedAdmin
 * @method Page protected
 * @method Page public
 * @method Page sitemapAllowed
 * @method Page prepareContent
 */
class Page extends webforma\models\WModel
{
    /**
     *
     */
    const STATUS_DRAFT = 0;
    /**
     *
     */
    const STATUS_PUBLISHED = 1;
    /**
     *
     */
    const STATUS_MODERATION = 2;

    /**
     *
     */
    const PROTECTED_NO = 0;
    /**
     *
     */
    const PROTECTED_YES = 1;

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className
     * @return Page the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{page_page}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['title, slug', 'required', 'on' => ['update', 'insert']],
            ['status, is_protected, order', 'numerical', 'integerOnly' => true, 'on' => ['update', 'insert']],
            ['parent_id, category_id', 'numerical', 'integerOnly' => true, 'allowEmpty' => true],
            ['parent_id, category_id', 'default', 'setOnEmpty' => true, 'value' => null],
            ['exclude_sitemap', 'boolean'],
            ['title_short, slug', 'length', 'max' => 150],
            ['title, keywords, description, layout, view, image', 'length', 'max' => 250],
            ['slug', 'unique'],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            ['is_protected', 'in', 'range' => array_keys($this->getProtectedStatusList())],
            ['title, title_short, slug, body, short_body, title_h1, description, keywords, meta_title', 'filter', 'filter' => 'trim'],
            ['title, title_short, slug, description, keywords', 'filter', 'filter' => [new WPurifier(), 'purify']],
            ['slug', 'webforma\components\validators\YSLugValidator'],
            [
                'id, parent_id, create_time, update_time, title, title_short, slug, body, short_body, keywords, description, status, order',
                'safe',
                'on' => 'search',
            ],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = [
            'CTimestampBehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ],
            'sortable' => [
                'class' => 'webforma\components\behaviors\SortableBehavior',
                'attributeName' => 'order',
            ],
	        'content' => [
		        'class' => 'webforma\components\behaviors\PrepareContentBehavior',
		        'attributeName' => 'body',
	        ],
			'tree' => [
				'class' => 'webforma\components\behaviors\DCategoryTreeBehavior',
				'aliasAttribute' => 'slug',
				'requestPathAttribute' => 'path',
				'parentAttribute' => 'parent_id',
				'parentRelation' => 'parent',
				'defaultCriteria' => [
					'order' => 't.order',
				],
				'titleAttribute' => 'title',
				'useCache' => true,
			],
        ];

        $module = Yii::app()->getModule('page');
        if($module->useImage){
			$behaviors['upload'] = [
				'class' => 'webforma\components\behaviors\ImageUploadBehavior',
				'attributeName' => 'image',
				'uploadPath' => 'page',
			];
		}

		return $behaviors;
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'childPages' => [self::HAS_MANY, 'Page', 'parent_id', 'condition' => 'childPages.status = '.self::STATUS_PUBLISHED, 'order' => 'childPages.order DESC, childPages.create_time DESC'],
            'parent' => [self::BELONGS_TO, 'Page', 'parent_id'],
            'author' => [self::BELONGS_TO, 'User', 'user_id'],
            'changeAuthor' => [self::BELONGS_TO, 'User', 'change_user_id'],
            'category' => [self::BELONGS_TO, 'Category', 'category_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('PageModule.page', 'Id'),
            'parent_id' => Yii::t('PageModule.page', 'Parent'),
            'category_id' => Yii::t('PageModule.page', 'Category'),
            'create_time' => Yii::t('PageModule.page', 'Created at'),
            'update_time' => Yii::t('PageModule.page', 'Changed'),
            'title' => Yii::t('PageModule.page', 'Title'),
            'title_short' => Yii::t('PageModule.page', 'Short title'),
            'slug' => Yii::t('PageModule.page', 'Url'),
            'exclude_sitemap' => 'Не добавлять в карту сайта',
            'body' => Yii::t('PageModule.page', 'Text'),
            'keywords' => Yii::t('PageModule.page', 'Keywords (SEO)'),
            'description' => Yii::t('PageModule.page', 'Description (SEO)'),
            'status' => Yii::t('PageModule.page', 'Status'),
            'is_protected' => Yii::t('PageModule.page', 'Access: * Only for authorized members'),
            'user_id' => Yii::t('PageModule.page', 'Created by'),
            'change_user_id' => Yii::t('PageModule.page', 'Changed by'),
            'order' => Yii::t('PageModule.page', 'Sorting'),
            'layout' => Yii::t('PageModule.page', 'Layout'),
            'view' => Yii::t('PageModule.page', 'View'),
            'title_h1' => 'Заголовок H1',
            'meta_title' => 'Заголовок title',
            'short_body' => 'Краткий текст',
            'image' => 'Изображение',
        ];
    }

    /**
     * @return array customized attribute descriptions (name=>description)
     */
    public function attributeDescriptions()
    {
        return [
            'parent_id' => Yii::t('PageModule.page', 'Parent page.'),
            'status' => '<span class=\'label label-success\'>Опубликовано</span> &ndash; Страницу видят все посетители сайта, режим по-умолчанию.<br /><br />
						<span class=\'label label-default\'>Черновик</span> &ndash; Данная страница видна только администратору.<br /><br />
						<span class=\'label label-info\'>Скрыто</span> &ndash; Данная страница не выводится на сайте.',
            'is_protected' => Yii::t('PageModule.page', 'Access: * Only for authorized members'),
            'order' => Yii::t('PageModule.page', 'Page priority in widgets and menu.'),
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (!$this->slug) {
            $this->slug = webforma\helpers\WText::translit($this->title);
        }

        return parent::beforeValidate();
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
    	if($this->change_user_id === null){
		    $this->change_user_id = Yii::app()->getUser()->getId();
	    }

        if ($this->getIsNewRecord()) {
            $this->user_id = $this->change_user_id;
        }

        return parent::beforeSave();
    }

    /**
     *
     */
    public function afterSave()
    {
        Yii::app()->eventManager->fire(PageEvents::PAGE_AFTER_SAVE, new Event($this));


        if($this->isNewRecord){
            SitemapHelper::remove();
        } else {
            foreach (['slug', 'status'] as $key){
                if($this->{$key} != $this->_oldData[$key]){
                    SitemapHelper::remove();
                    break;
                }
            }
        }

        Yii::app()->cache->delete(PageUrlRule::CACHE_KEY);
        \TaggedCache\TaggingCacheHelper::deleteTag(['pages', 'page-'.$this->id]);

        return parent::afterSave();
    }

	/**
	 *
	 */
	protected function afterDelete()
	{
		parent::afterDelete();

		Yii::app()->cache->delete(PageUrlRule::CACHE_KEY);
		\TaggedCache\TaggingCacheHelper::deleteTag(['pages', 'page-'.$this->id]);
	}


	/**
     * @return array
     */
    public function scopes()
    {
	    if(Yii::app()->getUser()->isSuperUser()){
	    	$published = [
			    'condition' => 'status != :status',
			    'params' => ['status' => self::STATUS_MODERATION],
		    ];
	    } else {
	    	$published = [
			    'condition' => 'status = :status',
			    'params' => ['status' => self::STATUS_PUBLISHED],
		    ];
	    }

        return [
            'published' => $published,
            'protected' => [
                'condition' => 'is_protected = :is_protected',
                'params' => [':is_protected' => self::PROTECTED_YES],
            ],
            'public' => [
                'condition' => 'is_protected = :is_protected',
                'params' => [':is_protected' => self::PROTECTED_NO],
            ],
	        'sitemapAllowed' => [
		        'condition' => 'exclude_sitemap = 0',
	        ],
        ];
    }

    /**
     * @param $slug
     * @return static
     */
    public function findBySlug($slug)
    {
        return $this->find('slug = :slug', [':slug' => trim($slug)]);
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria();

        $criteria->with = ['author', 'changeAuthor'];

        $criteria->compare('t.id', $this->id);
        $criteria->compare('parent_id', $this->parent_id);
        $criteria->compare('create_time', $this->create_time);
        $criteria->compare('update_time', $this->update_time);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('title_short', $this->title_short, true);
        $criteria->compare('slug', $this->slug, true);
        $criteria->compare('body', $this->body);
        $criteria->compare('short_body', $this->short_body);
        $criteria->compare('keywords', $this->keywords);
        $criteria->compare('description', $this->description);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('category_id', $this->category_id);
        $criteria->compare('is_protected', $this->is_protected);
        $criteria->compare('layout', $this->layout);

        return new CActiveDataProvider(
            get_class($this), [
                'criteria' => $criteria,
                'sort' => ['defaultOrder' => 't.order, t.create_time'],
            ]
        );
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_PUBLISHED => Yii::t('PageModule.page', 'Published'),
            self::STATUS_DRAFT => Yii::t('PageModule.page', 'Draft'),
            self::STATUS_MODERATION => Yii::t('PageModule.page', 'On moderation'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('PageModule.page', '*unknown*');
    }

    /**
     * @return array
     */
    public function getProtectedStatusList()
    {
        return [
            self::PROTECTED_NO => Yii::t('PageModule.page', 'no'),
            self::PROTECTED_YES => Yii::t('PageModule.page', 'yes'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getProtectedStatus()
    {
        $data = $this->getProtectedStatusList();

        return isset($data[$this->is_protected]) ? $data[$this->is_protected] : Yii::t('PageModule.page', '*unknown*');
    }

    /**
     * @param bool $selfId
     * @return array
     */
    public function getAllPagesList($selfId = false)
    {
        $criteria = new CDbCriteria();
        $criteria->order = "{$this->tableAlias}.order DESC, {$this->tableAlias}.create_time DESC";
        if ($selfId) {
            $otherCriteria = new CDbCriteria();
            $otherCriteria->addNotInCondition('id', (array)$selfId);
            $otherCriteria->group = "{$this->tableAlias}.slug, {$this->tableAlias}.id";
            $criteria->mergeWith($otherCriteria);
        }

        return CHtml::listData($this->findAll($criteria), 'id', 'title');
    }

    /**
     * @param bool $slug
     * @return array
     */
    public function getAllPagesListBySlug($slug = false)
    {
        $params = ['order' => 't.order DESC, t.create_time DESC'];
        if ($slug) {
            $params += [
                'condition' => 'slug != :slug',
                'params' => [':slug' => $slug],
                'group' => 'slug',
            ];
        }

        return CHtml::listData($this->findAll($params), 'id', 'title');
    }

    /**
     * @return string
     */
    public function getCategoryName()
    {
        return ($this->category === null) ? '---' : $this->category->name;
    }

    /**
     * @return string
     */
    public function getParentName()
    {
        return ($this->parent === null) ? '---' : $this->parent->title;
    }

    /**
     * @return bool
     */
    public function isProtected()
    {
        return (bool)$this->is_protected === self::PROTECTED_YES;
    }

    /**
     * Возвращает отформатированный список в соответствии со вложенность страниц.
     *
     * @param null|int $parentId
     * @param int $level
     * @param null|array|CDbCriteria $criteria
     * @return array
     */
    public function getFormattedList($parentId = null, $level = 0, $criteria = null)
    {
        if (empty($parentId)) {
            $parentId = null;
        }

        $models = $this->findAllByAttributes(['parent_id' => $parentId], $criteria);

        $list = [];

        foreach ($models as $model) {

            $model->title = str_repeat('&emsp;', $level) . $model->title;

            $list[$model->id] = $model->title;

            $list = CMap::mergeArray($list, $this->getFormattedList($model->id, $level + 1, $criteria));
        }

        return $list;
    }

	/**
	 *
	 */
	protected function afterFind()
	{
		parent::afterFind();

        foreach (['slug', 'status'] as $key){
            $this->_oldData[$key] = $this->{$key};
		}
	}

	/**
	 * @return string
	 */
	public function getTitle(){
    	return $this->title_h1 ?: $this->title;
	}

    /**
     * @return string
     */
    public function getMetaTitle(){
        return $this->meta_title ?: Helper::prepareSeoTitle($this->title);
    }

	/**
	 * @return mixed
	 */
	public function getLink()
	{
		return Yii::app()->createUrl($this->getPath());
	}

	/**
	 * @param $id
	 * @return string|null
	 */
	public static function getBodyById($id){
		$page = Page::model()->cache(Helper::getCacheTime(), \TaggedCache\TaggingCacheHelper::getDependency('page-'.$id))->findByPk($id);
		if($page){
			return $page->body;
		}

		return null;
	}

	/**
	 * @return string
	 */
	public function getBody(){
		return $this->prepareContent()->body;
	}
}
