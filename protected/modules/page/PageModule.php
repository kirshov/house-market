<?php

/**
 * PageModule основной класс модуля page
 */
class PageModule extends webforma\components\WebModule
{
	/**
	 *
	 */
	public $adminMenuOrder = 60;

	/**
	 * @var int
	 */
	public $useShortBody = 0;

	/**
	 * @var int
	 */
	public $useCategory = 0;

	/**
	 * @var
	 */
	public $mainCategory;

	/**
	 * @var int
	 */
	public $useImage = 0;

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [
            'editor' => Yii::t('PageModule.page', 'Visual editor'),
            'mainCategory' => Yii::t('PageModule.page', 'Main pages category'),
			'useShortBody' => 'Использовать краткий текст',
			'useImage' => 'Использовать изображения',
			'useCategory' => 'Использовать категории',
        ];
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
    	$params = [];

    	if(DEVMODE){
    		$params = CMap::mergeArray($params, [
    			'useCategory' => [
    				'input-type' => 'checkbox',
				],
				'mainCategory' => CHtml::listData($this->getCategoryList(), 'id', 'name'),
				'useShortBody' => [
    				'input-type' => 'checkbox',
				],
				'useImage' => [
					'input-type' => 'checkbox',
				]
			]);
		}

        return $params;
    }

	/**
	 * @return array
	 */
	public function getEditableParamsGroups()
	{
		$groups = [];

		if(DEVMODE){
			$groups = CMap::mergeArray($groups, [
				'system' => [
					'label' => 'Системные настройки',
					'items' => [
						'useCategory',
						'mainCategory',
						'useShortBody',
						'useImage',
					]
				],
			]);
		}

		return $groups;
	}

    /**
     * @return bool
     */
    public function getIsInstallDefault()
    {
        return true;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('PageModule.page', 'Content');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('PageModule.page', 'Pages');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('PageModule.page', 'Module for creating and manage static pages');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-file';
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'application.modules.page.models.*',
                'application.modules.page.components.widgets.*',
                'application.modules.sitemap.components.SitemapHelper',
            ]
        );

        if (!$this->editor) {
            $this->editor = Yii::app()->getModule('webforma')->editor;
        }
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/page/pageBackend/index';
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('PageModule.page', 'Pages list'),
                'url' => ['/page/pageBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('PageModule.page', 'Create page'),
                'url' => ['/page/pageBackend/create'],
            ],
        ];
    }
}
