<?php
/**
 * PagesWidget виджет для вывода страниц
 */
Yii::import('application.modules.page.models.*');

/**
 * Class PagesWidget
 */
class PagesWidget extends webforma\widgets\WWidget
{
    /**
     * @var
     */
    public $pageStatus;
    /**
     * @var bool
     */
    public $topLevelOnly = false;
    /**
     * @var string
     */
    public $order = 't.order ASC, t.create_time ASC';
    /**
     * @var
     */
    public $parent_id;
    /**
     * @var string
     */
    public $view = 'pageswidget';
    /**
     * @var bool
     */
    public $visible = true;

	/**
	 * @var
	 */
	public $categoryId;

	/**
	 * @var array
	 */
	public $options = [];

	/**
	 * @var null
	 */
	public $id = null;

    /**
     *
     */
    public function init()
    {
        parent::init();

        if (!$this->pageStatus) {
            $this->pageStatus = Page::STATUS_PUBLISHED;
        }

        $this->parent_id = (int)$this->parent_id;
    }

    /**
     * @throws CException
     */
    public function run()
    {
        if ($this->visible) {
            $criteria = new CDbCriteria();
            $criteria->order = $this->order;
            if($this->pageStatus){
				$criteria->addCondition("status = {$this->pageStatus}");
			}

            if (!Yii::app()->user->isAuthenticated()) {
                $criteria->addCondition('is_protected = '.Page::PROTECTED_NO);
            }
            if ($this->parent_id) {
                $criteria->addCondition("parent_id = {$this->parent_id}");
            }

	        if ($this->categoryId) {
		        $criteria->addCondition("category_id = {$this->categoryId}");
	        }

            if ($this->topLevelOnly) {
                $criteria->addCondition("parent_id is null or parent_id = 0");
            }

			if ($this->id) {
				$criteria->addCondition("id = {$this->id}");
			}

            $this->render($this->view, [
				'pages' => Page::model()->cache($this->cacheTime, \TaggedCache\TaggingCacheHelper::getDependency('pages'))->findAll($criteria),
	            'options' => $this->options,
			]);
        }
    }
}
