<?php

use webforma\components\Event;

Yii::import('application.modules.page.models.Page');

/**
 * Class SitemapGeneratorListener
 */
class PageSitemapGeneratorListener
{
    /**
     * @param Event $event
     */
    public static function onGenerate(Event $event)
    {
        $generator = $event->getGenerator();

        $provider = new CActiveDataProvider(Page::model()->published()->public()->sitemapAllowed());

        foreach (new CDataProviderIterator($provider) as $item) {
            $generator->addItem(
                Yii::app()->createAbsoluteUrl($item->getPath()),
                strtotime($item->update_time),
                SitemapHelper::FREQUENCY_WEEKLY,
                0.5
            );
        }
    }
} 
