<?php
/**
 * @var $this PageBackendController
 * @var $model Page
 * @var $form \webforma\widgets\ActiveForm
 */
if($model->getIsNewRecord() && !Yii::app()->getRequest()->getIsPostRequest()){
	$model->status = Page::STATUS_PUBLISHED;
}
?>

<ul class="nav nav-tabs horizontal-tabs">
	<li class="active"><a href="#common" data-toggle="tab">Общее</a></li>
	<li><a href="#seo" data-toggle="tab">SEO</a></li>
	<li><a href="#advanced" data-toggle="tab">Дополнительные параметры</a></li>
</ul>

<?php
$form = $this->beginWidget(
    'webforma\widgets\ActiveForm',
    [
        'id' => 'page-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => 'vertical',
		'htmlOptions' => ['class' => 'well sticky', 'enctype' => 'multipart/form-data'],
		'clientOptions' => [
			'validateOnSubmit' => true,
		],
    ]
); ?>

<div class="tab-content">
	<div class="tab-pane active" id="common">

		<div class="alert alert-info">
			<?= Yii::t('PageModule.page', 'Fields with'); ?>
			<span class="required">*</span>
			<?= Yii::t('PageModule.page', 'are required.'); ?>
		</div>

		<?= $form->errorSummary($model); ?>

		<div class="row">
			<div class="col-sm-6">
				<div class="row">
					<div class="col-sm-6">
						<?= $form->dropDownListGroup($model, 'parent_id', [
							'widgetOptions' => [
								'data'        => $pages,
								'htmlOptions' => [
									'class' => 'popover-help',
									'empty' => Yii::t('PageModule.page', '--choose--'),
									'encode' => false,
								],
							],
						]); ?>
					</div>

					<?if($this->getModule()->useCategory):?>
						<div class="col-sm-6">
							<?=  $form->dropDownListGroup($model, 'category_id', [
								'widgetOptions' => [
									'data'        => Yii::app()->getComponent('categoriesRepository')->getFormattedList($this->getModule()->mainCategory),
									'htmlOptions' => ['empty' => Yii::t('PageModule.page', '--choose--'), 'encode' => false],
								],
							]); ?>
						</div>
					<?endif;?>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<?= $form->textFieldGroup($model, 'title'); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<?
						if($model->isNewRecord) {
							echo $form->slugFieldGroup($model, 'slug',['sourceAttribute' => 'title']);
						} else {
							echo $form->textFieldGroup($model, 'slug');
						}?>
					</div>
				</div>
			</div>
			<?if($this->getModule()->useImage):?>
				<div class="col-sm-6">
					<div class='row'>
						<div class="col-sx-8 col-sm-8 col-sm-offset-2">
							<?php $this->widget('webforma\widgets\WInputFile', [
								'model' => $model,
								'attribute' => 'image',
							]); ?>
						</div>
					</div>
				</div>
			<?endif;?>
		</div>

		<?if($this->getModule()->useShortBody):?>
			<div class="row">
				<div class="col-sm-12">
					<?= $form->labelEx($model, 'short_body'); ?>
					<?php
					$this->widget($this->module->getVisualEditor(), [
						'model'     => $model,
						'attribute' => 'short_body',
						'htmlOptions' => [
							'rows' => 25,
						],
					]); ?>
					<br/>
				</div>
			</div>
		<?endif;?>

		<div class="row">
			<div class="col-sm-12">
				<?= $form->labelEx($model, 'body'); ?>
				<?php
				$this->widget($this->module->getVisualEditor(), [
					'model'     => $model,
					'attribute' => 'body',
					'htmlOptions' => [
						'rows' => 25,
					],
				]); ?>
				<br/>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="seo">
		<div class="row">
			<div class="col-xs-6">
				<?= $form->checkboxGroup($model,'exclude_sitemap'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6">
				<?= $form->textFieldGroup($model,'title_h1'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6">
				<?= $form->textFieldGroup($model,'meta_title'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6">
				<?= $form->textFieldGroup($model, 'keywords'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6">
				<?= $form->textAreaGroup($model,'description'); ?>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="advanced">
		<div class="row">
			<div class="col-sm-3">
				<?= $form->dropDownListGroup($model, 'status', [
					'widgetOptions' => [
						'data'        => $model->statusList,
						'htmlOptions' => [
							'class'               => 'popover-help',
							'empty'               => Yii::t('PageModule.page', '--choose--'),
							'data-original-title' => $model->getAttributeLabel('status'),
							'data-content'        => $model->getAttributeDescription('status'),
							'data-container'      => 'body',
						],
					],
				]); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<?= $form->checkBoxGroup($model, 'is_protected'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<?= $form->textFieldGroup($model, 'order', [
					'widgetOptions' => [
						'htmlOptions' => [
							'class' => 'popover-help',
							'data-original-title' => $model->getAttributeLabel('order'),
							'data-content' => $model->getAttributeDescription('order'),
						],
					],
				]); ?>
			</div>
		</div>

		<?if(DEVMODE):?>
			<div class="row">
				<div class="col-sm-3">
					<?= $form->dropDownListGroup($model, 'layout', [
						'widgetOptions' => [
							'data'        => Yii::app()->getModule('webforma')->getLayoutsList(),
							'htmlOptions' => [
								'empty' => Yii::t('PageModule.page', '--choose--'),
							],
						],
					]); ?>
				</div>
				<div class="col-sm-3">
					<?= $form->textFieldGroup($model, 'view'); ?>
				</div>
			</div>
		<?endif;?>
	</div>
</div>

<div class="buttons">
	<?php
	$this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context'    => 'primary',
		'label'      => $model->isNewRecord
			? Yii::t('PageModule.page', 'Create page and continue')
			: Yii::t('PageModule.page', 'Save page and continue'),
	]); ?>

	<?php
	$this->widget('bootstrap.widgets.TbButton', [
		'buttonType'  => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label'       => $model->isNewRecord
			? Yii::t('PageModule.page', 'Create page and close')
			: Yii::t('PageModule.page','Save page and close'),
	]); ?>
</div>
<?php $this->endWidget(); ?>
<script>
	function activateFirstTabWithErrors() {
		var tab = $('.has-error').parents('.tab-pane').first();
		if (tab.length) {
			var id = tab.attr('id');
			$('a[href="#' + id + '"]').tab('show');
		} else {
			setCurrentTabActive();
		}
	}

	activateFirstTabWithErrors();
</script>