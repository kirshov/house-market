<?php
$this->breadcrumbs = [
    Yii::t('PageModule.page', 'Pages') => ['/page/pageBackend/index'],
    Yii::t('PageModule.page', 'Add page'),
];

$this->pageTitle = Yii::t('PageModule.page', 'Add page');

$this->menu = [
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('PageModule.page', 'Pages list'),
        'url'   => ['/page/pageBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('PageModule.page', 'Create page'),
        'url'   => ['/page/pageBackend/create']
    ],
];
?>

<?= $this->renderPartial('_form', [
	'model'        => $model,
	'pages'        => $pages,
]); ?>
