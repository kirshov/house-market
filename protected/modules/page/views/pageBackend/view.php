<?php
$this->breadcrumbs = [
    Yii::t('PageModule.page', 'Pages') => ['/page/pageBackend/index'],
    $model->title,
];

$this->pageTitle = Yii::t('PageModule.page', 'Show page');

$this->menu = [
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('PageModule.page', 'Pages list'),
        'url'   => ['/page/pageBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('PageModule.page', 'Create page'),
        'url'   => ['/page/pageBackend/create']
    ],
    [
        'icon'  => 'fa fa-fw fa-pencil',
        'label' => Yii::t('PageModule.page', 'Edit page'),
        'url'   => [
            '/page/pageBackend/update',
            'id' => $model->id
        ]
    ],
	[
		'icon'  => 'fa fa-fw fa-external-link-square',
		'label' => 'Просмотр на сайте',
		'url'   => Yii::app()->createAbsoluteUrl($model->getPath()),
		'linkOptions' => [
			'target' => '_blank',
		],
		'visible' => $model->status == Page::STATUS_PUBLISHED || $model->status == Page::STATUS_DRAFT,
	],
    [
        'icon'  => 'fa fa-fw fa-eye',
        'label' => Yii::t('PageModule.page', 'Show page'),
        'url'   => [
            '/page/pageBackend/view',
            'id' => $model->id
        ]
    ],
    [
        'icon'        => 'fa fa-fw fa-trash-o',
        'label'       => Yii::t('PageModule.page', 'Remove page'),
        'url'         => '#',
        'linkOptions' => [
            'submit'  => ['/page/pageBackend/delete', 'id' => $model->id],
            'params'  => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
            'confirm' => Yii::t('PageModule.page', 'Do you really want to remove page?'),
        ]
    ],
];
?>

<h2><?= $model->title; ?></h2>
<br/>

<p><?= $model->body; ?></p>
<br/>

<small><?= Yii::t('PageModule.page', 'Author'); ?>: <?= ($model->changeAuthor instanceof User) ? $model->changeAuthor->getFullName() : Yii::t('PageModule.page', 'Removed'); ?></small>
<br/>
<br/>

<li class="fa fa-fw fa-globe"></li>
<?= CHtml::link(Yii::app()->createAbsoluteUrl($model->getPath()), Yii::app()->createAbsoluteUrl($model->getPath()), ['target' => '_blank']); ?>