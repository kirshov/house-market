<?php
$form = $this->beginWidget(
	'\webforma\widgets\ActiveForm',
	[
		'action'      => Yii::app()->createUrl($this->route),
		'method'      => 'get',
		'type' => 'vertical',
		'htmlOptions' => ['class' => 'well'],
	]
); ?>
<fieldset class="inline">
	<div class="row">
		<div class="col-sm-6">
			<?= $form->textFieldGroup($model, 'title'); ?>
		</div>
		<div class="col-sm-6">
			<?= $form->textFieldGroup($model, 'slug'); ?>
		</div>

	</div>

	<div class="row">
		<div class="col-sm-6">
			<?= $form->textFieldGroup($model, 'body', ['cols' => 3]); ?>
		</div>
		<div class="col-sm-3">
			<?= $form->dropDownListGroup($model, 'parent_id', [
				'widgetOptions' => [
					'data'        => $pages,
					'htmlOptions' => [
						'class' => 'popover-help',
						'empty' => Yii::t('PageModule.page', '- not set -')
					],
				],
				'cols' => 3,
			]); ?>
		</div>
		<div class="col-sm-3">
			<?= $form->dropDownListGroup($model, 'status', [
				'widgetOptions' => [
					'data'        => $model->statusList,
					'htmlOptions' => [
						'class' => 'popover-help',
						'empty' => Yii::t('PageModule.page', '- no matter -')
					],
				],
				'cols' => 3,
			]); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-3">
			<?= $form->textFieldGroup($model, 'title_h1'); ?>
		</div>
		<div class="col-sm-3">
			<?= $form->textFieldGroup($model, 'meta_title'); ?>
		</div>
		<div class="col-sm-3">
			<?= $form->textFieldGroup($model, 'keywords'); ?>
		</div>
		<div class="col-sm-3">
			<?= $form->textFieldGroup($model, 'description'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-3">
			<?= $form->checkBoxGroup($model, 'exclude_sitemap'); ?>
		</div>
		<div class="col-sm-3">
			<?= $form->checkBoxGroup($model, 'is_protected'); ?>
		</div>
		<?if(DEVMODE):?>
			<div class="col-sm-3">
				<?= $form->textFieldGroup($model, 'view'); ?>
			</div>
		<?endif;?>
	</div>

</fieldset>

<?php $this->widget('bootstrap.widgets.TbButton', [
	'buttonType'  => 'submit',
	'context'     => 'primary',
	'encodeLabel' => false,
	'label'       => '<i class="fa fa-search">&nbsp;</i> ' . Yii::t('PageModule.page', 'Find pages'),
]); ?>

<?php $this->widget('bootstrap.widgets.TbButton', [
	'buttonType' => 'reset',
	'context'    => 'danger',
	'encodeLabel' => false,
	'label'       => '<i class="fa fa-times">&nbsp;</i> Очистить форму',
]); ?>
<?php $this->endWidget(); ?>
