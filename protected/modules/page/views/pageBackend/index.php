<?php

/**
 * @var $model Page
 * @var $this PageBackendController
 * @var $pages array
 */
$this->breadcrumbs = [
    Yii::t('PageModule.page', 'Pages') => ['/page/pageBackend/index'],
    Yii::t('PageModule.page', 'List'),
];

$module = Yii::app()->getModule('page');

$this->pageTitle = Yii::t('PageModule.page', 'Pages list');

$this->menu = [
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('PageModule.page', 'Pages list'),
        'url'   => ['/page/pageBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('PageModule.page', 'Create page'),
        'url'   => ['/page/pageBackend/create']
    ],
];
?>

<p class="search-wrap">
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?= Yii::t('PageModule.page', 'Find pages'); ?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php
    Yii::app()->clientScript->registerScript(
        'search',
        "
    $('.search-form form').submit(function () {
        $.fn.yiiGridView.update('page-grid', {
            data: $(this).serialize()
        });

        return false;
    });
"
    );
    $this->renderPartial('_search', ['model' => $model, 'pages' => $pages]);
    ?>
</div>

<?php
$useCategory = $this->getModule()->useCategory;
$mainCategory = $this->getModule()->mainCategory;

$this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id'           => 'page-grid',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'sortableRows'      => true,
        'sortableAjaxSave'  => true,
        'sortableAttribute' => 'order',
        'sortableAction'    => '/page/pageBackend/sortable',
        'columns'      => [
			[
				'type' => 'raw',
				'value' => function ($data) {
    				if(!$data->image){
    					return '';
					}
					return CHtml::image($data->getImageUrl(100, 100), $data->title, ["width" => 100, "height" => 100, "class" => "img-thumbnail"]);
				},
				'htmlOptions' => [
					'style' => 'width: 80px;',
				],
				'visible' => $module->useImage,
				'filter' => CHtml::activeTextField($model, 'id', ['class' => 'form-control']),
			],
            [
                'class'    => 'webforma\widgets\WEditableColumn',
                'name'     => 'title',
                'editable' => [
                    'url'    => $this->createUrl('/page/pageBackend/inline'),
                    'mode'   => 'popup',
                    'params' => [
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'title', ['class' => 'form-control']),
            ],
            [
                'class'    => 'webforma\widgets\WEditableColumn',
                'name'     => 'slug',
                'editable' => [
                    'url'    => $this->createUrl('/page/pageBackend/inline'),
                    'mode'   => 'popup',
                    'params' => [
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'slug', ['class' => 'form-control']),
            ],

            [
                'name'   => 'parent_id',
                'value'  => '$data->parentName',
				'filter' => CHtml::activeDropDownList($model, 'parent_id', $model->getFormattedList(), [
					'encode' => false,
					'empty' => '',
					'class' => 'form-control'
				]),
            ],
			[
				'name' => 'category_id',
				'value'  => '$data->getCategoryName()',
				'filter' => CHtml::activeDropDownList($model, 'category_id', Yii::app()->getComponent('categoriesRepository')->getFormattedList($mainCategory), [
						'encode' => false,
						'empty' => '',
						'class' => 'form-control'
					]
				),
				'visible' => $useCategory,
			],
            [
                'class'   => 'webforma\widgets\EditableStatusColumn',
                'name'    => 'status',
                'url'     => $this->createUrl('/page/pageBackend/inline'),
                'source'  => $model->getStatusList(),
                'options' => [
                    Page::STATUS_PUBLISHED  => ['class' => 'label-success'],
                    Page::STATUS_MODERATION => ['class' => 'label-warning'],
                    Page::STATUS_DRAFT      => ['class' => 'label-default'],
                ],
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
                'frontViewButtonUrl' => function($data){
                    return Yii::app()->createUrl($data->getPath());
                },
                'buttons' => [
                    'front_view' => [
                        'visible' => function ($row, $data) {
                            return $data->status == Page::STATUS_PUBLISHED || $data->status == Page::STATUS_DRAFT;
                        }
                    ]
                ]
            ],
        ],
    ]
);