<?php
$this->breadcrumbs = [
    Yii::t('PageModule.page', 'Pages') => ['/page/pageBackend/index'],
    $model->title                      => ['/page/pageBackend/view', 'id' => $model->id],
    Yii::t('PageModule.page', 'Edit'),
];

$this->pageTitle = Yii::t('PageModule.page', 'Pages - edit');

$this->menu = [
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('PageModule.page', 'Pages list'),
        'url'   => ['/page/pageBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('PageModule.page', 'Create page'),
        'url'   => ['/page/pageBackend/create']
    ],
    [
        'icon'  => 'fa fa-fw fa-pencil',
        'label' => Yii::t('PageModule.page', 'Edit page'),
        'url'   => [
            '/page/pageBackend/update/',
            'id' => $model->id
        ]
    ],
	[
		'icon'  => 'fa fa-fw fa-external-link-square',
		'label' => 'Просмотр на сайте',
		'url'   => Yii::app()->createAbsoluteUrl($model->getPath()),
		'linkOptions' => [
			'target' => '_blank',
		],
		'visible' => $model->status == Page::STATUS_PUBLISHED || $model->status == Page::STATUS_DRAFT,
	],
    [
        'icon'  => 'fa fa-fw fa-eye',
        'label' => Yii::t('PageModule.page', 'View page'),
        'url'   => [
            '/page/pageBackend/view',
            'id' => $model->id
        ]
    ],
    [
        'icon'        => 'fa fa-fw fa-trash-o',
        'label'       => Yii::t('PageModule.page', 'Remove page'),
        'url'         => '#',
        'linkOptions' => [
            'submit'  => ['/page/pageBackend/delete', 'id' => $model->id],
            'params'  => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
            'confirm' => Yii::t('PageModule.page', 'Do you really want to remove page?'),
            'csrf'    => true,
        ]
    ],
];
?>

<?= $this->renderPartial('_form', [
	'pages'        => $pages,
	'model'        => $model,
]); ?>
