<?php

class m180318_135252_not_sitemap extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{page_page}}', 'exclude_sitemap', 'tinyint(1) not null default 0');
	}

	public function safeDown()
	{

	}
}