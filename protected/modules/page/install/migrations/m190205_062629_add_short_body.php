<?php

class m190205_062629_add_short_body extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{page_page}}', 'short_body', 'text default null');
	}

	public function safeDown()
	{

	}
}