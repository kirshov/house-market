<?php

class m190206_071541_add_image extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{page_page}}', 'image', 'varchar(255) default null');
	}

	public function safeDown()
	{

	}
}