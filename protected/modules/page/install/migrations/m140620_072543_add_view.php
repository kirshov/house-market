<?php

/**
 * m130115_155600_columns_rename install migration
 * Класс миграций для модуля Page
 */
class m140620_072543_add_view extends CDbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{page_page}}', 'view', 'varchar(250)');
    }

    public function safeDown()
    {
        $this->dropColumn('{{page_page}}', 'view');
    }
}
