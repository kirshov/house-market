<?php

/**
 * m130115_155600_columns_rename install migration
 * Класс миграций для модуля Page
 */
class m140115_083618_add_layout extends webforma\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{page_page}}', 'layout', 'varchar(250)');
    }

    public function safeDown()
    {
        $this->dropColumn('{{page_page}}', 'layout');
    }
}
