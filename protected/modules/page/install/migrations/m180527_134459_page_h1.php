<?php

class m180527_134459_page_h1 extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{page_page}}', 'title_h1', 'varchar(255) default null');
	}

	public function safeDown()
	{

	}
}