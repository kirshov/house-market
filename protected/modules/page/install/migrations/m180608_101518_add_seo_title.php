<?php

class m180608_101518_add_seo_title extends webforma\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{page_page}}', 'meta_title', 'varchar(255) default null');
	}

	public function safeDown()
	{

	}
}