<?php

use webforma\components\controllers\FrontController;

/**
 * PageController публичный контроллер для работы со страницами
 */
class PageController extends FrontController
{
    /**
     * Текущая просматриваемая страница
     */
    public $currentPage;

    /**
     * экшн для отображения конкретной страницы, отображает опубликованные страницы и превью
     */
    public function actionView($slug)
    {
		$path = Yii::app()->getRequest()->getPathInfo();

		$model = Page::model()->published()->findByPath($path);

        if (null === $model) {
            throw new CHttpException(404, Yii::t('PageModule.page', 'Page was not found'));
        }

        // проверим что пользователь может просматривать эту страницу
        if ($model->isProtected() && !Yii::app()->getUser()->isAuthenticated()) {
            Yii::app()->getUser()->setFlash(
                webforma\widgets\WFlashMessages::ERROR_MESSAGE,
                Yii::t('PageModule.page', 'You must be authorized user for view this page!')
            );

            $this->redirect([Yii::app()->getModule('user')->accountActivationSuccess]);
        }
	    $this->currentPage = $model;

        $dataProvider = new CActiveDataProvider('Page', [
            'criteria' => [
                'condition' => 'parent_id = :parent_id',
		        'params' => [
		            ':parent_id' => $model->id,
		        ],
		        'scopes' => ['published'],
	        ],
	        'pagination' => false,
        ]);


        $this->render($model->view ?: 'view', [
            'model' => $model,
	        'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Генерирует меню навигации по вложенным страницам для использования в zii.widgets.CBreadcrumbs
     */
    public function getBreadCrumbs()
    {
        $pages = [];
        if ($this->currentPage->parent) {
            $pages = $this->getBreadCrumbsRecursively($this->currentPage->parent);
        }

        $pages = array_reverse($pages);
        $pages[] = $this->currentPage->title;

        return $pages;
    }

    /**
     * Рекурсивно возвращает пригодный для zii.widgets.CBreadcrumbs массив, начиная со страницы $page
     * @param  Page $page
     * @return array
     * @internal param int $pageId
     */
    private function getBreadCrumbsRecursively(Page $page)
    {
        $pages = [];
	    $pages[$page->title] = Yii::app()->createUrl($page->getPath());
        $pp = $page->parent;

        if ($pp) {
            $pages += $this->getBreadCrumbsRecursively($pp);
        }

        return $pages;
    }
}
