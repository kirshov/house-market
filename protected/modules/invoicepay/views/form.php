<?php
/**
 * @var array $settings
 * @var Order $order
 * @var Payment $payment
 */

echo CHtml::link('Распечатать счет', Yii::app()->createUrl('/invoicepay/invoice/index', ['url' => $order->url]), ['class' => 'btn', 'target' => '_blank']);