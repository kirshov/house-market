<?php

return [
    'module' => [
        'class' => 'application.modules.invoicepay.InvoicepayModule',
    ],
    'component' => [
        'paymentManager' => [
            'paymentSystems' => [
                'invoice' => [
                    'class' => 'application.modules.invoicepay.components.payments.InvoicePaymentSystem',
                ]
            ],
        ],
    ],
	'rules' => [
		'/invoicepay/<url>' => 'invoicepay/invoice/index',
	],
];
