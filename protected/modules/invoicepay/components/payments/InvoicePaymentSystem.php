<?php

/**
 * Class InvoicePaymentSystem
 */

Yii::import('application.modules.invoicepay.TinkoffModule');

class InvoicePaymentSystem extends PaymentSystem
{
	protected $paymentSettings;

    public function renderCheckoutForm(Payment $payment, Order $order, $return = false)
    {
    	$this->paymentSettings = $payment->getPaymentSystemSettings();

        return Yii::app()->getController()->renderPartial('application.modules.invoicepay.views.form', [
            'settings' => $payment->getPaymentSystemSettings(),
	        'order' => $order,
	        'payment' => $payment,
        ], $return);
    }
}
