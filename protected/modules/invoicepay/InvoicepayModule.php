<?php

use webforma\components\WebModule;

class InvoicepayModule extends WebModule
{
    /**
     *
     */

    public function getDependencies()
    {
        return ['payment'];
    }

    public function getNavigation()
    {
        return false;
    }

    public function getAdminPageLink()
    {
        return false;
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getEditableParamsGroups()
    {
        return [];
    }

    public function getIsShowInAdminMenu()
    {
        return false;
    }

    public function getCategory()
    {
        return false;
    }

    public function getName()
    {
        return Yii::t('InvoicepayModule.invoicepay', 'Invoicepay');
    }

    public function init()
    {
        parent::init();
    }
}
