<?php

class m190205_115530_add_tags extends webforma\components\DbMigration
{
	public function safeUp()
	{
		// tags
		$this->createTable('{{news_tag}}', [
				'id'   => 'pk',
				'name' => 'varchar(255) NOT NULL',
			], $this->getOptions()
		);

		//ix
		$this->createIndex("ux_{{news_tag}}_tag_name", '{{news_tag}}', "name", true);

		// post to tag
		$this->createTable('{{news_to_tag}}', [
				'new_id' => 'integer NOT NULL',
				'tag_id'  => 'integer NOT NULL',
				'PRIMARY KEY (new_id, tag_id)'
			],
			$this->getOptions()
		);

		//ix
		$this->createIndex("ix_{{news_to_tag}}_new_id", '{{news_to_tag}}', "new_id", false);
		$this->createIndex("ix_{{news_to_tag}}_tag_id", '{{news_to_tag}}', "tag_id", false);

		//fk
		$this->addForeignKey("fk_{{news_to_tag}}_new_id", '{{news_to_tag}}', 'new_id', '{{news_news}}', 'id', 'CASCADE', 'NO ACTION');
		$this->addForeignKey("fk_{{news_to_tag}}_tag_id", '{{news_to_tag}}', 'tag_id', '{{news_tag}}', 'id', 'CASCADE', 'NO ACTION' );
	}

	public function safeDown()
	{

	}
}