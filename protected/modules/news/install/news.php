<?php
/**
 * Класс миграций для модуля News
 **/
return [
    'module' => [
        'class' => 'application.modules.news.NewsModule',
    ],
    'import' => [
        'application.modules.news.events.*',
        'application.modules.news.listeners.*',
        'application.modules.news.helpers.*',
    ],
    'component' => [
        'eventManager' => [
            'class' => 'webforma\components\EventManager',
            'events' => [
                'sitemap.before.generate' => [
                    ['\NewsSitemapGeneratorListener', 'onGenerate']
                ],
                'news.after.save' => [
                    ['\NewsListener', 'onAfterSave']
                ],
                'news.after.delete' => [
                    ['\NewsListener', 'onAfterDelete']
                ],

            ]
        ]
    ],
	'params'    => [
		'widgets'   => [
			'lastNews' => 'application.modules.news.widgets.LastNewsWidget',
		],
	],
    'rules' => [
        '/news/' => 'news/news/index',
        [
            'news/news/view',
            'pattern' => '/news/<slug>',
        ],
	    //'/news/categories' => 'news/newsCategory/index',
        /*'/news/<slug>' => 'news/newsCategory/view',*/
    ],
];
