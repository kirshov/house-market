<?php
use webforma\components\Event;

/**
 * Class NewsListener
 */
class NewsListener
{
    /**
     * onAfterSave event listener
     *
     * @param Event $event
     */
    public static function onAfterSave(Event $event)
    {
	    \TaggedCache\TaggingCacheHelper::deleteTag([NewsHelper::CACHE_NEWS_TAG]);
    }

    /**
     * @param Event $event
     */
    public static function onAfterDelete(Event $event)
    {
	    \TaggedCache\TaggingCacheHelper::deleteTag([NewsHelper::CACHE_NEWS_TAG]);
    }

}