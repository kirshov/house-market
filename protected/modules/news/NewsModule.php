<?php

/**
 * NewsModule основной класс модуля news
 */

use webforma\components\WebModule;

/**
 * Class NewsModule
 */
class NewsModule extends WebModule
{
	/**
	 *
	 */
	public $adminMenuOrder = 40;

    /**
     * @var string
     */
    public $uploadPath = 'news';

    /**
     * @var int
     */
    public $perPage = 10;

    /**
     * @var
     */
    public $metaTitle;
    /**
     * @var
     */
    public $metaDescription;
    /**
     * @var
     */
    public $metaKeyWords;

	/**
	 * @var int
	 */
    public $useTags = 0;

	/**
	 * @var int
	 */
	public $useCategory = 0;

	/**
	 * @var
	 */
	public $mainCategory;

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [];
    }

    /**
     * @return bool
     */
    public function getInstall()
    {
        if (parent::getInstall()) {
            @mkdir(Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath, 0755);
        }

        return false;
    }

    /**
     * @return array|bool
     */
    public function checkSelf()
    {
    	return true;
        $messages = [];

        $uploadPath = Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath;

        if (!is_writable($uploadPath)) {
            $messages[WebModule::CHECK_ERROR][] = [
                'type' => WebModule::CHECK_ERROR,
                'message' => Yii::t(
                    'NewsModule.news',
                    'Directory "{dir}" is not accessible for write! {link}',
                    [
                        '{dir}' => $uploadPath,
                        '{link}' => CHtml::link(
                            Yii::t('NewsModule.news', 'Change settings'),
                            [
                                '/webforma/backend/modulesettings/',
                                'module' => 'news',
                            ]
                        ),
                    ]
                ),
            ];
        }

        return (isset($messages[WebModule::CHECK_ERROR])) ? $messages : true;
    }

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [
            'useCategory' => 'Использовать категории',
            'mainCategory' => Yii::t('NewsModule.news', 'Main news category'),
            'editor' => Yii::t('NewsModule.news', 'Visual Editor'),
            'uploadPath' => Yii::t(
                'NewsModule.news',
                'Uploading files catalog (relatively {path})',
                [
                    '{path}' => Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . Yii::app()->getModule(
                            "webforma"
                        )->uploadPath,
                ]
            ),
            'useTags' => 'Использовать теги',
            'perPage' => Yii::t('NewsModule.news', 'News per page'),
            'metaTitle' => Yii::t('NewsModule.news', 'Title tag for the news section'),
            'metaDescription' => Yii::t('NewsModule.news', 'Description for the news section'),
            'metaKeyWords' => Yii::t('NewsModule.news', 'KeyWords for the news section'),

        ];
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
        $params = [
        	'perPage',
            'metaTitle',
            'metaDescription',
            'metaKeyWords',
        ];

        if(DEVMODE){
        	$params = CMap::mergeArray($params, [
        		'useTags' => [
        			'input-type' => 'checkbox',
				],
				'useCategory' => [
					'input-type' => 'checkbox',
				],
				'mainCategory' => CHtml::listData($this->getCategoryList(), 'id', 'name'),
			]);
		}
        return $params;
    }

    /**
     * @return array
     */
    public function getEditableParamsGroups()
    {
        $groups = [
			'main' => [
				'label' => Yii::t('NewsModule.news', 'General'),
				'items' => [
					'perPage',
				],
			],
            'seo' => [
                'label' => Yii::t('NewsModule.news', 'SEO'),
                'items' => [
                    'metaTitle',
                    'metaDescription',
                    'metaKeyWords',
                ],
            ],
        ];

        if(DEVMODE){
        	$groups = CMap::mergeArray($groups, [
        		'system' => [
        			'label' => 'Системные настройки',
					'items' => [
						'useTags',
						'useCategory',
						'mainCategory',
					]
				]
			]);
		}

        return $groups;
    }

    /**
     * @return bool
     */
    public function getIsInstallDefault()
    {
        return true;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('NewsModule.news', 'Content');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('NewsModule.news', 'News');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('NewsModule.news', 'Module for creating and management news');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-bullhorn';
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/news/newsBackend/index';
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('NewsModule.news', 'News list'),
                'url' => ['/news/newsBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('NewsModule.news', 'Create news'),
                'url' => ['/news/newsBackend/create'],
            ],
        ];
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'news.models.*',
				'vendor.yiiext.taggable-behavior.ETaggableBehavior',
				'vendor.yiiext.taggable-behavior.EARTaggableBehavior',
            ]
        );
    }
}
