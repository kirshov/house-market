<?php
/**
 * @var $this NewsBackendController
 * @var $model News
 * @var $form \webforma\widgets\ActiveForm
 */
$form = $this->beginWidget(
    '\webforma\widgets\ActiveForm',
    [
        'id' => 'news-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => 'vertical',
        'htmlOptions' => ['class' => 'well sticky', 'enctype' => 'multipart/form-data'],
		'clientOptions' => [
			'validateOnSubmit' => true,
		],
    ]
); ?>
<div class="alert alert-info">
    <?= Yii::t('NewsModule.news', 'Fields with'); ?>
    <span class="required">*</span>
    <?= Yii::t('NewsModule.news', 'are required'); ?>
</div>

<?= $form->errorSummary($model); ?>

<div class="row">
	<div class="col-sm-6">
		<div class="row">
			<div class="col-sm-12">
				<?= $form->textFieldGroup($model, 'title'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<?if($model->isNewRecord){
					echo $form->slugFieldGroup($model, 'slug', ['sourceAttribute' => 'title']);
				} else {
					echo $form->textFieldGroup($model, 'slug');
				}?>
			</div>
		</div>

		<?if($this->getModule()->useCategory):?>
			<div class="row">
				<div class="col-sm-6">
					<?=  $form->dropDownListGroup($model, 'category_id', [
						'widgetOptions' => [
							'data'        => Yii::app()->getComponent('categoriesRepository')->getFormattedList($this->getModule()->mainCategory),
							'htmlOptions' => ['empty' => Yii::t('NewsModule.news', '--choose--'), 'encode' => false],
						],
					]); ?>
				</div>
			</div>
		<?endif;?>

		<div class="row">
			<div class="col-sm-6">
				<?= $form->datePickerGroup($model, 'date', [
					'widgetOptions' => [
						'options' => [
							'format' => 'dd.mm.yyyy',
							'weekStart' => 1,
							'autoclose' => true,
						],
					],
					'prepend' => '<i class="fa fa-calendar"></i>',
				]);
				?>
			</div>

			<div class="col-sm-6">
				<?= $form->dropDownListGroup($model, 'status', [
					'widgetOptions' => [
						'data' => $model->getStatusList(),
					],
				]); ?>
			</div>
		</div>

		<?if($this->getModule()->useTags):?>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<?= $form->labelEx($model, 'tags', ['control-label']); ?>
						<div class="input-group">
							<?php $this->widget('booster.widgets.TbSelect2', [
								'asDropDownList' => false,
								'name' => 'tags',
								'options' => [
									'tags' => array_values(CHtml::listData(NewsTag::model()->findAll(), 'id', 'name')),
									'placeholder' => 'тэги',
								],
								'htmlOptions' => [
									'class' => '',
								],
								'value' => implode(", ", $model->getTags()),
							]); ?>
						</div>
					</div>
				</div>
			</div>
		<?endif;?>
	</div>

	<div class="col-sm-6">
		<div class='row'>
			<div class="col-sx-8 col-sm-8 col-sm-offset-2">
				<?php $this->widget('webforma\widgets\WInputFile', [
					'model' => $model,
					'attribute' => 'image',
				]); ?>
			</div>
		</div>
	</div>
</div>

<div class="row">
    <div class="col-sm-12 <?= $model->hasErrors('full_text') ? 'has-error' : ''; ?>">
        <?= $form->labelEx($model, 'full_text'); ?>
        <?php $this->widget($this->module->getVisualEditor(), [
			'model' => $model,
			'attribute' => 'full_text',
		]); ?>
        <span class="help-block">
            <?= Yii::t('NewsModule.news', 'Full text news which will be shown on news article page'); ?>
        </span>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?= $form->labelEx($model, 'short_text'); ?>
        <?php $this->widget($this->module->getVisualEditor(), [
			'model' => $model,
			'attribute' => 'short_text',
		]); ?>
        <span class="help-block">
            <?= Yii::t('NewsModule.news', 'News anounce text. Usually this is the main idea of the article.'); ?>
        </span>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?= $form->checkBoxGroup($model, 'is_protected'); ?>
    </div>
</div>

<?php $collapse = $this->beginWidget('bootstrap.widgets.TbCollapse'); ?>
<div class="panel-group" id="extended-options">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">
                <a data-toggle="collapse" data-parent="#extended-options" href="#collapseOne">
                    <?= Yii::t('NewsModule.news', 'Data for SEO'); ?>
                </a>
            </div>
			<div class="panel-heading-icon"><i class="fa fa-search"></i></div>
        </div>
        <div id="collapseOne" class="panel-collapse collapse">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->textFieldGroup($model, 'keywords'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->textAreaGroup($model, 'description'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>

<br/>

<div class="buttons">
	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context' => 'primary',
		'label' => $model->isNewRecord
			? Yii::t('NewsModule.news', 'Create article and continue')
			: Yii::t('NewsModule.news', 'Save news article and continue'),
	]); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label' => $model->isNewRecord
			? Yii::t('NewsModule.news', 'Create article and close')
			: Yii::t('NewsModule.news', 'Save news article and close'),
	]); ?>
</div>
<?php $this->endWidget(); ?>
