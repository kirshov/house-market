<?php

/**
 * @var $model News
 * @var $this NewsBackendController
 */

$this->breadcrumbs = [
    Yii::t('NewsModule.news', 'News') => ['/news/newsBackend/index'],
    Yii::t('NewsModule.news', 'Management'),
];

$this->pageTitle = Yii::t('NewsModule.news', 'News - management');

$this->menu = [
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('NewsModule.news', 'News management'),
        'url' => ['/news/newsBackend/index'],
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('NewsModule.news', 'Create news'),
        'url' => ['/news/newsBackend/create'],
    ],
	[
		'icon' => 'fa fa-fw fa-external-link-square',
		'label' => Yii::t('WebformaModule.webforma', 'To the section on the site'),
		'url' => ['/news/news/index'],
	],
];
?>
<p class="search-wrap">
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?= Yii::t('NewsModule.news', 'Find news'); ?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php
    Yii::app()->clientScript->registerScript(
        'search',
        "
    $('.search-form form').submit(function () {
        $.fn.yiiGridView.update('news-grid', {
            data: $(this).serialize()
        });

        return false;
    });
"
    );
    $this->renderPartial('_search', ['model' => $model]);
    ?>
</div>

<?php
$useCategory = $this->getModule()->useCategory;
$mainCategory = $this->getModule()->mainCategory;

$this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'news-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => [
			[
				'type' => 'raw',
				'value' => function ($data) {
    				if($data->image){
						return CHtml::image($data->getImageUrl(100, 100), $data->title, ["width" => 100, "height" => 100, "class" => "img-thumbnail"]);
					}
				},
				'htmlOptions' => [
					'style' => 'width: 120px;',
				],
				'filter' => CHtml::activeTextField($model, 'id', ['class' => 'form-control']),
			],
            [
                'class' => 'webforma\widgets\WEditableColumn',
                'name' => 'title',
                'editable' => [
                    'url' => $this->createUrl('/news/newsBackend/inline'),
                    'mode' => 'popup',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($model, 'title', ['class' => 'form-control']),
            ],
            [
                'class' => 'webforma\widgets\WEditableColumn',
                'name' => 'slug',
                'editable' => [
                    'url' => $this->createUrl('/news/newsBackend/inline'),
                    'mode' => 'popup',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($model, 'slug', ['class' => 'form-control']),
            ],
			[
				'name' => 'category_id',
				'value'  => '$data->getCategoryName()',
				'filter' => CHtml::activeDropDownList($model, 'category_id', Yii::app()->getComponent('categoriesRepository')->getFormattedList($mainCategory), [
					'encode' => false,
					'empty' => '',
					'class' => 'form-control'
					]
				),
				'visible' => $useCategory,
            ],
            'date',
            [
                'class' => 'webforma\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('/news/newsBackend/inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    News::STATUS_PUBLISHED => ['class' => 'label-success'],
                    News::STATUS_MODERATION => ['class' => 'label-warning'],
                    News::STATUS_DRAFT => ['class' => 'label-default'],
                ],
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
                'frontViewButtonUrl' => function ($data) {
                    return Yii::app()->createUrl('/news/news/view', ['slug' => $data->slug]);
                },
                'buttons' => [
                    'front_view' => [
                        'visible' => function ($row, $data) {
                            return $data->status == News::STATUS_PUBLISHED;
                        },
                    ],
                ],
            ],
        ],
    ]
);