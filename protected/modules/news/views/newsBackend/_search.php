<?php
$form = $this->beginWidget(
	'\webforma\widgets\ActiveForm',
    [
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
		'type' => 'vertical',
		'htmlOptions' => ['class' => 'well'],
    ]
); ?>

<div class="row">
    <div class="col-sm-6">
        <?= $form->textFieldGroup($model, 'title'); ?>
    </div>
    <div class="col-sm-6">
        <?= $form->textFieldGroup($model, 'slug'); ?>
    </div>
</div>

<div class="row">
	<div class="col-sm-6">
		<?= $form->datePickerGroup($model, 'date', [
			'widgetOptions' => [
				'options' => [
					'format' => 'dd-mm-yyyy',
					'weekStart' => 1,
					'autoclose' => true,
				],
			],
			'prepend' => '<i class="fa fa-calendar"></i>',
		]);
		?>
	</div>

	<div class="col-sm-6">
		<?= $form->dropDownListGroup($model, 'status', [
			'widgetOptions' => [
				'data' => $model->getStatusList(),
				'htmlOptions' => [
					'empty' => Yii::t('NewsModule.news', '-no matter-'),
				],
			],
		]); ?>
	</div>
</div>
<div class="row">
    <div class="col-sm-6">
        <?= $form->textFieldGroup($model, 'short_text'); ?>
    </div>
    <div class="col-sm-6">
        <?= $form->textFieldGroup($model, 'full_text'); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
		<?= $form->checkBoxGroup($model, 'is_protected'); ?>
	</div>
</div>

<?php $this->widget('bootstrap.widgets.TbButton', [
	'context' => 'primary',
	'encodeLabel' => false,
	'buttonType' => 'submit',
	'label' => '<i class="fa fa-search">&nbsp;</i> ' . Yii::t('NewsModule.news','Find article'),
]); ?>

<?php $this->widget('bootstrap.widgets.TbButton', [
	'buttonType' => 'reset',
	'context'    => 'danger',
	'encodeLabel' => false,
	'label'       => '<i class="fa fa-times">&nbsp;</i> Очистить форму',
]); ?>
<?php $this->endWidget(); ?>
