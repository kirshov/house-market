<?php

/**
 * Виджет вывода последних новостей
 **/
Yii::import('application.modules.news.models.*');

/**
 * Class LastNewsWidget
 */
class LastNewsWidget extends webforma\widgets\WWidget
{
	/**
	 * @var integer
	 */
	public $limit;

    /** @var $categories mixed Список категорий, из которых выбирать новости. NULL - все */
    public $categories = null;

    /**
     * @var string
     */
    public $view = 'default';

	/**
	 * @var int
	 */
    public $width = 100;

	/**
	 * @var int
	 */
    public $height = 100;

	public function init()
	{
		parent::init();
		Yii::app()->getModule('news');
	}


	/**
     * @throws CException
     */
    public function run()
    {
        $cacheName = NewsHelper::CACHE_NEWS_LIST;
        $news = Yii::app()->getCache()->get($cacheName);

        if ($news === false) {
            $criteria = new CDbCriteria();
            if($criteria->limit){
	            $criteria->limit = (int)$this->limit;
            }

            $criteria->order = 'date DESC';

            if ($this->categories) {
                if (is_array($this->categories)) {
                    $criteria->addInCondition('category_id', $this->categories);
                } else {
                    $criteria->compare('category_id', $this->categories);
                }
            }

            $news = News::model()->published()->findAll($criteria);

            Yii::app()->getCache()->set($cacheName, $news, $this->cacheTime, \TaggedCache\TaggingCacheHelper::getDependency(NewsHelper::CACHE_NEWS_TAG));
        }

        $this->render($this->view, [
        	'models' => $news,
	        'width' => $this->width,
	        'height' => $this->height,
        ]);
    }
}
