<?php

/**
 * NewsRssController контроллер для генерации rss-ленты новостей
 */
class NewsRssController extends webforma\components\controllers\RssController
{
    /**
     * @throws CHttpException
     */
    public function loadData()
    {
        if (!($limit = (int)$this->module->rssCount)) {
            throw new CHttpException(404);
        }

        $criteria = new CDbCriteria();
        $criteria->order = 'date DESC';
        $criteria->params = [];
        $criteria->limit = $limit;

        $this->title = $this->webforma->siteName;
        $this->description = $this->webforma->siteDescription;

        $categoryId = (int)Yii::app()->getRequest()->getQuery('category');

        if (!empty($categoryId)) {
            $category = Yii::app()->getComponent('categoriesRepository')->getById($categoryId);
            if (null === $category) {
                throw new CHttpException(404);
            }
            $this->title = $category->name;
            $this->description = $category->description;
            $criteria->addCondition('category_id = :category_id');
            $criteria->params[':category_id'] = $categoryId;
        }

        $this->data = News::model()->cache($this->webforma->coreCacheTime)->with('user')->published()->public()->findAll(
            $criteria
        );
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'feed' => [
                'class'       => 'webforma\components\actions\WFeedAction',
                'data'        => $this->data,
                'title'       => $this->title,
                'description' => $this->description,
                'itemFields'  => [
                    'author_object'   => 'user',
                    'author_nickname' => 'nick_name',
                    'content'         => 'short_text',
                    'datetime'        => 'date',
                    'link'            => '/news/news/view',
                    'linkParams'      => ['title' => 'slug'],
                    'title'           => 'title',
                    'updated'         => 'update_time',
                ],
            ],
        ];
    }
}
