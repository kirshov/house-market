<?php

/**
 * NewsController контроллер для работы с новостями в публичной части сайта
 */
class NewsController extends \webforma\components\controllers\FrontController
{
    /**
     * @param $slug
     * @throws CHttpException
     */
    public function actionView($slug)
    {
        $model = News::model()->published();

        $model = $model->find('slug = :slug', [':slug' => $slug]);

        if (!$model) {
            throw new CHttpException(404, Yii::t('NewsModule.news', 'News article was not found!'));
        }

        // проверим что пользователь может просматривать эту новость
        if ($model->isProtected() && !Yii::app()->getUser()->isAuthenticated()) {
            Yii::app()->getUser()->setFlash(
                webforma\widgets\WFlashMessages::ERROR_MESSAGE,
                Yii::t('NewsModule.news', 'You must be an authorized user for view this page!')
            );

            $this->redirect([Yii::app()->getModule('user')->accountActivationSuccess]);
        }

        $this->render('view', ['model' => $model]);
    }

    /**
     *
     */
    public function actionIndex()
    {
        $dbCriteria = new CDbCriteria([
            'condition' => 't.status = :status',
            'params' => [
                ':status' => News::STATUS_PUBLISHED,
            ],
            'order' => 't.date DESC',
            'with' => ['user'],
        ]);

        if (!Yii::app()->getUser()->isAuthenticated()) {
            $dbCriteria->mergeWith(
                [
                    'condition' => 'is_protected = :is_protected',
                    'params' => [
                        ':is_protected' => News::PROTECTED_NO,
                    ],
                ]
            );
        }

        $dataProvider = new CActiveDataProvider('News', [
            'criteria' => $dbCriteria,
            'pagination' => [
                'pageSize' => (int)$this->getModule()->perPage,
            ],
        ]);

        $this->render('index', ['dataProvider' => $dataProvider]);
    }
}
