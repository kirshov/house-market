<?php

/**
 * NewsBackendController контроллер для работы с новостями в панели управления
 */
class NewsBackendController extends webforma\components\controllers\BackController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['News.NewsBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['News.NewsBackend.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['News.NewsBackend.Create']],
            ['allow', 'actions' => ['update', 'inline'], 'roles' => ['News.NewsBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['News.NewsBackend.Delete']],
            ['deny'],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'inline' => [
                'class' => 'webforma\components\actions\WInLineEditAction',
                'model' => 'News',
                'validAttributes' => ['title', 'slug', 'date', 'status'],
            ],
        ];
    }

    /**
     * Displays a particular model.
     *
     * @param integer $id the ID of the model to be displayed
     *
     * @return void
     */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return void
     */
    public function actionCreate()
    {
        $model = new News();

        if (($data = Yii::app()->getRequest()->getPost('News')) !== null) {
            $model->setAttributes($data);
			$model->tags = Yii::app()->getRequest()->getPost('tags');

            if ($model->save()) {

                Yii::app()->getUser()->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('NewsModule.news', 'News article was created!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['create']
                    )
                );
            }
        } else {
        	$model->status = News::STATUS_PUBLISHED;
	        $model->date = date('d.m.Y');
        }

        $this->render('create', ['model' => $model]);
    }


    /**
     * @param $id
     * @throws CHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (($data = Yii::app()->getRequest()->getPost('News')) !== null) {
            $model->setAttributes($data);

			$model->tags = Yii::app()->getRequest()->getPost('tags');

            if ($model->save()) {

                Yii::app()->getUser()->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('NewsModule.news', 'News article was updated!')
                );

                $this->redirect(
                    Yii::app()->getRequest()->getIsPostRequest()
                        ? (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['update', 'id' => $model->id]
                    )
                        : ['view', 'id' => $model->id]
                );
            }
        }

        $this->render(
            'update',
            [
                'model' => $model,
            ]
        );
    }


    /**
     * @param null $id
     * @throws CHttpException
     */
    public function actionDelete($id = null)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            $this->loadModel($id)->delete();

            Yii::app()->getUser()->setFlash(
                webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                Yii::t('NewsModule.news', 'Record was removed!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            Yii::app()->getRequest()->getParam('ajax') !== null || $this->redirect(
                (array)Yii::app()->getRequest()->getPost('returnUrl', 'index')
            );
        } else {
            throw new CHttpException(
                400,
                Yii::t('NewsModule.news', 'Bad raquest. Please don\'t use similar requests anymore!')
            );
        }
    }

    /**
     * Manages all models.
     *
     * @return void
     */
    public function actionIndex()
    {
        $model = new News('search');

        $model->unsetAttributes(); // clear any default values

        $model->setAttributes(Yii::app()->getRequest()->getParam('News', []));

        $this->render('index', ['model' => $model]);
    }


    /**
     * @param $id
     * @return static
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        if (($model = News::model()->findByPk($id)) === null) {
            throw new CHttpException(
                404,
                Yii::t('NewsModule.news', 'Requested page was not found!')
            );
        }

        return $model;
    }
}
