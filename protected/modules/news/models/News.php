<?php
/**
 * News основная моделька для новостей
 */

/**
 * This is the model class for table "News".
 *
 * The followings are the available columns in table 'News':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property string $date
 * @property string $title
 * @property string $slug
 * @property string $short_text
 * @property string $full_text
 * @property integer $user_id
 * @property integer $status
 * @property integer $is_protected
 * @property string $link
 * @property string $image
 * @property string $description
 * @property string $keywords
 */

use webforma\components\Event;
use webforma\widgets\WPurifier;

/**
 * Class News
 */
class News extends webforma\models\WModel
{
    /**
     *
     */
    const STATUS_DRAFT = 0;
    /**
     *
     */
    const STATUS_PUBLISHED = 1;
    /**
     *
     */
    const STATUS_MODERATION = 2;

    /**
     *
     */
    const PROTECTED_NO = 0;
    /**
     *
     */
    const PROTECTED_YES = 1;

	/**
	 * @var  string|array tags list
	 */
	public $tags;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{news_news}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * @param  string $className
     * @return News   the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['title, slug, short_text, full_text, keywords, description', 'filter', 'filter' => 'trim'],
            ['title, slug, keywords, description', 'filter', 'filter' => [new WPurifier(), 'purify']],
            ['date, title, slug, full_text', 'required', 'on' => ['update', 'insert']],
            ['status, is_protected, category_id', 'numerical', 'integerOnly' => true],
            ['title, slug, keywords', 'length', 'max' => 150],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            ['slug', 'unique'],
            ['description', 'length', 'max' => 250],
            ['link', 'length', 'max' => 250],
            ['link', 'webforma\components\validators\YUrlValidator'],
            [
                'slug',
                'webforma\components\validators\YSLugValidator',
                'message' => Yii::t('NewsModule.news', 'Bad characters in {attribute} field')
            ],
            ['category_id', 'default', 'setOnEmpty' => true, 'value' => null],
			['tags', 'safe'],
			['tags', 'default', 'value' => []],
            [
                'id, keywords, description, create_time, update_time, date, title, slug, short_text, full_text, user_id, status, is_protected, category_id',
                'safe',
                'on' => 'search'
            ],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $module = Yii::app()->getModule('news');

        $behaviors = [
			'imageUpload' => [
				'class' => 'webforma\components\behaviors\ImageUploadBehavior',
				'attributeName' => 'image',
				'uploadPath' => $module->uploadPath,
			],
		];
        if($module->useTags){
			$behaviors['tags'] = [
				'class' => 'vendor.yiiext.taggable-behavior.EARTaggableBehavior',
				'tagTable' => Yii::app()->getDb()->tablePrefix.'news_tag',
				'tagBindingTable' => Yii::app()->getDb()->tablePrefix.'news_to_tag',
				'tagModel' => 'NewsTag',
				'modelTableFk' => 'new_id',
				'tagBindingTableTagId' => 'tag_id',
				'cacheID' => 'cache',
			];
		}

		return $behaviors;
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'category' => [self::BELONGS_TO, 'Category', 'category_id'],
            'user' => [self::BELONGS_TO, 'User', 'user_id'],
        ];
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'published' => [
                'condition' => 't.status = :status',
                'params' => [':status' => self::STATUS_PUBLISHED],
            ],
            'protected' => [
                'condition' => 't.is_protected = :is_protected',
                'params' => [':is_protected' => self::PROTECTED_YES],
            ],
            'public' => [
                'condition' => 't.is_protected = :is_protected',
                'params' => [':is_protected' => self::PROTECTED_NO],
            ],
            'recent' => [
                'order' => 'create_time DESC',
                'limit' => 5,
            ]
        ];
    }

    /**
     * @param $num
     * @return $this
     */
    public function last($num)
    {
        $this->getDbCriteria()->mergeWith(
            [
                'order' => 'date DESC',
                'limit' => $num,
            ]
        );

        return $this;
    }

	/**
     * @param $category_id
     * @return $this
     */
    public function category($category_id)
    {
        $this->getDbCriteria()->mergeWith(
            [
                'condition' => 'category_id = :category_id',
                'params' => [':category_id' => $category_id],
            ]
        );

        return $this;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('NewsModule.news', 'Id'),
            'category_id' => Yii::t('NewsModule.news', 'Category'),
            'create_time' => Yii::t('NewsModule.news', 'Created at'),
            'update_time' => Yii::t('NewsModule.news', 'Updated at'),
            'date' => Yii::t('NewsModule.news', 'Date'),
            'title' => Yii::t('NewsModule.news', 'Title'),
            'slug' => Yii::t('NewsModule.news', 'Alias'),
            'image' => Yii::t('NewsModule.news', 'Image'),
            'link' => Yii::t('NewsModule.news', 'Link'),
            'short_text' => Yii::t('NewsModule.news', 'Short text'),
            'full_text' => Yii::t('NewsModule.news', 'Full text'),
            'user_id' => Yii::t('NewsModule.news', 'Author'),
            'status' => Yii::t('NewsModule.news', 'Status'),
            'is_protected' => Yii::t('NewsModule.news', 'Access only for authorized'),
            'keywords' => Yii::t('NewsModule.news', 'Keywords (SEO)'),
            'description' => Yii::t('NewsModule.news', 'Description (SEO)'),
			'tags' => 'Тэги',
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (!$this->slug) {
            $this->slug = webforma\helpers\WText::translit($this->title);
        }

        return parent::beforeValidate();
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        $this->update_time = new CDbExpression('NOW()');
        $this->date = date('Y-m-d', strtotime($this->date));

        if ($this->getIsNewRecord()) {
            $this->create_time = $this->update_time;
            $this->user_id = Yii::app()->getUser()->getId();
        }

        if(Yii::app()->getModule('news')->useTags){
	        $this->setTags($this->tags);
        }

        return parent::beforeSave();
    }

    /**
     *
     */
    public function afterSave()
    {
        Yii::app()->eventManager->fire(NewsEvents::NEWS_AFTER_SAVE, new Event($this));

        return parent::afterSave();
    }

    /**
     *
     */
    public function afterDelete()
    {
        Yii::app()->eventManager->fire(NewsEvents::NEWS_AFTER_DELETE, new Event($this));

        parent::afterDelete();
    }

    /**
     *
     */
    public function afterFind()
    {
	    if(Yii::app()->getModule('news')->useTags) {
		    $this->tags = $this->getTags();
	    }
        $this->date = date('d.m.Y', strtotime($this->date));

        return parent::afterFind();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria();

        $criteria->compare('t.id', $this->id);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);
        if ($this->date) {
            $criteria->compare('date', date('Y-m-d', strtotime($this->date)));
        }
        $criteria->compare('title', $this->title, true);
        $criteria->compare('t.slug', $this->slug, true);
        $criteria->compare('short_text', $this->short_text, true);
        $criteria->compare('full_text', $this->full_text, true);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('category_id', $this->category_id, true);
        $criteria->compare('is_protected', $this->is_protected);
        $criteria->with = ['category'];

        return new CActiveDataProvider(get_class($this), [
            'criteria' => $criteria,
            'sort' => ['defaultOrder' => 'date DESC'],
        ]);
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_DRAFT => Yii::t('NewsModule.news', 'Draft'),
            self::STATUS_PUBLISHED => Yii::t('NewsModule.news', 'Published'),
            //self::STATUS_MODERATION => Yii::t('NewsModule.news', 'On moderation'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('NewsModule.news', '*unknown*');
    }

    /**
     * @return array
     */
    public function getProtectedStatusList()
    {
        return [
            self::PROTECTED_NO => Yii::t('NewsModule.news', 'no'),
            self::PROTECTED_YES => Yii::t('NewsModule.news', 'yes'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getProtectedStatus()
    {
        $data = $this->getProtectedStatusList();

        return isset($data[$this->is_protected]) ? $data[$this->is_protected] : Yii::t('NewsModule.news', '*unknown*');
    }

    /**
     * @return string
     */
    public function getCategoryName()
    {
        return ($this->category === null) ? '---' : $this->category->name;
    }

    /**
     * @return bool
     */
    public function isProtected()
    {
        return $this->is_protected == self::PROTECTED_YES;
    }

	/**
	 * @return string
	 */
    public function getFormatDate(){
    	DateHelper::getDate($this->date, 2);
    }

	/**
	 * @param $tag
	 * @param array $with
	 * @return mixed
	 */
	public function getByTag($tag, array $with = ['news'])
	{
		return News::model()->with($with)
			->published()
			->public()
			->taggedWith($tag)->findAll();
	}
}
