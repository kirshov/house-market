<?php

use webforma\components\WebModule;

/**
 * Class RobokassaModule
 */
class RobokassaModule extends WebModule
{
    /**
     * @return array
     */
    public function getDependencies()
    {
        return ['payment'];
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [];
    }

    /**
     * @return bool
     */
    public function getAdminPageLink()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function getIsShowInAdminMenu()
    {
        return false;
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('RobokassaModule.robokassa', 'Store');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('RobokassaModule.robokassa', 'Robokassa');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('RobokassaModule.robokassa', 'Robokassa payment module');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-rub';
    }
}
