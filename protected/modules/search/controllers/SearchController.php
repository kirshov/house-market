<?php

/**
 * Class SearchController
 */
class SearchController extends \webforma\components\controllers\FrontController
{
	public function init()
	{
		parent::init();
		Yii::app()->setImport([
			'webforma.extensions.phpMorphy.FMorphy',
		]);
	}

	/**
	 * @return array
	 */
	public function parseWord(){
        $q = Yii::app()->getRequest()->getQuery('q', []);
        $n = [];
        foreach (explode(' ', $q) as $_t){
            $_t = trim($_t);
            if($_t && mb_strlen($_t) >= 3){
                $n[] = $_t;
            }
        }
        return $n;
    }

	/**
	 * @throws CException
	 */
    public function actionIndex()
    {
		$term = Search::getTerm();

	    if ($term) {
        	$termMorphy = FMorphy::getRoots(explode(' ', $term));
		    $termMorphy = array_map(function($value){
			    return '+'.$value.'*';
		    }, $termMorphy);

		    $terms = Yii::app()->db->quoteValue(implode(' ', $termMorphy));

		    $dataProvider = new CActiveDataProvider('Search',[
		    	'criteria' => [
				    'select' => '*, MATCH(title, content) AGAINST ('.$terms.' IN BOOLEAN MODE) AS rel',
				    'condition' => 'MATCH(title, content) AGAINST ('.$terms.' IN BOOLEAN MODE)',
				    'order' => 'rel DESC',
				    'having' => 'rel > 0.5',
			    ],
			    'pagination' => [
			    	'pageSize' => 20,
			    ]
		    ]);
        }

	    $this->render('search', [
		    'dataProvider' => $dataProvider,
	    ]);
    }
}