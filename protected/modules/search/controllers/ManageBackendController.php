<?php
class ManageBackendController extends webforma\components\controllers\BackController
{

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index', 'create'], 'roles' => ['search.ManageBackend.Index']],
            ['deny'],
        ];
    }

    /**
     * Index-экшен:
     *
     * @return void
     */
    public function actionIndex()
    {
        $this->render('index');
    }

    /**
     * Search index creation
     */
    public function actionCreate()
    {
        /**
         * Если это не AJAX-запрос - посылаем:
         **/
        if (!Yii::app()->getRequest()->getIsPostRequest() && !Yii::app()->getRequest()->getIsAjaxRequest()) {
            throw new CHttpException(404, Yii::t('SearchModule.search', 'Page was not found!'));
        }

        try {
            // Модели, включенные в индекс
            $searchModels = Yii::app()->getModule('search')->searchModels;
            $messages = [];

            Search::model()->deleteAll();
            // Пробежаться по всем моделям и добавить их в индекс
            foreach ($searchModels as $modelName => $model) {
                if (!empty($model['path'])) {
                    Yii::import($model['path']);
                }
                if (!isset($model['module'])) {
                    $messages[] = Yii::t('SearchModule.search', 'Update config file or module, Module index not found for model "{model}"!', ['{model}' => $modelName]);
                } elseif (is_file(Yii::getPathOfAlias($model['path']).'.php') && Yii::app()->hasModule($model['module'])) {
                    $criteria = isset($model['criteria']) ? $model['criteria'] : [];
                    $searchNodes = $modelName::model()->findAll(new CDbCriteria($criteria));

                    foreach ($searchNodes as $node) {
                    	$doc = new Search();
                    	$doc->module = $model['module'];
                    	$doc->title = $node->{$model['titleColumn']};
                    	$doc->link = str_replace('{'.$model['linkColumn'].'}', $node->{$model['linkColumn']}, $model['linkPattern']);

	                    $contentColumns = explode(',', $model['textColumns']);
	                    foreach ($contentColumns as $column) {
		                    $doc->content .= $this->cleanContent($node->{$column}).' ';
	                    }
	                    $doc->save();
                    }
                } /*else {
                    $messages[] = Yii::t('SearchModule.search', 'Module "{module}" not installed!', ['{module}' => $model['module']]);
                }*/
            }

            Yii::app()->ajax->raw(
                empty($messages)
                    ? Yii::t('SearchModule.search', 'Index updated successfully!')
                    : Yii::t('SearchModule.search', 'There is an error!')
                    .': '
                    .implode("\n", $messages)
            );
        } catch (Exception $e) {
            Yii::app()->ajax->raw(
                Yii::t('SearchModule.search', 'There is an error!').":\n".$e->getMessage()
            );
        }
    }

    /**
     * @param $data
     * @return string
     */
    private function cleanContent($data)
    {
	    $data = preg_replace('/\[[^\[]*\]/', '', $data);
	    $data = preg_replace('/<table>.*<\/table>/', '', $data);
        return strip_tags($data);
    }
}
