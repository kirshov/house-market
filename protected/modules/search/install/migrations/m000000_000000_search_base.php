<?php

class m000000_000000_search_base extends webforma\components\DbMigration
{

    public function safeUp()
    {
        $this->createTable(
            '{{search_index}}',
            [
                'id'          => 'pk',
                'title'       => 'varchar(250) NOT NULL',
	            'module'      => "varchar(255) DEFAULT NULL",
                'content'     => "text DEFAULT NULL",
                'link'        => "varchar(255) DEFAULT NULL",
            ],
            'ENGINE=MyISAM DEFAULT CHARSET=utf8'
        );


	    $this->execute('ALTER TABLE {{search_index}} ADD FULLTEXT INDEX full_text (title, content)');
    }

    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{search_index}}');
    }
}
