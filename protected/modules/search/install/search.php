<?php

return [
    'module' => [
        'class' => 'application.modules.search.SearchModule',
        'searchModels' => [
            'News' => [
                'path' => 'application.modules.news.models.News',
                'module' => 'news',
                'titleColumn' => 'title',
                'linkColumn' => 'slug',
                'linkPattern' => '/news/news/view?slug={slug}',
                'textColumns' => 'full_text',
                'criteria' => [
                    'condition' => 'status = :status',
                    'params' => [
                        ':status' => 1
                    ],
                ],
            ],
	        'Articles' => [
		        'path' => 'application.modules.articles.models.Articles',
		        'module' => 'articles',
		        'titleColumn' => 'title',
		        'linkColumn' => 'slug',
		        'linkPattern' => '/articles/articles/view?slug={slug}',
		        'textColumns' => 'full_text',
		        'criteria' => [
			        'condition' => 'status = :status',
			        'params' => [
				        ':status' => 1
			        ],
		        ],
	        ],
            'Page' => [
                'path' => 'application.modules.page.models.Page',
                'module' => 'page',
                'titleColumn' => 'title',
                'linkColumn' => 'link',
                'linkPattern' => '{link}',
                'textColumns' => 'body',
                'criteria' => [
                    'condition' => 'status = :status',
                    'params' => [
                        ':status' => 1,
                    ],
                ],
            ],
	        'Product' => [
		        'path' => 'application.modules.store.models.Product',
		        'module' => 'store',
		        'titleColumn' => 'name',
		        'linkColumn' => 'link',
		        'linkPattern' => '{link}',
		        'textColumns' => 'description',
		        'criteria' => [
			        'condition' => 'status = :status',
			        'params' => [
				        ':status' => 1,
			        ],
		        ],
	        ],
        ],
    ],
    'import' => [
        'application.modules.search.models.*',
    ],
    'component' => [],
	'params'    => [
		'widgets'   => [
			'search' => 'application.modules.search.widgets.SearchBlockWidget',
		],
	],
    'rules' => [
        '/search' => 'search/search/index',
    ],
];
