<?php

/**
 * Виджет вывода поля для поискового запроса
 **/
class SearchBlockWidget extends webforma\widgets\WWidget
{
    /**
     * @var string
     */
    public $view = 'searchblockwidget';

    /**
     * @throws CException
     */
    public function run()
    {
        $this->render($this->view);
    }
}
