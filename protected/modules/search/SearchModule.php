<?php

/**
 * SearchModule основной класс модуля search
 */
class SearchModule extends webforma\components\WebModule
{
	public $adminMenuOrder = 4;

	/**
	 * @var array
	 */
	public $searchModels = [];

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [];
    }

    /**
     * @return bool
     */
    public function getIsInstallDefault()
    {
        return false;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('SearchModule.search', 'Services');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('SearchModule.search', 'Search');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('SearchModule.search', 'Find module');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-search';
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/search/manageBackend/index';
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'application.modules.search.models.*',
            ]
        );
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('SearchModule.search', 'Index'),
                'url' => ['/search/manageBackend/index'],
            ],
        ];
    }
}
