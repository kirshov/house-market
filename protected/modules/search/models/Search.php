<?php
/**
 * Search model class:
 *
 * This is the model class for table "Search".
 *
 * The followings are the available columns in table 'Search':
 * @property string $id
 * @property string $title
 * @property string $module
 * @property string $content
 * @property string $link
 *
 */
class Search extends webforma\models\WModel
{
	/**
	 * @var
	 */
	protected static $morphyForms;

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className - инстанс модели
     *
     * @return Search the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Имя таблицы в БД:
     *
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{search_index}}';
    }

    /**
     * Список правил для валидации полей модели:
     *
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['title,content, module, link', 'safe'],
        ];
    }

    public function getLink(){
	    $paramsArray = [];

	    $linkArray = explode('?', $this->link);
	    if (isset($linkArray[0])) {
		    $resultLink = $linkArray[0];
	    } else {
		    $resultLink = $this->link;
	    }

	    if (isset($linkArray[1])) {
		    foreach (explode('&', $linkArray[1]) as $param) {
			    $paramArray = explode('=', $param);
			    $paramsArray[$paramArray[0]] = $paramArray[1];
		    }
	    }

	    return Yii::app()->getController()->createUrl($resultLink, $paramsArray);
    }

	/**
	 * @return string
	 */
	public function getHighlightContent()
	{
		$term = self::getTerm();
		$termMorphy = FMorphy::getRoots(explode(' ', $term));

		$strPos = mb_strpos($this->content, $termMorphy[0]);
		$content = $this->content;
		if($strPos > 150 && mb_strlen($content) > 650){
			$firstWord = 0;
			$exp = explode(' ', $content);
			foreach ($exp as $_item){
				$firstWord += (mb_strlen($_item) + 1);
				if($firstWord > 100){
					break;
				}
			}
			$content = mb_substr($content, $firstWord);
			$content = '... '.$content;
		}
		if(strlen($content) > 650){
			$content = mb_substr($content, 0, 650);
			$content .= '...';
		}

		if(self::$morphyForms === null){
			self::$morphyForms = FMorphy::getAllForms(explode(' ', $term));
		}

		$content = preg_replace('/('.implode('|', self::$morphyForms).')/iu', '<span class="highlight">$1</span>', $content);

		return $content;
	}

	/**
	 * @return string|string[]|null
	 */
	public static function getTerm(){
		$term = Yii::app()->getRequest()->getQuery('q', null);
		$term = trim(preg_replace('/\s(\S{1,2})\s/', ' ', $term));
		$term = preg_replace('/ +/', ' ', $term);

		$terms = explode(' ', $term);
		foreach ($terms as $_key => $_term){
			if(mb_strlen($_term) < 3){
				unset($terms[$_key]);
			}
		}

		return implode(' ', $terms);
	}
}
