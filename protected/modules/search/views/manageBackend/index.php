<?php
/* @var $this DefaultController */

$this->breadcrumbs = [
    Yii::t('SearchModule.search', 'Find') => ['/search/manageBackend/index'],
    Yii::t('SearchModule.search', 'Manage'),
];

$this->pageTitle = Yii::t('SearchModule.search', 'Find - manage');
?>
<p>
    <?= Yii::t(
        'SearchModule.search',
        'Models you want to index is necessary to describe in configuration file.'
    ); ?><br/>
    <?= Yii::t('SearchModule.search', 'For index creation, please click button below.'); ?>
</p>
<?php
$this->widget(
    'bootstrap.widgets.TbButton',
    [
        'buttonType' => 'ajaxButton',
        'id' => 'create-search',
        'context' => 'primary',
        'label' => Yii::t('SearchModule.search', 'Update find index'),
        'loadingText' => Yii::t('SearchModule.search', 'Index is updating... Wait please...'),
        'size' => 'large',
        'url' => $this->createUrl('/search/manageBackend/create'),
        'ajaxOptions' => [
            'type' => 'POST',
            'data' => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
            'url' => $this->createUrl('/search/manageBackend/create'),
            'beforeSend' => 'function () {
	       $("#create-search").text("'.Yii::t('SearchModule.search', 'Wait please...').'");
	     }',
            'success' => 'js:function (data,status) {
            $("#create-search").text("'.Yii::t('SearchModule.search', 'Update find index').'");
            alert(data);
	     }',
        ],
    ]
);
?>
