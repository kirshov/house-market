<?php

/**
 * Class FavoriteWidget
 */
class FavoriteWidget extends \webforma\widgets\WWidget
{
    /**
     * @var
     */
    public $favorite;

    /**
     * @var string
     */
    public $view = 'default';

    /**
     *
     */
    public function init()
    {
		$this->favorite = Yii::app()->getComponent('favorite');
		/*if(Yii::app()->getUser()->getIsGuest()){
			$this->favorite = Yii::app()->getComponent('favorite');
		} else {
			$this->favorite = Yii::app()->getComponent('favoriteDb');
		}*/

        parent::init();
    }

    /**
     * @throws CException
     */
    public function run()
    {
        $this->render($this->view, ['favorite' => $this->favorite]);
    }
}