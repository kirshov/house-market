<?php

/**
 * Class FavoriteControl
 */
class FavoriteControl extends \webforma\widgets\WWidget
{
    /**
     * @var
     */
    public $product;

    /**
     * @var
     */
    public $favorite;

    /**
     * @var string
     */
    public $view = 'in-list';

    /**
     *
     */
    public function init()
    {
        Yii::app()->getClientScript()->registerScriptFile(
            Yii::app()->getAssetManager()->publish(
                Yii::getPathOfAlias('application.modules.favorite.view.web') . '/favorite.js'
            ),
            CClientScript::POS_END
        );

		$this->favorite = Yii::app()->getComponent('favorite');
	    /*if(Yii::app()->getUser()->getIsGuest()){
		    $this->favorite = Yii::app()->getComponent('favorite');
	    } else {
		    $this->favorite = Yii::app()->getComponent('favoriteDb');
	    }*/

        parent::init();
    }

    /**
     * @throws CException
     */
    public function run()
    {
    	if($this->favorite == null){
    		return false;
	    }
        $this->render($this->view, ['product' => $this->product, 'favorite' => $this->favorite]);
    }
}