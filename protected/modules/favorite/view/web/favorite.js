/*добавление/удаление товаров в избранное*/
$(document).ready(function () {
    $(document).on('click', '.store-favorite-add', function (event) {
        event.preventDefault();
        var $this = $(this);
        var product = parseInt($this.data('id'));
        var data = {'id': product};
        data[webformaTokenName] = webformaToken;
        $.post(storeAddFavoriteUrl, data, function (data) {
            if (data.result) {
				doTarget('favorite');
                $.each($('.store-favorite-total'), function () {
                    $(this).html(data.count);
                    $(this).closest('.favorite-widget').removeClass('empty');
                });

                $this
                    .removeClass('store-favorite-add')
                    .addClass('store-favorite-remove');
                /*if($this.html()){
					$this.html('В избранном');
                }*/
            }
        }, 'json');
    });

    $(document).on('click', '.store-favorite-remove', function (event) {
        event.preventDefault();
        var $this = $(this);
        var product = parseInt($this.data('id'));
        var data = {'id': product};
        data[webformaTokenName] = webformaToken;
        $.post(storeRemoveFavoriteUrl, data, function (data) {
            if (data.result) {

                $.each($('.store-favorite-total'), function () {
                    if(data.count == 0) {
                        $(this).html('');
                        $(this).closest('.favorite-widget').addClass('empty');
                    } else {
                        $(this).html(data.count);
                    }

                });

                if($this.hasClass('close-btn')){
                    $this.closest('.product-item').remove();
                    return true;
                }

                $this
                    .removeClass('store-favorite-remove')
                    .addClass('store-favorite-add');

				/*if($this.html()){
					$this.html('В избранное');
				}*/

            }
        }, 'json');
    });
});