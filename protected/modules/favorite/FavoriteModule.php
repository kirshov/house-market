<?php

/**
 * Class FavoriteModule
 */
class FavoriteModule extends \webforma\components\WebModule
{
    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(array(
            'favorite.components.*',
            'favorite.models.*',
        ));
    }

    /**
     * @return bool
     */
    public function getIsShowInAdminMenu()
    {
        return false;
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return ['store'];
    }

    /**
     * @return bool
     */
    public function getAdminPageLink()
    {
        return false;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('FavoriteModule.favorite', 'Favorite');
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('FavoriteModule.favorite', 'Store');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('FavoriteModule.favorite', 'Favorite products module');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-heart';
    }
}
