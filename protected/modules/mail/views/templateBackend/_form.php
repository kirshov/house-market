<?php
/**
 * @var $this TemplateBackendController
 * @var $model MailTemplate
 * @var $form \webforma\widgets\ActiveForm
 */
$form = $this->beginWidget(
    '\webforma\widgets\ActiveForm',
    [
        'id' => 'mail-template-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => 'vertical',
		'htmlOptions' => ['class' => 'well sticky'],
		'clientOptions' => [
			'validateOnSubmit' => true,
		],
    ]
); ?>

<div class="alert alert-info">
    <?=  Yii::t('MailModule.mail', 'Fields, with'); ?>
    <span class="required">*</span>
    <?=  Yii::t('MailModule.mail', 'are required.'); ?>
</div>

<?=  $form->errorSummary($model); ?>

<div class='row'>
    <div class="col-sm-7">
        <?=  $form->textFieldGroup($model, 'name'); ?>
    </div>
</div>

<div class='row'>
    <div class="col-sm-7">
        <?=  $form->textFieldGroup($model, 'theme'); ?>
    </div>
</div>

<div class='row'>
    <div class="col-sm-12 form-group">
        <?=  $form->labelEx($model, 'body'); ?>
        <?php $this->widget(
            $this->module->getVisualEditor(),
            [
                'model'     => $model,
                'attribute' => 'body',
				'selector' => null,
				'settings' => [
					'force_p_newlines' => false,
					'forced_root_block' => false,
				]
            ]
        ); ?>
    </div>

</div>
<div class='row'>
    <div class="col-sm-12 form-group">
        <?=  $form->labelEx($model, 'description'); ?>
        <?php $this->widget(
            $this->module->getVisualEditor(),
            [
                'model'     => $model,
                'attribute' => 'description',
            ]
        ); ?>
    </div>

</div>

<div class="buttons">
	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context'    => 'primary',
		'label'      => $model->isNewRecord
			? Yii::t('MailModule.mail', 'Create template and continue')
			: Yii::t('MailModule.mail', 'Save template and continue'),
	]); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType'  => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label'       => $model->isNewRecord
			? Yii::t('MailModule.mail', 'Create template and close')
			: Yii::t('MailModule.mail', 'Save template and close'),
	]); ?>
</div>

<?php $this->endWidget(); ?>
