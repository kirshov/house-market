<?php
$this->breadcrumbs = [
    Yii::t('MailModule.mail', 'Mail templates') => ['index'],
    $model->name                                => ['view', 'id' => $model->id],
    Yii::t('MailModule.mail', 'Edit'),
];
$this->pageTitle = Yii::t('MailModule.mail', 'Edit mail template');
$this->menu = [
    ['label' => Yii::t('MailModule.mail', 'Mail templates')],
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('MailModule.mail', 'Templates list'),
        'url'   => ['/mail/templateBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-pencil',
        'label' => Yii::t('MailModule.mail', 'Edit template'),
        'url'   => [
            '/mail/templateBackend/update',
            'id' => $model->id
        ]
    ],
    [
        'icon'  => 'fa fa-fw fa-eye',
        'label' => Yii::t('MailModule.mail', 'View template'),
        'url'   => [
            '/mail/templateBackend/view',
            'id' => $model->id
        ]
    ],
];
?>
<?=  $this->renderPartial('_form', ['model' => $model]); ?>
