<style>
    .checkbox-column{display: none;}
</style>
<?php
$this->breadcrumbs = [
    Yii::t('MailModule.mail', 'Mail templates') => ['index'],
    Yii::t('MailModule.mail', 'Management'),
];
$this->pageTitle = Yii::t('MailModule.mail', 'Mail templates list');

$this->menu = [
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('MailModule.mail', 'Mail templates'),
        'url'   => ['/mail/templateBackend/index']
    ],
];
?>

<?php
$this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id'             => 'mail-template-grid',
        'dataProvider'   => $model->search(),
        'filter'         => $model,
        'hideBulkActions' => true,
        'actionsButtons' => [],
        'columns'        => [
			[
				'name'        => 'icon',
				'type'        => 'raw',
				'value'       => function($data){
			        return '<i class="fa '.$data->icon.'"></i>';
                },
				'htmlOptions' => ['style' => 'width:30px; text-align: center;'],
			],
            [
				'type'        => 'raw',
                'name'     => 'name',
				//
				'value' => function($data){
					return CHtml::link($data->name, $this->createUrl('/mail/templateBackend/view', ['id' => $data->id]));
				},
                'filter'   => CHtml::activeTextField($model, 'name', ['class' => 'form-control']),
            ],
            [
                'class'    => 'webforma\widgets\WEditableColumn',
                'name'     => 'theme',
                'editable' => [
                    'url'    => $this->createUrl('/mail/templateBackend/inline'),
                    'mode'   => 'inline',
                    'type'   => 'text',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'theme', ['class' => 'form-control']),
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
				'buttons' => [
					'delete' => [
						'url' => function ($data) {
							return Yii::app()->createUrl('/mail/templateBackend/delete', ['id' => $data->id]);
						},
                        'visible' => function () { return DEVMODE; },
					],
				],
            ],
        ],
    ]
);