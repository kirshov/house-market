<?php
use webforma\widgets\WPurifier;

/**
 * This is the model class for table "mail_template".
 *
 * The followings are the available columns in table 'mail_template':
 *
 * @property string $id
 * @property string $name
 * @property string $module
 * @property string $code
 * @property string $description
 * @property string $theme
 * @property string $body
 * @property string $datetime
 *
 * The followings are the available model relations:
 *
 * MailTemplate model class
 * Класс модели MailTemplate
 **/
class MailTemplate extends webforma\models\WModel
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 0;

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return MailTemplate the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Получаем имя таблицы:
     *
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{mail_template}}';
    }

    /**
     * Получаем правила валидации полей таблицы:
     *
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['code, name, description, theme, icon', 'filter', 'filter' => [$obj = new WPurifier(), 'purify']],
            ['code, name, theme, body, module', 'required'],
            ['code', 'length', 'max' => 100],
            ['code', 'unique'],
            ['description', 'safe'],
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            ['id, name, description, theme, body', 'safe', 'on' => 'search'],
        ];
    }

	public function behaviors()
	{
		return [
			'CTimestampBehavior' => [
				'class' => 'zii.behaviors.CTimestampBehavior',
				'setUpdateOnCreate' => true,
				'createAttribute' => null,
				'updateAttribute' => 'update_at',
			],
		];
	}


	/**
     * Получаем атрибуты меток полей данной таблицы:
     *
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('MailModule.mail', 'ID'),
            'name'        => Yii::t('MailModule.mail', 'Title'),
            'description' => Yii::t('MailModule.mail', 'Description'),
            'theme'       => Yii::t('MailModule.mail', 'Topic'),
            'body'        => Yii::t('MailModule.mail', 'Message'),
            'code'        => Yii::t('MailModule.mail', 'Symbolic code'),
			'icon' => '',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria();

        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('theme', $this->theme, true);
        $criteria->compare('body', $this->body, true);

        return new CActiveDataProvider(get_class($this), ['criteria' => $criteria]);
    }

	/**
	 * @param $module
	 * @param $code
	 * @return MailTemplate|null
	 */
    public static function getTemplate($module, $code){
    	$model = self::model()->find('module = :module AND code = :code', [
    		':module' => $module,
    		':code' => $code,
	    ]);

    	if($model){
    		return [
    			'subject' => $model->theme,
    			'body' => $model->body,
		    ];
	    }

    	return null;
    }
}
