<?php

/**
 * MailModule основной класс модуля install
 */
class MailModule extends webforma\components\WebModule
{
	/**
	 * @var int
	 */
	public $adminMenuOrder = 18;

	/**
	 * @var string
	 */
	public $btnColor = '#0088cc';

	/**
	 * @var string
	 */
	public $btnTextColor = '#ffffff';

	/**
	 * @var string
	 */
	public $linkColor = '#0088cc';

	/**
	 * @var string
	 */
	public $font = '13px Arial';

	/**
	 * @var string
	 */
	public $fontH2 = '24px Arial';


	/**
	 * @var string
	 */
	public $fontH3 = '16px Arial';

	/**
	 * @var string
	 */
	public $borderColor = '#fecc54';


	/**
	 * @var
	 */
	public $socInstagram;

	/**
	 * @var
	 */
	public $socVk;

	/**
	 * @var
	 */
	public $socFacebook;

	/**
	 * @var
	 */
	public $socOK;

	/**
	 * @var
	 */
	public $socTelegram;

	/**
	 * @var
	 */
	public $socTwitter;

	/**
	 * @var
	 */
	public $socYoutube;

	/**
	 * @var
	 */
	public $footerText;

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [
            'editor' => Yii::t('MailModule.mail', 'Visual editor'),

			'btnColor' => 'Цвет кнопки',
			'btnTextColor' => 'Цвет текста кнопки',
			'linkColor' => 'Цвет ссылки',
			'font' => 'Шрифт',
			'fontH2' => 'Шрифт заголовка',
			'fontH3' => 'Шрифт подзаголовка',
			'borderColor' => 'Цвет рамки',

			'footerText' => 'Текст в нижней части письма',
			'socInstagram' => 'Ссылка на страницу инстаграм',
			'socVk' => 'Ссылка на страницу ВКонтакте',
			'socFacebook' => 'Ссылка на страницу Фейсбук',
			'socOK' => 'Ссылка на страницу в Одноклассниках',
			'socTelegram' => 'Ссылка на Телеграм канал',
			'socTwitter' => 'Ссылка на страницу в Twitter',
			'socYoutube' => 'Ссылка на страницу Youtube канал',
        ];
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
    	$params = [
			'socHint' => [
				'input-type' => 'html',
				'html' => '<div class="help-block">При заполненных ссылка на страницы в соц. сетях они будут выведены в нижней части письма</div>',
			],
			'socInstagram',
			'socVk',
			'socFacebook',
			'socOK',
			'socTelegram',
			'socTwitter',
			'socYoutube',
		    'footerText' => [
		    	'input-type' => 'textarea',
			    'htmlOptions' => [
			    	'style' => 'height: 50px;'
			    ],
			    'hint' => '
					<small>Например: Вы получили это письмо, потому что являетесь клиентом интернет-магазина "Название"</small>
				',
		    ],
		];

    	if(DEVMODE){
    		$params = CMap::mergeArray($params, [
				'font',
				'fontH2',
				'fontH3',
				'linkColor',
				'btnColor',
				'btnTextColor',
				'borderColor',
			]);
		}
        return $params;
    }

    /**
     * @return array
     */
    public function getEditableParamsGroups()
    {
        $groups = [
            '1.social' => [
                'label' => 'Настройки ссылок на соц. сети',
                'items' => [
					'footerText',
					'socHint',
					'socInstagram',
					'socVk',
					'socFacebook',
					'socOK',
					'socTelegram',
					'socTwitter',
					'socYoutube',
                ],
            ],
        ];

		if(DEVMODE){
			$groups = CMap::mergeArray($groups, [
				'2.template' => [
					'label' => 'Настройки шаблона',
					'items' => [
						'font',
						'fontH2',
						'fontH3',
						'linkColor',
						'btnColor',
						'btnTextColor',
						'borderColor',
					],
				],
			]);
		}

        return $groups;
    }

    /**
     * Метод получения категории:
     *
     * @return string category
     **/
    public function getCategory()
    {
        return Yii::t('MailModule.mail', 'Services');
    }

    /**
     * Метод получения названия модуля:
     *
     * @return string name
     **/
    public function getName()
    {
        return 'Шаблоны почтовых уведомлений';
    }

    /**
     * Метод получения описвния модуля:
     *
     * @return string description
     **/
    public function getDescription()
    {
        return Yii::t('MailModule.mail', 'Module for mail message management');
    }

    /**
     * Метод получения иконки:
     *
     * @return string icon
     **/
    public function getIcon()
    {
        return 'fa fa-fw fa-envelope';
    }

    /**
     * Метод получения адреса модуля в админ панели:
     *
     * @return string admin url
     **/
    public function getAdminPageLink()
    {
    	return $this->getSettingsUrl();
        //return '/mail/templateBackend/index';
    }

    /**
     * Метод инициализации модуля:
     *
     * @return nothing
     **/
    public function init()
    {
        $this->setImport(
            [
                'mail.models.*',
                'mail.components.*',
            ]
        );

        parent::init();
    }

	/**
	 * @return array
	 */
	public function getNavigation()
	{
		return Helper::hasMidTariff()
			? [
				[
					'icon' => $this->getIcon(),
					'label' => 'Список шаблонов',
					'url' => ['/mail/templateBackend/index'],
				],
			]
			: [];
	}

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [];
    }

    /**
     * @return bool
     */
    public function getIsNoDisable()
    {
        return true;
    }

	/**
	 * @return bool
	 */
	public function getIsInstallDefault()
	{
		return true;
	}
}
