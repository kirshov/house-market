<?php

/**
 * TemplateBackendController - Класс контроллера Template:
 **/
class TemplateBackendController extends webforma\components\controllers\BackController
{
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Mail.TemplateBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Mail.TemplateBackend.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['Mail.TemplateBackend.Create']],
            ['allow', 'actions' => ['update', 'inline'], 'roles' => ['Mail.TemplateBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Mail.TemplateBackend.Delete']],
            ['deny']
        ];
    }

    public function actions()
    {
        return [
            'inline' => [
                'class'           => 'webforma\components\actions\WInLineEditAction',
                'model'           => 'MailTemplate',
                'validAttributes' => ['event_id', 'name', 'description', 'from', 'to', 'theme', 'status']
            ]
        ];
    }

    /**
     * Отображает почтовый шаблон по указанному идентификатору
     *
     * @param integer $id Идинтификатор почтовый шаблон для отображения
     *
     * @return void
     */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }

    /**
     * Редактирование почтового шаблона.
     *
     * @param integer $id - the ID of the model to be updated
     *
     * @return void
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (($data = Yii::app()->getRequest()->getPost('MailTemplate')) !== null) {

            $model->setAttributes($data);

            if ($model->save()) {

                Yii::app()->user->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('MailModule.mail', 'Record was updated!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['update', 'id' => $model->id]
                    )
                );
            }
        }

        $this->render('update', ['model' => $model]);
    }

    /**
     * Удаяет модель почтового шаблона из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id - идентификатор почтового шаблона, который нужно удалить
     *
     * @return void
     *
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
    	if(!DEVMODE){
			throw new CHttpException(400, Yii::t('MailModule.mail', 'Bad request. Please don\'t repeate similar request anymore'));
		}
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                Yii::t('MailModule.mail', 'Record was removed!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            Yii::app()->getRequest()->getParam('ajax') !== null || $this->redirect(
                (array)Yii::app()->getRequest()->getPost('returnUrl', 'index')
            );
        } else {
            throw new CHttpException(
                400,
                Yii::t('MailModule.mail', 'Bad request. Please don\'t repeate similar request anymore')
            );
        }
    }

    /**
     * Управление почтовыми шаблонами.
     *
     * @return void
     */
    public function actionIndex()
    {
    	$cacheKey = 'email::template::sync';
    	$isSync = Yii::app()->getCache()->get($cacheKey);
    	if(!$isSync) {
		    //Yii::app()->getCache()->set($cacheKey, $this->webforma->coreCacheTime);
		    $this->synchronization();
	    }
        $model = new MailTemplate('search');

        $model->unsetAttributes(); // clear any default values

        $model->setAttributes(Yii::app()->getRequest()->getParam('MailTemplate', []));

        $this->render('index', ['model' => $model]);
    }

    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param integer $id - идентификатор нужной модели
     *
     * @return class $model
     *
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = MailTemplate::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(
                404,
                Yii::t('MailModule.mail', 'Requested page was not found')
            );
        }

        return $model;
    }

	/**
	 *
	 */
    protected function synchronization(){
    	//$modules = Yii::app()->moduleManager->getModules();
	    $templates = MailTemplate::model()->findAll();
	    foreach ($templates as $template){
	    	$templates[$template->module][$template->code] = true;
	    }

	    foreach (Yii::app()->getModules() as $key => $value) {
		    $key = strtolower($key);
		    $module = Yii::app()->getModule($key);
		    if(!$module instanceof \webforma\components\WebModule){
		    	continue;
		    }

		    $messagesComponent = $module->getMailMessages();
		    if($messagesComponent){
			    $moduleTemplatesComponent = Yii::app()->getComponent($messagesComponent);
			    $moduleTemplates = $moduleTemplatesComponent->getDefaultMessages();
			    if(is_array($moduleTemplates)){
				    foreach ($moduleTemplates as $code => $data){
					    if(!isset($templates[$key][$code])){
						    $newTemplate = new MailTemplate();
						    $newTemplate->setAttributes([
							    'module' => $key,
							    'name' => $data['name'],
							    'code' => $code,
							    'theme' => $data['subject'],
							    'body' => $data['body'],
							    'description' => $data['description'],
								'icon' => $moduleTemplatesComponent->icon,
						    ]);
						    $newTemplate->save();
					    }
				    }
			    }
		    }

	    }
    }
}
