<?php

/**
 *
 * Mail install migration
 * Класс миграций для модуля Mail
 **/
class m000000_000000_mail_template_base extends webforma\components\DbMigration
{

    public function safeUp()
    {
        /**
         * mail_template:
         **/
        $this->createTable(
            '{{mail_template}}',
            [
                'id'          => 'pk',
                'code'        => 'varchar(150) NOT NULL',
                'name'        => 'varchar(150) NOT NULL',
                'module'        => 'varchar(150) NOT NULL',
                'description' => 'text',
                'theme'       => 'text NOT NULL',
                'body'        => 'text NOT NULL',
                'update_at'   => 'datetime',
            ],
            $this->getOptions()
        );

        //ix
        $this->createIndex("ux_{{mail_template}}_code", '{{mail_template}}', "code", true);
    }

    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{mail_template}}');
    }
}
