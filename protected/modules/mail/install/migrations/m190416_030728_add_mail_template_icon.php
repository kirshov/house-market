<?php

class m190416_030728_add_mail_template_icon extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{mail_template}}', 'icon', 'varchar(255) default null');
	}

	public function safeDown()
	{

	}
}