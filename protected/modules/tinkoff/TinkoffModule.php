<?php

use webforma\components\WebModule;

class TinkoffModule extends WebModule
{
    /**
     *
     */
    public $adminMenuOrder = 80;

    public function getDependencies()
    {
        return ['payment'];
    }

    public function getNavigation()
    {
        return false;
    }

    public function getAdminPageLink()
    {
        return false;
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getEditableParamsGroups()
    {
        return [];
    }

    public function getIsShowInAdminMenu()
    {
        return false;
    }

    public function getCategory()
    {
        return Yii::t('TinkoffModule.tinkoff', 'Store');
    }

    public function getName()
    {
        return Yii::t('TinkoffModule.tinkoff', 'Tinkoff');
    }

    public function getDescription()
    {
        return Yii::t('TinkoffModule.tinkoff', 'Tinkoff payment module');
    }

    public function getIcon()
    {
        return 'fa fa-rub';
    }

    public function init()
    {
        parent::init();
    }
}
