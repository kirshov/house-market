<?php
return [
    'Pay' => 'Оплатить',
    'Payment order #{id} on "{site}" website' => 'Оплата заказа №{id} на сайте "{site}"',
    'Tinkoff payment module' => 'Модуль для приема оплаты через банк Тиньков',
    'Tinkoff' => 'Тиньков',
    'Store' => 'Магазин',
];
