<?php
/**
 * @var array $settings
 * @var Order $order
 * @var Payment $payment
 */

$paymentDetail = json_decode($order->payment_details);
if($paymentDetail->paymentURL){
	echo CHtml::form($paymentDetail->paymentURL, 'get');
	echo CHtml::endForm();
} else {
	if($settings['api_url'] && $settings['login'] && $settings['password']){
		echo CHtml::form('', 'post', ['class' => 'ajax']);
		echo CHtml::hiddenField('payment-method', 'tinkoff');
		echo CHtml::hiddenField('payment-id', $payment->id);
		echo CHtml::endForm();
	}
}
