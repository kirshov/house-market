<?php

return [
    'module' => [
        'class' => 'application.modules.tinkoff.TinkoffModule',
    ],
    'component' => [
        'paymentManager' => [
            'paymentSystems' => [
                'tinkoff' => [
                    'class' => 'application.modules.tinkoff.components.payments.TinkoffPaymentSystem',
                ]
            ],
        ],
    ],
    'params'    => [
        'widgets'   => [
            'tinkoff' => 'application.modules.tinkoff.widgets.TinkoffWidget',
        ],
    ],
];
