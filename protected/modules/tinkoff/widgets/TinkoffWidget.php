<?php
/**
 * Class TinkoffWidget
 *
 * @property string $view Widget view file
 */
class TinkoffWidget extends \webforma\widgets\WWidget
{
    /**
     * @var string
     */
    public $view = 'index';

    /**
     * @var Order
     */
    public $order;

	/**
     * @return bool
     * @throws CException
     */
    public function run()
    {
        $this->render($this->view, [
            'order' => $this->order,
        ]);
    }
}