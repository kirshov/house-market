<?php

/**
 * Class TinkoffPaymentSystem
 * @link http://www.tinkoff.ru/ru/Doc/Ru/Interface.aspx
 */

Yii::import('application.modules.tinkoff.TinkoffModule');
Yii::import('application.modules.tinkoff.components.TinkoffMerchantAPI');

class TinkoffPaymentSystem extends PaymentSystem
{
	public $errorCodes = [
		99 => 'Воспользуйтесь другой картой, банк, выпустивший карту, отклонил операцию',
		102 => 'Операция отклонена фрод-мониторингом',
		101 => 'Не пройдена идентификация 3DS',
		1006 => 'Проверьте реквизиты или воспользуйтесь другой картой',
		1012 => 'Воспользуйтесь другой картой',
		1013 => 'Повторите попытку позже',
		1014 => 'Неверно введены реквизиты карты. Проверьте корректность введенных данных',
		1030 => 'Повторите попытку позже',
		1033 => 'Проверьте реквизиты или воспользуйтесь другой картой',
		1034 => 'Воспользуйтесь другой картой, банк, выпустивший карту, отклонил операцию',
		1041 => 'Воспользуйтесь другой картой, банк, выпустивший карту, отклонил операцию',
		1043 => 'Воспользуйтесь другой картой, банк, выпустивший карту, отклонил операцию',
		1051 => 'Недостаточно средств на карте',
		1054 => 'Проверьте реквизиты или воспользуйтесь другой картой',
		1057 => 'Воспользуйтесь другой картой, банк, выпустивший карту, отклонил операцию',
		1065 => 'Воспользуйтесь другой картой, банк, выпустивший карту, отклонил операцию',
		1082 => 'Проверьте реквизиты или воспользуйтесь другой картой',
		1089 => 'Воспользуйтесь другой картой, банк, выпустивший карту, отклонил операцию',
		1091 => 'Воспользуйтесь другой картой',
		1096 => 'Повторите попытку позже',
		9999 => 'Внутренняя ошибка системы',
	];

    public function renderCheckoutForm(Payment $payment, Order $order, $return = false)
    {
        return Yii::app()->getController()->renderPartial('application.modules.tinkoff.views.form', [
            'settings' => $payment->getPaymentSystemSettings(),
	        'order' => $order,
	        'payment' => $payment
        ], $return);
    }


    public function processCheckout(Payment $payment, CHttpRequest $request)
    {
    	$postData = json_decode(file_get_contents('php://input'), true);
	    $orderId = $postData['OrderId'];
	    $status = $postData['Status'];
	    $token = $postData['Token'];

	    $orderModel = Order::model()->findByNum($orderId);
	    if(Yii::app()->hasModule('log')){
		    Log::add([
		    	'code' => Log::CODE_PAYMENT_REQUEST,
			    'type' => 'json',
			    'model' => 'Order',
			    'model_id' => $orderModel->id,
			    'description' => json_encode($postData, JSON_UNESCAPED_UNICODE),
		    ], true);
	    }

	    if(!$orderModel){
		    Yii::app()->end();
	    }

	    $paymentDetail = json_decode($orderModel->payment_details);
	    if(!$paymentDetail){
		    Yii::app()->end();
	    }

	    $settings = $payment->getPaymentSystemSettings();

	    //$postData['TerminalKey'] = $settings['login'];
	    $postData['Password'] = $settings['password'];
		$_token = $this->_genToken($postData);
	    if($_token === $token && $status === 'CONFIRMED' && $postData['Amount'] >= $orderModel->getTotalPriceWithDelivery()){
		    $tinkoffApi = new TinkoffMerchantAPI(
			    $settings['login'],
			    $settings['password'],
			    $settings['api_url']
		    );

		    $orderModel->pay($payment);

		    /*$tinkoffApi->getState([
			    'PaymentId' => $paymentDetail->paymentId,
			    'IP' => $request->getUserHostAddress(),
		    ]);

		    if($tinkoffApi->status === 'CONFIRMED'){
				$orderModel->pay($payment);
		    }*/
	    }


	    echo 'OK';
	    Yii::app()->end();
    }


	/**
	 * @param integer $id
	 * @return string
	 */
    public function getError($id){
		return $this->errorCodes[$id];
    }

	public function initPayment(Payment $payment, Order $order)
	{
		parent::initPayment($payment, $order);

		$settings = $payment->getPaymentSystemSettings();

		$description = Yii::t('TinkoffModule.tinkoff', 'Payment order #{id} on "{site}" website', [
			'{id}' => $order->getNumberForUser(),
			'{site}' => Yii::app()->getModule('webforma')->getShortSiteName()
		]);

		$data = [
			'Amount' => $order->getTotalPriceWithDelivery() * 100,
			'OrderId' => $order->getNumberForUser(),
			'IP' => Yii::app()->getRequest()->getUserHostAddress(),
			'Description' => mb_substr($description, 0, 250),
			'SuccessURL' => Yii::app()->createAbsoluteUrl('/order/order/view', ['url' => $order->url, 'result' => 'success']),
			'FailURL' => Yii::app()->createAbsoluteUrl('/order/order/view', ['url' => $order->url, 'result' => 'fail']),
		];
		if($order->user_id > 0){
			$data['CustomerKey'] = $order->user_id;
		}

		$data['DATA'] = [
			'Email' => $order->email,
			'Phone' => $order->phone,
		];


		if($settings['enabledTaxation']){
			$receiptItems = [];
			foreach ((array)$order->products as $product){
				$receiptItems[] = [
					'Name' => mb_substr($product->product_name . ' ('. $product->sku.')', 0, 128),
					'Price' => $product->price * 100,
					'Quantity' => $product->quantity,
					'Amount' => $product->price * $product->quantity * 100,
					'Tax' => $settings['nds'],
				];
			}

			if($order->delivery_price > 0){
				$deliveryName = $order->delivery ? $order->delivery->name : 'Доставка';
				$receiptItems[] = [
					'Name' => mb_substr($deliveryName, 0, 128),
					'Price' => $order->delivery_price * 100,
					'Quantity' => 1,
					'Amount' => $order->delivery_price * 100,
					'Tax' => $settings['nds'],
				];
			}

			if($receiptItems){
				$data['Receipt'] = [
					'Items' => $receiptItems,
					'Email' => $order->email,
					'Phone' => $order->phone,
					'Taxation' => $settings['taxations'],
				];
			}
		}

		$tinkoffApi = new TinkoffMerchantAPI(
			$settings['login'],
			$settings['password'],
			$settings['api_url']
		);

		$tinkoffApi->init($data);
		return [
			'Success' => $tinkoffApi->Success,
			'paymentURL' => $tinkoffApi->paymentURL,
			'errorMessage' => $tinkoffApi->errorMessage,
			'paymentId' => $tinkoffApi->paymentId,
			'errorCode' => $tinkoffApi->errorCode,
			'error' => $tinkoffApi->error,
		];
	}

	private function _genToken($args)
	{
		ksort($args);
		if(isset($args['Success'])){
			$args['Success'] = $args['Success'] ? 'true' : 'false';
		}
		unset($args['Token'], $args['DATA'], $args['Receipt']);
		$string = implode('', array_values($args));
		return hash('sha256', $string);
	}
}
