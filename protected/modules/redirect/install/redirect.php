<?php
return [
    'module' => [
        'class' => 'application.modules.redirect.RedirectModule',
    ],
    'import' => [
        'application.modules.redirect.RedirectModule',
    ],
    'component' => [
        'redirectManager' => [
            'class' => 'application.modules.redirect.components.RedirectManager',
        ],
    ],
];
