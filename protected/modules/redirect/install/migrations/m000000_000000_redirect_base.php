<?php

class m000000_000000_redirect_base extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->createTable('{{redirect}}', [
            'id' =>'pk',
            'from' => 'string',
            'to' => 'string',
            'position' => 'int(11)',
            'status' => 'integer DEFAULT 1'
        ]);
	}

	public function safeDown()
	{
        $this->dropTable('{{redirect}}');
	}
}