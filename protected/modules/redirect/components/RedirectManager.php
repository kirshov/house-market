<?php
class RedirectManager extends CApplicationComponent
{
	public function run(){
		if(IS_BACKEND === false){
			$cacheName = 'redirects';
			$redirects = Yii::app()->getCache()->get($cacheName);

			if (!is_array($redirects)) {
				/* @var $cmd CDbCommand */
				$cmd = Yii::app()->getDb()->createCommand();

				$redirects = $cmd
					->setFetchMode(PDO::FETCH_KEY_PAIR)
					->select('from, to')
					->from('{{redirect}}')
					->order('position')
					->where('status = 1')
					->queryAll();

				Yii::app()->getCache()->set($cacheName, $redirects);
			}

			if(!$redirects){
				return false;
			}

			$currentUrl = Yii::app()->getRequest()->getUrl();
			if(isset($redirects[$currentUrl])){
				Yii::app()->getRequest()->redirect($redirects[$currentUrl], true, 301);
			}

			list($currentUrl) = explode('?', $currentUrl);

			foreach ($redirects as $from => $to){
				if(mb_substr($from, -1) == '*'){
					$from = rtrim($from, '*');

					if(mb_substr($to, -1) == '*'){
						$to = rtrim($to, '*');
						$temp = str_replace($from, '', $currentUrl);
						$to .= $temp;
					}

					if(mb_strpos($currentUrl, $from) === 0){
						Yii::app()->getRequest()->redirect($to, true, 301);
					}
				} else {
					if($currentUrl == $from){
						$to = rtrim($to, '*');
						Yii::app()->getRequest()->redirect($to, true, 301);
					}
				}
			}
		}
	}
}