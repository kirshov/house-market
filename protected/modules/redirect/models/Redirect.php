<?php

/**
 * Class Redirect
 *
 * @property integer $id
 * @property string $from
 * @property string $to
 * @property string $position
 * @property integer $status
 */
class Redirect extends \webforma\models\WModel
{
    /**
     *
     */
    const STATUS_PUBLISHED = 1;
    /**
     *
     */
    const STATUS_NOT_PUBLISHED = 0;

    /**
     * @return string
     */
    public function tableName()
    {
        return '{{redirect}}';
    }

    /**
     * @param null|string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['from, to', 'required'],
            ['from', 'unique'],
            ['status, position', 'numerical', 'integerOnly' => true],
            ['id, from, to', 'safe', 'on' => 'search'],
        ];
    }

    public function behaviors()
    {
        return [
            'sortable' => [
                'class' => 'webforma\components\behaviors\SortableBehavior',
            ],
        ];
    }


    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'from' => Yii::t('RedirectModule.redirect', 'From'),
            'to' => Yii::t('RedirectModule.redirect', 'To'),
            'status' => Yii::t('RedirectModule.redirect', 'Status'),
        ];
    }

    /**
     * @return CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria();
        $criteria->compare('id', $this->id);
        $criteria->compare('`t`.`from`', $this->from, true);
        $criteria->compare('`t`.`to`', $this->to, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider(
            $this, [
                'criteria' => $criteria,
                'sort' => ['defaultOrder' => 'position'],
            ]
        );
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_PUBLISHED => 'Доступен',
            self::STATUS_NOT_PUBLISHED => 'Не доступен',
        ];
    }

    /**
     * @return array
     */
    public function getStatusLabelList()
    {
        return [
	        self::STATUS_PUBLISHED => ['class' => 'label-success'],
	        self::STATUS_NOT_PUBLISHED => ['class' => 'label-danger'],
        ];
    }

	protected function beforeSave()
	{
		$this->from = '/'.ltrim($this->from, '/');
		$this->to = '/'.ltrim($this->to, '/');
		return parent::beforeSave();
	}


	protected function afterSave()
    {
        Yii::app()->cache->delete('redirects');
        parent::afterSave();
    }
}