<?php
$this->breadcrumbs = [
	Yii::t('RedirectModule.redirect', 'Redirects') => ['/redirect/redirectBackend/index'],
    Yii::t('RedirectModule.redirect', 'Create'),
];

$this->pageTitle = Yii::t('RedirectModule.redirect', 'Redirect - create');

$this->menu = [
    [
        'icon'  => 'fa fa-fw fa-tasks',
        'label' => Yii::t('RedirectModule.redirect', 'Redirect manage'),
        'url'   => ['/redirect/redirectBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('RedirectModule.redirect', 'Create redirect'),
        'url'   => ['/redirect/redirectBackend/create']
    ],
];
?>
<?php echo $this->renderPartial('_form', [
	'model' => $model,
]
); ?>
