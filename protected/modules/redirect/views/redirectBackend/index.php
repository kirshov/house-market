<?php
$this->breadcrumbs = [
    Yii::t('RedirectModule.redirect', 'Redirect') => ['/redirect/redirectBackend/index'],
];

$this->pageTitle = Yii::t('RedirectModule.redirect', 'Redirect manage');

$this->menu = [
    [
        'label' => Yii::t('RedirectModule.redirect', 'Redirect'),
        'items' => [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('RedirectModule.redirect', 'Redirect manage'),
                'url' => ['/redirect/redirectBackend/index']
            ],
			[
				'icon'  => 'fa fa-fw fa-plus-square',
				'label' => Yii::t('RedirectModule.redirect', 'Create redirect'),
				'url'   => ['/redirect/redirectBackend/create']
			],
        ]
    ]
];
?>
<?php
$this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'redirect-grid',
        'type' => 'condensed',
	    'sortableRows' => true,
	    'sortableAjaxSave' => true,
	    'sortableAttribute' => 'position',
	    'sortableAction' => '/redirect/redirectBackend/sortable',
        'dataProvider' => $model->search(),
        'filter' => $model,
        /*'actionsButtons' => false,*/
        'columns' => [
            [
	            'class' => 'webforma\widgets\WEditableColumn',
	            'name' => 'from',
	            'editable' => [
		            'url' => $this->createUrl('/redirect/redirectBackend/inline'),
		            'mode' => 'popup',
		            'params' => [
			            Yii::app()->getRequest()->csrfTokenName => Yii::app()->request->csrfToken,
		            ],
	            ],
	            'filter' => CHtml::activeTextField($model, 'from', ['class' => 'form-control']),
			],
           	[
	            'class' => 'webforma\widgets\WEditableColumn',
	            'name' => 'to',
	            'editable' => [
		            'url' => $this->createUrl('/redirect/redirectBackend/inline'),
		            'mode' => 'popup',
		            'params' => [
			            Yii::app()->getRequest()->csrfTokenName => Yii::app()->request->csrfToken,
		            ],
	            ],
	            'filter' => CHtml::activeTextField($model, 'to', ['class' => 'form-control']),
			],
            [
                'class' => 'webforma\widgets\EditableStatusColumn',
                'name' => 'status',
                'type' => 'raw',
                'url' => $this->createUrl('/redirect/redirectBackend/inline'),
                'source' => Redirect::model()->getStatusList(),
                'options' => Redirect::model()->getStatusLabelList(),
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
                'template' => '{update}{delete}',
            ],
        ],
    ]
); ?>
