<?php
/**
 * @var FieldsBackendController $this
 * @var \webforma\widgets\ActiveForm $form
 * @var Fields $model
 */

$form = $this->beginWidget(
    '\webforma\widgets\ActiveForm',
    [
        'id'                     => 'redirect-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => 'vertical',
		'htmlOptions' => ['class' => 'well sticky'],
		'clientOptions' => [
			'validateOnSubmit' => true,
		],
    ]
); ?>
<div class="alert alert-info">
    <?php echo Yii::t('RedirectModule.redirect', 'Fields with'); ?>
    <span class="required">*</span>
    <?php echo Yii::t('RedirectModule.redirect', 'are required'); ?>
</div>

<?php echo $form->errorSummary($model); ?>
<div class="row">
	<div class="col-sm-6">
		<?php echo $form->textFieldGroup($model, 'from'); ?>
		<span class="help-block">Поставьте знак <b>*</b> в конце, если ссылка должна совпадать только с началом адреса</span>
	</div>

	<div class="col-sm-6">
		<?php echo $form->textFieldGroup($model, 'to'); ?>
		<span class="help-block">Поставьте знак <b>*</b> в конце, чтобы использовать часть адреса в ссылке 'откуда'</span>
	</div>
</div>

<div class="row">
    <div class='col-sm-3'>
        <?php echo $form->dropDownListGroup($model, 'status', [
			'widgetOptions' => [
				'data' => $model->getStatusList(),
			],
		]); ?>
    </div>
</div>

<div class="buttons">
	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context'    => 'primary',
		'label'      => $model->isNewRecord
			? Yii::t('RedirectModule.redirect', 'Create redirect and continue')
			: Yii::t('RedirectModule.redirect', 'Save redirect and continue'),
	]); ?>

	<?php $this->widget('bootstrap.widgets.TbButton',[
		'buttonType'  => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label'       => $model->isNewRecord
			? Yii::t('RedirectModule.redirect', 'Create redirect and close')
			: Yii::t('RedirectModule.redirect', 'Save redirect and close'),
	]); ?>
</div>
<?php $this->endWidget(); ?>
