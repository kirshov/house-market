<?php

use webforma\components\WebModule;

/**
 * Class RedirectModule
 */
class RedirectModule extends WebModule
{
	/**
	 * @var int
	 */
	public $adminMenuOrder = 50;

    /**
     * @return array
     */
    public function getEditableParams()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getEditableParamsGroups()
    {
        return [];
    }


    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('RedirectModule.redirect', 'Services');
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/redirect/redirectBackend/index';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('RedirectModule.redirect', 'Redirect');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('RedirectModule.redirect', 'Redirect management module');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-exchange';
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport([
            'redirect.models.*',
        ]);
    }
}