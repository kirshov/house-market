<?php

/**
 * Class RedirectBackendController
 */
class RedirectBackendController extends \webforma\components\controllers\BackController
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            'inline' => [
                'class' => 'webforma\components\actions\WInLineEditAction',
                'model' => 'Redirect',
                'validAttributes' => [
                    'status', 'from', 'to'
                ]
            ],
            'sortable' => [
                'class' => 'webforma\components\actions\SortAction',
                'model' => 'Redirect',
            ],
        ];
    }

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin'],],
            ['allow', 'actions' => ['index'], 'roles' => ['Redirect.RedirectBackend.Index'],],
            ['allow', 'actions' => ['update', 'inline'], 'roles' => ['Redirect.RedirectBackend.Update'],],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Redirect.RedirectBackend.Delete'],],
            ['deny',],
        ];
    }

    /**
     *
     */
    public function actionIndex()
    {
        $model = new Redirect('search');

        $model->unsetAttributes();

        if ($data = Yii::app()->getRequest()->getQuery('Redirect')) {
            $model->setAttributes($data);
        }

        $this->render('index', ['model' => $model]);
    }

	/**
	 * @return void
	 */
	public function actionCreate()
	{
		$model = new Redirect();
		if (($data = Yii::app()->getRequest()->getPost('Redirect')) !== null) {
			$model->setAttributes($data);
			if ($model->save()) {
				Yii::app()->user->setFlash(
					webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
					Yii::t('RedirectModule.redirect', 'Record was created!')
				);

				$this->redirect(
					(array) Yii::app()->getRequest()->getPost(
						'submit-type',
						['create']
					)
				);
			}
		}

		$this->render('create', [
				'model' => $model,
			]
		);
	}

	/**
	 * @return void
	 */
	public function actionUpdate($id)
	{
		// Указан ID новости страницы, редактируем только ее
		$model = $this->loadModel($id);

		if (($data = Yii::app()->getRequest()->getPost('Redirect')) !== null) {
			$model->setAttributes(Yii::app()->getRequest()->getPost('Redirect'));

			if ($model->save()) {
				Yii::app()->user->setFlash(
					webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
					Yii::t('RedirectModule.redirect', 'Redirect was changed!')
				);

				$this->redirect(
					(array)Yii::app()->getRequest()->getPost(
						'submit-type',
						[
							'update',
							'id' => $model->id,
						]
					)
				);
			}
		}

		$this->render(
			'update', [
				'model' => $model,
			]
		);
	}

    /**
     * @param $id
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            $this->loadModel($id)->delete();

            Yii::app()->getUser()->setFlash(
                webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                Yii::t('RedirectModule.redirect', 'Record removed!')
            );

            if (!isset($_GET['ajax'])) {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : ['index']);
            }
        } else {
            throw new CHttpException(400, Yii::t(
                'RedirectModule.redirect',
                'Unknown request. Don\'t repeat it please!'
            ));
        }
    }

    /**
     * @param $id
     * @return Redirect
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Redirect::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('RedirectModule.redirect', 'Page not found!'));
        }

        return $model;
    }
}