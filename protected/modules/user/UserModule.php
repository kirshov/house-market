<?php

/**
 * UserModule основной класс модуля user
 */

use webforma\components\WebModule;

/**
 * Class UserModule
 */
class UserModule extends WebModule
{
	/**
	 * @var int
	 */
	public $adminMenuOrder = 10;

    /**
     * @var string
     */
    public $accountActivationSuccess = '/user/account/login';
    /**
     * @var string
     */
    public $accountActivationFailure = '/user/account/registration';
    /**
     * @var string
     */
    public $loginSuccess = '/';
    /**
     * @var string
     */
    public $registrationSuccess = '/user/account/login';
    /**
     * @var string
     */
    public $loginAdminSuccess = '/webforma/backend/index';
    /**
     * @var string
     */
    public $logoutSuccess = '/';
    /**
     * @var int
     */
    public $sessionLifeTime = 7;

    /**
     * @var
     */
    public $notifyEmailFrom;
    /**
     * @var int
     */
    public $autoRecoveryPassword = 0;
    /**
     * @var int
     */
    public $recoveryDisabled = 0;
    /**
     * @var int
     */
    public $registrationDisabled = 1;
    /**
     * @var int
     */
    public $notifyAdmin = 0;
    /**
     * @var int
     */
    public $minPasswordLength = 8;
    /**
     * @var int
     */
    public $emailAccountVerification = 1;
    /**
     * @var int
     */
    public $showCaptcha = 0;
    /**
     * @var int
     */
    public $minCaptchaLength = 3;
    /**
     * @var int
     */
    public $maxCaptchaLength = 6;
    /**
     * @var
     */
    public $documentRoot;
    /**
     * @var string
     */
    public $avatarsDir = 'avatars';
    /**
     * @var int
     */
    public $avatarMaxSize = 5242880; // 5 MB
    /**
     * @var string
     */
    public $defaultAvatarPath = 'images/avatar.png';
    /**
     * @var string
     */
    public $avatarExtensions = 'jpg,png,gif,jpeg';
    /**
     * @var int
     */
    public $usersPerPage = 20;
    /**
     * @var int
     */
    public $badLoginCount = 3;
    /**
     * @var int
     */
    public $generateNickName = 1;

    /**
     * @var string
     */
    public static $logCategory = 'application.modules.user';
    /**
     * @var array
     */
    public $profiles = [];

    /**
     * @var
     */
    private $defaultAvatar;

    /**
     * @return string
     */
    public function getUploadPath()
    {
        return Yii::getPathOfAlias('webroot') . '/' .
        Yii::app()->getModule('webforma')->uploadPath . '/' .
        $this->avatarsDir . '/';
    }

    /**
     * @return bool
     */
    public function getInstall()
    {
        if (parent::getInstall()) {
            @mkdir($this->getUploadPath(), 0755);
        }

        return false;
    }

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [
            'accountActivationSuccess' => Yii::t('UserModule.user', 'Page after account activation'),
            'accountActivationFailure' => Yii::t('UserModule.user', 'Page after activation error'),
            'loginSuccess' => Yii::t('UserModule.user', 'Page after authorization'),
            'logoutSuccess' => Yii::t('UserModule.user', 'Page after logout'),
            'autoRecoveryPassword' => Yii::t('UserModule.user', 'Automatic password recovery'),
            'recoveryDisabled' => Yii::t('UserModule.user', 'Disable password recovery'),
            'registrationDisabled' => Yii::t('UserModule.user', 'Disable registration'),
            'notifyAdmin' => 'Уведомлять администратора о новой регистрации',
            'minPasswordLength' => Yii::t('UserModule.user', 'Minimum password length'),
            'emailAccountVerification' => Yii::t('UserModule.user', 'Confirm account by Email'),
            'showCaptcha' => Yii::t('UserModule.user', 'Show captcha on registration'),
            'minCaptchaLength' => Yii::t('UserModule.user', 'Minimum captcha length'),
            'maxCaptchaLength' => Yii::t('UserModule.user', 'Maximum captcha length'),
            'documentRoot' => Yii::t('UserModule.user', 'Server root'),
            'avatarExtensions' => Yii::t('UserModule.user', 'Avatar extensions'),
            'avatarsDir' => Yii::t('UserModule.user', 'Directory for avatar uploading'),
            'avatarMaxSize' => Yii::t('UserModule.user', 'Maximum avatar size'),
            'defaultAvatarPath' => Yii::t('UserModule.user', 'Empty avatar'),
            'loginAdminSuccess' => Yii::t('UserModule.user', 'Page after admin authorization'),
            'registrationSuccess' => Yii::t('UserModule.user', 'Page after success register'),
            'sessionLifeTime' => Yii::t(
                'UserModule.user',
                'Session lifetime (in days) when "Remember me" options enabled'
            ),
            'usersPerPage' => Yii::t('UserModule.user', 'Users per page'),
            'badLoginCount' => Yii::t('UserModule.user', 'Number of login attempts'),
            'generateNickName' => Yii::t('UserModule.user', 'Generate user name automatically')
        ];
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
		if(!Yii::app()->getModule('webforma')->allowUsers){
			return [];
		}

		return [
            'showCaptcha' => $this->getChoice(),
            'registrationDisabled' => $this->getChoice(),
            'notifyAdmin' => $this->getChoice(),
            'emailAccountVerification' => $this->getChoice(),
        ];
    }

    /**
     * @return array
     */
    public function getEditableParamsGroups()
    {
        return [
            'security' => [
                'label' => Yii::t('UserModule.user', 'Security settings'),
                'items' => [
                    'registrationDisabled',
                    'notifyAdmin',
                    'emailAccountVerification',
	                'showCaptcha',
                ]
            ],
        ];
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/user/userBackend/index';
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('UserModule.user', 'Manage users'),
                'url' => ['/user/userBackend/index']
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('UserModule.user', 'Create user'),
                'url' => ['/user/userBackend/create']
            ],
            /*['label' => Yii::t('UserModule.user', 'Tokens')],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('UserModule.user', 'Token list'),
                'url' => ['/user/tokensBackend/index']
            ],*/
        ];
    }

    /**
     * @return bool
     */
    public function getIsInstallDefault()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function getIsNoDisable()
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('UserModule.user', 'Users');
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('UserModule.user', 'Users');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('UserModule.user', 'Module for user registration and authorization management');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-user';
    }

    /**
     * @return array
     */
    public function getConditions()
    {
        return [
            'isAuthenticated' => [
                'name' => Yii::t('UserModule.user', 'Authorized'),
                'condition' => Yii::app()->getUser()->isAuthenticated(),
            ],
            'isSuperUser' => [
                'name' => Yii::t('UserModule.user', 'Administrator'),
                'condition' => Yii::app()->getUser()->isSuperUser(),
            ],
        ];
    }

    /**
     *
     */
    public function init()
    {
        $this->setImport(
            [
                'user.models.*',
                'user.events.*',
                'user.listeners.*',
                'user.components.*',
                'user.widgets.AvatarWidget',
                'webforma.WebformaModule'
            ]
        );

        parent::init();
    }

    /**
     * Возвращает аватар по умолчанию из текущей темы (<theme_name>/web/images/avatar.png)
     * @since 0.8
     * @return string
     */
    public function getDefaultAvatar()
    {
        if (null === $this->defaultAvatar) {
            $this->defaultAvatar = Yii::app()->getTheme()->getAssetsUrl() . '/' . $this->defaultAvatarPath;
        }

        return $this->defaultAvatar;
    }
}
