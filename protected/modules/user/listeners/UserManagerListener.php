<?php

Yii::import('application.modules.notify.models.NotifySettings');

/**
 * Class UserManagerListener
 */
class UserManagerListener
{
    /**
     * @param $model
     */
    public static function onUserRegistrationAdmin(UserRegistrationEvent $event)
    {
	    Yii::app()->getComponent('userNotifyService')->sendAdminRegistrationNotify($event->getUser());
    }

    /**
     * @param UserRegistrationEvent $event
     */
    public static function onUserRegistration(UserRegistrationEvent $event)
    {
		Yii::app()->getComponent('userNotifyService')->sendSuccessUserRegistration($event->getUser());
    }

    /**
     * @param UserRegistrationEvent $event
     */
    public static function onUserRegistrationNeedActivation(UserRegistrationEvent $event)
    {
		Yii::app()->getComponent('userNotifyService')->userAccountNeedActivation($event);
    }

    /**
     * @param UserPasswordRecoveryEvent $event
     */
    public static function onPasswordRecovery(UserPasswordRecoveryEvent $event)
    {
	    Yii::app()->getComponent('userNotifyService')->sendUserPasswordReset($event);
    }

    /**
     * @param UserActivatePasswordEvent $event
     */
    public static function onSuccessActivatePassword(UserActivatePasswordEvent $event)
    {
        if (true === $event->getNotify()) {
	        Yii::app()->getComponent('userNotifyService')->sendUserSuccessResetPassword($event->getUser());
        }
    }

	/**
	 * @param UserActivateEvent $event
	 */
    public static function onSuccessActivateAccount(UserActivateEvent $event)
    {
	    Yii::app()->getComponent('userNotifyService')->sendUserSuccessActivate($event->getUser());
    }

    /**
     * @param UserEmailConfirmEvent $event
     */
    public static function onSuccessEmailChange(UserEmailConfirmEvent $event)
    {
		Yii::app()->getComponent('userNotifyService')->sendUserConfirmNewEmail($event);
    }

    /**
     * @param UserEmailConfirmEvent $event
     */
    public static function onSuccessEmailConfirm(UserEmailConfirmEvent $event)
    {
	    Yii::app()->getComponent('userNotifyService')->sendSuccessUserConfirmNewEmail($event->getUser());
    }
}
