<?php

/**
 * Контроллер, отвечающий за просмотр профиля, смену почты и пароля.
 * То есть за действия авторизованного пользователя
 **/
class ProfileController extends \webforma\components\controllers\FrontController
{
    /**
     * @var null
     */
    public $user = null;

    /**
     * @return array
     */
    public function filters()
    {
        return [
            ['webforma\filters\WFrontAccessControl'],
        ];
    }

    /**
     * @param $action
     * @return bool
     */
    public function beforeAction($action)
    {
    	if(!Yii::app()->getModule('webforma')->allowUsers){
			throw new Exception(Yii::t('WebformaModule.webforma', 'Page was not found!'));
	    }
        $this->user = Yii::app()->getUser()->getProfile();

        if ($this->user === null) {

            Yii::app()->getUser()->setFlash(
                webforma\widgets\WFlashMessages::ERROR_MESSAGE,
                Yii::t('UserModule.user', 'User not found.')
            );

            Yii::app()->getUser()->logout();

            $this->redirect(
                ['/user/account/login']
            );
        }

        return true;
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'profile' => [
                'class' => 'application.modules.user.controllers.profile.ProfileAction'
            ],
            'password' => [
                'class' => 'application.modules.user.controllers.profile.PasswordAction'
            ],
            'email' => [
                'class' => 'application.modules.user.controllers.profile.EmailAction'
            ],
        ];
    }
}
