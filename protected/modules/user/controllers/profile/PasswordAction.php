<?php

/**
 * Экшн, отвечающий за смену пароля пользователя
 **/
class PasswordAction extends CAction
{
    public function run()
    {
        $user = $this->controller->user;
        $form = new ProfilePasswordForm();

        if (($data = Yii::app()->getRequest()->getPost('ProfilePasswordForm')) !== null) {
            $form->setAttributes($data);
            if ($form->validate()) {
                $user->hash = Yii::app()->userManager->hasher->hashPassword($form->password);
                if ($user->save()) {
                    Yii::app()->getUser()->setFlash(
                        webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                        Yii::t('UserModule.user', 'Your password was changed successfully.')
                    );
                    $this->getController()->redirect(['/user/profile/profile']);
                }
            }
        }
        $this->getController()->render('password', ['model' => $form]);
    }
}
