<?php

/**
 * Экшн, отвечающий за смену email пользователя
 **/
class EmailAction extends CAction
{
    /**
     *
     */
    public function run()
    {
        $user = $this->getController()->user;
        $form = new ProfileEmailForm();
        $module = Yii::app()->getModule('user');

        if (($data = Yii::app()->getRequest()->getPost('ProfileEmailForm')) !== null) {
            $form->setAttributes($data);
            if ($form->validate()) {
                $oldEmail = $user->email;

                // Если включена верификация при смене почты:
                if (/*$module->emailAccountVerification && */($oldEmail != $form->email)) {
                    // Вернуть старый email на время проверки
                    $user->email = $oldEmail;
                    if (Yii::app()->userManager->changeUserEmail($user, $form->email)) {
                        Yii::app()->getUser()->setFlash(
                            webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                            Yii::t(
                                'UserModule.user',
                                'You need to confirm your e-mail. Please check the mail!'
                            )
                        );
                    }
                } else {
                    $user->email = $form->email;
                    $user->save();
                    Yii::app()->getUser()->setFlash(
                        webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                        Yii::t('UserModule.user', 'Email was updated.')
                    );
                }
                $this->getController()->redirect(['/user/profile/profile']);
            }
        }
        $this->getController()->render('email', ['model' => $form,]);
    }
}
