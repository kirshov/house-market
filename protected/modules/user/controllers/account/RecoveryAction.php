<?php
/**
 * Экшн, отвечающий за запрос восстановления пароля пользователя
 **/

use webforma\widgets\WFlashMessages;

/**
 * Class RecoveryAction
 */
class RecoveryAction extends CAction
{
    /**
     * @throws CHttpException
     */
    public function run()
    {
        if (false === Yii::app()->getUser()->getIsGuest()) {
            $this->getController()->redirect(\webforma\helpers\Url::redirectUrl(
                Yii::app()->getModule('user')->loginSuccess
            ));
        }

        $module = Yii::app()->getModule('user');

        // Если восстановление отключено - ошбочка ;)
        if ($module->recoveryDisabled) {
            throw new CHttpException(
                404,
                Yii::t('UserModule.user', 'requested page was not found!')
            );
        }

        // Новая форма восстановления пароля:
        $form = new RecoveryForm();

        if (($data = Yii::app()->getRequest()->getPost('RecoveryForm')) !== null) {

            $form->setAttributes($data);

            if ($form->validate()) {

                if (Yii::app()->userManager->passwordRecovery($form->email)) {

                    Yii::app()->getUser()->setFlash(
                        WFlashMessages::SUCCESS_MESSAGE,
                        Yii::t(
                            'UserModule.user',
                            'Letter with password recovery instructions was sent on email which you choose during register'
                        )
                    );

                    $this->getController()->redirect(['/user/account/login']);
                }

                Yii::app()->getUser()->setFlash(
                    WFlashMessages::ERROR_MESSAGE,
                    Yii::t('UserModule.user', 'Password recovery error.')
                );

                $this->getController()->redirect(['/user/account/recovery']);
            }
        }

        $this->getController()->render('recovery', ['model' => $form]);
    }
}
