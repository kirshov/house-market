<?php

/**
 * Экшн, отвечающий за подтверждение email пользователя
 **/
class EmailConfirmAction extends CAction
{
    /**
     * @param $token
     */
    public function run($token)
    {
        // пытаемся подтвердить почту
        if (Yii::app()->userManager->verifyEmail($token)) {

            Yii::app()->getUser()->setFlash(
                webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                Yii::t('UserModule.user', 'You confirmed new e-mail successfully!')
            );
        } else {

            Yii::app()->getUser()->setFlash(
                webforma\widgets\WFlashMessages::ERROR_MESSAGE,
                Yii::t('UserModule.user', 'Activation error! Maybe e-mail already confirmed or incorrect activation code was used. Try to use another e-mail')
            );
        }

        $this->getController()->redirect(
            Yii::app()->getUser()->isAuthenticated()
                ? ['/user/profile/profile']
                : ['/user/account/login']
        );
    }
}
