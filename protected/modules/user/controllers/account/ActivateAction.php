<?php
/**
 * Экшн, отвечающий за активацию аккаунта пользователя
 *
 **/

use webforma\helpers\Url;

class ActivateAction extends CAction
{
    public function run($token)
    {
        $module = Yii::app()->getModule('user');

        // Пытаемся найти пользователя по токену,
        // в противном случае - ошибка:
        if (Yii::app()->userManager->activateUser($token)) {

            // Сообщаем пользователю:
            Yii::app()->getUser()->setFlash(webforma\widgets\WFlashMessages::SUCCESS_MESSAGE, Yii::t('UserModule.user', 'You activate account successfully. Now you can login!'));

            // Выполняем переадресацию на соответствующую страницу:
            $this->getController()->redirect(Url::redirectUrl($module->accountActivationSuccess));
        }

        // Сообщаем об ошибке:
        Yii::app()->getUser()->setFlash(
            webforma\widgets\WFlashMessages::ERROR_MESSAGE,
            Yii::t('UserModule.user', 'There was a problem with the activation of the account. Please refer to the site\'s administration.')
        );

        // Переадресовываем на соответствующую ошибку:
        $this->getController()->redirect(Url::redirectUrl($module->accountActivationFailure));
    }
}
