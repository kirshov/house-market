<?php

/**
 * Контроллер, отвечающий за регистрацию, авторизацию и т.д. действия неавторизованного пользователя
 *
 **/
class AccountController extends \webforma\components\controllers\FrontController
{
	protected function beforeAction($action)
	{
		if(!in_array($action->getId(), ['backendlogin', 'logout', 'captcha']) && !Yii::app()->getModule('webforma')->allowUsers){
			throw new Exception(Yii::t('WebformaModule.webforma', 'Page was not found!'));
		}

		return parent::beforeAction($action);
	}


	/**
     * @return array
     */
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'webforma\components\actions\WCaptchaAction',
                'backColor' => 0xFFFFFF,
                'testLimit' => 1,
                'minLength' => Yii::app()->getModule('user')->minCaptchaLength,
            ],
            'registration' => [
                'class' => 'application.modules.user.controllers.account.RegistrationAction',
            ],
            'activate' => [
                'class' => 'application.modules.user.controllers.account.ActivateAction',
            ],
            'login' => [
                'class' => 'application.modules.user.controllers.account.LoginAction',
            ],
            'backendlogin' => [
                'class' => 'application.modules.user.controllers.account.LoginAction',
            ],
            'logout' => [
                'class' => 'application.modules.user.controllers.account.LogOutAction',
            ],
            'recovery' => [
                'class' => 'application.modules.user.controllers.account.RecoveryAction',
            ],
            'restore' => [
                'class' => 'application.modules.user.controllers.account.RecoveryPasswordAction',
            ],
            'confirm' => [
                'class' => 'application.modules.user.controllers.account.EmailConfirmAction',
            ],
        ];
    }
}
