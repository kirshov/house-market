<?php
/**
 * Файл класса UserIdentity, который расширяет возможности стандартного CUserIdentity
 **/
use webforma\models\Settings;

class UserIdentity extends CUserIdentity
{
    private $_id;

    /**
     * Метод аутентификации пользователя:
     *
     * @return bool is user authenticated
     **/
    public function authenticate()
    {
        $user = User::model()->active()->find(
            [
                'condition' => 'email = :username OR nick_name = :username',
                'params' => [
                    ':username' => $this->username,
                ],
            ]
        );

        if (null === $user) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;

            return false;
        }
        if (!Yii::app()->userManager->hasher->checkPassword($this->password, $user->hash)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;

            return false;
        }

        // запись данных в сессию пользователя
        $this->_id = $user->id;
        $this->username = $user->nick_name;

        Yii::app()->getUser()->setState('id', $user->id);
        Yii::app()->getUser()->setState(YWebUser::STATE_ACCESS_LEVEL, $user->access_level);
        Yii::app()->getUser()->setState(YWebUser::STATE_NICK_NAME, $user->nick_name);

        // зафиксируем время входа
        $user->visit_time = new CDbExpression('NOW()');
        $user->update(['visit_time']);
        $this->errorCode = self::ERROR_NONE;

        return true;
    }

    /**
     * Метод получния идентификатора пльзователя:
     *
     * @return int userID
     **/
    public function getId()
    {
        return $this->_id;
    }
}
