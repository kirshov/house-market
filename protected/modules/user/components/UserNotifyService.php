<?php

class UserNotifyService extends NotifyService
{

	/**
	 * @var string
	 */
	protected $templatePath  = 'user/email';

	/**
	 * @var string
	 */
	protected $messageComponent = 'userMailMessage';

	/**
	 *
	 */
	public function init()
	{
		parent::init();
	}

	/**
	 * @param User $model
	 * @param $code
	 * @param bool $isAdmin
	 * @return bool
	 */
	public function sendMessage(User $model, $code, $isAdmin = false)
	{
		$webforma = Yii::app()->getModule('webforma');
		$from = $webforma->notifyEmailFrom;

		$toAdminEmail = ['adminRegistrationNotify'];
		if(in_array($code, $toAdminEmail)){
			$to = $webforma->notifyEmailTo;
		} else {
			$to = $model->email;
		}

		if(!$to || !$from){
			return false;
		}

		$mailContent = $this->getMailContent($model, $code, $isAdmin);

		if($mailContent['subject'] && $mailContent['body']){
			foreach (explode(',', $to) as $email) {
				$this->mail->send($from, $email, $mailContent['subject'], $mailContent['body']);
			}
		}

		return true;
	}

	/**
	 * @param User $model
	 */
	public function sendAdminRegistrationNotify(User $model){
		$this->sendMessage($model, 'adminRegistrationNotify', true);
	}

	/**
	 * @param User $model
	 */
	public function sendSuccessUserRegistration(User $model){
		$this->sendMessage($model, 'successUserRegistration');
	}

	/**
	 * @param UserRegistrationEvent $event
	 */
	public function userAccountNeedActivation(UserRegistrationEvent $event){
		Yii::app()->getComponent($this->messageComponent)->setToken($event->getToken());
		$this->sendMessage($event->getUser(), 'userAccountNeedActivation');
	}

	/**
	 * @param UserEmailConfirmEvent $event
	 */
	public function sendUserConfirmNewEmail(UserEmailConfirmEvent $event){
		Yii::app()->getComponent($this->messageComponent)->setToken($event->getToken());
		$this->sendMessage($event->getUser(), 'userConfirmNewEmail');
	}

	/**
	 * @param User $model
	 */
	public function sendSuccessUserConfirmNewEmail(User $model){
		$this->sendMessage($model, 'userSuccessConfirmNewEmail');
	}

	/**
	 * @param User $model
	 */
	public function sendUserSuccessActivate(User $model){
		$this->sendMessage($model, 'userSuccessActivate');
	}

	/**
	 * @param UserPasswordRecoveryEvent $event
	 */
	public function sendUserPasswordReset(UserPasswordRecoveryEvent $event){
		Yii::app()->getComponent($this->messageComponent)->setToken($event->getToken());
		$this->sendMessage($event->getUser(), 'userPasswordReset');
	}

	/**
	 * @param User $model
	 */
	public function sendUserSuccessResetPassword(User $model){
		$this->sendMessage($model, 'userSuccessResetPassword');
	}

}