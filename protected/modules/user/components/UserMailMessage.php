<?php
class UserMailMessage extends WMailMessage{

	/**
	 * @var string
	 */
	public $icon = 'fa-user';

	/**
	 * @var User
	 */
	protected $model;

	/**
	 * @var string
	 */
	protected $module = 'user';

	/**
	 * @var UserToken
	 */
	protected $token;

	/**
	 * @param UserToken $token
	 */
	public function setToken(UserToken $token){
		$this->token = $token;
	}

	/**
	 * @return UserToken
	 */
	public function getToken(){
		return $this->token;
	}

	/**
	 * @return array
	 */
	public function getDefaultMessages()
	{
		return [
			'adminRegistrationNotify' => [
				'name' => 'Сообщение администратору о зарегистрированном пользователе',
				'subject' => 'Регистрация нового пользователя на сайте "%siteName%"',
				'body' => '<h2 style="'.$this->getStyle('title').'">Зарегистрирован новый пользователь</h2>
					<h3 style="'.$this->getStyle('subtitle').'">на сайте "%siteName%"</h3>
					<p style="font: %font%;">
						Имя: %name%<br/>
						Email: %email%
					</p>
				',
			],
			'successUserRegistration' => [
				'name' => 'Сообщение пользователю об успешной регистрации',
				'subject' => 'Вы успешно зарегистрировались на сайте "%siteName%"',
				'body' => '<h2 style="'.$this->getStyle('title').'">Вы успешно зарегистрировались</h2>
					<h3 style="'.$this->getStyle('subtitle').'">на сайте "%siteName%"</h3>
					<br/>
					<p style="text-align: center;">%link-site%</p>
				',
			],
			'userAccountNeedActivation' => [
				'name' => 'Сообщение пользователю о необходимости подтвердить e-mail',
				'subject' => 'Подтвердите e-mail на сайте "%siteName%"',
				'body' => '<h2 style="'.$this->getStyle('title').'">Подтвердите e-mail адрес</h2>
					<h3 style="'.$this->getStyle('subtitle').'">на сайте "%siteName%"</h3>
					<p style="text-align: center; font: %font%;">
						Для завершения регистрации необходимо подтвердить свой e-mail.
					</p>
					<br/>
					<p style="text-align: center;">
						%link-activate%
					</p>
				',
			],
			'userConfirmNewEmail' => [
				'name' => 'Сообщение пользователю о необходимости подтвердить новый e-mail',
				'subject' => 'Подтвердите новый e-mail на сайте "%siteName%"',
				'body' => '<h2 style="'.$this->getStyle('title').'">Изменение e-mail адреса</h2>
					<h3 style="'.$this->getStyle('subtitle').'">на сайте "%siteName%"</h3>
					<p style="text-align: center; font: %font%;">
						Для завершения изменения необходимо подтвердить новый e-mail адрес.
					</p>
					<p style="text-align: center;">
						%link-confirm%
					</p>
				',
			],
			'userSuccessConfirmNewEmail' => [
				'name' => 'Сообщение пользователю об успешной смене e-mail адреса',
				'subject' => 'Новый e-mail адрес успешно сохранен на сайте "%siteName%"',
				'body' => '<h2 style="'.$this->getStyle('title').'">Новый e-mail адрес успешно сохранен</h2>
					<h3 style="'.$this->getStyle('subtitle').'">на сайте "%siteName%"</h3>
					<br/>				
					<p style="text-align: center;">
						%link-site%
					</p>
				',
			],
			'userSuccessActivate' => [
				'name' => 'Сообщение пользователю об активации аккаунта',
				'subject' => 'Ваш аккаунт активирован на сайте "%siteName%"',
				'body' => '<h2 style="'.$this->getStyle('title').'">Ваш аккаунт активирован</h2>
					<h3 style="'.$this->getStyle('subtitle').'">на сайте "%siteName%"</h3>
					<br/>
					<p style="text-align: center;">
						%link-site%
					</p>
				',
			],
			'userPasswordReset' => [
				'name' => 'Сообщение пользователю для сброса пароля',
				'subject' => 'Сбросить пароль на сайте "%siteName%"',
				'body' => '<h2 style="'.$this->getStyle('title').'">Сбросить пароль</h2>
					<h3 style="'.$this->getStyle('subtitle').'">на сайте "%siteName%"</h3>
					<p style="text-align: center; font: %font%;">
						Если Вы не запрашивали сброс пароля, просто удалите это письмо.
					</p>				
					<p style="text-align: center;">
						%link-pass-reset%
					</p>
				',
			],
			'userSuccessResetPassword' => [
				'name' => 'Сообщение пользователю о успешном сбросе пароля',
				'subject' => 'Ваш пароль успешно изменен на сайте "%siteName%"',
				'body' => '<h2 style="'.$this->getStyle('title').'">Ваш пароль успешно изменен</h2>
					<h3 style="'.$this->getStyle('subtitle').'">на сайте "%siteName%"</h3>
					<br/>			
					<p style="text-align: center;">
						%link-site%
					</p>
				',
			],
		];
	}

	/**
	 * @param $code
	 * @return mixed|string
	 */
	public function getContentBlock($code)
	{
		switch ($code){
			case '%name%':
				return $this->model->getFullName();
			case '%email%':
				return $this->model->email;
			case '%link-activate%':
				return '<a href="'.Yii::app()->createAbsoluteUrl('/user/account/activate',['token' => $this->getToken()->token]).'" target="_blank" style="'.$this->getStyle('btn').'">
					Завершить регистрацию &rarr;	
				</a>';
			case '%link-confirm%':
				return '<a href="'.Yii::app()->createAbsoluteUrl('/user/account/confirm',['token' => $this->getToken()->token]).'" target="_blank" style="'.$this->getStyle('btn').'">
					Подтвердить адрес &rarr;	
				</a>';
			case '%link-pass-reset%':
				return '<a href="'.Yii::app()->createAbsoluteUrl('/user/account/restore',['token' => $this->getToken()->token]).'" target="_blank" style="'.$this->getStyle('btn').'">
					Сбросить пароль &rarr;	
				</a>';
			default:
				return parent::getContentBlock($code);
		}
	}
}
