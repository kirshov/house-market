<?php
return [
    'module' => [
        'class' => 'application.modules.user.UserModule',
        /*'panelWidgets' => [
            'application.modules.user.widgets.PanelUserStatWidget' => [
                'limit' => 5,
            ],
        ],*/
        'documentRoot' => $_SERVER['DOCUMENT_ROOT'],
    ],
    'import' => [
        'application.modules.user.UserModule',
        'application.modules.user.models.*',
        'application.modules.user.forms.*',
        'application.modules.user.components.*',
    ],
    'component' => [
        // компонент Yii::app()->user, подробнее http://www.yiiframework.ru/doc/guide/ru/topics.auth
        'user' => [
            'class' => 'application.modules.user.components.YWebUser',
            'loginUrl' => ['/user/account/login'],
			'identityCookie' => [
				'httpOnly' => true,
				'domain' => defined('COOKIE_DOMAIN') ? COOKIE_DOMAIN : null,
				'path' => '/',
				'name' => '_identity',
			],
        ],
        'userManager' => [
            'class' => 'application.modules.user.components.UserManager',
            'hasher' => [
                'class' => 'application.modules.user.components.Hasher',
            ],
            'tokenStorage' => [
                'class' => 'application.modules.user.components.TokenStorage',
            ],
        ],
        'authenticationManager' => [
            'class' => 'application.modules.user.components.AuthenticationManager',
        ],
        'eventManager' => [
            'class' => 'webforma\components\EventManager',
            'events' => [
                'admin.registration' => [
                    ['UserManagerListener', 'onUserRegistrationAdmin'],
                ],
                'user.success.registration' => [
                    ['UserManagerListener', 'onUserRegistration'],
                ],
                'user.success.registration.need.activation' => [
                    ['UserManagerListener', 'onUserRegistrationNeedActivation'],
                ],
                'user.success.password.recovery' => [
                    ['UserManagerListener', 'onPasswordRecovery'],
                ],
                'user.success.activate.password' => [
                    ['UserManagerListener', 'onSuccessActivatePassword'],
                ],
                'user.success.activate.account' => [
                    ['UserManagerListener', 'onSuccessActivateAccount']
                ],
                'user.success.email.confirm' => [
                    ['UserManagerListener', 'onSuccessEmailConfirm'],
                ],
                'user.success.email.change' => [
                    ['UserManagerListener', 'onSuccessEmailChange'],
                ]
            ],
        ],
	    'userNotifyService' => [
		    'class' => 'application.modules.user.components.UserNotifyService',
	    ],
	    'userMailMessage' => [
		    'class' => 'application.modules.user.components.UserMailMessage',
	    ],
    ],
    'rules' => [
	    '/logout' => 'user/account/logout',
        '/user/account/captcha/refresh/<v>' => 'user/account/captcha/refresh',
        '/user/account/captcha/<v>' => 'user/account/captcha/',
        '/profile' => 'user/profile/profile',
	    '/change-password' => 'user/profile/password',
	    '/change-email' => 'user/profile/email',

	    '/login' => 'user/account/login',
	    '/registration' => 'user/account/registration',
	    '/recovery' => 'user/account/recovery',
	    '/confirm/<token>' => 'user/account/confirm',
	    '/recovery/<token>' => 'user/account/restore',
	    '/activate/<token>' => 'user/account/activate',
    ],
];
