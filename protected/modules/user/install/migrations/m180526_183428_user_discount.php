<?php

class m180526_183428_user_discount extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{user_user}}', 'discount', 'tinyint(3) DEFAULT 0');
	}

	public function safeDown()
	{

	}
}