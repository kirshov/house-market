<?php

class m180504_111948_add_news_subscribe extends webforma\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{user_user}}', 'news_subscribe', 'tinyint(1) DEFAULT 1');
	}

	public function safeDown()
	{

	}
}