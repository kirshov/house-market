<?php

/**
 * Добавляем параметр "телефон"
 **/
class m151006_000000_user_add_phone extends CDbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{user_user}}', 'phone', 'char(30) DEFAULT NULL');
    }

    public function safeDown()
    {
        $this->dropColumn('{{user_user}}', 'phone');
    }
}
