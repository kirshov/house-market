<?php $form = $this->beginWidget(
    '\webforma\widgets\ActiveForm',
    [
        'id' => 'user-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'type' => 'vertical',
        'htmlOptions' => ['class' => 'well sticky'],
	    'clientOptions' => [
		    'validateOnSubmit' => true,
	    ],
    ]
); ?>

<div class="alert alert-info">
    <?=  Yii::t('UserModule.user', 'Fields with'); ?>
    <span class="required">*</span>
    <?=  Yii::t('UserModule.user', 'are required'); ?>
</div>

<?=  $form->errorSummary($model); ?>

<div class="row">
    <div class="col-sm-4">
        <?=  $form->textFieldGroup($model, 'email'); ?>
    </div>
	<div class="col-sm-4">
		<?=  $form->dropDownListGroup($model, 'email_confirm', [
			'widgetOptions' => [
				'data' => $model->getEmailConfirmStatusList(),
			],
		]); ?>
    </div>
</div>

<div class="row">
	<div class="col-sm-4">
		<?=  $form->textFieldGroup($model, 'first_name'); ?>
	</div>
    <div class="col-sm-4">
        <?=  $form->textFieldGroup($model, 'last_name'); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <?=  $form->labelEx($model,'phone',['class' => 'control-label']); ?>
			<?php $this->widget('CMaskedTextField', [
				'model' => $model,
				'attribute' => 'phone',
				'mask' => Yii::app()->getModule('webforma')->phoneMask,
				'placeholder' => '*',
				'htmlOptions' => [
					'class' => 'form-control',
				],
			]); ?>
            <?=  $form->error($model,'phone'); ?>
        </div>
    </div>
	<?if(Yii::app()->getModule('store')):?>
		<div class="col-sm-4">
			<?=  $form->textFieldGroup($model, 'discount'); ?>
		</div>
	<?endif;?>
</div>

<div class="row">
    <div class="col-sm-12">
        <?=  $form->labelEx($model, 'about'); ?>
        <?php $this->widget($this->module->getVisualEditor(), [
			'model' => $model,
			'attribute' => 'about',
		]); ?>
    </div>
</div>
<br/>
<div class="row">
	<div class="col-sm-3">
		<?=  $form->dropDownListGroup($model, 'status', [
			'widgetOptions' => [
				'data' => $model->getStatusList(),
			],
		]); ?>
	</div>
</div>
<div class="row">
    <div class="col-sm-3">
        <?=  $form->dropDownListGroup($model, 'access_level', [
			'widgetOptions' => [
				'data' => $model->getAccessLevelsList(),
			],
		]); ?>
    </div>

	<?if(Yii::app()->hasModule('rbac')):?>
		<div class="col-sm-3 rbac_role<?=($model->access_level != User::ACCESS_LEVEL_EDITOR ? ' hidden' : '')?>">
			<?=  $form->dropDownListGroup($model, 'role_id', [
				'widgetOptions' => [
					'data' => ['--- нет ---'] + CHtml::listData(Roles::model()->findAll(['order' => 'name']), 'id', 'name'),
				],
			]); ?>
		</div>
	<?endif;?>
</div>

<div class="buttons">
	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context' => 'primary',
		'label' => $model->isNewRecord
			? Yii::t('UserModule.user', 'Create user and continue')
			: Yii::t('UserModule.user', 'Save user and continue'),
	]); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label' => $model->isNewRecord
			? Yii::t('UserModule.user', 'Create user and close')
			: Yii::t('UserModule.user', 'Save user and close'),
	]); ?>
</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">
	$('#User_access_level').on('change', function () {
		var adminId = <?=User::ACCESS_LEVEL_EDITOR;?>,
			roleWrap = $('.rbac_role');
		if($(this).val() == adminId){
			roleWrap.removeClass('hidden');
		} else {
			roleWrap.addClass('hidden');
		}
	})
</script>