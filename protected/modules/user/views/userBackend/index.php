<?php

/**
 * @var $model User
 * @var $this UserBackendController
 */

$this->breadcrumbs = [
    Yii::t('UserModule.user', 'Users') => ['/user/userBackend/index'],
    Yii::t('UserModule.user', 'Management'),
];

$this->pageTitle = Yii::t('UserModule.user', 'Users - management');

$this->menu = [
    [
        'label' => Yii::t('UserModule.user', 'Users'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('UserModule.user', 'Manage users'),
                'url'   => ['/user/userBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('UserModule.user', 'Create user'),
                'url'   => ['/user/userBackend/create']
            ],
        ]
    ],
    /*[
        'label' => Yii::t('UserModule.user', 'Tokens'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('UserModule.user', 'Token list'),
                'url'   => ['/user/tokensBackend/index']
            ],
        ]
    ],*/
];
?>

<p class="search-wrap">
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?=  Yii::t('UserModule.user', 'Find users'); ?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php
    Yii::app()->getClientScript()->registerScript(
        'search',
        "
    $('.search-form form').submit(function () {
        event.preventDefault();

        $.fn.yiiGridView.update('user-grid', {
            data: $(this).serialize()
        });
    });

    $(document).on('click', '.verify-email', function () {
        var link = $(this);

        event.preventDefault();

        $.post(link.attr('href'), actionToken.token)
            .done(function (response) {
                bootbox.alert(response.data);
            });
    });
"
    );
    $this->renderPartial('_search', ['model' => $model]);
    ?>
</div>

<?php $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id'           => 'user-grid',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => [
            /*[
                'name'  => 'nick_name',
                'type'  => 'raw',
                'value' => 'CHtml::link($data->nick_name, array("/user/userBackend/update", "id" => $data->id))',
            ],*/
            [
                'name'  => 'email',
                'type'  => 'raw',
                'value' => 'CHtml::link($data->email, "mailto:" . $data->email)',
            ],
			[
				'name'     => 'access_level',
				'type'     => 'raw',
				'value'    => '$data->getAccessLevel()',
				'filter'   => CHtml::activeDropDownList(
					$model,
					'access_level',
					$model->getAccessLevelsList(),
					['class' => 'form-control', 'empty' => '']
				),
			],
            /*[
                'class'    => 'webforma\widgets\WEditableColumn',
                'editable' => [
                    'url'    => $this->createUrl('/user/userBackend/inline'),
                    'mode'   => 'popup',
                    'type'   => 'select',
                    'source' => $model->getAccessLevelsList(),
                    'params' => [
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
                    ]
                ],
                'name'     => 'access_level',
                'type'     => 'raw',
                'value'    => '$data->getAccessLevel()',
                'filter'   => CHtml::activeDropDownList(
                    $model,
                    'access_level',
                    $model->getAccessLevelsList(),
                    ['class' => 'form-control', 'empty' => '']
                ),
            ],*/
            [
                'class'   => 'webforma\widgets\EditableStatusColumn',
                'name'    => 'status',
                'url'     => $this->createUrl('/user/userBackend/inline'),
                'source'  => $model->getStatusList(),
                'options' => [
                    User::STATUS_ACTIVE     => ['class' => 'label-success'],
                    User::STATUS_BLOCK      => ['class' => 'label-danger'],
                    User::STATUS_NOT_ACTIVE => ['class' => 'label-warning'],
                ],
            ],
            [
                'class'    => 'webforma\widgets\WEditableColumn',
                'editable' => [
                    'url'    => $this->createUrl('/user/userBackend/inline'),
                    'mode'   => 'popup',
                    'type'   => 'select',
                    'source' => $model->getEmailConfirmStatusList(),
                    'params' => [
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
                    ]
                ],
                'name'     => 'email_confirm',
                'type'     => 'raw',
                'value'    => '$data->getEmailConfirmStatus()',
                'filter'   => CHtml::activeDropDownList(
                    $model,
                    'email_confirm',
                    $model->getEmailConfirmStatusList(),
                    ['class' => 'form-control', 'empty' => '']
                ),
            ],
            /*[
                'class'    => 'webforma\widgets\WEditableColumn',
                'editable' => [
                    'url'    => $this->createUrl('/user/userBackend/inline'),
                    'mode'   => 'popup',
                    'type'   => 'select',
                    'title'  => mb_strtolower($model->getAttributeLabel('news_subscribe')),
                    'source' => ['Нет', 'Да'],
                    'params' => [
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
                    ]
                ],
                'name'     => 'news_subscribe',
                'type'     => 'raw',
                //'value'    => ['Нет', 'Да'],
                'header' => 'Подп. на новости',
                'filter'   => CHtml::activeDropDownList(
                    $model,
                    'news_subscribe',
                    ['Нет', 'Да'],
                    ['class' => 'form-control', 'empty' => '']
                ),
            ],*/
            [
                'name'   => 'create_time',
                'value'  => function($data){
                    return Yii::app()->getDateFormatter()->formatDateTime($data->create_time);
                },
                'filter' => $this->widget('booster.widgets.TbDatePicker', [
                    'model' => $model,
                    'attribute' => 'create_time',
                    'options' => [
                        'format' => 'yyyy-mm-dd',
                    ],
                    'htmlOptions' => [
                        'class' => 'form-control',
                    ],
                ], true),
            ],
            [
                'name'   => 'visit_time',
                'value'  => function($data){
                    return Yii::app()->getDateFormatter()->formatDateTime($data->visit_time);
                },
                'filter' => $this->widget('booster.widgets.TbDatePicker', [
                    'model' => $model,
                    'attribute' => 'visit_time',
                    'options' => [
                        'format' => 'yyyy-mm-dd',
                    ],
                    'htmlOptions' => [
                        'class' => 'form-control',
                    ],
                ], true),
            ],
            [
                'class'       => 'webforma\widgets\CustomButtonColumn',
                'template'    => '{view}{update}{password}{sendactivation}{delete}',
                'buttons'     => [
                    'password'       => [
                        'icon'  => 'fa fa-fw fa-lock',
                        'label' => Yii::t('UserModule.user', 'Change password'),
                        'url'   => 'array("/user/userBackend/changepassword", "id" => $data->id)',
                        'options' => ['class' => 'change-password btn btn-sm btn-default', 'title' => 'Сменить пароль',]
                    ],
                    'sendactivation' => [
                        'label'   => Yii::t('UserModule.user', 'Send activation confirm'),
                        'url'     => 'array("/user/userBackend/sendactivation", "id" => $data->id)',
                        'icon'    => 'fa fa-fw fa-repeat',
                        'visible' => '$data->status != User::STATUS_ACTIVE',
                        'options' => [
                            'class' => 'user sendactivation btn btn-sm btn-default',
							'title' => 'Отправить ссылку для активации',
                        ]
                    ],
                ],
                'afterDelete'=>'function(link,success,data){$("#notifications").notify(data).show(); }',
            ],
        ],
    ]
); ?>
