<?php
/**
 * Отображение для layouts/main
 **/
?>
<!DOCTYPE html>
<html lang="<?= Yii::app()->getLanguage(); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=  CHtml::encode(Yii::app()->name); ?> <?=  CHtml::encode($this->pageTitle); ?></title>
    <?php
    $mainAssets = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.webforma.views.assets'));
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/styles.css');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/main.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/jquery.li-translit.js');
    $path = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.user.views.assets') . '/css/bg-login.jpg');
    ?>
    <link rel="shortcut icon" href="<?=  $mainAssets; ?>/img/favicon.ico"/>
</head>

<body style='background-image: url("<?= $path;?>");background-size: cover;'>
<div id="overall-wrap" class="login-page-wrap">
    <!-- mainmenu -->
    <?php $brandTitle = Yii::t('UserModule.user', 'Control panel'); ?>
    <?php
    $this->widget(
        'bootstrap.widgets.TbNavbar',
        [
            //'type' => 'inverse',
            'fluid'    => true,
            'collapse' => true,
            'fixed'    => 'top',
            'brand'    => Yii::app()->getModule('webforma')->getLogo() ? CHtml::image(
                    Yii::app()->getModule('webforma')->getLogo(),
                    CHtml::encode(Yii::app()->name),
                    [
                        'height' => '38',
                        'title'  => CHtml::encode(Yii::app()->name),
                    ]
                ) : '',
            'brandUrl' => CHtml::normalizeUrl(["/webforma/backend/index"]),
        ]
    ); ?>

    <div class="container-fluid login-page" id="page">
        <div class="row">
            <?=  $content; ?>
        </div>
    </div>
    <div id="footer-guard"></div>
</div>

</body>
</html>
