<?php

/**
 * Форма для запроса смены пароля
 **/
class RecoveryForm extends CFormModel
{
    public $email;

    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'checkEmail'],
        ];
    }

    public function checkEmail($attribute, $params)
    {
        if ($this->hasErrors() === false) {
            $user = User::model()->active()->find(
                'email = :email',
                [
                    ':email' => $this->$attribute
                ]
            );

            if ($user === null) {
                $this->addError(
                    '',
                    Yii::t(
                        'UserModule.user',
                        'Email "{email}" was not found or user was blocked !',
                        [
                            '{email}' => $this->email
                        ]
                    )
                );
            }
        }
    }
}
