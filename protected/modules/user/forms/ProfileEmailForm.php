<?php
use webforma\widgets\WPurifier;

/**
 * Форма изменения email профиля
 **/
class ProfileEmailForm extends CFormModel
{
    public $email;

    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'filter', 'filter' => [$obj = new WPurifier(), 'purify']],
            ['email', 'required'],
            ['email', 'length', 'max' => 50],
            ['email', 'email'],
            ['email', 'checkEmail'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => Yii::t('UserModule.user', 'Email'),
        ];
    }

    public function checkEmail($attribute, $params)
    {
        // Если мыло поменяли
        if (Yii::app()->user->profile->email != $this->$attribute) {
            $model = User::model()->find('email = :email', [':email' => $this->$attribute]);
            if ($model) {
                $this->addError('email', Yii::t('UserModule.user', 'Email already busy'));
            }
        }
    }
}
