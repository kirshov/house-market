<?php

/**
 * Форма изменения пароля профиля
 **/
class ProfilePasswordForm extends CFormModel
{
    public $password;
    public $cPassword;

    public function rules()
    {
        $module = Yii::app()->getModule('user');

        return [
            ['password, cPassword', 'required'],
            ['password, cPassword', 'length', 'min' => $module->minPasswordLength],
            [
                'cPassword',
                'compare',
                'compareAttribute' => 'password',
                'message'          => Yii::t('UserModule.user', 'Password is not coincide')
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password'  => Yii::t('UserModule.user', 'New password'),
            'cPassword' => Yii::t('UserModule.user', 'Password confirmation'),
        ];
    }
}
