<?php

/**
 * Форма смены пароля
 **/
class ChangePasswordForm extends CFormModel
{
    public $password;
    public $cPassword;

    public function rules()
    {
        return [
            ['password, cPassword', 'required'],
            ['password, cPassword', 'length', 'min' => Yii::app()->getModule('user')->minPasswordLength],
            [
                'password',
                'compare',
                'compareAttribute' => 'cPassword',
                'message'          => Yii::t('UserModule.user', 'Password is not coincide!')
            ]
        ];
    }

    public function attributeLabels()
    {
        return [
            'password'  => Yii::t('UserModule.user', 'New password'),
            'cPassword' => Yii::t('UserModule.user', 'Confirm new password'),
        ];
    }
}
