<?php
/**
 * Виджет для вывода popup-информации о пользователе
 */

Yii::import('application.modules.user.models.User');

class UserPopupInfoWidget extends webforma\widgets\WWidget
{
    public $model = null;
    public $view = 'user-popup-info';

    public function run()
    {
        if ($this->model === null || $this->model instanceof User === false) {
            return null;
        }

        $this->render($this->view, ['model' => $this->model]);
    }
}
