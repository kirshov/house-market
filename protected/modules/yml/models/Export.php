<?php

/**
 *
 * @property integer $id
 * @property string $name
 * @property string $use_utm
 * @property string $use_promo
 * @property string $settings
 * @property int count_products
 * @property int count_categories
 * @property int last_update
 *
 */
class Export extends \webforma\models\WModel
{
    /**
     * @var array
     */
    public $brands = [];

    /**
     * @var array
     */
    public $providers = [];

    /**
     * @var array
     */
    public $categories = [];

    /**
     * @var
     */
    public $shop_name;
    /**
     * @var
     */
    public $shop_company;
    /**
     * @var
     */
    public $shop_url;

    /**
     * @var
     */
    public $shop_cpa;

    /**
     * @var
     */
    public $price_to;

    /**
     * @var
     */
    public $price_from;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{yml_export}}';
    }

    /**
     * @param null|string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['name, shop_name, shop_company, shop_url', 'required'],
            ['name', 'unique'],
            ['name', 'length', 'max' => 255],
            ['shop_name', 'length', 'max' => 20],
            ['shop_url, shop_cpa', 'length', 'max' => 255],
            ['price_from, price_to', 'numerical', 'integerOnly' => true],
            ['brands, categories, providers, use_utm, use_promo', 'safe'],
            ['id, name, settings', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('YmlModule.default', 'Id'),
            'name' => Yii::t('YmlModule.default', 'Title'),
            'settings' => Yii::t('YmlModule.default', 'Settings'),
            'brands' => Yii::t('YmlModule.default', 'Brands'),
            'providers' => Yii::t('YmlModule.default', 'Providers'),
            'categories' => Yii::t('YmlModule.default', 'Categories'),
            'shop_name' => Yii::t('YmlModule.default', 'Store short title'),
            'shop_company' => Yii::t('YmlModule.default', 'Company name'),
            'shop_url' => Yii::t('YmlModule.default', 'Store URL'),
            'shop_cpa' => Yii::t('YmlModule.default', 'To participate in the "Buy at Market" program'),
            'price_from' => 'Цена от',
            'price_to' => 'Цена до',
            'use_utm' => 'Использовать utm-метки',
            'use_promo' => 'Выгружать акции',
            'count_products' => 'Товаров',
            'count_categories' => 'Категорий',
            'last_update' => 'Обновлен',
        ];
    }


    /**
     * @return CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria();

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider(
            $this, [
                'criteria' => $criteria,
            ]
        );
    }

    /**
     *
     */
    public function afterFind()
    {
        $settings = json_decode($this->settings);
        $this->brands = $settings->brands;
        $this->providers = $settings->providers;
        $this->categories = $settings->categories;
        $this->shop_name = $settings->shop_name;
        $this->shop_company = $settings->shop_company;
        $this->shop_url = $settings->shop_url;
        $this->shop_cpa = $settings->shop_cpa;
        $this->price_from = $settings->price_from;
        $this->price_to = $settings->price_to;

		$this->last_update = Helper::isDate($this->last_update) ? date('d.m.Y H:i', strtotime($this->last_update)) : '-';
        parent::afterFind();
    }


    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $settings = [
            'brands' => $this->brands,
            'providers' => $this->providers,
            'categories' => $this->categories,
            'shop_name' => $this->shop_name,
            'shop_company' => $this->shop_company,
            'shop_url' => $this->shop_url,
            'shop_cpa' => $this->shop_cpa,
            'price_from' => $this->price_from,
            'price_to' => $this->price_to,
        ];
        $this->settings = json_encode($settings);

        return parent::beforeValidate();
    }

	/**
	 *
	 */
    public function afterDelete()
    {
	    parent::afterDelete();
	    $dir = Yii::app()->getModule('yml')->getYmlPath();

	    @unlink($dir.$this->id.'.yml');
    }
}
