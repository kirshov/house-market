<?php
set_time_limit(0);
use webforma\components\ContentType;

/**
 * Class ExportController
 *
 */
class ExportController extends \webforma\components\controllers\FrontController
{
	public function actionView($id)
	{
		$dir = Yii::getPathOfAlias('public').'/'.Yii::app()->getModule('webforma')->uploadPath.'/yml/';
		$file = $dir.$id.'.yml';
		$yml = '';
		if(is_file($file)){
			$yml = file_get_contents($file);
		}

		if(!$yml){
			Yii::app()->getComponent('ymlRepository')->createYml($id);
			$yml = file_get_contents($file);
		}

		\webforma\components\ContentType::setHeader(\webforma\components\ContentType::TYPE_XML);
		echo $yml;
		Yii::app()->end();
	}
}
