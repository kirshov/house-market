<?php

use webforma\components\WebModule;

/**
 * Class YmlModule
 */
class YmlModule extends WebModule
{
    /**
     *
     */
    public $adminMenuOrder = 11;

	/**
	 * @param int $id
	 * @return string
	 */
    public function getYmlPath(){
	    $path = Yii::getPathOfAlias('webroot').'/'.Yii::app()->getModule('webforma')->uploadPath.'/yml/';

	    if(!is_dir($path)){
		    CFileHelper::createDirectory($path, null, true);
	    }

	    return $path;
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return ['store'];
    }

    /**
     * @return bool
     */
    public function getEditableParams()
    {
        return false;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
	    return Yii::t('YmlModule.default', 'Store');
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/yml/exportBackend/index';
    }

    /**
     * @return string
     */
    public function appendNavigationToModule(){
        return 'Экспорт товаров';
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            ['label' => $this->getName()],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('YmlModule.default', 'Export lists'),
                'url' => ['/yml/exportBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('YmlModule.default', 'Create export list'),
                'url' => ['/yml/exportBackend/create'],
            ],
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('YmlModule.default', 'Yml export');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('YmlModule.default', 'Module to export products in the yml file');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-upload';
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'application.modules.yml.models.Export',
                'application.modules.store.models.Product',
            ]
        );
    }
}
