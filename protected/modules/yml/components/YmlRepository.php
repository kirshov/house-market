<?php
class YmlRepository extends CApplicationComponent
{
	/**
	 * @var Export
	 */
	protected $model;

	/**
	 * @var SimpleXMLElement
	 */
	protected $dom;

	/**
	 * @var SimpleXMLElement
	 */
	protected $shop;

	/**
	 * @var SimpleXMLElement
	 */
	protected $offers;

	/**
	 * @var string
	 */
	protected $minCost;

	/**
	 * @var array
	 */
	protected $promoPrices = [];

	/**
	 * @var string
	 */
	protected $minCostText;

	/**
	 * @var string
	 */
	protected $currency;

	/**
	 * @var array
	 */
	protected $allowedCategories = [];

	/**
	 * @var int
	 */
	protected $countCategories = 0;

	/**
	 * @var int
	 */
	protected $countOffers = 0;

	public function __construct() {
		/**
		 * @var StoreModule $storeModule
		 */
		$storeModule = Yii::app()->getModule('store');
		$this->minCost = $storeModule->minCost;
		$this->minCostText = $storeModule->minCost.' '.$this->getPluralCurrency('RUB', $storeModule->minCost);
		$this->currency = $storeModule->currency;
	}

	/**
	 * @return Product[]
	 */
	public function getOffers()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 't.status = :status AND (t.quantity > 0 OR ISNULL(t.quantity) OR t.quantity = "") AND discontinued = 0';
		$criteria->params = [
			':status' => Product::STATUS_ACTIVE,
		];

		$criteria->addInCondition('t.category_id', (array) $this->allowedCategories);

		if(is_array($this->model->brands) && isset($this->model->brands) && !$this->model->brands[0]){
			unset($this->model->brands[0]);
		}
		if (!empty($this->model->brands)) {
			$criteria->addInCondition('t.producer_id', (array)$this->model->brands);
		}

		if(is_array($this->model->providers) && isset($this->model->providers) && !$this->model->providers[0]){
			unset($this->model->providers[0]);
		}
		if (!empty($this->model->providers)) {
			$criteria->addInCondition('t.provider_id', (array)$this->model->providers);
		}

		if(is_numeric($this->model->price_from)){
			$criteria->addCondition('t.price >= '.$this->model->price_from);
		}
		if(is_numeric($this->model->price_to)){
			$criteria->addCondition('t.price <= '.$this->model->price_to);
		}

		$criteria->scopes = [];
		$criteria->with = ['variants', 'type', 'attributesValues', 'type.typeAttributesNoSort'];

		$countCriteria = clone $criteria;
		$countCriteria->with = [];

		$dataProvider = new CActiveDataProvider('Product', [
			'countCriteria' => $countCriteria,
			'criteria' => $criteria,
		]);

		return new CDataProviderIterator($dataProvider, 100);
	}

	public function createYml($id){
		$this->model = Export::model()->findByPk($id);

		if (!$this->model) {
			throw new CHttpException(404);
		}

		$this->dom = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><yml_catalog date="'.date('Y-m-d H:i').'" />');
		$this->shop = $this->dom->addChild('shop');

		$this->shop->addChild('name', $this->model->shop_name);
		$this->shop->addChild('company', $this->model->shop_company);
		$this->shop->addChild('url', $this->model->shop_url);

		$this->setCurrencies();
		$this->setCategories();

		$this->shop->addChild('cpa', $this->model->shop_cpa);

		$this->offers = $this->shop->addChild('offers');
		$this->setOffers();

		$dir = Yii::app()->getModule('yml')->getYmlPath();

		$dom = new DOMDocument('1.0', 'utf-8');
		$dom->formatOutput = true;

		$domXml = dom_import_simplexml($this->dom);
		$domXml = $dom->importNode($domXml, true);
		$dom->appendChild($domXml);

		$dom->save($dir.$id.'.yml');

		Export::model()->updateByPk($id, [
			'count_categories' => $this->countCategories,
			'count_products' => $this->countOffers,
			'last_update' => new CDbExpression('NOW()'),
		]);
	}

	/**
	 *
	 */
	public function setCurrencies(){
		$currencyList = Yii::app()->getModule('store')->getCurrencyList();
		$currencies = $this->shop->addChild('currencies');
		foreach ($currencyList as $currency) {
			$currencyElement = $currencies->addChild('currency');
			$currencyElement->addAttribute('id', $currency);
			$currencyElement->addAttribute('rate', ($currency === 'RUB' ? 1 : 'CBRF'));
		}
	}

	/**
	 *
	 */
	public function setCategories(){
		$categoryCriteria = new CDbCriteria();

		$categoryIds = Yii::app()->getComponent('categoryRepository')->getActiveCategories(0, true);
		if($this->model->categories){
			$activeCategories = array_uintersect($this->model->categories, $categoryIds, 'strcasecmp');
		} else {
			$activeCategories = $categoryIds;
		}

		$categoryCriteria->addInCondition('t.id', (array) $activeCategories);

		$categoryList = StoreCategory::model()->findAll($categoryCriteria);

		$categories = $this->shop->addChild('categories');
		foreach ($categoryList as $category) {
			$this->allowedCategories[] = $category->id;
			$categoryElement = $categories->addChild('category', $category->name);
			$categoryElement->addAttribute('id', $category->id);
			if($category->parent_id){
				$categoryElement->addAttribute('parentId', $category->parent_id);
			}
			$this->countCategories++;
		}
	}

	/**
	 *
	 */
	public function setOffers(){
		$offers = $this->getOffers();

		foreach ($offers as $offer){
			$data = [];
			$isNeedAdd = true;

			$price = $offer->getResultPrice();
			if (!$price || !$offer->getProducerName() || !$offer->sku || !$offer->image) {
				$isNeedAdd = false;
			}

			$data['image'] = StoreImage::product($offer);

			if (!$data['image'] || mb_strpos($data['image'], '/images/nophoto.jpg') !== false) {
				$isNeedAdd = false;
			}

			$data['categoryId'] = $offer->category_id;
			$data['producer'] = htmlspecialchars(strip_tags($offer->getProducerName()));
			$country = $offer->getProducer()->country ? : '';
			if($country){
				$data['country'] = $country;
			}
			$data['description'] = htmlspecialchars(strip_tags($offer->description));
			if (mb_strlen($data['description']) > 3000) {
				$data['description'] = mb_substr($data['description'], 0, 2990).'...';
			}

			$paramsData = $this->getParams($offer, htmlspecialchars(strip_tags($offer->name)));
			$data['name'] = $paramsData['name'];
			$data['ymlFields'] = $paramsData['ymlFields'];
			$data['params'] = $paramsData['params'];

			$variants = [];
			$needGrouping = false;
			if($offer->getVariants()){
				foreach ($offer->getVariants() as $variant){
					$price = $variant->getResultPrice();
					if (!$price || !$variant->sku) {
						continue;
					}

					if ($variant->sku == $offer->sku || (is_numeric($variant->quantity) && $variant->quantity < 1)) {
						continue;
					}

					if ((is_numeric($this->model->price_from) && $price <= $this->model->price_from) || (is_numeric($this->model->price_to) && $price >= $this->model->price_to)) {
						continue;
					}

					$dataVariant = $data;

					$dataVariant['image'] = StoreImage::variant($variant);

					if (!$dataVariant['image'] || mb_strpos($dataVariant['image'], '/images/nophoto.jpg') !== false) {
						continue;
					}

					$dataVariant['url'] = CHtml::normalizeUrl(Yii::app()->getBaseUrl(true) . ProductHelper::getUrl($offer)) . 'var-' . $variant->id.'/';

					if($variant->attribute->yml_group_variant){
						$dataVariant['groupId'] = $offer->id;
						$needGrouping = true;
					}

					$variantAttrValue = $variant->getOptionValue();
					if($variant->attribute->yml_add_name_on_variant){
						$dataVariant['name'] = htmlspecialchars(strip_tags($offer->name)).' '.$variantAttrValue;
					}
					if ($variant->attribute->yml_field) {
						$dataVariant['ymlFields'][$variant->attribute->yml_field] = $variantAttrValue;
					}

					$unit = $variant->attribute->unit;

					if($variant->attribute->yml_add_unit){
						$parseGeneralCharacteristic = $this->prepareGeneralCharacteristic($variantAttrValue);
						if($parseGeneralCharacteristic){
							if($parseGeneralCharacteristic['value']){
								$variantAttrValue = $parseGeneralCharacteristic['value'];
							}
							if($parseGeneralCharacteristic['unit']){
								$unit = $parseGeneralCharacteristic['unit'];
							}
						}
					}

					$dataVariant[$variant->attribute->name] = [
						'name' => $variant->attribute->title,
						'value' => $variantAttrValue,
						'unit' => $unit,
					];

					$dataVariant['is_variant'] = true;

					$variants[] = [
						'variant' => $variant,
						'data' => $dataVariant,
					];
				}
			}
			if(!empty($variants) && $needGrouping){
				$data['groupId'] = $offer->id;
			}
			if($isNeedAdd){
				$this->setOffer($offer, $data);
			}

			if(!empty($variants)){
				foreach ($variants as $_variantItem){
					$this->setOffer($_variantItem['variant'], $_variantItem['data']);
				}
			}
		}
	}

	/**
	 * @param $attributeValue
	 * @return array
	 */
	public function prepareGeneralCharacteristic($attributeValue){
		$exp = explode(' ', $attributeValue);
		$count = sizeOf($exp);
		if($count > 1){
			$unit = $exp[$count - 1];
			unset($exp[$count - 1]);
			$variantAttrValue = implode(' ', $exp);

			return [
				'value' => $variantAttrValue,
				'unit' => $unit,
			];
		}

		return [];
	}

	/**
	 * @param $offer Product | ProductVariant
	 * @param $data array
	 * @return mixed
	 */
	public function setOffer($offer, $data = []){
		$offerElement = $this->offers->addChild('offer');
		$offerElement->addAttribute('id', (isset($data['is_variant']) ? 'var' : '').$offer->id);
		$offerElement->addAttribute('type', 'vendor.model');
		$offerElement->addAttribute('available', ($offer->isInStock() ? 'true' : 'false'));
		if($data['groupId']){
			$offerElement->addAttribute('group_id', $data['groupId']);
		}

		$basePrice = (int) $offer->getBasePrice();
		$price = $offer->getResultPrice();
		if ($this->model->use_promo && $basePrice > $price) {
			$_id = $data['groupId'] ? $data['groupId'].'_'.$offer->id : $offer->id;
			$promoPrice[$_id] = $price;
		}

		$offerElement->addChild('cpa', $offer->isInStock() && $price >= $this->minCost ? 1 : 0);
		$offerElement->addChild('price', $price);
		if($basePrice > $price && (100 - ($price * 100 / $basePrice)) >= 5){
			$offerElement->addChild('oldprice', $basePrice);
		}

		$offerElement->addChild('categoryId', $data['categoryId']);
		$offerElement->addChild('currencyId', $this->currency);

		if($data['url']){
			$url = $data['url'];
		} else {
			$url = CHtml::normalizeUrl(Yii::app()->getBaseUrl(true) . ProductHelper::getUrl($offer));
		}
		if($this->model->use_utm){
			$url .= '?utm_source=market.yandex.ru&amp;utm_campaign='.$offer->id;
		}

		$offerElement->addChild('model', $data['name']);
		$offerElement->addChild('url', $url);
		$offerElement->addChild('picture', $data['image']);
		$offerElement->addChild('vendor', $data['producer']);
		$offerElement->addChild('vendorCode', $offer->sku);
		if($data['country']){
			$offerElement->addChild('country_of_origin', $data['country']);
		}

		$offerElement->addChild('description', $data['description']);

		if($data['ymlFields']){
			foreach ($data['ymlFields'] as $paramName => $paramValue){
				list($_value) = explode(',', $paramValue);
				$offerElement->addChild($paramName, $_value);
			}
		}

		if($data['params']){
			foreach ($data['params'] as $paramName => $paramValue){
				$param = $offerElement->addChild('param', htmlspecialchars(strip_tags($paramValue['value'])));
				$param->addAttribute('name', $paramValue['name']);
				if($paramValue['unit']){
					$param->addAttribute('unit', $paramValue['unit']);
				}
			}
		}

		if ($this->minCost > $price){
			$offerElement->addChild('sales_notes', 'Минимальная сумма заказа '.$this->minCostText.'.');
		}

		$this->countOffers++;
	}

	/**
	 * @param $offer Product
	 * @return array
	 */
	protected function getParams($offer, $name){
		$params = $ymlFields = [];
		foreach ($offer->getAttributeGroups() as $groupName => $items){
			foreach ($items as $attribute) {
				$attrValue = AttributeRender::renderValue($attribute, $offer->attribute($attribute), true, '{item}');
				if (!$attrValue) continue;
				$attrValue = trim($attrValue);
				if ($attribute->yml_field) {
					$ymlFields[$attribute->yml_field] = $attrValue;
				}
				if ($attribute->yml_add_name && (($attribute->yml_add_name_on_variant && $offer->variants) || !$attribute->yml_add_name_on_variant)) {
					$name .= ' '.$attrValue;
				}
				$unit = $attribute->unit;

				if($attribute->yml_add_unit){
					$parseGeneralCharacteristic = $this->prepareGeneralCharacteristic($attrValue);
					if($parseGeneralCharacteristic){
						if($parseGeneralCharacteristic['value']){
							$attrValue = $parseGeneralCharacteristic['value'];
						}
						if($parseGeneralCharacteristic['unit']){
							$unit = $parseGeneralCharacteristic['unit'];
						}
					}
				}

				$params[$attribute->name] = [
					'name' => $attribute->title,
					'value' => $attrValue,
					'unit' => $unit,
				];
			}
		}

		return [
			'name' => $name,
			'params' => $params,
			'ymlFields' => $ymlFields,
		];
	}

	protected function setPromo(){
		if($this->model->use_promo && Yii::app()->hasModule('promo')){
			$promo = Promo::model()->findAll('yandex_type != 0 AND status = '.Promo::STATUS_PUBLISHED);
		}
	}

	/**
	 * @param $currency
	 * @param $cost
	 * @return string
	 */
	public function getPluralCurrency($currency, $cost){
		if($currency == 'RUB'){
			return Helper::pluralForm($cost, ['рубль', 'рубля', 'рублей']);
		}
	}

	/**
	 * @return string
	 */
	public function getXmlHead()
	{
		return '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL.'<!DOCTYPE yml_catalog SYSTEM "shops.dtd">'.PHP_EOL;
	}

}