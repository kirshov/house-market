<?php
/* @var $model Export */
$this->breadcrumbs = [
    Yii::t('YmlModule.default', 'Products export') => ['/yml/exportBackend/index'],
    Yii::t('YmlModule.default', 'Manage'),
];

$this->pageTitle = Yii::t('YmlModule.default', 'Products export - manage');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('YmlModule.default', 'Manage export lists'), 'url' => ['/yml/exportBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('YmlModule.default', 'Create export list'), 'url' => ['/yml/exportBackend/create']],
];
?>

<?php $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'export-grid',
        'type' => 'condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => [
            [
                'name' => 'id',
            ],
            [
                'name' => 'name',
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::link($data->name, ["/yml/exportBackend/update", "id" => $data->id]);
                },
            ],
            [
                'header' => 'URL',
                'type' => 'raw',
                'value' => function ($data) {
                    $url = Yii::app()->createAbsoluteUrl('/yml/export/view', ['id' => $data->id]);
                    $url = rtrim($url, '/');
                    return CHtml::link($url, $url, ['target' => '_blank']);
                }
            ],
			[
				'name' => 'count_categories',
				'filter' => false,
				'htmlOptions' => [
					'style' => 'text-align:center',
				],
			],
			[
				'name' => 'count_products',
				'filter' => false,
				'htmlOptions' => [
					'style' => 'text-align:center',
				],
			],
			[
				'name' => 'last_update',
				'filter' => false,
				'htmlOptions' => [
					'style' => 'text-align:center',
				],
			],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
                'template' => '{update} {refresh} {delete}',
				'buttons' => [
					'refresh' => [
						'label' => 'Перегенирировать',
						'url' => function ($data) {
							return Yii::app()->createUrl("/backend/yml/export/refresh/".$data->id);
						},
						'icon' => 'fa fa-fw fa-refresh',
						'options' => ['class' => 'front-view btn btn-sm btn-default'],
					],
				],
            ],
        ],
    ]
); ?>
