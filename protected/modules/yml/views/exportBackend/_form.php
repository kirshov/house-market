<?php
/**
 * @var $model Export
 * @var $form TbActiveForm
 */

$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'id' => 'export-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'htmlOptions' => ['class' => 'well'],
    ]
); ?>
<div class="alert alert-info">
    <?= Yii::t('YmlModule.default', 'Fields with'); ?>
    <span class="required">*</span>
    <?= Yii::t('YmlModule.default', 'are required'); ?>
</div>

<?= $form->errorSummary($model); ?>

<div class='row'>
    <div class="col-sm-8">
        <?= $form->textFieldGroup($model, 'name'); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-8">
        <div class="row">
            <div class="col-sm-6">
                <?= $form->textFieldGroup($model, 'shop_name'); ?>
            </div>
            <div class="col-sm-6">
                <?= $form->textFieldGroup($model, 'shop_url'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?= $form->textFieldGroup($model, 'shop_company'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?= $form->checkboxGroup($model, 'shop_cpa'); ?>
            </div>
        </div>
		<div class="row">
			<div class="col-sm-4">
			    <?= $form->checkboxGroup($model, 'use_utm'); ?>
			</div>
			<?if(Yii::app()->hasModule('promo')):?>
				<div class="col-sm-4">
					<?= $form->checkboxGroup($model, 'use_promo'); ?>
				</div>
			<?endif;?>
		</div>
    </div>
</div>

<div class='row'>
    <div class="col-sm-4">
        <?= CHtml::hiddenField('Export[brands]'); ?>
        <?= $form->dropDownListGroup(
            $model,
            'brands',
            [
                'widgetOptions' => [
                    'data' => CMap::mergeArray([0 => 'Все бренды'], Producer::model()->getFormattedList()),
                    'htmlOptions' => ['multiple' => true, 'size' => 10],
                ]
            ]
        ); ?>
    </div>
    <div class="col-sm-4">
        <?= CHtml::hiddenField('Export[providers]'); ?>
        <?= $form->dropDownListGroup(
            $model,
            'providers',
            [
                'widgetOptions' => [
                    'data' => CMap::mergeArray([0 => 'Все поставщики'], Provider::model()->getFormattedList()),
                    'htmlOptions' => ['multiple' => true, 'size' => 10],
                ]
            ]
        ); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <?= $form->textFieldGroup($model, 'price_from'); ?>
    </div>
    <div class="col-sm-4">
        <?= $form->textFieldGroup($model, 'price_to'); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-8">
        <?= CHtml::label(Yii::t('YmlModule.default', 'Categories'), null); ?>
        <div class="row expand-control-wrapper">
            <div class="col-sm-3 a select-all">Выбрать все</div><div class="col-sm-3 a select-none">Снять все</div>
        </div>
        <?php $this->widget('store.widgets.CategoryTreeWidget', ['selectedCategories' => $model->categories, 'id' => 'category-tree']); ?>
        <?= CHtml::hiddenField('Export[categories]'); ?>
    </div>
</div>
<br/>

<div class="buttons">
	<?php $this->widget(
		'bootstrap.widgets.TbButton',
		[
			'buttonType' => 'submit',
			'context' => 'primary',
			'label' => $model->getIsNewRecord() ? Yii::t('YmlModule.default', 'Add list and continue') : Yii::t('YmlModule.default', 'Save list and continue'),
		]
	); ?>

	<?php $this->widget(
		'bootstrap.widgets.TbButton',
		[
			'buttonType' => 'submit',
			'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
			'label' => $model->getIsNewRecord() ? Yii::t('YmlModule.default', 'Add list and close') : Yii::t('YmlModule.default', 'Save list and close'),
		]
	); ?>
</div>
<?php $this->endWidget(); ?>

<?php Yii::app()->getClientScript()->registerScript(
    __FILE__,
    <<<JS
        $('#export-form').submit(function () {
            var form = $(this);
            $('#category-tree').find('a.jstree-clicked').each(function (index, element) {
                form.append('<input type="hidden" name="Export[categories][]" value="' + $(element).data('category-id') + '" />');
            });
        });
        $('document').ready(function(){
            $('.select-all').on('click', function(){
                $('.jstree-node.jstree-closed > .jstree-icon.jstree-ocl').trigger('click');
                //$('a.jstree-anchor').not('.jstree-clicked').trigger('click');
                $.each($('.jstree-container-ul li'), function(){
                    if($(this).find('li').length == 0){
                        $(this).find('a.jstree-anchor').not('.jstree-clicked').trigger('click');
                    }
                });
            });
            
            $('.select-none').on('click', function(){
                
                //$('a.jstree-anchor.jstree-clicked').trigger('click');
                $.each($('.jstree-container-ul li'), function(){
                    if($(this).find('li').length == 0){
                        $(this).find('a.jstree-anchor.jstree-clicked').trigger('click');
                    }
                });
                
                $('.jstree-node.jstree-open > .jstree-icon.jstree-ocl').trigger('click');
            })
        });
       
JS
);?>
