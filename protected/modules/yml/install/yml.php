<?php

return [
    'module' => [
        'class' => 'application.modules.yml.YmlModule',
    ],
	'component' => [
		'ymlRepository' => [
			'class' => 'application.modules.yml.components.YmlRepository'
		],
	],
    'rules' => [
        [
            'yml/export/view',
            'pattern' => '/uploads/yml/<id:\d+>.yml',
            'urlSuffix' => ''
        ],
    ],
];
