<?php

class m180904_023058_add_fields extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn("{{yml_export}}", "use_utm", "tinyint(1) not null default 0");
	}

	public function safeDown()
	{

	}
}