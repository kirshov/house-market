<?php

class m180910_160124_add_fields extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn("{{yml_export}}", "use_promo", "tinyint(1) not null default 0");
	}

	public function safeDown()
	{

	}
}