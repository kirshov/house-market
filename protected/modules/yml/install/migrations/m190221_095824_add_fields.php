<?php

class m190221_095824_add_fields extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn("{{yml_export}}", "count_products", "int not null default 0");
		$this->addColumn("{{yml_export}}", "count_categories", "int not null default 0");
		$this->addColumn("{{yml_export}}", "last_update", "datetime");
	}

	public function safeDown()
	{

	}
}