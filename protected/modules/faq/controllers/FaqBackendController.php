<?php

class FaqBackendController extends webforma\components\controllers\BackController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Faq.FaqBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Faq.FaqBackend.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['Faq.FaqBackend.Create']],
            [
                'allow',
                'actions' => ['update', 'inline'],
                'roles' => ['Faq.FaqBackend.Update'],
            ],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Faq.FaqBackend.Delete']],
            ['deny'],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'inline' => [
                'class' => 'webforma\components\actions\WInLineEditAction',
                'model' => 'Faq',
                'validAttributes' => ['name', 'description', 'status'],
            ],
	        'sortable' => [
		        'class' => 'webforma\components\actions\SortAction',
		        'model' => 'Faq',
		        'attribute' => 'position',
	        ],
        ];
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return void
     */
    public function actionCreate()
    {
        $model = new Faq();

        if (($data = Yii::app()->getRequest()->getPost('Faq')) !== null) {
            $model->setAttributes($data);

            if ($model->save()) {
                Yii::app()->getUser()->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('FaqModule.faq', 'New faq was added!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['create']
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id the ID of the model to be updated
     *
     * @return void
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (($data = Yii::app()->getRequest()->getPost('Faq')) !== null) {
            $model->setAttributes($data);

            if ($model->save()) {
                Yii::app()->getUser()->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('FaqModule.faq', 'Faq was changed!')
                );

                Yii::app()->getCache()->delete("Faq{$model->code}");

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['update', 'id' => $model->id]
                    )
                );
            }
        }
        $this->render('update', ['model' => $model]);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     *
     * @param integer $id the ID of the model to be deleted
     *
     * @return void
     *
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            // we only allow deletion via POST request
            $model = $this->loadModel($id);
            if($model->is_system == 1){
	            throw new CHttpException(403, 'Невозможно удалить системный блок');
            }
            $model->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            Yii::app()->getRequest()->getIsAjaxRequest() || $this->redirect(
                (array)Yii::app()->getRequest()->getPost('returnUrl', 'index')
            );

        } else {
            throw new CHttpException(400, Yii::t('FaqModule.faq', 'Unknown request!'));
        }
    }

    /**
     * Manages all models.
     *
     * @return void
     */
    public function actionIndex()
    {
        $model = new Faq('search');

        $model->unsetAttributes(); // clear any default values

        $model->setAttributes(
            Yii::app()->getRequest()->getParam(
                'Faq',
                []
            )
        );

        $this->render('index', ['model' => $model]);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     *
     * @param integer $id the ID of the model to be loaded
     *
     * @return Faq $model
     *
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Faq::model()->with('category')->findByPk((int)$id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('FaqModule.faq', 'Page was not found!'));
        }

        return $model;
    }
}
