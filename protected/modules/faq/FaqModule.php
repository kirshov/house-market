<?php

class FaqModule extends webforma\components\WebModule
{
	/**
	 *
	 */
	public $adminMenuOrder = 90;

	/**
	 * @var int
	 */
	public $useCategory = 0;

	/**
	 * @var
	 */
	public $mainCategory;

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return 'Контент';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Вопрос - ответ';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return "fa fa-fw fa-question-circle";
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'faq.models.*',
            ]
        );
    }


	/**
	 * @return array
	 */
	public function getParamsLabels()
	{
		return [
			'useCategory' => 'Использовать категории',
			'mainCategory' => 'Главная категория',
		];
	}

	/**
	 * @return array
	 */
	public function getEditableParams()
	{
		$params = [];

		if(DEVMODE){
			$params = CMap::mergeArray($params, [
				'useCategory' => [
					'input-type' => 'checkbox',
				],
				'mainCategory' => CHtml::listData($this->getCategoryList(), 'id', 'name'),
			]);
		}
		return !empty($params) ? $params : false;
	}

	/**
	 * @return array
	 */
	public function getEditableParamsGroups()
	{
		$groups = [];

		if(DEVMODE){
			$groups = CMap::mergeArray($groups, [
				'system' => [
					'label' => 'Системные настройки',
					'items' => [
						'useCategory',
						'mainCategory',
					]
				]
			]);
		}

		return $groups;
	}

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/faq/faqBackend/index';
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('FaqModule.faq', 'Faqs'),
                'url' => ['/faq/faqBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('FaqModule.faq', 'Add faq'),
                'url' => ['/faq/faqBackend/create'],
            ],
        ];
    }

    /**
     * @return bool
     */
    public function getIsInstallDefault()
    {
        return false;
    }
}
