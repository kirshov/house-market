<?php
$this->breadcrumbs = [
    Yii::t('FaqModule.faq', 'Faqs') => ['/faq/faqBackend/index'],
    $model->name => [
        '/faq/faqBackend/view',
        'id' => $model->id
    ],
    Yii::t('FaqModule.faq', 'Editing'),
];

$this->pageTitle = Yii::t('FaqModule.faq', 'Faqs - edit');

$this->menu = [
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('FaqModule.faq', 'Faqs'),
        'url'   => ['/faq/faqBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('FaqModule.faq', 'Add faq'),
        'url'   => ['/faq/faqBackend/create']
    ],
    [
        'icon'  => 'fa fa-fw fa-pencil',
        'label' => Yii::t('FaqModule.faq', 'Edit faq'),
        'url'   => [
            '/faq/faqBackend/update',
            'id' => $model->id
        ]
    ],
    [
        'icon'        => 'fa fa-fw fa-trash-o',
        'label'       => Yii::t('FaqModule.faq', 'Remove faq'),
        'url'         => '#',
        'linkOptions' => [
            'submit'  => ['/faq/faqBackend/delete', 'id' => $model->id],
            'params'  => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
            'confirm' => Yii::t('FaqModule.faq', 'Do you really want to remove faq?'),
        ]
    ],
];

$this->title = Yii::t('FaqModule.faq', 'Editing blocks');
$this->subTitle = '&laquo;'.$model->name.'&raquo;'
?>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>
