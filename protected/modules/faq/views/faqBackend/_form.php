<?php
/**
 * @var $model Faq
 * @var $this FaqBackendController
 * @var $form \webforma\widgets\ActiveForm
 */
?>
<?php
$form = $this->beginWidget(
    'webforma\widgets\ActiveForm',
    [
        'enableAjaxValidation'   => false,
        'enableClientValidation' => false,
		'type' => 'vertical',
		'htmlOptions' => ['class' => 'well sticky'],
		'clientOptions' => [
			'validateOnSubmit' => true,
		],
    ]
); ?>
<div class="alert alert-info">
    <?=  Yii::t('FaqModule.faq', 'Fields with'); ?>
    <span class="required">*</span>
    <?=  Yii::t('FaqModule.faq', 'are required.'); ?>
</div>

<?=  $form->errorSummary($model); ?>

<div class="row">
	<div class="col-sm-6">
		<?=  $form->textFieldGroup($model, 'name'); ?>
	</div>
</div>

<div class="row">
	<div class="col-sm-3">
        <?=  $form->dropDownListGroup($model, 'status', [
			'widgetOptions' => [
				'data' => $model->getStatusList()
			],
		]); ?>
    </div>
</div>

<?if($this->getModule()->useCategory):?>
	<div class="row">
		<div class="col-sm-3">
			<?=  $form->dropDownListGroup($model, 'category_id', [
				'widgetOptions' => [
					'data'        => Yii::app()->getComponent('categoriesRepository')->getFormattedList($this->getModule()->mainCategory),
					'htmlOptions' => ['empty' => Yii::t('FaqModule.faq', '--choose--'), 'encode' => false],
				],
			]); ?>
		</div>
	</div>
<?endif;?>

<div class="row">
    <div class="col-sm-12">
		<?=  $form->labelEx($model, 'content'); ?>
		<?php $this->widget($this->webforma->getVisualEditor(), [
			'model'     => $model,
			'attribute' => 'content',
			'options' => [
				'rows' => 25,
			],
		]); ?>
    </div>
</div>


<div class="buttons">
	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context'    => 'primary',
		'label'      => $model->isNewRecord
			? 'Добавить и продолжить'
			: 'Сохранить и продолжить',
	]); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType'  => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label'       => $model->isNewRecord
			? 'Добавить и закрыть'
			: 'Сохранить и закрыть',
	]); ?>
</div>
<?php $this->endWidget(); ?>
