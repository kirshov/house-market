<?php
/**
 * @var $this FaqBackendController
 * @var $model ContentBlock
 */
$this->breadcrumbs = [
    Yii::t('FaqModule.faq', 'Faqs'),
];

$this->pageTitle = Yii::t('FaqModule.faq', 'Faqs - admin');

$this->menu = [
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('FaqModule.faq', 'Faqs'),
        'url' => ['/faq/faqBackend/index']
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('FaqModule.faq', 'Add faq'),
        'url' => ['/faq/faqBackend/create']
    ],
];

$this->title = Yii::t('FaqModule.faq', 'Faq');
$this->subTitle = Yii::t('FaqModule.faq', 'administration');

$useCategory = $this->getModule()->useCategory;
$mainCategory = $this->getModule()->mainCategory;
?>

<?php $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'faq-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
	    'sortableRows'      => true,
	    'sortableAjaxSave'  => true,
	    'sortableAttribute' => 'position',
	    'sortableAction'    => '/faq/faqBackend/sortable',
        'columns' => [
            [
                'class' => 'webforma\widgets\WEditableColumn',
                'name' => 'name',
                'editable' => [
                    'url' => $this->createUrl('/faq/faqBackend/inline'),
                    'mode' => 'popup',
                    'params' => [
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
                    ]
                ],
                'filter' => CHtml::activeTextField($model, 'name', ['class' => 'form-control']),
            ],
			[
				'name'   => 'category_id',
				'value'  => '$data->getCategoryName()',
				'filter' => CHtml::activeDropDownList($model, 'category_id', Yii::app()->getComponent('categoriesRepository')->getFormattedList($mainCategory), [
                    'encode' => false,
                    'empty' => '',
                    'class' => 'form-control'
                ]),
				'visible' => $useCategory,
			],
            [
                'class' => 'webforma\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('/faq/faqBackend/inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    Faq::STATUS_ACTIVE => ['class' => 'label-success'],
                    Faq::STATUS_NOT_ACTIVE => ['class' => 'label-info'],
                ],
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
            ],
        ],
    ]
); ?>
