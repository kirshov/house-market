<?php
$this->breadcrumbs = [
    Yii::t('FaqModule.faq', 'Faqs') => ['/faq/faqBackend/index'],
    Yii::t('FaqModule.faq', 'Create'),
];

$this->pageTitle = Yii::t('FaqModule.faq', 'Faq - add');

$this->menu = [
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('FaqModule.faq', 'Faqs'),
        'url'   => ['/faq/faqBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('FaqModule.faq', 'Add faq'),
        'url'   => ['/faq/faqBackend/create']
    ],
];

$this->title = Yii::t('FaqModule.faq', 'Faq');
$this->subTitle = Yii::t('FaqModule.faq', 'add');
?>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>
