<?php

use webforma\widgets\WPurifier;

/**
 * @property string $id
 * @property string $name
 * @property string $content
 * @property integer $status
 * @property integer $category_id
 *
 * @property Category|null $category
 *
 * @method Faq active()
 */
class Faq extends webforma\models\WModel
{
    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;
    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     * Returns the static model of the specified AR class.
     * @param  string $className
     * @return ContentBlock the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{faq}}';
    }

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'sortable' => [
				'class' => 'webforma\components\behaviors\SortableBehavior',
				'attributeName' => 'position',
			],
		];
	}
    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['name, content', 'filter', 'filter' => 'trim'],
            ['name, content, status', 'required'],
            ['category_id, position, status', 'numerical', 'integerOnly' => true],
            ['name', 'length', 'max' => 250],
            ['id, name, content, category_id, status', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'active' => [
                'condition' => $this->tableAlias . '.status = :status',
                'params' => [':status' => self::STATUS_ACTIVE],
            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'category' => [self::BELONGS_TO, 'Category', 'category_id']
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('FaqModule.faq', 'id'),
            'name' => Yii::t('FaqModule.faq', 'Name'),
            'content' => Yii::t('FaqModule.faq', 'Content'),
            'category_id' => Yii::t('FaqModule.faq', 'Category'),
            'status' => Yii::t('FaqModule.faq', 'Status'),
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria();
        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.name', $this->name, true);
        $criteria->compare($this->tableAlias . '.content', $this->content, true);
        $criteria->compare($this->tableAlias . '.category_id', $this->category_id);
        $criteria->compare($this->tableAlias . '.status', $this->status);

        return new CActiveDataProvider(get_class($this), [
        	'criteria' => $criteria,
	        'sort' => ['defaultOrder' => 't.position'],
        ]);
    }

	/**
	 *
	 */
	protected function afterSave()
	{
		\TaggedCache\TaggingCacheHelper::deleteTag('faq-items');
		parent::afterSave();
	}

	/**
	 *
	 */
	protected function afterDelete()
	{
		\TaggedCache\TaggingCacheHelper::deleteTag('faq-items');
		parent::afterDelete();
	}

	/**
     * @return string
     */
    public function getCategoryName()
    {
        return empty($this->category) ? '---' : $this->category->name;
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_NOT_ACTIVE => Yii::t('FaqModule.faq', 'Disabled'),
            self::STATUS_ACTIVE => Yii::t('FaqModule.faq', 'Enabled'),
        ];
    }
}