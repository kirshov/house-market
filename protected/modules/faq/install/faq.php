<?php
/**
 * Файл конфигурации модуля
 **/
return [
    'module'    => [
        'class' => 'application.modules.faq.FaqModule',
    ],
    'import'    => [],
    'component' => [],
	'params'    => [
		'widgets'   => [
			'faq' => 'application.modules.faq.widgets.FaqWidget',
		],
	],
    'rules'     => [],
];
