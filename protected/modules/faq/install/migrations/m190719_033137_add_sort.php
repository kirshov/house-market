<?php

class m190719_033137_add_sort extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{faq}}', 'position', 'int(11) default 1');
		$this->createIndex('ix_{{faq}}_position', '{{faq}}', 'position', false);
	}

	public function safeDown()
	{

	}
}