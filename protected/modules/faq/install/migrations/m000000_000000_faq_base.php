<?php

class m000000_000000_faq_base extends webforma\components\DbMigration
{

    public function safeUp()
    {
        $this->createTable(
            '{{faq}}',
            [
                'id'          => 'pk',
                'name'        => 'varchar(250) NOT NULL',
                'content'     => "text NOT NULL",
                'description' => "varchar(255) DEFAULT NULL",
                'category_id' => "integer DEFAULT NULL",
                'status' => "tinyint(1) NOT NULL DEFAULT 1",
            ],
            $this->getOptions()
        );


		$this->addForeignKey("fk_{{faq}}_category_id", '{{faq}}', 'category_id', '{{category_category}}','id', 'SET NULL', 'NO ACTION');
	    $this->createIndex('ix_{{faq}}_status', '{{faq}}', 'status');
    }

    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{faq}}');
    }
}
