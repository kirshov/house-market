<?php
/**
 * Виджет для отрисовки блока контента:
 **/
Yii::import('application.modules.faq.models.Faq');
Yii::import('application.modules.faq.FaqModule');

/**
 * Class FaqWidget
 */
class FaqWidget extends webforma\widgets\WWidget
{
    /**
     * @var string
     */
    public $view = 'default';

    /**
     * @throws CException
     */
    public function run()
    {
        $cacheName = 'faq-items';

        $items = Yii::app()->getCache()->get($cacheName);

        if (false === $items) {

	        $items = Faq::model()->active()->findAll(['order' => 't.position']);

            Yii::app()->getCache()->set($cacheName, $items, 0, \TaggedCache\TaggingCacheHelper::getDependency([$cacheName]));
        }

        $this->render($this->view, ['items' => $items]);
    }
}
