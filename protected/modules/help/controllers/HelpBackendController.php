<?php
/**
 * HelpBackendController контроллер панели управления для управления страницами
 */

/**
 * Class HelpBackendController
 */
class HelpBackendController extends webforma\components\controllers\BackController
{
	public function init()
	{
		parent::init();
	}

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'inline' => [
                'class' => 'webforma\components\actions\WInLineEditAction',
                'model' => 'Help',
                'validAttributes' => ['title', 'status', 'title_short'],
            ],
            'sortable' => [
                'class' => 'webforma\components\actions\SortAction',
                'model' => 'Help',
                'attribute' => 'order',
            ],
        ];
    }

    /**
     * @var Help $model the currently loaded data model instance.
     */
    private $_model;

    /**
     * Displays a particular model.
     *
     * @param int $id - record ID
     *
     * @return void
     */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' help.
     *
     * @return void
     *
     * @throws CDbException
     */
    public function actionCreate()
    {
        $model = new Help();

        if (($data = Yii::app()->getRequest()->getPost('Help')) !== null) {
            $model->setAttributes($data);
            $transaction = Yii::app()->getDb()->beginTransaction();
            try {
                if (true === $model->save()) {
                    Yii::app()->getUser()->setFlash(
                        webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                        Yii::t('HelpModule.help', 'Page was created')
                    );

                    $transaction->commit();

                    $this->redirect(
                        (array)Yii::app()->getRequest()->getPost(
                            'submit-type',
                            ['create']
                        )
                    );
                }
            } catch (Exception $e) {
                $transaction->rollback();
                $model->addError(false, $e->getMessage());
            }
        }

        $this->render(
            'create',
            [
                'model' => $model,
                'pages' => Help::model()->getFormattedList(),
            ]
        );
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' help.
     *
     * @param int $id - record ID
     *
     * @return void
     *
     * @throws CDbException
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (($data = Yii::app()->getRequest()->getPost('Help')) !== null) {

            $model->setAttributes($data);

            if ($model->save()) {

                Yii::app()->getUser()->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('HelpModule.help', 'Page was updated!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['update', 'id' => $model->id]
                    )
                );
            }
        }

        $this->render(
            'update',
            [
                'model' => $model,
                'pages' => Help::model()->getFormattedList(
                    null,
                    0,
                    ['condition' => 'id != :id', 'params' => [':id' => $model->id]]
                ),
            ]
        );
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'index' help
     *
     * @param int $id - record ID
     *
     * @return void
     *
     * @throws CHttpException
     */
    public function actionDelete($id = null)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            $model = $this->loadModel($id);

            // we only allow deletion via POST request
            $model->delete();

            Yii::app()->getUser()->setFlash(
                webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                Yii::t('HelpModule.help', 'Record was removed!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            Yii::app()->getRequest()->getParam('ajax') !== null || $this->redirect(
                (array)Yii::app()->getRequest()->getPost('returnUrl', 'index')
            );
        } else {
            throw new CHttpException(
                404,
                Yii::t('HelpModule.help', 'Bad request. Please don\'t repeat similar requests anymore!')
            );
        }
    }

    /**
     * Manages all models.
     *
     * @return void
     */
    public function actionIndex()
    {
    	$viewPage = Yii::app()->getRequest()->getParam('page');
    	if($viewPage){
    		$model = $this->loadModel((int) $viewPage);
		    return $this->render('page', [
			    'model' => $model,
		    ]);
	    }

        $model = new Help('search');

        $model->unsetAttributes();

        $model->setAttributes(Yii::app()->getRequest()->getParam('Help', []));

        $this->render('index', [
            'model' => $model,
            'pages' => Help::model()->getAllPagesList(),
        ]);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     *
     * @param int $id - record ID
     *
     * @return Help $model
     *
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        if ($this->_model === null || $this->_model->id !== $id) {

            if (($this->_model = Help::model()->with('author')->findByPk($id)) === null) {
                throw new CHttpException(
                    404,
                    Yii::t('HelpModule.help', 'Page was not found')
                );
            }
        }

        return $this->_model;
    }

	/**
	 * @param array $pages
	 * @return array
	 */
	public function getBreadCrumbs($pages = [])
	{
		if ($this->_model->parent) {
			$pages = $this->getBreadCrumbsRecursively($this->_model->parent);
		}

		$pages = array_reverse($pages);
		$pages[] = $this->_model->title;

		return $pages;
	}

	/**
	 * @param  Help $page
	 * @return array
	 * @internal param int $pageId
	 */
	private function getBreadCrumbsRecursively(Help $page)
	{
		$pages = [];
		$pages[$page->title] = Yii::app()->createUrl('/help/helpBackend/index', ['page' => $page->id]);
		$pp = $page->parent;

		if ($pp) {
			$pages += $this->getBreadCrumbsRecursively($pp);
		}

		return $pages;
	}
}
