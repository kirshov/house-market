<?php
use webforma\components\Event;
use webforma\widgets\WPurifier;

/**
 * Help модель
 */

/**
 * This is the model class for table "Help".
 *
 * The followings are the available columns in table 'Help':
 *
 * @property integer $id
 * @property string $parent_id
 * @property integer $category_id
 * @property string $create_time
 * @property string $update_time
 * @property string $title
 * @property string $title_short
 * @property string $icon
 * @property string $body
 * @property integer $status
 * @property integer $user_id
 * @property integer $order
 * @property string $layout
 * @property string $show_isset_module
 *
 * @property Help $parent
 *
 * @method Help[] childPages
 * @method Help parent
 * @method Help published
 * @method Help root
 */
class Help extends webforma\models\WModel
{
    /**
     *
     */
    const STATUS_DRAFT = 0;
    /**
     *
     */
    const STATUS_PUBLISHED = 1;
    /**
     *
     */
    const STATUS_MODERATION = 2;

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className
     * @return Help the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{help}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['title', 'required', 'on' => ['update', 'insert']],
            ['status, order', 'numerical', 'integerOnly' => true, 'on' => ['update', 'insert']],
            ['parent_id, category_id', 'numerical', 'integerOnly' => true, 'allowEmpty' => true],
            ['parent_id, category_id', 'default', 'setOnEmpty' => true, 'value' => null],
            ['title_short, show_isset_module', 'length', 'max' => 150],
            ['title, layout', 'length', 'max' => 250],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            ['title, title_short, body, icon', 'filter', 'filter' => 'trim'],
            ['title, title_short', 'filter', 'filter' => [new WPurifier(), 'purify']],
            [
                'id, parent_id, create_time, update_time, title, title_short, body, status, order',
                'safe',
                'on' => 'search',
            ],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = [
            'CTimestampBehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ],
            'sortable' => [
                'class' => 'webforma\components\behaviors\SortableBehavior',
                'attributeName' => 'order',
            ],

			/*'tree' => [
				'class' => 'webforma\components\behaviors\DCategoryTreeBehavior',
				'aliasAttribute' => 'id',
				'requestPathAttribute' => 'path',
				'parentAttribute' => 'parent_id',
				'parentRelation' => 'parent',
				'defaultCriteria' => [
					'order' => 't.order',
				],
				'titleAttribute' => 'title',
				'useCache' => true,
			],*/
        ];

		return $behaviors;
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'childPages' => [self::HAS_MANY, 'Help', 'parent_id', 'condition' => 'childPages.status = '.self::STATUS_PUBLISHED, 'order' => 'childPages.order, childPages.create_time'],
            'parent' => [self::BELONGS_TO, 'Help', 'parent_id'],
	        'author' => [self::BELONGS_TO, 'User', 'user_id'],
            //'category' => [self::BELONGS_TO, 'Category', 'category_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('HelpModule.help', 'Id'),
            'parent_id' => Yii::t('HelpModule.help', 'Parent'),
            'category_id' => Yii::t('HelpModule.help', 'Category'),
            'create_time' => Yii::t('HelpModule.help', 'Created at'),
            'update_time' => Yii::t('HelpModule.help', 'Changed'),
            'title' => Yii::t('HelpModule.help', 'Title'),
            'title_short' => Yii::t('HelpModule.help', 'Short title'),
            'body' => Yii::t('HelpModule.help', 'Text'),
            'status' => Yii::t('HelpModule.help', 'Status'),
            'user_id' => Yii::t('HelpModule.help', 'Created by'),
            'order' => Yii::t('HelpModule.help', 'Sorting'),
            'layout' => Yii::t('HelpModule.help', 'Layout'),
            'icon' => Yii::t('HelpModule.help', 'Icon'),
            'show_isset_module' => 'Показывать только установлен модуль',
        ];
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        if ($this->getIsNewRecord()) {
            $this->user_id = Yii::app()->getUser()->getId();
        }

        return parent::beforeSave();
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'published' => [
				'condition' => 'status = :status',
				'params' => ['status' => self::STATUS_PUBLISHED],
			],
	        'root' => [
				'condition' => 'ISNULL(parent_id)',
			],
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria();

        //$criteria->with = ['author'];

        $criteria->compare('t.id', $this->id);
        $criteria->compare('parent_id', $this->parent_id);
        $criteria->compare('create_time', $this->create_time);
        $criteria->compare('update_time', $this->update_time);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('title_short', $this->title_short, true);
        $criteria->compare('body', $this->body);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('category_id', $this->category_id);
        $criteria->compare('layout', $this->layout);

        return new CActiveDataProvider(
            get_class($this), [
                'criteria' => $criteria,
                'sort' => ['defaultOrder' => 't.order, t.create_time'],
            ]
        );
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_PUBLISHED => Yii::t('HelpModule.help', 'Published'),
            self::STATUS_DRAFT => Yii::t('HelpModule.help', 'Draft'),
            self::STATUS_MODERATION => Yii::t('HelpModule.help', 'On moderation'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('HelpModule.help', '*unknown*');
    }

    /**
     * @param bool $selfId
     * @return array
     */
    public function getAllPagesList($selfId = false)
    {
        $criteria = new CDbCriteria();
        $criteria->order = "{$this->tableAlias}.order DESC, {$this->tableAlias}.create_time DESC";
        if ($selfId) {
            $otherCriteria = new CDbCriteria();
            $otherCriteria->addNotInCondition('id', (array)$selfId);
            $otherCriteria->group = "{$this->tableAlias}.slug, {$this->tableAlias}.id";
            $criteria->mergeWith($otherCriteria);
        }

        return CHtml::listData($this->findAll($criteria), 'id', 'title');
    }

    /**
     * @return string
     */
    public function getCategoryName()
    {
        return ($this->category === null) ? '---' : $this->category->name;
    }

    /**
     * @return string
     */
    public function getParentName()
    {
        return ($this->parent === null) ? '---' : $this->parent->title;
    }

    /**
     * Возвращает отформатированный список в соответствии со вложенность страниц.
     *
     * @param null|int $parentId
     * @param int $level
     * @param null|array|CDbCriteria $criteria
     * @return array
     */
    public function getFormattedList($parentId = null, $level = 0, $criteria = null)
    {
        if (empty($parentId)) {
            $parentId = null;
        }

        if($criteria === null){
	        $criteria = new CDbCriteria();
	        $criteria->order = 't.order, t.create_time';
        }

        $models = $this->findAllByAttributes(['parent_id' => $parentId], $criteria);

        $list = [];

        foreach ($models as $model) {

            $model->title = str_repeat('&emsp;', $level) . $model->title;

            $list[$model->id] = $model->title;

            $list = CMap::mergeArray($list, $this->getFormattedList($model->id, $level + 1, $criteria));
        }

        return $list;
    }

	/**
	 *
	 */
	protected function afterSave()
	{
		parent::afterSave();

		$cacheName = 'help_'. (int) $this->parent_id;
		\TaggedCache\TaggingCacheHelper::deleteTag([$cacheName]);
	}

	/**
	 *
	 */
	protected function afterDelete()
	{
		parent::afterDelete();

		$cacheName = 'help_'. (int) $this->parent_id;
		\TaggedCache\TaggingCacheHelper::deleteTag([$cacheName]);
	}

	/*protected function afterFind()
	{
		parent::afterFind();

        foreach (['slug', 'status'] as $key){
            $this->_oldData[$key] = $this->{$key};
		}
	}*/

	/**
	 * @param int|null $currentId
	 * @return array
	 */
	public static function getMenu($currentId = null){
		$menu = Yii::app()->getCache()->get('helpMenu');
		if(!$menu){
			$pages = self::getPagesWithCache(null);
			$menu = self::getSubmenu($pages, $currentId);
		}

		return $menu;
	}

	/**
	 * @param Help[] $pages
	 * @param int|null $currentId
	 * @return array
	 */
	protected static function getSubmenu($pages, $currentId = null){
		if(!$pages){
			return [];
		}

		$menu = [];
		foreach ($pages as $page){
			if($page->show_isset_module){
				if(!Yii::app()->hasModule($page->show_isset_module)){
					continue;
				}
			}

			$menu[$page->id] = [
				'label' => $page->title,
				'not-prepare' => true,
				'url' => ['/help/helpBackend/index', 'page' => $page->id],
			];

			$subPages = self::getPagesWithCache($page->id);

			if($subPages){
				$menu[$page->id]['items'] = self::getSubmenu($subPages, $currentId);
			} else {
				$menu[$page->id]['url'] = ['/help/helpBackend/index', 'page' => $page->id];
			}

			if($currentId && $page->id == $currentId){
				$menu[$page->id]['active'] = true;
			}

			if($page->icon){
				$menu[$page->id]['icon'] = 'fa fa-fw fa-'.$page->icon;
			}
		}

		return $menu;
	}

	/**
	 * @param $parentId
	 * @return array|mixed|null
	 */
	public static function getPagesWithCache($parentId){
		$cacheName = 'help_'. (int) $parentId;
		$pages = Yii::app()->getCache()->get($cacheName);

		if($pages === false){
			$criteria = new CDbCriteria();
			$criteria->order = 't.order, t.create_time';

			if(!$parentId){
				$criteria->condition = 'ISNULL(parent_id)';
			} else {
				$criteria->condition = 'parent_id = :parent_id AND status = :status';
				$criteria->params = ['status' => self::STATUS_PUBLISHED, ':parent_id' => (int) $parentId];
			}

			$pages = self::model()->findAll($criteria);
		}
		Yii::app()->getCache()->set($cacheName, $pages, 0, \TaggedCache\TaggingCacheHelper::getDependency([$cacheName]));
		return $pages;
	}
}
