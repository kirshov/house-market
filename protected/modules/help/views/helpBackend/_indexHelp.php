<?php

/**
 * @var $model Page
 * @var $this PageBackendController
 * @var $pages array
 */
$this->breadcrumbs = [
	//Yii::t('HelpModule.help', 'Pages') => ['/help/helpBackend/index'],
	Yii::t('HelpModule.help', 'Help'),
];

$this->pageTitle = Yii::t('HelpModule.help', 'Help');

$this->menu = Help::getMenu();
?>

<div class="help-content">
	<div class="help-body" style="text-align: center;">
		<h1>Добро пожаловать!</h1>
		<p>&nbsp</p>
		<p>&larr; Выберите интересующий раздел слева</p>
	</div>
</div>

