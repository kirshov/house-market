<?php
$this->breadcrumbs = [
    Yii::t('HelpModule.help', 'Pages') => ['/help/helpBackend/index'],
    Yii::t('HelpModule.help', 'Add page'),
];

$this->pageTitle = Yii::t('HelpModule.help', 'Add page');

$this->menu = [
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('HelpModule.help', 'Pages'),
        'url'   => ['/help/helpBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('HelpModule.help', 'Create page'),
        'url'   => ['/help/helpBackend/create']
    ],
];
?>

<?= $this->renderPartial('_form', [
	'model'        => $model,
	'pages'        => $pages,
]); ?>
