<?php
$this->breadcrumbs = [
    Yii::t('HelpModule.help', 'Pages') => ['/help/helpBackend/index'],
    $model->title                      => ['/help/helpBackend/view', 'id' => $model->id],
    Yii::t('HelpModule.help', 'Edit'),
];

$this->pageTitle = Yii::t('HelpModule.help', 'Pages - edit');

$this->menu = [
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('HelpModule.help', 'Pages'),
        'url'   => ['/help/helpBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('HelpModule.help', 'Create page'),
        'url'   => ['/help/helpBackend/create']
    ],
    [
        'icon'  => 'fa fa-fw fa-pencil',
        'label' => Yii::t('HelpModule.help', 'Edit page'),
        'url'   => [
            '/help/helpBackend/update/',
            'id' => $model->id
        ]
    ],
    [
        'icon'  => 'fa fa-fw fa-eye',
        'label' => Yii::t('HelpModule.help', 'View page'),
        'url'   => [
            '/help/helpBackend/view',
            'id' => $model->id
        ]
    ],
    [
        'icon'        => 'fa fa-fw fa-trash-o',
        'label'       => Yii::t('HelpModule.help', 'Remove page'),
        'url'         => '#',
        'linkOptions' => [
            'submit'  => ['/help/helpBackend/delete', 'id' => $model->id],
            'params'  => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
            'confirm' => Yii::t('HelpModule.help', 'Do you really want to remove page?'),
            'csrf'    => true,
        ]
    ],
];
?>

<?= $this->renderPartial('_form', [
	'pages'        => $pages,
	'model'        => $model,
]); ?>
