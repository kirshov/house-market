<?php

/**
 * @var $model Page
 * @var $this PageBackendController
 * @var $pages array
 */
$this->breadcrumbs = [
	Yii::t('HelpModule.help', 'Help') => ['/help/helpBackend/index'],
	Yii::t('HelpModule.help', 'List'),
];

$module = Yii::app()->getModule('page');

$this->pageTitle = Yii::t('HelpModule.help', 'Pages');

$this->menu = [
	[
		'icon'  => 'fa fa-fw fa-list-alt',
		'label' => Yii::t('HelpModule.help', 'Pages'),
		'url'   => ['/help/helpBackend/index']
	],
	[
		'icon'  => 'fa fa-fw fa-plus-square',
		'label' => Yii::t('HelpModule.help', 'Create page'),
		'url'   => ['/help/helpBackend/create']
	],
	[
		'icon'  => 'fa fa-fw fa-eye',
		'label' => Yii::t('HelpModule.help', 'View Help'),
		'url'   => ['/help/helpBackend/index', 'action' => 'view']
	],
];
?>

	<p class="search-wrap">
		<a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
			<i class="fa fa-search">&nbsp;</i>
			<?= Yii::t('HelpModule.help', 'Find pages'); ?>
			<span class="caret">&nbsp;</span>
		</a>
	</p>

	<div id="search-toggle" class="collapse out search-form">
		<?php
		Yii::app()->clientScript->registerScript(
			'search',
			"
    $('.search-form form').submit(function () {
        $.fn.yiiGridView.update('page-grid', {
            data: $(this).serialize()
        });

        return false;
    });
"
		);
		$this->renderPartial('_search', ['model' => $model, 'pages' => $pages]);
		?>
	</div>

<?php

$this->widget(
	'webforma\widgets\CustomGridView',
	[
		'id'           => 'page-grid',
		'dataProvider' => $model->search(),
		'filter'       => $model,
		'sortableRows'      => true,
		'sortableAjaxSave'  => true,
		'sortableAttribute' => 'order',
		'sortableAction'    => '/help/helpBackend/sortable',
		'columns'      => [
			'id',
			[
				'class'    => 'webforma\widgets\WEditableColumn',
				'name'     => 'title',
				'editable' => [
					'url'    => $this->createUrl('/help/helpBackend/inline'),
					'mode'   => 'inline',
					'params' => [
						Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
					]
				],
				'filter'   => CHtml::activeTextField($model, 'title', ['class' => 'form-control']),
			],
			[
				'name'   => 'parent_id',
				'value'  => '$data->parentName',
				'filter' => CHtml::activeDropDownList($model, 'parent_id', $model->getFormattedList(), [
					'encode' => false,
					'empty' => '',
					'class' => 'form-control'
				]),
			],
			[
				'class'   => 'webforma\widgets\EditableStatusColumn',
				'name'    => 'status',
				'url'     => $this->createUrl('/help/helpBackend/inline'),
				'source'  => $model->getStatusList(),
				'options' => [
					Page::STATUS_PUBLISHED  => ['class' => 'label-success'],
					Page::STATUS_MODERATION => ['class' => 'label-warning'],
					Page::STATUS_DRAFT      => ['class' => 'label-default'],
				],
			],
			[
				'class' => 'webforma\widgets\CustomButtonColumn',
			],
		],
	]
);