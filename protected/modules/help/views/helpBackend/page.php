<?php
$this->breadcrumbs = $this->getBreadCrumbs([
	Yii::t('HelpModule.help', 'Help') => ['/help/helpBackend/index']
]);

$this->pageTitle = $model->title;

$this->menu = Help::getMenu($model->id);
?>
<div class="help-content">
	<?if($model->title_short):?>
		<h3><?= $model->title_short; ?></h3>
	<?endif;?>

	<div class="help-body"><?= $model->body; ?></div>
</div>
<?if(EDITOR):?>
	<div class="help-actions">
		<div>
			<small>
				<?= CHtml::link('Перейти к редактированию', ['/help/helpBackend/update', 'id' => $model->id], ['target' => '_blank']); ?>
			</small>
		</div>
	</div>
<?endif;?>