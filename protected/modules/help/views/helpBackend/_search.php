<?php
$form = $this->beginWidget(
	'\webforma\widgets\ActiveForm',
	[
		'action'      => Yii::app()->createUrl($this->route),
		'method'      => 'get',
		'type' => 'vertical',
		'htmlOptions' => ['class' => 'well'],
	]
); ?>
<fieldset class="inline">
	<div class="row">
		<div class="col-sm-6">
			<?= $form->textFieldGroup($model, 'title'); ?>
		</div>
		<div class="col-sm-6">
			<?= $form->textFieldGroup($model, 'body', ['cols' => 3]); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-3">
			<?= $form->dropDownListGroup($model, 'parent_id', [
				'widgetOptions' => [
					'data'        => $pages,
					'htmlOptions' => [
						'class' => 'popover-help',
						'empty' => Yii::t('HelpModule.help', '- not set -')
					],
				],
				'cols' => 3,
			]); ?>
		</div>
		<div class="col-sm-3">
			<?= $form->dropDownListGroup($model, 'status', [
				'widgetOptions' => [
					'data'        => $model->statusList,
					'htmlOptions' => [
						'class' => 'popover-help',
						'empty' => Yii::t('HelpModule.help', '- no matter -')
					],
				],
				'cols' => 3,
			]); ?>
		</div>
	</div>

	<div class="row">
		<?if(DEVMODE):?>
			<div class="col-sm-3">
				<?= $form->textFieldGroup($model, 'layout'); ?>
			</div>
		<?endif;?>
	</div>

</fieldset>

<?php $this->widget('bootstrap.widgets.TbButton', [
	'buttonType'  => 'submit',
	'context'     => 'primary',
	'encodeLabel' => false,
	'label'       => '<i class="fa fa-search">&nbsp;</i> ' . Yii::t('HelpModule.help', 'Find pages'),
]); ?>

<?php $this->widget('bootstrap.widgets.TbButton', [
	'buttonType' => 'reset',
	'context'    => 'danger',
	'encodeLabel' => false,
	'label'       => '<i class="fa fa-times">&nbsp;</i> Очистить форму',
]); ?>
<?php $this->endWidget(); ?>
