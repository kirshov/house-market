<?php
/**
 * @var $this HelpBackendController
 * @var $model Help
 * @var $form \webforma\widgets\ActiveForm
 */
?>
<?php
$form = $this->beginWidget(
    'webforma\widgets\ActiveForm',
    [
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => 'vertical',
		'htmlOptions' => ['class' => 'well sticky'],
		'clientOptions' => [
			'validateOnSubmit' => true,
		],
    ]
); ?>

<div class="tab-content">
	<?= $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-sm-6">
			<div class="row">
				<div class="col-sm-6">
					<?= $form->dropDownListGroup($model, 'parent_id', [
						'widgetOptions' => [
							'data'        => $pages,
							'htmlOptions' => [
								'class' => 'popover-help',
								'empty' => Yii::t('HelpModule.help', '--choose--'),
								'encode' => false,
							],
						],
					]); ?>
				</div>

				<div class="col-sm-6">
					<?= $form->textFieldGroup($model, 'icon'); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<?= $form->textFieldGroup($model, 'title'); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<?= $form->textFieldGroup($model, 'title_short'); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<?= $form->labelEx($model, 'body'); ?>
			<?php
			$this->widget($this->module->getVisualEditor(), [
				'model'     => $model,
				'attribute' => 'body',
				'settings' => [
					'folder' => 'help/',
				]
			]); ?>
			<br/>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6">
			<?= $form->dropDownListGroup($model, 'status', [
				'widgetOptions' => [
					'data'        => $model->getStatusList(),
					'htmlOptions' => [
						'class' => 'popover-help',
						'empty' => Yii::t('HelpModule.help', '--choose--'),
						'encode' => false,
					],
				],
			]); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6">
			<?= $form->textFieldGroup($model, 'show_isset_module'); ?>
		</div>
	</div>
</div>

<div class="buttons">
	<?php
	$this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context'    => 'primary',
		'label'      => $model->isNewRecord
			? 'Создать страницу и продолжить'
			: 'Сохранить страницу и продолжить',
	]); ?>

	<?php
	$this->widget('bootstrap.widgets.TbButton', [
		'buttonType'  => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label'       => $model->isNewRecord
			? 'Создать страницу и закрыть'
			: 'Сохранить страницу и закрыть',
	]); ?>
</div>
<?php $this->endWidget(); ?>