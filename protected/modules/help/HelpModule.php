<?php


class HelpModule extends webforma\components\WebModule
{
	/**
	 * @var int
	 */
	public $useCategory = 0;

	/**
	 * @var
	 */
	public $mainCategory;

    /**
     * @return bool
     */
    public function getIsInstallDefault()
    {
        return true;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
    	return false;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('HelpModule.help', 'Help');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return '';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-support';
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'application.modules.help.models.*',
            ]
        );
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/help/helpBackend/index';
    }
}
