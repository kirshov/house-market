<?php

class m000000_000000_help_base extends webforma\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        $this->createTable(
            '{{help}}',
            [
                'id'             => 'pk',
                'category_id'    => 'integer DEFAULT NULL',
                'parent_id'      => 'integer DEFAULT NULL',
                'create_time'    => 'datetime NOT NULL',
                'update_time'    => 'datetime NOT NULL',
                'user_id'        => 'integer  DEFAULT NULL',
                'title'          => 'varchar(250) NOT NULL',
                'title_short'    => 'varchar(250) NOT NULL',
                'icon'           => 'varchar(250) NOT NULL',
                'body'           => 'MEDIUMTEXT NOT NULL',
                'layout'         => 'varchar(250) NOT NULL',
                'status'         => 'integer NOT NULL',
                'order'          => 'integer NOT NULL',
                'show_isset_module'  => 'varchar(150) default null',
            ],
            $this->getOptions()
        );

        $this->createIndex("ix_{{help}}_status", '{{help}}', "status", false);
        $this->createIndex("ix_{{help}}_user_id", '{{help}}', "user_id", false);
        $this->createIndex("ix_{{help}}_order", '{{help}}', "order", false);

        $this->addForeignKey(
            "fk_{{help}}_user_id",
            '{{help}}',
            'user_id',
            '{{user_user}}',
            'id',
            'SET NULL',
            'NO ACTION'
        );

		try {
			$path = dirname(dirname(__FILE__)).'/help.sql';
			if (file_exists($path)) {
				$content = file_get_contents($path);
				Yii::app()->getDb()->createCommand($content)->execute();
			}
		}catch (Exception $e) {}
	}

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropTableWithForeignKeys("{{help}}");
    }
}
