<?php

use webforma\components\WebModule;

/**
 * Class YandexMoneyModule
 */
class YandexMoneyModule extends WebModule
{
    /**
     * @return array
     */
    public function getDependencies()
    {
        return ['payment'];
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [];
    }

    /**
     * @return bool
     */
    public function getAdminPageLink()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function getIsShowInAdminMenu()
    {
        return false;
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('YandexMoneyModule.ymoney', 'Store');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('YandexMoneyModule.ymoney', 'Yandex.Money');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('YandexMoneyModule.ymoney', 'Yandex.Money payment module');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-jpy';
    }

    /**
     *
     */
    public function init()
    {
        parent::init();
    }
}
