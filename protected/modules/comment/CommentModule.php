<?php

/**
 * CommentModule основной класс модуля comment
 */

use webforma\components\WebModule;

/**
 * Class CommentModule
 */
class CommentModule extends WebModule
{
	/**
	 *
	 */
	public $adminMenuOrder = 70;

    /**
     * @var
     */
    public $defaultCommentStatus;
    /**
     * @var bool
     */
    public $autoApprove = true;
    /**
     * @var bool
     */
    public $notify = true;
    /**
     * @var
     */
    public $email;
    /**
     * @var int
     */
    public $showCaptcha = 1;
    /**
     * @var int
     */
    public $minCaptchaLength = 3;
    /**
     * @var int
     */
    public $maxCaptchaLength = 6;
    /**
     * @var int
     */
    public $perPage = 10;
    /**
     * @var int
     */
    public $antiSpamInterval = 3;
    /**
     * @var string
     */
    public $allowedTags = 'p,br,strong,img[src|style],a[href]';
    /**
     * @var int
     */
    public $allowGuestComment = 0;
    /**
     * @var int
     */
    public $stripTags = 1;
    /**
     * @var string
     */
    public $assetsPath = "application.modules.comment.views.assets";

    /**
     * @var
     */
    public $modelsAvailableForRss;

    /**
     * параметры для загрузки изображений
     */
    public $allowedExtensions = 'jpg,jpeg,png,gif';
    /**
     * @var int
     */
    public $minSize = 0;
    /**
     * @var int
     */
    public $maxSize = 5368709120;

    /**
     * @var string - id редактора
     */
    public $editor = 'textarea';

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getModelsAvailableForRss()
    {
        return empty($this->modelsAvailableForRss) ? [] : explode(',', $this->modelsAvailableForRss);
    }


    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [
            'defaultCommentStatus' => Yii::t('CommentModule.comment', 'Default comments status'),
            'autoApprove' => Yii::t('CommentModule.comment', 'Automatic comment confirmation'),
            'notify' => Yii::t('CommentModule.comment', 'Notify about comment?'),
            'email' => Yii::t('CommentModule.comment', 'Email for notifications'),
            'showCaptcha' => Yii::t('CommentModule.comment', 'Show captcha for guests'),
            'minCaptchaLength' => Yii::t('CommentModule.comment', 'Minimum captcha length'),
            'maxCaptchaLength' => Yii::t('CommentModule.comment', 'Maximum captcha length'),
            'rssCount' => Yii::t('CommentModule.comment', 'RSS records count'),
            'allowedTags' => Yii::t('CommentModule.comment', 'Accepted tags'),
            'antiSpamInterval' => Yii::t('CommentModule.comment', 'Antispam interval'),
            'allowGuestComment' => Yii::t('CommentModule.comment', 'Guest can comment ?'),
            'editor' => Yii::t('WebformaModule.webforma', 'Visual editor'),
            'stripTags' => Yii::t(
                'CommentModule.comment',
                'Remove tags in the derivation comment using strip_tags() ?'
            ),
            'modelsAvailableForRss' => Yii::t(
                'CommentModule.comment',
                'Models available for rss export (for example: Post, Blog etc.)'
            ),
        ];
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
        return [
            'allowGuestComment' => $this->getChoice(),
            'defaultCommentStatus' => Comment::model()->getStatusList(),
            'autoApprove' => $this->getChoice(),
	        'showCaptcha' => $this->getChoice(),
	        'notify' => $this->getChoice(),
            /*'email' => [
            	'input-type' => 'text',
	            'htmlOptions' => [
	            	'placeholder' => Yii::app()->getModule('webforma')->notifyEmailTo,
	            ]
            ],*/

           /* 'minCaptchaLength',
            'maxCaptchaLength',
            'allowedTags',
            'antiSpamInterval',
            'stripTags' => $this->getChoice(),*/
        ];
    }

    /**
     * @return array
     */
    public function getEditableParamsGroups()
    {
        return [
            'main' => [
                'label' => Yii::t('CommentModule.comment', 'Module general settings'),
                'items' => [
                    'defaultCommentStatus',
	                'autoApprove',
                    'allowGuestComment',
	                'showCaptcha',
                    'notify',
                    //'email',
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('CommentModule.comment', 'Content');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('CommentModule.comment', 'Comments');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('CommentModule.comment', 'Module for simple comments support');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return "fa fa-fw fa-comment";
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('CommentModule.comment', 'Comments list'),
                'url' => ['/comment/commentBackend/index'],
            ],
        ];
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/comment/commentBackend/index';
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $import = ['application.modules.comment.models.*'];

        foreach (Yii::app()->getModules() as $module => $data) {
            $import[] = "application.modules.{$module}.models.*";
            $import[] = "application.modules.{$module}.listeners.*";
        }

        $this->setImport($import);

        $this->defaultCommentStatus = Comment::STATUS_NEED_CHECK;
    }
}
