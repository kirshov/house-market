<?php
use webforma\widgets\WPurifier;

/**
 * Comment model class:
 *
 * This is the model class for table "Comment".
 *
 * @const    int STATUS_APPROVED    - Принят
 * @const    int STATUS_NEED_CHECK  - Проверка
 * @const    int STATUS_SPAM        - Спам
 *
 * @var      public $verifyCode - капча
 *
 * The followings are the available columns in table 'Comment':
 * @property string $id
 * @property string $model
 * @property string $model_id
 * @property string $create_time
 * @property string $name
 * @property string $email
 * @property string $url
 * @property string $text
 * @property integer $status
 * @property string $ip
 * @property string $user_id
 * @property integer $parent_id
 * @property integer $is_backend
 * @property integer $rating
 *
 */
class Comment extends webforma\models\WModel
{
    /**
     *
     */
    const STATUS_NEED_CHECK = 0;

    /**
     *
     */
    const STATUS_APPROVED = 1;

    /**
     *
     */
    const STATUS_SPAM = 2;

    /**
     * @var
     */
    public $verifyCode;

    /**
     * @var
     */
    public $spamField;

    /**
     * @var
     */
    public $comment;

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className - инстанс модели
     *
     * @return Comment the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Имя таблицы в БД:
     *
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{comment_comment}}';
    }

    /**
     * Список правил для валидации полей модели:
     *
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        $module = Yii::app()->getModule('comment');

        return [
            ['model, name, email, text, url, is_backend, admin_text', 'filter', 'filter' => 'trim'],
            ['model, name, email, url', 'filter', 'filter' => [$obj = new WPurifier(), 'purify']],
            ['text', 'purifyText'],
            ['model, model_id, text', 'required'],
			['name, email', 'required', 'on' => 'user'],
            ['status, user_id, model_id, parent_id', 'numerical', 'integerOnly' => true],
            ['rating', 'numerical', 'min' => 0, 'max' => 5,'integerOnly' => true],
            ['name, email, url, comment', 'length', 'max' => 150],
            ['model', 'length', 'max' => 100],
            ['ip', 'length', 'max' => 20],
            ['email', 'email'],
            ['url', 'webforma\components\validators\YUrlValidator', 'defaultScheme' => 'http'],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            [
                'verifyCode',
                'webforma\components\validators\YRequiredValidator',
                'allowEmpty' => !$module->showCaptcha || Yii::app()->getUser()->isAuthenticated()
            ],
            [
                'verifyCode',
                'captcha',
                'allowEmpty' => !$module->showCaptcha || Yii::app()->getUser()->isAuthenticated(),
                'captchaAction' => '/comment/comment/captcha',
            ],
            [
                'id, model, model_id, create_time, name, email, url, text, status, ip, parent_id',
                'safe',
                'on' => 'search'
            ],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function purifyText($attribute, $params)
    {
        $module = Yii::app()->getModule('comment');
        $p = new CHtmlPurifier();
        $p->options = [
            'HTML.Allowed' => $module->allowedTags,
        ];
        $this->$attribute = $p->purify($this->$attribute);
    }

    /**
     * Список атрибутов для меток формы:
     *
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('CommentModule.comment', 'ID'),
            'model' => 'Запись',
            'model_id' => 'Id записи',
            'create_time' => Yii::t('CommentModule.comment', 'Created at'),
            'name' => Yii::t('CommentModule.comment', 'Name'),
            'email' => Yii::t('CommentModule.comment', 'Email'),
            'url' => Yii::t('CommentModule.comment', 'Site'),
            'text' => Yii::t('CommentModule.comment', 'Comment'),
            'status' => Yii::t('CommentModule.comment', 'Status'),
            'verifyCode' => Yii::t('CommentModule.comment', 'Verification code'),
            'ip' => Yii::t('CommentModule.comment', 'IP address'),
            'parent_id' => Yii::t('CommentModule.comment', 'Parent'),
            'phone' => 'Телефон',
            'admin_text' => 'Ответ администрации',
            'rating' => 'Оценка',
        ];
    }

    /**
     * Список связей данной таблицы:
     *
     * @return mixed список связей
     **/
    public function relations()
    {
        return [
            'author' => [self::BELONGS_TO, 'User', 'user_id'],
        ];
    }

    /**
     * Получение группы условий:
     *
     * @return mixed список условий
     **/
    public function scopes()
    {
        return [
            'new' => [
                'condition' => 't.status = :status',
                'params' => [':status' => self::STATUS_NEED_CHECK],
            ],
            'approved' => [
                'condition' => 't.status = :status',
                'params' => [':status' => self::STATUS_APPROVED],
                'order' => 't.create_time DESC',
            ],
            'authored' => [
                'condition' => 't.user_id is not null',
            ],
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria();

        $criteria->compare('id', $this->id, true);
        $criteria->compare('model', $this->model, true);
        $criteria->compare('model_id', $this->model_id);
        $criteria->compare('parent_id', $this->parent_id);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('ip', $this->ip, true);
        $criteria->compare('is_backend', 0);

        return new CActiveDataProvider(get_class($this), [
            'criteria' => $criteria,
            'sort' => [
                'defaultOrder' => 'id DESC',
            ]
        ]);
    }

    /**
     * Событие выполняемое перед сохранением модели
     *
     * @return parent::beforeSave()
     **/
    public function beforeSave()
    {
        if ($this->getIsNewRecord()) {
            $this->create_time = new CDbExpression('NOW()');
            $this->ip = Yii::app()->getRequest()->userHostAddress;
        }

        return parent::beforeSave();
    }

    /**
     * Событие, которое вызывается после сохранения модели:
     *
     * @return parent::afterSave()
     **/
    public function afterSave()
    {
        Yii::app()->eventManager->fire(
            CommentEvents::AFTER_SAVE_COMMENT,
            new CommentEvent($this, Yii::app()->getUser(), Yii::app()->getModule('comment'))
        );

        return parent::afterSave();
    }

    /**
     * Получение списка статусов:
     *
     * @return mixed список статусов
     **/
    public function getStatusList()
    {
        return [
            self::STATUS_APPROVED => Yii::t('CommentModule.comment', 'Accepted'),
            self::STATUS_NEED_CHECK => Yii::t('CommentModule.comment', 'Check'),
            self::STATUS_SPAM => Yii::t('CommentModule.comment', 'Spam'),
        ];
    }

    /**
     * Получение статуса по заданному:
     *
     * @return string текст статуса
     **/
    public function getStatus()
    {
        $list = $this->getStatusList();

        return isset($list[$this->status]) ? $list[$this->status] : Yii::t('CommentModule.comment', 'Unknown status');
    }

    /**
     * Получаем автора:
     *
     * @return Comment->author || bool false
     **/
    public function getAuthorName()
    {
        return ($this->author) ? $this->author->nick_name : $this->name;
    }

    /**
     * @param int $size
     * @param array $params
     * @return string
     */
    public function getAuthorAvatar($size = 32, array $params = ['width' => 32, 'height' => 32])
    {
        return false;
        if ($this->author) {
            return CHtml::image($this->author->getAvatar((int)$size), $this->author->nick_name, $params);
        }

        return CHtml::image(Yii::app()->getModule('user')->defaultAvatar, $this->name, $params);
    }

    /**
     * @return string
     */
    public function getText()
    {
        return (Yii::app()->getModule('comment')->stripTags)
            ? strip_tags($this->text)
            : $this->text;
    }

    /**
     * @param array $with
     * @return array|mixed|null|string|static
     */
    public function getTarget(array $with = [])
    {
        if (!class_exists($this->model)) {
            return $this->model;
        }

        $model = CActiveRecord::model($this->model);

        $model = $model->with($with)->findByPk($this->model_id);

        if (null === $model) {
            return $this->model;
        }

        return $model;
    }

    /**
     * @return string
     */
    public function getTargetTitle()
    {
        $target = $this->getTarget();

        if (is_object($target)) {
            return $target->getTitle();
        }

        return $this->model;
    }

    /**
     * @param array|null $options
     * @return array|Comment|mixed|null|string
     */
    public function getTargetTitleLink(array $options = null)
    {
        $target = $this->getTarget();

        if (is_object($target)) {
            return CHtml::link($target->getTitle(), $target->getLink(), $options);
        }

        return $target;
    }

	/**
	 * @return string
	 */
    public function getTargetLink(){
	    $target = $this->getTarget();

	    if (is_object($target)) {
		    return $target->getLink();
	    }
	    return '';
    }

    /**
     * @return bool
     */
    public function isApproved()
    {
        return $this->status == self::STATUS_APPROVED;
    }

    /**
     * @return int
     */
    public function hasParent()
    {
        return $this->parent_id;
    }

    /**
     * @param int $cache
     * @return array|mixed|null
     */
    public function getParent($cache = 3600)
    {
        return $this->cache((int)$cache)->findByPk($this->parent_id);
    }

    /**
     * @param array $items
     * @return int
     * @throws CDbException
     */
    public function multiDelete(array $items)
    {
        $count = 0;

        $transaction = Yii::app()->getDb()->beginTransaction();

        try {
            $items = array_filter($items, 'intval');

            $models = $this->findAllByPk($items);

            foreach ($models as $model) {

                Yii::app()->eventManager->fire(
                    CommentEvents::AFTER_DELETE_COMMENT,
                    new CommentEvent($model, Yii::app()->getUser(), Yii::app()->getModule('comment'))
                );

	            $count += (int)$model->delete();
            }

            $transaction->commit();

        } catch (Exception $e) {
            $transaction->rollback();
            Yii::log($e->__toString(), CLogger::LEVEL_ERROR);
        }

        return $count;
    }

    /**
     * @return mixed
     */
    public function approve()
    {
        $this->status = self::STATUS_APPROVED;
        return $this->save();
    }

	/**
	 * @return array
	 */
	public static function getRatingRanges(){
		return [
			0 => 'Нет оценки',
			5 => 'Отлично',
			4 => 'Хорошо',
			3 => 'Средне',
			2 => 'Плохо',
			1 => 'Ужасно',
		];
	}

	/**
	 * @return bool|mixed
	 */
	public function getRatingTitle(){
		if(!$this->rating){
			return 'Нет оценки';
		}
		$data = self::getRatingRanges();
		return isset($data[$this->rating]) ? $data[$this->rating] : 'Нет оценки';
	}

	/**
	 * @return mixed
	 */
	public function getRating(){
		return $this->rating;
	}

	/**
	 * @return string
	 */
	public function getDate(){
		return date('d.m.Y', strtotime($this->create_time));
	}
}
