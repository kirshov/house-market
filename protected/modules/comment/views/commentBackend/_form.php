<?php
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
	    'id' => 'comment-form',
	    'enableAjaxValidation' => false,
	    'enableClientValidation' => false,
	    'type' => 'vertical',
	    'htmlOptions' => ['class' => 'well sticky'],
	    'clientOptions' => [
		    'validateOnSubmit' => true,
	    ],
    ]
); ?>
<div class="alert alert-info">
    <?=  Yii::t('CommentModule.comment', 'Fields with'); ?>
    <span class="required">*</span>
    <?=  Yii::t('CommentModule.comment', 'are require.'); ?>
</div>

<?=  $form->errorSummary($model); ?>

<?if(DEVMODE):?>
	<div class='row'>
		<div class="col-sm-6">
			<?=  $form->textFieldGroup($model, 'model'); ?>
		</div>
	</div>
	<div class='row'>
		<div class="col-sm-6">
			<?=  $form->textFieldGroup($model, 'model_id'); ?>
		</div>
	</div>
<?endif;?>
<div class='row'>
    <div class="col-sm-6">
        <?=  $form->textFieldGroup($model, 'name'); ?>
    </div>
</div>
<div class='row'>
    <div class="col-sm-6">
        <?=  $form->textFieldGroup($model, 'email'); ?>
    </div>
</div>

<div class='row'>
    <div class="col-sm-6">
        <?=  $form->dropDownListGroup($model, 'status', [
			'widgetOptions' => [
				'data' => $model->getStatusList(),
			],
		]); ?>
    </div>
</div>


<div class='row'>
	<div class="col-sm-6">
		<?=  $form->dropDownListGroup($model, 'rating', [
			'widgetOptions' => [
				'data' => ['--не выбрано--'] + Comment::getRatingRanges(),
			]
		]); ?>
	</div>
</div>
<div class='row'>
	<div class="col-sm-6">
		<?= $form->dateTimePickerGroup($model, 'create_time', [
			'widgetOptions' => [
				'options' => [
					'format' => 'dd.mm.yyyy hh:ii:ss',
					'weekStart' => 1,
					'autoclose' => true,
				],
			],
			'prepend' => '<i class="fa fa-calendar"></i>',
		]);?>
	</div>
</div>

<div class='row'>
	<div class="col-sm-12">
		<?=  $form->labelEx($model, 'text'); ?>
		<?php $this->widget($this->module->getVisualEditor(), [
			'model'     => $model,
			'attribute' => 'text',
		]); ?>
	</div>
</div>
<br/>
<div class='row'>
	<div class="col-sm-12">
		<?=  $form->labelEx($model, 'admin_text'); ?>
		<?php $this->widget($this->module->getVisualEditor(), [
			'model'     => $model,
			'attribute' => 'admin_text',
		]); ?>
	</div>
</div>

<div class="buttons">
	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context'    => 'primary',
		'label'      => $model->isNewRecord
			? Yii::t('CommentModule.comment', 'Create comment and continue')
			: Yii::t('CommentModule.comment', 'Save comment and continue'),
	]); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType'  => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label'       => $model->isNewRecord
			? Yii::t('CommentModule.comment', 'Create comment and close')
			: Yii::t('CommentModule.comment', 'Save comment and close'),
	]); ?>
</div>

<?php $this->endWidget(); ?>
