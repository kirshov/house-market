function commentSendForm(form, data, hasError) {
    if (hasError) {
        return false;
    }

    var resultField = form.find('.result-message');
    resultField
        .html('')
        .removeClass('bs-callout-danger')
        .removeClass('bs-callout-success')
        .addClass('hidden');

    $.ajax({
        url: form.attr('action'),
        type: 'POST',
        data: form.serialize(),
        dataType: 'json',
        success: function (response) {
            if (response.result) {
                form.trigger('reset');
            }
            if(response.result){
                resultField.html('<p>' + response.data.message + '</p>');
                if(response.data.commentContent){
                    form.closest('.comments__body').find('.comment-items').append(response.data.commentContent);
                    form.closest('.comments__body').find('.empty').addClass('hidden');
                }

                resultField.addClass('bs-callout-success');
            } else {
                if(response.data && response.data.message){
                    resultField.append(response.data.message);
                } else if(typeof response == 'object' && Object.keys(response).length){
                    resultField.append('<ul></ul>');
                    $.each(response, function(index, value){
                        $.each(value, function(key, item){
                            resultField.find('ul').append('<li>' + item + '</li>');
                            if(index == 'Comment_verifyCode'){
                                form.find('.image-captcha').trigger('click');
                                $('#Comment_verifyCode').html('');
                            }
                        });
                    });
                }
                resultField.addClass('bs-callout-danger');
            }
            resultField.removeClass('hidden');
        },
        error: function () {
            resultField
                .html('Извините, при обработке запроса возникла ошибка')
                .addClass('bs-callout-danger')
                .removeClass('hidden');
        }
    });
    form.find('button[type="submit"]').prop('disabled', false);
    return false;
}