<?php

/**
 * Class PanelCommentStatWidget
 */
class PanelCommentStatWidget extends \webforma\widgets\WWidget
{
    /**
     * @throws CException
     */
    public function run()
    {
        $criteria = new CDbCriteria();

        $dataProvider = new CActiveDataProvider('Comment', [
            'criteria' => $criteria,
            'sort' => [
                'defaultOrder' => 'id DESC',
            ],
            'pagination' => [
                'pageSize' => (int)$this->limit,
            ],
        ]);

        $cacheTime = Yii::app()->controller->webforma->coreCacheTime;

        $this->render(
            'panel-stat',
            [
                'commentsCount' => Comment::model()->cache($cacheTime)->count(
                    'create_time >= :time',
                    [':time' => time() - 24 * 60 * 60]
                ),
                'allCommentsCnt' => Comment::model()->cache($cacheTime)->all()->count(),
                'newCnt' => Comment::model()->cache($cacheTime)->new()->count(),
                'dataProvider' => $dataProvider,
            ]
        );
    }
}
