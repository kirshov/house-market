<?php

/**
 * Виджет для вывода последних комментариев
 *
 **/
class LastCommentsWidget extends webforma\widgets\WWidget
{
    /**
     * @var
     */
    public $model;
    /**
     * @var
     */
    public $commentStatus;
    /**
     * @var int
     */
    public $limit = 10;
    /**
     * @var bool
     */
    public $onlyWithAuthor = true;
    /**
     * @var string
     */
    public $view = 'lastcomments';

    /**
     *
     */
    public function init()
    {
        if ($this->model) {
            $this->model = is_object($this->model) ? get_class($this->model) : $this->model;
        }

        $this->commentStatus || ($this->commentStatus = Comment::STATUS_APPROVED);
    }

    /**
     * @throws CException
     */
    public function run()
    {
        $criteria = new CDbCriteria([
            'condition' => 'status = :status',
            'params' => [':status' => $this->commentStatus],
            'limit' => $this->limit,
            'order' => 'id DESC',
        ]);

        if ($this->model) {
            $criteria->addCondition('model = :model');
            $criteria->params[':model'] = $this->model;
        }

        if ($this->onlyWithAuthor) {
            $criteria->addCondition('user_id is not null');
        }

        $comments = Comment::model()->findAll($criteria);

        $this->render($this->view, ['models' => $comments]);
    }
}
