<?php
Yii::import('application.modules.comment.CommentModule');

/**
 * Comment Widget
 **/
class CommentsWidget extends webforma\widgets\WWidget
{
    /**
     * @var CActiveRecord
     */
    public $model;
    /**
     * @var
     */
    public $redirectTo;
    /**
     * @var
     */
    public $status;
    /**
     * @var bool
     */
    public $showComments = true;
    /**
     * @var bool
     */
    public $showForm = true;
    /**
     * @var bool
     */
    public $allowGuestComment = false;
    /**
     * @var string
     */
    public $view = 'comments';

	/**
	 * @var bool
	 */
    public $isBackend = false;

    /**
     * @throws CException
     */
    public function init()
    {
        if (null === $this->model && empty($this->comments)) {
            throw new CException(
                Yii::t(
                    'CommentModule.comment',
                    'Please, set "model" property for "{widget}" widget!',
                    [
                        '{widget}' => get_class($this),
                    ]
                )
            );
        }

        if (!$this->showComments && !$this->showForm) {
            throw new CException(
                Yii::t(
                    'CommentModule.comment',
                    'Nothing to show. Please, set "showForm" or "showComments" property to true.'
                )
            );
        }

        if (null === $this->status) {
            $this->status = Comment::STATUS_APPROVED;
        }

        $this->allowGuestComment = Yii::app()->getModule('comment')->allowGuestComment;
        $this->generateTokens();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $model = new Comment('user');
        $model->setAttributes([
            'model' => get_class($this->model),
            'model_id' => $this->model->id,
        ]);

        $this->render($this->view, [
            'comments' => $this->getComments(),
            'model' => $model,
            'redirectTo' => $this->redirectTo,
            'spamField' => Yii::app()->getUser()->getState('spamField'),
            'spamFieldValue' => Yii::app()->getUser()->getState('spamFieldValue'),
            'module' => Yii::app()->getModule('comment'),
        ]);
    }

    /**
     * @return bool
     */
    public function isAllowed()
    {
        return $this->allowGuestComment || !Yii::app()->getUser()->getIsGuest();
    }

    /**
     * @return array
     */
    protected function getComments()
    {
        if ($this->showComments) {
	        $dbCriteria = new CDbCriteria([
		        'condition' => 't.model = :model AND t.model_id = :modelId AND t.status = :status AND t.is_backend = :is_backend',
		        'params' => [
			        ':model' => get_class($this->model),
			        ':modelId' => (int)$this->model->id,
			        ':status' => (int)$this->status,
			        ':is_backend' => (int)$this->isBackend,
		        ],
		        'order' => 't.create_time',
		        'with' => ['author'],
	        ]);

	        return new CActiveDataProvider('Comment', [
		        'criteria' => $dbCriteria,
		        'pagination' => [
			        'pageSize' => (int)Yii::app()->getModule('comment')->perPage,
			        'pageVar' => 'page',
		        ],
	        ]);
        }

        return false;
    }

    /**
     * @return string
     */
    protected function getCacheKey()
    {
        $class = get_class($this->model);

        return "Comment{$class}{$this->model->id}";
    }

    /**
     *
     */
    protected function generateTokens()
    {
        Yii::app()->getUser()->setState('spamField', md5(Yii::app()->userManager->hasher->generateRandomToken(8)));
        Yii::app()->getUser()->setState('spamFieldValue', md5(Yii::app()->userManager->hasher->generateRandomToken(8)));
    }
}
