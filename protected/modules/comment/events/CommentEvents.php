<?php

/**
 * Class CommentEvents
 */
class CommentEvents
{
    /**
     *
     */
    const BEFORE_ADD_COMMENT = 'comment.before.add';

    /**
     *
     */
    const SUCCESS_ADD_COMMENT = 'comment.add.success';

    /**
     *
     */
    const AFTER_SAVE_COMMENT = 'comment.after.save';

    /**
     *
     */
    const AFTER_DELETE_COMMENT = 'comment.after.delete';
}
