<?php

/**
 * Comment controller class
 * Класс для обработки комментариев на фронт-части.
 *
 * @tutorial для ajax-варианта добавления комментария:
 *            $.ajax({
 *                type: 'post',
 *                url: $(<comment_form>).attr('action'),
 *                data: $(<comment_form>).serialize(),
 *                success: function (data) {
 *                    // Обработка нормального состояния
 *                },
 *                error: function (data) {
 *                    // Обработка ошибки
 *                },
 *                dataType: 'json'
 *            });
 * @return   для ajax-варианта добавления комментария:
 *              {
 *                  "result": <result>,                                 // boolean
 *                  "data": {
 *                      "message": "<сообщение>",                       // string
 *                      "comment": {
 *                          "parent_id": <parent_id>                    // int
 *                      },
 *                      "commentContent": "<отрисованный комментарий>"  // string
 *                  }
 *              }
 *
 *
 **/
class CommentController extends \webforma\components\controllers\FrontController
{
    /**
     * Объявляем действия:
     *
     * @return mixed actions
     **/
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'webforma\components\actions\WCaptchaAction',
                'backColor' => 0xFFFFFF,
                'testLimit' => 1
            ],
            'ajaxImageUpload' => [
                'class' => 'webforma\components\actions\WAjaxImageUploadAction',
                'maxSize' => $this->module->maxSize,
                'mimeTypes' => $this->module->mimeTypes,
                'types' => $this->module->allowedExtensions,
            ],
        ];
    }

    /**
     * Action добавления комментария
     *
     *
     **/
    public function actionAdd()
    {
    	$request = Yii::app()->getRequest();
        if (!$request->getIsPostRequest() || !$request->getPost('Comment')) {
            throw new CHttpException(404);
        }

        $module = Yii::app()->getModule('comment');

        if (!$module->allowGuestComment && !Yii::app()->getUser()->isAuthenticated()) {
            throw new CHttpException(404);
        }

        if ($request->getIsAjaxRequest() && Yii::app()->getRequest()->getPost('ajax') == 'comment-form') {
            $validate = CActiveForm::validate(new Comment());
            if($validate && $validate != '[]'){
            	echo $validate;
	            Yii::app()->end();
            }
        }


		$redirect = Yii::app()->getRequest()->getPost('redirectTo', Yii::app()->getUser()->getReturnUrl());
		if($request->getPost('Comment')){
			$params = Yii::app()->getRequest()->getPost('Comment');

			if (Yii::app()->getUser()->isAuthenticated()) {
				$params = CMap::mergeArray($params, [
					'user_id' => Yii::app()->getUser()->getId(),
				]);
			}

			$comment = new Comment($params['is_admin'] ? 'admin' : 'user');
			$comment->setAttributes($params);
			$comment->status = (int)$module->defaultCommentStatus;
			if ($module->autoApprove && Yii::app()->getUser()->isAuthenticated()) {
				$comment->status = Comment::STATUS_APPROVED;
			}

			if ($comment->save()) {
				$commentContent = $comment->isApproved() ? $this->_renderComment($comment) : '';
				if (Yii::app()->getRequest()->getIsAjaxRequest()) {
					Yii::app()->ajax->success([
						'message' => $comment->isApproved()
							? 'Ваш комментарий успешно добавлен!'
							: 'Ваш комментарий успешно добавлен! Он будет размещен после одобрения администратором.',
						'comment' => [
							'parent_id' => $comment->parent_id,
						],
						'commentContent' => $commentContent,
					]);
				} else {
					Yii::app()->getUser()->setFlash(
						webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
						Yii::t('CommentModule.comment', 'You record was created. Thanks.')
					);

					$this->redirect($redirect);
				}
			} else {
				if (Yii::app()->getRequest()->getIsAjaxRequest()) {
					Yii::app()->ajax->rawText(CActiveForm::validate($comment));
				} else {
					Yii::app()->getUser()->setFlash(webforma\widgets\WFlashMessages::ERROR_MESSAGE, 'Ошибка!');

					$this->redirect($redirect);
				}
			}
		}
	}


    /**
     * Отрисовка комментария:
     *
     * @param Comment $comment - комментарий
     *
     * @return string html отрисованного комментария
     **/
    private function _renderComment(Comment $comment)
    {
        $comment->refresh();
	    $view = Yii::app()->getRequest()->getPost('view') ? Yii::app()->getRequest()->getPost('view') : '_comment';
	    $viewData = Yii::app()->getRequest()->getPost('viewData') ? Yii::app()->getRequest()->getPost('viewData') : '';

	    $viewDataArray = [];
	    if($viewData){
		    $viewDataArray = json_decode($viewData, true);
		    if(!is_array($viewDataArray)){
			    $viewDataArray = [];
		    }
	    }

        return $this->renderPartial($view, CMap::mergeArray([
	        'data' => $comment,
        ], $viewDataArray), true);
    }
}
