<?php

/**
 * CommentBackendController контроллер для управления комментариями в панели управления
 */
class CommentBackendController extends webforma\components\controllers\BackController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Comment.CommentBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Comment.CommentBackend.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['Comment.CommentBackend.Create']],
            ['allow', 'actions' => ['update', 'inline', 'approve'], 'roles' => ['Comment.CommentBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Comment.CommentBackend.Delete']],
            ['deny']
        ];
    }

    /**
     * @throws CHttpException
     */
    public function actionInline()
    {
        if (!Yii::app()->getRequest()->getIsAjaxRequest() || !Yii::app()->getRequest()->getIsPostRequest()) {
            throw new CHttpException(404);
        }

        $name = Yii::app()->getRequest()->getPost('name');
        $value = Yii::app()->getRequest()->getPost('value');
        $pk = (int)Yii::app()->getRequest()->getPost('pk');

        if (!isset($name, $value, $pk)) {
            throw new CHttpException(404);
        }

        if (!in_array($name, ['status'])) {
            throw new CHttpException(404);
        }

        $model = Comment::model()->findByPk($pk);

        if (null === $model) {
            throw new CHttpException(404);
        }

        $model->$name = $value;

        if ($model->saveNode()) {
            Yii::app()->ajax->success();
        }

        throw new CHttpException(500, $model->getError($name));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $oldAdminText = $model->admin_text;

        Yii::app()->getCache()->delete("Comment{$model->model}{$model->model_id}");

        if (($data = Yii::app()->getRequest()->getPost('Comment')) !== null) {

            $model->setAttributes($data);

            if ($model->save()) {
                Yii::app()->getUser()->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('CommentModule.comment', 'Comment was updated!')
                );

                if(!$oldAdminText && $model->admin_text){
	                Yii::app()->getComponent('commentNotifyService')->sendReplyUserNotify($model);
                }

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['update', 'id' => $model->id]
                    )
                );
            }
        }
        $this->render('update', ['model' => $model]);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            $model = $this->loadModel($id);

            Yii::app()->getCache()->delete("Comment{$model->model}{$model->model_id}");

            $model->delete();

            if(Yii::app()->getRequest()->getIsAjaxRequest()){
	            Yii::app()->ajax->success();
            } else {
	            Yii::app()->getRequest()->getParam('ajax') !== null || $this->redirect(
		            (array)Yii::app()->getRequest()->getPost('returnUrl', 'index')
	            );
            }
        } else {
            throw new CHttpException(400, Yii::t(
                'CommentModule.comment',
                'Bad request. Please don\'t repeate similar requests anymore'
            ));
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $model = new Comment('search');

        $model->unsetAttributes(); // clear any default values

		$model->setAttributes(Yii::app()->getRequest()->getParam('Comment', []));

        $this->render('index', ['model' => $model]);
    }

    /**
     * @throws CHttpException
     */
    public function actionMultiaction()
    {
        if (!Yii::app()->getRequest()->getIsAjaxRequest() || !Yii::app()->getRequest()->getIsPostRequest()) {
            throw new CHttpException(404);
        }

        $items = Yii::app()->getRequest()->getPost('items');

        if (!is_array($items) || empty($items)) {
            Yii::app()->ajax->success();
        }

        if ($count = Comment::model()->multiDelete($items)) {

            Yii::app()->ajax->success(
                Yii::t(
                    'WebformaModule.webforma',
                    'Removed {count} records!',
                    [
                        '{count}' => $count
                    ]
                )
            );
        } else {
            Yii::app()->ajax->failure(
                Yii::t('WebformaModule.webforma', 'There was an error when processing the request')
            );
        }
    }

    /**
     * @throws CHttpException
     */
    public function actionApprove()
    {
        if (!Yii::app()->getRequest()->getIsAjaxRequest() || !Yii::app()->getRequest()->getIsPostRequest()) {
            throw new CHttpException(404);
        }

        if ($items = Yii::app()->getRequest()->getPost('items')) {
            if (Yii::app()->commentManager->multiApprove($items)) {
                Yii::app()->ajax->success();
            } else {
                Yii::app()->ajax->failure();
            }
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     * @return Comment
     */
    public function loadModel($id)
    {
        $model = Comment::model()->findByPk((int)$id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('CommentModule.comment', 'Requested page was not found!'));
        }

        return $model;
    }
}
