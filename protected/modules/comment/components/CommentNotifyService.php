<?php

class CommentNotifyService extends NotifyService
{

	/**
	 * @var string
	 */
	protected $templatePath  = 'comment/email';

	/**
	 * @var string
	 */
	protected $messageComponent = 'commentMailMessage';

	/**
	 * @param Comment $model
	 * @return bool
	 */
	public function sendCreatedAdminNotify(Comment $model)
	{
		$from = Yii::app()->getModule('webforma')->notifyEmailFrom;
		$to = Yii::app()->getModule('webforma')->notifyEmailTo;

		if(!$to || !$from){
			return false;
		}

		$mailContent = $this->getMailContent($model, 'newCommentAdmin', true);

		if($mailContent['subject'] && $mailContent['body']){
			foreach (explode(',', $to) as $email) {
				$this->mail->send($from, $email, $mailContent['subject'], $mailContent['body']);
			}
		}

		return true;
	}

	/**
	 * @param Comment $model
	 * @return bool
	 */
	public function sendReplyUserNotify(Comment $model)
	{
		if(!$model->email){
			return false;
		}
		$from = Yii::app()->getModule('webforma')->notifyEmailFrom;
		$to = $model->email;

		if(!$to || !$from){
			return false;
		}

		$mailContent = $this->getMailContent($model, 'replyCommentUser', true);

		if($mailContent['subject'] && $mailContent['body']){
			foreach (explode(',', $to) as $email) {
				$this->mail->send($from, $email, $mailContent['subject'], $mailContent['body']);
			}
		}

		return true;
	}

}