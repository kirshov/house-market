<?php
class CommentMailMessage extends WMailMessage{
	/**
	 * @var Comment
	 */
	protected $model;

	/**
	 * @var string
	 */
	protected $module = 'review';

	public function getDefaultMessages()
	{
		return [
			'newCommentAdmin' => [
				'name' => 'Сообщение администратору о новом комментарии',
				'subject' => 'Оставлен новый комментарий на сайте "%siteName%"',
				'body' => '<h2 style="'.$this->getStyle('title').'">Новый комментарий</h2>
					<h3 style="'.$this->getStyle('subtitle').'">на сайте "%siteName%"</h3>
					<p style="font: %font%">
						Отправитель: %autor%<br/>
						Оценка: %rating%<br/>
						Текст: %text%<br/>
					</p>
				',
			],

			'replyCommentUser' => [
				'name' => 'Сообщение пользователю об ответе на комментарий',
				'subject' => 'Получен ответ на ваш комментарий на сайте "%siteName%"',
				'body' => '<h2 style="'.$this->getStyle('title').'">Ответ на Ваш комментарий</h2>
					<h3 style="'.$this->getStyle('subtitle').'">на сайте "%siteName%"</h3>
					<p style="font: %font%">
						Текст ответа: %answer%<br/>
					</p>
					<div align="center">%link%</div>
				',
			],
		];
	}

	/**
	 * @param $code
	 * @return mixed|string
	 */
	public function getContentBlock($code)
	{
		switch ($code){
			case '%autor%':
				return CHtml::encode($this->model->name);
			case '%rating%':
				return CHtml::encode($this->model->getRatingTitle());
			case '%link%':
				return $this->getModelLink();
			case '%text%':
				return CHtml::encode($this->model->text);
			case '%answer%':
				return strip_tags($this->model->admin_text);
			default:
				return parent::getContentBlock($code);
		}
	}

	/**
	 * @return string
	 */
	public function getModelLink(){
		return '<a href="'.Yii::app()->createAbsoluteUrl($this->model->getTargetLink()).'#tab-comments" style="'.$this->getStyle('btn').'">
			Перейти к комментариям
		</a>';
	}
}