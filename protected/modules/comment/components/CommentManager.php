<?php

use webforma\models\WModel;

/**
 * Class CommentManager
 */
class CommentManager extends CApplicationComponent
{
    /**
     * @param array $items
     * @return bool
     */
    public function multiApprove(array $items)
    {
        $transaction = Yii::app()->getDb()->beginTransaction();

        try {
            $models = Comment::model()->findAllByPk($items);

            foreach ($models as $model) {
                $model->approve();
            }

            $transaction->commit();

            return true;

        } catch (Exception $e) {
            $transaction->rollback();

            Yii::log($e->__toString(), CLogger::LEVEL_ERROR);

            return false;
        }
    }
}
