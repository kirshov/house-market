<?php
/**
 *
 * Файл конфигурации модуля
 *
 **/
return [
    'module' => [
        'class' => 'application.modules.comment.CommentModule',
        /*'panelWidgets' => [
            'application.modules.comment.widgets.PanelCommentStatWidget' => [
                'limit' => 5
            ]
        ],*/
    ],
    'import' => [
        'application.modules.comment.models.*',
        'application.modules.comment.events.*',
        'application.modules.comment.listeners.*',
        'application.modules.blog.models.*',
        'vendor.yiiext.nested-set-behavior.NestedSetBehavior',
    ],
    'component' => [
        'commentManager' => [
            'class' => 'application.modules.comment.components.CommentManager',
        ],
        'eventManager' => [
            'class' => 'webforma\components\EventManager',
            'events' => [
                'comment.add.success' => [
                    ['NewCommentListener', 'onSuccessAddComment']
                ],
                'comment.before.add' => [
                    ['NewCommentListener', 'onBeforeAddComment']
                ],
                'comment.after.save' => [
                    ['NewCommentListener', 'onAfterSaveComment']
                ],
                'comment.after.delete' => [
                    ['NewCommentListener', 'onAfterDeleteComment']
                ]
            ]
        ],
	    'commentNotifyService' => [
		    'class' => 'application.modules.comment.components.CommentNotifyService',
	    ],
	    'commentMailMessage' => [
		    'class' => 'application.modules.comment.components.CommentMailMessage',
	    ],
    ],
	'params' => [
		'widgets' => [
			'comments' => 'application.modules.comment.widgets.CommentsWidget',
		],
	],
    'rules' => [
        '/comment/comment/captcha/refresh/<v>' => 'comment/comment/captcha/refresh/',
        '/comment/comment/captcha/<v>' => 'comment/comment/captcha/',
        '/comment/add/' => 'comment/comment/add/',
    ],
];
