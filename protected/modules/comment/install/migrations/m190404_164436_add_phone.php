<?php

class m190404_164436_add_phone extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{comment_comment}}', 'rating', 'tinyint(3) default null');
		$this->addColumn('{{comment_comment}}', 'admin_text', 'text default null');
		$this->addColumn('{{comment_comment}}', 'phone', 'varchar(150) default null');
	}

	public function safeDown()
	{

	}
}