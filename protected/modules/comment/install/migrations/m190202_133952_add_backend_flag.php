<?php

class m190202_133952_add_backend_flag extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{comment_comment}}', 'is_backend', 'tinyint(1) DEFAULT 0');
		$this->createIndex('ix_{{comment_comment}}_is_backend', '{{comment_comment}}', "is_backend", false);
	}

	public function safeDown()
	{

	}
}