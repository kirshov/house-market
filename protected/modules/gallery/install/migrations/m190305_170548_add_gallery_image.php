<?php

class m190305_170548_add_gallery_image extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{gallery_gallery}}', 'image', 'varchar(255) default null');
		$this->addColumn('{{gallery_gallery}}', 'slug', 'varchar(255) default null');
		$this->addColumn('{{gallery_gallery}}', 'position', 'int(11) default 0');
	}

	public function safeDown()
	{

	}
}