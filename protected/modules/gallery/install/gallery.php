<?php
/**
 * Файл конфигурации модуля
 **/
return [
    'module' => [
        'class' => 'application.modules.gallery.GalleryModule',
    ],
    'import' => [],
    'component' => [],
	'params'    => [
		'widgets'   => [
			'gallery' => 'application.modules.gallery.widgets.GalleryWidget',
		],
	],
    'rules' => [
    	//'gallery' => '/gallery/gallery/gallery',
        '/gallery' => '/gallery/gallery/index',
        '/gallery/<id:\d+>' => '/gallery/gallery/view',
        /*'/albums/images/<id:\d+>' => '/gallery/gallery/image',
        '/albums/categories' => '/gallery/galleryCategory/index',
        '/albums/<slug>' => '/gallery/galleryCategory/view',*/
    ],
];
