<?php
use webforma\widgets\WPurifier;

/**
 * Gallery
 *
 * Модель для работы с галереями
 *
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string $image
 * @property integer $status
 * @property integer $preview_id
 * @property integer $category_id
 * @property integer $position
 *
 * @property Image $preview
 *
 * @property Gallery $published
 * @property Gallery $first
 */
class Gallery extends webforma\models\WModel
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 0;

    /**
     * Returns the static model of the specified AR class.
     * @param  string $className
     * @return Gallery the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{gallery_gallery}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['name', 'filter', 'filter' => [new WPurifier(), 'purify']],
            ['name', 'required'],
            ['status, owner, preview_id, category_id, position', 'numerical', 'integerOnly' => true],
            ['name', 'length', 'max' => 250],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            ['id, name, description, status, owner', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'imagesRell' => [self::HAS_MANY, 'ImageToGallery', ['gallery_id' => 'id']],
            'images' => [self::HAS_MANY, 'Image', 'image_id', 'through' => 'imagesRell'],
            'imagesCount' => [self::STAT, 'ImageToGallery', 'gallery_id'],
            'user' => [self::BELONGS_TO, 'User', 'owner'],
            'lastUpdated' => [self::STAT, 'ImageToGallery', 'gallery_id', 'select' => 'max(create_time)'],
            'preview' => [self::BELONGS_TO, 'Image', 'preview_id'],
            'category' => [self::BELONGS_TO, 'Category', 'category_id'],
        ];
    }

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'upload' => [
				'class' => 'webforma\components\behaviors\ImageUploadBehavior',
				'attributeName' => 'image',
				'uploadPath' => '/gallery',
			],
			'sortable' => [
				'class' => 'webforma\components\behaviors\SortableBehavior',
				'attributeName' => 'position',
			],
		];
	}

    /**
     * beforeValidate
     *
     * @return parent::beforeValidate()
     **/
    public function beforeValidate()
    {
        // Проверяем наличие установленного хозяина галереи
        if (isset($this->owner) && empty($this->owner)) {
            $this->owner = Yii::app()->user->getId();
        }

        return parent::beforeValidate();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('GalleryModule.gallery', 'Id'),
            'name' => Yii::t('GalleryModule.gallery', 'Title'),
            'owner' => Yii::t('GalleryModule.gallery', 'Vendor'),
            'description' => Yii::t('GalleryModule.gallery', 'Description'),
            'status' => Yii::t('GalleryModule.gallery', 'Status'),
            'imagesCount' => Yii::t('GalleryModule.gallery', 'Images count'),
            'category_id' => Yii::t('GalleryModule.gallery', 'Category'),
            'image' => 'Изображение',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria();

        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('owner', $this->owner);
        $criteria->compare('status', $this->status);
        $criteria->compare('category_id', $this->category_id);

        $criteria->order = 't.position';

        return new CActiveDataProvider(get_class($this), [
        	'criteria' => $criteria,
        ]);
    }

	/**
	 * @return array
	 */
    public function getStatusList()
    {
        return [
            self::STATUS_NOT_ACTIVE => 'Не активен',
            self::STATUS_ACTIVE => 'Активен',
        ];
    }

	/**
	 * @return mixed|string
	 */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('GalleryModule.gallery', '*неизвестно*');
    }

	/**
	 * @param Image $image
	 * @return bool
	 */
    public function addImage(Image $image)
    {
        $im2g = new ImageToGallery();

        $im2g->setAttributes(
            [
                'image_id' => $image->id,
                'gallery_id' => $this->id,
            ]
        );

        return $im2g->save();
    }

    /**
     * Получаем картинку для галереи:
     *
     * @param int $width - ширина
     * @param int $height - высота
     *
     * @return string image Url
     **/
    public function previewImage($width = 190, $height = 190)
    {
        $preview = Yii::app()->getTheme()->getAssetsUrl() . '/images/thumbnail.png';

        if (isset($this->preview)) {
            $preview = $this->preview->getImageUrl($width, $height);
        } elseif (!isset($this->preview) && $this->imagesCount > 0) {
            $preview = $this->images[0]->getImageUrl($width, $height);
        }

        return $preview;
    }

    /**
     * get owner name
     *
     * @return string owner name
     **/
    public function getOwnerName()
    {
        return $this->user instanceof User
            ? $this->user->getFullName()
            : '---';
    }

    /**
     *  can add photo
     *
     * @return boolean can add photo
     **/
    public function getCanAddPhoto()
    {
        return true;
    }

    /**
     * Именованные условия
     *
     * @return array of scopes
     **/
    public function scopes()
    {
        return [
            'published' => [
                'condition' => 'status  = :status',
                'params' => [
                    ':status' => self::STATUS_ACTIVE
                ]
            ],
	        'first' => [
		        'order' => 't.position',
	        ],
        ];
    }

	/**
	 * @return string
	 */
    public function getCategoryName()
    {
        return ($this->category === null) ? '---' : $this->category->name;
    }
}
