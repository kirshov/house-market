<?php
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'id' => 'image-form',
	    'enableAjaxValidation' => false,
	    'enableClientValidation' => false,
	    'type' => 'vertical',
	    'htmlOptions' => ['enctype' => 'multipart/form-data', 'class' => 'well sticky'],
	    'clientOptions' => [
		    'validateOnSubmit' => true,
	    ],
    ]
); ?>
<div class="alert alert-info">
    <?= Yii::t('GalleryModule.gallery', 'Fields with'); ?>
    <span class="required">*</span>
    <?= Yii::t('GalleryModule.gallery', 'are required.'); ?>
</div>

<?= $form->errorSummary($model); ?>

<div class='row'>
	<div class='col-sm-6'>
		<div class='row'>
			<div class="col-sm-12">
				<?= $form->textFieldGroup($model, 'name'); ?>
			</div>
		</div>

		<?if($model->getIsNewRecord()):?>
			<div class='row'>
				<div class="col-sm-12">
					<?= $form->textFieldGroup($model, 'alt'); ?>
				</div>
			</div>
		<?endif;?>

		<div class='row'>
			<div class='col-sm-6'>
				<?= $form->dropDownListGroup($model, 'status', [
					'widgetOptions' => [
						'data' => $model->getStatusList(),
					],
				]); ?>
			</div>
		</div>
	</div>

	<div class="col-sm-6">
		<div class='row'>
			<div class="col-sx-8 col-sm-8 col-sm-offset-2">
				<?php $this->widget('webforma\widgets\WInputFile', [
					'model' => $model,
					'attribute' => 'file',
					'altAttribute' => 'alt',
				]); ?>
			</div>
		</div>
	</div>
</div>

<?/*
<div class='row'>
    <div class="col-sm-12">
        <?=$form->textAreaGroup($model, 'description'); ?>
    </div>
</div>
*/?>

<div class="buttons">
<?php
$this->widget('bootstrap.widgets.TbButton', [
	'buttonType' => 'submit',
	'context' => 'primary',
	'label' => $model->isNewRecord
		? Yii::t('GalleryModule.gallery', 'Create image')
		: Yii::t('GalleryModule.gallery', 'Save image'),
]); ?>
</div>
<?php $this->endWidget(); ?>
<!--<style>
    #preview {
        display: none;
        max-width: 250px;
        max-height: 250px;
    }
</style>-->
<!--<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#preview').attr('src', e.target.result).show();
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
-->