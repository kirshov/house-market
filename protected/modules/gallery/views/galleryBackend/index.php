<?php
/**
 * Отображение для default/index:
 **/

$this->breadcrumbs = [
    Yii::t('GalleryModule.gallery', 'Galleries') => ['/gallery/galleryBackend/index'],
    Yii::t('GalleryModule.gallery', 'Management'),
];

$this->pageTitle = Yii::t('GalleryModule.gallery', 'Galleries - manage');

$this->menu = [
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('GalleryModule.gallery', 'Gallery management'),
        'url' => ['/gallery/galleryBackend/index']
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('GalleryModule.gallery', 'Create gallery'),
        'url' => ['/gallery/galleryBackend/create']
    ],
];
?>

<?php
$useCategory = $this->getModule()->useCategory;
$this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'gallery-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'sortableRows'      => true,
        'sortableAjaxSave'  => true,
        'sortableAttribute' => 'position',
        'sortableAction'    => '/gallery/galleryBackend/sortableGallery',
        'columns' => [
            [
                'header' => '',
                'value' => 'CHtml::image($data->getImageUrl(75, 75), $data->name, array("width" => 75))',
                'type' => 'html'
            ],
            [
                'class' => 'webforma\widgets\WEditableColumn',
                'name' => 'name',
                'editable' => [
                    'url' => $this->createUrl('/gallery/galleryBackend/inline'),
                    'mode' => 'popup',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter' => CHtml::activeTextField($model, 'name', ['class' => 'form-control']),
            ],
            /*[
                'class' => 'webforma\widgets\WEditableColumn',
                'name' => 'description',
                'value' => 'trim(strip_tags($data->description))',
                'editable' => [
                    'url' => $this->createUrl('/gallery/galleryBackend/inline'),
                    'type' => 'textarea',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter' => CHtml::activeTextField($model, 'description', ['class' => 'form-control']),
            ],*/
            [
                'name' => 'category_id',
                'value' => '$data->getCategoryName()',
                'filter' => CHtml::activeDropDownList(
                    $model,
                    'category_id',
                    Yii::app()->getComponent('categoriesRepository')->getFormattedList(Yii::app()->getModule('gallery')->mainCategory),
                    ['class' => 'form-control', 'encode' => false, 'empty' => '']
                ),
				'visible' => $useCategory,
            ],
            [
                'class' => 'webforma\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('/gallery/galleryBackend/inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    Gallery::STATUS_NOT_ACTIVE => ['class' => 'label-default'],
                    Gallery::STATUS_ACTIVE => ['class' => 'label-info'],
                ],
            ],
            /*[
                'name' => 'owner',
                'value' => '$data->ownerName',
                'filter' => CHtml::activeDropDownList(
                    $model,
                    'owner',
                    User::getFullNameList(),
                    ['class' => 'form-control', 'empty' => '']
                ),

            ],*/
            [
                'name' => 'imagesCount',
                'value' => 'CHtml::link($data->imagesCount, array("/gallery/galleryBackend/images", "id" => $data->id))',
                'type' => 'raw',
                'filter' => false
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
                'template' => '{images}{update}{delete}',
                'buttons' => [
                    'images' => [
                        'icon' => 'fa fa-fw fa-picture-o',
                        'label' => Yii::t('GalleryModule.gallery', 'Gallery images'),
                        'url' => 'array("/gallery/galleryBackend/images", "id" => $data->id)',
                        'options' => ['class' => 'btn btn-sm btn-default'],
                    ],
                ],
            ],
        ],
    ]
); ?>
