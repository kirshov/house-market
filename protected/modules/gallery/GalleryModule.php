<?php

/**
 * GalleryModule основной класс модуля gallery
 */
class GalleryModule extends webforma\components\WebModule
{
	/**
	 *
	 */
	public $adminMenuOrder = 90;

	/**
	 *
	 */
	public $useCategory = 0;

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            'image'
        ];
    }

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [
            'editor' => Yii::t('GalleryModule.gallery', 'Visual Editor'),
            'mainCategory' => Yii::t('GalleryModule.gallery', 'Main gallery category'),
        ];
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('GalleryModule.gallery', 'Content');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('GalleryModule.gallery', 'Galleries');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('GalleryModule.gallery', 'Module for create simple image galleries');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return "fa fa-fw fa-camera";
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/gallery/galleryBackend/index';
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'gallery.models.*',
                'application.image.models.*',
            ]
        );
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
        return [
            //'mainCategory' => CHtml::listData($this->getCategoryList(), 'id', 'name'),
        ];
    }

	public function getEditableParamsGroups()
	{
		return [
			'main' => [
				'label' => Yii::t('WebformaModule.webforma', 'Main module settings'),
				'items' => [
					/*'adminMenuOrder',
					'coreCacheTime'*/
				]
			],
		];
	}

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('GalleryModule.gallery', 'Galleries list'),
                'url' => ['/gallery/galleryBackend/index']
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('GalleryModule.gallery', 'Create gallery'),
                'url' => ['/gallery/galleryBackend/create']
            ],
        ];
    }
}
