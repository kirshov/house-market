<?php

/**
 * GalleryController контроллер для просмотра галерей на публичной части сайта
 **/
class GalleryController extends \webforma\components\controllers\FrontController
{
    /**
     *
     */
    const GALLERY_PER_PAGE = 9;

    /**
     *
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider(
            'Gallery', [
                'criteria' => [
                    'scopes' => 'published'
                ],
		        'sort' => [
			        'defaultOrder' => 't.position',
		        ],
				'pagination' => [
					'pageSize' => self::GALLERY_PER_PAGE,
				],
            ]
        );

        $this->render('index', ['dataProvider' => $dataProvider]);
    }

	/**
	 * @param $id
	 * @throws CException
	 * @throws CHttpException
	 */
    public function actionView($id)
    {
        if (($gallery = Gallery::model()->published()->findByPk($id)) === null) {
            throw new CHttpException(404, Yii::t('GalleryModule.gallery', 'Page was not found!'));
        }

	    $dataProvider = new CActiveDataProvider(
		    'Image', [
			    'criteria' => [
				    'condition' => 'galleryRell.gallery_id = :id AND t.status = :status',
				    'params' => [
					    ':id' => $gallery->id,
					    ':status' => Image::STATUS_CHECKED,
				    ],
				    'with' => ['gallery', 'galleryRell'],
			    ],
			    'sort' => [
				    'defaultOrder' => 't.`sort` ASC',
			    ],
			    'pagination' => [
				    'pageSize' => self::GALLERY_PER_PAGE,
			    ],
		    ]
	    );

        $this->render(
            'view',
            [
                'dataProvider' => $dataProvider,
                'model' => $gallery,
            ]
        );
    }

	/**
	 * @param $id
	 * @throws CException
	 * @throws CHttpException
	 */
    public function actionImage($id)
    {
        $model = Image::model()->findByPk((int)$id);

        if (!$model) {
            throw new CHttpException(404, Yii::t('GalleryModule.gallery', 'Page was not found!'));
        }

        $this->render('image', ['model' => $model]);
    }


    /**
     * @param null $id
     * @throws CHttpException
     */
    public function actionDeleteImage($id = null)
    {
        if (($image = Image::model()->findByPk($id)) === null || $image->canChange() === false) {
            throw new CHttpException(404, Yii::t('GalleryModule.gallery', 'Page was not found!'));
        }

        $message = Yii::t(
            'GalleryModule.gallery',
            'Image #{id} {result} deleted',
            [
                '{id}' => $id,
                '{result}' => ($result = $image->delete())
                    ? Yii::t('GalleryModule.gallery', 'успешно')
                    : Yii::t('GalleryModule.gallery', 'не')
            ]
        );

        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getIsAjaxRequest()) {
            $result === true
                ? Yii::app()->ajax->success($message)
                : Yii::app()->ajax->failure($message);
        }

        Yii::app()->user->setFlash(
            $result ? webforma\widgets\WFlashMessages::SUCCESS_MESSAGE : webforma\widgets\WFlashMessages::ERROR_MESSAGE,
            $message
        );

        $this->redirect(
            Yii::app()->getRequest()->urlReferer(
                $this->createAbsoluteUrl('gallery/gallery/list')
            )
        );
    }


    /**
     * @param null $id
     * @throws CException
     * @throws CHttpException
     */
    public function actionEditImage($id = null)
    {
        if (($image = Image::model()->findByPk($id)) === null || $image->canChange() === false) {
            throw new CHttpException(404, Yii::t('GalleryModule.gallery', 'Page was not found!'));
        }

        if ((Yii::app()->getRequest()->getIsPostRequest() || Yii::app()->getRequest()->getIsAjaxRequest())
            && Yii::app()->getRequest()->getPost('Image') !== null
        ) {

            $image->setAttributes(Yii::app()->getRequest()->getPost('Image'));

            if ($image->validate() && $image->save()) {

                $message = Yii::t(
                    'GalleryModule.gallery',
                    'Image #{id} edited',
                    [
                        '{id}' => $id,
                    ]
                );

                if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getIsAjaxRequest()) {
                    Yii::app()->ajax->success(
                        [
                            'message' => $message,
                            'type' => 'saved',
                        ]
                    );
                }

                Yii::app()->user->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    $message
                );

                $this->redirect(
                    ['/gallery/gallery/view', 'id' => $image->gallery->id]
                );
            }
        }

        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getIsAjaxRequest()) {
            Yii::app()->ajax->success(
                [
                    'form' => $this->renderPartial('_form', ['model' => $image], true)
                ]
            );
        }
        $this->render('edit-image', ['model' => $image]);
    }

	/**
	 * @throws CException
	 * @throws CHttpException
	 */
	public function actionGallery()
	{
		if (($gallery = Gallery::model()->published()->first()->find()) === null) {
			throw new CHttpException(404, Yii::t('GalleryModule.gallery', 'Page was not found!'));
		}

		$dataProvider = new CActiveDataProvider(
			'Image', [
				'criteria' => [
					'condition' => 'galleryRell.gallery_id = :id AND t.status = :status',
					'params' => [
						':id' => $gallery->id,
						':status' => Image::STATUS_CHECKED,
					],
					'with' => ['gallery', 'galleryRell'],
				],
				'sort' => [
					'defaultOrder' => 't.id DESC',
				],
				'pagination' => [
					'pageSize' => self::GALLERY_PER_PAGE,
				],
			]
		);

		$this->render('gallery', [
			'model' => $gallery,
			'dataProvider' => $dataProvider,
		]);
	}
}
