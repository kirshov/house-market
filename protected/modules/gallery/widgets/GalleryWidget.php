<?php

/**
 * GalleryWidget виджет отрисовки галереи изображений
 */

Yii::import('application.modules.gallery.models.*');

class GalleryWidget extends webforma\widgets\WWidget
{
	// сколько изображений выводить на одной странице
	public $limit = 10;

	// ID-галереи
	public $galleryId;

	// Галерея
	public $gallery;

	public $view = 'default';

	public $width = 1000;
	public $height = 1000;

	/**
	 * Запускаем отрисовку виджета
	 *
	 * @return void
	 */
	public function run()
	{
		if(!$this->gallery){
			if($this->galleryId){
				$this->gallery = Gallery::model()->published()->findByPk($this->galleryId);
			} else {
				//$this->gallery = Gallery::model()->published()->first()->find();
			}
		}
		if($this->gallery){
			$criteria = [
				'condition' => 't.gallery_id = :gallery_id',
				'params' => [':gallery_id' => $this->gallery->id],
				'limit' => $this->limit,
				'order' => 'image.sort',
				'with' => 'image',
			];
		} else {
			$criteria = [
				'join' => 'INNER JOIN '.Gallery::model()->tableName().' g ON g.id = t.gallery_id',
				'condition' => 'g.status = '.Gallery::STATUS_ACTIVE.' AND image.status = '.Image::STATUS_CHECKED,
				'limit' => $this->limit,
				'order' => 'image.sort',
				'with' => 'image',
			];
		}

		$dataProvider = new CActiveDataProvider(
			'ImageToGallery', [
				'criteria' => $criteria,
				'pagination' => ['pageSize' => $this->limit],
			]
		);

		$this->render($this->view, [
			'dataProvider' => $dataProvider,
			'gallery' => $this->gallery,
			'width' => $this->width,
			'height' => $this->height,
		]);
	}
}
