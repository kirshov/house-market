<?php

/**
 * Class PickupBackendController
 */
class PickupBackendController extends webforma\components\controllers\BackController
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            'inline' => [
                'class' => 'webforma\components\actions\WInLineEditAction',
                'model' => 'Pickup',
                'validAttributes' => [
                    'status', 'name', 'group_id',
                ],
            ],
            'sortable' => [
                'class' => 'webforma\components\actions\SortAction',
                'model' => 'Pickup',
            ],
        ];
    }

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin'],],
            ['allow', 'actions' => ['index'], 'roles' => ['Pickup.PickupBackend.Index'],],
            ['allow', 'actions' => ['create'], 'roles' => ['Pickup.PickupBackend.Create'],],
            ['allow', 'actions' => ['update', 'sortable', 'inline'], 'roles' => ['Pickup.PickupBackend.Update'],],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Pickup.PickupBackend.Delete'],],
            ['deny',],
        ];
    }


    /**
     *
     */
    public function actionCreate()
    {
        $model = new Pickup();

        if (isset($_POST['Pickup'])) {
            $model->attributes = $_POST['Pickup'];

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('DeliveryModule.delivery', 'Record created!')
                );

	            $this->redirect((array)Yii::app()->getRequest()->getPost('submit-type', ['create']));
            }
        }

        $this->render('create', ['model' => $model]);
    }

    /**
     * @param $id
     * @throws CHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Pickup'])) {
            $model->attributes = $_POST['Pickup'];

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('DeliveryModule.delivery', 'Record updated!')
                );

                if (!isset($_POST['submit-type'])) {
                    $this->redirect(['update', 'id' => $model->id]);
                } else {
                    $this->redirect([$_POST['submit-type']]);
                }
            }
        }

        $this->render('update', ['model' => $model]);
    }

    /**
     * @param $id
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                Yii::t('DeliveryModule.delivery', 'Record removed!')
            );

            if (!isset($_GET['ajax'])) {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : ['index']);
            }
        } else {
            throw new CHttpException(
                400, Yii::t(
                'DeliveryModule.delivery',
                'Unknown request. Don\'t repeat it please!'
            )
            );
        }
    }


    /**
     *
     */
    public function actionIndex()
    {
        $model = new Pickup('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Pickup'])) {
            $model->attributes = $_GET['Pickup'];
        }
        $this->render('index', ['model' => $model]);
    }


    /**
     * @param $id
     * @return array|mixed|null
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Pickup::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('DeliveryModule.delivery', 'Page not found!'));
        }

        return $model;
    }


    /**
     * @param Pickup $model
     */
    protected function performAjaxValidation(Pickup $model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'delivery-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

	/**
	 * @return void
	 * @throws CHttpException If not ajax/post query || record not found
	 */
	public function actionGetjsonitems()
	{
		if (!Yii::app()->getRequest()->getIsPostRequest() || !Yii::app()->getRequest()->getIsAjaxRequest()) {
			throw new CHttpException(404);
		}

		if (($deliveryId = Yii::app()->getRequest()->getPost('deliveryId')) === null) {
			throw new CHttpException(404);
		}

		$items = Pickup::model()->findAll([
			'condition' => 'group_id = :group_id',
			'order' => 'name DESC',
			'params' => [
				':group_id' => $deliveryId,
			],
		]);

		Yii::app()->ajax->success(CHtml::listData($items, 'id', 'name'));
	}
}
