<?php

/**
 * Class PickupGroupBackendController
 */
class PickupGroupBackendController extends webforma\components\controllers\BackController
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            'inline' => [
                'class' => 'webforma\components\actions\WInLineEditAction',
                'model' => 'PickupGroup',
                'validAttributes' => [
                    'status', 'name',
                ],
            ],
            'sortable' => [
                'class' => 'webforma\components\actions\SortAction',
                'model' => 'PickupGroup',
            ],
        ];
    }

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin'],],
            ['allow', 'actions' => ['index'], 'roles' => ['PickupGroup.PickupGroupBackend.Index'],],
            ['allow', 'actions' => ['create'], 'roles' => ['PickupGroup.PickupGroupBackend.Create'],],
            ['allow', 'actions' => ['update', 'sortable', 'inline'], 'roles' => ['PickupGroup.PickupGroupBackend.Update'],],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['PickupGroup.PickupGroupBackend.Delete'],],
            ['deny',],
        ];
    }


    /**
     *
     */
    public function actionCreate()
    {
        $model = new PickupGroup();

        if (isset($_POST['PickupGroup'])) {
            $model->attributes = $_POST['PickupGroup'];

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('DeliveryModule.delivery', 'Record created!')
                );

                if (!isset($_POST['submit-type'])) {
                    $this->redirect(['create']);
                } else {
                    $this->redirect([$_POST['submit-type']]);
                }
            }
        }

        $this->render('create', ['model' => $model]);
    }

    /**
     * @param $id
     * @throws CHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['PickupGroup'])) {
            $model->attributes = $_POST['PickupGroup'];

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('DeliveryModule.delivery', 'Record updated!')
                );

                if (!isset($_POST['submit-type'])) {
                    $this->redirect(['update', 'id' => $model->id]);
                } else {
                    $this->redirect([$_POST['submit-type']]);
                }
            }
        }

        $this->render('update', ['model' => $model]);
    }

    /**
     * @param $id
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                Yii::t('DeliveryModule.delivery', 'Record removed!')
            );

            if (!isset($_GET['ajax'])) {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : ['index']);
            }
        } else {
            throw new CHttpException(
                400, Yii::t(
                'DeliveryModule.delivery',
                'Unknown request. Don\'t repeat it please!'
            )
            );
        }
    }


    /**
     *
     */
    public function actionIndex()
    {
        $model = new PickupGroup('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['PickupGroup'])) {
            $model->attributes = $_GET['PickupGroup'];
        }
        $this->render('index', ['model' => $model]);
    }


    /**
     * @param $id
     * @return array|mixed|null
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = PickupGroup::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('DeliveryModule.delivery', 'Page not found!'));
        }

        return $model;
    }


    /**
     * @param PickupGroup $model
     */
    protected function performAjaxValidation(PickupGroup $model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'delivery-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
