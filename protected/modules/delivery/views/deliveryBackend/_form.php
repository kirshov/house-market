<?php
$form = $this->beginWidget(
    'webforma\widgets\ActiveForm',
    [
        'id' => 'delivery-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'htmlOptions' => ['class' => 'well'],
    ]
);
?>

<div class="alert alert-info">
    <?= Yii::t('DeliveryModule.delivery', 'Fields with'); ?>
    <span class="required">*</span>
    <?= Yii::t('DeliveryModule.delivery', 'are required'); ?>
</div>

<?= $form->errorSummary($model); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="row">
			<div class="col-sm-6">
		        <?= $form->textFieldGroup($model, 'name'); ?>
			</div>

            <div class="col-sm-6">
                <?= $form->dropDownListGroup(
                    $model,
                    'status',
                    [
                        'widgetOptions' => [
                            'data' => $model->getStatusList(),
                        ],
                    ]
                ); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <?= $form->textFieldGroup($model, 'price'); ?>
            </div>
            <div class="col-sm-6">
                <?= $form->textFieldGroup($model, 'available_from'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->textFieldGroup($model, 'free_from'); ?>
            </div>

            <div class="col-sm-6">
                <?= $form->textFieldGroup($model, 'free_text', ['widgetOptions' => ['htmlOptions' => ['placeholder' => 'Бесплатно']]]); ?>
            </div>
        </div>

		<?if(Yii::app()->hasModule('regions')):?>
			<div class="row">
				<div class="col-sm-6">
					<?= $form->dropDownListGroup($model, 'show_region', [
						'widgetOptions' => [
							'data' => $model->getRegionList(),
						],
					]); ?>
				</div>
			</div>
        <?endif;?>
		
		<div class="row">
			<div class="col-sm-6">
				<?= $form->checkBoxGroup($model, 'separate_payment'); ?>
			</div>
			<div class="col-sm-6">
			    <?= $form->checkBoxGroup($model, 'need_address'); ?>
			</div>
		</div>
		
		<div class="row">
			<?if(Yii::app()->hasModule('cdek_calc')):?>
				<div class="col-sm-6">
					<?= $form->textFieldGroup($model, 'delivery_calc[cdek]', [
						'label' => 'Id тарифа СДЕК',
						'widgetOptions' => [
							'htmlOptions' => [
								'placeholder' => 'Id тарифа СДЕК',
							]
						],
					]); ?>
				</div>
			<?endif;?>
		</div>
		<div class="row">
			<div class="col-sm-6">
			    <?= $form->dropDownListGroup($model, 'pickup_group_id', [
					'widgetOptions' => [
						'data' => CHtml::listData(PickupGroup::model()->findAll(), 'id', 'name'),
						'htmlOptions' => [
							'empty' => '-- не выбрано --',
						],
					],
				]); ?>
			</div>
		</div>
        <div class='row'>
            <div class="col-sm-12 <?= $model->hasErrors('description') ? 'has-error' : ''; ?>">
                <?= $form->labelEx($model, 'description'); ?>
                <?php $this->widget($this->module->getVisualEditor(), [
					'model' => $model,
					'attribute' => 'description',
				]); ?>
            </div>
        </div>
		<br/>
		<div class="row">
			<div class="col-sm-12">
			    <?= $form->textFieldGroup($model, 'track_num_link'); ?>
				<div class="help-block">
					Например: https://www.pochta.ru/tracking#<br/>
					При заполненной ссылке на сервис отслеживания и номере отправления, в письме будет указана ссылка на отслеживание отправления.
				</div>
			</div>
		</div>

        <?= $form->hiddenField($model, 'position'); ?>
    </div>
</div>

<br/>
<div class="buttons">
	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context' => 'primary',
		'label' => $model->getIsNewRecord() ? Yii::t('DeliveryModule.delivery', 'Add delivery and continue') : Yii::t('DeliveryModule.delivery', 'Save delivery and continue'),
	]); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label' => $model->getIsNewRecord() ? Yii::t('DeliveryModule.delivery', 'Add delivery and close') : Yii::t('DeliveryModule.delivery', 'Save delivery and close'),
	]); ?>
</div>
<?php $this->endWidget(); ?>
