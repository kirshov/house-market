<?php
$this->breadcrumbs = [
    Yii::t('DeliveryModule.delivery', 'Delivery methods') => ['/delivery/deliveryBackend/index'],
    Yii::t('DeliveryModule.delivery', 'Manage'),
];

$this->pageTitle = Yii::t('DeliveryModule.delivery', 'Delivery methods - manage');

$hasMidTariff = Helper::hasMidTariff();

$this->menu = [
	['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('DeliveryModule.delivery', 'Manage delivery methods'), 'url' => ['/delivery/deliveryBackend/index']],
	['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('DeliveryModule.delivery', 'Create delivery'), 'url' => ['/delivery/deliveryBackend/create']],
	['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('DeliveryModule.delivery', 'Manage pickup group methods'), 'url' => ['/delivery/pickupGroupBackend/index'], 'visible' => $hasMidTariff],
	['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('DeliveryModule.delivery', 'Create pickup group'), 'url' => ['/delivery/pickupGroupBackend/create'], 'visible' => $hasMidTariff],
	['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('DeliveryModule.delivery', 'Manage pickup methods'), 'url' => ['/delivery/pickupBackend/index'], 'visible' => $hasMidTariff],
	['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('DeliveryModule.delivery', 'Create pickup'), 'url' => ['/delivery/pickupBackend/create'], 'visible' => $hasMidTariff],
];

$this->title = Yii::t('DeliveryModule.delivery', 'Delivery methods');
$this->subTitle = Yii::t('DeliveryModule.delivery', 'manage');

$this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'delivery-grid',
        'type' => 'condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'sortableRows' => true,
        'sortableAjaxSave' => true,
        'sortableAttribute' => 'position',
        'sortableAction' => '/delivery/deliveryBackend/sortable',
        'columns' => [
            [
                'name' => 'name',
                'type' => 'raw',
                'value' => 'CHtml::link($data->name, array("/delivery/deliveryBackend/update", "id" => $data->id))',
            ],
            'price',
            'free_from',
            'available_from',
	        [
		        'class' => 'webforma\widgets\EditableStatusColumn',
		        'name' => 'show_region',
		        'url' => $this->createUrl('/delivery/deliveryBackend/inline'),
		        'source' => $model->getRegionList(),
		        'header' => 'Показывать в регионах',
		        'value' => function (Delivery $data) {
			        return $data->getShowInRegion();
		        },
		        'options' => [
			        'empty' => '--выберите--',
		        ],
		        'editable' => [
			        'mode'   => 'popup',
		        ],
		        'visible' => Yii::app()->hasModule('regions'),
	        ],
            [
                'class'   => 'webforma\widgets\EditableStatusColumn',
                'name'    => 'status',
                'url'     => $this->createUrl('/delivery/deliveryBackend/inline'),
                'source'  => $model->getStatusList(),
                'options' => [
                    Delivery::STATUS_ACTIVE => ['class' => 'label-success'],
                    Delivery::STATUS_NOT_ACTIVE => ['class' => 'label-default'],
                ],
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
                'template' => '{update}{delete}'
            ],
        ],
    ]
);