<?php
$this->breadcrumbs = [
    Yii::t('DeliveryModule.delivery', 'Delivery methods') => ['/delivery/deliveryBackend/index'],
    Yii::t('DeliveryModule.delivery', 'Creating'),
];

$this->pageTitle = Yii::t('DeliveryModule.delivery', 'Delivery methods - creating');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('DeliveryModule.delivery', 'Manage delivery methods'), 'url' => ['/delivery/deliveryBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('DeliveryModule.delivery', 'Create delivery'), 'url' => ['/delivery/deliveryBackend/create']],
];

$this->title = Yii::t('DeliveryModule.delivery', 'Delivery methods');
$this->subTitle = Yii::t('DeliveryModule.delivery', 'creating');

echo $this->renderPartial('_form', ['model' => $model]);