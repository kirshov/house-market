<?php
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'id' => 'delivery-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'htmlOptions' => ['class' => 'well'],
    ]
);
?>

<div class="alert alert-info">
    <?= Yii::t('DeliveryModule.delivery', 'Fields with'); ?>
    <span class="required">*</span>
    <?= Yii::t('DeliveryModule.delivery', 'are required'); ?>
</div>

<?= $form->errorSummary($model); ?>
<div class="row">
    <div class="col-sm-12">
		<div class="row">
			<div class="col-sm-4">
			    <?= $form->dropDownListGroup(
				    $model,
				    'group_id',
				    [
					    'widgetOptions' => [
						    'data' => CHtml::listData(PickupGroup::model()->findAll(['order' => 'position']), 'id', 'name'),
					    ],
				    ]
			    ); ?>
			</div>

			<div class="col-sm-4">
				<?= $form->textFieldGroup($model, 'name'); ?>
			</div>
		</div>
        <div class="row">
            <div class="col-sm-4">
                <?= $form->textFieldGroup($model, 'address'); ?>
            </div>
			<div class="col-sm-4">
		        <?= $form->textFieldGroup($model, 'coords'); ?>
			</div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <?= $form->textFieldGroup($model, 'price'); ?>
            </div>
            <div class="col-sm-4">
                <?= $form->textFieldGroup($model, 'free_from'); ?>
            </div>
        </div>

		<div class="row">
			<div class="col-sm-8">
			    <?= $form->textFieldGroup($model, 'worktime'); ?>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-8">
			    <?= $form->textAreaGroup($model, 'description'); ?>
			</div>
		</div>


		<div class="row">
			<div class="col-sm-8">
			    <?= $form->textAreaGroup($model, 'how_to_get'); ?>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<?= $form->dropDownListGroup(
					$model,
					'payment',
					[
						'widgetOptions' => [
							'data' => $model->getPaymentList(),
							'htmlOptions' => [
								'multiple' => true,
							]
						],
					]
				); ?>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<?= $form->dropDownListGroup(
					$model,
					'status',
					[
						'widgetOptions' => [
							'data' => $model->getStatusList(),
						],
					]
				); ?>
			</div>
		</div>
    </div>
</div>

<?php $this->widget(
    'bootstrap.widgets.TbButton',
    [
        'buttonType' => 'submit',
        'context' => 'primary',
        'label' => $model->getIsNewRecord() ? Yii::t('DeliveryModule.delivery', 'Add delivery and continue') : Yii::t('DeliveryModule.delivery', 'Save delivery and continue'),
    ]
); ?>

<?php $this->widget(
    'bootstrap.widgets.TbButton',
    [
        'buttonType' => 'submit',
        'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
        'label' => $model->getIsNewRecord() ? Yii::t('DeliveryModule.delivery', 'Add delivery and close') : Yii::t('DeliveryModule.delivery', 'Save delivery and close'),
    ]
); ?>

<?php $this->endWidget(); ?>
