<?php
$this->breadcrumbs = [
    Yii::t('DeliveryModule.delivery', 'Delivery methods') => ['/delivery/deliveryBackend/index'],
    Yii::t('DeliveryModule.delivery', 'Pickup methods') => ['/delivery/pickupBackend/index'],
    Yii::t('DeliveryModule.delivery', 'Edition'),
];

$this->pageTitle = Yii::t('DeliveryModule.delivery', 'Delivery methods - edition');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('DeliveryModule.delivery', 'Manage pickup methods'), 'url' => ['/delivery/pickupBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('DeliveryModule.delivery', 'Create pickup'), 'url' => ['/delivery/pickupBackend/create']],
    [
        'icon' => 'fa fa-fw fa-pencil',
        'label' => Yii::t('DeliveryModule.delivery', 'Update pickup method'),
        'url' => [
            '/delivery/pickupBackend/update',
            'id' => $model->id
        ]
    ],
    [
        'icon' => 'fa fa-fw fa-trash-o',
        'label' => Yii::t('DeliveryModule.delivery', 'Delete pickup method'),
        'url' => '#',
        'linkOptions' => [
            'submit' => ['/delivery/pickupBackend/delete', 'id' => $model->id],
            'confirm' => Yii::t('DeliveryModule.delivery', 'Do you really want to remove this pickup method?'),
            'params' => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
            'csrf' => true,
        ]
    ],
];
?>
<div class="page-header">
    <h1>
        <?= Yii::t('DeliveryModule.delivery', 'Updating delivery'); ?><br/>
        <small>&laquo;<?= $model->name; ?>&raquo;</small>
    </h1>
</div>
<?= $this->renderPartial('_form', ['model' => $model]); ?>