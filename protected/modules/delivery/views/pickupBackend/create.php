<?php
$this->breadcrumbs = [
    Yii::t('DeliveryModule.delivery', 'Delivery methods') => ['/delivery/deliveryBackend/index'],
    Yii::t('DeliveryModule.delivery', 'Pickup methods') => ['/delivery/pickupBackend/index'],
    Yii::t('DeliveryModule.delivery', 'Creating'),
];

$this->pageTitle = Yii::t('DeliveryModule.delivery', 'Pickup methods - creating');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('DeliveryModule.delivery', 'Manage pickup methods'), 'url' => ['/delivery/pickupBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('DeliveryModule.delivery', 'Create pickup'), 'url' => ['/delivery/pickupBackend/create']],
];
?>

<?$this->title = Yii::t('DeliveryModule.delivery', 'Pickup methods').'<br><small>'.Yii::t('DeliveryModule.delivery', 'creating').'</small>';?>

<?= $this->renderPartial('_form', ['model' => $model]); ?>
