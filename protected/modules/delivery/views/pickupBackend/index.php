<?php
$this->breadcrumbs = [
    Yii::t('DeliveryModule.delivery', 'Delivery methods') => ['/delivery/deliveryBackend/index'],
    Yii::t('DeliveryModule.delivery', 'Pickup methods') => ['/delivery/pickupBackend/index'],
    Yii::t('DeliveryModule.delivery', 'Manage'),
];

$this->pageTitle = Yii::t('DeliveryModule.delivery', 'Pickup methods - manage');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('DeliveryModule.delivery', 'Manage pickup methods'), 'url' => ['/delivery/pickupBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('DeliveryModule.delivery', 'Create pickup'), 'url' => ['/delivery/pickupBackend/create']],
];
?>

<?php
$pickupGroups = $model->getPickupGroups();
$this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'delivery-grid',
        'type' => 'condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'sortableRows' => true,
        'sortableAjaxSave' => true,
        'sortableAttribute' => 'position',
        'sortableAction' => '/delivery/pickupBackend/sortable',
        'columns' => [
            [
                'name' => 'name',
                'type' => 'raw',
                'value' => 'CHtml::link($data->name, array("/delivery/pickupBackend/update", "id" => $data->id))',
            ],
            'price',
            'free_from',
            [
                'class'    => 'webforma\widgets\WEditableColumn',
                'editable' => [
                    'url'    => $this->createUrl('/delivery/pickupBackend/inline'),
                    'mode'   => 'popup',
                    'type'   => 'select',
                    'title'  => 'Выберите группу',
                    'source' => $pickupGroups,
                    'params' => [
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
                    ]
                ],
                'name'     => 'group_id',
                'type'     => 'raw',
                'value'    => '$data->getPickupGroupName()',
                'filter'   => CHtml::activeDropDownList(
                    $model,
                    'group_id',
                    $pickupGroups,
                    ['class' => 'form-control', 'empty' => '']
                ),
            ],
            [
                'class'   => 'webforma\widgets\EditableStatusColumn',
                'name'    => 'status',
                'url'     => $this->createUrl('/delivery/pickupBackend/inline'),
                'source'  => $model->getStatusList(),
                'options' => [
                    Delivery::STATUS_ACTIVE => ['class' => 'label-success'],
                    Delivery::STATUS_NOT_ACTIVE => ['class' => 'label-default'],
                ],
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
                'template' => '{update}{delete}'
            ],
        ],
    ]
); ?>
