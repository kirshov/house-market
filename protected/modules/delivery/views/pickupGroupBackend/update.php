<?php
$this->breadcrumbs = [
    Yii::t('DeliveryModule.delivery', 'Delivery methods') => ['/delivery/deliveryBackend/index'],
    Yii::t('DeliveryModule.delivery', 'Pickup group methods') => ['/delivery/pickupGroupBackend/index'],
    Yii::t('DeliveryModule.delivery', 'Edition'),
];

$this->pageTitle = Yii::t('DeliveryModule.delivery', 'Delivery pickup group methods - edition');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('DeliveryModule.delivery', 'Manage pickup group methods'), 'url' => ['/delivery/pickupGroupBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('DeliveryModule.delivery', 'Create pickup group'), 'url' => ['/delivery/pickupGroupBackend/create']],
    ['label' => Yii::t('DeliveryModule.delivery', 'Delivery method') . ' «' . mb_substr($model->name, 0, 32) . '»'],
    [
        'icon' => 'fa fa-fw fa-pencil',
        'label' => Yii::t('DeliveryModule.delivery', 'Update pickup group'),
        'url' => [
            '/delivery/pickupGroupBackend/update',
            'id' => $model->id
        ]
    ],
    [
        'icon' => 'fa fa-fw fa-trash-o',
        'label' => Yii::t('DeliveryModule.delivery', 'Delete pickup group'),
        'url' => '#',
        'linkOptions' => [
            'submit' => ['/delivery/pickupGroupBackend/delete', 'id' => $model->id],
            'confirm' => Yii::t('DeliveryModule.delivery', 'Do you really want to remove this pickup group?'),
            'params' => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
            'csrf' => true,
        ]
    ],
];
?>
<?= $this->renderPartial('_form', ['model' => $model]); ?>