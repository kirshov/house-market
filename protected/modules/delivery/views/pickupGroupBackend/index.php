<?php
$this->breadcrumbs = [
    Yii::t('DeliveryModule.delivery', 'Delivery methods') => ['/delivery/deliveryBackend/index'],
    Yii::t('DeliveryModule.delivery', 'Pickup group methods') => ['/delivery/pickupGroupBackend/index'],
    Yii::t('DeliveryModule.delivery', 'Manage'),
];

$this->pageTitle = Yii::t('DeliveryModule.delivery', 'Pickup group methods - manage');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('DeliveryModule.delivery', 'Manage pickup group methods'), 'url' => ['/delivery/pickupGroupBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('DeliveryModule.delivery', 'Create pickup group'), 'url' => ['/delivery/pickupGroupBackend/create']],
];
?>

<?php
$this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'delivery-grid',
        'type' => 'condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'sortableRows' => true,
        'sortableAjaxSave' => true,
        'sortableAttribute' => 'position',
        'sortableAction' => '/delivery/pickupGroupBackend/sortable',
        'columns' => [
            [
                'name' => 'name',
                'type' => 'raw',
                'value' => 'CHtml::link($data->name, array("/delivery/pickupGroupBackend/update", "id" => $data->id))',
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
                'template' => '{update}{delete}'
            ],
        ],
    ]
); ?>
