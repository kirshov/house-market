<?php
$this->breadcrumbs = [
    Yii::t('DeliveryModule.delivery', 'Delivery methods') => ['/delivery/deliveryBackend/index'],
    Yii::t('DeliveryModule.delivery', 'Pickup group methods') => ['/delivery/pickupGroupBackend/index'],
    Yii::t('DeliveryModule.delivery', 'Creating'),
];

$this->pageTitle = Yii::t('DeliveryModule.delivery', 'Pickup group methods - creating');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('DeliveryModule.delivery', 'Manage pickup group methods'), 'url' => ['/delivery/pickupGroupBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('DeliveryModule.delivery', 'Create pickup group'), 'url' => ['/delivery/pickupGroupBackend/create']],
];
?>

<?= $this->renderPartial('_form', ['model' => $model]); ?>
