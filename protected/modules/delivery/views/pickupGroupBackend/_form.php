<?php
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'id' => 'delivery-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'htmlOptions' => ['class' => 'well'],
    ]
);
?>

<div class="alert alert-info">
    <?= Yii::t('DeliveryModule.delivery', 'Fields with'); ?>
    <span class="required">*</span>
    <?= Yii::t('DeliveryModule.delivery', 'are required'); ?>
</div>

<?= $form->errorSummary($model); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="row">
			<div class="col-sm-4">
		        <?= $form->textFieldGroup($model, 'name'); ?>
			</div>

            <div class="col-sm-2">
                <?= $form->dropDownListGroup($model, 'status', [
					'widgetOptions' => [
						'data' => $model->getStatusList(),
					],
				]); ?>
            </div>
        </div>
    </div>
</div>

<?php $this->widget('bootstrap.widgets.TbButton', [
	'buttonType' => 'submit',
	'context' => 'primary',
	'label' => $model->getIsNewRecord()
		? Yii::t('DeliveryModule.delivery', 'Add pickup group and continue')
		: Yii::t('DeliveryModule.delivery', 'Save pickup group and continue'),
]); ?>

<?php $this->widget('bootstrap.widgets.TbButton', [
	'buttonType' => 'submit',
	'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
	'label' => $model->getIsNewRecord()
		? Yii::t('DeliveryModule.delivery', 'Add pickup group and close')
		: Yii::t('DeliveryModule.delivery', 'Save pickup group and close'),
]); ?>

<?php $this->endWidget(); ?>
