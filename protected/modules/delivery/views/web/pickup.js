var map, clusterer, placemarks = new Object(), isMapInit = false, allowedCenter = true;
function mapInit(rand, c1, c2, placemarkItems) {
	$(document).ready(function(){
		ymaps.ready(function () {
			init(rand, c1, c2);
            setPlacemark(placemarkItems);
			window.isMapInit = true;
        });
	});
	function init(c1, c2) {
		window.map = new ymaps.Map("map_" + rand, {
			center: [c1, c2],
			zoom: 15
		}, {
			searchControlProvider: 'yandex#search'
		});

        window.clusterer = new ymaps.Clusterer({
            clusterDisableClickZoom: true,
            clusterHideIconOnBalloonOpen: false,
            geoObjectHideIconOnBalloonOpen: false,
			groupByCoordinates: true
        });
        window.map.geoObjects.add(window.clusterer);
	}
}

function setPlacemark(items) {
	$.each(items, function (index, item) {
		window.placemarks[index] = new ymaps.Placemark([item.coord1, item.coord2], {}, {
            preset: "islands#blueShoppingIcon",
		});

		window.placemarks[index].events.add('click', function () {
			openPickupItem(index)
		});

		window.clusterer.add(window.placemarks[index]);
	});

	setCenterClusterer();
}

function setCenterClusterer() {
	if(!window.isMapInit){
		if(window.allowedCenter) {
			setTimeout(function () {
				setCenterClusterer();
			}, 50);
		}
	} else {
		window.map.setBounds(window.clusterer.getBounds(), {
			checkZoomRange: true,
			zoomMargin: [20, 20, 20, 20]
		});
	}
}

$(document).ready(function () {
	$('body').on('click', '.pickup-wrapper .pickup-nav__item', function () {
		var wrapper = $(this).closest('.pickup-nav');
		wrapper.removeClass('active');
		wrapper.find('.pickup-nav__item').removeClass('active');

		$(this).addClass('active');
		$(this).closest('.pickup-nav').addClass('active');
		centerPlacemark($(this).attr('data-id'));
	});

	$('body').on('click', '.pickup-nav_all', function () {
		var wrapper = $(this).closest('.pickup-nav');
		wrapper.removeClass('active');
		wrapper.find('.pickup-nav__item').removeClass('active');
		setCenterClusterer();
	});

	$('body').on('click', '.pickup-select .button-title', function () {
		var deliveryId = $(this).closest('.pickup-select').attr('data-pickup');
		var pickupMap = $('.pickup-wrapper[data-id=' + deliveryId + ']').closest('.delivery-pickup__map');
        openModal(pickupMap);
        if(!pickupMap.find('.pickup-nav').hasClass('active')){
			redrawMapModal();
		}
		var windowHeight = getWindowHeight();
		if(pickupMap.height() > windowHeight){
			pickupMap.height(windowHeight - 50);
		}
    });

	$('body').on('click', '.select-pickup-item', function(){
		var pickupId = $(this).closest('.pickup-wrapper').attr('data-id');
		var wrapper = $('.delivery-pickup[data-pickup=' + pickupId + ']');
		$('.input-pickup-id').val('');
		wrapper.find('.input-pickup-id').val($(this).attr('data-id'));
		wrapper.find('.selected-pickup').html($(this).attr('data-address')).addClass('selected');
		wrapper.closest('.order-box__item').find('input:radio').trigger('click');
		wrapper.find('.button-title').html(wrapper.find('.button-title').attr('data-other-title'));
		formCloseAll();
	});
});

function redrawMapModal() {
	if(!window.isMapInit){
		window.allowedCenter = false;
		setTimeout(function () {
			redrawMapModal();
		}, 100);
	} else {
		window.map.container.fitToViewport()
		setCenterClusterer();
	}
}

function openPickupItem(id) {
	if($('.pickup-nav__item[data-id='+ id +']').hasClass('active')){
		$('.pickup-nav__item[data-id='+ id +'] .select-pickup-item').trigger('click');
	} else {
		$('.pickup-nav__item[data-id='+ id +']').trigger('click');
	}
	//centerPlacemark(id);
}

function centerPlacemark(id) {
	if(!window.isMapInit){
		window.allowedCenter = false;
		setTimeout(function () {
			centerPlacemark(id);
		}, 100);
	} else {
		if(window.map.getZoom() < 15 || !window.allowedCenter){
			map.setCenter(window.placemarks[id].geometry.getCoordinates(), 16);
		}

		window.allowedCenter = true;
	}
}
