$(document).ready(function () {
	try{
		calculateDeliveryCost();
	} catch (e){}

	try{
		calculateCartDeliveryCost();
	} catch (e){}
});

function calculateDeliveryCost(){
	if($('.delivery-widget__list .cdek').length){
		$.get('/getPriceCdek/', {}, function (response) {
			$('.delivery-widget__list .cdek_stock').html(response.cdek_stock).removeClass('loader');
			$('.delivery-widget__list .cdek_apartment').html(response.cdek_apartment).removeClass('loader');
		}, 'json');
	}
}
function calculateCartDeliveryCost(){
	if($('.real-price.cdek').length){
		var tariffs = [];
		$.each($('.real-price.cdek'), function(){
			if($(this).data('tariff')){
				tariffs.push($(this).data('tariff'));
			}
		});
		$.get('/getPriceCdek/', {tariffs: tariffs}, function (response) {
			$.each($('.real-price.cdek'), function(){
				var tariff = $(this).data('tariff');
				if(tariff && response[tariff]){
					$('.real-price.cdek[data-tariff="' + tariff + '"]').html(response[tariff]['price']).removeClass('loader');
					if(response[tariff]['available']){
						$(this).closest('.order-box__item').find('input').removeAttr('disabled').data('price', response[tariff]['price_num']).attr('data-price', response[tariff]['price_num']);
						$(this).closest('.order-box__item').find('.jq-radio').removeClass('disabled');
						$(this).closest('.order-box__item').removeClass('disabled');
					} else {
						$(this).closest('.order-box__item').find('input').prop('checked', false).attr('disabled', true).attr('price', 0).attr('data-price', 0);
						$(this).closest('.order-box__item').addClass('disabled');
						$(this).closest('.order-box__item').find('.jq-radio').addClass('disabled').removeClass('checked');
					}

				} else {
					$(this).closest('.order-box__item').find('input').attr('disabled', true).attr('price', 0).attr('data-price', 0);
					$(this).closest('.order-box__item').addClass('disabled');
					$(this).closest('.order-box__item').find('.jq-radio').addClass('disabled');
				}
			});

			if(!$('.order-box__inner input.radio__input:checked:enabled').length){
				$('.order-box__inner input.radio__input:enabled:first').prop('checked', true).trigger('change');
			} else {
				$('.order-box__inner input.radio__input:checked').trigger('change');
			}
		}, 'json');
	}
}