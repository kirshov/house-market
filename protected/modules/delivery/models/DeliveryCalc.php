<?php

/**
 *
 * @property integer $delivery_id
 * @property integer $type
 * @property integer $value
 *
 */
class DeliveryCalc extends \webforma\models\WModel
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{store_delivery_calc}}';
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

	/**
	 * @return bool
	 */
	public function hasPickup(){
    	return in_array($this->value, Yii::app()->getModule('cdek_calc')->pickupTariffs);
	}

	public function getPickupList(){
		if($this->type == 'cdek' && $_SESSION['geo']['city']['name_ru']){
			$cdekCity = new CdekCity();
			return $cdekCity->getOffices($_SESSION['geo']['city']['name_ru']);
		}
	}
}
