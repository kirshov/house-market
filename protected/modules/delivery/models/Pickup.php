<?php

/**
 * @property integer $id
 * @property string $name
 * @property string $coords
 * @property integer $group_id
 * @property double $price
 * @property double $free_from
 * @property integer $position
 * @property integer $status
 * @property integer $address
 * @property integer $worktime
 * @property integer $description
 * @property integer $how_to_get
 * @property integer $payment
 */

class Pickup extends webforma\models\WModel
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 0;


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{store_pickup}}';
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['name, price, position, status, coords, group_id', 'required'],
            ['name, address,worktime,description,how_to_get', 'filter', 'filter' => 'trim'],
            ['position, group_id', 'numerical', 'integerOnly' => true],
            ['price, free_from', 'store\components\validators\NumberValidator'],
            ['name', 'length', 'max' => 255],
            ['description, payment_methods', 'safe'],
	        ['payment', 'safe'],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            [
                'id, name, status, position, description, price, free_from,address,worktime,description,payment',
                'safe',
                'on' => 'search'
            ],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'sortable' => [
                'class' => 'webforma\components\behaviors\SortableBehavior',
                'attributeName' => 'position',
            ],
        ];
    }


    public function scopes()
    {
        return [
            'published' => [
                'condition' => 'status = :status',
                'params' => [':status' => self::STATUS_ACTIVE],
            ],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('DeliveryModule.delivery', 'ID'),
            'name' => Yii::t('DeliveryModule.delivery', 'Title'),
            'status' => Yii::t('DeliveryModule.delivery', 'Status'),
            'position' => Yii::t('DeliveryModule.delivery', 'Position'),
            'price' => Yii::t('DeliveryModule.delivery', 'Price'),
            'free_from' => Yii::t('DeliveryModule.delivery', 'Free from'),
            'coords' => 'Координаты на карте',
            'group_id' => 'Группа',
            'address' => 'Адрес',
            'worktime' => 'Время работы (через запятую)',
            'description' => 'Описание',
            'how_to_get' => 'Ближайшие остановки',
            'payment' => 'Способы оплаты',
        ];
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('price', $this->price);
        $criteria->compare('free_from', $this->free_from);
        $criteria->compare('group_id', $this->group_id);
        $criteria->compare('status', $this->status);
        $criteria->compare('position', $this->position);
        $criteria->compare('address', $this->address);
        $criteria->compare('worktime', $this->worktime);
        $criteria->compare('description', $this->description);
        $criteria->compare('how_to_get', $this->how_to_get);
        $criteria->compare('payment', $this->payment);

        return new CActiveDataProvider(
            $this, [
                'criteria' => $criteria,
                'sort' => ['defaultOrder' => 't.position']
            ]
        );
    }

    public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t("DeliveryModule.delivery", 'Active'),
            self::STATUS_NOT_ACTIVE => Yii::t("DeliveryModule.delivery", 'Not active'),
        ];
    }

    public function getStatusTitle()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t("DeliveryModule.delivery", '*unknown*');
    }

    /**
     * @param $totalPrice float - Сумма заказа
     * @return float
     */
    public function getCost($totalPrice)
    {
        if (null === $this->free_from) {
            return $this->price;
        }

        return $this->free_from < $totalPrice ? 0 : $this->price;
    }

	protected function beforeSave()
	{
		if($this->payment){
			$this->payment = json_encode($this->payment);
		}

		return parent::beforeSave();
	}


	protected function afterFind()
	{
		if($this->payment){
			$this->payment = json_decode($this->payment);
		}
		parent::afterFind();
	}

	public function getPaymentList(){
    	return [
    	    1 => 'Наличные',
		    2 => 'Банковские карты',
	    ];
	}

	/**
	 * @return array|mixed
	 */
	public function getFormattedPayment(){
		$data = [];
		$payments = $this->getPaymentList();
		foreach ($this->payment as $item){
			if($payments[$item]){
				$data[] = $payments[$item];
			}
		}

		return $data;
	}

	public function getWorktimeAsRows(){
		$times = explode(',', $this->worktime);
		if($times){
			foreach ($times as $id => $value){
				$times[$id] = trim($value);
			}
		}

		return implode('<br>', $times);
	}

	protected function afterSave()
	{
		Yii::app()->cache->delete('Pickup');
		Yii::app()->cache->delete('Pickup'.$this->group_id);

		parent::afterSave();
	}

	protected function afterDelete()
	{
		Yii::app()->cache->delete('Pickup');
		Yii::app()->cache->delete('Pickup'.$this->group_id);

		parent::afterDelete();
	}

	/**
	 * @param $id
	 * @return mixed
	 */
	public static function getPickupAddress($id){
		$pickup = Pickup::model()->published()->findByPk($id);
		if($pickup){
			return $pickup->address;
		}
	}

	/**
	 * @param $deliveryId
	 * @return array
	 */
	public static function getPickupItemByGroup($deliveryId){
		$pickups = [];
		$delivery = Delivery::model()->findByPk($deliveryId);
		if($delivery->pickup_group_id){
			$criteria = new CDbCriteria();
			$criteria->addCondition('group_id', $delivery->pickup_group_id);
			$pickups = Pickup::model()->published()->findAll($criteria);
		}

		return $pickups;
	}

    /**
     * @return array
     */
    public function getPickupGroups()
    {
        return CHtml::listData(PickupGroup::model()->findAll(['order' => 't.position']), 'id', 'name');
    }

    /**
     * @return string
     */
    public function getPickupGroupName()
    {
        $data = $this->getPickupGroups();

        return isset($data[$this->group_id]) ? $data[$this->group_id] : '---';
    }
}
