<?php

/**
 * @property integer $id
 * @property string $name
 * @property integer $region_id
 * @property integer $position
 * @property integer $status
 *
 */
class PickupGroup extends webforma\models\WModel
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 0;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{store_pickup_groups}}';
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['name, position, status', 'required'],
            ['name', 'filter', 'filter' => 'trim'],
            ['position', 'numerical', 'integerOnly' => true],
            ['name', 'length', 'max' => 255],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            [
                'id, name, status, position, status',
                'safe',
                'on' => 'search'
            ],
        ];
    }

    public function scopes()
    {
        return [
            'published' => [
                'condition' => 'status = :status',
                'params' => [':status' => self::STATUS_ACTIVE],
            ],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('DeliveryModule.delivery', 'ID'),
            'name' => Yii::t('DeliveryModule.delivery', 'Title'),
            'status' => Yii::t('DeliveryModule.delivery', 'Status'),
            'position' => Yii::t('DeliveryModule.delivery', 'Position'),
            'region_id' => 'Город',
        ];
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('position', $this->position);
        $criteria->compare('region_id', $this->region_id, true);

        return new CActiveDataProvider(
            $this, [
                'criteria' => $criteria,
                'sort' => ['defaultOrder' => 't.position']
            ]
        );
    }

    public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t("DeliveryModule.delivery", 'Active'),
            self::STATUS_NOT_ACTIVE => Yii::t("DeliveryModule.delivery", 'Not active'),
        ];
    }

    public function getStatusTitle()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t("DeliveryModule.delivery", '*unknown*');
    }

	protected function beforeDelete()
	{
		Pickup::model()->deleteAll('group_id = :group_id', [':group_id' => $this->id]);
		return parent::beforeDelete();
	}


}
