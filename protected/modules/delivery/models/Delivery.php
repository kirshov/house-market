<?php

/**
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property double $price
 * @property double $free_from
 * @property double $available_from
 * @property integer $position
 * @property integer $status
 * @property integer $separate_payment
 * @property array $pickup_group_id
 * @property string $free_text
 * @property integer $show_region
 * @property integer $need_address
 *
 * @property DeliveryCalc[] $deliveryCalc
 * @property Payment[] $paymentMethods
 *
 */
class Delivery extends webforma\models\WModel
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 0;

    const SHOW_REGION_ALL = 0;
    const SHOW_REGION_MY = 1;
    const SHOW_REGION_OTHER = 2;


    /* сюда передаются id способов оплаты, доступные для этого способа доставки*/
    public $payment_methods = [];

	public $delivery_calc = [];

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{store_delivery}}';
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['name, price, position, status', 'required'],
            ['name, free_text, track_num_link', 'filter', 'filter' => 'trim'],
            ['pickup_group_id', 'safe'],
            ['position, separate_payment, show_region, need_address', 'numerical', 'integerOnly' => true],
            ['price, free_from, available_from', 'store\components\validators\NumberValidator'],
            ['name', 'length', 'max' => 255],
            ['description, payment_methods, delivery_calc', 'safe'],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            [
                'id, name, status, position, description, price, free_from, available_from, separate_payment',
                'safe',
                'on' => 'search'
            ],
        ];
    }

    public function relations()
    {
        $relations = [
	        'deliveryCalc' => [self::HAS_ONE, 'DeliveryCalc', 'delivery_id'],
        ];
        if(Yii::app()->hasModule('payment')){
	        $relations = CMap::mergeArray($relations,
		        [
			        'paymentMethods' => [
				        self::HAS_MANY,
				        'Payment',
				        ['payment_id' => 'id'],
				        'through' => 'paymentRelation',
				        'order' => 'paymentMethods.position ASC',
				        'condition' => 'paymentMethods.status = :status',
				        'params' => [':status' => Payment::STATUS_ACTIVE]
			        ],

		        ]
	        );
        }

        return $relations;
    }

    public function scopes()
    {
        return [
            'published' => [
                'condition' => 'status = :status',
                'params' => [':status' => self::STATUS_ACTIVE],
            ],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('DeliveryModule.delivery', 'ID'),
            'name' => Yii::t('DeliveryModule.delivery', 'Title'),
            'description' => Yii::t('DeliveryModule.delivery', 'Description'),
            'status' => Yii::t('DeliveryModule.delivery', 'Status'),
            'position' => Yii::t('DeliveryModule.delivery', 'Position'),
            'price' => Yii::t('DeliveryModule.delivery', 'Price'),
            'free_from' => Yii::t('DeliveryModule.delivery', 'Free from'),
            'available_from' => Yii::t('DeliveryModule.delivery', 'Available from'),
            'separate_payment' => Yii::t('DeliveryModule.delivery', 'Separate payment'),
            'payment_methods' => Yii::t('DeliveryModule.delivery', 'Payment methods'),
            'pickup_group_id' => 'Группа самовывоза',
            'free_text' => 'Текст при бесплатной доставке',
            'show_region' => 'Показывать в регионах',
            'need_address' => 'Необходимо указать адрес доставки',
            'track_num_link' => 'Ссылка для отслеживания отправления',
        ];
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('price', $this->price);
        $criteria->compare('free_from', $this->free_from);
        $criteria->compare('available_from', $this->available_from);
        $criteria->compare('separate_payment', $this->separate_payment);
        $criteria->compare('status', $this->status);
        $criteria->compare('position', $this->position);
        $criteria->compare('description', $this->description, true);

        return new CActiveDataProvider(
            $this, [
                'criteria' => $criteria,
                'sort' => ['defaultOrder' => 't.position']
            ]
        );
    }

    public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t("DeliveryModule.delivery", 'Active'),
            self::STATUS_NOT_ACTIVE => Yii::t("DeliveryModule.delivery", 'Not active'),
        ];
    }

    public function getStatusTitle()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t("DeliveryModule.delivery", '*unknown*');
    }

    public function afterFind()
    {
        /*$this->payment_methods = array_map(
            function ($x) {
                return $x->id;
            },
            $this->paymentMethods
        );*/

        if($this->deliveryCalc){
	        $this->delivery_calc[$this->deliveryCalc->type] = $this->deliveryCalc->value;
        }

        if($this->pickup_group_id){
	        $this->pickup_group_id = json_decode($this->pickup_group_id);
        }
        parent::afterFind();
    }


	public function clearDeliveryCalc()
	{
		DeliveryCalc::model()->deleteAllByAttributes(['delivery_id' => $this->id]);
	}

    public function afterSave()
    {
        parent::afterSave();

	    $this->clearDeliveryCalc();
	    foreach ((array)$this->delivery_calc as $type => $value) {
	    	if($value){
			    $deliveryCalc = new DeliveryCalc();
			    $deliveryCalc->delivery_id = $this->id;
			    $deliveryCalc->type = $type;
			    $deliveryCalc->value = $value;
			    $deliveryCalc->save();
		    }
	    }
    }

    /**
     * @param $totalPrice float - Сумма заказа
     * @return float
     */
    public function getCost($totalPrice)
    {
        if (null === $this->free_from) {
            return $this->price;
        }

        try{
	        if($this->delivery_calc && $this->deliveryCalc){
		        if($this->deliveryCalc->type == 'cdek' && Yii::app()->hasModule('cdek_calc')){
		        	Yii::app()->getModule('cdek_calc')->setCityId();

			        $calcProcess = new CdekCalcProcess();
			        $this->price = $calcProcess->getDeliveryPrice($this->deliveryCalc->value);
		        }
	        }
        } catch (Exception $exception){

        }

        return $this->free_from < $totalPrice ? 0 : $this->price;
    }

    public function hasPaymentMethods()
    {
        return count($this->paymentRelation);
    }

    public function checkAvailable(Order $order)
    {
        return $order->getProductsCost() >= $this->available_from;
    }

    public function findById($id)
    {
        return $this->findByPk($id);
    }

	protected function beforeSave()
	{
		if($this->pickup_group_id){
			$this->pickup_group_id = json_encode($this->pickup_group_id);
		} else {
			$this->pickup_group_id = null;
		}
		return parent::beforeSave();
	}

	public function getRegionList(){
    	return [
    		0 => 'Во всех',
		    1 => 'Только в моем',
		    2 => 'Только в другом',
	    ];
	}

	public function getShowInRegion(){
		$data = $this->getRegionList();
		return $data[$this->show_region];
	}

	/**
	 * @return Delivery[]
	 */
	public static function getAll(){
		return Delivery::model()->published()->findAll(['order' => 'position']);
	}

	/**
	 * @return array
	 */
	public static function getOptionsForDropdown(){
		$options = [];
		/* @var $delivery Delivery */
		foreach ((array)Delivery::model()->with('deliveryCalc')->findAll() as $delivery) {
			$options[$delivery->id] = [
				'data-separate-payment' => $delivery->separate_payment,
				'data-price' => $delivery->price,
				'data-available-from' => $delivery->available_from,
				'data-pickup-group' => (int) $delivery->pickup_group_id,
			];
		};

		return $options;
	}
}
