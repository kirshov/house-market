<?php

class m180408_124848_add_pickup extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->createTable(
			"{{store_pickup_groups}}",
			[
				"id" => "pk",
				"name" => "varchar(255) not null",
				"region_id" => "integer default null",
				"position" => "integer not null default '1'",
				"status" => "tinyint not null default '1'",
			],
			$this->getOptions()
		);
		$this->createIndex("idx_{{store_pickup_groups}}_position", "{{store_pickup_groups}}", "position");

		$this->createTable(
			"{{store_pickup}}",
			[
				"id" => "pk",
				"name" => "varchar(255) not null",
				"coords" => "varchar(255) not null",
				"group_id" => "integer not null default '1'",
				"price" => "float(10, 2) null",
				"free_from" => "float(10, 2) null",
				"position" => "integer not null default '1'",
				"status" => "tinyint not null default '1'",
			],
			$this->getOptions()
		);
		$this->createIndex("idx_{{store_pickup}}_position", "{{store_pickup}}", "position");
	}

	public function safeDown()
	{

	}
}