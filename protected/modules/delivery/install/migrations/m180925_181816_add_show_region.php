<?php

class m180925_181816_add_show_region extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_delivery}}', 'show_region', 'tinyint(3) default 0');
	}

	public function safeDown()
	{

	}
}