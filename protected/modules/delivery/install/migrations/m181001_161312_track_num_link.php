<?php

class m181001_161312_track_num_link extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_delivery}}', 'track_num_link', 'varchar(128) default null');
	}

	public function safeDown()
	{

	}
}