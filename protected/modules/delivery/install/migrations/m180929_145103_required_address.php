<?php

class m180929_145103_required_address extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_delivery}}', 'need_address', 'tinyint(1) default 0');
	}

	public function safeDown()
	{

	}
}