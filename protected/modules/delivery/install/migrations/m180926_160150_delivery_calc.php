<?php

class m180926_160150_delivery_calc extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->createTable(
			"{{store_delivery_calc}}",
			[
				"id" => "pk",
				"delivery_id" => "int(11) default null",
				"type" => "varchar(32) not null",
				"value" => "varchar(32) not null",
			],
			$this->getOptions()
		);
		$this->createIndex("idx_{{store_delivery_calc}}_delivery_id", "{{store_delivery_calc}}", "delivery_id");
		$this->addForeignKey("fk_{{store_delivery_calc}}_delivery_id", "{{store_delivery_calc}}", "delivery_id", "{{store_delivery}}", "id", "CASCADE", "CASCADE");
	}

	public function safeDown()
	{

	}
}