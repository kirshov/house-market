<?php

class m180323_105448_cost_change extends webforma\components\DbMigration
{
	public function safeUp()
    {
        $this->alterColumn('{{store_delivery}}', 'price', 'float(10, 0) not null default "0"');
        $this->alterColumn('{{store_delivery}}', 'free_from', 'float(10, 0) not null default "0"');
        $this->alterColumn('{{store_delivery}}', 'available_from', 'float(10, 0) not null default "0"');
    }

	public function safeDown()
	{

	}
}