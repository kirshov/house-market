<?php

class m180510_120744_add_free_text extends webforma\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_delivery}}', 'free_text', 'varchar(255) default null');
	}

	public function safeDown()
	{

	}
}