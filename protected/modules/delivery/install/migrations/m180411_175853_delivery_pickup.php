<?php

class m180411_175853_delivery_pickup extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_delivery}}', 'pickup_group_id', 'text not null default ""');
	}

	public function safeDown()
	{

	}
}