<?php

class m180409_043435_add_pickup_fields extends webforma\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_pickup}}', 'address', 'varchar(255) not null default ""');
        $this->addColumn('{{store_pickup}}', 'worktime', 'varchar(255) not null default ""');
        $this->addColumn('{{store_pickup}}', 'description', 'text not null default ""');
        $this->addColumn('{{store_pickup}}', 'how_to_get', 'text not null default ""');
        $this->addColumn('{{store_pickup}}', 'payment', 'text not null default ""');
	}

	public function safeDown()
	{

	}
}