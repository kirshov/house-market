<?php

class m180625_104007_set_sort extends webforma\components\DbMigration
{
	public function safeUp()
	{
        $this->update('{{store_pickup}}', ['position' => new CDbExpression('id')]);
	}

	public function safeDown()
	{

	}
}