<?php

return [
    'module' => [
        'class' => 'application.modules.delivery.DeliveryModule',
    ],
    'import' => [
        'application.modules.delivery.models.*',
    ],
	'params' => [
		'widgets' => [
			'pickup' => 'application.modules.delivery.widgets.PickupWidget',
			'deliveryCost' => 'application.modules.delivery.widgets.DeliveryCostWidget',
		]
	],
];
