<?php
use webforma\components\WebModule;

/**
 * Class DeliveryModule
 */
class DeliveryModule extends WebModule
{
    /**
     *
     */
    public $adminMenuOrder = 3;

	/**
	 * @var
	 */
    public $myCity;

    /**
     * @return array
     */
    public function getDependencies()
    {
        return ['store'];
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
        return [
        	'myCity'
        ];
    }

	/**
	 * @return array
	 */
	public function getParamsLabels()
	{
		return [
			'myCity' => 'Мой город',
		];
	}


	/**
	 * @return array
	 */
	public function getEditableParamsGroups()
	{
		return [
			'main' => [
				'label' => 'Основные настройки',
				'items' => [
					'myCity',
				]
			],
		];
	}


	/**
     * @return string
     */
    public function getCategory()
    {
	    return Yii::t('DeliveryModule.delivery', 'Store');
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
	    $hasMidTariff = Helper::hasMidTariff();
        return [
            [
                'label' => 'Способы доставки',
            ],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('DeliveryModule.delivery', 'Delivery lists'),
                'url' => ['/delivery/deliveryBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('DeliveryModule.delivery', 'Create delivery'),
                'url' => ['/delivery/deliveryBackend/create'],
            ],
            [
                'label' => 'Группы самовывоза',
	            'visible' => $hasMidTariff,
            ],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('DeliveryModule.delivery', 'Manage pickup group methods'),
                'url' => ['/delivery/pickupGroupBackend/index'],
	            'visible' => $hasMidTariff,
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('DeliveryModule.delivery', 'Create pickup group'),
                'url' => ['/delivery/pickupGroupBackend/create'],
	            'visible' => $hasMidTariff,
            ],
            [
                'label' => 'Пункты самовывоза',
	            'visible' => $hasMidTariff,
            ],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('DeliveryModule.delivery', 'Manage pickup methods'),
                'url' => ['/delivery/pickupBackend/index'],
	            'visible' => $hasMidTariff,
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('DeliveryModule.delivery', 'Create pickup'),
                'url' => ['/delivery/pickupBackend/create'],
	            'visible' => $hasMidTariff,
            ],
        ];
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/delivery/deliveryBackend/index';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('DeliveryModule.delivery', 'Delivery');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('DeliveryModule.delivery', 'Delivery module');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-car';
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'delivery.models.*',
            ]
        );
    }
}
