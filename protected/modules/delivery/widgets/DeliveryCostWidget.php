<?php
class DeliveryCostWidget extends \webforma\widgets\WWidget
{
	/**
	 * @var Product
	 */
	public $product;

	public function init()
	{
		parent::init();

		Yii::app()->setParams(['needChangeCity' => true]);

		Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getAssetManager()->publish(
			Yii::getPathOfAlias('application.modules.delivery.views.web') . '/delivery-cost.js'
		), CClientScript::POS_END);
	}

	public function run()
	{
	    $isCdek = false;
	    if(Yii::app()->hasModule('cdek_calc')){
	        $cCdek = Yii::app()->getModule('cdek_calc');
	        $isCdek = !empty($cCdek->cityFrom);
        }

		$this->render('index', [
			'product' => $this->product,
            'isCdek' => $isCdek,
		]);
	}
}