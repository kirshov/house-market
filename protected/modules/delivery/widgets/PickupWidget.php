<?php

/**
 * Class PickupWidget
 */
class PickupWidget extends webforma\widgets\WWidget
{
    /**
     * @var
     */
    public $group;

	/**
	 * @var
	 */
	public $deliveryId;

    /**
     * @var string
     */
    public $view = 'default';

	/**
	 * @throws CException
	 */
	protected function connectScript()
	{
		Yii::app()->getClientScript()->registerScriptFile('https://api-maps.yandex.ru/2.1/?lang=ru_RU', CClientScript::POS_END);

		Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getAssetManager()->publish(
			Yii::getPathOfAlias('application.modules.delivery.views.web') . '/pickup.js'
		), CClientScript::POS_END);
		parent::init();
	}

    /**
     * @throws CException
     */
    public function run()
    {
        $cacheName = "Pickup{$this->group}";

	    $pickupItems = Yii::app()->getCache()->get($cacheName);
        $criteria = new CDbCriteria();
        $criteria->order = 'position';
        if ($pickupItems === false) {
			if(is_numeric($this->group)){
			    $criteria->condition = 'group_id = :group_id';
			    $criteria->params[':group_id'] = $this->group;
				$pickupItems = Pickup::model()->published()->findAll($criteria);
			} else {
				$pickupItems = Pickup::model()->published()->findAll($criteria);
			}

            Yii::app()->getCache()->set($cacheName, $pickupItems);
        }

        if(!empty($pickupItems)){
        	$this->connectScript();
        }

        $this->render($this->view, [
	        'items' => $pickupItems,
	        'deliveryId' => $this->deliveryId,
        ]);
    }
}
