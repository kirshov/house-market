<?php

Yii::import('application.modules.promo.PromoModule');
Yii::import('application.modules.promo.models.*');

/**
 * Class PromoWidget
 */
class PromoWidget extends webforma\widgets\WWidget
{
    /**
     * @var string
     */
    public $view = 'default';

	/**
	 * @var int
	 */
    public $limit = 2;

	/**
	 * @var int
	 */
	public $width = 100;

	/**
	 * @var int
	 */
	public $height = 100;

    /**
     * @throws CException
     */
    public function run()
    {
        $criteria = new CDbCriteria();
        $criteria->limit = (int)$this->limit;
        $criteria->order = 't.date_end DESC';

        $articles = Promo::model()->published()->actual()->findAll($criteria);

        $this->render($this->view, [
        	'models' => $articles,
	        'width' => $this->width,
	        'height' => $this->height,
        ]);
    }
}
