<?php

class m181003_170642_promo_images extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{promo}}', 'image', 'varchar(255) default null');
	}

	public function safeDown()
	{

	}
}