<?php

class m180905_164029_promo_base extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->createTable(
			"{{promo}}",
			[
				"id" => "pk",
				"name" => "varchar(250) not null",
				"slug" => "varchar(150) not null",
				"title" => "varchar(250) not null",
				"description" => "text",
				"description_bottom" => "text",
				"meta_title" => "varchar(250) default null",
				"meta_keywords" => "varchar(250) default null",
				"meta_description" => "varchar(250) default null",
				"status" => "tinyint not null default '1'",
				'date_start' => 'date not null',
				'date_end' => 'date not null',
				'ids' => 'text',
				'yandex_type' => 'tinyint(1) default null',
				'text_on_yandex' => 'text default null',
				'condition' => 'text default null',
				'npm_quantity' => 'tinyint(1) default null',
				'npm_free_quantity' => 'tinyint(1) default null',
			],
			$this->getOptions()
		);

		$this->createIndex("ux_{{promo}}_slug", "{{promo}}", "slug", true);
		$this->createIndex("ix_{{promo}}_status", "{{promo}}", "status", false);
	}

	public function safeDown()
	{

	}
}