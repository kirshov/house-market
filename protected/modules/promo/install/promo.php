<?php

return [
    'module' => [
        'class' => 'application.modules.promo.PromoModule',
    ],
    'import' => [
        'application.modules.promo.models.*',
    ],
	'params' => [
		'widgets' => [
			'promo' => 'application.modules.promo.widgets.PromoWidget',
		],
	],
    'rules' => [
        '/promo' => 'promo/promo/index',
	    '/promo/<slug:[\w_\/-]+>' => 'promo/promo/view',
    ],
];
