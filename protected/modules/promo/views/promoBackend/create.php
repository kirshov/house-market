<?php
$this->breadcrumbs = [
	'Акции' => ['/promo/promoBackend/index'],
    Yii::t('PromoModule.promo', 'Creating'),
];

$this->pageTitle = Yii::t('PromoModule.promo', 'Categories - create');

require_once '_menu.php';
?>

<?= $this->renderPartial('_form', [
	'model' => $model,
]); ?>
