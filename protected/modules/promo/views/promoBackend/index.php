<?php
$this->breadcrumbs = [
    'Акции' => ['/promo/promoBackend/index'],
    'Управление',
];

$this->pageTitle = 'Акции';

require_once '_menu.php';
?>

<?php $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'category-grid',
        'sortableRows' => true,
        'sortableAjaxSave' => true,
        'sortableAttribute' => 'sort',
        'sortableAction' => '/promo/promoBackend/sortable',
        'type' => 'condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'actionsButtons' => [
            CHtml::link(
                Yii::t('WebformaModule.webforma', 'Add'),
                ['/promo/promoBackend/create'],
                ['class' => 'btn btn-success pull-right btn-sm']
            ),
        ],
        'columns' => [
            [
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::image($data->getImageUrl(100, 100), $data->title, ["width" => 100, "height" => 100, "class" => "img-thumbnail"]);
                },
                'htmlOptions' => [
                    'style' => 'width: 80px;',
                ],
                'filter' => CHtml::activeTextField($model, 'id', ['class' => 'form-control']),
            ],
            [
                'name' => 'name',
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::link($data->name, array("/promo/promoBackend/update", "id" => $data->id));
                },
            ],
            /*[
                'name' => 'slug',
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::link($data->slug, array("/promo/promoBackend/update", "id" => $data->id));
                },
            ],*/
            [
                'name' => 'date_end',
                'type' => 'raw',
                'value' => function ($data) {
                    return $data->date_end;
                },
            ],
            [
                'class' => 'webforma\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('/promo/promoBackend/inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    StoreCategory::STATUS_PUBLISHED => ['class' => 'label-success'],
                    StoreCategory::STATUS_DRAFT => ['class' => 'label-default'],
                ],
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
                'frontViewButtonUrl' => function($data){
                    return Yii::app()->createUrl('/promo/promo/view', ['slug' => $data->slug]);
                },
                'buttons' => [
                    'view' => [
                        'visible' => function ($row, $data) {
                            return false;
                        },
                    ],
                    'front_view' => [
                        'visible' => function ($row, $data) {
                            return $data->status == Promo::STATUS_PUBLISHED;
                        },
                    ],
                ],
            ],
        ],
    ]
); ?>
