<?php
$this->menu = [
	[
		'icon' => 'fa fa-fw fa-list-alt',
		'label' => 'Список акций',
		'url' => ['/promo/promoBackend/index'],
	],
	[
		'icon' => 'fa fa-fw fa-plus-square',
		'label' => 'Добавить акцию',
		'url' => ['/promo/promoBackend/create'],
	],
];