<?php
/**
 * @var $this PromoBackendController
 * @var $form \webforma\widgets\ActiveForm
 */
$form = $this->beginWidget(
    '\webforma\widgets\ActiveForm',
    [
        'id'                     => 'category-form',
	    'enableAjaxValidation' => false,
	    'enableClientValidation' => false,
	    'type' => 'vertical',
	    'htmlOptions' => ['class' => 'well sticky', 'enctype' => 'multipart/form-data'],
	    'clientOptions' => [
		    'validateOnSubmit' => true,
	    ],
    ]
); ?>

<?= $form->errorSummary($model); ?>

<div class='row'>
	<div class="col-sm-6">
		<div class='row'>
			<div class="col-sm-12">
				<?= $form->textFieldGroup($model, 'name'); ?>
			</div>
		</div>
		<div class='row'>
			<div class="col-sm-12">
				<?if($model->isNewRecord){
					echo $form->slugFieldGroup($model, 'slug', ['sourceAttribute' => 'name']);
				} else {
					echo $form->textFieldGroup($model, 'slug');
				}?>
			</div>
		</div>


		<div class="row">
			<div class="col-sm-6">
				<?= $form->datePickerGroup($model, 'date_start', [
					'widgetOptions' => [
						'options' => [
							'format' => 'dd.mm.yyyy',
							'weekStart' => 1,
							'autoclose' => true,
						],
					],
					'cols' => 2,
					'prepend' => '<i class="fa fa-calendar"></i>',
				]);
				?>
			</div>

			<div class="col-sm-6">
				<?= $form->datePickerGroup( $model, 'date_end', [
					'widgetOptions' => [
						'options' => [
							'format' => 'dd.mm.yyyy',
							'weekStart' => 1,
							'autoclose' => true,
						],
					],
					'cols' => 2,
					'prepend' => '<i class="fa fa-calendar"></i>',
				]);
				?>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<?= $form->textAreaGroup($model, 'ids'); ?>
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class='row'>
			<div class="col-sx-8 col-sm-8 col-sm-offset-2">
				<?php $this->widget('webforma\widgets\WInputFile', [
					'model' => $model,
					'attribute' => 'image',
				]); ?>
			</div>
		</div>
	</div>
</div>

<div class='row'>
    <div class="col-sm-12 <?= $model->hasErrors('description') ? 'has-error' : ''; ?>">
        <?= $form->labelEx($model, 'description'); ?>
        <?php $this->widget($this->module->getVisualEditor(), [
			'model'     => $model,
			'attribute' => 'description',
		]); ?>
		<p></p>
    </div>
</div>
<?/*
<div class='row'>
    <div class="col-sm-12 <?= $model->hasErrors('condition') ? 'has-error' : ''; ?>">
        <?= $form->labelEx($model, 'condition'); ?>
        <?php $this->widget(
            $this->module->getVisualEditor(),
            [
                'model'     => $model,
                'attribute' => 'condition',
            ]
        ); ?>
        <p></p>
    </div>
</div>
*/?>
<div class='row'>
    <div class="col-sm-12 <?= $model->hasErrors('description_bottom') ? 'has-error' : ''; ?>">
        <?= $form->labelEx($model, 'description_bottom'); ?>
        <?php $this->widget(
            $this->module->getVisualEditor(),
            [
                'model'     => $model,
                'attribute' => 'description_bottom',
            ]
        ); ?>
		<p></p>
    </div>
</div>

<div class='row'>
	<div class="col-sm-6">
		<?= $form->dropDownListGroup($model, 'status', [
			'widgetOptions' => [
				'data' => $model->getStatusList(),
			],
		]); ?>
	</div>
</div>


<?php $collapse = $this->beginWidget('bootstrap.widgets.TbCollapse'); ?>
<div class="panel-group" id="ya-market-options">
	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="panel-title">
				<a data-toggle="collapse" data-parent="#ya-market-options" href="#collapseYaMarket">
					Настройки выгрузки в Яндекс.Маркет
				</a>
			</div>
		</div>
		<div id="collapseYaMarket" class="panel-collapse collapse">
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-12">
						<?= $form->dropDownListGroup($model, 'yandex_type', [
							'widgetOptions' => [
								'data' => $model->getYandexMarketTypes(),
								'htmlOptions' => [
									'empty' => '---',
								],
							],
						]); ?>
					</div>
				</div>

				<div class='row yandex-wrap<?=($model->yandex_type == 0 ? ' hidden' : '')?>'>
					<div class="col-sm-12">
						<?= $form->textAreaGroup($model, 'text_on_yandex'); ?>
					</div>
				</div>

				<div class='row yandex-npm-wrap<?=($model->yandex_type == Promo::YA_TYPE_NPM ? '' : ' hidden')?>'>
					<div class="col-sm-6">
						<?= $form->numberFieldGroup($model, 'npm_quantity', ['type' => 'number', 'cols' => 2]); ?>
					</div>

					<div class="col-sm-6">
						<?= $form->numberFieldGroup($model, 'npm_free_quantity', ['cols' => 2]); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>

<?php $collapse = $this->beginWidget('bootstrap.widgets.TbCollapse'); ?>
<div class="panel-group" id="extended-options">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">
                <a data-toggle="collapse" data-parent="#extended-options" href="#collapseOne">
                    <?= Yii::t('PromoModule.promo', 'Data for SEO'); ?>
                </a>
            </div>
        </div>
        <div id="collapseOne" class="panel-collapse collapse">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12">
                        <?= $form->textFieldGroup($model, 'title'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <?= $form->textFieldGroup($model, 'meta_title'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <?= $form->textFieldGroup($model, 'meta_keywords'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <?= $form->textAreaGroup($model, 'meta_description'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>

<div class="buttons">
	<?php $this->widget(
		'bootstrap.widgets.TbButton',
		[
			'buttonType' => 'submit',
			'context'    => 'primary',
			'label'      => $model->isNewRecord
				? 'Создать и продолжить'
				: 'Сохранить и продолжить',
		]
	); ?>

	<?php $this->widget(
		'bootstrap.widgets.TbButton',
		[
			'buttonType'  => 'submit',
			'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
			'label'       => $model->isNewRecord
				? 'Создать и закрыть'
				: 'Сохранить и закрыть',
		]
	); ?>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    $(function(){
        $('#Promo_yandex_type').on('change', function(){
            var val = $(this).val();
            if(val == 0){
                $('.row.yandex-wrap').addClass('hidden');
            } else {
                $('.row.yandex-wrap').removeClass('hidden');

				if(val == 3){
					$('.row.yandex-npm-wrap').removeClass('hidden');
				} else {
					$('.row.yandex-npm-wrap').addClass('hidden');
				}
            }
        })
    });
</script>
