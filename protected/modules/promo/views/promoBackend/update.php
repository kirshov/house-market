<?php
$this->breadcrumbs = [
	'Акции' => ['/promo/promoBackend/index'],
    'Редактирование «'.$model->name.'»',
];

$this->pageTitle = 'Акции - редактирование';

require_once '_menu.php';

$this->menu = CMap::mergeArray($this->menu, [
	[
		'icon' => 'fa fa-fw fa-external-link-square',
		'label' => 'Посмотреть на сайте',
		'url' => [
			'/promo/promo/view',
			'slug' => $model->slug
		],
		'linkOptions' => [
			'target' => '_blank',
		]
	],
    [
        'icon' => 'fa fa-fw fa-pencil',
        'label' => 'Изменить акцию',
        'url' => [
            '/promo/promoBackend/update',
            'id' => $model->id
        ]
    ],
    [
        'icon' => 'fa fa-fw fa-trash-o',
	    'label' => 'Удалить акцию',
        'url' => '#',
        'linkOptions' => [
            'submit' => ['/promo/promoBackend/delete', 'id' => $model->id],
            'params' => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
            'confirm' => 'Вы действительно хотите удалить акцию?',
            'csrf' => true,
        ]
    ],
]);
$this->title = 'Редактирование акции';
$this->subTitle = '«'.$model->name.'»';
?>

<?= $this->renderPartial('_form', [
	'model' => $model,
]); ?>
