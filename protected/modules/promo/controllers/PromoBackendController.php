<?php

class PromoBackendController extends webforma\components\controllers\BackController
{
    public function actions()
    {
        return [
            'inline' => [
                'class'           => 'webforma\components\actions\WInLineEditAction',
                'model'           => 'Promo',
                'validAttributes' => [
                    'status',
                    'slug',
                ]
            ],
            'sortable' => [
                'class' => 'webforma\components\actions\SortAction',
                'model' => 'Promo',
                'attribute' => 'sort'
            ]
        ];
    }

    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin'],],
            ['allow', 'actions' => ['index'], 'roles' => ['Store.CategoryBackend.Index'],],
            ['allow', 'actions' => ['create'], 'roles' => ['Store.CategoryBackend.Create'],],
            ['allow', 'actions' => ['update', 'inline', 'sortable'], 'roles' => ['Store.CategoryBackend.Update'],],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Store.CategoryBackend.Delete'],],
            ['deny',],
        ];
    }

    /**
     *
     * @return void
     */
    public function actionCreate()
    {
        $model = new Promo;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
	    $filters = '';
        if (($data = Yii::app()->getRequest()->getPost('Promo')) !== null) {

            $model->setAttributes($data);

            if ($model->save()) {

                Yii::app()->user->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('PromoModule.promo', 'Record was created!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['create']
                    )
                );
            }
        }

        $this->render('create', [
        	'model' => $model,
	        'filters' => $filters
        ]);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id the ID of the model to be updated
     *
     * @return void
     */
    public function actionUpdate($id)
    {
        // Указан ID новости страницы, редактируем только ее
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (($data = Yii::app()->getRequest()->getPost('Promo')) !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('Promo'));

            if ($model->save()) {

                Yii::app()->user->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    'Акция успешно сохранена!'
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id,
                        ]
                    )
                );
            }
        }

        $this->render(
            'update',
            [
                'model' => $model,
            ]
        );
    }

    /**
     * @param integer $id
     *
     * @return void
     *
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            $transaction = Yii::app()->db->beginTransaction();

            try {
                // поддерживаем удаление только из POST-запроса
                $this->loadModel($id)->delete();
                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser

                $transaction->commit();

                if (!isset($_GET['ajax'])) {
                    $this->redirect(
                        (array)Yii::app()->getRequest()->getPost('returnUrl', 'index')
                    );
                }
            } catch (Exception $e) {
                $transaction->rollback();

                Yii::log($e->__toString(), CLogger::LEVEL_ERROR);
            }

        } else {
            throw new CHttpException(
                400,
                Yii::t('PromoModule.promo', 'Bad request. Please don\'t use similar requests anymore')
            );
        }
    }

    /**
     * @return void
     */
    public function actionIndex()
    {
        $model = new Promo('search');
        $model->unsetAttributes();  // clear any default values

        if (isset($_GET['Promo'])) {
            $model->setAttributes($_GET['Promo']);
        }

        $this->render('index', ['model' => $model]);
    }

    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param integer $id идентификатор нужной модели
     *
     * @return Promo $model
     *
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Promo::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('PromoModule.promo', 'Page not found!'));
        }
        return $model;
    }

    /**
     * Производит AJAX-валидацию
     *
     * @param Promo $model модель, которую необходимо валидировать
     *
     * @return void
     */
    protected function performAjaxValidation(Promo $model)
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest() && Yii::app()->getRequest()->getPost('ajax') === 'category-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
