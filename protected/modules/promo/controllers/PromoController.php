<?php
Yii::import('application.modules.store.StoreModule');
/**
 * Class PromoController
 */
class PromoController extends \webforma\components\controllers\FrontController
{
    public $pageSize = 20;

	public function actionIndex()
	{
        $pagination = [
            'pageSize' => (int)$this->pageSize,
            'pageVar' => 'page',
        ];

        $criteria = new CDbCriteria();
        $criteria->condition = 't.status = '.Promo::STATUS_PUBLISHED.' AND t.date_end >= NOW()';
        //$criteria->limit = $this->limit;

        $dataProvider = new CActiveDataProvider(
            Promo::model(),
            [
                'criteria' => $criteria,
                'pagination' => $pagination,
                'sort' => [
                    'defaultOrder' => 't.date_start DESC',
                ],
            ]
        );

		$this->render('index', [
			'dataProvider' => $dataProvider,
		]);
	}

	/**
     * @param $slug
     * @throws CHttpException
     */
    public function actionView($slug)
    {
	    /**
	     * @var Promo $group
	     */
        $promo = Promo::model()->published()->find('slug = :slug', [':slug' => $slug]);

        if(!$promo){
            throw new CHttpException(404);
        }

        if(strtotime($promo->date_end) > time()){
	        $view = 'view';
	        $dataProvider = $promo->getIds()
		        ? Yii::app()->getComponent('productRepository')->getByIds($promo->getIds())
		        : null;
        } else {
	        $view = 'view_end';
	        $dataProvider = null;
        }
        $this->render($view, [
            'dataProvider' => $dataProvider,
            'promo' => $promo,
        ]);
    }
}