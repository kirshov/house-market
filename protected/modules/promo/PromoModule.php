<?php

use webforma\components\WebModule;

/**
 * Class PromoModule
 */
class PromoModule extends WebModule
{
    /**
     *
     */
    public $adminMenuOrder = 6;

    /**
     * @var
     */
    public $promoH1;

    /**
     * @var
     */
    public $metaPromoTitle;

    /**
     * @var
     */
    public $metaPromoDescription;

    /**
     * @var
     */
    public $metaPromoKeywords;

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            'store',
        ];
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
        return [
	        'promoH1',
	        'metaPromoTitle',
	        'metaPromoKeywords',
	        'metaPromoDescription',
        ];
    }

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [
            'promoH1' => 'H1 для страницы акций',
            'metaPromoTitle' => 'Title для страницы акций',
            'metaPromoKeywords' => 'Keywords для страницы акций',
            'metaPromoDescription' => 'Descriptions для страницы акций',
        ];
    }

    /**
     * @return array
     */
    public function getEditableParamsGroups()
    {
        return [
            '0.seoPromo' => [
                'label' => 'Настройки страницы акций',
                'items' => [
                    'promoH1',
                    'metaPromoTitle',
                    'metaPromoDescription',
                    'metaPromoKeywords',
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
	    return [
		    [
			    'icon' => 'fa fa-fw fa-list-alt',
			    'label' => 'Список акций',
			    'url' => ['/promo/promoBackend/index'],
		    ],
		    [
			    'icon' => 'fa fa-fw fa-plus-square',
			    'label' => 'Добавить акцию',
			    'url' => ['/promo/promoBackend/create'],
		    ],
	    ];
    }

    /**
     * @return bool
     */
    public function getIsShowInAdminMenu()
    {
        return true;
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/promo/promoBackend/index';
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('PromoModule.promo', 'Store');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Акции';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return '';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-money';
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'application.modules.promo.models.*',
                'application.modules.sitemap.components.SitemapHelper',
            ]
        );
    }
}
