<?php

/**
 * @property string $id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property string $description_bottom
 * @property string $title
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property integer $status
 * @property string $date_start
 * @property string $date_end
 * @property string $ids
 * @property integer $yandex_type
 * @property string $text_on_yandex
 * @property string $condition
 * @property string $npm_quantity
 * @property string $npm_free_quantity
 * @property string $image
 *
 * @method Promo actual
 * @method Promo published
 * @method getImageUrl
 *
 */
class Promo extends \webforma\models\WModel
{
    /**
     *
     */
    const STATUS_DRAFT = 0;
    /**
     *
     */
    const STATUS_PUBLISHED = 1;

	/**
	 *
	 */
	const YA_TYPE_PRICE = 2;

	/**
	 *
	 */
	const YA_TYPE_NPM = 3;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{promo}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className
     * @return StoreCategory the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            [
                'name, title, description, description_bottom, slug, meta_title, meta_keywords, meta_description, date_start, date_end, ids, text_on_yandex, condition, image',
                'filter',
                'filter' => 'trim',
            ],
            ['name, slug, date_start, date_end', 'required'],
            ['status, yandex_type', 'numerical', 'integerOnly' => true],
            ['status', 'length', 'max' => 11],
            ['status', 'numerical', 'integerOnly' => true],
            ['name, title, meta_title, meta_keywords, meta_description', 'length', 'max' => 250],
            ['slug', 'length', 'max' => 150],
	        ['npm_quantity, npm_free_quantity', 'numerical', 'integerOnly' => true, 'allowEmpty' => true],
            [
                'slug',
                'webforma\components\validators\YSLugValidator',
                'message' => Yii::t('PromoModule.promo', 'Bad characters in {attribute} field'),
            ],
            ['slug', 'unique'],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            ['id, name, description, description_bottom, slug, status', 'safe', 'on' => 'search'],
        ];
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [];
	}

    /**
     * @return array
     */
    public function behaviors()
    {
    	$module = Yii::app()->getModule('webforma');
        return [
            'date_start' => [
                'class' => 'webforma\components\behaviors\DateFormatBehavior',
                'attributeName' => 'date_start',
                'dbFormat' => 'Y-m-d',
                'siteFormat' => 'd.m.Y',
            ],
            'date_end' => [
                'class' => 'webforma\components\behaviors\DateFormatBehavior',
                'attributeName' => 'date_end',
                'dbFormat' => 'Y-m-d',
                'siteFormat' => 'd.m.Y',
            ],
	        'upload' => [
		        'class' => 'webforma\components\behaviors\ImageUploadBehavior',
		        'attributeName' => 'image',
		        'uploadPath' => $module->uploadPath.'/promo',
	        ],
	        'content' => [
		        'class' => 'webforma\components\behaviors\PrepareContentBehavior',
		        'attributeName' => 'description',
	        ],
	        'content_bottom' => [
		        'class' => 'webforma\components\behaviors\PrepareContentBehavior',
		        'attributeName' => 'description_bottom',
	        ],
        ];
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'published' => [
                'condition' => 't.status = :status',
                'params' => [':status' => self::STATUS_PUBLISHED],
            ],
	        'actual' => [
		        'condition' => 't.date_end > NOW()',
	        ],
        ];
    }

	/**
     *
     */
    protected function afterSave()
    {
	    Yii::app()->cache->delete('promo::slugs');
	    SitemapHelper::remove();

        return parent::afterSave();
    }

    /**
     *
     */
    public function afterDelete()
    {
	    Yii::app()->cache->delete('promo::slugs');
	    SitemapHelper::remove();

        parent::afterDelete();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('PromoModule.promo', 'Id'),
            'name' => Yii::t('PromoModule.promo', 'Name'),
            'description' => Yii::t('PromoModule.promo', 'Description'),
	        'description_bottom' => 'Описание под списком товаров',
	        'slug' => Yii::t('PromoModule.promo', 'Alias'),
            'meta_title' => Yii::t('PromoModule.promo', 'Meta title'),
            'meta_keywords' => Yii::t('PromoModule.promo', 'Meta keywords'),
            'meta_description' => Yii::t('PromoModule.promo', 'Meta description'),
            'status' => Yii::t('PromoModule.promo', 'Status'),
            'title' => Yii::t('PromoModule.promo', 'SEO_Title'),
            'ids' => 'Список ID товаров',
            'date_start' => 'Дата начала',
            'date_end' => 'Дата завершения',
            'yandex_type' => 'Тип акции в Яндекс.Маркете',
            'text_on_yandex' => 'Описание акции в Яндекс.Маркете',
            'condition' => 'Условия акции в позиции',
            'npm_quantity' => 'Количество товаров к покупке',
            'npm_free_quantity' => 'Количество товаров в подарок',
	        'image' => 'Изображение',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('slug', $this->slug, true);
        $criteria->compare('meta_title', $this->meta_title, true);
        $criteria->compare('meta_keywords', $this->meta_keywords, true);
        $criteria->compare('meta_description', $this->meta_description, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider(
            Promo::_CLASS_(),
            [
                'criteria' => $criteria,
                'sort' => ['defaultOrder' => 't.date_start'],
            ]
        );
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_DRAFT => Yii::t('PromoModule.promo', 'Draft'),
            self::STATUS_PUBLISHED => Yii::t('PromoModule.promo', 'Published'),
        ];
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('PromoModule.promo', '*unknown*');
    }

    /**
     * @return string
     */
    public function getParentName()
    {
        return $this->parent ? $this->parent->name : '---';
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title ?: $this->name;
    }

    /**
     * @return string
     */
    public function getMetaTitle()
    {
        if($this->meta_title){
            return $this->meta_title;
        }

        return Helper::prepareSeoTitle($this->getTitle());
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
        if($this->meta_description){
            return $this->meta_description;
        }
    }

    /**
     * @return string
     */
    public function getMetaKeywords()
    {
        if($this->meta_keywords){
            return $this->meta_keywords;
        }
    }

    /**
     * Get canonical url
     *
     * @return string
     */
    public function getMetaCanonical()
    {
        list($address, $params) = explode('?', Yii::app()->getRequest()->getUrl());

        if($params){
            return $address;
        }
    }

    /**
     * @return array
     */
    public function getIds(){
    	$ids = [];
    	$data = explode(',', $this->ids);
    	if($data){
		    foreach ($data as $item) {
			    $item = trim($item);
			    if(is_numeric($item)){
				    $ids[] = $item;
			    }
    		}
	    }
	    return $ids;
    }

    /**
     * @return array
     */
    public function getYandexMarketTypes(){
        return [
            //1 => 'Промокод',
            2 => 'Специальная цена',
            3 => 'N + M',
        ];
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getYandexMarketTypeCode($id){
        $data = [
            1 => 'promo code',
            2 => 'flash discount',
            3 => 'n plus m',
            4 => 'gift with purchase',
        ];

        return $data[$id];
    }
}
