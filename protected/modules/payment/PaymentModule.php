<?php

use webforma\components\WebModule;

/**
 * Class PaymentModule
 */
class PaymentModule extends WebModule
{
	/**
	 *
	 */
	const PAYMENT_OPTION_CART = 1;

	/**
	 *
	 */
	const PAYMENT_OPTION_MODERATION = 2;

    /**
     *
     */
    public $adminMenuOrder = 5;

    /**
     * @var string
     */
    public $pathAssets = 'payment.views.assets';

	/**
	 * @var
	 */
    public $paymentOption = self::PAYMENT_OPTION_CART;

    /**
     * @return array
     */
    public function getDependencies()
    {
        return ['store', 'order'];
    }

    /**
     * @return string
     */
    public function getCategory()
    {
	    return Yii::t('PaymentModule.payment', 'Store');
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('PaymentModule.payment', 'Payment methods list'),
                'url' => ['/payment/paymentBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('PaymentModule.payment', 'Create payment'),
                'url' => ['/payment/paymentBackend/create'],
            ],
        ];
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/payment/paymentBackend/index';
    }

	/**
	 * @return array
	 */
	public function getParamsLabels()
	{
		return [
			'paymentOption' => 'Способ вывода оплаты',
		];
	}


	/**
     * @return array
     */
    public function getEditableParams()
    {
        return [
        	'paymentOption' => [
		        self::PAYMENT_OPTION_CART => 'Списком в корзине',
		        self::PAYMENT_OPTION_MODERATION => 'Онлайн оплата после подтверждения заказа',
	        ],
	        'paymentOptionHint' => [
	        	'input-type' => 'html',
		        'html' => '<div class="help-block">
		            <b>Списком в корзине</b> - список выбора вариантов оплаты выводится в корзине, в случае выбора онлайн оплаты* пользователь послучае возможность оплатить заказ сразу после оформления.
		            <br/><br/>
		            <b>Онлайн оплата после подтверждения заказа</b> - возможность оплатить заказ онлайн* появится после того, как заказу будет присвоен статус "Ожидает оплаты".
		            <br/><br/>
		            <b>*</b> - при наличии способа оплаты через интернет-эквайринг 
		        </div>',
	        ],
        ];
    }

	public function getEditableParamsGroups()
	{
		return [
			[
				'label' => 'Общие настройки',
				'items' => [
					'paymentOption',
					'paymentOptionHint',
				],
			],
		];
	}


	/**
     * @return string
     */
    public function getName()
    {
        return Yii::t('PaymentModule.payment', 'Payment');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('PaymentModule.payment', 'Payment orders module');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-usd';
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'payment.models.*',
                'payment.components.payments.*',
                'payment.listeners.*',
            ]
        );
    }
}
