<?php

class PayOrderListener
{
    public static function onSuccessPay(PayOrderEvent $event)
    {
        $order = $event->getOrder();

        $order->refresh();

	    \Yii::app()->getComponent('orderNotifyService')->sendOrderPayOrderAdmin($order);

	    \Yii::app()->getComponent('orderNotifyService')->sendOrderPayOrderUser($order);
    }
} 
