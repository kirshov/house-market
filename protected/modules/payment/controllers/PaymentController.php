<?php
use webforma\components\controllers\FrontController;
use webforma\widgets\WFlashMessages;

/**
 * Class PaymentController
 */
class PaymentController extends FrontController
{
    /**
     * @param null $id
     * @throws CException
     * @throws CHttpException
     */
    public function actionProcess($id = null)
    {
        /* @var $payment Payment */
        $payment = Payment::model()->findByPk($id);

        if (null === $payment && !$payment->module) {
            throw new CHttpException(404);
        }

        /** @var PaymentSystem $paymentSystem */
        if ($paymentSystem = Yii::app()->paymentManager->getPaymentSystemObject($payment->module)) {
            $result = $paymentSystem->processCheckout($payment, Yii::app()->getRequest());
            if ($result instanceof Order) {
                Yii::app()->getUser()->setFlash(WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('PaymentModule.payment', 'Success get pay info!'));
                $this->redirect(['/order/order/view', 'url' => $result->url]);
            }
        }
    }

    public function actionTo()
    {
        $model = Order::model()->findByNum(Yii::app()->getRequest()->getParam('OrderId'));
        if (null === $model) {
            throw new CHttpException(404, 'Страница не найдена');
        }

        $this->redirect(Yii::app()->createUrl('order/order/view/', ['url' => $model->url]));
    }
}
