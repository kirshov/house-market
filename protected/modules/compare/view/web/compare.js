/*добавление/удаление товаров в избранное*/
$(document).ready(function () {
    $(document).on('click', '.store-compare-add', function (event) {
        event.preventDefault();
        var $this = $(this);
        var product = parseInt($this.data('id'));
        var data = {'id': product};
        data[webformaTokenName] = webformaToken;
        $.post(storeAddCompareUrl, data, function (data) {
            if (data.result) {
				doTarget('compare');
				$.each($('.store-compare-total'), function () {
                    $(this).html(data.count);
                    $(this).closest('.compare-widget').removeClass('empty');
                });

                $this
                    .removeClass('store-compare-add')
                    .addClass('store-compare-remove');
                if($this.html()){
					$this.html('В сравнении');
                }
            }
        }, 'json');
    });

    $(document).on('click', '.store-compare-remove', function (event) {
        event.preventDefault();
        var $this = $(this);
        var product = parseInt($this.data('id'));
        var data = {'id': product};
        var del = false;
        data[webformaTokenName] = webformaToken;

        if($('.compare-col.product-item').length > 1){
            $('.compare-col.product_id_' + product + ', .compare__ch-item.product_id_' + product ).remove();
            del = true;
        }

        $.each($('.compare__characteristic-item-wrap').not('.same'), function () {
            var wrap = $(this);
            var same = true;
            var oldValue = false;
            $.each(wrap.find('.compare__ch-item'), function () {
                if(same && oldValue && oldValue != $(this).text()){
                    same = false;
                }
                oldValue = $(this).text();
            });
            if(same){
                wrap.addClass('same');
            }
        });
        $('#compare-toggle').trigger('change');
        deleteCompare();

        $.post(storeRemoveCompareUrl, data, function (data) {
            if($('.compare-col.product-item').length == 1 && !del){
                window.location.href = $('#compareLink').data('url');
            }
            if (data.result) {
                $.each($('.store-compare-total'), function () {
                    if(data.count == 0) {
                        $(this).html('');
                        $(this).closest('.compare-widget').addClass('empty');
                    } else {
                        $(this).html(data.count);
                    }

                });

                if($('body').hasClass('compare-page')){
                    return true;
                }

                $this
                    .removeClass('store-compare-remove')
                    .addClass('store-compare-add');

				if($this.html()){
					$this.html('Сравнить');
				}
            }
        }, 'json');
    });
});