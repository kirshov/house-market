<?php

/**
 * Class CompareModule
 */
class CompareModule extends \webforma\components\WebModule
{
    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(array(
            'compare.components.*',
            'compare.models.*',
        ));
    }

    /**
     * @return bool
     */
    public function getIsShowInAdminMenu()
    {
        return false;
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return ['store'];
    }

    /**
     * @return bool
     */
    public function getAdminPageLink()
    {
        return false;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('CompareModule.compare', 'Compare');
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('CompareModule.compare', 'Store');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('CompareModule.compare', 'Compare products module');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-columns';
    }
}
