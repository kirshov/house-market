<?php

/**
 * Class CompareWidget
 */
class CompareWidget extends \webforma\widgets\WWidget
{
    /**
     * @var
     */
    public $compare;

    /**
     * @var string
     */
    public $view = 'default';

    /**
     *
     */
    public function init()
    {
		$this->compare = Yii::app()->getComponent('compare');
		/*if(Yii::app()->getUser()->getIsGuest()){
			$this->compare = Yii::app()->getComponent('compare');
		} else {
			$this->compare = Yii::app()->getComponent('compareDb');
		}*/

        parent::init();
    }

    /**
     * @throws CException
     */
    public function run()
    {
        $this->render($this->view, ['compare' => $this->compare]);
    }
}