<?php

/**
 * Class CompareControl
 */
class CompareControl extends \webforma\widgets\WWidget
{
    /**
     * @var
     */
    public $product;

    /**
     * @var
     */
    public $compare;

    /**
     * @var string
     */
    public $view = 'in-list';

    /**
     *
     */
    public function init()
    {
        Yii::app()->getClientScript()->registerScriptFile(
            Yii::app()->getAssetManager()->publish(
                Yii::getPathOfAlias('application.modules.compare.view.web') . '/compare.js'
            ),
            CClientScript::POS_END
        );

		$this->compare = Yii::app()->getComponent('compare');
	    /*if(Yii::app()->getUser()->getIsGuest()){
		    $this->compare = Yii::app()->getComponent('compare');
	    } else {
		    $this->compare = Yii::app()->getComponent('compareDb');
	    }*/

        parent::init();
    }

    /**
     * @throws CException
     */
    public function run()
    {
    	if($this->compare == null){
    		return false;
	    }
        $this->render($this->view, ['product' => $this->product, 'compare' => $this->compare]);
    }
}