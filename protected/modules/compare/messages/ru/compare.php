<?php
return [
    'Compare' => 'Сравнение товаров',
    'Store' => 'Магазин',
    'Compare products module' => 'Модуль для сравнения товаров',
    'Success added!' => 'Успешно добавлено!',
    'Error =(' => 'Произошла ошибка =(',
    'Success removed!' => 'Успешно удалено!',
    'Remove error =(' => 'При удалении произошла ошибка =('
];