<?php

use webforma\components\controllers\FrontController;

/**
 * Class CompareController
 */
class CompareController extends FrontController
{
    /**
     * @var CompareService
     *
     */

    protected $compare;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

	/**
	 * @var StoreCategoryRepository
	 */
	protected $categoryRepository;


    /**
     *
     */
    public function init()
    {
		$this->compare = Yii::app()->getComponent('compare');
    	/*if(Yii::app()->getUser()->getIsGuest()){
		    $this->compare = Yii::app()->getComponent('compare');
	    } else {
		    $this->compare = Yii::app()->getComponent('compareDb');
	    }*/

        $this->productRepository = Yii::app()->getComponent('productRepository');
        $this->categoryRepository = Yii::app()->getComponent('categoryRepository');

        parent::init();
    }

    /**
     * @throws CHttpException
     */
    public function actionAdd()
    {
        if (!Yii::app()->getRequest()->getIsPostRequest() || !Yii::app()->getRequest()->getIsAjaxRequest()) {
            throw new CHttpException(404);
        }

        $productId = (int)Yii::app()->getRequest()->getPost('id');

        if (!$productId) {
            throw new CHttpException(404);
        }

        if ($this->compare->add($productId)) {
            Yii::app()->ajax->raw(
                [
                    'result' => true,
                    'data' => Yii::t('CompareModule.compare', 'Success added!'),
                    'count' => $this->compare->count(),
                ]
            );
        }

        Yii::app()->ajax->raw(
            [
                'message' => Yii::t('CompareModule.compare', 'Error =('),
                'result' => false,
                'count' => $this->compare->count(),
            ]
        );
    }

    /**
     * @throws CHttpException
     */
    public function actionRemove()
    {
        if (!Yii::app()->getRequest()->getIsPostRequest() || !Yii::app()->getRequest()->getIsAjaxRequest()) {
            throw new CHttpException(404);
        }

        $productId = (int)Yii::app()->getRequest()->getPost('id');

        if (!$productId) {
            throw new CHttpException(404);
        }

        if ($this->compare->remove($productId)) {
            Yii::app()->ajax->raw(
                [
                    'result' => true,
                    'data' => Yii::t('CompareModule.compare', 'Success removed!'),
                    'count' => $this->compare->count(),
                ]
            );
        }

        Yii::app()->ajax->raw(
            [
                'message' => Yii::t('CompareModule.compare', 'Error =('),
                'result' => false,
                'count' => $this->compare->count(),
            ]
        );
    }

    /**
     *
     */
    public function actionIndex()
    {
		$itemsData = Yii::app()->getRequest()->getParam('items');
		$items = $products = [];
		if($itemsData) {
			$temp = explode(',', $itemsData);
			if ($temp) {
				foreach ($temp as $value) {
					$value = trim($value);
					if (is_numeric($value)) {
						$items[] = $value;
					}
				}
			}

			$products = $this->productRepository->getByIds($items, false);
		}
		if(!$items && $this->compare->getData()){
			return $this->redirect(Yii::app()->createUrl('/compare/compare/index').'?items='.implode(',',array_keys($this->compare->getData())));
		}

		if($itemsData && !$products){
			throw new CHttpException(404);
		}

		if(!$products){
			$products = $this->productRepository->getByIds($items, false);
		}

		Yii::app()->getModule('dictionary');

		$catsProducts = $catsIds = $productsCats = $characteristic = [];
		$productsData = $products->getData();
		foreach ($productsData as $product) {
			//$catsIds[$product->category_id] = $product->category_id;
			//$catsProducts[$product->category_id][$product->id] = $product;

			//$characteristic[$product->category_id] = Helper::mergeArray($characteristic[$product->category_id], $this->getProductData($product), $this->getAttributesData($product), $this->getVariantsData($product));


			$characteristic['Площадь'][$product->id] = Helper::getFormatPrice($product->square).' м<sup>2</sup>';
			$characteristic['Этажность'][$product->id] = DictionaryData::getFloor($product->floor_id);
			$characteristic['Готовность'][$product->id] = DictionaryData::getReadiness($product);
			$characteristic['Цена'][$product->id] = $product->getFormatPrice();
			$characteristic['Участок'][$product->id] = Helper::getFormatPrice($product->square_area).' соток';

			$characteristic = Helper::mergeArray($characteristic, $this->getProductData($product), $this->getAttributesData($product), $this->getVariantsData($product));
    	}

		$categories = [];
		if($catsIds){
			$categories = $this->categoryRepository->getCatsByIds($catsIds);
		}

		$currentCategory = false;
		if($categories){
			$currentCategory = Yii::app()->getRequest()->getParam('category');
			if(!$currentCategory || !in_array($currentCategory, $catsIds)){
				$firstCategory = reset($categories);
				$currentCategory = $firstCategory->id;
			}
		}
        $this->render(Yii::app()->getRequest()->getParam('print') ? 'print' : 'index', [
        	'currentCategory' => $currentCategory,
        	//'categories' => $categories,
        	'catsProducts' => $catsProducts,
			'characteristic' => $characteristic,
			'products' => $productsData,
			'link' => Yii::app()->createAbsoluteUrl('/compare/compare/index').($items ? '?items='.implode(',', $items) : ''),
			'printLink' => Yii::app()->createAbsoluteUrl('/compare/compare/index').($items ? '?items='.implode(',', $items).'&print=1' : '?print=1'),
			'clearLink' => Yii::app()->createAbsoluteUrl('/compare/compare/index'),
		]);
    }

	/**
	 * @param Product $product
	 * @return array
	 */
	protected function getProductData($product){
    	$data = [];
		if($product->sku){
			$data[Yii::t('StoreModule.store', 'SKU')][$product->id] = $product->sku;
		}

		return $data;
	}

	/**
	 * @param Product $product
	 * @return array
	 */
    protected function getAttributesData($product)
	{
		$data = [];
		foreach ($product->getAttributeGroupsWithId() as $groupId => $items) {
			if($groupId == 3){
				continue;
			}
			foreach ($items as $attribute) {
				if ($attribute->hide_in_site || $product->hasVariantByAttribute($attribute->id)) {
					continue;
				}

				if($groupId == 2) {
					$value = AttributeRender::renderValue($attribute, $product->attribute($attribute), 2, '{item}');
				} else {
					$value = AttributeRender::renderValue($attribute, $product->attribute($attribute), true, '{item}');
				}

				if($attribute->title == 'Вид внутренней отделки'){
					$attribute->title = 'Внутренняя отделка';
				}
				if($groupId == 2){
					if(is_array($value) && !empty($value)){
						foreach ($value as $item){
							$data[$item][$product->id] = 'Да';
						}
					}
				} else {
					$data[$attribute->title][$product->id] = AttributeRender::formatSting($value);
				}

			}
		}
		return $data;
	}

	/**
	 * @param Product $product
	 * @return array
	 */
	protected function getVariantsData($product){
		$data = [];

		$variantsGroups = $product->getVariantsGroup();
		if ($variantsGroups) {
			foreach ($variantsGroups as $type => $variantsDataGroup) {
				foreach ($variantsDataGroup as $title => $variantsGroup) {
					$itemsValues = [];
					foreach ($variantsGroup as $itemTitle => $item) {
						$itemsValues[] = strip_tags($itemTitle);
					}

					$data[$title][$product->id] =  implode(', ', $itemsValues);
				}
			}
		}

		return $data;
	}
}