<?php

/**
 * Class CompareService
 */
class CompareDbService extends CApplicationComponent
{
	/**
	 * @var string
	 */
	public $key = 'webforma::store::compares::db';

    /**
     * @var ProductRepository
     */
    protected $productRepository;

	/**
	 * @var array
	 */
    protected $data = [];

    /**
     *
     */
    public function init()
    {
    	Yii::import('application.modules.compare.models.Compare');
        $this->productRepository = Yii::app()->getComponent('productRepository');

	    $this->data = Yii::app()->getCache()->get($this->key);

	    if($this->data == false){
	    	foreach (Compare::model()->findAll('user_id = :userId', ['userId' => Yii::app()->getUser()->getId()]) as $item){
	    		$this->data[$item->product_id] = 1;
		    }

		    Yii::app()->getCache()->set($this->key, $this->data);
	    }
    }


    /**
     * @param $productId
     * @return bool
     */
    public function add($productId)
    {
    	if(isset($this->data[$productId])){
    		return true;
	    }

    	$model = new Compare();
    	$model->product_id = $productId;
    	$model->user_id = Yii::app()->getUser()->getId();
        $model->save();

        $this->data[$productId] = 1;

        Yii::app()->cache->delete($this->key);

        return true;
    }


    /**
     * @param $productId
     * @return bool
     */
    public function remove($productId)
    {
    	Compare::model()->deleteAll([
    		'condition' => 'user_id = :userId AND product_id = :productId',
		    'params' => [
		    	':userId' => Yii::app()->getUser()->getId(),
			    ':productId' => $productId,
		    ],
		    'limit' => 1,
	    ]);

        if (isset($this->data[$productId])) {
            unset($this->data[$productId]);
        }

	    Yii::app()->cache->delete($this->key);

        return true;
    }

    /**
     *
     */
    public function count()
    {
    	if(is_array($this->data)){
			return sizeof($this->data);
		}
    	return 0;
    }

    /**
     * @param $productId
     * @return bool
     */
    public function has($productId)
    {
        return isset($this->data[$productId]);
    }

    /**
     * @return mixed
     */
    public function products()
    {
		return $this->productRepository->getByIds(array_keys($this->data), false);
    }
}