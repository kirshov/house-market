<?php

/**
 * Class CompareEvents
 */
class CompareEvents
{
    /**
     *
     */
    const ADD_TO_COMPARE = 'compare.add.success';

    /**
     *
     */
    const REMOVE_FROM_COMPARE = 'compare.remove.success';
}