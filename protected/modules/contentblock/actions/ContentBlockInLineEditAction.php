<?php
/**
 * Class ContentBlockInLineEditAction
 */
class ContentBlockInLineEditAction extends \webforma\components\actions\WInLineEditAction
{
    /**
     * @throws \CHttpException
     */
    public function init()
    {
    	parent::init();

        if (!DEVMODE && $this->name == 'code') {
        	$model = $this->getModel($this->pk);
        	if($model->is_system){
				Yii::app()->ajax->rawText('Редактирование этого блока запрещено', 403);
			}
        }
    }
}
