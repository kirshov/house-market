<?php

/**
 * ContentBlockModule основной класс модуля contentblock
 */
class ContentBlockModule extends webforma\components\WebModule
{
	/**
	 *
	 */
	public $adminMenuOrder = 10;

	/**
	 * @var int
	 */
	public $useCategory = 0;

	/**
	 * @var
	 */
	public $mainCategory;

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('ContentBlockModule.contentblock', 'Content');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('ContentBlockModule.contentblock', 'Blocks');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('ContentBlockModule.contentblock', 'Module for create simple content blocks');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return "fa fa-fw fa-th-large";
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'contentblock.actions.*',
                'contentblock.models.*',
            ]
        );
    }


	/**
	 * @return array
	 */
	public function getParamsLabels()
	{
		return DEVMODE ? [
			'useCategory' => 'Использовать категории',
			'mainCategory' => 'Главная категория',
		] : [];
	}

	/**
	 * @return array
	 */
	public function getEditableParams()
	{
		$params = [];

		if(DEVMODE){
			$params = CMap::mergeArray($params, [
				'useCategory' => [
					'input-type' => 'checkbox',
				],
				'mainCategory' => CHtml::listData($this->getCategoryList(), 'id', 'name'),
			]);
		}
		return !empty($params) ? $params : false;
	}

	/**
	 * @return array
	 */
	public function getEditableParamsGroups()
	{
		$groups = [];

		if(DEVMODE){
			$groups = CMap::mergeArray($groups, [
				'system' => [
					'label' => 'Системные настройки',
					'items' => [
						'useCategory',
						'mainCategory',
					]
				]
			]);
		}

		return $groups;
	}

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/contentblock/contentBlockBackend/index';
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ContentBlockModule.contentblock', 'Blocks list'),
                'url' => ['/contentblock/contentBlockBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ContentBlockModule.contentblock', 'Add block'),
                'url' => ['/contentblock/contentBlockBackend/create'],
            ],
        ];
    }

    /**
     * @return bool
     */
    public function getIsInstallDefault()
    {
        return true;
    }
}
