<?php
/**
 * @var $this ContentBlockBackendController
 * @var $model ContentBlock
 */
$this->breadcrumbs = [
    Yii::t('ContentBlockModule.contentblock', 'Content blocks'),
];

$this->pageTitle = Yii::t('ContentBlockModule.contentblock', 'Content blocks - admin');

$this->menu = [
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('ContentBlockModule.contentblock', 'Content blocks'),
        'url' => ['/contentblock/contentBlockBackend/index']
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('ContentBlockModule.contentblock', 'Add content block'),
        'url' => ['/contentblock/contentBlockBackend/create']
    ],
];

$this->title = Yii::t('ContentBlockModule.contentblock', 'Blocks');
$this->subTitle = Yii::t('ContentBlockModule.contentblock', 'administration');

$useCategory = $this->getModule()->useCategory;
$mainCategory = $this->getModule()->mainCategory;
?>

<?php $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'content-block-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => [
            [
                'class' => 'webforma\widgets\WEditableColumn',
                'name' => 'name',
                'editable' => [
                    'url' => $this->createUrl('/contentblock/contentBlockBackend/inline'),
                    'mode' => 'popup',
                    'params' => [
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
                    ]
                ],
                'filter' => CHtml::activeTextField($model, 'name', ['class' => 'form-control']),
            ],
            [
                'class' => 'webforma\widgets\WEditableColumn',
                'editable' => [
                    'url' => $this->createUrl('/contentblock/contentBlockBackend/inline'),
                    'type' => 'select',
                    'source' => $model->getTypes(),
                    'params' => [
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
                    ]
                ],
                'name' => 'type',
                'type' => 'raw',
                'value' => '$data->getType()',
                'filter' => CHtml::activeDropDownList(
                    $model,
                    'type',
                    $model->getTypes(),
                    ['class' => 'form-control', 'empty' => '']
                ),
            ],
            [
                'class' => 'webforma\widgets\WEditableColumn',
                'name' => 'code',
                'editable' => [
                    'url' => $this->createUrl('/contentblock/contentBlockBackend/inline'),
                    'mode' => 'popup',
                    'params' => [
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
                    ],
                ],
				'cssClassExpression' => '$data->is_system ? "system_block" : ""',
                'filter' => CHtml::activeTextField($model, 'code', ['class' => 'form-control']),
            ],
			[
				'name'   => 'category_id',
				'value'  => '$data->getCategoryName()',
				'filter' => CHtml::activeDropDownList($model, 'category_id', Yii::app()->getComponent('categoriesRepository')->getFormattedList($mainCategory), [
                    'encode' => false,
                    'empty' => '',
                    'class' => 'form-control'
                ]),
				'visible' => $useCategory,
			],
            [
                'class' => 'webforma\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('/contentblock/contentBlockBackend/inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    ContentBlock::STATUS_ACTIVE => ['class' => 'label-success'],
                    ContentBlock::STATUS_NOT_ACTIVE => ['class' => 'label-info'],
                ],
            ],
	        [
		        'class' => 'webforma\widgets\EditableStatusColumn',
		        'name' => 'is_system',
		        'url' => $this->createUrl('/contentblock/contentBlockBackend/inline'),
		        'source' => $model->getSystemFlagList(),
		        'options' => [
			        0 => ['class' => 'label-success'],
			        1 => ['class' => 'label-danger'],
		        ],
		        'visible' => DEVMODE,
	        ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
	            'buttons' => [
	            	'delete' => [
			            'visible' => function ($row, $data) {
    	                    return !$data->is_system || DEVMODE;
						},
		            ]
	            ]
            ],
        ],
    ]
); ?>
<style type="text/css">
	.system_block a, .system_block a:hover{border: none;color: #555;line-height: 28px;}
</style>