<?php
$this->breadcrumbs = [
    Yii::t('ContentBlockModule.contentblock', 'Content blocks') => ['/contentblock/contentBlockBackend/index'],
    Yii::t('ContentBlockModule.contentblock', 'Create'),
];

$this->pageTitle = Yii::t('ContentBlockModule.contentblock', 'Content blocks - add');

$this->menu = [
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('ContentBlockModule.contentblock', 'Content blocks'),
        'url'   => ['/contentblock/contentBlockBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('ContentBlockModule.contentblock', 'Add content block'),
        'url'   => ['/contentblock/contentBlockBackend/create']
    ],
];

$this->title = Yii::t('ContentBlockModule.contentblock', 'Content block');
$this->subTitle = Yii::t('ContentBlockModule.contentblock', 'add');
?>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>
