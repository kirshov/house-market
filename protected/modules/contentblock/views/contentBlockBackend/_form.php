<?php
/**
 * @var $model ContentBlock
 * @var $this ContentBlockBackendController
 * @var $form \webforma\widgets\ActiveForm
 */
?>
<?php
$form = $this->beginWidget(
    'webforma\widgets\ActiveForm',
    [
        'id'                     => 'content-block-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => false,
		'type' => 'vertical',
		'htmlOptions' => ['class' => 'well sticky'],
		'clientOptions' => [
			'validateOnSubmit' => true,
		],
    ]
); ?>
<div class="alert alert-info">
    <?=  Yii::t('ContentBlockModule.contentblock', 'Fields with'); ?>
    <span class="required">*</span>
    <?=  Yii::t('ContentBlockModule.contentblock', 'are required.'); ?>
</div>

<?=  $form->errorSummary($model); ?>

<div class="row">
	<div class="col-sm-6">
		<?=  $form->textFieldGroup($model, 'name'); ?>
	</div>
</div>
<?if(!$model->is_system || $model->isNewRecord):?>
	<div class="row">
		<div class="col-sm-6">
			<?=  $form->slugFieldGroup($model, 'code', ['sourceAttribute' => 'name']); ?>
		</div>
	</div>
<?endif;?>

<div class="row">
	<div class="col-sm-3">
		<?=  $form->dropDownListGroup($model, 'type', [
			'widgetOptions' => [
				'data' => $model->getTypes()
			],
		]); ?>
	</div>

	<div class="col-sm-3">
        <?=  $form->dropDownListGroup($model, 'status', [
			'widgetOptions' => [
				'data' => $model->getStatusList()
			],
		]); ?>
    </div>
</div>

<?if($this->getModule()->useCategory):?>
	<div class="row">
		<div class="col-sm-3">
			<?=  $form->dropDownListGroup($model, 'category_id', [
				'widgetOptions' => [
					'data'        => Yii::app()->getComponent('categoriesRepository')->getFormattedList($this->getModule()->mainCategory),
					'htmlOptions' => ['empty' => Yii::t('ContentBlockModule.contentblock', '--choose--'), 'encode' => false],
				],
			]); ?>
		</div>
	</div>
<?endif;?>

<?if(DEVMODE):?>
	<div class="row">
		<div class="col-sm-6">
			<?=  $form->checkboxGroup($model, 'is_system'); ?>
		</div>
	</div>
<?endif;?>

<div class="row">
    <div class="col-sm-12">
        <?php if (!$model->isNewRecord && $model->type == ContentBlock::HTML_TEXT): ?>
            <?=  $form->labelEx($model, 'content'); ?>
            <?php $this->widget($this->webforma->getVisualEditor(), [
				'model'     => $model,
				'attribute' => 'content',
				'options' => [
					'rows' => 25,
				],
			]); ?>
        <?php else: ?>
            <?=  $form->textAreaGroup($model, 'content'); ?>
        <?php endif; ?>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-sm-12">
		<?=  $form->textAreaGroup($model, 'description', [
			'widgetOptions' => [
				'htmlOptions' => [
					'style' => 'height:100px;',
				],
			],
		]); ?>
    </div>
</div>

<div class="buttons">
	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context'    => 'primary',
		'label'      => $model->isNewRecord
			? Yii::t('ContentBlockModule.contentblock', 'Add block and continue')
			: Yii::t('ContentBlockModule.contentblock', 'Save block and continue'),
	]); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType'  => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label'       => $model->isNewRecord ? Yii::t('ContentBlockModule.contentblock', 'Add block and close')
			: Yii::t('ContentBlockModule.contentblock', 'Save block and close'),
	]); ?>
</div>
<?php $this->endWidget(); ?>
