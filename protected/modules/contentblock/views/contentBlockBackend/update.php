<?php
$this->breadcrumbs = [
    Yii::t('ContentBlockModule.contentblock', 'Content blocks') => ['/contentblock/contentBlockBackend/index'],
    $model->name => [
        '/contentblock/contentBlockBackend/view',
        'id' => $model->id
    ],
    Yii::t('ContentBlockModule.contentblock', 'Editing'),
];

$this->pageTitle = Yii::t('ContentBlockModule.contentblock', 'Content blocks - edit');

$this->menu = [
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('ContentBlockModule.contentblock', 'Content blocks'),
        'url'   => ['/contentblock/contentBlockBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('ContentBlockModule.contentblock', 'Add content block'),
        'url'   => ['/contentblock/contentBlockBackend/create']
    ],
    [
        'label' => Yii::t('ContentBlockModule.contentblock', 'Content block') . ' «' . mb_substr(
                $model->name,
                0,
                32
            ) . '»'
    ],
    [
        'icon'  => 'fa fa-fw fa-pencil',
        'label' => Yii::t('ContentBlockModule.contentblock', 'Edit content block'),
        'url'   => [
            '/contentblock/contentBlockBackend/update',
            'id' => $model->id
        ]
    ],
    [
        'icon'  => 'fa fa-fw fa-eye',
        'label' => Yii::t('ContentBlockModule.contentblock', 'View content block'),
        'url'   => [
            '/contentblock/contentBlockBackend/view',
            'id' => $model->id
        ]
    ],
    [
        'icon'        => 'fa fa-fw fa-trash-o',
        'label'       => Yii::t('ContentBlockModule.contentblock', 'Remove content block'),
        'url'         => '#',
        'linkOptions' => [
            'submit'  => ['/contentblock/contentBlockBackend/delete', 'id' => $model->id],
            'params'  => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
            'confirm' => Yii::t('ContentBlockModule.contentblock', 'Do you really want to remove content block?'),
        ]
    ],
];

$this->title = Yii::t('ContentBlockModule.contentblock', 'Editing blocks');
$this->subTitle = '&laquo;'.$model->name.'&raquo;'
?>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>
