<?php
$this->breadcrumbs = [
    Yii::t('ContentBlockModule.contentblock', 'Content blocks') => ['/contentblock/contentBlockBackend/index'],
    $model->name,
];

$this->pageTitle = Yii::t('ContentBlockModule.contentblock', 'Content blocks - view');

$this->menu = [
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('ContentBlockModule.contentblock', 'Content blocks'),
        'url'   => ['/contentblock/contentBlockBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('ContentBlockModule.contentblock', 'Add new content block'),
        'url'   => ['/contentblock/contentBlockBackend/create']
    ],
    [
        'icon'  => 'fa fa-fw fa-pencil',
        'label' => Yii::t('ContentBlockModule.contentblock', 'Edit content block'),
        'url'   => [
            '/contentblock/contentBlockBackend/update',
            'id' => $model->id
        ]
    ],
    [
        'icon'  => 'fa fa-fw fa-eye',
        'label' => Yii::t('ContentBlockModule.contentblock', 'View content block'),
        'url'   => [
            '/contentblock/contentBlockBackend/view',
            'id' => $model->id
        ]
    ],
    [
        'icon'        => 'fa fa-fw fa-trash-o',
        'label'       => Yii::t('ContentBlockModule.contentblock', 'Remove content block'),
        'url'         => '#',
        'linkOptions' => [
            'submit'  => ['/contentblock/contentBlockBackend/delete', 'id' => $model->id],
            'params'  => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
            'confirm' => Yii::t('ContentBlockModule.contentblock', 'Do you really want to delete content block?'),
        ]
    ],
];

$this->title = Yii::t('ContentBlockModule.contentblock', 'Viewing content block');
$this->subTitle = '&laquo;'.$model->name.'&raquo;';
?>

<?php $this->widget(
    'bootstrap.widgets.TbDetailView',
    [
        'data'       => $model,
        'attributes' => [
            'id',
            'name',
            'code',
            [
                'name'  => 'type',
                'value' => $model->getType(),
            ],
			[
				'type'  => 'raw',
				'name' => 'category_id',
				'value' => $model->category_id ? $model->category->name : '',
				'visible' => $this->getModule()->useCategory,
			],
            'content',
            [
                'name' => 'description',
                'type' => 'raw',
                'value' => $model->description,
            ]
        ],
    ]
); ?>

<br/>
<div>
    <?=  Yii::t('ContentBlockModule.contentblock', 'Shortcode for using this block in template:'); ?>
    <br/><br/>
    <?=  $example; ?>
	<br/>
</div>