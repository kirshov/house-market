<?php
/**
 * Виджет для отрисовки группы блоков контента:
 **/
Yii::import('application.modules.contentblock.models.ContentBlock');
Yii::import('application.modules.contentblock.ContentBlockModule');

/**
 * Class ContentBlockGroupWidget
 */
class ContentBlockGroupWidget extends webforma\widgets\WWidget
{
    /**
     * @var
     */
    public $category;
    /**
     * @var
     */
    public $limit;
    /**
     * @var bool
     */
    public $silent = true;
    /**
     * @var int
     */
    public $cacheTime = 60;
    /**
     * @var bool
     */
    public $rand = false;
    /**
     * @var string
     */
    public $view = 'contentblockgroup';

	/**
	 * @var
	 */
    protected $categoryId;

    /**
     * @throws CException
     */
    public function init()
    {
		if(is_numeric($this->category)){
			$this->categoryId = $this->category;
		} elseif($this->category instanceof Category) {
			$this->categoryId = $this->category->id;
		} elseif(!empty($this->category)) {
			$category = Yii::app()->getComponent('categoriesRepository')->getByAlias($this->category);

			if (null === $category) {
				throw new CException(Yii::t('ContentBlockModule.contentblock', 'Category "{category}" does not exist, please enter the unsettled category',['{category}' => $this->category]));
			}
			$this->categoryId = $category->id;
		}

        if (!$this->categoryId) {
            throw new CException(Yii::t('ContentBlockModule.contentblock', 'Insert group content block title for ContentBlockGroupWidget!'));
        }

        $this->silent = (bool)$this->silent;
        $this->cacheTime = (int)$this->cacheTime;
        $this->rand = (int)$this->rand;
    }

    /**
     * @throws CException
     */
    public function run()
    {
        $cacheName = "ContentBlock{$this->categoryId}";

        $blocks = Yii::app()->getCache()->get($cacheName);

        if ($blocks === false) {
			$criteria = new CDbCriteria([
				'scopes' => ['active'],
			]);
			$criteria->addCondition('category_id = :category_id');

			$criteria->params[':category_id'] = $this->categoryId;

            if ($this->rand) {
                $criteria->order = 'RAND()';
            }

            if ($this->limit) {
                $criteria->limit = (int)$this->limit;
            }

            $blocks = ContentBlock::model()->findAll($criteria);

            if (empty($blocks) && $this->silent === false) {
                throw new CException(Yii::t('ContentBlockModule.contentblock', 'Group content block "{category_id}" was not found !', ['{category_id}' => $this->category]));
            }

            Yii::app()->getCache()->set($cacheName, $blocks, $this->cacheTime);
        }

        $this->render($this->view, ['blocks' => $blocks]);
    }
}
