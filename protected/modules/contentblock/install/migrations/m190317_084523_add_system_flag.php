<?php

class m190317_084523_add_system_flag extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{contentblock_content_block}}', 'is_system', 'tinyint(1) NOT NULL DEFAULT 0');
	}

	public function safeDown()
	{

	}
}