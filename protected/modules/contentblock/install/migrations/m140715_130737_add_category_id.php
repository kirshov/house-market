<?php

class m140715_130737_add_category_id extends webforma\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{contentblock_content_block}}','category_id','integer DEFAULT NULL');

		$this->addForeignKey(
			"fk_{{contentblock_content_block}}_category_id",
			'{{contentblock_content_block}}',
			'category_id',
			'{{category_category}}',
			'id',
			'SET NULL',
			'NO ACTION'
		);
    }

    public function safeDown()
    {
        $this->dropColumn('{{contentblock_content_block}}', 'category_id');
    }
}