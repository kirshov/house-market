<?php

/**
 * Class TargetTemplateListener
 */
class TargetTemplateListener
{
    /**
     *
     */
    public static function js()
    {
        Yii::app()->getController()->renderPartial('application.modules.target.views.js');
    }
}