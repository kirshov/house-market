<?php
/**
 * TargetModule основной класс модуля target
 */

class TargetModule extends webforma\components\WebModule
{

	/**
	 * @var int
	 */
	public $adminMenuOrder = 50;

	/**
	 * @var
	 */
	public $metrika_id;

    /**
     * Массив с именами модулей, от которых зависит работа данного модуля
     *
     * @return array
     */
    public function getDependencies()
    {
        return parent::getDependencies();
    }

    /**
     * Каждый модуль должен принадлежать одной категории, именно по категориям делятся модули в панели управления
     *
     * @return string
     */
    public function getCategory()
    {
        return 'Сервисы';
    }

    /**
     * массив лейблов для параметров (свойств) модуля. Используется на странице настроек модуля в панели управления.
     *
     * @return array
     */
    public function getParamsLabels()
    {
        return [
        	'metrika_id' => 'Id метрики',
			'metrika_info' => 'Идентификаторы целей',
		];
    }

    /**
     * массив параметров модуля, которые можно редактировать через панель управления (GUI)
     *
     * @return array
     */
    public function getEditableParams()
    {
		return [
			'metrika_id',
			'metrika_info' => [
				'input-type' => 'html',
				'html' => '
					<small><b>orderСart</b> - заказ через корзину</small><br/>
					<small><b>orderOneClick</b> - заказ через один клик</small><br/>
					<small><b>toCart</b> - добавили в корзину</small><br/>
					<small><b>favorite</b> - добавили в избранное</small><br/>
					<small><b>subscribe</b> - подписка на рассылку</small><br/>
					<small><b>call</b> - клик по номеру телефона</small><br/>
					<small><b>callback</b> - заказ обратного звонка</small><br/>
					<small><b>feedback</b> - оставили сообщение</small><br/>
					<small><b>registration</b> - регистрация пользователя</small><br/>
				',
			],
		];
    }

    /**
     * массив групп параметров модуля, для группировки параметров на странице настроек
     *
     * @return array
     */
    public function getEditableParamsGroups()
    {
        return [
        	'0.target' => [
        		'label' => 'Яндекс.Метрика',
        		'items' => [
					'metrika_id',
					'metrika_info',
				],
			]
		];
    }

    /**
     * если модуль должен добавить несколько ссылок в панель управления - укажите массив
     *
     * @return array
     */
    public function getNavigation()
    {
        return [];
    }

    /**
     * Возвращает название модуля
     *
     * @return string.
     */
    public function getName()
    {
        return 'Цели';
    }

    /**
     * Возвращает описание модуля
     *
     * @return string.
     */
    public function getDescription()
    {
        return '';
    }

    /**
     * Ссылка, которая будет отображена в панели управления
     * Как правило, ведет на страничку для администрирования модуля
     *
     * @return string
     */
    public function getAdminPageLink()
    {
        return $this->getSettingsUrl();
    }

    /**
     * Название иконки для меню админки, например 'user'
     *
     * @return string
     */
    public function getIcon()
    {
        return "fa fa-fw fa-arrow-circle-o-right";
    }

    /**
      * Возвращаем статус, устанавливать ли галку для установки модуля в инсталяторе по умолчанию:
      *
      * @return bool
      **/
    public function getIsInstallDefault()
    {
        return parent::getIsInstallDefault();
    }
}
