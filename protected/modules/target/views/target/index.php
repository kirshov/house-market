<?php
/**
* Отображение для target/index
**/
$this->pageTitle = Yii::t('TargetModule.target', 'target');
$this->description = Yii::t('TargetModule.target', 'target');
$this->keywords = Yii::t('TargetModule.target', 'target');

$this->breadcrumbs = [Yii::t('TargetModule.target', 'target')];
?>

<h1>
    <small>
        <?php echo Yii::t('TargetModule.target', 'target'); ?>
    </small>
</h1>