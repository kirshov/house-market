<?
/**
 * @var TargetModule $module
 */
$module = Yii::app()->getModule('target');

?>
<script type="text/javascript">
	var infinityLoopGoal = 0;
	function doTargetRun(target) {
		yaGoal(target);
	}
	function yaGoal(target) {
		<?if($module->metrika_id):?>
			try {
				ym(<?=$module->metrika_id;?>, 'reachGoal', target);
				window.infinityLoopGoal = 0;
			} catch(e) {
				window.infinityLoopGoal++;
				if(window.infinityLoopGoal < 5){
					setTimeout(function(){yaGoal(target)}, 200);
				}
			}
		<?else:?>
			return;
		<?endif;?>
	}
</script>