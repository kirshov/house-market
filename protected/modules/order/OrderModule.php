<?php

use webforma\components\WebModule;

/**
 * Class OrderModule
 */
class OrderModule extends WebModule
{
    /**
     *
     */
    public $adminMenuOrder = 2;

    /**
     * @var
     */
    public $notifyEmailFrom;

    /**
     * @var
     */
    public $notifyEmailsTo;

    /**
     * @var string
     */
    public $assetsPath = 'order.views.assets';

    /**
     * @var int
     */
    public $showOrder = 1;

    /**
     * @var int
     */
    public $enableCheck = 1;

    /**
     * @var int
     */
    public $defaultStatus = 1;

	/**
	 * @var string
	 */
	public $afterMessage = 'Мы свяжемся с Вами в ближайшее время!';

	/**
	 * @var string
	 */
	public $cartBeforeMessage;

    /**
     * @return array
     */
    public function getDependencies()
    {
        return ['store', 'delivery'];
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
		$module = Yii::app()->getModule('webforma');
        return [
			'notifyEmailFrom' => [
				'input-type' => 'text',
				'htmlOptions' => [
					'placeholder' => $module->notifyEmailFrom
				],
			],
			'notifyEmailsTo' => [
				'input-type' => 'text',
				'htmlOptions' => [
					'placeholder' => $module->notifyEmailTo
				],
			],
            'showOrder' => $this->getChoice(),
            //'enableCheck' => $this->getChoice(),
            'defaultStatus' => CHtml::listData(OrderStatus::model()->findAll(), 'id', 'name'),
	        'cartBeforeMessage' => [
		        'input-type' => 'editor'
	        ],
	        'afterMessage' => [
	        	'input-type' => 'editor'
	        ],
        ];
    }

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [
            'notifyEmailFrom' => Yii::t('OrderModule.order', 'Notification email'),
            'notifyEmailsTo' => Yii::t('OrderModule.order', 'Recipients of notifications (comma separated)'),
            'showOrder' => Yii::t('OrderModule.order', 'Public ordering page'),
            'enableCheck' => Yii::t('OrderModule.order', 'Allow orders validation by number'),
            'defaultStatus' => Yii::t('OrderModule.order', 'Default order status'),
            'afterMessage' => 'Сообщение после оформление заказа',
            'cartBeforeMessage' => 'Сообщение в корзине над списком товаров',
        ];
    }

    /**
     * @return array
     */
    public function getEditableParamsGroups()
    {
        return [
            '0.main' => [
                'label' => Yii::t('OrderModule.order', 'Orders settings'),
                'items' => [
                    'defaultStatus',
                    'showOrder',
                    //'enableCheck',
	                'cartBeforeMessage',
	                'afterMessage',
                ],
            ],
            '1.notify' => [
                'label' => Yii::t('OrderModule.order', 'Notifications'),
                'items' => [
					'notifyEmailFrom',
					'notifyEmailsTo',
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('OrderModule.order', 'Store');
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-shopping-basket',
                'label' => Yii::t('OrderModule.order', 'Orders'),
                'url' => ['/order/orderBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-users',
                'label' => Yii::t('OrderModule.order', 'Clients'),
                'url' => ['/order/clientBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('OrderModule.order', 'Statuses'),
                'url' => ['/order/statusBackend/index'],
            ],
	        [
                'icon' => 'fa fa-fw fa-puzzle-piece',
                'label' => Yii::t('OrderModule.source', 'Sources'),
                'url' => ['/order/sourceBackend/index'],
            ],
        ];
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/order/orderBackend/index';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('OrderModule.order', 'Orders');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('OrderModule.order', 'Orders manage module');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-shopping-basket';
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'order.models.*',
                'order.forms.*',
                'order.listeners.*',
            ]
        );
    }

    /**
     * @return array
     */
    public function getNotifyTo()
    {
    	if($this->notifyEmailsTo){
			$notifyEmailsTo = $this->notifyEmailsTo;
		} else {
			$notifyEmailsTo = Yii::app()->getModule('webforma')->notifyEmailTo;
		}
        return explode(',', $notifyEmailsTo);
    }
}
