<?
$mainAssets = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.webforma.views.assets'));
Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/chartjs/utils.js');
Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/chartjs/Chart.min.js');
?>
<div class="col-md-8">
	<div class="panel box">
		<div class="box-row">
			<div class="box-cell col-md-4 p-a-3 valign-top">
				<h4 class="m-y-1 font-weight-normal">
					<i class="fa fa-shopping-basket text-primary"></i>&nbsp;&nbsp;В текущем месяце
				</h4>
				<ul class="list-group m-x-0 m-t-3 m-b-0">
					<li class="list-group-item p-x-1 b-x-0 b-t-0">
						<span class="badge badge-info pull-right"><?=$all?></span>
						Заказов
					</li>
					<li class="list-group-item p-x-1 b-x-0">
						<span class="badge badge-success pull-right"><?=$success?></span>
						Обработанных
					</li>
					<li class="list-group-item p-x-1 b-x-0 b-b-0">
						<span class="badge badge-warning pull-right"><?=Helper::getFormatPrice($amount)?> <?=Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency)?></span>
						На сумму
					</li>
				</ul>
			</div>
			<div class="box-cell col-md-8 p-a-1 bg-info">
				<div id="main-orders-graph" style="height: 220px;">
					<canvas id="order-canvas"></canvas>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	var lineChartData = {
		labels: [<?=$labels?>],
		datasets: [{
			label: 'Количество заказов',
			borderColor: window.chartColors.white,
			backgroundColor: window.chartColors.white,
			fontColor: window.chartColors.white,
			fill: false,
			data: [<?=$orders?>],
			yAxisID: 'y-axis-1',
			borderWidth: 1,
		}, {
			label: 'Сумма',
			borderColor: window.chartColors.brown,
			backgroundColor: window.chartColors.brown,
			fill: false,
			data: [<?=$amounts?>],
			yAxisID: 'y-axis-2',
			borderWidth: 1,
		}]
	};

	window.onload = function() {
		var ctx = document.getElementById('order-canvas').getContext('2d');
		window.myLine = Chart.Line(ctx, {
			data: lineChartData,
			options: {
				responsive: true,
				maintainAspectRatio: false,
				hoverMode: 'index',
				stacked: false,
				title: {
					display: true,
					text: 'Подтвержденные заказы за последние 10 дней',
					fontColor: 'white',
				},
				scales: {
					yAxes: [{
						type: 'linear',
						display: true,
						position: 'left',
						id: 'y-axis-1',
					}, {
						type: 'linear',
						display: true,
						position: 'right',
						id: 'y-axis-2',

						gridLines: {
							drawOnChartArea: false,
						},
					}],
				}
			}
		});
	};
</script>