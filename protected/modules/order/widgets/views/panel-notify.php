<?php
/**
 * @var $ordersCount int
 * @var $newOrdersCount int
 * @var $workOrdersCount int
 * @var $dataProvider CActiveDataProvider
 */
?>

<div class="col-md-4 col-xs-12 col-sm-6">
	<div class="panel panel-success panel-dark">
		<div class="panel-heading">
			<h3 class="widget-profile-header">
				Заказы
			</h3>
			<i class="panel-icon fa fa-fw fa-shopping-basket"></i>
		</div>
		<div class="widget-profile-counters">
			<a href="<?=Yii::app()->createUrl('/backend/order/order/')?>?Order[status_id]=<?=OrderStatus::STATUS_NEW?>" class="col-xs-4">
				<span class="widget-profile-counter<?=($newOrdersCount ? ' text-danger' : '')?>"><?=$newOrdersCount?></span>
				НОВЫХ
			</a>
			<a href="<?=Yii::app()->createUrl('/backend/order/order/')?>?Order[status_id]=<?=OrderStatus::STATUS_WORK?>" class="col-xs-4">
				<span class="widget-profile-counter<?=($workOrdersCount ? ' text-info' : '')?>"><?=$workOrdersCount?></span>
				В РАБОТЕ
			</a>
			<a href="<?=Yii::app()->createUrl('/backend/order/order/')?>" class="col-xs-4">
				<span class="widget-profile-counter"><?=$ordersCount;?></span>
				ВСЕГО
			</a>
		</div>
		<div class="panel-footer">
			<a href="<?=Yii::app()->createUrl('/backend/order/order/')?>">Перейти к заказам</a>
		</div>
	</div>
</div>