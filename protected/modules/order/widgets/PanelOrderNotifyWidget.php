<?php

/**
 * Class PanelOrderNotifyWidget
 */
class PanelOrderNotifyWidget extends \webforma\widgets\WWidget
{
    /**
     * @throws CException
     */
    public function run()
    {
        $criteria = new CDbCriteria();
        $criteria->with = ['status'];

        $dataProvider = new CActiveDataProvider(
            'Order', [
                'criteria' => $criteria,
                'sort' => [
                    'defaultOrder' => 't.id DESC',
                ],
                'pagination' => [
                    'pageSize' => (int)$this->limit,
                ],
            ]
        );

        $cacheTime = Yii::app()->getController()->webforma->coreCacheTime;
        $model = Order::model();
        $dependency = new CDbCacheDependency('select max(modified) from {{store_order}}');

        $this->render('panel-notify', [
            'ordersCount' => $model->cache($cacheTime, $dependency)->count(),
            'newOrdersCount' => $model->cache($cacheTime, $dependency)->new()->count(),
            'workOrdersCount' => $model->cache($cacheTime, $dependency)->work()->count(),
            'dataProvider' => $dataProvider,
        ]);
    }
}
