<?php

/**
 * Class PanelOrderStatWidget
 */
class PanelOrderStatWidget extends \webforma\widgets\WWidget
{
    /**
     * @throws CException
     */
    public function run()
    {
        $cacheTime = Yii::app()->getController()->webforma->coreCacheTime;
        $dependency = new CDbCacheDependency('select max(modified) from {{store_order}}');

	    $all = $success = $amount = 0;
	    $ordersOnMont = Order::model()->cache($cacheTime, $dependency)->findAll('MONTH(`date`) = MONTH(NOW()) AND YEAR(`date`) = YEAR(NOW())');
	    foreach ($ordersOnMont as $order){
		    $all++;
		    if(in_array($order->status_id, [OrderStatus::STATUS_DELETED, OrderStatus::STATUS_NEW])){
		    	continue;
		    }
		    $success++;
		    $amount += $order->getTotalPrice();
	    }

	    $currency = Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency);

	    $orders = Order::model()->cache($cacheTime, $dependency)->findAll('date >= NOW() - INTERVAL 10 DAY AND status_id != :status_id', [':status_id' => OrderStatus::STATUS_DELETED]);

	    $dates = [];
	    $periods = new DatePeriod(
		    new DateTime(date('Y-m-d', strtotime('-10 days'))),
		    new DateInterval('P1D'),
		    new DateTime()
	    );
	    foreach ($periods as $key => $value) {
	    	$_d = $value->format('d.m');
		    $dates['orders'][$_d] = 0;
		    $dates['amounts'][$_d] = 0;
		    $labels[$_d] = '"'.$_d.', '.Helper::getDayOfWeek($value->format('N')).'"';
	    }

	    foreach ($orders as $order){
	    	$_datetime = strtotime($order->date);
	    	$_d = date('d.m', $_datetime);
	    	$_totalPrice = $order->getTotalPrice();
		    $dates['orders'][$_d] = (int) $dates['orders'][$_d] + 1;
		    $dates['amounts'][$_d] = (int) $dates['amounts'][$_d] + $_totalPrice;
	    }

	    $this->render('panel-stat', [
            'orders' => implode(', ', (array) $dates['orders']),
            'amounts' => implode(', ', (array) $dates['amounts']),
            'labels' => implode(', ', (array) $labels),
            'all' => $all,
            'success' => $success,
            'amount' => $amount,
        ]);
    }
}
