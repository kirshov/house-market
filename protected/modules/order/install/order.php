<?php

return [
    'module' => [
        'class' => 'application.modules.order.OrderModule',
        'panelWidgets' => [
            'application.modules.order.widgets.PanelOrderNotifyWidget' => [
            	'position' => 5,
            ],
	        'application.modules.order.widgets.PanelOrderStatWidget' => [
		        'position' => 35,
	        ],
        ],
	    'mailMessages' => 'orderMailMessage',
    ],
    'import' => [
        'application.modules.order.models.*',
        'application.modules.order.helpers.*',
    ],
    'component' => [
        'eventManager' => [
            'class' => 'webforma\components\EventManager',
            'events' => [
                'order.pay.success' => [
                    ['PayOrderListener', 'onSuccessPay']
                ],
                'http.order.created' => [
                    ['OrderListener', 'onCreate']
                ],
                'http.order.updated' => [
                    ['OrderListener', 'onUpdate'],
                ]
            ]
        ],
        'orderNotifyService' => [
            'class' => 'application.modules.order.components.OrderNotifyService',
        ],
	    'orderMailMessage' => [
		    'class' => 'application.modules.order.components.OrderMailMessage',
	    ],
    ],
    'rules' => [
		'/order/<url>' => 'order/order/view',
		'/orders' => 'order/user/index',
		'/buy-one-click' => 'order/order/buyOneClick',

        //'/order/check'    => '/order/order/check',
        //'/orders/<action:\w+>' => 'order/order/<action>',
        //'/store/account/<action:\w+>' => 'order/user/<action>',
    ],
];
