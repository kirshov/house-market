<?php

class m180414_152413_add_pickup_id extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_order}}', 'pickup_id', 'int(11) not null default 0');
	}

	public function safeDown()
	{

	}
}