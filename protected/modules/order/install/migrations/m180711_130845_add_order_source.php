<?php

class m180711_130845_add_order_source extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_order}}', 'source_id', 'int(11) default null');
	}

	public function safeDown()
	{

	}
}