<?php

class m180803_180857_order_status_edit2 extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->update('{{store_order_status}}', ['position' => '10'], 'id = 5');
		$this->update('{{store_order_status}}', ['position' => '12'], 'id = 6');
		$this->update('{{store_order_status}}', ['position' => '16'], 'id = 7');
		$this->update('{{store_order_status}}', ['position' => '20'], 'id = 3');
		$this->update('{{store_order_status}}', ['position' => '24'], 'id = 4');

		$this->insert('{{store_order_status}}', [
			'id' => 9,
			'name' => 'В работе',
			'is_system' => 1,
			'position' => 9,
			'color' => 'info',
		]);
	}

	public function safeDown()
	{

	}
}