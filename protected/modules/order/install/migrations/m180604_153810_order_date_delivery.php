<?php

class m180604_153810_order_date_delivery extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_order}}', 'date_delivery', 'datetime');
		$this->update('{{store_order_status}}', ['color' => 'info'], 'id IN (5,6,7)');
	}

	public function safeDown()
	{

	}
}