<?php

class m180930_131728_order_phone_index extends webforma\components\DbMigration
{
	public function safeUp()
	{

		$this->createIndex("ix_{{store_order}}_phone", '{{store_order}}', "phone", false);
	}

	public function safeDown()
	{

	}
}