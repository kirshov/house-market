<?php

class m180916_050044_change_fields extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->alterColumn('{{store_order}}', 'pickup_id', 'int(11) default null');
		$this->execute('UPDATE {{store_order}} SET pickup_id = NULL WHERE pickup_id = 0');
	}

	public function safeDown()
	{

	}
}