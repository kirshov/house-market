<?php

class m180919_050044_add_fields extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_order}}', 'company', 'varchar(255) default null');
		$this->addColumn('{{store_order}}', 'inn', 'varchar(64) default null');
	}

	public function safeDown()
	{

	}
}