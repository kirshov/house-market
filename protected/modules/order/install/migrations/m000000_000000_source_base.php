<?php

class m000000_000000_source_base extends webforma\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        $this->createTable(
            '{{store_order_source}}',
            [
                'id'                => 'pk',
                'name'              => 'varchar(250) NOT NULL',
                'image'             => 'varchar(250) DEFAULT NULL',
                'description'       => 'text NOT NULL',
                'status'            => "boolean NOT NULL DEFAULT '1'",
            ],
            $this->getOptions()
        );

        //ix
        $this->createIndex("ix_{{store_order_source}}_status", '{{store_order_source}}', "status", false);
    }

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{source_source}}');
    }
}
