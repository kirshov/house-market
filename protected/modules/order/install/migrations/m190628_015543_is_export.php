<?php

class m190628_015543_is_export extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_order}}', 'is_export', 'tinyint(1) default 0');
	}

	public function safeDown()
	{

	}
}