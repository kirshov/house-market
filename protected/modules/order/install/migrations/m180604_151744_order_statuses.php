<?php

class m180604_151744_order_statuses extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_order_status}}', 'position', 'integer(11) default 0');
		$this->update('{{store_order_status}}', ['position' => 1], 'id = 1');
		$this->update('{{store_order_status}}', ['position' => 2], 'id = 2');
		$this->update('{{store_order_status}}', ['position' => 6], 'id = 3');
		$this->update('{{store_order_status}}', ['position' => 7, 'name' => 'Отменен'], 'id = 4');
		$this->insertMultiple('{{store_order_status}}', [
			[
				'name' => 'Ожидает',
				'is_system' => 1,
				'position' => 3,
			],
			[
				'name' => 'Передан в службу самовывоза',
				'is_system' => 1,
				'position' => 4,
			],
			[
				'name' => 'Передан в службу доставки',
				'is_system' => 1,
				'position' => 5,
			],
		]);
	}

	public function safeDown()
	{

	}
}