<?php

class m180701_042906_add_order_fields extends \webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_order}}', 'file', 'varchar(255) default null');
	}

	public function safeDown()
	{

	}
}