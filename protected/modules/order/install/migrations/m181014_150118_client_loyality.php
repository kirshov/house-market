<?php

class m181014_150118_client_loyality extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->createTable(
			"{{store_order_client_loyality}}",
			[
				"id" => "pk",
				"connect" => "varchar(150) not null",
				"status" => "tinyint not null default '0'",
			],
			$this->getOptions()
		);

		$this->createIndex("ux_{{store_order_client_loyality}}_connect", "{{store_order_client_loyality}}", "connect", true);
		$this->createIndex("ix_{{store_order_client_loyality}}_connect", "{{store_order_client_loyality}}", "connect", false);
	}

	public function safeDown()
	{

	}
}