<?php

class m180930_153401_order_track_num extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_order}}', 'track_num', 'varchar(64) default null');
	}

	public function safeDown()
	{

	}
}