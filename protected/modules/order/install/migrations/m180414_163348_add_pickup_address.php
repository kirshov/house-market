<?php

class m180414_163348_add_pickup_address extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_order}}', 'address', 'varchar(255) not null default ""');
	}

	public function safeDown()
	{

	}
}