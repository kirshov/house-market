<?php

class m180616_152549_add_custom_num extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_order}}', 'custom_num', 'varchar(32) default NULL');
	}

	public function safeDown()
	{

	}
}