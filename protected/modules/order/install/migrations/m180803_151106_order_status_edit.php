<?php

class m180803_151106_order_status_edit extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->update('{{store_order_status}}', ['name' => 'Ожидает оплаты'], 'id = 5');
	}

	public function safeDown()
	{

	}
}