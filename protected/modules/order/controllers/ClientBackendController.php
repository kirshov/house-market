<?php

use webforma\components\controllers\BackController;

/**
 * Class ClientBackendController
 */
class ClientBackendController extends BackController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin'],],
            ['allow', 'actions' => ['index'], 'roles' => ['Order.clientBackend.Index'],],
            ['allow', 'actions' => ['view'], 'roles' => ['Order.clientBackend.View'],],
            ['allow', 'actions' => ['create', 'productRow'], 'roles' => ['Order.clientBackend.Create'],],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Order.clientBackend.Delete'],],
            ['deny',],
        ];
    }

	/**
	 * @throws \CHttpException
	 */
	public function actionLoyality()
	{
		if (!Yii::app()->getRequest()->getIsAjaxRequest() || !Yii::app()->getRequest()->getIsPostRequest()) {
			throw new CHttpException(404);
		}

		$value = trim(Yii::app()->getRequest()->getPost('value'));
		$pk = Yii::app()->getRequest()->getPost('pk');

		if (!isset($pk)) {
			throw new CHttpException(404);
		}

		$model = ClientLoyality::model()->find('connect = :connect', [':connect' => $pk]);

		if(!$model){
			$model = new ClientLoyality();
			$model->connect = $pk;
		}

		$model->status = $value;

		if ($model->save()) {
			Yii::app()->ajax->success();
		}

		Yii::app()->ajax->rawText($model->getError('status'), 500);
	}

	/**
	 * @throws \CHttpException
	 */
	public function actionLoyalityByOrder()
	{
		if (!Yii::app()->getRequest()->getIsAjaxRequest() || !Yii::app()->getRequest()->getIsPostRequest()) {
			throw new CHttpException(404);
		}

		$value = trim(Yii::app()->getRequest()->getPost('value'));
		$pk = Yii::app()->getRequest()->getPost('pk');

		if (!isset($pk)) {
			throw new CHttpException(404);
		}

		$order = Order::model()->findByPk($pk);
		if (!isset($order)) {
			throw new CHttpException(404);
		}

		$model = ClientLoyality::model()->find('connect = :connect', [':connect' => $order->phone]);

		if(!$model){
			$model = new ClientLoyality();
			$model->connect = $order->phone;
		}

		$model->status = $value;

		if ($model->save()) {
			Yii::app()->ajax->success();
		}

		Yii::app()->ajax->rawText($model->getError('status'), 500);
	}

    /**
     *
     */
    public function actionIndex()
    {
        $model = new Client('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getQuery('Client')) {
            $model->setAttributes(Yii::app()->getRequest()->getQuery('Client'));
        }
        $this->render('index', ['model' => $model]);
    }

    /**
     * @param $id
     * @throws CHttpException
     */
    public function actionView($id)
    {
	    $model = User::model()->findByPk($id);

        if (null === $model) {
            throw new CHttpException(404);
        }
	    /**
	     * @var CActiveDataProvider $orders
	     */
        $orders = $model->getOrders();
        if(!$model->phone){
	        $ordersList = $orders->getData();
	        if($ordersList[0]){
		        $model->phone = $ordersList[0]->phone;
	        }
        }

        $this->render('view', [
	        'model' => $model,
	        'order' => new Order(),
	        'orders' => $orders,
        ]);
    }

	/**
	 * @throws CHttpException
	 */
	public function actionViewGuest()
	{
		$phone = Yii::app()->getRequest()->getParam('phone');
		if($phone){
			$phone = base64_decode($phone);
			$model = Order::model()->find([
				'condition' => 'phone = :phone',
				'params' => [
					':phone' => $phone,
				],
				'order' => 't.date DESC',
			]);
		}

		if (null === $model) {
			throw new CHttpException(404);
		}

		$provider = new CActiveDataProvider(
			'Order', [
				'criteria' => [
					'condition' => 'phone = :phone',
					'params' => [
						':phone' => $phone,
					],
				],
			]
		);


		$userData = new CArrayDataProvider([
			[
				'id' => 1,
				'name' => $model->name,
				'phone' => $model->phone,
				'email' => $model->email,
			]
		]);

		$this->render('view-guest', [
			'model' => $userData,
			'order' => $model,
			'orders' => $provider,
		]);
	}
}