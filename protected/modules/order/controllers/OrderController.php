<?php

/**
 * Class OrderController
 */
class OrderController extends \webforma\components\controllers\FrontController
{
	/**
	 * @var ProductRepository
	 */
	protected $productRepository;

	/**
	 * @param null $url
	 * @throws CException
	 * @throws CHttpException
	 */
    public function actionView($url = null)
    {
        if (!Yii::app()->getModule('order')->showOrder && !Yii::app()->getUser()->isAuthenticated()) {
            throw new CHttpException(404, Yii::t('OrderModule.order', 'Page not found!'));
        }
        $view = 'view';

        if(mb_stripos($url, 'new-') !== false){
            $url = mb_substr($url, 4);
            $view = 'created';
        }
        $model = Order::model()->findByUrl($url);

        if ($model === null) {
            throw new CHttpException(404, Yii::t('OrderModule.order', 'Page not found!'));
        }

	    $paymentMethod = Yii::app()->getRequest()->getPost('payment-method');
	    $paymentId = (int) Yii::app()->getRequest()->getPost('payment-id');
	    if(!$paymentId){
		    $paymentId = $model->payment_method_id;
	    }
	    if(!$paymentMethod){
	    	$_payment = $model->payment;
	    	if($_payment){
			    $paymentMethod = $_payment->module;
		    }
	    }

	    /*!$model->isPaymentMethodSelected() && */
	    if(!$model->isPaid() && $paymentId && $paymentMethod && Yii::app()->paymentManager){
		    $paymentSystem = Yii::app()->paymentManager->getPaymentSystemObject($paymentMethod);
		    $paymentModel = Payment::model()->published()->findByPk($paymentId);

		    if($paymentSystem && $paymentModel){
			    $result = $paymentSystem->initPayment($paymentModel, $model);
			    if(is_array($result)){
				    /*if($result['Success']){
					    $paymentResultData = [
						    'status' => true,
						    'url' => $result['paymentURL'],
					    ];
				    } else {
					    $paymentResultData = [
						    'status' => false,
						    'error' => $result['errorMessage'],
					    ];
				    }*/

				    foreach (['paymentId', 'paymentURL', 'errorCode', 'message', 'details', 'orderId'] as $_key){
					    if(isset($result[$_key])){
						    $paymentData[$_key] = $result[$_key];
					    }
				    }

				    $orderData = [
					    'payment_method_id' => $paymentId,
					    'payment_details' => json_encode($paymentData, JSON_UNESCAPED_UNICODE),
				    ];

				    $model->payment_method_id = $paymentId;
				    $model->payment_details = $orderData['payment_details'];
				    $paymentResultData = $orderData;
				    if(Order::model()->updateByPk($model->id, $orderData) === false) {
					    $paymentResultData = [
						    'status' => false,
						    'error' => 'Произошла внутренняя ошибка',
					    ];
				    } else {
					    $paymentResultData = CMap::mergeArray($paymentResultData, [
					    	'status' => true,
						    'url' => $paymentData['paymentURL'],
					    ]);
				    }

				    if(Yii::app()->getRequest()->getIsAjaxRequest()) {
					    echo json_encode($paymentResultData);
					    Yii::app()->end();
				    }
			    } else {
				    $paymentResultData = json_decode($model->payment_details, true);
			    }
		    }
	    }

	    $this->render($view, [
		    'model' => $model,
		    'paymentResultData' => $paymentResultData
	    ]);
    }

	/**
	 * @throws CHttpException
	 */
	public function actionBuyOneClick(){
		$request = Yii::app()->getRequest();
		$order = Yii::app()->getRequest()->getPost('Order');
		if($order['is_boc']) {
			if (!$request->getIsAjaxRequest()) {
				throw new CHttpException(404, Yii::t('WebformaModule.webforma', 'Page was not found!'));
			}
			$model = new Order(Order::SCENARIO_BOC);

			if ($request->getIsPostRequest() && $order) {
				$products = Yii::app()->getRequest()->getPost('OrderProduct');

				if ($model->store($order, $products, Yii::app()->getUser()->getId(), (int) Yii::app()->getModule('order')->defaultStatus)) {

					Yii::app()->eventManager->fire(OrderEvents::CREATED_HTTP, new OrderEvent($model));

					Yii::app()->ajax->success('<div class="modal-title">Ваш заказ №'.$model->getNumberForUser().' отправлен!</div> <span>Мы свяжемся с Вами в ближайшее время.</span>');
				} else {
					Yii::app()->ajax->failure(['errors' => $model->getErrors()]);
				}
			}
		}

		$this->productRepository = Yii::app()->getComponent('productRepository');

		$request = Yii::app()->getRequest();
		$id = $request->getParam('id');
		if(!$id){
			throw new CHttpException(404, Yii::t('StoreModule.catalog', 'Product was not found!'));
		}

		$product = $this->productRepository->getById($id);

		if(!$product){
			throw new CHttpException(404, Yii::t('StoreModule.catalog', 'Product was not found!'));
		}

		$model = new Order(Order::SCENARIO_BOC);

		$count = $request->getParam('count', 1);
		$price = $product->getResultPrice();
		$sum = $count * $price;

		$variantIds = $request->getParam('variant', []);
		sort($variantIds);
		$positionId = 'product_'.$product->id.'_'.implode('_', $variantIds);

		$this->renderPartial('_buy_one_click', [
			'order' => $model,
			'product' => $product,
			'count' => $count,
			'price' => $price,
			'sum' => $sum,
			'positionId' => $positionId,
			'variants' => $variantIds,
		]);
	}

    /**
     * @throws CHttpException
     */
    /*public function actionCheck()
    {
        if (!Yii::app()->getModule('order')->enableCheck) {
            throw new CHttpException(404);
        }

        $form = new CheckOrderForm();

        $order = null;

        if (Yii::app()->getRequest()->getIsPostRequest()) {

            $form->setAttributes(
                Yii::app()->getRequest()->getPost('CheckOrderForm')
            );

            if ($form->validate()) {
                $order = Order::model()->findByNumber($form->number);
            }
        }

        $this->render('check', ['model' => $form, 'order' => $order]);
    }*/
}
