<?php
return [
    '*unknown*' => '*неизвестно*',
    '--choose--' => '--выберите--',
    '--no--' => '--нет--',
	'Sources - create' => 'Источник заказа - добавление',
	'Sources - edit' => 'Источник заказа - редактирование',
	'Sources - manage' => 'Источник заказа - управление',
	'Sources - show' => 'Источник заказа - просмотр',
	'Sources list' => 'Источники заказов',
	'Sources' => 'Источники',
	'Source' => 'Источник заказов',
	'Manage' => 'Управление',
	'manage' => 'управление',
	'Source manage' => 'Источники заказов',
	'Create source' => 'Добавить источник заказа',
	'Create' => 'Добавление',
	'create' => 'добавление',
	'Creating source' => 'Создание источника заказа',
	'Create source and close' => 'Добавить источник и закрыть',
	'Create source and continue' => 'Добавить источник и продолжить',
	'Title' => 'Название',
	'Save source and close' => 'Сохранить источник и закрыть',
	'Save source and continue' => 'Сохранить источник и продолжить',
	'Short description' => 'Короткое описание',
	'Show source' => 'Просмотр источника',
	'Status' => 'Статус',
	'Draft' => 'Черновик',
	'Delete' => 'Удалить',
	'Description' => 'Описание',
	'Fields with' => 'Поля, отмеченные',
	'are required' => 'обязательны.',
	'Published' => 'Опубликовано',
	'Record was created!' => 'Запись добавлена!',
	'Editing source' => 'Редактирование источника',
	'Remove source' => 'Удалить источник',
	'Change' => 'Редактирование',
	'Change source' => 'Редактировать источник',
	'Do you really want to remove source?' => 'Вы уверены, что хотите удалить источник?',
];
