<?php
class OrderMailMessage extends WMailMessage{

	/**
	 * @var string
	 */
	public $icon = 'fa-shopping-basket';

	/**
	 * @var
	 */
	protected $_currency;

	/**
	 * @var Order
	 */
	protected $model;

	/**
	 * @var string
	 */
	protected $module = 'order';

	/**
	 * @var array
	 */
	protected $images = [
		'payment-wait' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFcAAABXCAYAAABxyNlsAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQ1IDc5LjE2MzQ5OSwgMjAxOC8wOC8xMy0xNjo0MDoyMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTkgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjQ1NzJCQzA0NkJBQTExRTk4NjY1OTNFQzgwMTY1RTVDIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjQ1NzJCQzA1NkJBQTExRTk4NjY1OTNFQzgwMTY1RTVDIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NDU3MkJDMDI2QkFBMTFFOTg2NjU5M0VDODAxNjVFNUMiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NDU3MkJDMDM2QkFBMTFFOTg2NjU5M0VDODAxNjVFNUMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6F6W/2AAACQklEQVR42uycPUsDQRCGL+ofUFCwSy8GLOziV2Fh7CLYCH7bWlrZ+TM0gVikM2Us/cBOUESsTGsULLQTIRBnyQhHNBjM3u1x97zwsktYZsjDZfZ2Lkmq2Wx6KBj1gQC4wEXABS5wEXCBC1wEXOACFwEXuMBFwAUucBFwgYuAC1zgIuACF7gIuMCNvQbi+sbGJ7PtLw2L98V799dXn8C1JwP2TDwmTol3YwP3l6vIFdgH8QE1145G2sDOiV/jVnNvxBMOIYcONswr98gh2HMXYMOEWxZ/6Dytm0qQTmsukzPvAmyYZeFdXBGviHf0lihIbetocr7JrVfsDxGHOk6HkGsmAuUoVLiX4pp4SrwYYJ6c5jC5LpIC1/wmq6Dz1QDzrOlY1JyJ6S0cixviZfFQAPEHNbbJUXJ9kx023Lq4qvOtADeyU82VKLierzTkAqq3zjcyl3CrelXNim02HbIa89n36Ugc3IbWXtulYVPHkuZIJNzv0mB28nVxv4V4JsaGxix6EZEruDW97/X0xGZrIzMxH5MO139iy1uItRSljSwKcCvac5gXZ3qIk9EYJtYJcFsyHauyzu+0Xv7HdxrD33lLPFzbH+OCFzG5fkB567X6r7FUKgr/cdPpAWZ7H7bbdZSFn1rQk1td572uA25bzRxVFyysA26ncmV5XeLhmlPWi/jpj55Dt+vY0OIsvuUIXOAi4AIXuAi4wAUuAi5wgYuAC1zgIuACF7ggAC5wEXCBC1wEXOACFwEXuMBFPehLgAEAWXF4k2ZQU5UAAAAASUVORK5CYII=',
		'cancelled' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFcAAABXCAYAAABxyNlsAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQ1IDc5LjE2MzQ5OSwgMjAxOC8wOC8xMy0xNjo0MDoyMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTkgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjcxNzE5MDg2NkJBQTExRTlCNDZDQTg0MTEzMUU1RDc2IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjcxNzE5MDg3NkJBQTExRTlCNDZDQTg0MTEzMUU1RDc2Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NzE3MTkwODQ2QkFBMTFFOUI0NkNBODQxMTMxRTVENzYiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NzE3MTkwODU2QkFBMTFFOUI0NkNBODQxMTMxRTVENzYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4rwvt1AAABrElEQVR42uzcQWrCQBSA4Tr0Cp7Bhe1G6K4KegUP0h6lPUh7hJZWd0I30oVXMIdIX+gEJBAa9dl5L/P/8EBERvgIcTILB2VZXtFlChCACy6BCy64BC644BK44IJL4IILLoELLrgELrgELrjgErjggkvggtv7rlN86e3d/b9+33az5srltqDbROZTZqi4ZrXWR1w7a9xnmanMuxJwtcabzEzmKXfcpcy3zFgBuIa9iWsuc8fdyywUgJuwc5mCrdgv8PwMYJOwlnYLxYnAZmGtbcWOBR7Gz5iEtbjP7Qpcw46twlp9iPgL2AWs5Se0NmA3sMnOFo4ErjFX8f2RB1jruIfAq4hatfMAa/m20Ius49b32FG8YnfxtdZZRLa4zR+vaRyts4hscdt2Bac+yYHbcR/rBjg4g3UFHBzCugEORmEXHfexpoGDUdi94llEtrjnwpoGTo37qgDbBvySO+6DzFoBtglcrfmYGjf1wc1XfOrSrLjAmr08W3DdgD9t48oFl8AFF1wCF1xwCVxwwSVwwQWXwAUXXAIXXAIXXHAJXHDBJXDBBZfANdmPAAMAZTOE3V4nOfQAAAAASUVORK5CYII=',
		'pickup' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFcAAABXCAYAAABxyNlsAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQ1IDc5LjE2MzQ5OSwgMjAxOC8wOC8xMy0xNjo0MDoyMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTkgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjk4NUEwQjA5NkJBQTExRTlBMDA0RjBGOUI2NkNDOUFCIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjk4NUEwQjBBNkJBQTExRTlBMDA0RjBGOUI2NkNDOUFCIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6OTg1QTBCMDc2QkFBMTFFOUEwMDRGMEY5QjY2Q0M5QUIiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6OTg1QTBCMDg2QkFBMTFFOUEwMDRGMEY5QjY2Q0M5QUIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6miYM5AAAB/ElEQVR42uycTStEYRiGHcZCibJQys5GSqIsRMr3RpEokZCPn+MHyGfYICsLspAoC6WR1ewsFMmCFSWO+61ncZoF42POzJy57rqSVzPPzOWed97RDM/3/QKSnhSiALnIJchFLnIJcpGLXIJc5CKXIBe5yCXIRS5yCXKRS5CLXOQS5CIXueQHiYUxpL657S8XLxUjYk6UiyWxIR5TvYLrizOam5QWE3knVuz7OrEgbsW26M7m+xDLsttTKcbFvKgNrB+IHfEkJsSgtdlxY/JXTXrWxAvjXY7fbAtFok9MiwFRbOvnwj2el0Ui6TJV9gvoFO229i4Ore374i3T20Im5daISZNaHVjfE1v2NZX02nWMBtYerMmORL7ILRFDYkp0ufm2fmKtczLufzmmTMyKftFha+7OnVqbdyX5JYpym6xdY6Ii8KN14/ifR7bavJnA2rPYdPuzJF9GSW5wyJE95NfEawinIdfmYdET2IO9KJ4WGkU8xHkfYtFoCHl26GfEeEHmcsWLCP62QHLhFVq6n009mktzc6phWfHJRZqbB82N5GdkaS57LnsuYc+luTSXPZfmIhe5BLnIRS5BLufcL8+ffsjzaG6ux+PfDdJc5BLkIhe5BLnIRS5BLnKRS5CLXOQS5CIXuQS5yCXIRS5yCXKRi1yCXORGPp8CDAABCGs+7QLeiAAAAABJRU5ErkJggg==',
		'delivery' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFcAAABXCAYAAABxyNlsAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQ1IDc5LjE2MzQ5OSwgMjAxOC8wOC8xMy0xNjo0MDoyMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTkgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkI4RkQyQ0MzNkJBQTExRTlBQjhBRkRDRjFCRDVDQTQ2IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkI4RkQyQ0M0NkJBQTExRTlBQjhBRkRDRjFCRDVDQTQ2Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QjhGRDJDQzE2QkFBMTFFOUFCOEFGRENGMUJENUNBNDYiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QjhGRDJDQzI2QkFBMTFFOUFCOEFGRENGMUJENUNBNDYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5pbkssAAAB9ElEQVR42uyavUoDQRRGXRFBFLEyDyCijTYipLDyEbSQNDYikkK0T5/GQgtBsFQkWCRPEUEM2ARBHyFW/oGNuH6BW9hsJOtsMpOcAx+7zIZJcnL3zmaTKI7jEciGURQgF7mAXOQiF5CLXOQCcpGLXEAucpELyEUucgG5yAXkIhe50Imxfr+ApdW1jsebjTpy/0nSP1Mi2gJ4XbkRch3300Hss72u3HhYq5ieG3jlRsMqN+L/uQFXrha1zD89LYBenh303AG5zo26rEYvzgoql8rta1/f1plwSeVmw5k+gEXkpu/nSblQJpVrCZ5Arlt2lBtlWTlGrlu+lD3lu71V9W4h1y0PyoHtn0vwHHLdcqpcKdPWf8eR65Zd5U5ZUY6Q65ZPpWj7+8h1z32ar+PI5d4CcnvBvFJVXi1VG/N13q7p142bBeVWmfk1tqGsK3nlybN5g6rcsgmoKDlLxcbKHs6b7oZH1r+hJdwOfLOL+VyzUX+2x81q01I+lKmUT9fVvFn/PMSCFnLlJlRz1Xph+5Q9tOETpaDUVFGbPs0bWuWWlBd70y1LwcZKHs4bjlxV0KOt3jXl3dLez9sxr+YNqi3wJQKQi1zkAnKRC8hFLnIBuchFLiAXucgF5CIXuYBc5CIXkItcQC5ykQvIRS5y4U9+BBgARE2IFEm94GQAAAAASUVORK5CYII=',
		'fulfilled' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFcAAABXCAYAAABxyNlsAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQ1IDc5LjE2MzQ5OSwgMjAxOC8wOC8xMy0xNjo0MDoyMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTkgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkQ0MDZFQUQwNkJBQTExRTlCMkY0OEE3NTcxMjYzRUMxIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkQ0MDZFQUQxNkJBQTExRTlCMkY0OEE3NTcxMjYzRUMxIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RDQwNkVBQ0U2QkFBMTFFOUIyRjQ4QTc1NzEyNjNFQzEiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RDQwNkVBQ0Y2QkFBMTFFOUIyRjQ4QTc1NzEyNjNFQzEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5jSjMiAAABg0lEQVR42uzcTUrDQBiA4UR6hV7Cn2XBhRsFEfRaXsilm7pxIwU32oVXaI8gWCcYQUoRf5LON+nzwrdO8xCmmSm0Xq1WlfppDwFcuIILF67gwoUruHDhCi5cuIILF67gwhVcuHAFFy5cwYULV3BDNtrGRY4mJ+Fu/Gl278ntsHGaS8tCP7B3aW7SXMHtHvYgzUuaGdzuYedpztIs4BYOO2Tc7LC5cOtdgM2Be5HmoQUYNOy2cZtrXaeZpJn2ABwKdtu4b+07ZnPjhy3EeKiwOZaFRXvj8xaiC+CQsLm+0LoEDgub81WsC+DQsLnfc/8DHB42wibiL8BFwEbZof0GeB32NCpspO3vT4A3wS4j78EjnS18B1wcbMSDm03A+yXCNo0CfqZP4GkL+lx9HPYUBRvxyV0Hbn41eC0RNuqT+xX4OM15msfSYKPjNjWHPbdVodX++mr31ly4ggsXruDChSu4cOEKLly4CODCFVy4cAUXLlzBhQtXcOHCFVy4cNVb7wIMAOfBbgcYnl4DAAAAAElFTkSuQmCC',
	];

	/**
	 * @return array
	 */
	public function getDefaultMessages()
	{
		return [
			'newOrderAdmin' => [
				'name' => 'Сообщение администратору о новом заказе',
				'subject' => 'Новый заказ №%order% в интернет-магазине "%siteName%"',
				'body' => '
					<h2 style="'.$this->getStyle('title').'">Новый заказ №%order% на сумму %total%</h2>
					<h3 style="'.$this->getStyle('subtitle').'">в интернет-магазине "%siteName%"</h3>
					%info%
					<h2 style="font: bold %fontH3%; color:#222; margin: 30px 0 20px;">Состав заказа:</h2>
					%products%
					<br/><br/>
					<p align="center">%link-order%</div>
				',
			],
			'newOrderUser' => [
				'name' => 'Сообщение пользователю о новом заказе',
				'subject' => 'Ваш заказ №%order% в интернет-магазине "%siteName%"',
				'body' => '
					<h2 style="'.$this->getStyle('title').'">Новый заказ №%order%</h2>
					<h3 style="'.$this->getStyle('subtitle').'">в интернет-магазине "%siteName%"</h3>
					%info%
					<h2 style="font: bold %fontH3%; color:#222; margin: 30px 0 20px;">Состав заказа:</h2>
					%products%
					<br/><br/>
					<p align="center">%link-order%</div>
				',
			],
			'orderChangeStatus' => [
				'name' => 'Сообщение пользователю об изменени статуса заказа',
				'subject' => 'Статус заказа №%order% изменен',
				'body' => '<h2 style="'.$this->getStyle('title').'">Статус заказа №%order% изменен</h2>
					<h3 style="'.$this->getStyle('subtitle').'">интернет-магазин "%siteName%"</h3>
					%info%
					<h2 style="font: bold %fontH3%; color:#222; margin: 30px 0 20px;">Состав заказа:</h2>
					%products%
					<br/><br/>
					<p align="center">%link-order%</div>
				',
			],
			'orderChange' => [
				'name' => 'Сообщение пользователю об изменени заказа',
				'subject' => 'Заказ №%order% изменен',
				'body' => '<h2 style="'.$this->getStyle('title').'">Заказ №%order% изменен</h2>
					<h3 style="'.$this->getStyle('subtitle').'">интернет-магазин "%siteName%"</h3>
					%info%
					<h2 style="font: bold %fontH3%; color:#222; margin: 30px 0 20px;">Состав заказа:</h2>
					%products%
					<br/><br/>
					<p align="center">%link-order%</div>
				',
			],
			'orderCancel' => [
				'name' => 'Сообщение пользователю об отмене заказа',
				'subject' => 'Заказ №%order% отменен!',
				'body' => '<h2 style="'.$this->getStyle('title').'">Интернет-магазин "%siteName%"</h2>
					<div style="text-align: center; width: 87px; height: 87px; margin: 44px auto 44px; border: 2px solid %borderColor%; border-radius: 50%; position: relative;">
						<img src="'.$this->getImage('cancelled').'">
					</div>
					<h2 style="text-align: center; color: #222; font: bold %fontH2%; margin-bottom: 26px;">Заказ №%order%<br/> отменен!</h2>

					<p style="text-align: center; font: %font%;">Если вы считаете, что заказ был отменен ошибочно, пожалуйста, свяжитесь с нами!</p>
					<p align="center">%link-order%</div>
				',
			],
			'orderChangePickup' => [
				'name' => 'Сообщение пользователю об передачи заказа в ПСВ',
				'subject' => 'Заказ №%order% отправлен в пункт самовывоза',
				'body' => '<h2 style="'.$this->getStyle('title').'">Интернет-магазин "%siteName%"</h2>
					<div style="text-align: center; width: 87px; height: 87px; margin: 44px auto 44px; border: 2px solid %borderColor%; border-radius: 50%; position: relative;">
						<img src="'.$this->getImage('pickup').'">
					</div>
					<h2 style="text-align: center; color: #222; font: bold %fontH2%; margin-bottom: 26px;">Заказ №%order%<br/> отправлен в пункт самовывоза!</h2>
					%date%
					%track%
					<p align="center">%link-order%</div>',
			],
			'orderChangeDelivery' => [
				'name' => 'Сообщение пользователю об передачи заказа в доставку',
				'subject' => 'Заказ №%order% передан в службу доставки',
				'body' => '<h2 style="'.$this->getStyle('title').'">Интернет-магазин "%siteName%"</h2>
					<div style="text-align: center; width: 87px; height: 87px; margin: 44px auto 44px; border: 2px solid %borderColor%; border-radius: 50%; position: relative;">
						<img src="'.$this->getImage('delivery').'">
					</div>
					<h2 style="text-align: center; color: #222; font: bold %fontH2%; margin-bottom: 26px;">Заказ №%order%<br/> передан в службу доставки!</h2>
					%date%
					%track%
					<p align="center">%link-order%</div>
				',

			],
			'orderDone' => [
				'name' => 'Сообщение пользователю о выполненном заказе',
				'subject' => 'Заказ №%order% выполнен',
				'body' => '<h2 style="'.$this->getStyle('title').'">Интернет-магазин "%siteName%"</h2>
					<div style="text-align: center; width: 87px; height: 87px; margin: 44px auto 44px; border: 2px solid %borderColor%; border-radius: 50%; position: relative;">
						<img src="'.$this->getImage('fulfilled').'">
					</div>
					<h2 style="text-align: center; color: #222; font: bold %fontH2%; margin-bottom: 26px;">Заказ №%order%<br/> выполнен!</h2>

					<p align="center">%link-order%</div>'
			],
			'orderWaitPayment' => [
				'name' => 'Сообщение пользователю об ожидании оплаты',
				'subject' => 'Заказ №%order% ожидает оплаты',
				'body' => '<h2 style="'.$this->getStyle('title').'">Интернет-магазин "%siteName%"</h2>
				<div style="text-align: center; width: 87px; height: 87px; margin: 44px auto 44px; border: 2px solid %borderColor%; border-radius: 50%; position: relative;">
					<img src="'.$this->getImage('payment-wait').'">
				</div>
				<h2 style="text-align: center; color: #222; font: bold %fontH2%; margin-bottom: 26px;">Заказ №%order%<br/> ожидает оплаты</h2>

				<p style="text-align: center; margin-bottom: 26px; font: %font%;">После оплаты заказ будет собран и передан в службу доставки.</p>

				<p align="center">%link-payment%</div>'
			],
			'orderWaitReminderPayment' => [
				'name' => 'Напоминание пользователю об ожидании оплаты',
				'subject' => 'Заказ №%order% ожидает оплаты',
				'body' => '<h2 style="'.$this->getStyle('title').'">Интернет-магазин "%siteName%"</h2>
				<div style="text-align: center; width: 87px; height: 87px; margin: 44px auto 44px; border: 2px solid %borderColor%; border-radius: 50%; position: relative;">
					<img src="'.$this->getImage('payment-wait').'">
				</div>
				<h2 style="text-align: center; color: #222; font: bold %fontH2%; margin-bottom: 26px;">Заказ №%order%<br/> ожидает оплаты</h2>

				<p style="text-align: center; margin-bottom: 26px; font: %font%;">Здравствуйте!<br/>
						Напоминаем о том, что Ваш заказ ожидает оплаты.<br/>
						После оплаты заказ будет собран и передан в службу доставки.
				</p>

				<p align="center">%link-payment%</div>',
			],
			'orderPaidAdmin' => [
				'name' => 'Сообщение администратору об оплате заказа',
				'subject' => 'Оплачен заказ №%order% на сумму %total%',
				'body' => '<h2 style="text-align: center; color: #222; font: bold %fontH2%;">Оплачен заказ №%order% на сумму %total%</h2>
					%info%
					<h2>Состав заказа:</h2>
					%products%
					<br/><br/>
					<p align="center">%link-order%</div>
				'
			],
			'orderPaidUser' => [
				'name' => 'Сообщение пользователю об оплате заказа',
				'subject' => 'Ваш заказ №%order% успешно оплачен!',
				'body' => '<h2 style="'.$this->getStyle('title').'">Интернет-магазин "%siteName%"</h2>
				<div style="text-align: center; width: 87px; height: 87px; margin: 44px auto 44px; border: 2px solid %borderColor%; border-radius: 50%; position: relative;">
					<img src="'.$this->getImage('fulfilled').'">
				</div>
				<h2 style="text-align: center; color: #222; font: bold %fontH2%; margin-bottom: 26px;">Заказ №%order%<br/> успешно оплачен!</h2>

				<p align="center">%link-order%</div>'
			],
		];
	}

	/**
	 * @param $code
	 * @param bool $isAdmin
	 * @return mixed|string
	 */
	public function getContentBlock($code, $isAdmin = false)
	{
		switch ($code){
			case '%order%':
				return $this->model->getNumberForUser();

			case '%total%':
				return ProductHelper::getFormatPrice($this->model->getTotalPriceWithDelivery()).' '.$this->getCurrency();

			case '%info%':
				return $this->includeContent('order.emailTemplate.info', ['order' => $this->model, 'isAdmin' => $isAdmin]);

			case '%products%':
				return $this->includeContent('order.emailTemplate.products', ['order' => $this->model, 'currency' => $this->getCurrency(), 'isAdmin' => $isAdmin]);

			case '%link-order%':
				return $this->getOrderLink();

			case '%link-payment%':
				return $this->getPaymentLink();

			case '%date%':
				return $this->getDateDelivery();

			case '%track%':
				return $this->getTrackNum();

			default:
				return parent::getContentBlock($code, $isAdmin);
		}
	}

	/**
	 * @return string
	 */
	protected function getCurrency(){
		if($this->_currency === null){
			$this->_currency = Yii::t('StoreModule.store', Yii::app()->getModule('store')->currency);
		}
		return $this->_currency;
	}

	/**
	 * @return string
	 */
	public function getOrderLink(){
		return '<a href="'.Yii::app()->createAbsoluteUrl('/order/order/view', ['url' => $this->model->url]).'" style="'.$this->getStyle('btn').'" >
			Посмотреть состав заказа на сайте
		</a>';
	}

	/**
	 * @return string
	 */
	public function getPaymentLink(){
		return '<a href="'.Yii::app()->createAbsoluteUrl('/order/order/view', ['url' => $this->model->url]).'#pay" style="'.$this->getStyle('btn').'">
			Перейти на страницу оплаты &rarr;
		</a>';
	}

	/**
	 * @return string
	 */
	public function getDateDelivery()
	{
		$html = '';
		if($this->model->date_delivery){
			if ($this->model->status_id == OrderStatus::STATUS_PICKUP){
				$html = 'Ориентировочная дата прибытия в пункт самовывоза: ';
			} elseif ($this->model->status_id == OrderStatus::STATUS_DELIVERY){
				$html = 'Дата доставки: ';
			}

			return $html ? '<p align="center" style="font: %font%;">'.$html.date('d.m.Y', strtotime($this->model->date_delivery)).'</p>' : '';
		}

		return '';
	}

	/**
	 * @return string
	 */
	public function getTrackNum(){
		$html = '';
		if($this->model->track_num){
			$html .= 'Номер для отслеживания: <b>'.$this->model->track_num.'</b>';
			if($this->model->delivery && $this->model->delivery->track_num_link){
				$html .= '<br/><a style="'.$this->getStyle('a').'" href="'.$this->model->delivery->track_num_link.$this->model->track_num.'" target="_blank">Отследить на сайте</a>';
			}
		}
		return $html ? '<p align="center" style="font: %font%;">'.$html.'</p>' : '';
	}
}
