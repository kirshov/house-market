<?php

/**
 * Class OrderNotifyService
 */
class OrderNotifyService extends NotifyService
{
	/**
	 * @var string
	 */
	protected $templatePath  = 'order/email';

	/**
	 * @var string
	 */
	protected $messageComponent = 'orderMailMessage';

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->module = Yii::app()->getModule('order');
    }

	/**
	 * @param Order $order
	 * @param string $code
	 * @return string
	 */
	protected function getDefaultSubject($order, $code){
		switch ($code){
			case 'newOrderAdmin':
			case 'newOrderUser':
				return 'Новый заказ '.$order->getNumberForUser().' в интернет-магазине "'.Yii::app()->getModule('webforma')->getShortSiteName().'"';
				break;
			case 'orderCancel':
				return 'Заказ №'.$order->getNumberForUser().' отменен';
				break;
			case 'orderChangeStatus':
				return 'Статус заказа №'.$order->getNumberForUser().' изменен';
				break;
			case 'orderChange':
				return 'Заказ №'.$order->getNumberForUser().' изменен';
				break;
			case 'orderChangeDelivery':
				return 'Заказ №'.$order->getNumberForUser().' передан в службу доставки';
				break;
			case 'orderChangePickup':
				return 'Заказ №'.$order->getNumberForUser().' отправлен в пункт самовывоза';
				break;
			case 'orderDone':
				return 'Заказ №'.$order->getNumberForUser().' выполнен!';
				break;
			case 'orderWaitPayment':
			case 'orderWaitReminderPayment':
				return 'Заказ №'.$order->getNumberForUser().' ожидает оплаты';
				break;
			case 'payOrderAdmin':
				return 'Оплачен заказ №'.$order->getNumberForUser();
				break;
			case 'payOrderUser':
				return 'Ваш заказ №'.$order->getNumberForUser().' успешно оплачен!';
				break;
		}
	}

	/**
	 * @param $model
	 * @param $code
	 * @param $isAdmin
	 * @return mixed
	 */
	protected function getBody($model, $code, $isAdmin = false)
	{
		return $this->view->render($this->getRenderFile($code), ['order' => $model, 'isAdmin' => $isAdmin], true);
	}

	/**
     * @param Order $order
     * @return bool
     */
    public function sendOrderCreatedAdminNotify(Order $order)
    {
        $from = $this->module->notifyEmailFrom ?: Yii::app()->getModule('webforma')->notifyEmailFrom;
		$to = $this->module->getNotifyTo();
		$code = 'newOrderAdmin';

		$mailContent = $this->getMailContent($order, $code, true);

        if($mailContent['subject'] && $mailContent['body']){
	        foreach ($to as $email) {
		        $this->mail->send($from, $email, $mailContent['subject'], $mailContent['body']);
	        }
        }

        return true;
    }

    /**
     * @param Order $order
     */
    public function sendOrderCreatedUserNotify(Order $order)
    {
        $from = $this->module->notifyEmailFrom ?: Yii::app()->getModule('webforma')->notifyEmailFrom;
		$code = 'newOrderUser';

	    $mailContent = $this->getMailContent($order, $code);

	    if($mailContent['subject'] && $mailContent['body']){
		    $this->mail->send($from, $order->email, $mailContent['subject'], $mailContent['body']);
	    }
    }

    /**
     * @param Order $order
     */
    public function sendOrderChangesNotify(Order $order)
    {
        if($order->status_id == OrderStatus::STATUS_DELETED){
        	$code = 'orderCancel';
        } elseif($order->status_id == OrderStatus::STATUS_WAIT_PAYMENT){
			$code = 'orderWaitPayment';
        } elseif($order->status_id == OrderStatus::STATUS_PICKUP){
        	$code = 'orderChangePickup';
	    } elseif($order->status_id == OrderStatus::STATUS_DELIVERY){
        	$code = 'orderChangeDelivery';
	    } elseif($order->status_id == OrderStatus::STATUS_FINISHED){
        	$code = 'orderDone';
		} elseif($order->isStatusChanged()) {
        	$code = 'orderChangeStatus';
	    } else {
	        $code = 'orderChange';
        }

	    $from = $this->module->notifyEmailFrom ?: Yii::app()->getModule('webforma')->notifyEmailFrom;

	    $mailContent = $this->getMailContent($order, $code);

	    if($mailContent['subject'] && $mailContent['body']){
		    $this->mail->send($from, $order->email, $mailContent['subject'], $mailContent['body']);
	    }
    }


	/**
	 * @param Order $order
	 */
	public function sendOrderWaitPayRemindNotify(Order $order)
	{
		$from = $this->module->notifyEmailFrom ?: Yii::app()->getModule('webforma')->notifyEmailFrom;
		$code = 'orderWaitReminderPayment';

		$mailContent = $this->getMailContent($order, $code);

		if($mailContent['subject'] && $mailContent['body']){
			$this->mail->send($from, $order->email, $mailContent['subject'], $mailContent['body']);
		}
	}

	/**
	 * @param Order $order
	 */
	public function sendOrderPayOrderAdmin(Order $order)
	{
		$from = $this->module->notifyEmailFrom ?: Yii::app()->getModule('webforma')->notifyEmailFrom;
		$to = $this->module->getNotifyTo();

		$code = 'orderPaidAdmin';

		$mailContent = $this->getMailContent($order, $code);

		if($mailContent['subject'] && $mailContent['body']){
			$this->mail->send($from, $to, $mailContent['subject'], $mailContent['body']);
		}
	}

	/**
	 * @param Order $order
	 */
	public function sendOrderPayOrderUser(Order $order)
	{
		$from = $this->module->notifyEmailFrom ?: Yii::app()->getModule('webforma')->notifyEmailFrom;
		$code = 'orderPaidUser';

		$mailContent = $this->getMailContent($order, $code);

		if($mailContent['subject'] && $mailContent['body']){
			$this->mail->send($from, $order->email, $mailContent['subject'], $mailContent['body']);
		}
	}
} 
