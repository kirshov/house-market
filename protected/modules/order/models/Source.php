<?php

/**
 * Модель Source
 *
 * @property string $id
 * @property string $name
 * @property string $description
 * @property integer $status
 *
 * @method Source published()
 */

use webforma\components\Event;
use webforma\widgets\WPurifier;

class Source extends webforma\models\WModel
{
    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;
    const STATUS_MODERATION = 2;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{store_order_source}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * @return Source the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['name, description', 'filter', 'filter' => 'trim'],
            ['name', 'filter', 'filter' => [new WPurifier(), 'purify']],
            ['name', 'required'],
            ['status', 'numerical', 'integerOnly' => true],
            ['status', 'length', 'max' => 11],
            ['name', 'length', 'max' => 250],
            ['status', 'in', 'range' => array_keys($this->statusList)],
            ['id, name, description, status', 'safe', 'on' => 'search'],
        ];
    }

    public function scopes()
    {
        return [
            'published' => [
                'condition' => 'status = :status',
                'params' => [':status' => self::STATUS_PUBLISHED],
            ],
        ];
    }


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('OrderModule.source', 'Id'),
            'parent_id' => Yii::t('OrderModule.source', 'Parent'),
            'name' => Yii::t('OrderModule.source', 'Title'),
            'image' => Yii::t('OrderModule.source', 'Image'),
            'short_description' => Yii::t('OrderModule.source', 'Short description'),
            'description' => Yii::t('OrderModule.source', 'Description'),
            'slug' => Yii::t('OrderModule.source', 'Alias'),
            'status' => Yii::t('OrderModule.source', 'Status'),
        ];
    }

    /**
     * @return array customized attribute descriptions (name=>description)
     */
    public function attributeDescriptions()
    {
        return [
            'id' => Yii::t('OrderModule.source', 'Id'),
            'parent_id' => Yii::t('OrderModule.source', 'Parent'),
            'name' => Yii::t('OrderModule.source', 'Title'),
            'image' => Yii::t('OrderModule.source', 'Image'),
            'short_description' => Yii::t('OrderModule.source', 'Short description'),
            'description' => Yii::t('OrderModule.source', 'Description'),
            'slug' => Yii::t('OrderModule.source', 'Alias'),
            'status' => Yii::t('OrderModule.source', 'Status'),
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria();

        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider(get_class($this), ['criteria' => $criteria]);
    }

    /**
     * Returns available status list
     *
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_DRAFT => Yii::t('OrderModule.source', 'Draft'),
            self::STATUS_PUBLISHED => Yii::t('OrderModule.source', 'Published'),
        ];
    }

    /**
     * Returns current status name
     *
     * @return string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('OrderModule.source', '*unknown*');
    }

    /**
     * Returns parent source name
     *
     * @param string $empty Text shown if the source have no parent. Default: ---
     * @return string
     */
    public function getParentName($empty = '---')
    {
        return isset($this->parent) ? $this->parent->name : $empty;
    }

    public static function getList(){
	    return CHtml::listData(Source::model()->findAll(['order' => 'name']), 'id', 'name');
    }
}
