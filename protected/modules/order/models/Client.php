<?php

/**
 * Class Client
 *
 * @property User $user
 */
class Client extends webforma\models\WModel//Order
{
    /**
     * @var
     */
    public $ordersTotalNumber;

    /**
     * @var
     */
    public $ordersTotalSum;

	/**
	 * @var
	 */
	private $_identity;

	/**
	 * @var
	 */
	private $_viewAction;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{store_order}}';
	}

    /**
     * @param null|string $className
     * @return Client|static
     */
    static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array|mixed
     */
    public function relations()
    {
	    return CMap::mergeArray(parent::relations(), [
		    'user' => [self::BELONGS_TO, 'User', 'user_id'],
	    ]);
    }

    /**
     * @return mixed
     */
    public function getOrderNumber()
    {
    	return $this->ordersTotalNumber;
    }

    /**
     * @return mixed
     */
    public function getOrderSum()
    {
        return $this->ordersTotalSum;
    }


    /**
     * @return array|mixed
     */
    public function rules()
    {
    	return [];
        return CMap::mergeArray(
            parent::rules(),
            [
                [
                    'middle_name, first_name, last_name, nick_name, email, gender, status, access_level, visit_time, phone, ordersTotalNumber, ordersTotalSum',
                    'filter',
                    'filter' => 'trim',
                    'on' => 'search',
                ],
                [
                    'middle_name, first_name, last_name, nick_name, email, gender, status, access_level, visit_time, phone, ordersTotalNumber, ordersTotalSum',
                    'safe',
                    'on' => 'search',
                ],
            ]
        );
    }

	public function attributeLabels()
	{
		return [
			'name' => 'Имя',
			'phone' => 'Телефон',
		];
	}


	/**
     * Поиск пользователей по заданным параметрам:
     *
     * @return CActiveDataProvider
     */
    public function search($pageSize = 10)
    {
        $criteria = new CDbCriteria();
        $criteria->group = 't.phone';


        /*$criteria->compare('t.first_name', trim($this->first_name), true, 'OR');
        $criteria->compare('t.middle_name', trim($this->middle_name), true, 'OR');
        $criteria->compare('t.last_name', trim($this->last_name), true, 'OR');
        $criteria->compare('t.nick_name', trim($this->nick_name), true, 'OR');
        $criteria->compare('t.email', trim($this->email), true, 'OR');
        $criteria->compare('t.phone', trim($this->phone), true, 'OR');
        $criteria->compare('t.gender', $this->gender);
        $criteria->compare('t.status', $this->status);*/

        /*$orderTable = Order::model()->tableName();
        $orderNumberSql = "(select count(*) from {$orderTable} o where o.user_id = t.id) ";
        $orderSumSql = "(select sum(total_price) from {$orderTable} o where o.user_id = t.id AND o.paid = :paid) ";

        $criteria->compare($orderNumberSql, $this->ordersTotalNumber);
        $criteria->compare($orderSumSql, $this->ordersTotalSum);

        $criteria->select = [
            '*',
            $orderNumberSql.' as ordersTotalNumber',
            $orderSumSql.' as ordersTotalSum',
        ];

        $criteria->params[':paid'] = Order::PAID_STATUS_PAID;*/

	    $orderTable = Order::model()->tableName();
	    $orderNumberSql = "(select count(*) from {$orderTable} o where o.phone = t.phone) ";
	    $orderSumSql = "(select sum(total_price) from {$orderTable} o where o.phone = t.phone AND o.paid = :paid) ";

	    $criteria->select = [
		    '*',
		    $orderNumberSql.' as ordersTotalNumber',
		    $orderSumSql.' as ordersTotalSum',
	    ];

	    $criteria->params[':paid'] = Order::PAID_STATUS_PAID;

        return new CActiveDataProvider(
            __CLASS__, [
                'criteria' => $criteria,
                'sort' => [
                    'defaultOrder' => 't.date DESC',
                    'attributes' => [
                        'ordersTotalNumber' => [
                            'asc' => 'ordersTotalNumber ASC',
                            'desc' => 'ordersTotalNumber DESC',
                        ],
                        'ordersTotalSum' => [
                            'asc' => 'ordersTotalSum ASC',
                            'desc' => 'ordersTotalSum DESC',
                        ],
                        '*',
                    ],
                ],
            ]
        );
    }

	/**
	 * @return mixed|string
	 */
    public function getName(){
		if($this->user){
			return $this->user->getFullName();
		}

		return $this->name;
    }

    /**
     * @return null
     */
    public function getLastOrder()
    {
	    return null;
        /*if ($this->orders) {
            return $this->orders[0];
        }*/

        return null;
    }

	/**
	 * @return array|bool
	 */
    public function getViewLink(){
    	$identity = $this->getIdentityId();
    	if($identity){
		    $viewAction = $this->getViewAction();
		    $param = $viewAction == 'view' ? 'id' : 'phone';

		    return ['/order/clientBackend/'.$viewAction, $param => $identity];
	    }
    	 return false;
    }

	/**
	 * @return bool|mixed|string
	 */
    public function getIdentityId(){
    	if($this->_identity !== null){
    		return $this->_identity;
	    }
    	if($this->user_id){
		    $this->_identity = $this->user_id;
	    } elseif($this->phone) {
		    $this->_identity = base64_encode($this->phone);
	    } else {
    		$this->_identity = false;
	    }
	    return $this->_identity;
    }

	/**
	 * @return mixed|string
	 */
    public function getViewAction(){
    	if(!$this->_viewAction){
    		$this->_viewAction = is_numeric($this->_identity) ? 'view' : 'viewGuest';
	    }
	    return $this->_viewAction;
    }
}