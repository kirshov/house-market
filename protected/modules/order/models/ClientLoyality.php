<?php

/**
 * Class ClientLoyality
 *
 * @property int $id
 * @property string $connect
 * @property int $status
 *
 */
class ClientLoyality extends webforma\models\WModel
{
	static $_list;
    /**
     * @param null|string $className
     * @return ClientLoyality|static
     */
    static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{store_order_client_loyality}}';
	}

	public static function loyalityList(){
		return [
			2 => 'Супер',
			1 => 'Хорошо',
			0 => 'Нормально',
			-1 => 'Так себе',
			-2 => 'Ужасно',
		];
	}

	public static function labelList(){
		return [
			2 => ['class' => 'label-success'],
			1 => ['class' => 'label-info'],
			0 => ['class' => 'label-default'],
			-1 => ['class' => 'label-warning'],
			-2 => ['class' => 'label-danger'],
		];
	}

	public static function getLoyality($phone){
		if($phone){
			if(self::$_list == null){
				self::$_list = CHtml::listData(ClientLoyality::model()->findAll(), 'connect', 'status');
			}

			if(isset(self::$_list[$phone])){
				return self::$_list[$phone];
			}
			return 0;
		}
		return 0;
	}
}