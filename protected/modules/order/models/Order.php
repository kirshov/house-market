<?php
/**
 * @property integer $id
 * @property integer $delivery_id
 * @property double $delivery_price
 * @property integer $payment_method_id
 * @property integer $paid
 * @property string $payment_time
 * @property string $payment_details
 * @property double $total_price
 * @property double $discount
 * @property double $coupon_discount
 * @property integer $separate_delivery
 * @property integer $status_id
 * @property string $date
 * @property integer $user_id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $comment
 * @property string $ip
 * @property string $url
 * @property string $note
 * @property string $modified
 * @property string $zipcode
 * @property string $country
 * @property string $city
 * @property string $street
 * @property string $house
 * @property string $apartment
 * @property integer $manager_id
 * @property integer $pickup_id
 * @property string $address
 * @property string $date_delivery
 * @property string $custom_num
 * @property string $track_num
 * @property string $file
 * @property string $company
 * @property string $inn
 * @property string $address
 * @property integer $source_id;
 *
 * @property OrderProduct[] $products
 * @property Delivery $delivery
 * @property Payment $payment
 * @property User $user
 * @property OrderStatus $status
 * @property Pickup $pickup
 * @property Source $source
 *
 */

Yii::import('application.modules.order.OrderModule');
Yii::import('application.modules.order.events.OrderEvents');
Yii::import('application.modules.order.events.PayOrderEvent');
Yii::import('application.modules.order.events.OrderEvent');
Yii::import('application.modules.order.events.OrderChangeStatusEvent');

/**
 * Class Order
 */
class Order extends webforma\models\WModel
{
    /**
     *
     */
    const PAID_STATUS_NOT_PAID = 0;
    /**
     *
     */
    const PAID_STATUS_PAID = 1;

    /**
     *
     */
    const SCENARIO_USER = 'front';

    /**
     *
     */
    const SCENARIO_BOC = 'buy_one_click';

    /**
     *
     */
    const SCENARIO_ADMIN = 'admin';

    /**
     * @var null
     */
    public $couponId = null;

	/**
	 * @var string
	 */
	public $oldDate;

    /**
     * @var bool
     */
    public $is_boc;

    /**
     * @var OrderProduct[]
     */
    private $_orderProducts = [];

    /**
     * @var bool
     */
    private $hasProducts = false; // ставим в true, когда в сценарии front добавляем хотя бы один продукт

    /**
     * @var
     */
    protected $oldAttributes;

    /**
     * @var bool
     */
    private $productsChanged = false; // менялся ли список продуктов в заказе

    /**
     * @var null
     */
    private $_validCoupons = null;

	/**
	 * @var int
	 */
	private $loyality;

	/**
	 * @var int
	 */
	public $is_user;

	/**
	 * @var int
	 */
	public $cntOrders = 0;

	/**
	 * @var int
	 */
	public $totalAll;

	/**
	 * @var
	 */
	protected $productDataProvider;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{store_order}}';
    }

    /**
     * @param null|string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        $rules = [
            ['name, email, phone, zipcode, country, city, street, house, apartment, address, is_boc, date, file, date_delivery, track_num, loyality, company, inn', 'filter', 'filter' => 'trim'],
            //['status_id, phone', 'required'],
	        ['status_id, source_id', 'numerical'],
            ['phone', 'required', 'on' => self::SCENARIO_USER],
            ['phone', 'required', 'on' => self::SCENARIO_BOC],
	        [
		        'phone',
		        'match',
		        'pattern' => Yii::app()->getModule('webforma')->phonePattern,
		        'message' => 'Неверный формат телефона',
	        ],
            ['name', 'required', 'on' => self::SCENARIO_BOC],
            ['name, email', 'required', 'on' => self::SCENARIO_USER],
            ['email', 'email'],
            [
                'delivery_id, separate_delivery, payment_method_id, paid, user_id, couponId, manager_id',
                'numerical',
                'integerOnly' => true,
            ],
	        //['address', 'required', 'on' => self::SCENARIO_USER],
	        ['pickup_id', 'checkPickupId'],
	        ['delivery_id', 'checkNeedDelivery'],
            ['delivery_price, total_price, discount, coupon_discount', 'store\components\validators\NumberValidator'],
            ['name, phone, email, city, street', 'length', 'max' => 255],
            ['comment, note', 'length', 'max' => 1024],
            ['zipcode', 'length', 'max' => 30],
            ['house', 'length', 'max' => 50],
            ['country', 'length', 'max' => 150],
            ['apartment', 'length', 'max' => 10],
            ['url', 'unique'],
	        ['date, payment_details', 'filter', 'filter' => 'trim', 'on' => self::SCENARIO_ADMIN],
            [
                'user_id, paid, payment_time, payment_details, total_price, discount, coupon_discount, separate_delivery, status_id, date, ip, url, modified',
                'unsafe',
                'on' => self::SCENARIO_USER,
            ],
            [
                'id, delivery_id, delivery_price, payment_method_id, paid, payment_time, payment_details, total_price, discount, coupon_discount, separate_delivery, status_id, date, date_delivery, user_id, name, phone, email, comment, ip, url, note, modified, manager_id, source_id, loyality, is_user',
                'safe',
                'on' => 'search',
            ],
        ];

        if(Yii::app()->hasModule('uds')){
	        $rules[] = ['uds_code, uds_user, uds_discount', 'filter', 'filter' => 'trim'];
        }

        return $rules;
    }


	/**
	 * @return bool
	 */
    public function checkPickupId(){
    	$flag = true;
    	if(!$this->delivery_id){
		    $flag = true;
	    }

    	$delivery = Delivery::model()->findByPk($this->delivery_id);
    	if($this->delivery_id && $this->delivery && $this->delivery->pickup_group_id){
    		if(!$this->pickup_id){
			    $flag = false;
		    } else {
    			$criteria = new CDbCriteria();
    			$criteria->addInCondition('group_id', (array) $delivery->pickup_group_id);
			    $pickup = Pickup::model()->published()->findByPk($this->pickup_id, $criteria);
    			if(!$pickup){
				    $flag = false;
			    }
		    }
	    }

	    if(!$flag){
    		$this->addError('pickup_id', 'Необходимо выбрать пункт самовывоза');
	    }
	    return $flag;
    }

    /**
	 * @return bool
	 */
    public function checkNeedDelivery(){
    	$flag = true;
    	if(!$this->delivery_id || !$this->delivery){
		    $flag = true;
	    }

    	if($this->delivery->need_address){
    		$flag = !empty($this->address);
	    }
	    if(!$flag){
    		$this->addError('address', 'Необходимо заполнить адрес доставки ');
	    }
	    return $flag;
    }

    /**
     * @return array
     */
    public function relations()
    {
        $relations = [
            'products' => [self::HAS_MANY, 'OrderProduct', 'order_id', 'order' => 'products.id ASC'],
            'delivery' => [self::BELONGS_TO, 'Delivery', 'delivery_id'],
            'pickup' => [self::BELONGS_TO, 'Pickup', 'pickup_id'],
            'status' => [self::BELONGS_TO, 'OrderStatus', 'status_id'],
            'client' => [self::BELONGS_TO, 'User', 'user_id'],
            'source' => [self::BELONGS_TO, 'Source', 'source_id'],
            'manager' => [self::BELONGS_TO, 'User', 'manager_id'],
        ];

        if(Yii::app()->hasModule('payment')){
	        $relations['payment'] = [self::BELONGS_TO, 'Payment', 'payment_method_id'];
        }

	    if(Yii::app()->hasModule('coupon')){
		    $relations = CMap::mergeArray($relations, [
			    'couponsIds' => [self::HAS_MANY, 'OrderCoupon', 'order_id'],
			    'coupons' => [self::HAS_MANY, 'Coupon', 'coupon_id', 'through' => 'couponsIds'],
		    ]);

	    }

	    return $relations;
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'new' => [
                'condition' => 't.status_id = :status_id',
                'params' => [':status_id' => OrderStatus::STATUS_NEW],
            ],
	        'work' => [
		        'condition' => 't.status_id = :status_id',
		        'params' => [':status_id' => OrderStatus::STATUS_WORK],
	        ],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'CTimestampBehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
                'createAttribute' => 'date',
                'updateAttribute' => 'modified',
            ],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('OrderModule.order', '#'),
            'delivery_id' => Yii::t('OrderModule.order', 'Delivery method'),
            'delivery_price' => Yii::t('OrderModule.order', 'Delivery price'),
            'payment_method_id' => Yii::t('OrderModule.order', 'Payment method'),
            'paid' => Yii::t('OrderModule.order', 'Paid'),
            'payment_time' => Yii::t('OrderModule.order', 'Paid date'),
            'payment_details' => Yii::t('OrderModule.order', 'Payment details'),
            'total_price' => Yii::t('OrderModule.order', 'Total price'),
            'discount' => Yii::t('OrderModule.order', 'Discount (%)'),
            'coupon_discount' => Yii::t('OrderModule.order', 'Discount coupon'),
            'separate_delivery' => Yii::t('OrderModule.order', 'Separate delivery payment'),
            'status_id' => Yii::t('OrderModule.order', 'Status'),
            'date' => Yii::t('OrderModule.order', 'Created'),
            'date_delivery' => 'Дата доставки/самовывоза',
            'user_id' => Yii::t('OrderModule.order', 'Client'),
            'name' => 'Имя, фамилия',
            'phone' => 'Телефон',
            'email' => 'Электронная почта',
            'comment' => Yii::t('OrderModule.order', 'Comment'),
            'ip' => Yii::t('OrderModule.order', 'IP'),
            'url' => Yii::t('OrderModule.order', 'Url'),
            'note' => Yii::t('OrderModule.order', 'Note'),
            'modified' => Yii::t('OrderModule.order', 'Update date'),
            'zipcode' => Yii::t('OrderModule.order', 'Zipcode'),
            'country' => Yii::t('OrderModule.order', 'Country'),
            'city' => Yii::t('OrderModule.order', 'City'),
            'street' => Yii::t('OrderModule.order', 'Street'),
            'house' => Yii::t('OrderModule.order', 'House'),
            'apartment' => Yii::t('OrderModule.order', 'Apartment'),
            'manager_id' => Yii::t('OrderModule.order', 'Manager'),
            'address' => Yii::t('OrderModule.order', 'Address'),
            'pickup_id' => 'Пункт самовывоза',
			'source_id' => 'Источник заказа',
			'track_num' => 'Номер для отслеживания',
			'loyality' => 'Адекватность',
			'is_user' => 'Зарегистрированный пользователь',
			'couponId' => 'Купон',
			'delivery' => 'Способ доставки',
			'uds_discount' => 'UDS Скидка',
			'file' => 'Файл',
			'inn' => 'ИНН',
			'company' => 'Название компании',
        ];
    }

    /**
     * @return CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->with = ['delivery', 'delivery.deliveryCalc', 'pickup', 'client', 'status'];
        if(Yii::app()->hasModule('payment')){
	        $criteria->with[] = 'payment';
        }
        $criteria->select = 't.*, COUNT(so.id) as cntOrders';
        $criteria->join = 'LEFT JOIN {{store_order}} so ON t.phone = so.phone';
        $criteria->group = 't.id';

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.name', $this->name, true);
	    $criteria->compare('t.track_num', $this->name, true, 'OR');
        $criteria->compare('t.delivery_id', $this->delivery_id);
        $criteria->compare('t.pickup_id', $this->pickup_id);
        $criteria->compare('t.delivery_price', $this->delivery_price);
        $criteria->compare('t.payment_method_id', $this->payment_method_id);
        $criteria->compare('t.paid', $this->paid);
        $criteria->compare('t.payment_time', $this->payment_time);
        $criteria->compare('t.payment_details', $this->payment_details, true);
        $criteria->compare('t.total_price', $this->total_price);
        $criteria->compare('t.discount', $this->discount);
        $criteria->compare('t.coupon_discount', $this->coupon_discount);
        $criteria->compare('t.separate_delivery', $this->separate_delivery);
        $criteria->compare('t.status_id', $this->status_id);
        if(!empty($this->date)){
	        $criteria->compare('date', date('Y-m-d', strtotime($this->date)), true);
        }

        if(is_numeric($this->is_user)){
	        if($this->is_user){
		        $criteria->addCondition('user_id IS NOT NULL');
	        } else {
		        $criteria->addCondition('user_id NOT NULL');
	        }
        }

        $criteria->compare('t.user_id', $this->user_id);

        $replacePhone = 'replace(replace(replace(replace(t.phone, "-", ""), " ", ""), ")", ""), "(", "")';
	    $criteria->addSearchCondition($replacePhone, $this->phone, true);
        $criteria->compare('t.phone', $this->phone, true, 'OR');
        $criteria->compare('t.email', $this->email, true);
        $criteria->compare('comment', $this->comment, true);
        $criteria->compare('ip', $this->ip, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('note', $this->note, true);
        $criteria->compare('modified', $this->modified, true);
        $criteria->compare('t.manager_id', $this->manager_id);
        $criteria->compare('t.source_id', $this->source_id);
        if ($this->couponId) {
            $criteria->with['couponsIds'] = ['together' => true];
            $criteria->addCondition('couponsIds.coupon_id = :id');
            $criteria->params = CMap::mergeArray($criteria->params, [':id' => $this->couponId]);
        }

        /*if (null !== $this->name) {
            $clientCriteria = new CDbCriteria();
            $clientCriteria->with['client'] = ['together' => true];
            $clientCriteria->addSearchCondition('client.last_name', $this->name, true, 'OR');
            $clientCriteria->addSearchCondition('client.first_name', $this->name, true, 'OR');
            $clientCriteria->addSearchCondition('client.middle_name', $this->name, true, 'OR');
            $clientCriteria->addSearchCondition('client.nick_name', $this->name, true, 'OR');
            $criteria->mergeWith($clientCriteria, 'OR');
        }*/

        return new CActiveDataProvider(
            __CLASS__, [
                'criteria' => $criteria,
                'sort' => ['defaultOrder' => $this->getTableAlias().'.id DESC'],
            ]
        );
    }

    /**
     * @return CActiveDataProvider
     */
    public function searchCoupons()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('t.order_id', $this->id);

        $criteria->with = ['coupon'];

        return new CActiveDataProvider(
            OrderCoupon::_CLASS_(), [
                'criteria' => $criteria,
            ]
        );
    }

    /**
     *
     */
    public function afterFind()
    {
        $this->oldAttributes = $this->getAttributes();
        /*if($this->date){
        	if($this->date == '0000-00-00 00:00:00'){
		        $this->date = null;
	        } else {
		        $this->oldDate = $this->date;
		        $this->date = date('d.m.Y', strtotime($this->date));
	        }
        }*/

	    if($this->date_delivery){
        	if($this->date_delivery == '0000-00-00 00:00:00'){
        		$this->date_delivery = null;
	        } else {
		        $this->date_delivery = date('d.m.Y', strtotime($this->date_delivery));
	        }
	    }

	    if(!$this->custom_num){
	    	$this->saveCustomName();
	    }
        parent::afterFind();
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if ($this->getScenario() === self::SCENARIO_USER) {
            if (!$this->hasProducts) {
                $this->addError('products', Yii::t('OrderModule.order', 'There are no selected products'));
            }
        }

        return parent::beforeValidate();
    }

	/**
     * @return float|int
     */
    public function getProductsCost()
    {
        $cost = 0;
        $products = $this->productsChanged ? $this->_orderProducts : $this->products;

        foreach ($products as $op) {
            $cost += $op->price * $op->quantity;
        }

        return $cost;
    }

    /**
     * @return bool
     */
    public function isCouponsAvailable()
    {
        return Yii::app()->hasModule('coupon');
    }

    /**
     * @return bool
     */
    public function hasCoupons()
    {
        return !empty($this->couponsIds);
    }

    /**
     * @return array
     */
    public function getCouponsCodes()
    {
        $codes = [];

        foreach ($this->coupons as $coupon) {
            $codes[] = $coupon->code;
        }

        return $codes;
    }

    /**
     * @return mixed
     */
    public function getCoupons()
    {
        return $this->coupons;
    }

    /**
     * @return int|mixed
     */
    public function getDeliveryCost()
    {
        $cost = $this->delivery_price;
        if ($this->isCouponsAvailable()) {
            $validCoupons = $this->getValidCoupons($this->getCouponsCodes());
            foreach ($validCoupons as $coupon) {
                if ($coupon->free_shipping) {
                    $cost = 0;
                }
            }
        }

        return $cost;
    }

    /**
     * Фильтрует переданные коды купонов и возвращает объекты купонов
     * @param $codes - массив кодов купонов
     * @return Coupon[] - массив объектов-купонов
     */
    public function getValidCoupons($codes)
    {
        if ($this->_validCoupons !== null) {
            return $this->_validCoupons;
        }

        $productsTotalPrice = $this->getProductsCost();

        $validCoupons = [];

        /* @var $coupon Coupon */
        /* проверим купоны на валидность */
        foreach ($codes as $code) {

            $coupon = Coupon::model()->getCouponByCode($code);

            if (null !== $coupon && $coupon->getIsAvailable($productsTotalPrice)) {
                $validCoupons[] = $coupon;
            }
        }

        return $validCoupons;
    }

    /**
     * Получает скидку для переданных купонов
     * @param $coupons Coupon[]
     * @return float - скидка
     */
    public function getCouponDiscount(array $coupons)
    {
        $productsTotalPrice = $this->getProductsCost();
        $delta = 0.00;
        if ($this->isCouponsAvailable()) {
            foreach ($coupons as $coupon) {
                switch ($coupon->type) {
                    case CouponType::TYPE_SUM:
                        $delta += $coupon->value;
                        break;
                    case CouponType::TYPE_PERCENT:
                        $delta += ($coupon->value / 100) * $productsTotalPrice;
                        break;
                }
            }
        }

        return Yii::app()->getModule('store')->roundPrice ? round($delta, 0) : (float) $delta;
    }

    /**
     * @param array $attributes
     * @param array $products
     * @param int $status
     * @param int $client
     *
     * @return bool
     */
    public function store(array $attributes, array $products, $client = null, $status = OrderStatus::STATUS_NEW)
    {
        $isNew = $this->getIsNewRecord();
        $transaction = Yii::app()->getDb()->beginTransaction();

        try {
            $this->status_id = (int)$status;
            $this->user_id = $client;

            $this->setAttributes($attributes);
            $this->setProducts($products);

            if(Yii::app()->hasModule('uds')){
            	$udsData = Yii::app()->getUser()->getState('uds-data');
            	if($udsData){
		            $this->uds_code = $udsData['code'];
		            $this->uds_user = $udsData['userId'];
		            $this->uds_discount = $udsData['discount'];
	            }
            }

            if(!$this->city && $_SESSION['geo']['city']['name_ru']){
	            $this->city = $_SESSION['geo']['city']['name_ru'];
            }

	        if($this->getScenario() == self::SCENARIO_ADMIN){
		        if($this->date){
			        list($date, $time) = explode(' ', $this->date);
			        $this->date = date('Y-m-d', strtotime($this->date));
		            $timeCount = sizeof(explode(':', $time));
		            if($timeCount == 3){
				        $this->date .= ' '.$time;
			        } elseif ($timeCount == 2){
		                $this->date .= ' '.$time.':00';
			        } else {
				        $this->date .= ' '.date('H:i:s', time());
			        }

			        if(!Helper::isDate($this->date)){
			        	$this->date = new CDbExpression('NULL');
			        }
		        } else {
		        	$this->date = date('Y-m-d H:i:s');
		        }

		        if($this->date_delivery){
			        $this->date_delivery = date('Y-m-d', strtotime($this->date_delivery));

			        if(!Helper::isDate($this->date_delivery, 'Y-m-d')){
				        $this->date_delivery = new CDbExpression('NULL');
			        }
		        } else {
			        $this->date_delivery = null;
		        }
	        }

            if(in_array($this->getScenario(), [self::SCENARIO_BOC, self::SCENARIO_USER])){
                $minSumCart = Yii::app()->getModule('store')->minCost;
                if($this->is_boc){
                    $product = reset($this->_orderProducts);
                    $currentCost = $product->price;
                } else {
                    $currentCost = Yii::app()->cart->getCost();
                }

                if($minSumCart > $currentCost){
                    $this->addError('total_price', 'Минимальная сумма заказа '.ProductHelper::getFormatPrice($minSumCart).' '.Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency));
                    return false;
                }
            }

            if($this->delivery && $this->delivery->deliveryCalc){
            	$addedAddressData = Yii::app()->getRequest()->getPost('deliveryCalc');
            	$addedAddress = $addedAddressData[$this->delivery->deliveryCalc->type][$this->delivery->deliveryCalc->value];
            	if($addedAddress){
		            $this->address = $addedAddress;
	            }
            }

	        if($this->validate()){
		        $fileInstance = CUploadedFile::getInstance($this, 'file');
		        if($fileInstance){
			        $path = Yii::getPathOfAlias('webroot').'/uploads/order_files/';
			        if(!is_dir($path)){
				        CFileHelper::createDirectory($path, null, true);
			        }

			        $name = $fileInstance->getName();
			        $name = \dosamigos\yii\helpers\TransliteratorHelper::process($name, '');
			        $name = str_replace(' ', '_', $name);
			        $name = date('Ymd_His_').$name;
			        if($fileInstance->saveAs($path.$name)){
				        $this->file = $name;
			        }
		        }

	            if (!$this->save()) {
	                return false;
	            }
            } else {
		        return false;
	        }

            Yii::app()->eventManager->fire(
                $isNew ? OrderEvents::CREATED : OrderEvents::UPDATED,
                new OrderEvent($this)
            );

            Yii::app()->getUser()->setState('uds-data', null);

            $transaction->commit();

            return true;
        } catch (Exception $e) {
            $transaction->rollback();

            return false;
        }
    }

    /**
     * @param array $coupons
     * @return bool
     */
    public function applyCoupons(array $coupons)
    {
        if (!$this->isCouponsAvailable()) {
            return true;
        }

        $coupons = $this->getValidCoupons($coupons);

        $transaction = Yii::app()->getDb()->beginTransaction();

        try {

            foreach ($coupons as $coupon) {

                $model = new OrderCoupon();

                $model->setAttributes(
                    [
                        'order_id' => $this->id,
                        'coupon_id' => $coupon->id,
                        'create_time' => new CDbExpression('NOW()'),
                    ]
                );

                $model->save();

                $coupon->decreaseQuantity();
            }

            $this->coupon_discount = $this->getCouponDiscount($coupons);

            $this->delivery_price = $this->getDeliveryCost();

            $this->update(['coupon_discount', 'delivery_price']);

            $transaction->commit();

            return true;
        } catch (Exception $e) {
            $transaction->rollback();

            return false;
        }
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        $this->total_price = $this->getProductsCost();

        if ($this->getIsNewRecord()) {
        	if($this->pickup_id && $this->delivery_id){
		        $delivery = Delivery::model()->findByPk($this->delivery_id);
		        if(!$delivery->pickup_group_id){
			        $this->pickup_id = 0;
		        } else {
		        	$this->address = '';
		        }
	        }

            $this->url = md5(uniqid(time(), true));
            $this->ip = Yii::app()->getRequest()->userHostAddress;
            if ($this->getScenario() === self::SCENARIO_USER) {
                $this->delivery_price = $this->delivery ? $this->delivery->getCost($this->total_price) : 0;
                $this->separate_delivery = $this->delivery ? $this->delivery->separate_payment : null;

                if(!$this->discount && !IS_BACKEND && !Yii::app()->getUser()->getIsGuest()){
					$userDiscount = Yii::app()->getUser()->getDiscount();

					$this->discount = $userDiscount;
				}
            }

            if(!$this->city && Yii::app()->hasComponent('geoLocateManager')){
            	$this->city = Yii::app()->getComponent('geoLocateManager')->getCurrentCity();
			}
        }

        $this->delivery_price = $this->getDeliveryCost();

	    if(!$this->isPaid()){
		    $this->payment_details = null;
	    }

        return parent::beforeSave();
    }

    /**
     *
     */
    public function afterDelete()
    {
        foreach ($this->products as $product) {
            $product->delete();
        }
        parent::afterDelete();
    }

    /**
     * @return array
     */
    public function getPaidStatusList()
    {
        return [
            self::PAID_STATUS_PAID => Yii::t("OrderModule.order", 'Paid'),
            self::PAID_STATUS_NOT_PAID => Yii::t("OrderModule.order", 'Not paid'),
        ];
    }

    /**
     * @return string
     */
    public function getPaidStatus()
    {
        $data = $this->getPaidStatusList();

        return isset($data[$this->paid]) ? $data[$this->paid] : Yii::t("OrderModule.order", '*unknown*');
    }

    /**
     *
     * Формат массива:
     * <pre>
     * array(
     *    '45' => array( //реальный id или сгенерированный новый, у новых внутри массива нет id
     *        'id' => '10', //если нет id, то новый
     *        'variant_ids' => array('10', '20, '30'), // массив с id вариантов
     *        'quantity' = > '5',
     *        'price' => '1000',
     *        'product_id' => '123',
     *    )
     * )
     * </pre>
     * @param $orderProducts Array
     */
    public function setProducts($orderProducts)
    {
        $this->productsChanged = true;
        $orderProductsObjectsArray = [];
        if (is_array($orderProducts)) {
            foreach ($orderProducts as $key => $op) {
                $product = null;
                if (isset($op['product_id'])) {
                    $product = Product::model()->findByPk($op['product_id']);
                }
                $variantIds = isset($op['variant_ids']) ? $op['variant_ids'] : [];
                if ($product) {
                    $this->hasProducts = true;
                }

                /* @var $orderProduct OrderProduct */
                $orderProduct = null;
                if (isset($op['id'])) {
                    $orderProduct = OrderProduct::model()->findByPk($op['id']);
                }

                if (!$orderProduct) {
                    $orderProduct = new OrderProduct();
                    $orderProduct->product_id = $product->id;
                    $orderProduct->product_name = $product->name;
                    $orderProduct->sku = $product->sku;
                }

                if ($product !== null && $this->getScenario() == self::SCENARIO_USER || $this->getScenario() == self::SCENARIO_BOC) {
                    $orderProduct->price = $product->getPrice($variantIds);
                } else {
                    $orderProduct->price = $op['price'];
                }
                if($variantIds[0]){
                    $variantItem = ProductVariant::model()->findByPk($variantIds[0]);
                    if($variantItem && $variantItem->sku){
                        $orderProduct->sku = $variantItem->sku;
                    }
                }

                $orderProduct->variantIds = $variantIds;
                $orderProduct->quantity = $op['quantity'];

                $orderProductsObjectsArray[] = $orderProduct;
            }
            $this->_orderProducts = $orderProductsObjectsArray;
        }
    }

    /**
     * Массив объектов OrderProduct
     * @param $products
     */
    private function updateOrderProducts($products)
    {
        if (!$this->productsChanged) {
            return;
        }

        $validOrderProductIds = [];

        foreach ($products as $var) {
            /* @var $var OrderProduct */
            if ($var->getIsNewRecord()) {
                $var->order_id = $this->id;
            }

            if ($var->save()) {
                $validOrderProductIds[] = $var->id;
            }
        }

        $criteria = new CDbCriteria();
        $criteria->addCondition('order_id = :order_id');
        $criteria->params = [':order_id' => $this->id];
        $criteria->addNotInCondition('id', $validOrderProductIds);
        OrderProduct::model()->deleteAll($criteria);
    }

    /**
     *
     */
    public function afterSave()
    {
        $this->updateOrderProducts($this->_orderProducts);

	    if(!$this->isNewRecord && !$this->custom_num){
	        $this->saveCustomName();
	    }
        parent::afterSave();
    }

	/**
	 *
	 */
    public function saveCustomName(){
    	if($this->id){
			$this->custom_num = $this->getCustomNum();
			$this->isNewRecord = false;
			$this->save(true, ['custom_num']);
		}
    }

	/**
	 * @return float
	 */
	public function getFullTotalPrice()
	{
		return (float)$this->total_price;
	}

    /**
     * @return float
     */
    public function getTotalPrice()
    {
    	$totalPrice = (float)$this->total_price - (float)$this->discount - (float)$this->coupon_discount;

    	if(Yii::app()->hasModule('uds')){
		    $totalPrice -= (float) $this->uds_discount;
	    }
        return $totalPrice;

    }

    /**
     * @return float
     */
    public function getDeliveryPrice()
    {
        return (float)$this->delivery_price;
    }

    /**
     * @return float
     */
    public function getTotalPriceWithDelivery()
    {
        $price = $this->getTotalPrice();

        if (!$this->separate_delivery) {
            $price += $this->getDeliveryPrice();
        }

        return $price;
    }

    /**
     * @return bool
     */
    public function isPaid()
    {
        return (int)$this->paid === static::PAID_STATUS_PAID;
    }

	/**
	 * @return bool
	 */
    public function isDiscount(){
	    return (float)$this->discount + (float)$this->coupon_discount > 0;
    }

	/**
	 * @return float
	 */
	public function getDiscountPrice(){
		$discount = (float)$this->discount + (float)$this->coupon_discount;

		if(Yii::app()->hasModule('uds')){
			$discount += (float) $this->uds_discount;
		}

		return $discount;
	}

    /**
     * @return mixed
     */
    public function isPaymentMethodSelected()
    {
        return $this->payment_method_id;
    }

    /**
     * @param Payment $payment
     * @param int $paid
     * @return bool
     */
    public function pay(Payment $payment, $paid = self::PAID_STATUS_PAID)
    {
        if ($this->isPaid()) {
            return true;
        }

        $attributes = [
	        'paid' => (int)$paid,
	        'payment_method_id' => $payment->id,
	        'payment_time' => new CDbExpression('NOW()'),
        ];
        if($this->status_id == OrderStatus::STATUS_WAIT_PAYMENT){
	        $this->status_id = OrderStatus::STATUS_WORK;
        }
        $this->setAttributes($attributes);

        $result = $this->save();

        if ($result) {
	        if(Yii::app()->hasModule('uds')){
		        if($this->uds_discount && $this->uds_code){
			        /**
			         * @var uds $uds
			         */
			        $uds = Yii::app()->getComponent('udsComponent');

			        $resultUds = $uds->purchase($this->getNumberForUser(), $this->uds_code, ($this->getTotalPriceWithDelivery() + $this->uds_discount), $this->uds_discount);
			        if($resultUds){
				        $this->uds_apply = 1;
				        $this->save();
			        }
		        }
	        }

            Yii::app()->eventManager->fire(OrderEvents::SUCCESS_PAID, new PayOrderEvent($this, $payment));
        } else {
            Yii::app()->eventManager->fire(OrderEvents::FAILURE_PAID, new PayOrderEvent($this, $payment));
        }

        return $result;
    }

    /**
     * @param $url
     * @return static
     */
    public function findByUrl($url)
    {
        return $this->findByAttributes(['url' => $url]);
    }

    /**
     * @param $num
     * @return static
     */
    public function findByNum($num)
    {
        return $this->findByAttributes(['custom_num' => $num]);
    }

    /**
     * @return bool
     */
    public function isStatusChanged()
    {
        if ($this->oldAttributes['status_id'] != $this->status_id) {

            Yii::app()->eventManager->fire(OrderEvents::STATUS_CHANGED, new OrderChangeStatusEvent($this));

            return true;
        }

        return false;
    }

    /**
     * @param $number
     * @return static
     */
    public function findByNumber($number)
    {
        return $this->findByPk($number);
    }

    /**
     * @return string
     */
    public function getAddress()
    {
    	if($this->address){
    		return $this->address;
	    }
        return sprintf(
            '%s %s %s %s',
            /*$this->country,
            $this->city,*/
            $this->street,
            $this->house,
            $this->apartment,
            $this->zipcode
        );
    }

    /**
     * @return CActiveDataProvider
     */
    public function getProducts()
    {
    	if($this->productDataProvider === null){
		    $this->productDataProvider = new CActiveDataProvider(
			    'OrderProduct', [
				    'criteria' => [
					    'condition' => 'order_id = :id',
					    'params' => [
						    ':id' => $this->id,
					    ],
				    ],
			    ]
		    );
	    }

	    return $this->productDataProvider;
    }

    /**
     * @return string
     */
    public function getStatusTitle()
    {
        return isset($this->status) ? $this->status->name : Yii::t('OrderModule.order', '*unknown*');
    }

    /**
     * @param IWebUser $user
     * @return bool
     */
    public function checkManager(IWebUser $user)
    {
        if (!$this->manager_id) {
            return true;
        }

        if (((int)$this->manager_id === (int)$user->getId()) || $user->isSuperUser()) {
            return true;
        }

        return false;
    }

	/**
	 * @return string
	 */
    public function getNumberForUser(){
		return $this->custom_num ?: $this->getCustomNum();
    }

	/**
	 * @return string
	 */
    protected function getCustomNum(){
	    $date = $this->date;
	    if($date instanceof CDbExpression){
		    $date = date('Y-m-d');
	    }
	    return date('ym', strtotime($date)).'-'.$this->id;
    }

	/**
	 * @return string
	 */
    public function getSourceName(){
	    if($this->source){
	        return $this->source->name;
        }
        return 'Прямая покупка';
    }

	/**
	 * @return int
	 */
    public function getLoyality(){
    	if($this->loyality === null){
		    $this->loyality = ClientLoyality::getLoyality($this->phone);
	    }
    	return $this->loyality;
    }

	/**
	 * @param $value
	 */
	public function setLoyality($value){
		$this->loyality = $value;
	}

	/**
	 * @return string
	 */
	public function getDeliveryInfo(){
		$delivery = $this->delivery->name;
		if($this->pickup_id){
			$delivery .= ', '.$this->pickup->name;
		}

		return $delivery;
	}

	/**
	 * @return string
	 */
	public function getStatusHtmlClass(){
		switch ($this->status_id){
			case OrderStatus::STATUS_DELETED:
				return 'status-cancel';

			case OrderStatus::STATUS_NEW:
				return 'status-new';

			case OrderStatus::STATUS_WORK:
			case OrderStatus::STATUS_WAIT_PAYMENT:
			case OrderStatus::STATUS_ACCEPTED:
				return 'status-wait';

			case OrderStatus::STATUS_FINISHED:
			case OrderStatus::STATUS_DELIVERY:
			case OrderStatus::STATUS_PICKUP:
				return 'status-done';
		}
	}
}
