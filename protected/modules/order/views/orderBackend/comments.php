<?php
/* @var $this CommentsWidget */
/* @var $form TbActiveForm */
/* @var $model Comment */
/* @var $spamField string */
/* @var $spamFieldValue string */
/* @var $redirectTo string */
/* @var $comments CActiveDataProvider */

Yii::app()->clientScript
    //->registerScriptFile(Yii::app()->getModule('comment')->getAssetsUrl().'/js/comments.js')
    ->registerScript(__FILE__, "$(document).ready(function(){
        $(document).on('focus', '#Comment_text', function() {
            $('#$spamField').val('$spamFieldValue');
        })
    });"
    );
?>

<div class="comments-widget" id="comments">
	<h2>
		<small>
			<?= Yii::t('CommentModule.comment', 'Comments').' '.$comments->getTotalItemCount(); ?>
		</small>
	</h2>

	<div class="comments-list">
		<?php if ($comments->getTotalItemCount() > 0) {
			$this->widget('webforma.widgets.WListView', [
				'dataProvider' => $comments,
				'itemView' => 'application.modules.order.views.orderBackend._comment',
				'template' => "{summary}\n{items}\n{pager}",
				'ajaxUpdate' => false,
				'summaryText' => '',
				'enableHistory' => true,
				'cssFile' => false,
				'itemsCssClass' => 'comment-items',
				'pagerCssClass' => 'catalog__pagination',
				'pager' => [
					'header' => '',
					'prevPageLabel' => '&laquo;',
					'nextPageLabel' => '&raquo;',
					'firstPageLabel' => false,
					'lastPageLabel' => false,
					'htmlOptions' => [
						'class' => 'pagination',
					],
				],
			]);
		};
		?>
	</div>

    <?php if ($this->showForm): ?>
        <?php if (!$this->isAllowed()): ?>
            <div class="alert alert-warning">
                <?= Yii::t('CommentModule.comment', 'Please, {login} or {register} for commenting!', [
					'{login}' => CHtml::link(Yii::t('CommentModule.comment', 'login'), ['/user/account/login']),
					'{register}' => CHtml::link(
						Yii::t('CommentModule.comment', 'register'),
						['/user/account/registration']
					),
				]); ?>
            </div>
        <?php else: ?>
            <div class="comment-form-wrap">
                <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',
                    [
                        'action' => Yii::app()->createUrl('/comment/add/'),
                        'id' => 'comment-form',
	                    'enableAjaxValidation' => false,
	                    'enableClientValidation' => false,
	                    'type' => 'vertical',
	                    'htmlOptions' => ['class' => 'well'],
	                    'clientOptions' => [
		                    'validateOnSubmit' => true,
	                    ],
                    ]
                ); ?>

                <?= $form->errorSummary($model); ?>
                <?= $form->hiddenField($model, 'is_admin', ['value' => 1]); ?>
                <?= $form->hiddenField($model, 'model'); ?>
                <?= $form->hiddenField($model, 'model_id'); ?>
                <?= $form->hiddenField($model, 'parent_id'); ?>
                <?= $form->hiddenField($model, 'is_backend', ['value' => true]); ?>
                <?= CHtml::hiddenField('redirectTo', $redirectTo); ?>
                <?= CHtml::hiddenField('view', 'order.views.orderBackend._comment'); ?>
                <?= CHtml::hiddenField('viewData', json_encode(['showForm' => 1])); ?>

                <?= $form->textField($model, 'spamField', [
                    'name' => $spamField,
                    'value' => $spamFieldValue,
                    'style' => 'position:absolute;display:none;visibility:hidden;',
                ]); ?>

                <?= $form->textField($model, 'comment', [
                    'style' => 'position:absolute;display:none;visibility:hidden;',
                ]); ?>

                <div class='row'>
                    <div class="col-sm-12">
                        <?= $form->textAreaGroup($model, 'text', [
							'widgetOptions' => [
								'htmlOptions' => [
									'style' => 'height:100px;',
								],
							],
						]); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <button class="btn btn-primary" id="add-comment" type="submit" name="add-comment">
                            <i class="glyphicon glyphicon-comment"></i>
                            <?= Yii::t('CommentModule.comment', 'Create comment'); ?>
                        </button>
                        <button class="btn btn-default" id="close-comment" type="button" style="display: none">
                            <?= Yii::t('CommentModule.comment', 'Отмена'); ?>
                        </button>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>
</div>
<script type="text/javascript">
	$('.delete-comment').on('click', function () {
		if(!confirm('Вы действительно хотите удалить комментарий?')){
			return false;
		}
		var _this = $(this),
			data = {};

		data[webformaTokenName] = webformaToken;
		$.ajax({
			url: _this.data('href'),
			method: 'post',
			data: data,
			success: function (data) {
				_this.closest('.comments-item').remove();
			}
		});
	});
</script>