<?php
/* @var $data Comment */
?>

<div class="comments-item" data-pid="<?= $data->parent_id; ?>" data-id="<?= $data->id; ?>">
    <div>
        <div class="comments-item-top">
            <div class="comments-item-author">
                <?= $data->author->getFullName(); ?>
                <span class='comments-item-date'>
                    <time datetime="<?= str_replace(' ', '_', $data->create_time); ?>">
                        <?= Yii::app()->getDateFormatter()->formatDateTime($data->create_time, 'long', 'short'); ?>
                    </time>
                </span>
            </div>
        </div>
        <div class="comments-item-message">
            <?= trim($data->getText()); ?>
			<div>
				<?php if ($showForm): ?>
					<?= CHtml::link(Yii::t('CommentModule.comment', 'reply'), '#', [
						'rel' => $data->id,
						'data-id' => $data->id . '_' . str_replace(' ', '_', $data->create_time),
						'class' => 'reply',
						'title' => Yii::t('CommentModule.comment', 'Reply')
					]); ?>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<?php endif; ?>

				<?= CHtml::link('удалить', '#delete', [
					'class' => 'delete-comment',
					'data-href'  => Yii::app()->createUrl('/backend/comment/comment/delete/'.$data->id),
				]); ?>
			</div>
        </div>
    </div>
</div>
