<?php
/* @var $model Order */
$this->breadcrumbs = [
    Yii::t('OrderModule.order', 'Orders') => ['/order/orderBackend/index'],
    Yii::t('OrderModule.order', 'Order #').$model->getNumberForUser(),
];

$this->pageTitle = Yii::t('OrderModule.order', 'Orders - view');

$this->menu = [
    [
        'label' => Yii::t('OrderModule.order', 'Orders'),
        'items' => [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('OrderModule.order', 'Manage orders'),
                'url' => ['/order/orderBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('OrderModule.order', 'Create order'),
                'url' => ['/order/orderBackend/create'],
            ],
            ['label' => Yii::t('OrderModule.order', 'Order #').' «'.$model->id.'»'],
            [
                'icon' => 'fa fa-fw fa-pencil',
                'label' => Yii::t('OrderModule.order', 'Update order'),
                'url' => [
                    '/order/orderBackend/update',
                    'id' => $model->id,
                ],
            ],
            [
                'icon' => 'fa fa-fw fa-eye',
                'label' => Yii::t('OrderModule.order', 'View order'),
                'url' => [
                    '/order/orderBackend/view',
                    'id' => $model->id,
                ],
            ],
            [
                'icon' => 'fa fa-fw fa-trash-o',
                'label' => Yii::t('OrderModule.order', 'Delete order'),
                'url' => '#',
                'linkOptions' => [
                    'submit' => ['/order/orderBackend/delete', 'id' => $model->id],
                    'confirm' => Yii::t('OrderModule.order', 'Do you really want to remove this order?'),
                    'params' => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
                    'csrf' => true,
                ],
            ],
        ],
    ],
    [
        'label' => Yii::t('OrderModule.order', 'Order statuses'),
        'items' => [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('OrderModule.order', 'Manage statuses'),
                'url' => ['/order/statusBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('OrderModule.order', 'Create status'),
                'url' => ['/order/statusBackend/create'],
            ],
        ],
    ],
];

$this->title = Yii::t('OrderModule.order', 'Viewing order').'<br><small>&laquo;'.Yii::t('OrderModule.order', '#').$model->getNumberForUser().'&raquo;</small>';


$module = Yii::app()->getModule('store');
Yii::app()->getClientScript()->registerCssFile($this->module->getAssetsUrl().'/css/order-print.css', 'print,screen');
$date = Yii::app()->getDateFormatter()->formatDateTime($model->date);
list($orderDate, $orderTime) = explode(',', $date);

$currency = Yii::t("StoreModule.store", $module->currency);
?>

<div class="row">
	<div class="panel-body p-a-4 b-b-1">
		<div class="box m-a-0">
			<div class="box-row valign-middle">

				<div class="box-cell col-xs-8">
					<div class="display-inline-block m-r-3 valign-middle">
						<div class="font-size-18 font-weight-bold line-height-1">ЗАКАЗ #<?=$model->getNumberForUser()?></div>
						<strong>Статус: </strong><?=$model->getStatusTitle();?>
					</div>
					<?if(($model->manager_id && $model->manager) || $model->note):?>
						<br/><br/>
					<?endif;?>

					<?if($model->manager_id && $model->manager):?>
						<div><strong>Менеджер:</strong> <?=$model->manager->getFullname()?></div>
					<?endif;?>
					<?if($model->note):?>
						<div><strong>Примечание:</strong> <?=$model->note?></div>
					<?endif;?>
				</div>

				<div class="box-cell col-xs-4 box-cell-va">
					<div class="pull-xs-right font-size-15">
						<div class="font-size-13 line-height-1">Дата</div>
						<strong><?=$orderDate;?><br/><?=$orderTime;?></strong>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel-body p-a-4 b-b-1">
		<div class="box m-a-0">
			<div class="box-row valign-middle">
				<div class="box-cell col-xs-6 font-size-14">
					<div><strong><?=$model->name?></strong></div>
					<div><strong><?=$model->phone?></strong></div>
					<div><strong><?=$model->email?></strong></div>
					<div><?=$model->getDeliveryInfo()?></div>
					<div><?=$model->getAddress();?></div>

					<?if($model->comment):?>
						<br/>
						<div><b>Комментарий:</b> <?=$model->comment;?></div>
					<?endif;?>
				</div>

				<div class="box-cell col-xs-6 p-y-2 box-cell-va">
					<div class="box-row">
						<div class="box">
							<div class="pull-xs-right font-size-24"><strong><?=Helper::getFormatPrice($model->getTotalPriceWithDelivery())?> <?=$currency?></strong></div>
							<div class="pull-xs-right m-y-1 p-r-2 font-size-12"><strong>К ОПЛАТЕ:</strong></div>
						</div>
						<div class="box">
							<div class="pull-xs-right">
								<?if($model->payment_method_id && $model->payment):?>
									<div><b>Способ оплаты:</b> <?=$model->payment->name;?></div>
								<?endif?>
								<div class="font-size-12" style="text-align: right;"><strong><?=$model->isPaid() ? 'ОПЛАЧЕН' : 'НЕ ОПЛАЧЕН';?></strong></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel-body p-a-4">
		<table class="table m-a-0">
			<thead>
			<tr>
				<th class="p-x-2">
					Наименование
				</th>
				<th class="p-x-2" style="width: 100px;">
					Цена
				</th>
				<th class="p-x-2" style="width: 80px;">
					Кол-во
				</th>
				<th class="p-x-2" style="width: 120px;">
					Стоимость
				</th>
			</tr>
			</thead>
			<tbody class="font-size-14">
			<?php foreach ((array)$model->products as $position): ?>
				<?
				$product = $position->product;
				?>
				<tr>
					<td class="p-a-2">
						<?if(is_null($product)):?>
							<div class="font-weight-semibold"><?= CHtml::encode($position->product_name); ?></div>
						<?else:?>
							<div class="font-weight-semibold"><?= CHtml::encode($position->getNameInEmail()); ?></div>
							<?php if ($product->getCategoryId()): ?>
								<div class="font-size-12"><?= $product->category->name ?></div>
							<?php endif; ?>
							<?
							$variantsData = [];
							if($position->variantsArray){
								$i = 0;
								foreach ($position->variantsArray as $variant){
									if($i == 0 && $variant['sku']){
										$variantsData[] = '<span class="cart-item__variant-item">Артикул: '.$variant['sku'].'</span>';
										$i++;
									}
									$variantsData[] = '<span class="cart-item__variant-item">'.$variant['attribute_title'].': '.$variant['optionValue'].'</span>';
								}
								$variants = $variantsData ? implode(', ', $variantsData) : '';
							} else{
								if($product->sku) {
									$variantsData[] = '<span class="cart-item__variant-item">Артикул: '.$product->sku.'</span>';
								}
								$variants = $this->renderPartial('_attributes', ['product' => $product, 'variantsData' => $variantsData], true);
							}
							?>
							<?if($variants):?>
								<div class="font-size-12"><?=$variants;?></div>
							<?endif;?>
						<?endif;?>
					</td>
					<td class="p-a-2">
						<strong><?= ProductHelper::getFormatPrice($position->price); ?><span class="ruble"> <?= $currency; ?></span></strong>
					</td>
					<td class="p-a-2">
						<strong><?= $position->quantity; ?></strong>
					</td>
					<td class="p-a-2">
						<strong><?= ProductHelper::getFormatPrice($position->getTotalPrice()); ?><span class="ruble"> <?= $currency ?></span></strong>
					</td>
				</tr>
			<?endforeach;?>

			<?php if ($model->hasCoupons()): ?>
				<?foreach ($model->getCoupons() as $coupon):?>
					<tr>
						<td class="p-a-2">
							<div class="font-weight-semibold"><?= $coupon->name; ?></div>
							<div class="font-size-12">Купон на скидку</div>
						</td>
						<td class="p-a-2"></td>
						<td class="p-a-2"></td>
						<td class="p-a-2">
							<strong>-<?=ProductHelper::getFormatPrice($coupon->value)?> <span class="ruble"> <?= CouponType::title($coupon->type) ?></span></strong>
						</td>
					</tr>
				<?endforeach;?>
			<?php endif; ?>

			<tr>
				<td class="p-a-2">
					<div class="font-weight-semibold"><strong>Товаров на сумму</strong></div>
				</td>
				<td class="p-a-2"></td>
				<td class="p-a-2"></td>
				<td class="p-a-2">
					<strong><?= ProductHelper::getFormatPrice($model->getFullTotalPrice()); ?><span class="ruble"> <?= $currency ?></span></strong>
				</td>
			</tr>

			<?if($model->isDiscount()):?>
				<tr>
					<td class="p-a-2">
						<div class="font-weight-semibold"><strong>Скидка</strong></div>
					</td>
					<td class="p-a-2"></td>
					<td class="p-a-2"></td>
					<td class="p-a-2">
						<strong><?= ProductHelper::getFormatPrice($model->getDiscountPrice()); ?><span class="ruble"> <?= $currency ?></span></strong>
					</td>
				</tr>
			<?endif;?>

			<?if($model->delivery):?>
				<tr>
					<td class="p-a-2">
						<div class="font-weight-semibold">
							<strong><?=$model->delivery->name?></strong>
							<?if($model->separate_delivery):?>
								(оплачивается отдельно)
							<?endif;?>
						</div>
						<?if($model->date_delivery):?>
							<div>
								<b>Дата доставки: </b><?=Yii::app()->getDateFormatter()->formatDateTime($model->date_delivery, 'medium', false)?>
							</div>
						<?endif;?>

						<?if($model->track_num):?>
							<div>
								<b>TN:</b> #<?=$model->track_num?>
							</div>
						<?endif;?>
					</td>
					<td class="p-a-2"></td>
					<td class="p-a-2"></td>
					<td class="p-a-2">
						<strong><?= ProductHelper::getFormatPrice($model->getDeliveryPrice());?><span class="ruble"> <?= $currency ?></span></strong>
					</td>
				</tr>
			<?endif;?>

			<?if($model->isDiscount() || $model->delivery):?>
				<tr>
					<td class="p-a-2">
						<div class="font-weight-semibold"><strong>Итого</strong></div>
					</td>
					<td class="p-a-2"></td>
					<td class="p-a-2"></td>
					<td class="p-a-2">
						<strong><?= ProductHelper::getFormatPrice($model->getTotalPriceWithDelivery()); ?><span class="ruble"> <?= $currency ?></span></strong>
					</td>
				</tr>
			<?endif;?>

			</tbody>
		</table>
	</div>
</div>


<style type="text/css">
	.box {border: none;}
	.box-cell-va {vertical-align: top;}
</style>
