<?
/**
 * @var Order $model
 * @var StoreModule $module
 */
$module = Yii::app()->getModule('store');
Yii::app()->getClientScript()->registerCssFile($this->module->getAssetsUrl().'/css/order-print.css', 'print,screen');
$date = Yii::app()->getDateFormatter()->formatDateTime($model->date);
list($orderDate, $orderTime) = explode(',', $date);

$currency = Yii::t("StoreModule.store", $module->currency);
?>

<div style="min-width: 768px;">
	<div class="panel-body p-a-4 b-b-1">
		<div class="box m-a-0">
			<div class="box-row valign-middle">

				<div class="box-cell col-xs-8">
					<div class="display-inline-block m-r-3 valign-middle">
						<strong><?=$module->getShopTitle()?></strong>
						<div class="font-size-18 font-weight-bold line-height-1">ЗАКАЗ #<?=$model->getNumberForUser()?></div>
					</div>

					<div class="display-inline-block p-l-1 b-l-1 valign-middle font-size-12">
						<?foreach (explode("\n", $module->shopRequisites) as $_item):?>
							<div><?=$_item?></div>
						<?endforeach;?>
					</div>
				</div>

				<div class="box-cell col-xs-4">
					<div class="pull-xs-right font-size-15">
						<div class="font-size-13 line-height-1">Дата</div>
						<strong><?=$orderDate;?><br/><?=$orderTime;?></strong>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel-body p-a-4 b-b-1">
		<div class="box m-a-0">
			<div class="box-row valign-middle">
				<div class="box-cell col-xs-6 font-size-14">
					<div><strong><?=$model->name?></strong></div>
					<div><strong><?=$model->phone?></strong></div>
					<div><?=$model->getDeliveryInfo()?></div>
					<div><?=$model->getAddress();?></div>
				</div>

				<div class="box-cell col-xs-6 p-y-2">
					<div class="pull-xs-right font-size-24"><strong><?=Helper::getFormatPrice($model->getTotalPriceWithDelivery())?> <?=$currency?></strong></div>
					<div class="pull-xs-right m-y-1 p-r-2 font-size-12"><strong>К ОПЛАТЕ:</strong></div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel-body p-a-4">
		<table class="table m-a-0">
			<thead>
			<tr>
				<th class="p-x-2">
					Наименование
				</th>
				<th class="p-x-2" style="width: 100px;">
					Цена
				</th>
				<th class="p-x-2" style="width: 80px;">
					Кол-во
				</th>
				<th class="p-x-2" style="width: 120px;">
					Стоимость
				</th>
			</tr>
			</thead>
			<tbody class="font-size-14">
			<?php foreach ((array)$model->products as $position): ?>
				<?
					$product = $position->product;
				?>
				<tr>
					<td class="p-a-2">
						<div class="font-weight-semibold"><?= CHtml::encode($position->getNameInEmail()); ?></div>
						<?
						$variantsData = [];
						if($position->variantsArray){
							$i = 0;
							foreach ($position->variantsArray as $variant){
								if($i == 0 && $variant['sku']){
									$variantsData[] = '<span class="cart-item__variant-item">Артикул: '.$variant['sku'].'</span>';
									$i++;
								}
								$variantsData[] = '<span class="cart-item__variant-item">'.$variant['attribute_title'].': '.$variant['optionValue'].'</span>';
							}
						} elseif($product->sku) {
							$variantsData[] = '<span class="cart-item__variant-item">Артикул: '.$product->sku.'</span>';
						}
						?>
						<?if($variantsData):?>
							<div class="font-size-12"><?=implode(', ', $variantsData);?></div>
						<?endif;?>
					</td>
					<td class="p-a-2">
						<strong><?= ProductHelper::getFormatPrice($position->price); ?><span class="ruble"> <?= $currency; ?></span></strong>
					</td>
					<td class="p-a-2">
						<strong><?= $position->quantity; ?></strong>
					</td>
					<td class="p-a-2">
						<strong><?= ProductHelper::getFormatPrice($position->getTotalPrice()); ?><span class="ruble"> <?= $currency ?></span></strong>
					</td>
				</tr>
			<?endforeach;?>

			<?php if ($model->hasCoupons()): ?>
				<?foreach ($model->getCoupons() as $coupon):?>
					<tr>
						<td class="p-a-2">
							<div class="font-weight-semibold"><?= $coupon->name; ?></div>
							<div class="font-size-12">Купон на скидку</div>
						</td>
						<td class="p-a-2"></td>
						<td class="p-a-2"></td>
						<td class="p-a-2">
							<strong>-<?=ProductHelper::getFormatPrice($coupon->value)?> <span class="ruble"> <?= CouponType::title($coupon->type) ?></span></strong>
						</td>
					</tr>
				<?endforeach;?>
			<?php endif; ?>

			<tr>
				<td class="p-a-2">
					<div class="font-weight-semibold"><strong>Товаров на сумму</strong></div>
				</td>
				<td class="p-a-2"></td>
				<td class="p-a-2"></td>
				<td class="p-a-2">
					<strong><?= ProductHelper::getFormatPrice($model->getFullTotalPrice()); ?><span class="ruble"> <?= $currency ?></span></strong>
				</td>
			</tr>

			<?if($model->isDiscount()):?>
				<tr>
					<td class="p-a-2">
						<div class="font-weight-semibold"><strong>Скидка</strong></div>
					</td>
					<td class="p-a-2"></td>
					<td class="p-a-2"></td>
					<td class="p-a-2">
						<strong><?= ProductHelper::getFormatPrice($model->getDiscountPrice()); ?><span class="ruble"> <?= $currency ?></span></strong>
					</td>
				</tr>
			<?endif;?>

			<?if($model->delivery):?>
				<tr>
					<td class="p-a-2">
						<div class="font-weight-semibold"><strong>Стоимость доставки</strong></div>
						<div class="font-size-12"><?=$model->delivery->name?></div>
					</td>
					<td class="p-a-2"></td>
					<td class="p-a-2"></td>
					<td class="p-a-2">
						<strong><?= ProductHelper::getFormatPrice($model->getDeliveryPrice());?><span class="ruble"> <?= $currency ?></span></strong>
					</td>
				</tr>
			<?endif;?>

			<?if($model->isDiscount() || $model->delivery):?>
				<tr>
					<td class="p-a-2">
						<div class="font-weight-semibold"><strong>Итого</strong></div>
					</td>
					<td class="p-a-2"></td>
					<td class="p-a-2"></td>
					<td class="p-a-2">
						<strong><?= ProductHelper::getFormatPrice($model->getTotalPriceWithDelivery()); ?><span class="ruble"> <?= $currency ?></span></strong>
					</td>
				</tr>
			<?endif;?>

			</tbody>
		</table>
	</div>
</div>
<script>
	window.onload = function () {
		window.print();
		window.close();
	};
</script>