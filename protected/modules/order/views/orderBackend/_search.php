<?php
$post = Yii::app()->getRequest()->getPost('Order');
$search = clone $model;
if(!isset($post['loyality'])){
	$search->setLoyality('');
}

Yii::app()->getClientScript()->registerScriptFile($this->module->getAssetsUrl() . '/js/order-backend.js');

$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'action'      => Yii::app()->createUrl($this->route),
        'method'      => 'get',
        'htmlOptions' => ['class' => 'well'],
    ]
); ?>
<fieldset class="inline">
    <div class="row">
		<div class="col-sm-3">
            <?=$form->textFieldGroup($search, 'name'); ?>
        </div>
		<div class="col-sm-3">
		    <?=$form->textFieldGroup($search, 'phone'); ?>
		</div>
		<div class="col-sm-3">
		    <?=$form->textFieldGroup($search, 'email'); ?>
		</div>
		<div class="col-sm-3">
		    <?= $form->dropDownListGroup($search, 'manager_id', [
			    'widgetOptions' => [
				    'data'        => CHtml::listData(User::model()->findAll('access_level = :level', [':level' => User::ACCESS_LEVEL_ADMIN]), 'id', 'fullName'),
				    'htmlOptions' => [
					    'class' => 'popover-help',
					    'empty' => '-- не выбран --',
				    ],
			    ],
		    ]); ?>
		</div>
    </div>

    <div class="row">
		<div class="col-sm-3">
		    <?= $form->dropDownListGroup($search, 'user_id', [
			    'widgetOptions' => [
				    'data'        => CHtml::listData(User::model()->findAll('status = :status', [':status' => User::STATUS_ACTIVE]), 'id', 'fullName'),
				    'htmlOptions' => [
					    'class' => 'popover-help',
					    'empty' => '-- не выбран --',
				    ],
			    ],
		    ]); ?>
		</div>
		<div class="col-sm-3">
			<?= $form->dropDownListGroup($search, 'is_user', [
				'widgetOptions' => [
					'data' => [
						0 => 'Нет',
						1 => 'Да',
					],
					'htmlOptions' => [
						'class' => 'popover-help',
						'empty' => '-- не выбран --',
					],
				],
			]); ?>
		</div>
		<div class="col-sm-3">
		    <?= $form->dropDownListGroup($search, 'loyality', [
			    'widgetOptions' => [
				    'data'        => ClientLoyality::loyalityList(),
				    'htmlOptions' => [
					    'class' => 'popover-help',
					    'empty' => '-- не выбран --',
				    ],
			    ],
		    ]); ?>
		</div>
		<div class="col-sm-3">
		    <?=$form->textFieldGroup($search, 'comment'); ?>
		</div>
	</div>
    <div class="row">
		<div class="col-sm-3">
			<div class="form-group">
				<?=CHtml::activeLabel($search, 'date', ['class' => 'control-label'])?>
				<?$this->widget('booster.widgets.TbDatePicker', [
					'model' => $search,
					'attribute' => 'date',
					'options' => [
						'format' => 'dd.mm.yyyy',
						'autoclose' => true,
					],
					'htmlOptions' => [
						'class' => 'form-control',
						'id' => 'search_date',
					],
				])?>
			</div>
		</div>

		<div class="col-sm-3">
		    <?= $form->dropDownListGroup($search, 'source_id', [
			    'widgetOptions' => [
				    'data' => Provider::model()->getFormattedList(),
				    'htmlOptions' => [
					    'empty' => Yii::t('StoreModule.store', '--choose--'),
					    'encode' => false,
				    ],
			    ],
		    ]) ?>
		</div>
		<div class="col-sm-3">
		    <?=$form->textFieldGroup($search, 'track_num'); ?>
		</div>
		<div class="col-sm-3">
		    <?= $form->dropDownListGroup($search, 'status_id', [
			    'widgetOptions' => [
				    'data'        => OrderHelper::statusList(),
				    'htmlOptions' => [
					    'class' => 'popover-help',
					    'empty' => '-- не выбран --'
				    ],
			    ],
		    ]); ?>
		</div>
    </div>

    <div class="row">
		<div class="col-sm-3">
			<?= $form->dropDownListGroup($search, 'paid', [
				'widgetOptions' => [
					'data' => $search->getPaidStatusList(),
					'htmlOptions' => [
						'class' => 'popover-help',
						'empty' => '-- не выбран --',
					],
				],
			]); ?>
		</div>
		<?if(Yii::app()->hasModule('payment')):?>
			<div class="col-sm-3">
				<?= $form->dropDownListGroup($search, 'payment_method_id', [
					'widgetOptions' => [
						'data'        => CHtml::listData(Payment::model()->findAll(), 'id', 'name'),
						'htmlOptions' => [
							'class' => 'popover-help',
							'empty' => '-- не выбран --'
						],
					],
				]); ?>
			</div>
		<?endif;?>
	    <?if(Yii::app()->hasModule('coupon')):?>
			<div class="col-sm-3">
				<?= $form->dropDownListGroup($search, 'couponId', [
					'widgetOptions' => [
						'data'        => CHtml::listData(Coupon::model()->findAll(), 'id', 'name'),
						'htmlOptions' => [
							'class' => 'popover-help',
							'empty' => '-- не выбран --'
						],
					],
				]); ?>
			</div>
		<?endif;?>
		<div class="col-sm-3">
		    <?=$form->textFieldGroup($search, 'note'); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-3">
			<?=$form->textFieldGroup($search, 'address'); ?>
		</div>
		<div class="col-sm-3">
			<?=$form->dropDownListGroup($model, 'delivery_id', [
				'widgetOptions' => [
					'data' => CHtml::listData(Delivery::model()->with('deliveryCalc')->findAll(), 'id', 'name'),
					'htmlOptions' => [
						'empty' => '-- не выбран --',
						'id' => 'delivery-type',
						'options' => Delivery::getOptionsForDropdown(),
					],
				],
			]); ?>
		</div>
		<div class="col-sm-3 wrapper <?=(!$search->pickup_id) ? ' hidden' : ''?>">
			<?= $form->dropDownListGroup($search, 'pickup_id', [
				'widgetOptions' => [
					'data' => CHtml::listData(Pickup::getPickupItemByGroup($search->delivery_id), 'id', 'name'),
					'htmlOptions' => [
						'empty' => '-- не выбран --'
					],
				],
			]); ?>
		</div>
	</div>
</fieldset>

<?php $this->widget('bootstrap.widgets.TbButton', [
	'buttonType'  => 'submit',
	'context'     => 'primary',
	'encodeLabel' => false,
	'label'       => '<i class="fa fa-search">&nbsp;</i> Найти заказы',
]); ?>

<?php $this->widget('bootstrap.widgets.TbButton', [
	'buttonType' => 'reset',
	'context'    => 'danger',
	'encodeLabel' => false,
	'label'       => '<i class="fa fa-times">&nbsp;</i> Очистить форму',
]); ?>
<br/>
<br/>

<?php $this->endWidget(); ?>

<script>
	$('#delivery-type').change(function () {
		changePickupItems("<?=Yii::app()->createUrl('/delivery/pickupBackend/getjsonitems/');?>", <?=(int) $search->pickup_id?>)
	});
</script>

