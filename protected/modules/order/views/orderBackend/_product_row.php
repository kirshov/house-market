<?php /* @var $model OrderProduct */
$new = false;
$product = $model->product;
$productExists = !is_null($product);
$variants = $productExists ? $product->getVariants() : [];
$hasVariants = $productExists && !empty($variants);

if (!$model->id) {
    $new = true;
    $model->id = 'new-' . rand(10000, 50000);
    $model->price = $product->getResultPrice();
    $model->quantity = 1;
}
$id = $model->id;
?>

<tr class="product-row">
    <td style="width: 100px;">
        <?php if (!$new): ?>
            <input type="hidden" name="OrderProduct[<?= $id; ?>][id]" value="<?= $id; ?>"/>
        <?php endif; ?>
        <?php if ($productExists): ?>
            <input type="hidden" class="product-base-price" value="<?= $product->getResultPrice(); ?>"/>
            <input type="hidden" name="OrderProduct[<?= $id; ?>][product_id]"
                   value="<?= $product->id; ?>"/>
            <span  class="img-thumbnail" style="display: inline-block; text-align: center; padding 0;width:48px; height: 48px;"><img src="<?= $product->getImageUrl(40, 40, false); ?>" alt=""/></span>
        <?php endif; ?>
    </td>
    <td <?=(!$hasVariants) ? 'colspan="2"' : '';?>>
        <?php if ($productExists): ?>
            <?= CHtml::link($model->product_name ?: $product->name, ['/store/productBackend/update', 'id' => $product->id]); ?>
            <br/>
			<?if($model->sku):?>
            	<span class="sku">Артикул: <?=$model->sku?></span>
			<?endif;?>
			[<?= $model->price; ?> <?= Yii::t("OrderModule.order", Yii::app()->getModule('store')->currency); ?>]
			<?if(!$hasVariants){
				$variants = $this->renderPartial('_attributes', ['product' => $product], true);
				echo  '<br/><small>'.$variants.'</small>';
	        }
	    	?>
        <?php else: ?>
            <?= $model->product_name; ?>
        <?php endif; ?>
    </td>
    <?php if ($hasVariants): ?>
		<td>
			<?php if ($productExists): ?>
				<?php
				$variantGroups = [];
				$variantGroupsSelected = [];
				$options = [];

				foreach ($variants as $variant) {
					$title = $variant->attribute->title;

					$variantGroups[$title][$variant->id] = $variant;
					$options[$variant->id] = ['data-type' => $variant->type, 'data-amount' => $variant->getResultPrice(), 'data-sku' => $variant->sku];

					/* выясняем какие из выбранных вариантов в покупке еще существуют, если эти варианты уже удалили, то позже их все равно добавим в список*/
					$varFound = false;
				}

				/* варианты, которых уже нет в базе */
				if($model->variantsArray){
					foreach ($model->variantsArray as $key => $var) {
						$title = $var['attribute_title'];

						if(isset($variantGroups[$title][$var['id']])){
							$variant = $variantGroups[$title][$var['id']];
							if($variant instanceof ProductVariant && $variant->getOptionValue() == $var['optionValue']){
								$variantGroupsSelected[$title] = $var['id'];
								continue;
							}
						}
						if(isset($variantGroups[$title][$var['id']])){
							$var['id'] = 'old_'.$var['id'];
						}
						$var['optionValue'] = '[Удален] ' . $var['optionValue'];
						$variantGroups[$title][] = $var;
						$variantGroupsSelected[$title] = $var['id'];
						$options[$var['id']] = ['data-type' => $var['type'], 'data-amount' => $var['amount'], 'data-sku' => $var['sku'], 'class' => 'muted',];
					}
				}
				?>
				<?php foreach ($variantGroups as $title => $variantGroup): ?>
					<div class="row" style="width: 180px;">
						<div class="col-sm-5" style="margin-top: 6px;">
							<?= $title; ?>
						</div>
						<div class="col-sm-7" style="padding-right: 0;">
							<?= CHtml::dropDownList(
								'OrderProduct[' . $id . '][variant_ids][]',
								isset($variantGroupsSelected[$title]) ? $variantGroupsSelected[$title] : null,
								CHtml::listData($variantGroup, 'id', 'optionValue'),
								['options' => $options, 'class' => 'form-control product-variant']
							); ?>
						</div>
					</div>
				<?php endforeach; ?>
			<?php else: ?>
				<p class="text-muted"><?= Yii::t("OrderModule.order", "Product deleted"); ?></p>
			<?php endif; ?>
		</td>
    <?php endif; ?>
    <td>
        <?= CHtml::activeTextField($model, 'quantity', ['class' => 'form-control product-quantity', 'name' => 'OrderProduct[' . $id . '][quantity]', 'style' => 'width: 60px;']); ?>
    </td>
    <td>
        <?= CHtml::activeTextField($model, 'price', ['class' => 'form-control product-price', 'name' => 'OrderProduct[' . $id . '][price]', 'style' => 'width: 80px;']); ?>
    </td>
    <td>
        <a href="#" class="btn btn-sm btn-danger remove-product"><i class="fa fa-fw fa-times"></i></a>
    </td>
</tr>