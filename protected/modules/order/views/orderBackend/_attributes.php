<?php
if(!isset($variantsData)){
	$variantsData = [];
}
foreach ($product->getAttributeGroups() as $groupName => $items){
	foreach ($items as $attribute){
		$value = AttributeRender::renderValue($attribute, $product->attribute($attribute),true, '{item}');
		if(!$attribute->is_general || !$attribute->title || !$value){
			continue;
		}

		$variantsData[] = '<span class="cart-item__variant-item">'.$attribute->title.': '.AttributeRender::formatSting($value).'</span>';
	}
}
if($variantsData){
	echo implode(', ', $variantsData);
}