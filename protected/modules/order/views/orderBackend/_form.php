<?php
/**
 * @var $model Order
 * @var $form TbActiveForm
 */

Yii::app()->getClientScript()->registerCssFile($this->module->getAssetsUrl().'/css/order-backend.css');
Yii::app()->getClientScript()->registerScriptFile($this->module->getAssetsUrl() . '/js/order-backend.js');
$mainAssets = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.webforma.views.assets'));
Yii::app()->getClientScript()->registerScriptFile($mainAssets.'/js/jquery.mask.min.js');

$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'id' => 'order-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'htmlOptions' => ['class' => 'well'],
    ]
);
?>

<div class="alert alert-info hidden">
    <?= Yii::t('OrderModule.order', 'Fields with'); ?>
    <span class="required">*</span>
    <?= Yii::t('OrderModule.order', 'are required'); ?>
</div>

<?= $form->errorSummary($model); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-3">
                <?= $form->labelEx($model, 'status_id'); ?>
                <?php $this->widget(
                    'bootstrap.widgets.TbSelect2',
                    [
                        'model' => $model,
                        'attribute' => 'status_id',
                        'data' => OrderHelper::statusList(),
                        'options' => [
                            'placeholder' => Yii::t('OrderModule.order', 'Status'),
                            'width' => '100%'
                        ],
                    ]
                ); ?>
            </div>
				<div class="col-sm-3">
					<?=
					$form->dateTimePickerGroup(
						$model,
						'date',
						[
							'widgetOptions' => [
								'options' => [
									'format' => 'dd.mm.yyyy hh:ii',
									'weekStart' => 1,
									'autoclose' => true,

								],
								'htmlOptions' => [
                                    'id' => 'orderDate',
                                    'value' => $model->date && $model->date != '0000-00-00 00:00:00' ? date('d.m.Y H:i', strtotime($model->date)) : '',
                                ],
							],
							'prepend' => '<i class="fa fa-calendar"></i>',
						]
					);
					?>
				</div>
            <div class="col-sm-3">
                <?= $form->labelEx($model, 'user_id'); ?>
                <?php $this->widget(
                    'bootstrap.widgets.TbSelect2',
                    [
                        'model' => $model,
                        'attribute' => 'user_id',
                        'asDropDownList' => false,
                        'options' => [
                            'minimumInputLength' => 2,
                            'placeholder' => Yii::t('OrderModule.order', 'Select client'),
                            'width' => '100%',
                            'allowClear' => true,
                            'ajax' => [
                                'url' => Yii::app()->getController()->createUrl(
                                    '/order/orderBackend/ajaxClientSearch'
                                ),
                                'dataType' => 'json',
                                'data' => 'js:function(term, page) { return {q: term }; }',
                                'results' => 'js:function(data) { return {results: data}; }',
                            ],
                            'formatResult' => 'js:productFormatResult',
                            'formatSelection' => 'js:productFormatSelection',
                            'initSelection' => $model->client ?
                                'js:function(element,callback){callback({name:"'.$model->client->getFullName().'"})}'
                                : false,
                        ],
                        'htmlOptions' => [
                            'id' => 'client-select',
                        ],
                    ]
                ); ?>
            </div>
            <div class="col-sm-3">
                <?= $form->labelEx($model, 'manager_id'); ?>
                <?php $this->widget('bootstrap.widgets.TbSelect2', [
					'model' => $model,
					'attribute' => 'manager_id',
					'data' => CHtml::listData(User::model()->findAll('access_level IN ('.implode(', ', [User::ACCESS_LEVEL_ADMIN, User::ACCESS_LEVEL_EDITOR]).')'), 'id', 'fullName'),
					'options' => [
						'placeholder' => Yii::t('OrderModule.order', 'Select manager'),
						'width' => '100%'
					],
				]); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="panel-title"><?= Yii::t('OrderModule.order', 'Products'); ?></span>
                    </div>
                    <div class="panel-body">
                        <table id="products" class="table table-hover table-responsive">
                            <tr>
                                <th><?= Yii::t('OrderModule.order', 'Image');?></th>
                                <th><?= Yii::t('OrderModule.order', 'Name');?></th>
                                <th><?= Yii::t('OrderModule.order', 'Variants');?></th>
                                <th>Кол-во</th>
                                <th><?= Yii::t('OrderModule.order', 'Price');?></th>
                                <th></th>
                            </tr>
                            <?php $totalProductCost = 0; ?>
                            <?php foreach ((array)$model->products as $orderProduct): ?>
                                <?php $totalProductCost += $orderProduct->price * $orderProduct->quantity; ?>
                                <?php $this->renderPartial('_product_row', ['model' => $orderProduct]); ?>
                            <?php endforeach; ?>
                        </table>

                        <div class="row" id="orderAddProduct">
                            <div class="col-sm-10">
                                <?php $this->widget(
                                    'bootstrap.widgets.TbSelect2',
                                    [
                                        'name' => 'ProductSelect',
                                        'asDropDownList' => false,
                                        'options' => [
                                            'minimumInputLength' => 2,
                                            'placeholder' => Yii::t('OrderModule.order', 'Select product'),
                                            'width' => '100%',
                                            'allowClear' => true,
                                            'ajax' => [
                                                'url' => Yii::app()->getController()->createUrl(
                                                    '/order/orderBackend/ajaxProductSearch'
                                                ),
                                                'dataType' => 'json',
                                                'data' => 'js:function(term, page) { return {q: term }; }',
                                                'results' => 'js:function(data) { return {results: data}; }',
                                            ],
                                            'formatResult' => 'js:productFormatResult',
                                            'formatSelection' => 'js:productFormatSelection',
                                        ],
                                        'htmlOptions' => [
                                            'id' => 'product-select',
                                        ],
                                    ]
                                ); ?>
                            </div>
                            <div class="col-sm-2">
                                <a class="btn btn-primary btn-sm" href="#" id="add-product"><?= Yii::t(
                                        'OrderModule.order',
                                        'Add'
                                    ); ?></a>
                            </div>
                        </div>
                        <div class="text-right">
							<?if($model->isDiscount()):?>
								<h4>
									Товаров на сумму:
									<span id="total-product-cost"><?= $totalProductCost; ?></span>
									<?= Yii::t('OrderModule.order', Yii::app()->getModule('store')->currency); ?>
								</h4>
								<h4>
									Скидка:
									<span id="total-product-cost"><?= $model->getDiscountPrice() ?></span>
									<?= Yii::t('OrderModule.order', Yii::app()->getModule('store')->currency); ?>
								</h4>
							<?endif;?>
                            <h4>
                                Итого:
                                <span id="total-product-cost"><?= $totalProductCost - $model->getDiscountPrice(); ?></span>
                                <?= Yii::t('OrderModule.order', Yii::app()->getModule('store')->currency); ?>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="panel-title"><?= Yii::t('OrderModule.order', 'Delivery'); ?></span>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <?=$form->dropDownListGroup($model, 'delivery_id', [
									'widgetOptions' => [
										'data' => CHtml::listData(Delivery::model()->findAll(['order' => 'position']), 'id', 'name'),
										'htmlOptions' => [
											'empty' => Yii::t('OrderModule.order', 'Not selected'),
											'id' => 'delivery-type',
											'options' => Delivery::getOptionsForDropdown(),
										],
									],
								]); ?>
                            </div>
                            <div class="col-sm-6 wrapper pickup-area<?=($model->pickup_id) ? '' : ' hidden'?>">
                                <?=
                                $form->dropDownListGroup($model, 'pickup_id', [
                                        'widgetOptions' => [
                                            'data' => CHtml::listData(
                                                Pickup::getPickupItemByGroup($model->delivery_id),
                                                'id',
                                                'name'
                                            ),
                                            'htmlOptions' => [
                                                'empty' => Yii::t('OrderModule.order', 'Not selected'),
                                            ],
                                        ],
                                    ]
                                ); ?>
                            </div>
						</div>
						<div class="row">
                            <div class="col-sm-6">
                                <?= $form->textFieldGroup($model, 'delivery_price'); ?>
                            </div>
                            <div class="col-sm-6">
                                <br/>
                                <?= $form->checkBoxGroup($model, 'separate_delivery'); ?>
                            </div>
                        </div>
	                    <?if($model->city):?>
							<div class="row">
								<div class="col-sm-12">
				                    <?= $form->textFieldGroup($model, 'city'); ?>
								</div>
							</div>
	                    <?endif;?>
                        <div class="row">
                            <div class="col-sm-12">
                                <?= $form->textAreaGroup($model, 'address', [
	                                'widgetOptions' => [
		                                'htmlOptions' => [
			                                'style' => 'height:100px;',
		                                ],
	                                ],
								]); ?>
                            </div>
                        </div>
						<div class="row">
							<div class="col-sm-12">
			                    <?= $form->textFieldGroup($model, 'track_num'); ?>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<?=
								$form->datePickerGroup(
									$model,
									'date_delivery',
									[
										'widgetOptions' => [
											'options' => [
												'format' => 'dd.mm.yyyy',
												'weekStart' => 1,
												'autoclose' => true,

											],
											'htmlOptions' => ['id' => 'orderDeliveryDate'],
										],
										'prepend' => '<i class="fa fa-calendar"></i>',
									]
								);
								?>
							</div>
						</div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="panel-title"><?= Yii::t('OrderModule.order', 'Payment') ?></span>
                    </div>
                    <div class="panel-body">
                        <div class="row">
							<?if(Yii::app()->hasModule('payment')):?>
								<div class="col-sm-4">
									<?=
									$form->dropDownListGroup(
										$model,
										'payment_method_id',
										[
											'widgetOptions' => [
												'data' => CHtml::listData(
													Payment::model()->published()->findAll(),
													'id',
													'name'
												),
												'htmlOptions' => [
													'empty' => Yii::t('OrderModule.order', 'Not selected'),
												],
											],
										]
									); ?>
								</div>
							<?endif;?>
                            <div class="col-sm-2">
                                <br/>
                                <?= $form->checkBoxGroup($model, 'paid'); ?>
                            </div>
                            <div class="col-sm-6 text-right">
                                <br/>
                                <h4>
                                    <?= Yii::t('OrderModule.order', 'Total'); ?>
                                    : <?= $model->getTotalPriceWithDelivery(); ?> <?= Yii::t(
                                        'OrderModule.order',
                                        Yii::app()->getModule('store')->currency
                                    ); ?>
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
	            <?php if (Yii::app()->hasModule('coupon') && $model->hasCoupons()): ?>
					<div class="panel panel-default">
						<div class="panel-heading">
							<span class="panel-title"><?= Yii::t('OrderModule.order', 'Coupons'); ?></span>
						</div>
						<?php if ($model->hasCoupons()): ?>
							<div class="panel-body coupons">
								<?php
								$this->widget(
									'webforma\widgets\CustomGridView',
									[
										'id' => 'order-coupon-grid',
										'type' => 'condensed',
										'dataProvider' => $model->searchCoupons(),
										'actionsButtons' => false,
										'bulkActions' => [false],
										'template' => '{items}',
										'columns' => [
											[
												'name' => 'coupon_id',
												'value' => function ($data) {
													return CHtml::link($data->coupon->code, ['/coupon/couponBackend/update', 'id' => $data->coupon_id]);
												},
												'type' => 'html',
											],
											'name' => 'create_time',
										],
									]
								);
								?>
							</div>
						<?php endif; ?>
					</div>
	            <?php endif; ?>
            </div>
            <?php if (!$model->getIsNewRecord() && isset($model->client)): ?>
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="panel-title"><?= Yii::t('OrderModule.order', 'Client'); ?></span>
                        </div>
                        <div class="panel-body">
                            <?php $this->widget('bootstrap.widgets.TbDetailView', [
								'data' => $model->client,
								'attributes' => [
									[
										'name' => 'full_name',
										'value' => CHtml::link(
											$model->client->getFullName(),
											['/order/clientBackend/view', 'id' => $model->client->id]
										),
										'type' => 'html',
									],
									//'nick_name',
									[
										'name' => 'email',
										'value' => CHtml::mailto($model->client->email, $model->client->email),
										'type' => 'html',
									],
									'phone',
									[
										'label' => Yii::t('OrderModule.order', 'Orders'),
										'value' => CHtml::link(
											$model->client->getOrderNumber(),
											['/order/orderBackend/index', 'Order[user_id]' => $model->client->id]
										),
										'type' => 'html',
									],
									[
										'label' => Yii::t('OrderModule.order', 'Money'),
										'value' => '<span  class="label label-success">'.Yii::app()->getNumberFormatter()->formatCurrency(
											$model->client->getOrderSum(),
											Yii::app()->getModule('store')->currency
										).'</span>',
										'type' => 'html'
									],
								],
							]); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="panel-title"><?= Yii::t('OrderModule.order', 'Order details'); ?></span>
                    </div>
                    <div class="panel-body">
						<div class="row">
							<div class="col-sm-12">
								<?= $form->dropDownListGroup($model, 'source_id', [
									'widgetOptions' => [
										'data'        => Source::getList(),
										'htmlOptions' => [
											'empty'               => Yii::t('OrderModule.order', '--выберите--'),
											'encode'              => false
										],
									],
								]); ?>
							</div>
						</div>

                        <div class="row">
                            <div class="col-sm-12">
                                <?= $form->textFieldGroup($model, 'name'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <?= $form->textFieldGroup($model, 'phone', ['widgetOptions' => ['htmlOptions' => ['class' => 'input-phone']]]); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <?= $form->textFieldGroup($model, 'email'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <?= $form->textAreaGroup($model, 'comment', [
	                                'widgetOptions' => [
		                                'htmlOptions' => [
											'style' => 'height:100px;',
		                                ],
	                                ],
								]); ?>
                            </div>
                        </div>
                        <?php if (!$model->getIsNewRecord()): ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= CHtml::link('Посмотреть заказ на сайте', ['/order/order/view', 'url' => $model->url], ['target' => '_blank']); ?>
                                </div>
                            </div>
                        <?php endif; ?>

	                    <?if($model->file){
		                    echo '<div class="row"><div class="col-sm-12">';
		                    $url = Yii::app()->createAbsoluteUrl('/').'uploads/order_files/'.$model->file;
		                    echo '<br/>К заказу был приложен файл: <a href="'.$url.'" target="_blank">Скачать</a>';
		                    echo '</div></div>';
	                    }?>
						
						<?if(Yii::app()->hasModule('sprofile')){
							$this->widget('application.modules.sprofile.widgets.SprofileAdminWidget', [
								'service' => 'order',
								'modelId' => $model->id,
							]);
						}?>
                    </div>
                </div>

				<div class="panel panel-default">
					<div class="panel-heading">
						<span class="panel-title"><?= Yii::t('OrderModule.order', 'Note') ?></span>
					</div>
					<div class="panel-body">
			            <?= $form->textAreaGroup($model, 'note', [
							'label' => false,
				            'widgetOptions' => [
					            'htmlOptions' => [
						            'style' => 'height:100px;',
					            ],
				            ],
						]); ?>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <label class="checkbox">
            <input class="" name="notify_user" value="1" type="checkbox"><?= Yii::t('OrderModule.order','Inform buyer about order status'); ?>
        </label>
		<div class="help-block small">Пользователю будет отправлено письмо о статусе заказа</div>
    </div>
</div>

<div class="buttons">
	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context' => 'primary',
		'label' => $model->getIsNewRecord()
			? Yii::t('OrderModule.order', 'Add order and continue')
			: Yii::t('OrderModule.order', 'Save order and continue'),
	]); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label' => $model->getIsNewRecord()
			? Yii::t('OrderModule.order', 'Add order and close')
			: Yii::t('OrderModule.order', 'Save order and close'),
	]); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'link',
		'context' => 'success',
		'htmlOptions' => ['value' => 'print', 'target' => '_blank'],
		'url' => Yii::app()->getController()->createUrl('/order/orderBackend/print', ['id' => $model->id]),
		'encodeLabel' => false,
		'label' => '<i class="fa fa-print"></i> Распечатать заказ',
	]); ?>

	<?php
	if($model->status_id == OrderStatus::STATUS_WAIT_PAYMENT && !$model->isPaid() && $model->email){
		$this->widget('bootstrap.widgets.TbButton', [
			'buttonType' => 'link',
			'context' => 'warning',
			'url' => Yii::app()->getController()->createUrl('/order/orderBackend/remind', ['id' => $model->id]),
			'label' => 'Напомнить об ожидании оплаты',
		]);
	} ?>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    function updateTotalProductCost() {
        var cost = 0;
        $.each($('#products').find('.product-row'), function (index, elem) {
            var quantity = parseInt($(elem).find('.product-quantity').val());
            var price = parseFloat($(elem).find('.product-price').val());
            cost += quantity * price;
        });
        $('#total-product-cost').html(parseFloat(cost.toFixed(2)));
    }
    function updatePrice($productRowElement) {
        var basePrice = parseFloat($productRowElement.find('.product-base-price').val());
        var _basePrice = basePrice;
        var variants = [];
        var hasBasePriceVariant = false;
        $.each($productRowElement.find('.product-variant'), function (index, elem) {
            var varId = elem.value;
            if (varId) {
                var option = $(elem).find('option[value="' + varId + '"]');
                var variant = {amount: option.data('amount'), type: option.data('type')};
                switch (variant.type) {
                    case 2: // base price
                        // еще не было варианта
                        if (!hasBasePriceVariant) {
                            _basePrice = variant.amount;
                            hasBasePriceVariant = true;
                        }
                        else {
                            if (_basePrice < variant.amount) {
                                _basePrice = variant.amount;
                            }
                        }
                        break;
                }
            }
        });
        var newPrice = _basePrice;
        $.each($productRowElement.find('.product-variant'), function (index, elem) {
            var varId = elem.value;
            if (varId) {
                var option = $(elem).find('option[value="' + varId + '"]');
                var variant = {amount: option.data('amount'), type: option.data('type')};
                variants.push(variant);
                switch (variant.type) {
                    case 0: // sum
                        newPrice += variant.amount;
                        break;
                    case 1: // percent
                        newPrice += _basePrice * ( variant.amount / 100);
                        break;
                }
            }
        });

        $productRowElement.find('.product-price').val(parseFloat(newPrice.toFixed(2)));
        updateTotalProductCost();
    }

    function getShippingCost() {
        var cartTotalCost = parseFloat($('#total-product-cost').text());
        var selectedDeliveryType = $('#delivery-type').find(':selected');
        if (!selectedDeliveryType.val()) {
            return 0;
        }
        if (parseFloat(selectedDeliveryType.data('free-from')) < cartTotalCost) {
            return 0;
        } else {
            return parseFloat(selectedDeliveryType.data('price'));
        }
    }

    function updateShippingCost() {
        $('#delivery-price').val(getShippingCost());
    }

    $(document).ready(function () {
        $('#add-product').click(function (e) {
            e.preventDefault();
            var productId = $("#product-select").select2("val");
            if (productId) {
                $.ajax({
                    url: '<?= Yii::app()->getController()->createUrl('/order/orderBackend/productRow')?>',
                    type: 'get',
                    data: {
                        'OrderProduct[product_id]': productId
                    },
                    success: function (data) {
                        $('#products').append(data);
                        updateTotalProductCost();
                    }
                });
            }
        });

        $('#products').on('click', '.remove-product', function (e) {
            e.preventDefault();
            $(this).parents('.product-row').remove();
            updateTotalProductCost();
        });

        $('#products').on('change', '.product-variant', function () {
            updatePrice($(this).parents('.product-row'));

            $(this).parents('.product-row').find('span.sku').html($(this).find('option:selected').data('sku'));
        });

		$('#delivery-type').change(function () {
			changePickupItems("<?=Yii::app()->createUrl('/delivery/pickupBackend/getjsonitems/');?>", <?=(int) $model->pickup_id?>)
		});
    });

    function productFormatResult(product) {
        var markup = "<table class='result'><tr>";
        if (product.thumb !== undefined) {
            markup += "<td width='30px'><img src='" + product.thumb + "' class=''/></td>";
        }
        markup += "<td class='info'><div class='title'>" + product.name + "</div>";
        markup += "</td></tr></table>";
        return markup;
    }

    function productFormatSelection(product) {
        return product.name;
    }

</script>
