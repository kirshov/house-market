<?php
Yii::app()->getClientScript()->registerCssFile($this->module->getAssetsUrl().'/css/order-backend.css');

$this->breadcrumbs = [
    Yii::t('OrderModule.order', 'Orders') => ['/order/orderBackend/index'],
    Yii::t('OrderModule.order', 'Manage'),
];

$this->pageTitle = Yii::t('OrderModule.order', 'Orders - manage');

/*$this->menu = [
    [
        'label' => Yii::t('OrderModule.order', 'Orders'),
        'items' => [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('OrderModule.order', 'Manage orders'),
                'url' => ['/order/orderBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('OrderModule.order', 'Create order'),
                'url' => ['/order/orderBackend/create'],
            ],
        ],
    ],
    [
        'label' => Yii::t('OrderModule.order', 'Order statuses'),
        'items' => [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('OrderModule.order', 'Manage statuses'),
                'url' => ['/order/statusBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('OrderModule.order', 'Create status'),
                'url' => ['/order/statusBackend/create'],
            ],
        ],
    ],
];*/

$this->title = Yii::t('OrderModule.order', 'Orders').'<small>'.Yii::t('OrderModule.order', 'manage').'</small>';
?>
<p class="search-wrap">
	<a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
		<i class="fa fa-search">&nbsp;</i>
		Поиск заказов
		<span class="caret">&nbsp;</span>
	</a>
</p>

<div id="search-toggle" class="collapse out search-form">
	<?php
	Yii::app()->clientScript->registerScript('search', "$('.search-form form').submit(function () { $.fn.yiiGridView.update('order-grid', {data: $(this).serialize()});return false;});");
	$this->renderPartial('_search', ['model' => $model]);
	?>
</div>
<?php
$this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'order-grid',
        'type' => 'condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'datePickers' => ['Order_date'],
        'afterAjaxUpdate' => 'reinstallDatePicker',
        'columns' => [
            [
                'name' => 'id',
                'htmlOptions' => ['width' => '90px'],
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::link($data->getNumberForUser(), ["/order/orderBackend/update", "id" => $data->id]);
                },
            ],
            [
                'name' => 'date',
                'type' => 'html',
                'filter' => $this->widget('booster.widgets.TbDatePicker', [
                    'model' => $model,
                    'attribute' => 'date',
                    'options' => [
                        'format' => 'dd.mm.yyyy',
	                    'autoclose' => true,
                    ],
                    'htmlOptions' => [
                        'class' => 'form-control',
                    ],
                ], true),
                'value' => function ($data) {
                    return CHtml::link(date('d.m.Y H:i:s', strtotime($data->date)), ["/order/orderBackend/update", "id" => $data->id]);
                },
            ],
            [
                'name' => 'name',
                'type' => 'raw',
                'value' => function ($data) {
                    return isset($data->client)
						? CHtml::link(CHtml::encode($data->client->getFullName()), ['/order/clientBackend/view', 'id' => $data->user_id])
						: CHtml::encode($data->name);
                },
                'htmlOptions' => ['width' => '150px'],
            ],
	        [
		        'name' => 'phone',
		        'type' => 'raw',
		        'value' => function ($data) {
			        return isset($data->client)
				        ? CHtml::link($data->phone,['/order/clientBackend/view', 'id' => $data->user_id])
				        : CHtml::encode($data->phone);
		        },
		        'htmlOptions' => ['width' => '150px'],
	        ],
            [
                'name' => 'total_price',
                'value' => function ($data) {
                    return Yii::app()->getNumberFormatter()->formatCurrency($data->getTotalPriceWithDelivery(), Yii::app()->getModule('store')->currency);
                },
            ],
            [
                'class' => 'webforma\widgets\EditableStatusColumn',
                'name' => 'status_id',
                'url' => $this->createUrl('/order/orderBackend/inline'),
                'source' => OrderHelper::statusList(),
                'options' => OrderHelper::labelList(),
	            'htmlOptions' => ['width' => '120px'],
				'editable' => [
					'mode'   => 'popup',
				],
            ],
            [
                'class' => 'webforma\widgets\EditableStatusColumn',
                'name' => 'paid',
                'url' => $this->createUrl('/order/orderBackend/inline'),
                'source' => $model->getPaidStatusList(),
                'options' => [
                    Order::PAID_STATUS_NOT_PAID => ['class' => 'label-danger'],
                    Order::PAID_STATUS_PAID => ['class' => 'label-success'],
                ],
	            'editable' => [
		            'mode'   => 'popup',
	            ],
            ],
            [
                'name' => 'delivery_id',
                'header' => Yii::t('OrderModule.order', 'Delivery'),
                'filter' => CHtml::listData(Delivery::model()->findAll(), 'id', 'name'),
                'value' => function (Order $data) {
                    $delivery = $data->delivery->name;

                    if($data->pickup_id && $data->pickup){
	                    $delivery .= ', '.$data->pickup->name;
                    }

                    return $delivery;
                },
            ],
			[
				'header' => 'Заказы',
				'value' => function ($data) {
    				if($data->client) {
						return CHtml::link($data->cntOrders, ['/order/clientBackend/view', 'id' => $data->user_id], ['class' => 'badge']);
					} elseif ($data->phone){
						return CHtml::link($data->cntOrders, ['/order/clientBackend/viewGuest', 'phone' => base64_encode($data->phone)], ['class' => 'badge']);
					}
    				return $data->cntOrders;
				},
				'type' => 'raw',
				'htmlOptions' => [
					'style' => 'text-align:center;',
				]
			],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
	            'frontViewButtonUrl' => function($data){
		            return ['/order/order/view', 'url' => $data->url];
	            },
	            'template' => '{front_view} {view} {update} {delete}',
                'buttons' => [
	                'front_view' => [
		                'visible' => function ($row, $data) {
			                return true;
		                }
	                ],
	                'view' => [
		                'visible' => function ($row, $data) {
			                return true;
		                }
	                ],
                    'update' => [
                        'visible' => function ($row, Order $order) {
                            return $order->checkManager(Yii::app()->getUser());
                        },
                    ],
                    'delete' => [
                        'visible' => function ($row, Order $order) {
                            return $order->checkManager(Yii::app()->getUser());
                        },
                    ],
                ],
            ],
        ],
    ]
); ?>
