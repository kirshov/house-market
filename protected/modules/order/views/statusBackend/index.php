<?php
Yii::app()->getClientScript()->registerCssFile($this->module->getAssetsUrl() . '/css/order-backend.css');

$this->breadcrumbs = [
    Yii::t('OrderModule.order', 'Orders') => ['/order/orderBackend/index'],
    Yii::t('OrderModule.order', 'Order statuses')
];

$this->pageTitle = Yii::t('OrderModule.order', 'Order statuses - manage');

$this->menu = [
    [
        'label' => Yii::t('OrderModule.order', 'Orders'),
        'items' => [
            ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OrderModule.order', 'Manage orders'), 'url' => ['/order/orderBackend/index']],
            ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('OrderModule.order', 'Create order'), 'url' => ['/order/orderBackend/create']],
        ]
    ],
    [
        'label' => Yii::t('OrderModule.order', 'Order statuses'),
        'items' => [
            ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OrderModule.order', 'Manage statuses'), 'url' => ['/order/statusBackend/index']],
            ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('OrderModule.order', 'Create status'), 'url' => ['/order/statusBackend/create']],
        ]
    ],
];
?>
<?php
$this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'status-grid',
        'type' => 'condensed',
        'dataProvider' => $model->search(),
	    'sortableRows'      => true,
	    'sortableAjaxSave'  => true,
	    'sortableAttribute' => 'position',
	    'sortableAction'    => '/order/statusBackend/sortable',
        'filter' => $model,
        'actionsButtons' => [
            'add' => CHtml::link(
                Yii::t('OrderModule.order', 'Add'),
                ['/order/statusBackend/create'],
                ['class' => 'btn btn-sm btn-success pull-right']
            ),
        ],
        'columns' => [
            [
                'name' => 'id',
                'htmlOptions' => ['width' => '50px'],
                'filter' => false,
            ],
            [
                'name' => 'name',
                'type' => 'raw',
                'value' => function (OrderStatus $data) {
                    if (!$data->color) {
                        return $data->name;
                    }

                    return CHtml::tag('span', ['class' => 'label label-' . $data->color], $data->name);
                }
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => [
                        'visible' => function ($row, $data) {
                            return !$data->is_system ? true : false;
                        }
                    ],
                    'delete' => [
                        'visible' => function ($row, $data) {
                            return !$data->is_system ? true : false;
                        }
                    ],
                ],
            ],
        ],
    ]
); ?>
