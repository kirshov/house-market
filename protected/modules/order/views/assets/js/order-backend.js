$(function () {
	if ($('#delivery-type').val() > 0) {
		$('#delivery-type').trigger('change');
	}
})
function changePickupItems(link, current) {
	$('#Order_pickup_id option').filter('[value!=""]').remove();
	var deliveryId = parseInt($('#delivery-type option:selected').data('pickup-group'));
	if (deliveryId) {
		var data = {'deliveryId': deliveryId};
		data[webformaTokenName] = webformaToken;
		$.post(link, data, function (response) {
			if (response.result && Object.keys(response.data).length) {
				var option = false;
				$.each(response.data, function (index, element) {
					if (index == current) {
						option = true;
					} else {
						option = false;
					}
					$('#Order_pickup_id').append(new Option(element, index, option));
				})
				if (current) {
					$('#Order_pickup_id').val(current);
				}
				$('#Order_pickup_id').removeAttr('disabled');
				$('#Order_pickup_id').closest('.wrapper').removeClass('hidden');
			} else {
				$('#Order_pickup_id').closest('.wrapper').addClass('hidden');
			}
		});
	} else {
		$('#Order_pickup_id').closest('.wrapper').addClass('hidden');
	}
}