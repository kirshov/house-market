<?php
$this->breadcrumbs = [
    Yii::t('OrderModule.source', 'Sources') => ['/order/sourceBackend/index'],
    Yii::t('OrderModule.source', 'Change'),
];

$this->pageTitle = Yii::t('OrderModule.source', 'Sources - edit');

$this->menu = [
	['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OrderModule.order', 'Manage orders'), 'url' => ['/order/orderBackend/index']],
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('OrderModule.source', 'Source manage'),
        'url' => ['/order/sourceBackend/index']
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('OrderModule.source', 'Create source'),
        'url' => ['/order/sourceBackend/create']
    ],
    ['label' => Yii::t('OrderModule.source', 'Source') . ' «' . mb_substr($model->name, 0, 32) . '»'],
    [
        'icon' => 'fa fa-fw fa-pencil',
        'label' => Yii::t('OrderModule.source', 'Change source'),
        'url' => [
            '/order/sourceBackend/update',
            'id' => $model->id
        ]
    ],
    [
        'icon' => 'fa fa-fw fa-trash-o',
        'label' => Yii::t('OrderModule.source', 'Remove source'),
        'url' => '#',
        'linkOptions' => [
            'submit' => ['/order/sourceBackend/delete', 'id' => $model->id],
            'params' => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
            'confirm' => Yii::t('OrderModule.source', 'Do you really want to remove source?'),
            'csrf' => true,
        ]
    ],
];
?>
<?$this->title = Yii::t('OrderModule.source', 'Editing source').'<br><small>&laquo;'.$model->name.'&raquo;</small>';?>

<?= $this->renderPartial(
    '_form',
    ['model' => $model]
);
