<?php
$this->breadcrumbs = [
    Yii::t('OrderModule.source', 'Sources') => ['/order/sourceBackend/index'],
    Yii::t('OrderModule.source', 'Manage'),
];

$this->pageTitle = Yii::t('OrderModule.source', 'Sources - manage');

$this->menu = [
	[
		'icon' => 'fa fa-fw fa-list-alt',
		'label' => Yii::t('OrderModule.order', 'Manage orders'),
		'url' => ['/order/orderBackend/index'],
	],
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('OrderModule.source', 'Source manage'),
        'url' => ['/order/sourceBackend/index']
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('OrderModule.source', 'Create source'),
        'url' => ['/order/sourceBackend/create']
    ],
];
?>

<?$this->title = Yii::t('OrderModule.source', 'Sources').'<br><small>'.Yii::t('OrderModule.source', 'manage').'</small>';?>

<?php $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'source-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => [
            [
                'name' => 'id',
                'htmlOptions' => ['style' => 'width:20px'],
                'type' => 'raw',
                'value' => 'CHtml::link($data->id, array("/order/sourceBackend/update", "id" => $data->id))'
            ],
            [
                'class' => 'webforma\widgets\WEditableColumn',
                'name' => 'name',
                'editable' => [
                    'url' => $this->createUrl('/order/sourceBackend/inline'),
                    'mode' => 'popup',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter' => CHtml::activeTextField($model, 'name', ['class' => 'form-control']),
            ],
            [
                'class' => 'webforma\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('/order/sourceBackend/inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    Source::STATUS_PUBLISHED => ['class' => 'label-success'],
                    Source::STATUS_MODERATION => ['class' => 'label-warning'],
                    Source::STATUS_DRAFT => ['class' => 'label-default'],
                ],
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
				'template' => '{update} {delete}',
            ],
        ],
    ]
);
