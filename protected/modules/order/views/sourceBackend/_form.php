<?php
/**
 * @var $this SourceBackendController
 * @var $form \webforma\widgets\ActiveForm
 * @var $model Source
 */

$form = $this->beginWidget(
    '\webforma\widgets\ActiveForm',
    [
        'id' => 'source-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => 'vertical',
		'htmlOptions' => ['class' => 'well sticky'],
		'clientOptions' => [
			'validateOnSubmit' => true,
		],
    ]
); ?>
<div class="alert alert-info">
    <?= Yii::t('OrderModule.source', 'Fields with'); ?>
    <span class="required">*</span>
    <?= Yii::t('OrderModule.source', 'are required'); ?>
</div>

<?= $form->errorSummary($model); ?>

<div class="row">

    <div class='col-sm-6'>

    </div>
</div>

<div class='row'>
    <div class="col-sm-3">
        <?= $form->textFieldGroup($model, 'name'); ?>
    </div>

	<div class="col-sm-3">
		<?= $form->dropDownListGroup($model, 'status', [
			'widgetOptions' => [
				'data' => $model->getStatusList(),
			],
		]); ?>
	</div>
</div>

<div class='row'>
    <div class="col-sm-12">
        <div class="form-group">
            <?= $form->labelEx($model, 'description'); ?>
            <?php $this->widget($this->module->getVisualEditor(), [
				'model' => $model,
				'attribute' => 'description',
			]); ?>
        </div>
    </div>
</div>


<div class="buttons">
	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context' => 'primary',
		'label' => $model->isNewRecord
			? Yii::t('OrderModule.source','Create source and continue')
			: Yii::t('OrderModule.source','Save source and continue'),
	]); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label' => $model->isNewRecord
			? Yii::t('OrderModule.source', 'Create source and close')
			: Yii::t('OrderModule.source','Save source and close'),
	]); ?>
</div>
<?php $this->endWidget(); ?>
