<?php
$this->breadcrumbs = [
    Yii::t('OrderModule.source', 'Sources') => ['/order/sourceBackend/index'],
    Yii::t('OrderModule.source', 'Create'),
];

$this->pageTitle = Yii::t('OrderModule.source', 'Sources - create');

$this->menu = [
	['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OrderModule.order', 'Manage orders'), 'url' => ['/order/orderBackend/index']],
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('OrderModule.source', 'Source manage'),
        'url' => ['/order/sourceBackend/index']
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('OrderModule.source', 'Create source'),
        'url' => ['/order/sourceBackend/create']
    ],
];
?>

<?$this->title = Yii::t('OrderModule.source', 'Source').'<br><small>'.Yii::t('OrderModule.source', 'create').'</small>';?>

<?= $this->renderPartial('_form', ['model' => $model]); ?>
