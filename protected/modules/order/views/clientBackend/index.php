<?php

$this->breadcrumbs = [
    Yii::t('OrderModule.order', 'Clients'),
];

$this->pageTitle = Yii::t('OrderModule.order', 'Clients - manage');

$this->menu = [
    [
        'label' => Yii::t('OrderModule.order', 'Clients'),
        'items' => [
            ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OrderModule.order', 'Manage orders'), 'url' => ['/order/orderBackend/index']],
            ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OrderModule.order', 'Manage clients'), 'url' => ['/order/clientBackend/index']],
        ]
    ]
];


$this->title = Yii::t('OrderModule.order', 'Clients');
$this->subTitle = Yii::t('OrderModule.order', 'manage');
?>

<?php
$this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'order-grid',
        'type' => 'condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'hideBulkActions' => true,
        'actionsButtons' => [],
        'columns' => [
            /*[
                'name' => 'last_name',
                'type' => 'html',
                'value' => function($data){
                    return CHtml::link($data->last_name, ['/order/clientBackend/view', 'id' => $data->id]);
                }
            ],
            [
                'name' => 'first_name',
                'type' => 'html',
                'value' => function($data){
                    return CHtml::link($data->first_name, ['/order/clientBackend/view', 'id' => $data->id]);
                }
            ],
            [
                'name' => 'middle_name',
                'type' => 'html',
                'value' => function($data){
                    return CHtml::link($data->middle_name, ['/order/clientBackend/view', 'id' => $data->id]);
                }
            ],*/
	        [
		        'name' => 'name',
		        'type' => 'html',
		        'value' => function($data){
	                $link = $data->getViewLink();
	                if($link){
		                return CHtml::link($data->getName(), $link);
                    } else {
	                    return $data->getName();
                    }
		        }
	        ],
            [
                'name' => 'email',
                'type' => 'html',
                'value' => function($data){
	                $link = $data->getViewLink();
	                if($link){
		                return CHtml::link($data->email, $link);
	                } else {
		                return $data->email;
	                }
                }
            ],
            [
                'name' => 'phone',
                'type' => 'html',
                'value' => function($data){
	                $link = $data->getViewLink();
	                if($link){
		                return CHtml::link($data->phone, $link);
	                } else {
		                return $data->phone;
	                }
                }
            ],
            [
                'name'   => 'ordersTotalNumber',
                'header' => Yii::t('OrderModule.order', 'Orders'),
                'type'   => 'html',
                'value'  => function(Client $client){
	        	    if($client->phone){
			            $data = CHtml::link($client->getOrderNumber(), ['/order/orderBackend/index', 'Order[phone]' => $client->phone]);
		            } else {
	        	    	$data = $client->getOrderNumber();
		            }

                    $order = $client->getLastOrder();
                    if($order) {
                        $data .= ' <span class="label label-default">'.CHtml::link($order->id, ['/order/orderBackend/update', 'id' => $order->id]).' '.Yii::t('OrderModule.order', 'from').' '.Yii::app()->getDateFormatter()->formatDateTime($order->date, 'short', false).'</span>';
                    }
                    return $data;
                },
            ],
            [
                'name'   => 'ordersTotalSum',
                'header' => Yii::t('OrderModule.order', 'Money'),
                'value' => function($data){
                    return '<span class="label label-default">'.Yii::app()->numberFormatter->formatCurrency($data->getOrderSum(), Yii::app()->getModule('store')->currency).'</span>';
                },
                'type' => 'html'
            ],
            /*[
                'name'   => 'create_time',
                'filter' => false,
                'value'  => function($data){
                    return Yii::app()->getDateFormatter()->formatDateTime($data->create_time, 'short', false);
                },
            ],*/
            /*[
                'name'   => 'visit_time',
                'value'  => function($data){
                    return Yii::app()->getDateFormatter()->formatDateTime($data->visit_time, 'short', false);
                },
                'filter' => false
            ],*/
            /*[
                'class'   => 'webforma\widgets\EditableStatusColumn',
                'name'    => 'status',
                'url'     => $this->createUrl('/user/userBackend/inline'),
                'source'  => $model->getStatusList(),
                'options' => [
                    User::STATUS_ACTIVE     => ['class' => 'label-success'],
                    User::STATUS_BLOCK      => ['class' => 'label-danger'],
                    User::STATUS_NOT_ACTIVE => ['class' => 'label-warning'],
                ],
            ],*/
            /*[
                'class' => 'webforma\widgets\CustomButtonColumn',
                'buttons' => [
                    'update' => [
                        'url' => '["/user/userBackend/update", "id" => $data->id]'
                    ],
                    'delete' => [
                        'url' => '["/user/userBackend/delete", "id" => $data->id]'
                    ]
                ]
            ],*/
        ],
    ]
); ?>
