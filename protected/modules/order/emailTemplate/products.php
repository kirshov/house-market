<?
/**
 * @var Order $order
 * @var OrderProduct $orderProduct
 */
?>
<table cellpadding="6" cellspacing="0" style="border-collapse: collapse; width: 100%;">
	<tr>
		<td colspan="2" style="border: 1px solid #eee; background: #f8f8f8; padding: 18px; color: #32373d; font: %font%;">Товар</td>
		<td align="center" style="border: 1px solid #eee; background: #f8f8f8; padding: 18px; color: #32373d; font: %font%;">Кол-во</td>
		<td align="center" style="border: 1px solid #eee; background: #f8f8f8; padding: 18px; color: #32373d; font: %font%;">Сумма</td>
	</tr>
	<?php foreach ($order->products as $orderProduct): ?>
		<?php
			$isProduct = !is_null($orderProduct->product);
			$productUrl = $isProduct ? ProductHelper::getUrl($orderProduct->product, true) : '';
		?>
		<tr>
			<td align="center" style="padding: 20px 19px; vertical-align: middle; border: 1px solid #eee; border-right-color: transparent;">
				<?php if ($isProduct): ?>
					<a style="display: block; width: 80px;" href="<?= $productUrl; ?>">
						<?php if ($orderProduct->product->image): ?>
							<img border="0" src="<?= $orderProduct->product->getImageUrl(80, 80, false); ?>">
						<?php endif; ?>
					</a>
				<?php else: ?>

				<?php endif; ?>
			</td>

			<td style="padding: 20px 0;vertical-align: middle; border: 1px solid #eee;">
				<?if($isProduct):?>
					<div style="width: 185px; padding-right: 10px;">
						<a style="color: %linkColor%; font: %font%; text-decoration: none;" href="<?= $productUrl; ?>"><?= $orderProduct->getNameInEmail(); ?></a>
					</div>

					<?$variants = Yii::app()->getController()->renderPartial('//order/order/_attributes', ['position' => $orderProduct, 'product' => $orderProduct->product], true);?>
					<?if($variants):?>
						<div style="color: #b1b1b1; font: %font%; margin-top: 10px; width: 185px; padding-right: 10px;">
							<?=$variants;?>
						</div>
					<?endif;?>
				<?else:?>
					<div style="width: 185px; padding-right: 10px;">
						<span style="color: %linkColor%; font: %font%; text-decoration: none;"><?= CHtml::encode($orderProduct->product_name); ?></span>
					</div>
				<?endif;?>
			</td>

			<td style="vertical-align: middle; border: 1px solid #eee; text-align: center; color: #222; font: %font%; padding: 20px 0;">
				<div><?= $orderProduct->quantity; ?></div>
			</td>
			<td style="vertical-align: middle; border: 1px solid #eee; text-align: center; color: #222; font: %font%; padding: 20px 0;">
				 <?= ProductHelper::getFormatPrice($orderProduct->price * $orderProduct->quantity); ?> <?= $currency ?>
			</td>
		</tr>
	<?php endforeach; ?>
	<tr>
		<td align="right" colspan="2" style="border:1px solid #eee; padding: 20px; font: bold %font%; color: #222;">Общая стоимость товара</td>
		<td align="center" colspan="2" style="border:1px solid #eee; padding: 20px; font: bold %font%; color: #222;"><?= ProductHelper::getFormatPrice($order->getFullTotalPrice()); ?>&nbsp;<?= $currency ?></td>
	</tr>

	<?php if ($order->hasCoupons()): ?>
		<tr>
			<td align="right" colspan="2" style="border:1px solid #eee; padding: 20px; font: bold %font%; color: #222;">
				Купон "<?= CHtml::encode(implode(', ', $order->getCouponsCodes())); ?>"
			</td>
			<td align="center" colspan="2" style="border:1px solid #eee; padding: 20px; font: bold %font%; color: #222;">
				&minus;<?= ProductHelper::getFormatPrice($order->coupon_discount); ?>&nbsp;<?= $currency ?>
			</td>
		</tr>
	<?php endif; ?>

	<?php if ($order->delivery && !$order->separate_delivery): ?>
		<tr>
			<?$titeDelivery = mb_stripos($model->delivery->name, 'доставка') !== false ? '' : 'Доставка: '?>
			<td align="right" colspan="2" style="border:1px solid #eee; padding: 20px; font: bold %font%; color: #222;"><?= $titeDelivery.CHtml::encode($order->delivery->name); ?></td>
			<td align="center" colspan="2" style="border:1px solid #eee; padding: 20px; font: bold %font%; color: #222;">
				<?= ProductHelper::getFormatPrice($order->getDeliveryPrice()); ?>&nbsp;<?= $currency ?>
			</td>
		</tr>
	<?php endif; ?>

	<tr>
		<td align="right" colspan="2" style="border:1px solid #eee; padding: 20px; font: bold %font%; color: #222; background: #f8f8f8;">Итого к оплате</td>
		<td colspan="2" align="center" style="border:1px solid #eee; padding: 20px; font: bold %font%; color: #222; background: #f8f8f8;">
			<?= ProductHelper::getFormatPrice($order->getTotalPriceWithDelivery()); ?>&nbsp;<?= $currency ?>
		</td>
	</tr>
</table>
