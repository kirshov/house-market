<?$c=1;?>
<table cellpadding="6" cellspacing="0" style="border-collapse: collapse; width: 100%;">
	<?if(!$isAdmin):?>
		<tr>
			<td style="%info-table-odd% width: 173px;">
				Статус заказа
			</td>
			<td style="%info-table-odd%">
				<?= CHtml::encode($order->getStatusTitle()); ?>
			</td>
		</tr>
	<?endif;?>
	<?$c++;?>
	<?php if ($order->name): ?>
		<tr>
			<td style="%info-table-<?=Helper::getParity($c);?>% width: 173px;">
				Имя, фамилия
			</td>
			<td style="%info-table-<?=Helper::getParity($c);?>%">
				<?= CHtml::encode($order->name); ?>
			</td>
		</tr>
		<?$c++;?>
	<?php endif; ?>
	<?php if ($order->email): ?>
		<tr>
			<td style="%info-table-<?=Helper::getParity($c);?>% width: 173px;">
				E-mail
			</td>
			<td style="%info-table-<?=Helper::getParity($c);?>%">
				<?= CHtml::encode($order->email); ?>
			</td>
		</tr>
		<?$c++;?>
	<?php endif; ?>
	<?php if ($order->phone): ?>
		<tr>
			<td style="%info-table-<?=Helper::getParity($c);?>% width: 173px;">
				Телефон
			</td>
			<td style="%info-table-<?=Helper::getParity($c);?>%">
				<?= CHtml::encode($order->phone); ?>
			</td>
		</tr>
		<?$c++;?>
	<?php endif; ?>
	<?php if ($isAdmin): ?>
		<tr>
			<td style="%info-table-<?=Helper::getParity($c);?>% width: 173px;">
				IP-адрес
			</td>
			<td style="%info-table-<?=Helper::getParity($c);?>%">
				<?= Yii::app()->getRequest()->getUserHostAddress(); ?>
			</td>
			<?$c++;?>
		</tr>
	<?php endif; ?>
	<?php if ($order->city): ?>
		<tr>
			<td style="%info-table-<?=Helper::getParity($c);?>% width: 173px;">
				Город
			</td>
			<td style="%info-table-<?=Helper::getParity($c);?>%">
				<?= CHtml::encode($order->city); ?>
			</td>
		</tr>
		<?$c++;?>
	<?php endif; ?>
	<?php if($order->pickup_id && $order->pickup):?>
		<tr>
			<td style="%info-table-<?=Helper::getParity($c);?>% width: 173px;">
				Самовывоз
			</td>
			<td style="%info-table-<?=Helper::getParity($c);?>%">
				<?= CHtml::encode($order->pickup->name); ?>
			</td>
		</tr>
		<?$c++;?>
	<?php else: ?>
		<?php if($order->delivery):?>
			<tr>
				<td style="%info-table-<?=Helper::getParity($c);?>% width: 173px">
					Способ доставки
				</td>
				<td style="%info-table-<?=Helper::getParity($c);?>%">
					<?= CHtml::encode($order->delivery->name); ?>
				</td>
			</tr>
			<?$c++;?>
		<?php endif; ?>
		<?if (trim($order->getAddress())):?>
			<tr>
				<td style="%info-table-<?=Helper::getParity($c);?>% width: 173px;">
					Адрес доставки
				</td>
				<td style="%info-table-<?=Helper::getParity($c);?>%">
					<?= CHtml::encode($order->getAddress()); ?>
				</td>
			</tr>
			<?$c++;?>
		<?php endif; ?>
	<?php endif; ?>
	<?php if (isset($order->payment) && $order->payment): ?>
		<tr>
			<td style="%info-table-<?=Helper::getParity($c);?>% width: 173px;">
				Способ оплаты
			</td>
			<td style="%info-table-<?=Helper::getParity($c);?>%">
				<?= CHtml::encode($order->payment->name); ?>
			</td>
		</tr>
		<?$c++;?>
	<?php endif; ?>
	<?php if ($order->comment): ?>
		<tr>
			<td style="%info-table-<?=Helper::getParity($c);?>% width: 173px;">
				Комментарий
			</td>
			<td style="%info-table-<?=Helper::getParity($c);?>%">
				<?= nl2br(CHtml::encode($order->comment)); ?>
			</td>
		</tr>
		<?$c++;?>
	<?php endif; ?>

	<?if($isAdmin && Yii::app()->hasModule('utm') && Yii::app()->getComponent('utm')->has()):?>
		<?foreach (Yii::app()->getComponent('utm')->get() as $title => $value):?>
			<tr>
				<td style="%info-table-<?=Helper::getParity($c);?>% width: 173px;">
					<?=$title?>
				</td>
				<td style="%info-table-<?=Helper::getParity($c);?>%">
					<?= CHtml::encode($value); ?>
				</td>
			</tr>
			<?$c++;?>
		<?endforeach;?>
	<?endif?>
</table>
