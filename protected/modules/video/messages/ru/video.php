<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yiic message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE, this file must be saved in UTF-8 encoding.
 */
return [
    '*unknown*' => '*неизвестно*',
    '--choose video--' => '--выберите видеогалерею--',
    '0.3' => '0.3',
    'active' => 'активно',
    'Add necessary classes to &lt;li&gt; tag' => 'Добавляет необходимые классы тегу &lt;li&gt;',
    'Add notice to link' => 'Добавляет подсказку к ссылке',
    'Address' => 'Адрес',
    'Answer' => 'Ответить',
    'are required.' => 'обязательны.',
    'Attribute class' => 'Атрибут class',
    'Attribute rel' => 'Атрибут rel',
    'Attribute target' => 'Атрибут target',
    'Attribute title' => 'Атрибут title',
    'Bad request. Please don\'t try similar requests anymore!' => 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы!',
    'Change video item' => 'Редактировать видео',
    'Change video' => 'Редактировать видеогалереи',
    'choose video' => 'выберите видеогалерею',
    'Create video and close' => 'Добавить видеогалерею и закрыть',
    'Create video and continue' => 'Добавить видеогалерею и продолжить',
    'Create video item and close' => 'Добавить видео и закрыть',
    'Create video item and continue' => 'Добавить видео и продолжить',
    'Create video item' => 'Добавить видео',
    'Create video' => 'Добавить видеогалерею',
    'Create' => 'Добавление',
    'create' => 'добавление',
    'Creating video item' => 'Создание видеозаписей',
    'Creating video' => 'Создание видеогалереи',
    'Delete' => 'Удалить',
    'Description' => 'Описание',
    'Do you really want to delete selected elements?' => 'Вы уверены, что хотите удалить выбранные элементы?',
    'Do you really want to delete?' => 'Подтверждаете удаление?',
    'Do you really want to remove video item?' => 'Вы уверены, что хотите удалить видео?',
    'Do you really want to remove video?' => 'Вы уверены, что хотите удалить видеогалерею?',
    'Don\'t handle address to router' => 'Не обрабатывать адрес роутером',
    'Edit video item' => 'Редактирование видеозаписи',
    'Edit video' => 'Редактирование видеогалереи',
    'Edit' => 'Редактирование',
    'Editing video items' => 'Редактирование видеозаписей',
    'Editing video' => 'Редактирование видеогалереи',
    'Extended settings' => 'Расширенные параметры',
    'False condition' => 'Отрицание условия',
    'Fields with' => 'Поля, отмеченные',
    'Find video item' => 'Искать видео',
    'Find video items' => 'Поиск видеозаписей',
    'Find video' => 'Поиск видеогалереи',
    'id' => 'Id',
    'Item order number in video' => 'Порядковый номер видеозаписи.',
    'Item title' => 'Заголовок видеозаписи.',
    'Items' => 'Видеозаписи',
    'List of video items' => 'Просмотр списка видеозаписей',
    'List of video' => 'Просмотр списка видеогалерей',
    'Manage video items' => 'Видеозаписи',
    'Manage video' => 'Видеогалерея',
    'Manage videos' => 'Видеогалерея',
    'Manage' => 'Управление',
    'manage' => 'управление',
    'Video - edit' => 'Видеогалерея - редактирование',
    'Video - insert' => 'Видеогалерея - добавление',
    'Video - manage' => 'Видеогалерея - управление',
    'Video - show' => 'Видеогалерея - просмотр',
    'Video Id' => 'Id',
    'Video item Id' => 'Id видеозаписи.',
    'Video item' => 'видео',
    'Video items - create' => 'Видеозаписи - добавление',
    'Video items - edit' => 'Видеозаписи - редактирование',
    'Video items - remove' => 'Видеозаписи - управление',
    'Video items - show' => 'Видеозаписи - просмотр',
    'Video items' => 'Видеозаписи',
    'Video management module' => 'Модуль для создания и редактирования видеогалерей',
    'Video name' => 'Название меню в системе.',
    'Video root' => 'Корень меню',
    'Video was created!' => 'Видеогалерея создана!',
    'Video' => 'Видеогалерея',
    'Name' => 'Название',
    'New item was added to video!' => 'Новое видео добавлено!',
    'no' => 'нет',
    'not active' => 'не активно',
    'Page address' => 'Адрес странице на сайте.',
    'Page was not found!' => 'Запрошенная страница не найдена!',
    'Parent' => 'Родитель',
    'Record was updated!' => 'Запись изменена!',
    'Regular link' => 'Обычная ссылка',
    'Remove video item' => 'Удалить видео',
    'Remove video' => 'Удалить видеогалерею',
    'Removing video item' => 'Удаление видеозаписей',
    'Removing video' => 'Удаление видеогалерею',
    'Save video and close' => 'Сохранить видеогалерею и закрыть',
    'Save video and continue' => 'Сохранить видеогалерею и продолжить',
    'Save video item and close' => 'Сохранить видео и закрыть',
    'Save video item and continue' => 'Сохранить видео и продолжить',
    'Select condition for video item visibility' => 'Если данный видео, необходимо выводить только при определенном условии, укажите его.',
    'Select {field}' => 'Выберите {field}',
    'Short description' => 'Краткое описание видеогалереи.',
    'Show video item' => 'Просмотр видеозаписи',
    'Show video' => 'Просмотр видеогалереи',
    'Sorting' => 'Сортировка',
    'Status' => 'Статус',
    'Structure' => 'Структура',
    'Text after link' => 'Текст после ссылки',
    'Text before link' => 'Текст перед ссылкой',
    'This section describes Video Items Management' => 'В данном разделе представлены средства управления видеозаписями',
    'This section describes Video Management' => 'В данном разделе представлены средства управления видеогалерей',
    'Title' => 'Заголовок',
    'Unified code' => 'Уникальный код',
    'Use drag and drop to sort' => 'Используйте drag & drop для сортировки',
    'Using for xfr' => 'Используется для указания xfr',
    'View video item' => 'Просмотреть видео',
    'View video' => 'Просмотреть видеогалерею',
    'Viewing video items' => 'Просмотр видеозаписей',
    'Viewing video' => 'Просмотр видеогалереи',
    'yes' => 'да',
    'webforma team' => 'webforma team',
];
