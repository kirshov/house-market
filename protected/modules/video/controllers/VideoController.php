<?php

class VideoController extends \webforma\components\controllers\FrontController
{
	protected function setLeftMenu()
	{
		$galleries = Video::model()->active()->findAll(['order' => 't.position']);
		if($galleries){
			$menu = [];
			foreach ($galleries as $gallery){
				$menu[] = [
					'label' => $gallery->name,
					'url' => Yii::app()->createUrl('/video/video/view', ['id' => $gallery->id]),
				];
			}

			$this->setTemplateVar('left-menu', $menu);
		}
	}

	/**
     *
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider(
            'Video', [
                'criteria' => [
                    'scopes' => 'active'
                ],
                'sort' => [
                    'defaultOrder' => 't.position',
                ],
		        'pagination' => [
			        'pageSize' => (int)$this->getModule()->perPage,
		        ],
            ]
        );

        $this->setLeftMenu();
        $this->render('index', ['dataProvider' => $dataProvider]);
    }

	/**
	 * @param $id
	 * @throws CDbException
	 * @throws CHttpException
	 */
	public function actionView($id)
	{
		if (($gallery = Video::model()->active()->findByPk($id)) === null) {
			throw new CHttpException(404, Yii::t('VideoModule.video', 'Page was not found!'));
		}

		$dataProvider = new CActiveDataProvider(
			'VideoItem', [
				'criteria' => [
					'condition' => 't.gallery_id = :id AND t.status = :status',
					'params' => [
						':id' => $id,
						':status' => VideoItem::STATUS_ACTIVE,
					],
				],
				'sort' => [
					'defaultOrder' => 't.sort DESC',
				],
				'pagination' => [
					'pageSize' => (int)$this->getModule()->perPage,
				],
			]
		);

		$this->setLeftMenu();

		$this->render('view', [
			'model' => $gallery,
			'dataProvider' => $dataProvider,
		]);
	}
}
