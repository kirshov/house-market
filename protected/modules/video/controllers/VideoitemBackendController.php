<?php

/**
 * VideoitemBackendController контроллер для управления пунктами меню в панели управления
 */
class VideoitemBackendController extends webforma\components\controllers\BackController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Video.VideoitemBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Video.VideoitemBackend.View']],
            [
                'allow',
                'actions' => ['create', 'dynamicParent', 'getjsonitems'],
                'roles' => ['Video.VideoitemBackend.Create'],
            ],
            [
                'allow',
                'actions' => ['update', 'inline', 'sortable', 'dynamicParent', 'getjsonitems'],
                'roles' => ['Video.VideoitemBackend.Update'],
            ],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Video.VideoitemBackend.Delete']],
            ['deny'],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'inline' => [
                'class' => 'webforma\components\actions\WInLineEditAction',
                'model' => 'VideoItem',
                'validAttributes' => ['title', 'href', 'status', 'gallery_id', 'sort'],
            ],
            'sortable' => [
                'class' => 'webforma\components\actions\SortAction',
                'model' => 'VideoItem',
                'attribute' => 'sort',
            ],
        ];
    }

    /**
     * Отображает пункт меню по указанному идентификатору
     *
     * @param integer $id Идинтификатор меню для отображения
     *
     * @return void
     */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }

    /**
     * Создает новую модель пунта меню.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return void
     */
    public function actionCreate()
    {
        $model = new VideoItem();

        $model->gallery_id = Yii::app()->getRequest()->getQuery('mid', null);

        if (($data = Yii::app()->getRequest()->getPost('VideoItem')) !== null) {

            $model->setAttributes($data);

            if ($model->save()) {

                Yii::app()->getUser()->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('VideoModule.video', 'New item was added to video!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['create']
                    )
                );
            }
        }

        $this->render('create', ['model' => $model]);
    }

    /**
     * Редактирование пункта меню.
     *
     * @param integer $id Идинтификатор пункта меню для редактирования
     *
     * @return void
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (($data = Yii::app()->getRequest()->getPost('VideoItem')) !== null) {

            $model->setAttributes($data);

            if ($model->save()) {

                Yii::app()->getUser()->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('VideoModule.video', 'Record was updated!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['update', 'id' => $model->id]
                    )
                );
            }
        }

        $this->render('update', ['model' => $model]);
    }

    /**
     * Удаляет модель пункта меню из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id идентификатор пункта меню, который нужно удалить
     *
     * @return void
     *
     * @throws CHttpException If not POST-query
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            Yii::app()->getUser()->setFlash(
                webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                Yii::t('VideoModule.video', 'Record was removed!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            Yii::app()->getRequest()->getParam('ajax') !== null || $this->redirect(
                (array)Yii::app()->getRequest()->getPost('returnUrl', 'index')
            );
        } else {
            throw new CHttpException(
                400,
                Yii::t('VideoModule.video', 'Bad request. Please don\'t try similar requests anymore!')
            );
        }
    }

    /**
     * Управление пунктами меню.
     *
     * @return void
     */
    public function actionIndex()
    {
        $model = new VideoItem('search');

        $model->unsetAttributes(); // clear any default values

        if (($data = Yii::app()->getRequest()->getParam('VideoItem')) !== null) {
            $model->setAttributes($data);
        }

        $this->render('index', ['model' => $model]);
    }


    /**
     * @throws CHttpException
     */
    public function actionDynamicParent()
    {
        if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
            throw new CHttpException(404);
        }

        $model = new VideoItem('search');

        $model->setAttributes(
            Yii::app()->getRequest()->getParam('VideoItem')
        );

        if ($model->gallery_id) {
            $model->id = Yii::app()->getRequest()->getQuery('id');
        }

        Yii::app()->end();
    }


    /**
     * @param $id
     * @return array|mixed|null
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        if (($model = VideoItem::model()->findByPk($id)) === null) {
            throw new CHttpException(
                404,
                Yii::t('VideoModule.video', 'Page was not found!')
            );
        }

        return $model;
    }
}
