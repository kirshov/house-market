<?php
/**
 * Файл представления video/index
 **/
$this->breadcrumbs = [
    Yii::t('VideoModule.video', 'Video') => ['/video/videoBackend/index'],
    Yii::t('VideoModule.video', 'Manage')
];

$this->pageTitle = Yii::t('VideoModule.video', 'Video - manage');

$this->menu = [
    [
        'label' => Yii::t('VideoModule.video', 'Video'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VideoModule.video', 'Manage video'),
                'url'   => ['/video/videoBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VideoModule.video', 'Create video'),
                'url'   => ['/video/videoBackend/create']
            ],
        ]
    ],
    [
        'label' => Yii::t('VideoModule.video', 'Video items'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VideoModule.video', 'Manage video items'),
                'url'   => ['/video/videoitemBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VideoModule.video', 'Create video item'),
                'url'   => ['/video/videoitemBackend/create']
            ],
	        [
		        'icon'  => 'fa fa-fw fa-external-link-square',
		        'label' => 'Смотреть на сайте',
		        'url'   => Yii::app()->createAbsoluteUrl('video/video/index/'),
	        ],
        ]
    ],
];
?>
<?php $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id'           => 'video-grid',
        'sortableRows'      => true,
        'sortableAjaxSave'  => true,
        'sortableAttribute' => 'position',
        'sortableAction'    => '/video/videoBackend/sortable',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => [
            [
                'header' => '',
                'value' => 'CHtml::image($data->getImageUrl(75, 75), $data->name, array("width" => 75))',
                'type' => 'html'
            ],
            [
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'name'     => 'name',
                'editable' => [
                    'url'    => $this->createUrl('/video/videoBackend/inline'),
                    'mode'   => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'name', ['class' => 'form-control']),
            ],
            [
                'class'   => 'webforma\widgets\EditableStatusColumn',
                'name'    => 'status',
                'url'     => $this->createUrl('/video/videoBackend/inline'),
                'source'  => $model->getStatusList(),
                'options' => [
                    Video::STATUS_ACTIVE   => ['class' => 'label-success'],
                    Video::STATUS_DISABLED => ['class' => 'label-default'],
                ],
            ],
            [
                'header' => Yii::t('VideoModule.video', 'Items'),
                'type'   => 'raw',
                'value'  => 'CHtml::link(count($data->videoItems), Yii::app()->createUrl("/video/videoitemBackend/index", array("VideoItem[video_id]" => $data->id)))',
            ],
            [
                'class'    => 'webforma\widgets\CustomButtonColumn',
                'template' => '{view}{update}{delete}{add}',
                'buttons'  => [
                    'add' => [
                        'icon'  => 'fa fa-fw fa-plus-square',
                        'label' => Yii::t('VideoModule.video', 'Create video item'),
                        'url'   => 'Yii::app()->createUrl("/video/videoitemBackend/create", array("mid" => $data->id))',
                        'options' => ['class' => 'btn btn-sm btn-default']
                    ],
                ],
            ],
        ],
    ]
); ?>
