<?php
$form = $this->beginWidget(
    'webforma\widgets\ActiveForm',
    [
        'id'                     => 'video-form',
	    'enableAjaxValidation' => false,
	    'enableClientValidation' => false,
	    'type' => 'vertical',
	    'htmlOptions' => ['class' => 'well sticky', 'enctype' => 'multipart/form-data'],
	    'clientOptions' => [
		    'validateOnSubmit' => true,
	    ],
    ]
); ?>
<div class="alert alert-info">
    <?=  Yii::t('VideoModule.video', 'Fields with'); ?>
    <span class="required">*</span>
    <?=  Yii::t('VideoModule.video', 'are required.'); ?>
</div>

<?=  $form->errorSummary($model); ?>

<div class="row">
    <div class="col-sm-6">
        <?=  $form->textFieldGroup($model, 'name'); ?>
    </div>
</div>
<?/*
<div class="row">
    <div class="col-sm-6">
        <?=  $form->slugFieldGroup($model, 'slug', [
			'sourceAttribute' => 'name',
		]); ?>
    </div>
</div>
*/?>
<div class='row'>
	<div class="col-sm-6">
		<?php $this->widget('webforma\widgets\WInputFile', [
			'model' => $model,
			'attribute' => 'image',
		]); ?>
	</div>
</div>
<div class="row">
    <div class="col-sm-12">
	    <?= $form->labelEx($model, 'description'); ?>
		<?
		$this->widget($this->module->getVisualEditor(), [
			'model'     => $model,
			'attribute' => 'description',
		]); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-3">
        <?=  $form->dropDownListGroup($model, 'status', [
			'widgetOptions' => [
				'data'        => $model->getStatusList(),
			],
		]); ?>
    </div>
</div>

<div class="buttons">
	<?php
	$this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context'    => 'primary',
		'label'      => $model->getIsNewRecord()
			? Yii::t('VideoModule.video', 'Create video and continue')
			: Yii::t('VideoModule.video', 'Save video and continue'),
	]); ?>

	<?php
	$this->widget('bootstrap.widgets.TbButton', [
		'buttonType'  => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label'       => $model->getIsNewRecord()
			? Yii::t('VideoModule.video', 'Create video and close')
			: Yii::t('VideoModule.video', 'Save video and close'),
	]); ?>
</div>
<?php $this->endWidget(); ?>
