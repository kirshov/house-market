<?php
/**
 * Файл представления video/update
 **/
$this->breadcrumbs = [
    Yii::t('VideoModule.video', 'Video') => ['/video/videoBackend/index'],
    $model->name                      => ['/video/videoBackend/view', 'id' => $model->id],
    Yii::t('VideoModule.video', 'Edit'),
];

$this->pageTitle = Yii::t('VideoModule.video', 'Video - edit');

$this->menu = [
    [
        'label' => Yii::t('VideoModule.video', 'Video'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VideoModule.video', 'Manage video'),
                'url'   => ['/video/videoBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VideoModule.video', 'Create video'),
                'url'   => ['/video/videoBackend/create']
            ],
            [
                'icon'  => 'fa fa-fw fa-pencil',
                'label' => Yii::t('VideoModule.video', 'Change video'),
                'url'   => [
                    '/video/videoBackend/update',
                    'id' => $model->id
                ]
            ],
            [
                'icon'        => 'fa fa-fw fa-trash-o',
                'label'       => Yii::t('VideoModule.video', 'Remove video'),
                'url'         => '#',
                'linkOptions' => [
                    'submit'  => ['/video/videoBackend/delete', 'id' => $model->id],
                    'confirm' => Yii::t('VideoModule.video', 'Do you really want to remove video?'),
                    'params'  => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
                ],
            ],
        ]
    ],
    [
        'label' => Yii::t('VideoModule.video', 'Video items'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VideoModule.video', 'Manage video items'),
                'url'   => ['/video/videoitemBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VideoModule.video', 'Create video item'),
                'url'   => ['/video/videoitemBackend/create']
            ],
        ]
    ],
]; ?>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>
