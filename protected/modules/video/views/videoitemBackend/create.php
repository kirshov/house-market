<?php
$this->breadcrumbs = [
    Yii::t('VideoModule.video', 'Video')       => ['/video/videoBackend/index'],
    Yii::t('VideoModule.video', 'Video items') => ['/video/videoitemBackend/index'],
    Yii::t('VideoModule.video', 'Create'),
];

$this->pageTitle = Yii::t('VideoModule.video', 'Video items - create');

$this->menu = [
    [
        'label' => Yii::t('VideoModule.video', 'Video'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VideoModule.video', 'Manage video'),
                'url'   => ['/video/videoBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VideoModule.video', 'Create video'),
                'url'   => ['/video/videoBackend/create']
            ],
        ]
    ],
    [
        'label' => Yii::t('VideoModule.video', 'Video items'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VideoModule.video', 'Manage video items'),
                'url'   => ['/video/videoitemBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VideoModule.video', 'Create video item'),
                'url'   => ['/video/videoitemBackend/create']
            ],
        ]
    ],
];
?>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>
