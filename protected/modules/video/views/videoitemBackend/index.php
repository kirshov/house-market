<?php
$this->breadcrumbs = [
    Yii::t('VideoModule.video', 'Video') => ['/video/videoBackend/index'],
    Yii::t('VideoModule.video', 'Video items'),
];

$this->pageTitle = Yii::t('VideoModule.video', 'Video items - remove');

$this->menu = [
    [
        'label' => Yii::t('VideoModule.video', 'Video'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VideoModule.video', 'Manage video'),
                'url'   => ['/video/videoBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VideoModule.video', 'Create video'),
                'url'   => ['/video/videoBackend/create']
            ],
        ]
    ],
    [
        'label' => Yii::t('VideoModule.video', 'Video items'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VideoModule.video', 'Manage video items'),
                'url'   => ['/video/videoitemBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VideoModule.video', 'Create video item'),
                'url'   => ['/video/videoitemBackend/create']
            ],
        ]
    ],
];
?>


<?php $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id'                => 'video-items-grid',
        'sortableRows'      => true,
        'sortableAjaxSave'  => true,
        'sortableAttribute' => 'sort',
        'sortableAction'    => '/video/videoitemBackend/sortable',
        'dataProvider'      => $model->search(),
        'filter'            => $model,
        'actionsButtons'    => [
            CHtml::link(
                Yii::t('WebformaModule.webforma', 'Add'),
                ['/video/videoitemBackend/create'],
                ['class' => 'btn btn-success pull-right btn-sm']
            )
        ],
        'columns'           => [
            [
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'name'     => 'title',
                'editable' => [
                    'url'    => $this->createUrl('/video/videoitemBackend/inline'),
                    'mode'   => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'title', ['class' => 'form-control']),
            ],
            [
                'name'   => 'gallery_id',
                'value'  => '$data->gallery->name',
                'filter' => CHtml::listData(Video::model()->findAll(), 'id', 'name')
            ],
            [
                'class'   => 'webforma\widgets\EditableStatusColumn',
                'name'    => 'status',
                'url'     => $this->createUrl('/video/videoitemBackend/inline'),
                'source'  => $model->getStatusList(),
                'options' => [
                    VideoItem::STATUS_ACTIVE   => ['class' => 'label-success'],
                    VideoItem::STATUS_DISABLED => ['class' => 'label-default'],
                ],
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
            ],
        ],
    ]
); ?>
