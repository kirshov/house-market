<?php
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'id' => 'video-item-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'type' => 'vertical',
        'htmlOptions' => ['class' => 'well sticky'],
    ]
); ?>
<div class="alert alert-info">
    <?= Yii::t('VideoModule.video', 'Fields with'); ?>
    <span class="required">*</span>
    <?= Yii::t('VideoModule.video', 'are required.'); ?>
</div>

<?= $form->errorSummary($model); ?>

<div class="row">
    <div class="col-sm-3">
        <?= $form->dropDownListGroup(
            $model,
            'gallery_id',
            [
                'widgetOptions' => [
                    'data' => CHtml::listData(Video::model()->findAll(), 'id', 'name'),
                    'htmlOptions' => [
                        'empty' => Yii::t('VideoModule.video', '--choose video--'),
                    ],
                ],
            ]
        ); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <?= $form->textFieldGroup($model, 'title'); ?>
    </div>
    <div class="col-sm-2">
        <?= $form->dropDownListGroup($model, 'status', [
			'widgetOptions' => [
				'data' => $model->getStatusList(),
			],
		]); ?>
    </div>
</div>

<div class="row">
	<div class="col-sm-12">
		<?= $form->textAreaGroup($model, 'code'); ?>
	</div>
</div>


<div class="row hidden">
    <div class="col-sm-6">
        <?= $form->textFieldGroup($model, 'sort'); ?>
    </div>
</div>


<div class="buttons">
	<?php $this->widget(
		'bootstrap.widgets.TbButton',
		[
			'buttonType' => 'submit',
			'context' => 'primary',
			'label' => $model->getIsNewRecord() ? Yii::t('VideoModule.video', 'Create video item and continue') : Yii::t(
				'VideoModule.video',
				'Save video item and continue'
			),
		]
	); ?>

	<?php $this->widget(
		'bootstrap.widgets.TbButton',
		[
			'buttonType' => 'submit',
			'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
			'label' => $model->getIsNewRecord() ? Yii::t('VideoModule.video', 'Create video item and close') : Yii::t(
				'VideoModule.video',
				'Save video item and close'
			),
		]
	); ?>
</div>
<?php $this->endWidget(); ?>
