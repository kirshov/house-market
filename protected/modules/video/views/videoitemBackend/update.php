<?php
$this->breadcrumbs = [
    Yii::t('VideoModule.video', 'Video')       => ['/video/videoBackend/index'],
    Yii::t('VideoModule.video', 'Video items') => ['/video/videoitemBackend/index'],
    $model->title                           => ['/video/videoitemBackend/view', 'id' => $model->id],
    Yii::t('VideoModule.video', 'Edit'),
];

$this->pageTitle = Yii::t('VideoModule.video', 'Video items - edit');

$this->menu = [
    [
        'label' => Yii::t('VideoModule.video', 'Video'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VideoModule.video', 'Manage video'),
                'url'   => ['/video/videoBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VideoModule.video', 'Create video'),
                'url'   => ['/video/videoBackend/create']
            ],
        ]
    ],
    [
        'label' => Yii::t('VideoModule.video', 'Video items'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VideoModule.video', 'Manage video items'),
                'url'   => ['/video/videoitemBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VideoModule.video', 'Create video item'),
                'url'   => ['/video/videoitemBackend/create']
            ],
            ['label' => Yii::t('VideoModule.video', 'Video item') . ' «' . $model->title . '»'],
            [
                'icon'  => 'fa fa-fw fa-pencil',
                'label' => Yii::t('VideoModule.video', 'Change video item'),
                'url'   => [
                    '/video/videoitemBackend/update',
                    'id' => $model->id
                ]
            ],
            [
                'icon'  => 'fa fa-fw fa-eye',
                'label' => Yii::t('VideoModule.video', 'View video item'),
                'url'   => [
                    '/video/videoitemBackend/view',
                    'id' => $model->id
                ]
            ],
            [
                'icon'        => 'fa fa-fw fa-trash-o',
                'label'       => Yii::t('VideoModule.video', 'Remove video item'),
                'url'         => '#',
                'linkOptions' => [
                    'submit'  => ['/video/videoitemBackend/delete', 'id' => $model->id],
                    'confirm' => Yii::t('VideoModule.video', 'Do you really want to remove video item?'),
                    'params'  => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
                ],
            ],
        ]
    ],
];
?>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>
