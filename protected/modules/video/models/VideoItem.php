<?php
/**
 * Video основная модель для пунктов меню
 */

/**
 * This is the model class for table "video_item".
 *
 * The followings are the available columns in table 'video_item':
 * @property string $id
 * @property string $gallery_id
 * @property string $title
 * @property string $code
 * @property integer $sort
 * @property integer $status

 * The followings are the available model relations:
 * @property Video $video
 */
class VideoItem extends webforma\models\WModel
{
    /**
     * @var bool
     */
    public $regular_link = true;

    /**
     *
     */
    const STATUS_DISABLED = 0;
    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     * Returns the static model of the specified AR class.
     * @param  string $className active record class name.
     * @return VideoItem the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{video_item}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['gallery_id, title, code', 'required', 'except' => 'search'],
            ['sort, status,', 'numerical', 'integerOnly' => true],
            [
                'id, gallery_id, title, code',
                'safe',
                'on' => 'search',
            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'gallery' => [self::BELONGS_TO, 'Video', 'gallery_id'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'sortable' => [
                'class' => 'webforma\components\behaviors\SortableBehavior',
                'attributeName' => 'sort',
            ],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('VideoModule.video', 'Id'),
            'gallery_id' => Yii::t('VideoModule.video', 'Video'),
            'title' => Yii::t('VideoModule.video', 'Title'),
            'code' => 'Код видео',
            'sort' => Yii::t('VideoModule.video', 'Sorting'),
            'status' => Yii::t('VideoModule.video', 'Status'),
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria();

        $criteria->compare('t.id', $this->id, true);
        $criteria->compare('t.gallery_id', $this->gallery_id, true);
        $criteria->compare('t.title', $this->title, true);


        $criteria->compare('t.sort', $this->sort);
        $criteria->compare('t.status', $this->status);
        $criteria->with = ['gallery'];

        return new CActiveDataProvider(
            get_class($this), [
                'criteria' => $criteria,
                'sort' => ['defaultOrder' => 't.sort'],
            ]
        );
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'public' => [
                'condition' => 'status = :status',
                'params' => [
                    ':status' => self::STATUS_ACTIVE,
                ],
            ],
        ];
    }


    /**
     *
     */
    protected function afterSave()
    {
        return parent::afterSave();
    }

    /**
     *
     */
    protected function afterDelete()
    {
        return parent::afterDelete();
    }


    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('VideoModule.video', 'active'),
            self::STATUS_DISABLED => Yii::t('VideoModule.video', 'not active'),
        ];
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return array_key_exists($this->status, $data) ? $data[$this->status] : $this->status;
    }
}
