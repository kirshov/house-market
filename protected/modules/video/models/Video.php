<?php
/**
 * Video основная модель для video
 */
use webforma\widgets\WPurifier;

/**
 * This is the model class for table "video".
 *
 * The followings are the available columns in table 'video':
 * @property string $id
 * @property string $image
 * @property string $name
 * @property string $description
 * @property integer $status
 * @property integer $position
 *
 * The followings are the available model relations:
 * @property VideoItem[] $videoItems
 *
 * @method Video active()
 */
class Video extends webforma\models\WModel
{
    /**
     *
     */
    const STATUS_DISABLED = 0;
    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     * Returns the static model of the specified AR class.
     * @param  string $className active record class name.
     * @return Video   the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{video_gallery}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['name', 'required', 'except' => 'search'],
            //['name, slug', 'required', 'except' => 'search'],
            ['status, position', 'numerical', 'integerOnly' => true],
            ['name', 'filter', 'filter' => [$obj = new WPurifier(), 'purify']],
            ['name, description', 'length', 'max' => 255],
            /*['slug', 'length', 'max' => 100],
            ['slug', 'webforma\components\validators\YSLugValidator'],
            ['slug', 'unique'],*/
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            ['id, name, description, status', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'videoItems' => [self::HAS_MANY, 'VideoItem', 'gallery_id'],
        ];
    }

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'upload' => [
				'class' => 'webforma\components\behaviors\ImageUploadBehavior',
				'attributeName' => 'image',
				'uploadPath' => '/videogallery',
			],
			'sortable' => [
				'class' => 'webforma\components\behaviors\SortableBehavior',
				'attributeName' => 'position',
			],
		];
	}

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('VideoModule.video', 'Id'),
            'name' => Yii::t('VideoModule.video', 'Name'),
            'image' => 'Изображение',
            'description' => Yii::t('VideoModule.video', 'Description'),
            'status' => Yii::t('VideoModule.video', 'Status'),
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria();

        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider(
            get_class($this), [
                'criteria' => $criteria,
		        'sort' => ['defaultOrder' => 't.position'],
            ]
        );
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'active' => [
                'condition' => 'status = :status',
                'params' => [
                    ':status' => self::STATUS_ACTIVE,
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_DISABLED => Yii::t('VideoModule.video', 'not active'),
            self::STATUS_ACTIVE => Yii::t('VideoModule.video', 'active'),
        ];
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return array_key_exists($this->status, $data) ? $data[$this->status] : $this->status;
    }
}
