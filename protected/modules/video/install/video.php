<?php
/**
 * Файл конфигурации модуля
 **/
return [
    'module'    => [
        'class' => 'application.modules.video.VideoModule',
    ],
    'import'    => [],
    'component' => [],
	'params'    => [
		'widgets'   => [
			'video' => 'application.modules.video.widgets.VideoWidget',
		],
	],
    'rules'     => [
	    '/video/' => 'video/video/index/',
	    '/video/<id:\d+>' => '/video/video/view',
    ],
];
