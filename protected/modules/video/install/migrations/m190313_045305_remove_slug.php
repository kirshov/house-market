<?php

class m190313_045305_remove_slug extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->dropColumn('{{video_gallery}}', 'slug');
	}

	public function safeDown()
	{

	}
}