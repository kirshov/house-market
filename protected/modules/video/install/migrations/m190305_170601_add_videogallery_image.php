<?php

class m190305_170601_add_videogallery_image extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{video_gallery}}', 'image', 'varchar(255) default null');
		$this->addColumn('{{video_gallery}}', 'position', 'int(11) default 0');
	}

	public function safeDown()
	{

	}
}