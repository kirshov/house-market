<?php

/**
 * File Doc Comment
 * Video install migration
 * Класс миграций для модуля Video
 **/
class m000000_000000_video_base extends webforma\components\DbMigration
{

    public function safeUp()
    {
        $this->createTable(
            '{{video_gallery}}',
            [
                'id'          => 'pk',
                'name'        => 'varchar(255) NOT NULL',
	            'slug'           => 'varchar(150) NOT NULL',
                'description' => 'varchar(255) NOT NULL',
                'status'      => "integer NOT NULL DEFAULT '1'",
            ],
            $this->getOptions()
        );

        //ix
        $this->createIndex("ix_{{video_gallery}}_status", '{{video_gallery}}', "status", false);
	    $this->createIndex("ux_{{video_gallery}}_slug", '{{video_gallery}}', "slug", true);
        /**
         * video_item:
         **/
        $this->createTable(
            '{{video_item}}',
            [
                'id'               => 'pk',
                'gallery_id'       => 'integer NOT NULL',
                'title'            => 'varchar(150) NOT NULL',
                'code'             => "text DEFAULT NULL",
                'sort'             => "integer NOT NULL DEFAULT '1'",
                'status'           => "integer NOT NULL DEFAULT '1'",
            ],
            $this->getOptions()
        );

        $this->createIndex("ix_{{video_item}}_gallery_id", '{{video_item}}', "gallery_id", false);
        $this->createIndex("ix_{{video_item}}_sort", '{{video_item}}', "sort", false);
        $this->createIndex("ix_{{video_item}}_status", '{{video_item}}', "status", false);

        //fk
        $this->addForeignKey(
            "fk_{{video_item}}_gallery_id",
            '{{video_item}}',
            'gallery_id',
            '{{video_gallery}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{video_item}}');
        $this->dropTableWithForeignKeys('{{video_gallery}}');
    }
}
