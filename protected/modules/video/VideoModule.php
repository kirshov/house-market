<?php

/**
 * VideoModule основной класс модуля video
 */
class VideoModule extends webforma\components\WebModule
{
    /**
     * @var string
     */
    public $defaultController = 'video';

    /**
     * @var string
     */
    public $videoCache = 'video.cache';

	/**
	 * @var int порядок следования модуля в меню панели управления (сортировка)
	 */
	public $adminMenuOrder = 105;

	/**
	 * @var int
	 */
	public $perPage = 20;

	/**
	 * @return array
	 */
	public function getParamsLabels()
	{
		return [
			'perPage' => 'Видеозаписей на страницу',
		];
	}

	/**
	 * @return array
	 */
	public function getEditableParams()
	{
		return [
			'perPage'
		];
	}

	/**
	 * @return array
	 */
	public function getEditableParamsGroups()
	{
		$groups = [
			'general' => [
				'label' => 'Общие настройки',
				'items' => [
					'perPage',
				],
			],
		];

		return $groups;
	}

    /**
     * @return string
     */
    public function getCategory()
    {
        return 'Контент';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('VideoModule.video', 'Video');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('VideoModule.video', 'Video management module');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-video-camera';
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/video/videoBackend/index';
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            ['label' => Yii::t('VideoModule.video', 'Video')],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VideoModule.video', 'Manage video'),
                'url' => ['/video/videoBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VideoModule.video', 'Create video'),
                'url' => ['/video/videoBackend/create'],
            ],
            ['label' => Yii::t('VideoModule.video', 'Video items')],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VideoModule.video', 'Manage video items'),
                'url' => ['/video/videoitemBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VideoModule.video', 'Create video item'),
                'url' => ['/video/videoitemBackend/create'],
            ],
        ];
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'application.modules.video.models.*',
            ]
        );
    }

    /**
     * @return array
     */
    public function getAuthItems()
    {
        return [
            [
                'name' => 'Video.VideoManager',
                'description' => Yii::t('VideoModule.video', 'Manage videos'),
                'type' => AuthItem::TYPE_TASK,
                'items' => [
                    //video
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Video.VideoBackend.Create',
                        'description' => Yii::t('VideoModule.video', 'Creating video'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Video.VideoBackend.Delete',
                        'description' => Yii::t('VideoModule.video', 'Removing video'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Video.VideoBackend.Index',
                        'description' => Yii::t('VideoModule.video', 'List of video'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Video.VideoBackend.Update',
                        'description' => Yii::t('VideoModule.video', 'Editing video'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Video.VideoBackend.View',
                        'description' => Yii::t('VideoModule.video', 'Viewing video'),
                    ],
                    //video items
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Video.VideoitemBackend.Create',
                        'description' => Yii::t('VideoModule.video', 'Creating video item'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Video.VideoitemBackend.Delete',
                        'description' => Yii::t('VideoModule.video', 'Removing video item'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Video.VideoitemBackend.Index',
                        'description' => Yii::t('VideoModule.video', 'List of video items'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Video.VideoitemBackend.Update',
                        'description' => Yii::t('VideoModule.video', 'Editing video items'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Video.VideoitemBackend.View',
                        'description' => Yii::t('VideoModule.video', 'Viewing video items'),
                    ],
                ],
            ],
        ];
    }
}
