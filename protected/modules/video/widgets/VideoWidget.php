<?php
Yii::import('application.modules.video.models.*');

use webforma\widgets\WWidget;

class VideoWidget extends WWidget
{
	/**
	 * @var
	 */
	public $galleryId;

	/**
	 * @var int
	 */
	public $limit = 5;

	/**
	 * @var string
	 */
	public $order = 't.sort';

	/**
	 * @var
	 */
	public $view = 'default';

	public function run()
	{
		$criteria = new CDbCriteria();
		$criteria->compare('status', VideoItem::STATUS_ACTIVE);
		$criteria->limit = $this->limit;

		/*if ($this->galleryId && Yii::app()->hasModule('gallery')) {
			$criteria->with = ['gallery', 'image'];
			$criteria->compare('gallery_id', $this->galleryId);
			$criteria->together = true;
		}*/

		if($this->galleryId){
			$criteria->compare('galleryId', $this->categoryId);
		}

		if($this->order){
			$criteria->order = $this->order;
		}

		$this->render($this->view, [
			'videos' =>  VideoItem::model()->findAll($criteria),
		]);
	}
}
