<?php

/**
 * QueueModule основной класс модуля queue
 */
class QueueModule extends webforma\components\WebModule
{
    /**
     * @var
     */
    public $workerNamesMap;

    /**
     * @return mixed
     */
    public function getWorkerNamesMap()
    {
        return $this->workerNamesMap;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('QueueModule.queue', 'Services');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('QueueModule.queue', 'Tasks');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('QueueModule.queue', 'Tasks creation and management module');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-tasks';
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/queue/queueBackend/index';
    }

    /**
     *
     */
    public function init()
    {
        $this->setImport(
            [
                'application.modules.queue.models.*',
                'application.modules.queue.components.*',
            ]
        );

        parent::init();
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('QueueModule.queue', 'Task list'),
                'url' => ['/queue/queueBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('QueueModule.queue', 'Create task'),
                'url' => ['/queue/queueBackend/create'],
            ],
            [
                'icon' => 'fa fa-fw fa-trash-o',
                'label' => Yii::t('QueueModule.queue', 'Clean queue'),
                'url' => ['/queue/queueBackend/clear'],
            ],
        ];
    }
}
