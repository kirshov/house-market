<?php

/**
 * WQueueInterface интерфейс для всех очередей
 */
interface WQueueInterface
{
    /**
     * @param $worker
     * @param array $task
     * @return mixed
     */
    public function add($worker, array $task);

    /**
     * @param null $worker
     * @return mixed
     */
    public function flush($worker = null);
}
