<?php

/**
 * WQueueMail компонент для отправки почты через очередь
 */
class WQueueMail extends webforma\components\Mail
{
    /**
     * @var string
     */
    public $queueComponent = 'queue';
    /**
     * @var int
     */
    public $queueMailWorkerId = 1;
    /**
     * @var
     */
    private $_queue;
    /**
     * @return mixed
     * @throws Exception
     */
    public function getQueueComponent()
    {
        if ($this->_queue !== null) {
            return $this->_queue;
        } elseif (($id = $this->queueComponent) !== null) {
            if (($this->_queue = Yii::app()->getComponent($id)) instanceof WQueue) {
                return $this->_queue;
            }
        }
        throw new Exception(
            Yii::t(
                'QueueModule.queue',
                'YQuemail.queueComponent contains bad identifier of queue component!'
            )
        );
    }

    /**
     * @param string $from
     * @param array|string $to
     * @param string $theme
     * @param string $body
     * @param bool|false $isText
     * @return mixed
     * @throws Exception
     */
    public function send($from, $to, $theme, $body, $isText = false, $replyTo = [])
    {
        return $this->getQueueComponent()->add(
            $this->queueMailWorkerId,
            [
                'from' => $from,
                'to' => $to,
                'theme' => $theme,
                'body' => $body,
                'replyTo' => $replyTo
            ]
        );
    }
}
