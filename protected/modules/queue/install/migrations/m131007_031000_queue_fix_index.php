<?php

/**
 * Queue fix index migration
 * Класс миграций для модуля Queue
 **/
class m131007_031000_queue_fix_index extends webforma\components\DbMigration
{

    public function safeUp()
    {
        $this->dropIndex("ux_{{queue_queue}}_worker", '{{queue_queue}}');
        $this->dropIndex("ux_{{queue_queue}}_priority", '{{queue_queue}}');

        $this->createIndex("ux_{{queue_queue}}_worker", '{{queue_queue}}', "worker");
        $this->createIndex("ux_{{queue_queue}}_priority", '{{queue_queue}}', "priority");
    }

    public function safeDown()
    {

    }
}
