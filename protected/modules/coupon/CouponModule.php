<?php

use webforma\components\WebModule;

/**
 * Class CouponModule
 */
class CouponModule extends WebModule
{
    /**
     *
     */
    public $adminMenuOrder = 7;

    public function getDependencies()
    {
        return ['order'];
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('CouponModule.coupon', 'Coupons list'),
                'url' => ['/coupon/couponBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('CouponModule.coupon', 'Create coupon'),
                'url' => ['/coupon/couponBackend/create'],
            ],

        ];
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/coupon/couponBackend/index';
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('CouponModule.coupon', 'Store');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('CouponModule.coupon', 'Coupons');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('CouponModule.coupon', 'Store coupon module');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-tags';
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'coupon.models.*',
            ]
        );
    }
}
