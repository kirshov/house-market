<?php
/**
 * @var Coupon $model
 */

$this->breadcrumbs = [
    Yii::t('CouponModule.coupon', 'Coupons') => ['/coupon/couponBackend/index'],
    Yii::t('CouponModule.coupon', 'Manage'),
];

$this->pageTitle = Yii::t('CouponModule.coupon', 'Coupons - manage');

$this->menu = [
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('CouponModule.coupon', 'Manage coupons'),
        'url' => ['/coupon/couponBackend/index'],
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('CouponModule.coupon', 'Create coupon'),
        'url' => ['/coupon/couponBackend/create'],
    ],
];
?>

<?php
$this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'coupon-grid',
        'type' => 'condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => [
            [
                'name' => 'name',
                'type' => 'raw',
                'value' => function (Coupon $data) {
                    return CHtml::link($data->name, ['/coupon/couponBackend/update', 'id' => $data->id]);
                },
            ],
            'code',
            'value',
            [
                'name' => 'type',
                'type' => 'raw',
                'value' => function (Coupon $data) {
                    return CouponType::title($data->type);
                },
            ],
            'start_time',
            'end_time',
            [
                'class' => 'webforma\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('/coupon/couponBackend/inline'),
                'source' => CouponStatus::all(),
                'options' => CouponStatus::colors(),
            ],
            [
                'header' => Yii::t('CouponModule.coupon', 'Orders'),
                'value' => function (Coupon $data) {
                    return CHtml::link(
                        $data->ordersCount,
                        ['/order/orderBackend/index', 'Order[couponId]' => $data->id],
                        ['class' => 'badge']
                    );
                },
                'type' => 'raw',
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
            ],
        ],
    ]
); ?>
