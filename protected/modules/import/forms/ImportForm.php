<?php

class ImportForm extends webforma\models\WFormModel
{
	/**
	 *
	 */
    const UPDATE_TYPE_FULL = 0;

	/**
	 *
	 */
    const UPDATE_TYPE_ADD_NEW_ONLY = 1;

	/**
	 *
	 */
    const UPDATE_TYPE_REMOVE_NOT_EXIST = 2;


	/**
     * @var
     */
    public $file;

	/**
	 * @var
	 */
	protected $type;

	/**
	 * @var
	 */
	public $updateType;

	/**
	 * @var string
	 */
	protected $resultText = '';

	/**
	 * @return array
	 */
	public function rules()
	{
		return [
			['updateType', 'numerical', 'integerOnly' => true],
			['file', 'file']
		];
	}

	/**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'file' => Yii::t('ImportModule.default', 'File'),
	        'updateType' => 'Тип загрузки'
        ];
    }

    /**
     * @return array
     */
    public function attributeDescriptions()
    {
        return [];
    }

	/**
	 * @return array
	 */
    public function updateTypeList(){
		return [
			self::UPDATE_TYPE_FULL => 'Полное обновление',
			self::UPDATE_TYPE_ADD_NEW_ONLY => 'Добавить новые',
			self::UPDATE_TYPE_REMOVE_NOT_EXIST => 'Обработать отсутствующие в файле',
		];
    }

	/**
	 * @param $text
	 */
    public function setResultText($text){
    	$this->resultText = $text;
    }

	/**
	 * @return string
	 */
	public function getResultText(){
		return $this->resultText;
	}
}
