<?php

class ImportXMLForm extends ImportForm
{
    /**
     * @var integer
     */
    public $format;

	/**
	 * @var string
	 */
    protected $type = 'ImportXML';

    /**
     * @var
     */
    public $file;


    /**
     * @return array
     */
    public function rules()
    {
    	return parent::rules();
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
    	return parent::attributeLabels();
    }

    /**
     * @return array
     */
    public function attributeDescriptions()
    {
        return [];
    }
}
