<?php

class ImportExcelForm extends ImportForm
{
    /**
     * @var integer
     */
    public $format;

	/**
	 * @var string
	 */
    protected $type = 'ImportExcel';

    /**
     * @var
     */
    public $file;


    /**
     * @return array
     */
    public function rules()
    {
    	$import = new $this->type();
    	$rules = parent::rules();
        return CMap::mergeArray($rules, [
            ['format', 'required'],
	        ['format', 'in', 'range' => array_keys($import->getAllFormat())],
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
    	$labels = parent::attributeLabels();
        $labels = CMap::mergeArray($labels, [
            'format' => Yii::t('ImportModule.default', 'Format'),
        ]);
        return $labels;
    }

    /**
     * @return array
     */
    public function attributeDescriptions()
    {
        return [];
    }

    /**
     * Обнуляем введённое значение капчи, если оно введено неверно:
     *
     * @param string $attribute - имя атрибута
     * @param mixed $params - параметры
     *
     * @return void
     **/
    public function emptyOnInvalid($attribute, $params)
    {
        if ($this->hasErrors()) {
            $this->verifyCode = null;
        }
    }
}
