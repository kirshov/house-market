<?php

class ImportCsvForm extends ImportExcelForm
{
	/**
	 * @var string
	 */
	protected $type = 'ImportCsv';

	public function attributeDescriptions()
	{
		$descriptions = parent::attributeDescriptions();
		$descriptions['file'] = '(разделитель - ";")';
		return $descriptions;
	}
}
