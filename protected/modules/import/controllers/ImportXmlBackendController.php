<?php

class ImportXmlBackendController extends AllImportBackendController
{

	public function init()
	{
		parent::init();
		$this->modelName = 'ImportXML';

		$this->handler = new ImportHandlerXML();
	}
}
