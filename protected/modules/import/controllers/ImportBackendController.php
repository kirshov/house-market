<?php
/**
 * Class ImportBackendController
 */
class ImportBackendController extends webforma\components\controllers\BackController
{

	public function actionIndex()
	{
		$navigation = $this->getModule()->getNavigation();
		$this->redirect($navigation[0]['url']);
		$this->render('index');
	}
}
