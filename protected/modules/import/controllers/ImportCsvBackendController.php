<?php
/**
 * Class ImportCsvBackendController
 */

use PhpOffice\PhpSpreadsheet\IOFactory;
use \PhpOffice\PhpSpreadsheet\Cell\Coordinate;

class ImportCsvBackendController extends AllImportBackendController
{
	public function init()
	{
		parent::init();
		$this->modelName = 'ImportCsv';

		$this->handler = new ImportHandlerCSV();
	}
}
