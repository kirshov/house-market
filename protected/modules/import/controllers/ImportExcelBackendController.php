<?php
/**
 * Class ImportExcelBackendController
 */

class ImportExcelBackendController extends AllImportBackendController
{

	public function init()
	{
		parent::init();
		$this->modelName = 'ImportExcel';

		$this->handler = new ImportHandlerExcel();
	}
}
