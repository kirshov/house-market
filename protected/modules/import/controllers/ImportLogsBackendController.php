<?php
/**
 * Class ImportLogsBackendController
 */
class ImportLogsBackendController extends webforma\components\controllers\BackController
{

	public function actionIndex()
	{
		$model = new ImportLogs('search');
		$model->unsetAttributes();

		$this->render('index', [
			'model' => $model
		]);
	}

	public function actionView($id){
		$model = ImportLogs::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, Yii::t('ImportModule.default', 'Page not found!'));
		}

		$this->render('view', ['model' => $model]);
	}
}
