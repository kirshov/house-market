<?php
/* @var $model Import */

$this->breadcrumbs = [
    Yii::t('ImportModule.default', 'Products import from XML') => ['/import/importXmlBackend/index'],
    Yii::t('ImportModule.default', 'Creating import format'),
];

$this->pageTitle = Yii::t('ImportModule.default', 'Products import - creating');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('ImportModule.default', 'Import'), 'url' => ['/import/importXmlBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('ImportModule.default', 'Create format'), 'url' => ['/import/importXmlBackend/create']],
];
?>

<?= $this->renderPartial('_form', ['model' => $model, 'dataProvider' => $dataProvider, ]); ?>
