<?php
/* @var $model Import */

$this->breadcrumbs = [
    Yii::t('ImportModule.default', 'Products import from XML') => ['/import/importXmlBackend/index'],
    Yii::t('ImportModule.default', 'Edition import format'),
];

$this->pageTitle = Yii::t('ImportModule.default', 'Products import - edition');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('ImportModule.default', 'Import'), 'url' => ['/import/importXmlBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('ImportModule.default', 'Create format'), 'url' => ['/import/importXmlBackend/create']],
    [
        'icon' => 'fa fa-fw fa-pencil',
        'label' => Yii::t('ImportModule.default', 'Update format'),
        'url' => [
            '/import/importXmlBackend/update',
            'id' => $model->id
        ]
    ],
    [
        'icon' => 'fa fa-fw fa-trash-o',
        'label' => Yii::t('ImportModule.default', 'Delete format'),
        'url' => '#',
        'linkOptions' => [
            'submit' => ['/import/importXmlBackend/delete', 'id' => $model->id],
            'params' => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
            'confirm' => Yii::t('ImportModule.default', 'Do you really want to remove this format?'),
            'csrf' => true,
        ]
    ],
];
?>

<?= $this->renderPartial('_form', ['model' => $model]); ?>
