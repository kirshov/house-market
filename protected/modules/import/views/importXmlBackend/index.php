<?php
/* @var $model Import */
$this->breadcrumbs = [
	'Импорт товаров из XML' => ['/import/importXmlBackend/index'],
    Yii::t('ImportModule.default', 'Manage'),
];

$this->pageTitle = Yii::t('ImportModule.default', 'Products import from Excel').' - '.Yii::t('ImportModule.default', 'manage');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('ImportModule.default', 'Import'), 'url' => ['/import/importXmlBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('ImportModule.default', 'Create format'), 'url' => ['/import/importXmlBackend/create']],
];

$form = $this->beginWidget(
	'bootstrap.widgets.TbActiveForm',
	[
		'id' => 'import-file-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'htmlOptions' => [
			'class' => 'well',
			'enctype' => 'multipart/form-data'
		],
	]
);
?>

<?= $form->errorSummary($importForm); ?>
	<div class="row">
		<div class="col-sm-4">
			<?= $form->dropDownListGroup($importForm, 'updateType', [
				'widgetOptions' => [
					'data' => $importForm->updateTypeList(),
					'append' => $importForm->getAttributeDescription('file'),
				]
			]); ?>

			<div class="hint"><?=$importForm->getAttributeDescription('file')?></div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-4">
			<?= $form->fileFieldGroup($importForm, 'file'); ?>
		</div>
	</div>

<?php $this->widget(
	'bootstrap.widgets.TbButton',
	[
		'buttonType' => 'submit',
		'context' => 'primary',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label' => 'Импорт',
	]
); ?>
<?php $this->endWidget(); ?>
