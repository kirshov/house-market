<?php
/* @var $model Import */
$this->breadcrumbs = [
	'Импорт товаров из CSV' => ['/import/importCsvBackend/index'],
    Yii::t('ImportModule.default', 'Manage'),
];

$this->pageTitle = Yii::t('ImportModule.default', 'Products import from Excel').' - '.Yii::t('ImportModule.default', 'manage');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('ImportModule.default', 'Import'), 'url' => ['/import/importCsvBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('ImportModule.default', 'Create format'), 'url' => ['/import/importCsvBackend/create']],
];

require_once dirname(dirname(__FILE__)).'/importBackend/_doImportForm.php';
?>

<h2><?= Yii::t('ImportModule.default', 'Import formats'); ?></h2>
<?php $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'export-grid',
        'type' => 'condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => [
            [
                'name' => 'id',
            ],
            [
                'name' => 'name',
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::link($data->name, ["/import/importCsvBackend/update", "id" => $data->id]);
                },
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]
); ?>
