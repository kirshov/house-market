<?php
/* @var $model Import */

$this->breadcrumbs = [
    Yii::t('ImportModule.default', 'Products import from CSV') => ['/import/importCsvBackend/index'],
    Yii::t('ImportModule.default', 'Creating import format'),
];

$this->pageTitle = Yii::t('ImportModule.default', 'Products import - creating');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('ImportModule.default', 'Import'), 'url' => ['/import/importCsvBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('ImportModule.default', 'Create format'), 'url' => ['/import/importCsvBackend/create']],
];
?>

<?= $this->renderPartial('_form', ['model' => $model, 'dataProvider' => $dataProvider, ]); ?>
