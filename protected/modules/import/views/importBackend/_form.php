<?php
/**
 * @var $model Import
 * @var $form TbActiveForm
 */

$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'id' => 'export-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => 'vertical',
		'htmlOptions' => ['class' => 'well sticky'],
		'clientOptions' => [
			'validateOnSubmit' => true,
		],
    ]
);
?>
<div class="alert alert-info">
    <?= Yii::t('ImportModule.default', 'Columns with'); ?>
    <span class="required">*</span>
    <?= Yii::t('ImportModule.default', 'are required'); ?>
</div>

<?= $form->errorSummary($model); ?>

<div class='row'>
    <div class="col-sm-6">
        <?= $form->textFieldGroup($model, 'name'); ?>
    </div>
</div>

<?if(Yii::app()->getModule('store')->priceType == StoreModule::PRICE_TYPE_CHARGE):?>
	<div class='row'>
		<div class="col-sm-6">
			<?= $form->dropDownListGroup($model, 'price_type', [
				'widgetOptions' => [
					'data' => [
						'price' => 'Основная',
						'purchase' => 'Закупочная',
					],
				],
			]); ?>
		</div>
	</div>
<?endif;?>

<div class='row'>
	<div class="col-sm-6">
		<?= $form->dropDownListGroup($model, 'provider', [
			'widgetOptions' => [
				'data' => Provider::model()->getFormattedList(),
				'htmlOptions' => [
					'empty' => '---',
				],
			],
		]); ?>
	</div>
</div>
<div class='row'>
	<div class="col-sm-6">
		<?= $form->dropDownListGroup($model, 'missing_product', [
			'widgetOptions' => [
				'data' => $model->getMissingList(),
			],
		]); ?>
	</div>
</div>
<div class='row'>
	<div class="col-sm-6">
		<hr>
	</div>
</div>

<?if($model->getIsNewRecord()):?>
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-success">
				Возможность добавлять поля появится после сохранения формата
			</div>
		</div>
	</div>
<?else:?>
	<div id="column_list">
		<div class="row">
			<div class="col-md-3">
				<strong>Добавить столбец</strong>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<?= CHtml::dropDownList('Column[name]', null, Column::getColumnNames(), ['class' => 'form-control']); ?>
			</div>
			<div class="col-md-3">
				<?= CHtml::button(Yii::t('ImportModule.default', 'Add'), [
					'class' => 'btn btn-success',
					'style' => 'height: 31px;line-height: 18px;',
					'id' => 'add-column-btn',
					'data-url' => Yii::app()->createUrl('/import/importExcelBackend/addColumn', ['id' => $model->id]),
				]); ?>
			</div>
		</div>
		<br/>
		<div class="row">
			<div class="col-md-6">
				<?php $this->widget(
					'webforma\widgets\CustomGridView',
					[
						'hideBulkActions' => true,
						'id' => 'columns-grid',
						'enableSorting' => false,
						'sortableRows' => true,
						'sortableAjaxSave' => true,
						'sortableAttribute' => 'position',
						'sortableAction' => '/import/importExcelBackend/sortcolumns',
						'type' => 'condensed',
						'ajaxUrl' => Yii::app()->createUrl('/import/importExcelBackend/update', ['id' => $model->id]),
						'template' => "{items}\n{pager}<br/><br/>",
						'dataProvider' => new CActiveDataProvider('Column',
							[
								'criteria' => [
									'condition' => 'import_id = :id',
									'params' => [':id' => $model->id],
								],
								'pagination' => false,
								'sort' => [
									'defaultOrder' => 'position ASC',
								],
							]
						),
						'columns' => [
							[
								'header' => '№',
								'value' => '$row+1',
							],
							[
								'name' => 'column',
								'value' => function($data){
									return $data->getColumnName();
								},
							],
							[
								'class' => 'webforma\widgets\CustomButtonColumn',
								'template' => '{delete}',
								'buttons' => [
									'delete' => [
										'url' => function ($data) {
											return Yii::app()->createUrl('/import/importExcelBackend/deleteColumn',
												['id' => $data->id]);
										},
										'options' => [
											'class' => 'delete btn-sm btn-default',
										],
									],
								],
							],
						],
					]
				) ?>
			</div>
		</div>
	</div>
<?endif;?>

<div class="buttons">
	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context' => 'primary',
		'label' => $model->getIsNewRecord()
			? Yii::t('ImportModule.default', 'Add format and continue')
			: Yii::t('ImportModule.default', 'Save format and continue'),
	]); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label' => $model->getIsNewRecord()
			? Yii::t('ImportModule.default', 'Add format and close')
			: Yii::t('ImportModule.default', 'Save format and close'),
	]); ?>
</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(function () {
		$('#add-column-btn').on('click', function (event) {
			event.preventDefault();
			var data = $('#Column_name').val();
			if (data) {
				$.post($(this).data('url'), {
					'value': data,
					'<?= Yii::app()->getRequest()->csrfTokenName;?>': '<?= Yii::app()->getRequest()->getCsrfToken()?>'
				}, function (response) {
					$.fn.yiiGridView.update('columns-grid');
				}, 'json');
			}
		});
	});
</script>
<style>
	.checkbox-column{display: none;}
</style>