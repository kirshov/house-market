<?php
$form = $this->beginWidget(
	'bootstrap.widgets.TbActiveForm',
	[
		'id' => 'import-file-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'htmlOptions' => [
			'class' => 'well',
			'enctype' => 'multipart/form-data'
		],
	]
);
?>

<?= $form->errorSummary($importForm); ?>

	<div class="row">
		<div class="col-sm-4">
			<?= $form->dropDownListGroup($importForm, 'format', [
				'widgetOptions' => [
					'data' => $model->getAllFormat(),
				]
			]); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-4">
			<?= $form->dropDownListGroup($importForm, 'updateType', [
				'widgetOptions' => [
					'data' => $importForm->updateTypeList(),
					'append' => $importForm->getAttributeDescription('file'),
				]
			]); ?>

			<div class="hint"><?=$importForm->getAttributeDescription('file')?></div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-4">
			<?= $form->fileFieldGroup($importForm, 'file'); ?>
		</div>
	</div>

<?php $this->widget(
	'bootstrap.widgets.TbButton',
	[
		'buttonType' => 'submit',
		'context' => 'primary',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label' => 'Импорт',
	]
); ?>
<?php $this->endWidget(); ?>