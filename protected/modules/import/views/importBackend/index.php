<?php
/* @var $model Import */
$this->breadcrumbs = [
    Yii::t('ImportModule.default', 'Products import') => ['/import/importBackend/index'],
    Yii::t('ImportModule.default', 'Manage'),
];

$this->pageTitle = Yii::t('ImportModule.default', 'Products import - manage');