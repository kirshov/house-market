<?php
$this->breadcrumbs = [
	Yii::t('ImportModule.default', 'Products import') => ['/import/importBackend/index'],
	'Журнал импорта',
];

$this->pageTitle = 'Журнал импорта';

$this->menu = [];
?>
<?php $this->widget(
	'bootstrap.widgets.TbDetailView',
	[
		'data' => $model,
		'attributes' => [
			'id',
			[
				'name' => 'create_time',
				'value' => Yii::app()->getDateFormatter()->formatDateTime($model->date, "short", "short"),
			],
			[
				'name' => 'type',
				'value' => function($model) {
					return $model->getType();
				},
			],
			[
				'name' => 'result',
				'type' => 'raw',
				'value' => function($model){
					$htmlOptions = $model->result ? ['class' => 'label label-success'] : ['class' => 'label label-warning'];
					return CHtml::tag('span', $htmlOptions, $model->result ? 'Да' : 'Нет');
				}
			],
			[
				'name' => 'message',
				'type' => 'raw',
				'value' => function($model){
					$data = json_decode($model->message, true);
					$text = '';
					$titles = ImportHandler::getStatsKeys();
					if($data){
						foreach ($data as $title => $item) {
							if(!$item){
								continue;
							}
							$text .= $titles[$title].': '.$item.'<br/>';
						}
					}
					return $text;
				},
			]
		],
	]
);