<?php
/* @var $model Import */
$this->breadcrumbs = [
	Yii::t('ImportModule.default', 'Products import') => ['/import/importBackend/index'],
	'Журнал импорта',
];

$this->pageTitle = 'Журнал импорта';

$this->menu = [];
$importTypes = Import::model()->getImportTypes();
?>

<h2><?= Yii::t('ImportModule.default', 'Import formats'); ?></h2>
<?php $this->widget(
	'webforma\widgets\CustomGridView',
	[
		'id' => 'export-grid',
		'type' => 'condensed',
		'dataProvider' => $model->search(),
		'filter' => $model,
		'columns' => [
			[
				'name' => 'date',
				'type' => 'raw',
				'value' => function ($data) {
					return CHtml::link(date('d.m.Y H:i:s', strtotime($data->date)), ["/import/importLogsBackend/view", "id" => $data->id]);
				},
			],
			[
				'name' => 'type',
				'type' => 'raw',
				'value' => function ($data) {
					return $data->getType();
				},
				'filter' => CHtml::activeDropDownList($model, 'result', $model->getImportTypes(), [
					'encode' => false,
					'empty' => '',
					'class' => 'form-control'
				]),
				'htmlOptions' => [
					'style' => 'width:150px;',
				],
			],
			[
				'name' => 'result',
				'type' => 'raw',
				'value' => function ($data) {
					$htmlOptions = $data->result ? ['class' => 'label label-success'] : ['class' => 'label label-warning'];
					return CHtml::tag('span', $htmlOptions, $data->result ? 'Да' : 'Нет');
				},
				'filter' => CHtml::activeDropDownList($model, 'result', [0 => 'Нет', 1 => 'Да'], [
					'encode' => false,
					'empty' => '',
					'class' => 'form-control'
				]),
				'htmlOptions' => [
					'style' => 'width:80px;',
				],
			],
			[
				'name' => 'message',
				'type' => 'raw',
				'value' => function($model){
					$data = json_decode($model->message, true);
					$text = '';
					$titles = ImportHandler::getStatsKeys();
					if($data){
						foreach ($data as $title => $item) {
							if(!$item){
								continue;
							}
							$text .= $titles[$title].': '.$item.'<br/>';
						}
					}
					return $text;
				},
				'htmlOptions' => [
					'style' => 'max-width:150px;',
				],
				'filter' => false,
			],
			[
				'class' => 'webforma\widgets\CustomButtonColumn',
				'template' => '{view} {delete}',
			],
		],
	]
); ?>
