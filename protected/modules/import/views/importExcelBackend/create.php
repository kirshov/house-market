<?php
/* @var $model Import */

$this->breadcrumbs = [
    Yii::t('ImportModule.default', 'Products import from Excel') => ['/import/importExcelBackend/index'],
    Yii::t('ImportModule.default', 'Creating import format'),
];

$this->pageTitle = Yii::t('ImportModule.default', 'Products import from Excel');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('ImportModule.default', 'Import'), 'url' => ['/import/importExcelBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('ImportModule.default', 'Create format'), 'url' => ['/import/importExcelBackend/create']],
];
?>

<?= $this->renderPartial('_form', ['model' => $model, 'dataProvider' => $dataProvider, ]); ?>
