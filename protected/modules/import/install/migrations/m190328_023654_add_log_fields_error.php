<?php

class m190328_023654_add_log_fields_error extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_import_logs}}', 'errors', 'text default null');
	}

	public function safeDown()
	{

	}
}