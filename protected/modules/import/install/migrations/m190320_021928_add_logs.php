<?php

class m190320_021928_add_logs extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->createTable(
			'{{store_import_logs}}',
			[
				'id' => 'pk',
				'type' => 'int(3) default 0',
				'date' => 'datetime',
				'result' => 'int(3) default 0',
				'message' => 'text default null',
				'has_errors' => 'int(1) default 0',
			],
			$this->getOptions()
		);
	}

	public function safeDown()
	{

	}
}