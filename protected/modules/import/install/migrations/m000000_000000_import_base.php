<?php

class m000000_000000_import_base extends webforma\components\DbMigration
{
    public function safeUp()
    {
        $this->createTable(
            "{{store_import_settings}}",
            [
                "id" => "pk",
                "type" => "int(3) default 0",
                "name" => "varchar(255) not null",
                "settings" => "text null default null",
            ],
            $this->getOptions()
        );

	    $this->createTable(
		    "{{store_import_columns}}",
		    [
			    "id" => "pk",
			    "import_id" => "int(11) not null default 0",
			    "column" => "varchar(32) not null default ''",
			    "position" => "int(11) not null default 0",
		    ],
		    $this->getOptions()
	    );

	    $this->createIndex("ix_{{store_import_columns}}_import_id", '{{store_import_columns}}', "import_id", false);

	    //fk
	    $this->addForeignKey(
		    "fk_{{store_import_columns}}_import_id",
		    '{{store_import_columns}}',
		    'import_id',
		    '{{store_import_settings}}',
		    'id',
		    'CASCADE',
		    'NO ACTION'
	    );
    }

    public function safeDown()
    {
        $this->dropTable("{{store_import_settings}}");
        $this->dropTable("{{store_import_columns}}");
    }
}
