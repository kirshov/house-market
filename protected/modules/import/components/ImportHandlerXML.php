<?php

class ImportHandlerXML extends ImportHandler
{
	protected $catsIds = [];

	/**
	 * @param $file
	 * @return bool|mixed
	 * @throws Exception
	 */
	public function run($file)
	{
		$dataPosToUpdate = [];
		$categories = [];
		$categoriesParents = [];

		$namesArray = [
			'price',
			'category' => 'categoryId',
			'producer' => 'vendor',
			'name',
			'description',
			'characteristic' => 'param',
			//'images' => 'picture',
			'sku' => 'vendorCode',
		];

		$doc = new DOMDocument();

		$reader = new XMLReader();
		$reader->open($file);
		while($reader->read()) {
			if($reader->nodeType == XMLReader::ELEMENT) {
				if($reader->localName == 'category') {
					$data = [];
					foreach (['external_id' => 'id', 'parent_id' => 'parentId'] as $_key => $_xKey) {
						$_val = $reader->getAttribute($_xKey);
						if($_val){
							$data[$_key] = $_val;
						}
					}

					$reader->read();
					if($reader->nodeType == XMLReader::TEXT) {
						$data['name'] = $reader->value;
					}

					//$parentId = isset($this->categoriesById[$data['parent_id']]) ? $this->categoriesById[$data['parent_id']] : null;
					//$this->getCatIdByName($parentId, $data['name']);
					$categories[$data['external_id']] = $data;
					$categoriesParents[(int) $data['parent_id']][] = $data['external_id'];
				}

				if($reader->localName == 'offer') {
					$data = [];

					$offer = simplexml_import_dom($doc->importNode($reader->expand(), true));

					foreach ($namesArray as $_key => $_xKey) {
						if(is_numeric($_key)){
							$_key = $_xKey;
						}

						/*if($_xKey == 'param'){
							foreach ($offer->param as $_param){
								$data[$_key][(string)$_param['name']] = (string)$_param;
							}

							continue;
						}*/

						if($_xKey == 'picture'){
							foreach ($offer->picture as $_param){
								if($_param['main'] == 'true'){
									$data[$_key]['main'] = (string)$_param;
								} else {
									$data[$_key][] = (string)$_param;
								}
							}

							continue;
						}
						if($_xKey == 'price'){
							$price = (string) $offer->{$_xKey};
							$price = str_replace(',', '.', $price);
							$price = preg_replace('/[^0-9\.]/', '', $price);
							$data[$_key] = $price;
							continue;
						}

						$data[$_key] = (string) $offer->{$_xKey};
					}
					if(!$data['sku']){
						$data['sku'] = $reader->getAttribute('id');
					}

					$dataPosToUpdate[$data['sku']] = $data;
				}
			}
		}
		$reader->close();

		if(!empty($categoriesParents[0])){
			$this->updateCategoriesTree(null, $categoriesParents[0], $categoriesParents, $categories);
		}

		if($dataPosToUpdate){
			foreach ($dataPosToUpdate as $item){
				$item['category_id'] = $this->catsIds[$item['category']];
				unset($item['category']);
				$this->updateProductsHandler($item);
			}
		}
		$this->prepareMissingPoses();

		return true;
	}

	/**
	 * @param array $data
	 * @param int $categoryId
	 * @return array
	 */
	public function prepareAttribute($data, $categoryId)
	{
		$result = [];
		foreach ($data as $key => $value){
			$characteristicName = $this->fields['characteristic'][$key];
			if(!$characteristicName){
				continue;
			}
			$attributeId = $this->getAttributeId($characteristicName, $categoryId);
			if(!$attributeId){
				continue;
			}

			$optionId = $this->getAttributeOptionId($attributeId, $value);
			if(!$optionId){
				continue;
			}

			$result[] = [
				'name' => $characteristicName,
				'value' => $value,
				'attribute_id' => $attributeId,
				'option_id' => $optionId
			];
		}
		return $result;
	}

	/**
	 * @param $id
	 */
	public function actionUpdate($id)
	{
		parent::actionUpdate($id);
	}

	/**
	 * @param $parentId
	 * @param $items
	 * @param $categoriesParents
	 * @param $categories
	 * @throws Exception
	 */
	protected function updateCategoriesTree($parentId, $items, $categoriesParents, $categories){
		foreach ($items as $item){
			$category = $categories[$item];
			$subParentId = $this->getCatIdByName($parentId, $category['name']);
			$this->catsIds[$item] = $subParentId;
			if($subParentId && !empty($categoriesParents[$item])){
				$this->updateCategoriesTree($subParentId, $categoriesParents[$item], $categoriesParents, $categories);
			}
		}
	}
}