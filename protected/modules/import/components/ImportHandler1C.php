<?php
class ImportHandler1C extends ImportHandler
{
	/**
	 * @var int
	 */
	protected $fieldRow = 1;

	/**
	 * @var array
	 */
	protected $__cats = [];

	/**
	 * @var array
	 */
	protected $__catsParents = [];

	/**
	 * @var array
	 */
	protected $__products = [];

	/**
	 * @var string
	 */
	protected $logType = 'file';

	/**
	 * @var string
	 */
	protected $productSystemField = 'external_id';

	/**
	 * @param $file
	 * @return bool|mixed
	 * @throws CException
	 */
	public function run($file)
	{
		Yii::import('system.web.CUrlManager', true);
		Yii::import('application.modules.store.components.StoreUrlRule');

		ini_set('auto_detect_line_endings', 1);

		$this->forceSetProductType = true;

		$handle = @fopen($file, 'r');
		if ($handle) {
			$lineNum = 1;
			while (($buffer = fgets($handle, 1024 * 1024)) !== false) {
				$this->parseLine($buffer, $lineNum);
				$lineNum++;
			}

			fclose($handle);
		}

		if($this->__cats){
			$this->updateCats(null, 0);
		}

		if(!empty($this->__products)){
			$this->updateProducts();

			$this->updateLinkedPositions();
		}

		Yii::app()->eventManager->fire(StoreEvents::STORE_CHANGE, new \webforma\components\Event($this));

		$this->prepareMissingPoses();

		return true;
	}

	/**
	 * @param $parentId
	 * @param $systemParent
	 * @return bool
	 * @throws Exception
	 */
	protected function updateCats($parentId, $systemParent){
		if(!isset($this->__catsParents[$systemParent])){
			return false;
		}
		foreach ($this->__catsParents[$systemParent] as $key){
			$item = $this->__cats[$key];

			$catId = $this->getCatIdByName($parentId, $item['title'], $item);
			if($catId){
				$this->updateCats($catId, $key);
			}
		}
	}

	/**
	 * @throws Exception
	 */
	public function updateProducts(){
		foreach ($this->__products as $productData){
			$productExternalId = '';
			foreach ($productData as $key => $product){
				$product['characteristic'] = $this->parseAttributes($product['characteristic']);
				if($product['char']){
					$categoryId = $this->getCategoryId($product['category']);
					$rootCategoryId = $this->getRootCat($categoryId);

					$category = $this->categoriesById[$rootCategoryId];
					$name = $category['name'];

					$product['characteristic']['Вариант - '.$name] = $product['char'];
				}
				if($key == 0){
					$this->updateProductsHandler($product, false);
					$productExternalId = $product[$this->productSystemField];
				} else {
					//$product['external_id'] = $product['external_id'].($product['code'] ? '-'.$product['code'] : '');
					$product['external_id'] = $product['external_id'].($product['char'] ? '-'.$this->prepareSlug($product['char']) : '');

					$product['product_external_id'] = $productExternalId;
					$this->updateVariantHandler($product);
				}
			}
		}
	}

	/**
	 * @param $str
	 * @param $lineNum
	 */
	public function parseLine($str, $lineNum){
		if(!$str){
			return;
		}
		$data = explode("\t", $str);

		if(sizeof($data) < 5){
			return;
		}

		$parentId = $data[1] ?: 0;
		if($data[3] == '1'){
			$this->__cats[$data[0]] = $this->convertArrayCharset([
				'external_id' => $data[0],
				'parentId' => $parentId,
				'title' => $data[2],
				'line_num' => $lineNum,
			]);

			$this->__catsParents[$parentId][] = $data[0];
		} elseif($data[3] === '0') {
			$quantity = explode('|', $data[7]);
			if($data[21]){
				$similarItems = explode(',', trim($data[21], ','));
			} else {
				$similarItems = false;
			}


			$this->__products[$data[0]][] = $this->convertArrayCharset([
				'external_id' => $data[0],
				'category' => $data[1],
				'name' => $data[2],
				'sku' => $data[5],
				'char' => $data[6],
				'quantity' => (int) $quantity[0],
				'in_stock' => (int) $quantity[3] > 0 ? 1 : 0,
				'price' => $data[9],
				'discount_price' => $data[10],
				'code' => $data[11],
				'image' => $data[12],
				'short_description' => $data[13],
				'pod_zakaz' => (int) $data[14],
				'producer' => $data[15],
				'images' => $data[16] ? explode(',', $data[16]) : [],
				'description' => $data[17],
				'technology' => $data[18],
				'geometry' => $data[19],
				'shablon' => '', //20
				'similar' => $similarItems,
				'is_hit' => (int) $data[22],
				'characteristic' => $data[23],
				'line_num' => $lineNum,
			]);
		}
	}

	/**
	 * @param $data
	 * @return array
	 */
	public function convertArrayCharset($data){
		if(empty($data) || !is_array($data)){
			return $data;
		}
		foreach($data as $key => $value){
			if(is_array($value)){
				$data[$key] = self::convertArrayCharset($value);
			}
			else{
				$data[$key] = iconv('windows-1251', 'utf-8', $value);
			}
		}
		return $data;
	}

	/**
	 * @param $data
	 * @param $categoryId
	 * @return array
	 */
	public function prepareAttribute($data, $categoryId)
	{
		$result = [];
		foreach ($data as $key => $value){
			$characteristicName = $key;
			if(!$characteristicName){
				continue;
			}
			$attributeId = $this->getAttributeId($characteristicName, $categoryId);
			if(!$attributeId){
				continue;
			}

			$optionId = $this->getAttributeOptionId($attributeId, $value);
			if(!$optionId){
				continue;
			}

			$result[] = [
				'name' => $characteristicName,
				'value' => $value,
				'attribute_id' => $attributeId,
				'option_id' => $optionId
			];
		}
		return $result;
	}

	/**
	 * @param $id
	 */
	public function actionUpdate($id)
	{
		parent::actionUpdate($id);
	}

	/**
	 * @param $category
	 * @return int
	 * @throws Exception
	 */
	protected function getCategoryId($category){
		$category = trim($category);

		$id = $this->getCategoryIdBySystemName($category);
		if(!$id){
			$id = $this->getUnsortedCatId();
		}

		return $id;
	}

	/**
	 * @param $str
	 * @return array|bool
	 */
	protected function parseAttributes($str){
		if(!$str){
			return false;
		}
		$items = explode(';', $str);
		if(empty($items)){
			return false;
		}
		$data = [];
		foreach ($items as $item){
			$values = explode('|', $item);
			if(!empty($values[1]) && !empty($values[3])){
				$data[$values[1]] = $values[3];
			}
		}

		return $data;
	}

	/**
	 * @param $categoryId
	 * @return int
	 */
	protected function getProductType($categoryId){
		if($this->productType !== null){
			return $this->productType;
		} else {

			$rootCategoryId = $this->getRootCat($categoryId);

			$category = $this->categoriesById[$rootCategoryId];
			$name = $category['name'];

			if(!empty($this->productTypes[$name])){
				return $this->productTypes[$name];
			} else {
				return $this->createProductType($name);
			}
		}
	}

	/**
	 * @param $categoryId
	 * @return mixed
	 */
	protected function getRootCat($categoryId){
		$category = $this->categoriesById[$categoryId];

		if($category && is_numeric($category['parent_id'])){
			return $this->getRootCat($this->categoriesById[$category['parent_id']]['id']);
		} else {
			return $categoryId;
		}
	}

	/**
	 * @param $product
	 * @param $data
	 * @return mixed
	 */
	protected function updateProductAdvancedFields($product, $data)
	{
		foreach (['pod_zakaz', 'technology', 'geometry'] as $item){
			if(!empty($data[$item])){
				$product[$item] = $data[$item];
			}
		}
		return $product;
	}

	/**
	 *
	 */
	protected function updateLinkedPositions()
	{
		if(!$this->linkedPositions){
			return false;
		}

		$codesAll = $products = $currentLinked = [];
		foreach ($this->linkedPositions as $from => $list){
			$codesAll = CMap::mergeArray($codesAll, $list);
		}

		$productsList = Yii::app()->db->createCommand([
			'select' => 'id, code',
			'from' => '{{store_product}}',
			'where' => 'code IN ('.implode(', ', $codesAll).')'
		])->queryAll();

		if(!empty($productsList)){
			foreach ($productsList as $item){
				$products[$item['code']] = $item['id'];
			}
		}

		$currentLinkedList = Yii::app()->db->createCommand([
			'select' => 'product_id, linked_product_id',
			'from' => '{{store_product_link}}',
			'where' => 'type_id = '. (int) $this->linkedType,
		])->queryAll();

		if(!empty($currentLinkedList)){
			foreach ($currentLinkedList as $item){
				$currentLinked[$item['product_id']][$item['linked_product_id']] = $item['linked_product_id'];
			}
		}

		foreach ($this->linkedPositions as $productId => $codes){
			foreach ($codes as $code){
				$itemProductId = $products[$code];
				if(isset($currentLinked[$productId][$itemProductId])){
					unset($currentLinked[$productId][$itemProductId]);
					continue;
				} else {
					$model = new ProductLink();
					$model->type_id = $this->linkedType;
					$model->product_id = $productId;
					$model->linked_product_id = $itemProductId;
					$model->save();
					unset($currentLinked[$productId][$itemProductId]);
				}
			}
		}


		$command = Yii::app()->db->createCommand();
		foreach ($currentLinked as $productId => $items){
			if(!empty($items)){
				$command->delete('{{store_product_link}}', 'product_id = '.$productId.' AND linked_product_id IN ('.implode(', ', $items).')');
			}
		}
	}
}