<?php

ini_set('max_execution_time', 0);
/**
 * Class ImportBackendController
 */
class AllImportBackendController extends webforma\components\controllers\BackController
{

	/**
	 * @var string
	 */
	protected $modelName = 'Import';

	/**
	 * @var ImportHandler
	 */
	protected $handler;

	/**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin'],],
            ['allow', 'actions' => ['index'], 'roles' => ['ImportModule.ImportBackend.Index'],],
            ['allow', 'actions' => ['create'], 'roles' => ['ImportModule.ImportBackend.Create'],],
            ['allow', 'actions' => ['update', 'addColumn', 'deleteColumn', 'inline', 'inlinecolumns', 'sortcolumns'], 'roles' => ['ImportModule.ImportBackend.Update'],],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['ImportModule.ImportBackend.Delete'],],
            ['deny',],
        ];
    }

	/**
	 * @return array
	 */
	public function actions()
	{
		return [
			'inline' => [
				'class' => 'webforma\components\actions\WInLineEditAction',
				'model' => $this->modelName,
				'validAttributes' => ['name'],
			],
			'inlinecolumns' => [
				'class' => 'webforma\components\actions\WInLineEditAction',
				'model' => 'Column',
				'validAttributes' => ['Column'],
			],
			'sortcolumns' => [
				'class' => 'webforma\components\actions\SortAction',
				'model' => 'Column',
				'attribute' => 'position',
			],
		];
	}

    /**
     *
     */
    public function actionCreate()
    {
        $model = new $this->modelName();


        if (($data = Yii::app()->getRequest()->getPost($this->modelName)) !== null) {

            $model->setAttributes($data);

            if ($model->save()) {

                Yii::app()->getUser()->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('WebformaModule.webforma', 'Record was created!')
                );

                $this->redirect((array)Yii::app()->getRequest()->getPost('submit-type', ['update', 'id' => $model->id,]));
            }
        }

        $this->render('create', [
        	'model' => $model,
        ]);
    }


    /**
     * @param $id
     * @throws CHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        if (($data = Yii::app()->getRequest()->getPost($this->modelName)) !== null) {

            $model->setAttributes($data);

            if ($model->save()) {

                Yii::app()->getUser()->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
	                'Запись успешно сохранена!'
                );

                $this->redirect(
	                (array) Yii::app()->getRequest()->getPost('submit-type', ['update', 'id' => $model->id,])
                );
            }
        }

        $this->render('update', [
        	'model' => $model,
        ]);
    }


    /**
     * @param $id
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $this->loadModel($id)->delete();
        }
    }


    /**
     *
     */
    public function actionIndex()
    {
    	$importFormName = $this->modelName.'Form';
	    /**
	     * @var ImportForm $importForm
	     */
    	$importForm = new $importFormName();
    	if($post = Yii::app()->getRequest()->getPost($importFormName)){
    		$importForm->setAttributes($post);

    		if($importForm->validate()){
			    $importForm = $this->doImport($importForm);
			    if(!$importForm->hasErrors()){
			    	$resultMessage = 'Позиции успешно загружены';
			    	if($importForm->getResultText()){
			    		$resultMessage .= CHtml::tag('div', ['class' => 'panel'], CHtml::tag('div', ['class' => 'panel-body'], $importForm->getResultText()));
				    }
				    Yii::app()->getUser()->setFlash(webforma\widgets\WFlashMessages::SUCCESS_MESSAGE, $resultMessage);
			    }
		    }
	    }
        $model = new $this->modelName('search');
        $model->unsetAttributes();

        if (isset($_GET[$this->modelName])) {
            $model->attributes = $_GET[$this->modelName];
        }

        $this->render('index', ['model' => $model, 'importForm' => $importForm]);
    }

    /**
     * @param $id
     * @return Import
     * @throws CHttpException
     */
    public function loadModel($id)
    {
    	$modelName = $this->modelName;
        $model = $modelName::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('ImportModule.default', 'Page not found!'));
        }

        return $model;
    }

	/**
	 * @param $id
	 * @throws CHttpException
	 */
	public function actionAddColumn($id){
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$model = $this->loadModel($id);

			$column = new Column();
			$column->setAttributes([
				'column' => Yii::app()->getRequest()->getPost('value'),
				'import_id' => $model->id
			]);

			if (true === $column->save()) {
				Yii::app()->ajax->success();
			}

			Yii::app()->ajax->failure();
		} else {
			throw new CHttpException(
				400,
				Yii::t('WebformaModule.store', 'Bad request. Please don\'t use similar requests anymore')
			);
		}
	}

	/**
	 * @param $id
	 * @throws CDbException
	 * @throws CHttpException
	 */
	public function actionDeleteColumn($id)
	{
		if (Yii::app()->getRequest()->getIsPostRequest()) {

			$column = Column::model()->findByPk($id);

			if (null === $column) {
				throw new CHttpException(404);
			}

			$column->delete();

			if (!isset($_GET['ajax'])) {
				$this->redirect(
					(array)Yii::app()->getRequest()->getPost('returnUrl', 'index')
				);
			}

		} else {
			throw new CHttpException(
				400,
				Yii::t('WebformaModule.store', 'Bad request. Please don\'t use similar requests anymore')
			);
		}
	}

	/**
	 * @param ImportForm $form
	 * @return mixed
	 */
	protected function doImport($form){
		$post = Yii::app()->getRequest()->getPost(get_class($form));
		$importModel = Import::model()->findByPk($post['format']);

		$this->handler->updateType = $form->updateType;
		$this->handler->settings = $importModel->settings;
		$this->handler->format = $importModel->id;

		$this->handler->init();

		$file = CUploadedFile::getInstance($form, 'file');

		$this->handler->run($file->getTempName());
		if($this->handler->hasErrors()){
			$form->addErrors($this->handler->getErrors());
		} else {
			$form->setResultText($this->handler->getStatText());
			$statModel = new ImportLogs();
			$statModel->setAttributes([
				'type' => $importModel->type,
				'result' => 1,
				'has_errors' => 0,
				'message' => json_encode($this->handler->getStats()),
			]);
			$statModel->save();
		}

		return $form;
	}
}
