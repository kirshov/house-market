<?php
class ImportHandlerCSV extends ImportHandler
{
	/**
	 * @var int
	 */
	protected $fieldRow = 1;

	/**
	 * @param $file
	 * @return bool|mixed
	 * @throws Exception
	 */
	public function run($file)
	{
		$columnsData = Column::model()->findAll([
			'condition' => 'import_id = :import_id',
			'params' => [
				':import_id' => $this->format,
			],
			'order' => 'position'
		]);

		if(!$columnsData){
			$this->addError('format', 'В настройках формата выгрзуки не определены столбцы');
			return false;
		}

		$columns = [];
		$k = 1;
		$isSku = false;
		foreach ($columnsData as $item){
			$columns[$k] = $item->column;
			if($item->column == 'sku'){
				$isSku = true;
			}
			$k++;
		}
		if(!$isSku){
			$this->addError('format', 'Формат выгрузки не содержит поля "Артикул"');
			return false;
		}

		//читаем файл
		$row = 0;
		$skipRows = $this->skipRows;
		$k = 1;
		$dataToUpdate = [];
		if (($handle = fopen($file, 'r')) !== false) {
			while (($read = fgetcsv($handle, 1000, ';')) !== false) {
				$countColumns = sizeof($read);
				if($row == 0 && $countColumns != sizeof($columns)){
					$this->addError('file', 'Количество столбцов в файле и в формате не совпадает');
					return false;
				}

				$row++;
				$data = [];
				$fullEmpty = true;
				foreach ($columns as $keyId => $keyValue){
					$columnIndex = $keyId - 1;
					if($keyValue == 'skip'){
						continue;
					}

					$_value = (string) trim($read[$columnIndex]);
					$_value = ltrim($_value);
					if($keyValue == 'price' && $_value === ''){
						continue;
					}
					if($keyValue == 'characteristic'){
						$data[$keyValue][$keyId] = $_value;
					} else {
						$data[$keyValue] = $_value;
					}

					if($read[$columnIndex] !== null){
						$fullEmpty = false;
					}
				}

				if(!$fullEmpty && $data['sku'] && strlen($data['sku']) < self::MAX_LENGTH_SKU){
					if($k == $this->fieldRow){
						$this->fields = $data;
					}
					if(!$skipRows){
						$dataToUpdate[$data['sku']] = $data;
					} else {
						$skipRows--;
					}

				}
				$k++;
			}
			fclose($handle);
		}

		if(!$row){
			$this->addError('file', 'Файл не содержит записей');
			return false;
		}

		if($dataToUpdate){
			foreach ($dataToUpdate as $data){
				$this->updateProductsHandler($data);
			}
		}
		$this->prepareMissingPoses();

		return true;
	}

	/**
	 * @param array $data
	 * @param int $categoryId
	 * @return array
	 */
	public function prepareAttribute($data, $categoryId)
	{
		$result = [];
		foreach ($data as $key => $value){
			$characteristicName = $this->fields['characteristic'][$key];
			if(!$characteristicName){
				continue;
			}
			$attributeId = $this->getAttributeId($characteristicName, $categoryId);
			if(!$attributeId){
				continue;
			}

			$optionId = $this->getAttributeOptionId($attributeId, $value);
			if(!$optionId){
				continue;
			}

			$result[] = [
				'name' => $characteristicName,
				'value' => $value,
				'attribute_id' => $attributeId,
				'option_id' => $optionId
			];
		}
		return $result;
	}

	/**
	 * @param $id
	 */
	public function actionUpdate($id)
	{
		parent::actionUpdate($id);
	}

	/**
	 * @param $category
	 * @return int|void|null
	 * @throws Exception
	 */
	protected function getCategoryId($category){
		$category = trim($category, '/');

		$id = null;
		foreach (explode('/', $category) as $catItem){
			$catItem = trim($catItem);
			if(!$catItem){
				continue;
			}
			$id = $this->getCatIdByName($id, $catItem);
		}
		if(!$id){
			$id = $this->getUnsortedCatId();
		}

		return $id;
	}
}