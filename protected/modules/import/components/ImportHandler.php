<?php
abstract class ImportHandler
{
	/**
	 *
	 */
	const MAX_LENGTH_SKU = 32;

	/**
	 * @var
	 */
	public $updateType;

	/**
	 * @var array
	 */
	public $settings;

	/**
	 * @var
	 */
	public $format;

	/**
	 * @var int
	 */
	protected $skipRows = 0;

	/**
	 * @var int
	 */
	protected $fieldRow = 0;

	/**
	 * @var array
	 */
	protected $fields = [];

	/**
	 * @var string
	 */
	protected $productSystemField = 'sku';

	/**
	 * @var int
	 */
	protected $productType;

	/**
	 * @var integer
	 */
	protected $unsortedCatId;

	/**
	 * @var string
	 */
	protected $unsortedCatName = 'Неразобранное';

	/**
	 * @var array
	 */
	protected $products;

	/**
	 * @var array
	 */
	protected $productsVariants;

	/**
	 * @var array
	 */
	protected $workProducts;

	/**
	 * @var array
	 */
	protected $workProductsVariants;

	/**
	 * @var
	 */
	protected $minPosProductsVariants;

	/**
	 * @var array
	 */
	protected $producers;

	/**
	 * @var array
	 */
	protected $categories;

	/**
	 * @var array
	 */
	protected $categoriesById;

	/**
	 * @var
	 */
	protected $modulePriceType;

	/**
	 * @var array
	 */
	protected $productTypes = [];

	/**
	 * @var array
	 */
	protected $attributes = [];

	/**
	 * @var array
	 */
	protected $attributesTypes = [];

	/**
	 * @var bool
	 */
	protected $forceSetProductType = false;

	/**
	 * @var int
	 */
	protected $linkedType = 1;

	/**
	 * @var array
	 */
	protected $linkedPositions = [];

	/**
	 * @var array
	 */
	protected $errors = [];

	/**
	 * @var array
	 */
	protected $_log = [];

	/**
	 * @var string
	 */
	protected $logType = 'db'; //db, file

	/**
	 * @var array
	 */
	protected $productsGetFields = ['id', 'external_id', 'category_id', 'producer_id', 'name', 'sku', 'quantity', 'price', 'purchase_price', 'discount_price', 'discount', 'image', 'is_hit', 'is_new', 'is_special', 'in_stock', 'type_id', 'status', 'description', 'short_description'];

	/**
	 * @var array
	 */
	protected $productsVariantsGetFields = ['id', 'product_id', 'external_id', 'amount', 'sku', 'quantity', 'purchase_price', 'discount_price', 'discount', 'in_stock'];

	/**
	 * @var array
	 */
	protected $stats = [
		'addedCats' => 0,
		'addedProduct' => 0,
		'addedProductVariant' => 0,
		'updatedProduct' => 0,
		'updatedProductVariant' => 0,
		'missingProduct' => 0,
		'missingProductVariant' => 0,
		'addedUnsortedProduct' => 0,
		'addedProducer' => 0,
		'skipProduct' => 0,
	];


	/**
	 *
	 */
	public function init() {
		$this->modulePriceType = Yii::app()->getModule('store')->priceType;
		$this->setData();
	}

	/**
	 * @param $form
	 * @return mixed
	 */
	public function run($form){
		return $form;
	}

	/**
	 *
	 */
	public function setData(){
		//Товары
		$command = Yii::app()->db->createCommand(array(
			'select' => $this->productsGetFields === '*' ? '*' : implode(', ', $this->productsGetFields),
			'from' => Product::model()->tableName(),
		));

		if($this->settings['provider']){
			$command->where = 'provider_id = :provider_id';
			$command->params[':provider_id'] = $this->settings['provider'];
		}

		$temp = $command->queryAll();
		foreach ($temp as $item){
			if($item[$this->productSystemField]){
				$this->products[$item[$this->productSystemField]] = $item;
				$this->workProducts[$item[$this->productSystemField]] = $item['id'];
			}
		}

		//Варианты
		$variantsWhere = $this->settings['provider'] && $this->workProducts
			? 'product_id IN ('.implode(', ', $this->workProducts).')'
			: '';
		$variants = Yii::app()->db->createCommand(array(
			'select' => $this->productsVariantsGetFields === '*' ? '*' : implode(', ', $this->productsVariantsGetFields),
			'from' => ProductVariant::model()->tableName(),
			'where' => $variantsWhere
		))->queryAll();

		foreach ($variants as $item){
			if($item[$this->productSystemField]){
				$this->productsVariants[$item[$this->productSystemField]] = $item;
				$this->workProductsVariants[$item[$this->productSystemField]] = $item['id'];
			}
		}

		//Категории магазина
		$categories = Yii::app()->db->createCommand([
			'select' => 'id, parent_id, name, external_id',
			'from' => '{{store_category}}',
		])->queryAll();

		if(!empty($categories)){
			foreach ($categories as $item){
				$this->categories[$item['external_id']] = $item['id'];
				$this->categoriesById[$item['id']] = $item;
			}
		}

		//Типы товаров
		$temp = Yii::app()->db->createCommand(array(
			'from' => Type::model()->tableName(),
		))->queryAll();

		foreach ($temp as $item){
			$this->productTypes[$item['name']] = $item['id'];
		}

		//Атрибуты
		$temp = Yii::app()->db->createCommand(array(
			'from' => Attribute::model()->tableName(),
		))->queryAll();

		foreach ($temp as $item){
			$this->attributes[$item['name']] = $item['id'];
		}

		//Атрибут - тип
		$temp = Yii::app()->db->createCommand(array(
			'from' => TypeAttribute::model()->tableName(),
		))->queryAll();

		foreach ($temp as $item){
			$this->attributesTypes[$item['type_id']][$item['attribute_id']] = true;
		}
	}

	/**
	 * @param $category
	 * @return int
	 * @throws Exception
	 */
	protected function getCategoryId($category){
		return $this->getUnsortedCatId();
	}

	/**
	 * @return int
	 * @throws Exception
	 */
	protected function getUnsortedCatId(){
		if($this->unsortedCatId === null){
			$cat = StoreCategory::model()->find(
				'slug = :slug',
				[
					':slug' => $this->getUnsortedCatSlug(),
				]
			);

			if($cat){
				$this->unsortedCatId = $cat->id;
			} else {
				$cat = new StoreCategory();
				$cat->name = $this->unsortedCatName;
				$cat->slug = $this->getUnsortedCatSlug();
				$cat->external_id = $cat->slug;
				$cat->status = StoreCategory::STATUS_DRAFT;
				if(!$cat->save()){
					$text = 'Не удалось создать категорию для неразобранных позиций. Текст ошибки: '.print_r($cat->getErrors(), true)."\n\n";
					$this->log($text);
					throw new Exception('Не удалось создать категорию для неразобранных позиций');
				}
				$this->unsortedCatId = $cat->id;
			}
		}
		return $this->unsortedCatId;
	}

	/**
	 * @return string
	 */
	protected function getUnsortedCatSlug(){
		return md5($this->unsortedCatName);
	}

	/**
	 * @param $systemName
	 * @return bool
	 * @throws CException
	 */
	public function getCategoryIdBySystemName($systemName){
		return $this->categories[$systemName] ?: false;
	}

	/**
	 * @param $parentId
	 * @param $name
	 * @param $data
	 * @return int
	 * @throws Exception
	 */
	protected function getCatIdByName($parentId, $name, $data = []){
		$slug = $this->prepareSlug($name);
		$name = mb_strtolower($name);
		$name = Helper::mb_ucfirst($name);

		$systemName = $data['external_id'] ?: $slug;
		if($this->categories[$systemName]){
			return $this->categories[$systemName];
		}
		if(!$parentId){
			$condition = 'external_id = :external_id AND parent_id IS NULL';
			$params = [':external_id' => $systemName];
		} else {
			$condition = 'external_id = :external_id AND parent_id = :parent_id';
			$params = [':external_id' => $systemName, ':parent_id' => $parentId];
		}
		$cat = StoreCategory::model()->find($condition, $params);

		if(!$cat){
			$cat = new StoreCategory();
			$cat->parent_id = $parentId;
			$cat->name = $name;
			$cat->slug = $slug;
			$cat->external_id = $systemName;
			$cat->status = StoreCategory::STATUS_PUBLISHED;

			$slugUniqueValidator = CUniqueValidator::createValidator('unique', $cat, ['slug']);
			$slugUniqueValidator->validate($cat, ['slug']);
			$validate = !$cat->hasErrors('slug');
			$infinityLoop = 1;
			$oldSlug = $cat->slug;
			while(!$validate && $infinityLoop < 100){
				$cat->slug = $oldSlug.'-'.$infinityLoop;
				$cat->clearErrors('slug');
				$slugUniqueValidator->validate($cat, ['slug']);
				$validate = !$cat->hasErrors('slug');
				$infinityLoop++;
			}

			if(!$cat->save()){
				$text = 'Не удалось создать категорию "'.$name.' ('.$systemName.')". Текст ошибки: '.print_r($cat->getErrors(), true)."\n\n";
				$this->log($text);

				//throw new Exception('Не удалось создать категорию "'.$name.' ('.$systemName.')"');
			} else {
				$this->stats['addedCats']++;
				$this->categories[$systemName] = $cat->id;
				$this->categoriesById[$cat->id] = $cat->getAttributes();

				if(!$parentId){
					$typeId = $this->getProductType($cat->id);
					$cat->product_type = $typeId;
					$cat->save();
				}

			}
		}
		return $cat->id;
	}

	/**
	 * @param null $sku
	 * @return bool|mixed
	 */
	public function getProductsBySku($sku = null){

		if($sku !== null){
			return $this->products[$sku];
		}
		return false;
	}

	/**
	 * @param string|null $sku
	 * @return ProductVariant|bool
	 */
	public function getProductsVariantBySku($sku){
		return isset($this->productsVariants[$sku]) ? $this->productsVariants[$sku] : false;
	}

	/**
	 * @param $data
	 * @param bool $updateVariants
	 * @throws Exception
	 */
	public function updateProductsHandler($data, $updateVariants = true){
		$productId = false;
		$data[$this->productSystemField] = (string) $data[$this->productSystemField];
		unset($this->workProducts[$data[$this->productSystemField]]);
		if($updateVariants){
			unset($this->workProductsVariants[$data[$this->productSystemField]]);
		}

		if($this->updateType == ImportForm::UPDATE_TYPE_REMOVE_NOT_EXIST){

		} else {
			$productId = $this->updateProduct($data);
			if($updateVariants) {
				$this->updateProductVariant($data);
			}
		}

		if($productId && !empty($data['similar'])){
			$this->linkedPositions[$productId] = $data['similar'];
		}
	}

	public function updateVariantHandler($data){
		$data[$this->productSystemField] = (string) $data[$this->productSystemField];
		unset($this->workProductsVariants[$data[$this->productSystemField]]);
		$this->updateProductVariant($data);
	}

	/**
	 * @param $data
	 * @return int|null
	 * @throws Exception
	 */
	public function updateProduct($data){
		if(!$data[$this->productSystemField]){
			$this->stats['skipProduct']++;
			return null;
		}
		$data[$this->productSystemField] = (string) $data[$this->productSystemField];
		$externalId = $data[$this->productSystemField];

		$newProduct = [];
		$isNewProduct = true;
		$productId = null;
		$isUnsortedAdd = false;

		$product = $this->getProductsBySku($externalId);

		if($product){
			$productId = $product['id'];
			$isNewProduct = false;
		}

		if(!$isNewProduct){
			if($this->updateType != ImportForm::UPDATE_TYPE_FULL){
				return null;
			}
		} else {
			if($this->getProductsVariantBySku($externalId)){
				return false;
			}

			$newProduct['slug'] = $this->getUniqueSlug('{{store_product}}', $this->prepareSlug($data['name']) ?: $data['sku']);

			if($this->settings['provider']){
				$newProduct['provider_id'] = $this->settings['provider'];
			}
		}
		if($this->productSystemField != 'sku'){
			$newProduct['sku'] = $data['sku'];
		}
		if($data['name']){
			$newProduct['name'] = $data['name'];
		}

		if($data['category']){
			$newProduct['category_id'] = $this->getCategoryId($data['category']);
		} elseif($isNewProduct) {
			$newProduct['category_id'] = $this->getUnsortedCatId();
			if($isNewProduct) {
				$newProduct['status'] = Product::STATUS_NOT_ACTIVE;
			}
		}

		if(!$newProduct['status']){
			$newProduct['status'] = Product::STATUS_ACTIVE;
		}

		if($data['price']){
			if($this->modulePriceType == StoreModule::PRICE_TYPE_CHARGE && $this->settings['price_type'] == 'purchase'){
				$newProduct['purchase_price'] = (float) $data['price'];
				$newProduct['price'] = 0;
			} else {
				$newProduct['price'] = (float) $data['price'];
			}
		}

		if(is_numeric($data['discount_price']) && (float)$data['discount_price'] != (float)$data['price']){
			$newProduct['discount_price'] = (float) $data['discount_price'];
		}

		foreach (['quantity', 'is_hit'] as $key){
			if(isset($data[$key])){
				$newProduct[$key] = (int) $data[$key];
			}
		}

		foreach (['short_description', 'description', 'image', 'unit'] as $key){
			if(isset($data[$key])){
				$newProduct[$key] = $data[$key];
			}
		}

		if($this->productSystemField != 'sku'){
			if($data[$this->productSystemField] && !$newProduct[$this->productSystemField]){
				$newProduct[$this->productSystemField] = $data[$this->productSystemField];
			}
		} else {
			if($isNewProduct && $data['sku']){
				$newProduct['sku'] = $data['sku'];
			}
		}

		if(isset($data['in_stock'])){
			$newProduct['in_stock'] = (int) $data['in_stock'];
		}

		$newProduct = $this->updateProductAdvancedFields($newProduct, $data);

		if(isset($data['producer'])){
			$producerId = $this->getProducerId($data['producer']);
			if($producerId){
				$newProduct['producer_id'] = $producerId;
			}
		}

		$attributes = [];
		if($data['characteristic']){
			$attributes = $this->prepareAttribute($data['characteristic'], $newProduct['category_id']);
		}

		if($attributes || $this->forceSetProductType){
			$newProduct['type_id'] = $this->getProductType($newProduct['category_id']);
		}

		try{
			$needUpdate = false;
			$command = Yii::app()->db->createCommand();
			$isSkip = false;
			$isSave = false;
			if($isNewProduct){
				$newModel = new Product();
				$newModel->setAttributes($newProduct, false);
				$isSave = $newModel->save();
				$newModel->id;
				//$isSave = $command->insert('{{store_product}}', $newProduct);
				//$productId = Yii::app()->db->getLastInsertID();
			} else {
				$needUpdate = $this->getIsNeedUpdateProduct($product, $newProduct);

				if($needUpdate){
					$isSave = $command->update('{{store_product}}', $newProduct, 'id = :id', [':id' => $productId]);
				} else {
					$this->stats['skipProduct']++;
					$isSkip = true;
				}
			}

			if($isSave || (!$isSave && !$needUpdate && !$isNewProduct)){
				$this->updateAdvancedImages($productId, $data['images']);

				if($isNewProduct){
					$newProduct['id'] = $productId;
					$this->products[$newProduct[$this->productSystemField]] = $newProduct;
					if($isUnsortedAdd){
						$this->stats['addedUnsortedProduct']++;
					} else {
						$this->stats['addedProduct']++;
					}
				} else {
					if(!$isSkip) {
						$this->stats['updatedProduct']++;
					}
				}

				if($attributes){
					AttributeValue::model()->deleteAll('product_id = :product_id', [':product_id' => $productId]);

					foreach ($attributes as $attrData){
						$attributeValue = new AttributeValue();
						$attributeValue->product_id = $productId;
						$attributeValue->attribute_id = $attrData['attribute_id'];
						$attributeValue->option_value = $attrData['option_id'];
						$attributeValue->save();
					}
				}

				return $productId;
			}

		} catch (Exception $e){
			$line = ($product['line_num']) ? ' (строка '.$product['line_num'].')' : '';
			$text = 'Позиция "'.$data[$this->productSystemField].$line.'" не обработана. Текст ошибки: '.print_r($e->getMessage(), true)."\n\n";
			$this->log($text);
			return null;
		}

		return $productId;
	}

	/**
	 * @param $data
	 * @return |null
	 * @throws Exception
	 */
	public function updateProductVariant($data){
		if($this->updateType != ImportForm::UPDATE_TYPE_FULL){
			return null;
		}

		if(!$data['char']){
			$this->log('Нет значения для варианта '.$data['char'].' ('.$data['product_external_id'].')');
			return false;
		}

		$externalId = $data[$this->productSystemField];

		$isNewVariant = true;
		$variantId = null;
		$newVariant = [];

		$variant = $this->getProductsVariantBySku($externalId);

		if($variant){
			$variantId = $variant['id'];
			$isNewVariant = false;
		}

		if(!$isNewVariant){
			if($this->updateType != ImportForm::UPDATE_TYPE_FULL){
				//return null;
			}
		} else {
			$newVariant['sku'] = $data['sku'];

			$product = $this->getProductsBySku($data['product_external_id']);
			if(!$product){
				$this->log('Не найден товар '.$data['product_external_id']);
				return false;
			}

			$newVariant['product_id'] = $product['id'];
		}

		$categoryId = $this->getCategoryId($data['category']);
		$rootCategoryId = $this->getRootCat($categoryId);

		$category = $this->categoriesById[$rootCategoryId];
		$name = $category['name'];
		$attributeId = $this->getAttributeId('Вариант - '.$name, $categoryId);
		if(!$attributeId){
			$this->log('Нет атрибута для '.$name.' ('.$data['product_external_id'].', categoryId = '.$category.')');
			return false;
		}

		$optionId = $this->getAttributeOptionId($attributeId, $data['char']);
		if(!$optionId){
			$this->log('Нет значения для атрибута '.$data['char'].' ('.$data['product_external_id'].')');
			return false;
		}

		$newVariant['attribute_id'] = $attributeId;
		$newVariant['attribute_value'] = $optionId;

		if($data['price']){
			if($this->modulePriceType == StoreModule::PRICE_TYPE_CHARGE && $this->settings['price_type'] == 'purchase'){
				$newVariant['purchase_price'] = (float) $data['price'];
				$newVariant['amount'] = 0;
			} else {
				$newVariant['amount'] = (float) $data['price'];
			}
			$data['amount'] = $data['price'];
		}

		if(is_numeric($data['discount_price']) && (float)$data['discount_price'] != (float)$data['price']){
			$newVariant['discount_price'] = (float) $data['discount_price'];
		}

		if(isset($data['quantity'])){
			$newVariant['quantity'] = (int) $data['quantity'];
		}

		if(isset($data['in_stock'])){
			$newVariant['in_stock'] = (int) $data['in_stock'];
		}

		if($this->productSystemField != 'sku') {
			if ($data[$this->productSystemField] && !$newVariant[$this->productSystemField]) {
				$newVariant[$this->productSystemField] = $data[$this->productSystemField];
			}

			if ($data['sku']) {
				$newVariant['sku'] = $data['sku'];
			}
		}

		try{
			$needUpdate = false;
			$command = Yii::app()->db->createCommand();
			$isSave = false;
			$isSkip = false;

			if($isNewVariant){
				$newVariantModel = new ProductVariant();
				$newVariantModel->setAttributes($newVariant);

				if($newVariantModel->save()){
					$variantId = $newVariantModel->id;
					$this->stats['addedProductVariant']++;
				} else {
					$line = ($data['line_num']) ? ' (строка '.$data['line_num'].')' : '';
					$text = 'Вариант "'.$data[$this->productSystemField].$line.'" не обработан. Текст ошибки: '.print_r($newVariantModel->getErrors(), true)."\n\n";
					$this->log($text);
					return null;
				}
			} else {
				$needUpdate = $this->getIsNeedUpdateProduct($data, $newVariant, true);

				if ($needUpdate) {
					$isSave = $command->update('{{store_product_variant}}', $newVariant, 'id = :id', [':id' => $variantId]);
				} else {
					$isSkip = true;
					$this->stats['skipProductVariant']++;
				}
			}

			if($isSave || (!$isSave && !$needUpdate && !$isNewVariant)){
				if($isNewVariant){
					$this->stats['addedProductVariant']++;
				} else {
					if(!$isSkip){
						$this->stats['updatedProductVariant']++;
					}
				}
				return $variantId;
			}
		} catch (Exception $e){
			$line = ($data['line_num']) ? ' (строка '.$data['line_num'].')' : '';
			$text = 'Вариант "'.$data[$this->productSystemField].$line.'" не обработан. Текст ошибки: '.print_r($e->getMessage(), true)."\n\n";
			$this->log($text);
			return null;
		}
	}

	/**
	 * @param $product
	 * @param $data
	 * @return mixed
	 */
	protected function updateProductAdvancedFields($product, $data){
		return $product;
	}

	/**
	 * @var string $name
	 * @return mixed
	 */
	protected function getProducerId($name){
		if(!$name){
			return false;
		}
		if($this->producers === null){
			foreach (Producer::model()->findAll() as $item){
				$this->producers[$item->slug] = $item->id;
			}
		}

		$slug = $this->prepareSlug($name);

		if($this->producers[$slug]){
			return $this->producers[$slug];
		} else {
			return $this->addProducer($name, $slug);
		}
	}

	/**
	 * @var string $name
	 * @var string $slug
	 * @return mixed
	 */
	private function addProducer($name, $slug){
		$title = $name;

		$producer = new Producer();
		$producer->name = $name;
		$producer->slug = $slug;
		if(!$producer->save()){
			$text = 'Производитель "'.$title.'" slug="'.$name.'" не обработан. Текст ошибки: '.print_r($producer->getErrors(), true)."\n\n";
			$this->log($text);
			return null;
		} else {
			$this->producers[$slug] = $producer->id;
			$this->stats['addedProducer']++;
			return $producer->id;
		}
	}

	/**
	 * @param string $message
	 */
	protected function log($message){
		if($this->logType == 'file'){
			$dir = Yii::app()->getRuntimePath().'/import/logs/';
			if(!is_dir($dir)){
				CFileHelper::createDirectory($dir, false, true);
			}

			file_put_contents($dir.date('dmY_Hi').'__import.log', $message."\n", FILE_APPEND);
		} elseif($this->logType == 'db') {
			$this->_log[] = $message;
		}
	}

	/**
	 * @param string $slug
	 * @return string mixed
	 */
	protected function prepareSlug($slug){
		$slug = trim($slug);
		$slug = preg_replace('/\s{2,}/',' ',$slug);
		$slug = str_replace(' ', '-', $slug);
		$slug = preg_replace('/[^a-zA-Zа-яА-Я0-9-_.]/ui', '', $slug);
		$slug = mb_strtolower($slug);
		$slug = \dosamigos\yii\helpers\TransliteratorHelper::process($slug);
		$slug = preg_replace(\webforma\components\validators\YSLugValidator::$pattern, '', $slug);
		$slug = mb_substr($slug, 0, 145);
		return $slug;
	}

	/**
	 * @param $data
	 * @param $categoryId
	 * @return array
	 */
	public function prepareAttribute($data, $categoryId){
		return [];
	}

	/**
	 * @param $name
	 * @param $categoryId
	 * @return bool|int
	 */
	public function getAttributeId($name, $categoryId){
		if(!$name){
			return false;
		}
		$slug = $this->prepareSlug($name);
		$attributeId = $this->attributes[$slug];

		if(!$attributeId){
			$model = new Attribute();
			$model->name = $slug;
			$model->title = $name;
			$model->type = 2;
			$model->is_filter = 0;

			if(!$model->save()){
				$text = 'Не удалось создать атрибут. Текст ошибки: '.print_r($model->getErrors(), true)."\n\n";
				$this->log($text);
				return false;
			}
			$attributeId = $model->id;
			$this->attributes[$slug] = $attributeId;
		}

		$typeId = $this->getProductType($categoryId);
		if(!isset($this->attributesTypes[$typeId][$attributeId])){
			$attrType = new TypeAttribute();
			$attrType->type_id = $typeId;
			$attrType->attribute_id = $attributeId;
			if($attrType->save()){
				$this->attributesTypes[$typeId][$attributeId] = true;
			}
		}

		return $attributeId;
	}

	/**
	 * @param $attributeId
	 * @param $value
	 * @return bool|int
	 */
	public function getAttributeOptionId($attributeId, $value){
		$model = AttributeOption::model()->find('attribute_id = :attribute_id AND value = :value', [
			':value' => $value,
			':attribute_id' => $attributeId
		]);

		if($model){
			return $model->id;
		} else {
			$model = new AttributeOption();
			$model->attribute_id = $attributeId;
			$model->value = $value;

			if($model->save()){
				return $model->id;
			}
		}

		return false;
	}

	/**
	 *
	 */
	public function prepareMissingPoses(){
		if($this->settings['missing_product'] === Import::MISSING_POS_SKIP){
			return true;
		}

		$missingProduct = 0;
		$missingProductVariant = 0;
		if($this->settings['missing_product'] === Import::MISSING_POS_DELETE){
			if($this->workProducts){
				$criteria = new CDbCriteria();
				$criteria->addInCondition('id', $this->workProducts);
				$missingProduct = Product::model()->deleteAll($criteria);
			}

			if($this->workProductsVariants){
				$criteria = new CDbCriteria();
				$criteria->addInCondition('id', $this->workProductsVariants);
				$missingProductVariant = ProductVariant::model()->deleteAll($criteria);
			}
		} else {
			$update = [];
			switch ($this->settings['missing_product']){
				case Import::MISSING_POS_AMOUNT_MINUS:
					$update = ['quantity' => -1];
					break;

				case Import::MISSING_POS_NOT_AMOUNT:
					$update = ['in_stock' => Product::STATUS_NOT_IN_STOCK];
					break;

				case Import::MISSING_POS_NOT_ACTIVE:
					$update = ['status' => Product::STATUS_NOT_ACTIVE];
					break;
				case Import::MISSING_POS_DISCOUNTED:
					$update = ['discontinued' => 1];
					break;
			}

			if($update){
				if($this->workProducts){
					$criteria = new CDbCriteria();
					$criteria->addInCondition('id', $this->workProducts);
					$missingProduct = Product::model()->updateAll($update, $criteria);
				}

				if($this->workProductsVariants && $this->settings['missing_product'] == Import::MISSING_POS_NOT_ACTIVE){
					$criteria = new CDbCriteria();
					$criteria->addInCondition('id', $this->workProductsVariants);
					$missingProductVariant = ProductVariant::model()->updateAll($update, $criteria);
				}
			}

		}

		$this->stats['missingProduct'] = $missingProduct;
		$this->stats['missingProductVariant'] = $missingProductVariant;
	}

	/**
	 * @param $key
	 * @param $message
	 */
	protected function addError($key, $message){
		$this->errors[$key][] = $message;
	}

	/**
	 * @return bool
	 */
	public function hasErrors(){
		return !empty($this->errors);
	}

	/**
	 * @return array
	 */
	public function getErrors(){
		return $this->errors;
	}

	/**
	 * @return array
	 */
	public static function getStatsKeys(){
		return [
			'addedCats' => 'Добавлено категорий',
			'addedProduct' => 'Добавлено товаров',
			'updatedProduct' => 'Обновлено товаров',
			'updatedProductVariant' => 'Обновлено вариантов товаров',
			'missingProduct' => 'Обработано отсутсвующих в файле товаров',
			'missingProductVariant' => 'Обработано отсутсвующих в файле вариантов товаров',
			'addedUnsortedProduct' => 'Добавлено товаров в неразобранное',
			'addedProducer' => 'Добавлено производителей',
		];
	}

	/**
	 * @return array
	 */
	public function getStats(){
		return $this->stats;
	}

	/**
	 * @return string
	 */
	public function getStatText(){
		$text = '';

		foreach (self::getStatsKeys() as $key => $title){
			if(isset($this->stats[$key]) && $this->stats[$key] > 0){
				$text .= $title.': '.$this->stats[$key].'<br/>';
			}
		}
		return $text;
	}

	/**
	 * @param $table
	 * @param $slug
	 * @param null $id
	 * @param string $slugField
	 * @return string
	 * @throws CException
	 */
	protected function getUniqueSlug($table, $slug, $id = null, $slugField = 'slug'){
		$linkCount = 0;
		$linkPostfix = '';

		$command = Yii::app()->db->createCommand();

		do {
			$newSlug = $slug.$linkPostfix;

			$command
				->reset()
				->select($slugField)
				->from($table)
				->where($slugField.' = :slug');
			$command->params = [':slug' => $newSlug];

			if($id !== null){
				$command->where = ' AND id != :id';
				$command->params[':id'] = $id;
			}

			$hasExist = $command->queryScalar();

			$linkCount++;
			$linkPostfix = '-' . $linkCount;
		} while ($hasExist);

		return $newSlug;
	}

	/**
	 * @param $product
	 * @param $data
	 * @param $isVariant
	 * @return bool
	 */
	protected function getIsNeedUpdateProduct($product, $data, $isVariant = false){
		foreach ($data as $key => $item){
			if(!isset($data[$key]) || $data[$key] === null || (!isset($product[$key]) && !is_null($product[$key]))){
				continue;
			}

			if(in_array($key, ['attribute_id', 'attribute_value'])){
				continue;
			}

			if(in_array($key, ['price', 'purchase_price', 'discount_price', 'discount', 'amount'])){
				if((float) $product[$key] != (float) $data[$key]){
					return true;
				}
			} elseif(is_string($data[$key]) || is_numeric($data[$key])) {
				if($product[$key] != $data[$key]){
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @param $productId
	 * @param $images
	 * @return bool
	 * @throws CException
	 */
	protected function updateAdvancedImages($productId, $images){
		if(!$images){
			Yii::app()->db->createCommand()->delete('{{store_product_image}}', 'product_id = :product_id', [':product_id' => $productId]);
			return true;
		}
		$currentImages = Yii::app()->db->createCommand([
			'select' => 'id, product_id, name',
			'from' => '{{store_product_image}}',
			'where' => 'product_id = :product_id',
			'params' => [':product_id' => $productId]
		])->queryAll();

		foreach ($images as $key => $image){
			if(!$image){
				continue;
			}
			foreach ($currentImages as $posKey => $posImage){
				if($posImage['name'] == $image){
					unset($images[$key], $currentImages[$posKey]);
				}
			}
		}

		if(!empty($currentImages)){
			$ids = [];
			foreach ($currentImages as $image){
				$ids[] = $image['id'];
			}

			Yii::app()->db->createCommand()->delete('{{store_product_image}}', 'product_id = :product_id AND id IN ('.implode(', ', $ids).')', [':product_id' => $productId]);
		}

		if(!empty($images)){
			$command = Yii::app()->db->createCommand();

			foreach ($images as $image){
				if(!$image){
					continue;
				}
				$command->insert('{{store_product_image}}', [
					'product_id' => $productId,
					'name' => $image,
				]);
			}
		}

		return true;
	}

	/**
	 * @param $categoryId
	 * @return int
	 */
	protected function getProductType($categoryId){
		if($this->productType !== null){
			return $this->productType;
		} else {
			$category = $this->categoriesById[$categoryId];
			$name = $category['name'];
			if($category['parent_id']){
				$parentCategory = $this->categoriesById[$category['parent_id']];
				$name = $parentCategory['name'].' - '.$name;
			}

			if(!empty($this->productTypes[$name])){
				return $this->productTypes[$name];
			} else {
				return $this->createProductType($name);
			}
		}
	}

	/**
	 * @param $name
	 * @return bool|mixed
	 */
	protected function createProductType($name){
		if(!$name){
			return false;
		}
		Yii::app()->db->createCommand()->insert(Type::model()->tableName(), ['name' => $name]);
		if(Yii::app()->db->getLastInsertID()){
			$this->productTypes[$name] = Yii::app()->db->getLastInsertID();
			return $this->productTypes[$name];
		}

		return false;
	}

	/**
	 *
	 */
	protected function updateLinkedPositions(){

	}

	/**
	 * @param $categoryId
	 * @return null
	 */
	protected function getRootCat($categoryId){
		return null;
	}
}