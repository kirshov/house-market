<?php

use PhpOffice\PhpSpreadsheet\IOFactory;
use \PhpOffice\PhpSpreadsheet\Cell\Coordinate;

class ImportHandlerExcel extends ImportHandler
{
	/**
	 * @var int
	 */
	protected $skipRows = 1;

	/**
	 * @var int
	 */
	protected $fieldRow = 1;

	protected $generateSkuByName = false;

	/**
	 * @param $file
	 * @return bool|mixed
	 * @throws Exception
	 */
	public function run($file)
	{
		$columnsData = Column::model()->findAll([
			'condition' => 'import_id = :import_id',
			'params' => [
				':import_id' => $this->format,
			],
			'order' => 'position'
		]);
		if(!$columnsData){
			$this->addError('format', 'В настройках формата выгрзуки не определены столбцы');
			return false;
		}

		$columns = [];
		$k = 1;
		$isSku = $isName = false;
		foreach ($columnsData as $item){
			$columns[$k] = $item->column;
			if($item->column == 'sku'){
				$isSku = true;
			}
			if($item->column == 'name'){
				$isName = true;
			}
			$k++;
		}
		if(!$isSku && !$isName){
			$this->addError('format', 'Формат выгрузки не содержит поля "Артикул" или "Название"');
			return false;
		}
		$this->generateSkuByName = !$isSku;

		//читаем файл
		try{
			$spreadsheet = IOFactory::load($file);
			$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
		} catch (\PhpOffice\PhpSpreadsheet\Exception $error){
			$this->addError('file', $error->getMessage());
			return false;
		}

		if(sizeof($sheetData) < 1){
			$this->addError('file', 'Файл не содержит записей');
			return false;
		}

		/*if(sizeof($sheetData[1]) != sizeof($columns)){
			$this->addError('file', 'Количество столбцов в файле и в формате не совпадает');
			return false;
		}*/

		$skipRows = $this->skipRows;
		$k = 1;
		$dataToUpdate = [];

		foreach ($sheetData as $sheetItem){
			$data = [];
			$fullEmpty = true;
			foreach ($columns as $keyId => $keyValue){
				if($keyValue == 'skip'){
					continue;
				}
				$columnIndex = Coordinate::stringFromColumnIndex($keyId);

				$_value = (string) trim($sheetItem[$columnIndex]);
				$_value = ltrim($_value);
				if($keyValue == 'price' && $_value === ''){
					continue;
				}
				if($keyValue == 'characteristic'){
					$data[$keyValue][$keyId] = $_value;
				} else {
					if($this->generateSkuByName && $keyValue == 'name'){
						$data['sku'] = SlugHelper::prepareSlug($_value, 32);
					}
					$data[$keyValue] = $_value;
				}

				if($sheetItem[$columnIndex] !== null){
					$fullEmpty = false;
				}
			}
			if(!$fullEmpty && $data['sku'] && strlen($data['sku']) <= self::MAX_LENGTH_SKU){
				if($k == $this->fieldRow){
					$this->fields = $data;
				}
				if(!$skipRows){
					$dataToUpdate[$data['sku']] = $data;
				} else {
					$skipRows--;
				}

			}
			$k++;
		}

		unset($spreadsheet, $sheetData);

		if($dataToUpdate){
			foreach ($dataToUpdate as $data){
				$this->updateProductsHandler($data);
			}
		}
		$this->prepareMissingPoses();

		return true;
	}

	/**
	 * @param array $data
	 * @param int $categoryId
	 * @return array
	 */
	public function prepareAttribute($data, $categoryId)
	{
		$result = [];
		foreach ($data as $key => $value){
			$characteristicName = $this->fields['characteristic'][$key];
			if(!$characteristicName){
				continue;
			}
			$attributeId = $this->getAttributeId($characteristicName, $categoryId);
			if(!$attributeId){
				continue;
			}

			$optionId = $this->getAttributeOptionId($attributeId, $value);
			if(!$optionId){
				continue;
			}

			$result[] = [
				'name' => $characteristicName,
				'value' => $value,
				'attribute_id' => $attributeId,
				'option_id' => $optionId
			];
		}
		return $result;
	}

	/**
	 * @param $id
	 */
	public function actionUpdate($id)
	{
		parent::actionUpdate($id);
	}

	/**
	 * @param $category
	 * @return int|void|null
	 * @throws Exception
	 */
	protected function getCategoryId($category){
		$category = trim($category, '/');

		$id = null;
		foreach (explode('/', $category) as $catItem){
			$catItem = trim($catItem);
			if(!$catItem){
				continue;
			}
			$id = $this->getCatIdByName($id, $catItem);
		}
		if(!$id){
			$id = $this->getUnsortedCatId();
		}

		return $id;
	}
}