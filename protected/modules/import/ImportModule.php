<?php

use webforma\components\WebModule;

/**
 * Class ImportModule
 */
class ImportModule extends WebModule
{
    /**
     *
     */
    public $adminMenuOrder = 9;

    /**
     * @return array
     */
    public function getDependencies()
    {
        return ['store'];
    }

    /**
     * @return array|bool
     */
    public function checkSelf()
    {
        $messages = [];

        return isset($messages[WebModule::CHECK_ERROR]) ? $messages : true;
    }

    /**
     * @return bool
     */
    public function getEditableParams()
    {
        return false;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
	    return Yii::t('ImportModule.default', 'Store');
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/import/importBackend/index';
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-file-excel-o',
                'label' => Yii::t('ImportModule.default', 'Import from Excel'),
                'url' => ['/import/importExcelBackend/index'],
            ],
	        [
		        'icon' => 'fa fa-fw fa-file-o',
		        'label' => 'Импор из CSV',
		        'url' => ['/import/importCsvBackend/index'],
	        ],
	        [
		        'icon' => 'fa fa-fw fa-file-o',
		        'label' => 'Импор из XML',
		        'url' => ['/import/importXmlBackend/index'],
	        ],
            '---',
	        [
		        'icon' => 'fa fa-fw fa-list',
		        'label' => 'Журнал импорта',
		        'url' => ['/import/importLogsBackend/index'],
	        ]
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('ImportModule.default', 'Import of goods');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('ImportModule.default', 'Module to import of goods');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-download';
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'application.modules.import.components.*',
                'application.modules.import.forms.*',
                'application.modules.import.models.*',
                'application.modules.store.models.Product',
            ]
        );
    }
}
