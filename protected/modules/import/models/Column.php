<?php

/**
 *
 * @property integer $id
 * @property integer $import_id
 * @property string $column
 * @property integer $position
 *
 * @property-read Attribute $parent
 */
class Column extends \webforma\models\WModel
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{store_import_columns}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * @return Attribute the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['import_id, column', 'required'],
            ['import_id, position', 'numerical', 'integerOnly' => true],
            ['column', 'length', 'max' => 32],
        ];
    }


    /**
     * @return array
     */
    public function relations()
    {
        return [
            'parent' => [self::BELONGS_TO, 'Import', 'import_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('ImportModule.default', 'ID'),
            'position' => Yii::t('ImportModule.default', 'Position'),
            'column' => Yii::t('ImportModule.default', 'Column'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'sortable' => [
                'class' => 'webforma\components\behaviors\SortableBehavior',
            ],
        ];
    }

	public static function getColumnNames(){
    	Yii::import('application.modules.store.StoreModule');
		return [
			'skip' => Yii::t('ImportModule.default', 'Skip column'),
			'name' => Yii::t('StoreModule.store', 'Name'),
			'category' => 'Категория',
			'price' => Yii::t('StoreModule.store', 'Price'),
			'sku' => Yii::t('StoreModule.store', 'SKU'),
			'quantity' => Yii::t('StoreModule.store', 'Quantity'),
			'unit' => 'Единица измерения',
			'producer' => Yii::t('StoreModule.store', 'Producer'),
			'characteristic' => 'Характеристика',
			'short_description' => 'Краткое описание',
			'description' => 'Описание',
		];
	}

    public function getColumnName(){
    	$data = self::getColumnNames();
    	return $data[$this->column];
    }
}
