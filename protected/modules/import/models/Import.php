<?php

/**
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property string $settings
 *
 */
class Import extends \webforma\models\WModel
{
	/**
	 *
	 */
	const TYPE_EXCEL = 1;

	/**
	 *
	 */
	const TYPE_CSV = 2;

	/**
	 *
	 */
	const TYPE_XML = 3;

	/**
	 * @var
	 */
	protected $_type;

	/**
	 * @var
	 */
	public $settings;

	/**
	 * @var
	 */
	public $price_type;

	/**
	 * @var
	 */
	public $provider;

	/**
	 * @var
	 */
	public $missing_product;

	/**
	 * @var array
	 */
	protected $paramsList = ['price_type', 'provider', 'missing_product'];

	/**
	 *
	 */
	const MISSING_POS_NOT_AMOUNT = 0;

	/**
	 *
	 */
	const MISSING_POS_AMOUNT_MINUS = 1;

	/**
	 *
	 */
	const MISSING_POS_NOT_ACTIVE = 2;

	/**
	 *
	 */
	const MISSING_POS_DELETE = 3;

	/**
	 *
	 */
	const MISSING_POS_SKIP = 4;

	/**
	 *
	 */
	const MISSING_POS_DISCOUNTED = 5;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{store_import_settings}}';
    }

    /**
     * @param null|string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        $rules = [
            ['name', 'required'],
            ['name', 'length', 'max' => 255],
            ['type', 'safe'],
            ['id, name, settings', 'safe', 'on' => 'search'],
        ];

        foreach ($this->paramsList as $_item){
        	$rules[] = [$_item, 'safe'];
		}

		return $rules;
    }


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('ImportModule.default', 'Id'),
            'name' => Yii::t('ImportModule.default', 'Title'),
            'settings' => Yii::t('ImportModule.default', 'Settings'),
            'type' => Yii::t('ImportModule.default', 'Type'),
	        'price_type' => 'Цена для обновления',
	        'new_pos' => 'Что делать с новыми позициями',
	        'provider' => 'Поставщик',
			'missing_product' => 'Если позиция отсутствует в выгрузке',
        ];
    }


    /**
     * @return CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria();

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('type', $this->_type);

        return new CActiveDataProvider(
            $this, [
                'criteria' => $criteria,
            ]
        );
    }

    /**
     *
     */
    public function afterFind()
    {
	    $this->settings = json_decode($this->settings, true);
	    if(!is_array($this->settings)){
		    $this->settings = [];
	    }

	    foreach ($this->paramsList as $item){
	    	if($this->settings[$item]){
			    $this->{$item} = $this->settings[$item];
		    }
	    }

        parent::afterFind();
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        return parent::beforeValidate();
    }

	protected function beforeSave()
	{
		$this->type = $this->_type;
		$settings = [];
		foreach ($this->paramsList as $key){
			if($this->{$key} === null){
				continue;
			}
			$settings[$key] = $this->{$key};
		}
		$this->settings = json_encode($settings);
		return parent::beforeSave();
	}

	/**
	 * @return array
	 */
	public function getAllFormat(){
		return CHtml::listData(self::model()->findAll('type = :type', [':type' => $this->_type]), 'id', 'name');
	}

	/**
	 * @return array
	 */
	public function getMissingList(){
		return [
			self::MISSING_POS_SKIP => 'Ничего не делать',
			self::MISSING_POS_AMOUNT_MINUS => 'Проставить наличие "-1"',
			self::MISSING_POS_NOT_AMOUNT => 'Проставить "Нет на складе"',
			self::MISSING_POS_NOT_ACTIVE => 'Сделать неактивными',
			self::MISSING_POS_DISCOUNTED => 'Снят с производства',
			self::MISSING_POS_DELETE => 'Удалить',
		];
	}

	/**
	 * @return mixed
	 */
	public function getType(){
		return $this->_type;
	}

	/**
	 * @return array
	 */
	public function getImportTypes(){
		return [
			self::TYPE_CSV => 'CSV',
			self::TYPE_EXCEL => 'Excel',
		];
	}
}
