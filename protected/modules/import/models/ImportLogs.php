<?php

/**
 * This is the model class for table "{{store_import_logs}}".
 *
 * The followings are the available columns in table '{{store_import_logs}}':
 * @property integer $id
 * @property integer $type
 * @property string $date
 * @property integer $result
 * @property string $message
 * @property integer $has_errors
 * @property string $errors
 */
class ImportLogs extends \webforma\models\WModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{store_import_logs}}';
	}

	public function behaviors()
	{
		return [
			'CTimestampBehavior' => [
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'date',
     			'updateAttribute' => null,
			],
		];
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type, result, has_errors', 'numerical', 'integerOnly'=>true),
			array('date, message, errors', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, type, date, result, message, has_errors, errors', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'type' => 'Тип импорта',
			'date' => 'Дата',
			'result' => 'Успешно',
			'message' => 'Сообщение',
			'has_errors' => 'Ошибки',
			'create_time' => 'Дата',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('type',$this->type);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('result',$this->result);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('has_errors',$this->has_errors);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => ['defaultOrder' => 'date DESC'],
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ImportLogs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return array
	 */
	public function getImportTypes(){
		return [
			Import::TYPE_CSV => 'CSV',
			Import::TYPE_EXCEL => 'Excel',
		];
	}

	public function getType(){
		$data = $this->getImportTypes();
		return isset($data[$this->type]) ? $data[$this->type] : '-';
	}
}
