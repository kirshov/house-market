<?php

class ImportXML extends Import
{
	public function init()
	{
		parent::init();
		$this->_type = self::TYPE_XML;
	}

	/**
	 * @param null|string $className
	 * @return $this
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * @return CActiveDataProvider
	 */
	public function search()
	{
		$criteria = new CDbCriteria();

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('type', $this->_type);

		return new CActiveDataProvider(
			$this, [
				'criteria' => $criteria,
			]
		);
	}
}
