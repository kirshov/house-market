<?php

class ImportCsv extends Import
{
	public function init()
	{
		parent::init();
		$this->_type = self::TYPE_CSV;
	}

	/**
	 * @param null|string $className
	 * @return $this
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
