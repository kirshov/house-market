<?php $installAssets = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.install.views.assets')); ?>
<?php $this->beginContent(Yii::app()->getController()->getModule()->getLayoutAlias()); ?>
<div class="row">
	<div class="left-column col-md-3 col-lg-2 hidden-xs hidden-sm">
		<?php
		$this->widget(
			'bootstrap.widgets.TbMenu',
			[
				'type' => 'list',
				'items' => Yii::app()->getController()->getModule()->getInstallMenu(),
				'htmlOptions' => [
					'class' => ''
				],
			]
		); ?>
	</div>

	<div class="right-column col-sm-12 col-md-9 col-lg-10">
		<?php $this->widget('bootstrap.widgets.TbAlert'); ?>
		<!-- breadcrumbs -->
		<?php if (count($this->breadcrumbs)) {
			$this->widget('bootstrap.widgets.TbBreadcrumbs', ['links' => $this->breadcrumbs]);
		}
		?>
		<div id="content">
			<?= $content; ?>
		</div>
		<!-- content -->
	</div>
</div>
<?php $this->endContent(); ?>
