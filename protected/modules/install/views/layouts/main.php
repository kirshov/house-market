<!DOCTYPE html>
<html lang="<?= Yii::app()->getLanguage(); ?>">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= CHtml::encode(Yii::app()->name); ?> <?= ($this->pageTitle) ? (' - ' . CHtml::encode($this->pageTitle)) : ''; ?></title>
	<?php
	Yii::app()->getClientScript()->registerCoreScript('jquery');
	$mainAssets = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.webforma.views.assets'));
    $installAssets = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.install.views.assets'));
    Yii::app()->getClientScript()->registerCssFile($installAssets . '/css/install.css');
	Yii::app()->getClientScript()->registerCssFile($mainAssets.'/css/font-awesome.min.css');

    $brandTitle = Yii::t('InstallModule.install', 'Install') . ' ' . CHtml::encode(Yii::app()->name);
	?>
	<link rel="shortcut icon" href="<?= $mainAssets; ?>/img/favicon.ico"/>
</head>

<body class="admin-panel">
<div id="overall-wrap">
	<?
	$this->widget(
        'bootstrap.widgets.TbNavbar',
		[
			'fluid'    => true,
			'brand'    => CHtml::image(
				$mainAssets . "/img/logo.png",
				$brandTitle,
				[
					'title'  => $brandTitle,
				]
			),
			'brandUrl' => $this->createUrl('index'),
			'items'    => [
				CHtml::tag('span', ['id' => 'stepName'], CHtml::encode($this->stepName)),
				[
					'class'       => 'bootstrap.widgets.TbMenu',
					'type'        => 'navbar',
					'htmlOptions' => ['class' => 'navbar-right'],
					'items'       => array_merge(
						[
							[
								'label' => 'web-forma.ru',
								'icon'  => 'fa fa-fw fa-thumbs-up',
								'url'   => 'http://web-forma.ru/?from=install',
								'linkOptions' => ['target' => '_blank'],
							],
						]
					),
				],
			],
		]
    );
    ?>
	<div class="container-fluid" id="page">
		<div class='installContentWrapper'>
			<div class="installContent row">
				<?= $content; ?>
			</div>
		</div>
	</div>
	<div id="footer-guard"></div>
</div>

<footer>
	&copy;<?= date('Y'); ?>
</footer>
</body>
</html>
