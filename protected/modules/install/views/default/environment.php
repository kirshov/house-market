<?php
/**
 * Отображение для environment
 **/
?>
<?php if (!$data['result']) : { ?>
    <div class="alert alert-danger">
        <b><?=  Yii::t('InstallModule.install', 'Install can\'t be continued. Please check errors!'); ?></b>
    </div>
<?php } endif; ?>

<div class="alert alert-info">
    <p>На данном этапе FORMA.cms проверяет права доступа для всех необходимых каталогов.<br/>
		Для продолжения установки необходимо исправить все возникшие проблемы.
	</p>
</div>

<table class="table table-striped">
    <tr>
        <th><?=  Yii::t('InstallModule.install', 'Value'); ?></th>
        <th><?=  Yii::t('InstallModule.install', 'Result'); ?></th>
        <th><?=  Yii::t('InstallModule.install', 'Comments'); ?></th>
    </tr>
    <?php foreach ($data['requirements'] as $requirement): { ?>
        <tr>
            <td style="width:200px;"><?=  $requirement[0]; ?></td>
            <td>
                <?php
                $this->widget(
                    'bootstrap.widgets.TbLabel',
                    [
                        'context' => $requirement[1] ? 'success' : 'danger',
                        'label'   => $requirement[1] ? 'ОК' : Yii::t('InstallModule.install', 'Error'),
                    ]
                ); ?>
            </td>
            <td><?=  $requirement[2]; ?></td>
        </tr>
    <?php } endforeach; ?>
</table>

<br/>

<?=  CHtml::link(
    Yii::t('InstallModule.install', '< Back'),
    ['/install/default/index'],
    ['class' => 'btn btn-primary']
); ?>

<?php
if ($data['result'] !== false) {
    echo CHtml::link(
        Yii::t('InstallModule.install', 'Continue >'),
        ['/install/default/requirements'],
        ['class' => 'btn btn-primary']
    );
} else {
    echo CHtml::link(
        Yii::t('InstallModule.install', 'Refresh'),
        ['/install/default/environment'],
        ['class' => 'btn btn-primary']
    );
}
?>
