<?php
/**
 * Отображение для finish
 **/
?>
<h3>Поздравляем, установка завершена!</h3>

<p>Ваш сайт готов к работе!</p>

<hr/>

<?= CHtml::link('ПЕРЕЙТИ НА САЙТ', '/', ['class' => 'btn btn-info']); ?> или
<?= CHtml::link('ПЕРЕЙТИ В ПАНЕЛЬ УПРАВЛЕНИЯ', ['/backend/index'],['class' => 'btn btn-info']); ?>
<hr/>