<?php
/**
 * Отображение для index
 **/
?>
<h1>Добро пожаловать!</h1>

<p>Следуйте инструкциям установщика!</p>

<div class="btn-group">
	<?php $this->widget('bootstrap.widgets.TbButton', [
		'label'      => 'Далее',
		'buttonType' => 'link',
		'url'        => ['/install/default/environment'],
	]); ?>
</div>