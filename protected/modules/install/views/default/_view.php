<?php
$this->renderPartial(
    Yii::app()->controller->action->id,
    [
        'data' => isset($data) ? $data : [],
    ]
);
