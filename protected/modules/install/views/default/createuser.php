<?php
/**
 * Отображение для createuser:
 **/
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'id'                     => 'createuser-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'type'                   => 'vertical',
    ]
);

Yii::app()->clientScript->registerScript(
    'fieldset',
    "
    $('document').ready(function () {
        $('.popover-help').popover({ trigger : 'hover', delay : 500 });
    });"
);
?>

<div class="alert alert-info">
    <p>Создайте учетную запись администратора сайта.</p>

    <p>Пожалуйста, указывайте сложный пароль, содержащий как цифры и буквы, так и специальные символы.</p>

    <p>Запомните, указанные на данном этапе данные, они Вам потребуются для доступа к панели управления.</p>
</div>

<?=  $form->errorSummary($data['model']); ?>

<div class="row">
    <div class="col-sm-6">
        <?=  $form->textFieldGroup($data['model'], 'userName', [
			'widgetOptions' => [
				'htmlOptions' => [
					'autocomplete' => 'off',
				]
			]
		]); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?=  $form->textFieldGroup($data['model'], 'userEmail', [
			'widgetOptions' => [
				'htmlOptions' => [
					'autocomplete' => 'off',
				]
			]
		]); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?=  $form->passwordFieldGroup($data['model'], 'userPassword', [
			'widgetOptions' => [
				'htmlOptions' => [
					'autocomplete' => 'off',
				]
			]
		]); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?=  $form->passwordFieldGroup($data['model'], 'cPassword', [
			'widgetOptions' => [
				'htmlOptions' => [
					'autocomplete' => 'off',
				]
			]
		]); ?>
    </div>
</div>

<?=  CHtml::link(
    Yii::t('InstallModule.install', '< Back'),
    ['/install/default/modulesinstall'],
    ['class' => 'btn btn-default']
); ?>

<?php
$this->widget(
    'bootstrap.widgets.TbButton',
    [
        'buttonType' => 'submit',
        'context'    => 'primary',
        'label'      => Yii::t('InstallModule.install', 'Continue >'),
    ]
); ?>

<?php $this->endWidget(); ?>
