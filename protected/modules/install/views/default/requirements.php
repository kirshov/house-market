<?php
/**
 * Отображение для requirements
 **/
?>
<?php if (!$data['result']) : { ?>
    <div class="alert alert-danger">
        <b>Дальнейшая установка невозможна, пожалуйста, исправьте ошибки!</b>
    </div>
<?php } endif; ?>

<div class="alert alert-info">
    <p>На данном этапе проверяем версию PHP и наличие всех необходимых модулей.</p>

    <p>Для продолжения установки необходимо исправить все возникшие проблемы.</p>
</div>

<table class="table table-striped">
    <tr>
        <th><?=  Yii::t('InstallModule.install', 'Value'); ?></th>
        <th><?=  Yii::t('InstallModule.install', 'Result'); ?></th>
        <th><?=  Yii::t('InstallModule.install', 'Comments'); ?></th>
    </tr>
    <?php foreach ($data['requirements'] as $requirement): { ?>
        <tr>
            <td style="width:200px;"><?=  $requirement[0]; ?></td>
            <td>
                <?php
                $this->widget(
                    'bootstrap.widgets.TbLabel',
                    [
                        'context' => $requirement[2] ? 'success' : ($requirement[1] ? 'danger' : 'default'),
                        'label'   => $requirement[2] ? 'ОК' : ($requirement[1] ? Yii::t(
                                'InstallModule.install',
                                'Error'
                            ) : Yii::t('InstallModule.install', 'Warning')),
                    ]
                ); ?>
            </td>
            <td><?=  ($requirement[4] == '') ? '&nbsp;' : $requirement[4]; ?></td>
        </tr>
    <?php } endforeach; ?>
</table>

<br/>

<?=  CHtml::link(
    Yii::t('InstallModule.install', '< Back'),
    ['/install/default/environment'],
    ['class' => 'btn btn-default']
); ?>

<?php
if ($data['result'] !== false) {
    echo CHtml::link(
        Yii::t('InstallModule.install', 'Continue >'),
        ['/install/default/dbsettings'],
        ['class' => 'btn btn-primary']
    );
} else {
    echo CHtml::link(
        Yii::t('InstallModule.install', 'Refresh'),
        ['/install/default/requirements'],
        ['class' => 'btn btn-primary']
    );
}
?>
