<?php
/**
 * Отображение для dbsettings:
 **/
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'id'                     => 'dbsettings-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'type'                   => 'vertical',
    ]
);

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('application.modules.install.views.assets') . '/js/dbinstall.js'
    ),
    CClientScript::POS_END
);

Yii::app()->clientScript->registerScript(
    'dbtypes',
    '
    var dbTypes = ' . json_encode($data['model']->dbTypeNames) . ';
    var defaultAttr = {
        "mysql": ' . json_encode($data['model']->attributes) . '
    };
    ',
    CClientScript::POS_BEGIN
);

Yii::app()->clientScript->registerScript(
    'fieldset',
    "
    $('document').ready(function () {
        $('.popover-help').popover({ trigger : 'hover', delay : 500 });
    });",
    CClientScript::POS_READY
);
?>

<div class="alert alert-info">
    <p>Укажите параметры соединения с базой данных.</p>

    <p>Базу данных можно создать при помощи phpmyadmin или любого другого инструмента.</p>

    <p><b>Попытаемся создать базу данных если вы поставите галочку "Создать базу данных"</p></b>
</div>

<?php if (!$data['result']) : { ?>
    <div class="alert alert-danger">
        <b>Файл <?=$data['file']?> не существует или не доступен для записи!</b>
    </div>
<?php } endif; ?>

<?=  $form->errorSummary($data['model']); ?>


<div class="row">
    <div class="col-sm-6">
        <?=  $form->dropDownListGroup($data['model'], 'dbType', [
			'widgetOptions' => [
				'data' => $data['model']->getDbTypeNames(),
			]
		]); ?>
    </div>
</div>

<div class="row mysql-enable">
    <div class="col-sm-6">
        <?=  $form->textFieldGroup($data['model'], 'host', [
			'widgetOptions' => [
				'htmlOptions' => [
					'autocomplete' => 'off',
				]
			]
		]); ?>
    </div>
</div>

<div class="row mysql-enable">
    <div class="col-sm-6">
        <?=  $form->textFieldGroup($data['model'], 'port'); ?>
    </div>
</div>

<div class="row mysql-enable">
    <div class="col-sm-6">
        <?=  $form->textFieldGroup($data['model'], 'dbName', [
			'widgetOptions' => [
				'htmlOptions' => [
					'autocomplete'  => 'off',
				]
			]
		]); ?>
    </div>
</div>

<div class="row mysql-enable">
    <div class="col-sm-6">
        <?=  $form->checkBoxGroup($data['model'], 'createDb'); ?>
    </div>
</div>

<div class="row mysql-enable">
    <div class="col-sm-6">
        <?=  $form->textFieldGroup($data['model'], 'tablePrefix', [
			'widgetOptions' => [
				'htmlOptions' => [
					'autocomplete' => 'off',
				]
			]
		]); ?>
    </div>
</div>

<div class="row mysql-enable">
    <div class="col-sm-6">
        <?=  $form->textFieldGroup($data['model'], 'dbUser', [
			'widgetOptions' => [
				'htmlOptions' => [
					'autocomplete' => 'off',
				]
			]
		]); ?>
    </div>
</div>

<div class="row mysql-enable">
    <div class="col-sm-6">
        <?=  $form->passwordFieldGroup($data['model'], 'dbPassword', [
			'widgetOptions' => [
				'htmlOptions' => [
					'autocomplete' => 'off',
				]
			]
		]); ?>
    </div>
</div>

<div class="row mysql-enable">
    <div class="col-sm-6">
        <?=  $form->textFieldGroup($data['model'], 'socket', [
			'widgetOptions' => [
				'htmlOptions' => [
					'autocomplete' => 'off',
				]
			]
		]); ?>
    </div>
</div>

<br/>

<?=  CHtml::link(Yii::t('InstallModule.install', '< Back'), ['/install/default/requirements'], [
	'class' => 'btn btn-default'
]); ?>

<?php $this->widget('bootstrap.widgets.TbButton', [
	'buttonType' => 'submit',
	'context'    => 'primary',
	'label'      => Yii::t('InstallModule.install', 'Check connection and continue >'),
]); ?>

<?php $this->endWidget(); ?>
