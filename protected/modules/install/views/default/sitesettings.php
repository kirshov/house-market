<?php
/**
 * Отображение для sitesettings
 **/
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'id'                     => 'sitesettings-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'type'                   => 'vertical',
    ]
);

Yii::app()->clientScript->registerScript(
    'fieldset',
    "$('document').ready(function () {
        $('.popover-help').popover({ trigger : 'hover', delay : 500 });
    });"
); ?>

<div class="alert alert-info">
    <p>Укажите название Вашего сайта, его описание и ключевые слова, необходимые для SEO-оптимизации.</p>
</div>

<?=  $form->errorSummary($data['model']); ?>

<div class="row">
    <div class="col-sm-6">
        <?=  $form->dropDownListGroup($data['model'], 'theme', [
			'widgetOptions' => [
				'data'        => $data['themes'],
			]
		]); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?=  $form->textFieldGroup($data['model'], 'siteName'); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?=  $form->textFieldGroup($data['model'], 'shortSiteName'); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?=  $form->textAreaGroup($data['model'], 'siteDescription', [
			'widgetOptions' => [
				'htmlOptions' => [
					'rows' => 6,
				]
			]
		]); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?=  $form->textAreaGroup($data['model'], 'siteKeyWords', [
			'widgetOptions' => [
				'htmlOptions' => [
					'rows' => 6,
				]
			]
		]); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?=  $form->textFieldGroup($data['model'], 'siteEmail'); ?>
    </div>
</div>

<?=  CHtml::link(
    Yii::t('InstallModule.install', '< Back'),
    ['/install/default/createuser'],
    ['class' => 'btn btn-default']
); ?>

<?php
$this->widget(
    'bootstrap.widgets.TbButton',
    [
        'buttonType' => 'submit',
        'context'    => 'primary',
        'label'      => Yii::t('InstallModule.install', 'Continue >'),
    ]
); ?>

<?php $this->endWidget(); ?>
