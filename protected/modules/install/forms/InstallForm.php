<?php

/**
 * Install Form Model
 * Класс формы установки
 **/
class InstallForm extends webforma\models\WFormModel
{
    /**
     * Типы баз данных:
     **/
    const DB_MYSQL = 1;
    const DB_POSTGRESQL = 2;
    const DB_MSSQL = 3;
    const DB_ORACLE = 4;

    /**
     * Параметры для настройки БД:
     **/
    public $host = 'localhost';
    public $port = '3306';
    public $socket = '';
    public $dbName;
    public $createDb;
    public $dbUser;
    public $dbPassword;
    public $tablePrefix = 'webforma_';
    public $dbType = self::DB_MYSQL;

    /**
     * Для создания пользователя:
     **/
    public $userName;
    public $userPassword;
    public $cPassword;
    public $userEmail;

    /**
     * Для начальной настройки сайта:
     **/
    public $siteName = 'Название сайта';
    public $shortSiteName = 'Название сайта';
    public $siteDescription = '';
    public $siteKeyWords = '';
    public $siteEmail;
    public $theme = 'default';

    public $email;

    public function init()
    {
        parent::init();
    }

    /**
     * Правила валидации параметров формы:
     *
     * @return mixed правила валидации параметров формы
     **/
    public function rules()
    {
        return [
            /**
             * Для настройки БД:
             **/
            ['host, port, dbName, dbUser, dbType', 'required', 'on' => 'dbSettings'],
            ['dbPassword', 'length', 'min' => 0, 'max' => 32],
            ['port, dbType', 'numerical', 'integerOnly' => true],
            ['dbName, dbUser', 'length', 'min' => 0, 'max' => 255],
            ['socket, createDb, tablePrefix', 'safe'],
            /**
             * Для начальной настройки сайта:
             **/
            ['siteName, siteEmail, theme', 'required', 'on' => 'siteSettings'],
            ['siteName', 'length', 'max' => 255],
            ['shortSiteName, siteDescription, siteKeyWords, theme', 'length', 'max' => 180],
            ['siteEmail', 'email'],
            /**
             * Для настройки администратора:
             **/
            ['userName, userPassword, cPassword, userEmail', 'required', 'on' => 'createUser'],
            ['userPassword, cPassword', 'length', 'min' => 8],
            ['userName', 'length', 'min' => 4],
            [
                'cPassword',
                'compare',
                'compareAttribute' => 'userPassword',
                'message'          => Yii::t('InstallModule.install', 'Passwords are not consistent')
            ],
            ['userEmail', 'email'],
        ];
    }

    /**
     * Отображаемые названия параметров формы:
     *
     * @return mixed отображаемые названия параметров формы
     **/
    public function attributeLabels()
    {
        return [
            /**
             * Для настройки БД:
             **/
            'host'            => Yii::t('InstallModule.install', 'Host'),
            'port'            => Yii::t('InstallModule.install', 'Port'),
            'socket'          => Yii::t('InstallModule.install', 'Unix socket (if it need)'),
            'dbName'          => Yii::t('InstallModule.install', 'DB name'),
            'createDb'        => Yii::t('InstallModule.install', 'Create DB'),
            'dbType'          => Yii::t('InstallModule.install', 'DBMS type'),
            'dbUser'          => Yii::t('InstallModule.install', 'User'),
            'dbPassword'      => Yii::t('InstallModule.install', 'Password'),
            'tablePrefix'     => Yii::t('InstallModule.install', 'Tables prefix'),
            /**
             * Для начальной настройки сайта:
             **/
            'siteName'        => Yii::t('InstallModule.install', 'Site title'),
            'shortSiteName'   => 'Краткое название сайта',
            'siteDescription' => Yii::t('InstallModule.install', 'Site description'),
            'siteKeyWords'    => Yii::t('InstallModule.install', 'Site keywords'),
            'siteEmail'       => Yii::t('InstallModule.install', 'Administrator e-mail'),
            'theme'           => Yii::t('InstallModule.install', 'Default frontend theme'),
            /**
             * Для настройки администратора:
             **/
            'userName'        => Yii::t('InstallModule.install', 'User name'),
            'userEmail'       => Yii::t('InstallModule.install', 'Email'),
            'userPassword'    => Yii::t('InstallModule.install', 'Password'),
            'cPassword'       => Yii::t('InstallModule.install', 'Password confirm'),
        ];
    }

    /**
     * Названия для типов БД:
     *
     * @return mixed названия для типов БД:
     **/
    public function getDbTypeNames()
    {
        /** Определяем доступные базы данных:
         *
         * Варианты СУБД
         *
         * self::DB_MYSQL => 'MySQL',
         *
         */

        $dbTypes = [];
        /**
         * Проверяем доступные СУБД:
         */

        if (extension_loaded('pdo_mysql')) {
            $dbTypes[self::DB_MYSQL] = 'MySQL';
        }

        return $dbTypes;
    }

    /**
     * Названия бд для строк подключения:
     *
     * @return mixed названия для типов БД:
     **/
    public function getDbTypes()
    {
        return [
            self::DB_MYSQL      => 'mysql',
            self::DB_POSTGRESQL => 'pgsql',
        ];
    }

    /**
     * Получение аттрибута почты:
     *
     * @return string аттрибут почты:
     **/
    public function getEmailName()
    {
        return User::model()->admin()->find()->getAttribute('email');
    }

}
