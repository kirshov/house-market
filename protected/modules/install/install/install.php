<?php
/**
 *
 * Файл конфигурации модуля
 **/
Yii::setPathOfAlias('bootstrap', realpath(Yii::getPathOfAlias('vendor') . '/clevertech/yii-booster/src'));
Yii::setPathOfAlias('booster', realpath(Yii::getPathOfAlias('vendor') . '/clevertech/yii-booster/src'));
return [
	'install' => true,
	'module'  => [
		'class' => 'application.modules.install.InstallModule',
	],
	'component' => [
		'bootstrap' => [
			'class' => 'application.modules.webforma.components.WFBooster',
			'coreCss' => true,
			'bootstrapCss' => true,
			'responsiveCss' => true,
			'yiiCss' => true,
			'jqueryCss' => true,
			'enableJS' => true,
			'fontAwesomeCss' => false,
			'enableNotifierJS' => false,
			'minify' => false,
			'forceCopyAssets' => defined('YII_DEBUG') && YII_DEBUG,
		],
	],
	'preload' => ['bootstrap'],
	'rules'   => [
		// правила контроллера site
		'/' => '/install/default/index',

	],
];
