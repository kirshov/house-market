<?php

use webforma\components\WebModule;

/**
 * Class MerchantModule
 */
class MerchantModule extends WebModule
{
    /**
     *
     */
    public $adminMenuOrder = 12;

    /**
     * @return array
     */
    public function getDependencies()
    {
        return ['store'];
    }

    /**
     * @return bool
     */
    public function getEditableParams()
    {
        return false;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
	    return Yii::t('MerchantModule.default', 'Store');
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/merchant/exportBackend/index';
    }

    /**
     * @return string
     */
    public function appendNavigationToModule(){
        return 'Экспорт товаров';
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            ['label' => $this->getName()],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('MerchantModule.default', 'Export lists'),
                'url' => ['/merchant/exportBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('MerchantModule.default', 'Create export list'),
                'url' => ['/merchant/exportBackend/create'],
            ],
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('MerchantModule.default', 'Merchant export');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('MerchantModule.default', 'Module to export products in the merchant file');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-upload';
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'application.modules.merchant.models.Export',
                'application.modules.store.models.Product',
            ]
        );
    }
}
