<? /**
 * @var  Product[] $offers
 */ ?>
<?= Yii::app()->getComponent('merchantRepository')->getXmlHead(); ?>
<feed xmlns="http://www.w3.org/2005/Atom" xmlns:g="http://base.google.com/ns/1.0">
    <title><?= $model->shop_name; ?></title>
    <link rel="self" href="<?= $model->shop_url; ?>"/>
    <updated><?= date(DATE_ATOM); ?></updated>

    <?php foreach ($offers as $offerId => $offer): ?>
        <?
        $price = $offer->getResultPrice();
        if (!$price) {
            continue;
        }

        $name = $clearName = htmlspecialchars(strip_tags($offer->name));

        $description = htmlspecialchars(strip_tags($offer->description));

        $productImage = null;
        if ($offer->image) {
            $productImage = StoreImage::product($offer);
            if ($productImage && mb_strpos($productImage, '/images/nophoto.jpg') !== false) {
                $productImage = null;
            }
        }

        $ymlFields = [];
        ?>
        <entry>
	        <?php foreach ($offer->getAttributeGroups() as $groupName => $items): ?>
		        <?php foreach ($items as $attribute): ?>
			        <?
			        $attrValue = AttributeRender::renderValue($attribute, $offer->attribute($attribute), true, '{item}');
			        if (!$attrValue) continue;
			        $attrValue = trim($attrValue);
			        if ($attribute->yml_field) {
				        $ymlFields[$attribute->yml_field] = $attrValue;
			        }
			        if ($attribute->yml_add_name && (($attribute->yml_add_name_on_variant && $offer->variants) || !$attribute->yml_add_name_on_variant)) {
				        $name .= ' ' . $attrValue;
			        }

			        $description .= "\n".$attribute->title.': '.htmlspecialchars(strip_tags($attrValue));

			        if (mb_strlen($description) > 5000) {
				        $description = mb_substr($description, 0, 4990) . '...';
			        }
			        ?>
		        <?php endforeach; ?>
	        <?php endforeach; ?>

            <g:id><?= $offer->id ?></g:id>
            <g:title><?= $name; ?></g:title>
            <g:description><?= $description; ?></g:description>
            <g:link><?= CHtml::normalizeUrl(Yii::app()->getBaseUrl(true) . ProductHelper::getUrl($offer)); ?></g:link>
            <g:image_link><?= $productImage ?></g:image_link>
			<g:availability> <?= $offer->isInStock() ? 'in stock' : 'out of stock'; ?></g:availability>
			<g:price><?=$price?>.00 <?=$currency?></g:price>
	        <? if ($offer->producer->name): ?>
				<g:brand><?= htmlspecialchars(strip_tags($offer->producer->name)); ?></g:brand>
	        <? endif; ?>
			<g:mpn><?= $offer->sku ?></g:mpn>
        </entry>
    <?php endforeach; ?>
</feed>