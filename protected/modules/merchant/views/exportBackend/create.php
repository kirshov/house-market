<?php
/* @var $model Export */

$this->breadcrumbs = [
    Yii::t('MerchantModule.default', 'Products export') => ['/merchant/exportBackend/index'],
    Yii::t('MerchantModule.default', 'Creating'),
];

$this->pageTitle = Yii::t('MerchantModule.default', 'Products export - creating');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('MerchantModule.default', 'Manage export lists'), 'url' => ['/merchant/exportBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('MerchantModule.default', 'Create export list'), 'url' => ['/merchant/exportBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?= Yii::t('MerchantModule.default', 'Products export'); ?>
        <small><?= Yii::t('MerchantModule.default', 'creating'); ?></small>
    </h1>
</div>

<?= $this->renderPartial('_form', ['model' => $model]); ?>
