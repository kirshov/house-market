<?php
/* @var $model Export */
$this->breadcrumbs = [
    Yii::t('MerchantModule.default', 'Products export') => ['/merchant/exportBackend/index'],
    Yii::t('MerchantModule.default', 'Manage'),
];

$this->pageTitle = Yii::t('MerchantModule.default', 'Products export - manage');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('MerchantModule.default', 'Manage export lists'), 'url' => ['/merchant/exportBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('MerchantModule.default', 'Create export list'), 'url' => ['/merchant/exportBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?= Yii::t('MerchantModule.default', 'Products export'); ?>
        <small><?= Yii::t('MerchantModule.default', 'administration'); ?></small>
    </h1>
</div>

<?php $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'export-grid',
        'type' => 'condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => [
            [
                'name' => 'id',
            ],
            [
                'name' => 'name',
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::link($data->name, ["/merchant/exportBackend/update", "id" => $data->id]);
                },
            ],
            [
                'header' => 'URL',
                'type' => 'raw',
                'value' => function ($data) {
                    $url = Yii::app()->createAbsoluteUrl('/merchant/export/view', ['id' => $data->id]);
                    $url = rtrim($url, '/');
                    return CHtml::link($url, $url, ['target' => '_blank']);
                }
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
                'template' => '{update} {refresh} {delete}',
				'buttons' => [
					'refresh' => [
						'label' => 'Перегенирировать',
						'url' => function ($data) {
							return Yii::app()->createUrl("/backend/merchant/export/refresh/".$data->id);
						},
						'icon' => 'fa fa-fw fa-refresh',
						'options' => ['class' => 'front-view btn btn-sm btn-default'],
					],
				],
            ],
        ],
    ]
); ?>
