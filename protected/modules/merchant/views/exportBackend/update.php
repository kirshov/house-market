<?php
/* @var $model Export */

$this->breadcrumbs = [
    Yii::t('MerchantModule.default', 'Products export') => ['/merchant/exportBackend/index'],
    Yii::t('MerchantModule.default', 'Edition'),
];

$this->pageTitle = Yii::t('MerchantModule.default', 'Products export - edition');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('MerchantModule.default', 'Manage export lists'), 'url' => ['/merchant/exportBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('MerchantModule.default', 'Create export list'), 'url' => ['/merchant/exportBackend/create']],
    ['label' => Yii::t('MerchantModule.default', 'Export list') . ' «' . mb_substr(CHtml::encode($model->name), 0, 32) . '»'],
    [
        'icon' => 'fa fa-fw fa-pencil',
        'label' => Yii::t('MerchantModule.default', 'Update export list'),
        'url' => [
            '/merchant/exportBackend/update',
            'id' => $model->id
        ]
    ],
    [
        'icon' => 'fa fa-fw fa-trash-o',
        'label' => Yii::t('MerchantModule.default', 'Delete export list'),
        'url' => '#',
        'linkOptions' => [
            'submit' => ['/merchant/exportBackend/delete', 'id' => $model->id],
            'params' => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
            'confirm' => Yii::t('MerchantModule.default', 'Do you really want to remove this export list?'),
            'csrf' => true,
        ]
    ],
];
?>
<div class="page-header">
    <h1>
        <?= Yii::t('MerchantModule.default', 'Updating export list'); ?><br/>
        <small>&laquo;<?= CHtml::encode($model->name); ?>&raquo;</small>
    </h1>
</div>

<?= $this->renderPartial('_form', ['model' => $model]); ?>
