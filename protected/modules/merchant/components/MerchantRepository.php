<?php
class MerchantRepository extends CApplicationComponent
{
	public function getXml($id)
	{
		$model = Export::model()->findByPk($id);

		if (false === $model) {
			throw new CHttpException(404);
		}

		$criteria = new CDbCriteria();
		$categoryCriteria = new CDbCriteria();
		$criteria->compare('t.status', Product::STATUS_ACTIVE);
		$criteria->addCondition('t.in_stock > 0');
		$criteria->addCondition('(t.quantity > 0 OR ISNULL(t.quantity) OR t.quantity = "")');

		if (!empty($model->categories)) {
			$criteria->addInCondition('t.category_id', (array)$model->categories);
			$categoryCriteria->addInCondition('t.id', (array)$model->categories);
		}

		if (!empty($model->brands)) {
			$criteria->addInCondition('t.producer_id', (array)$model->brands);
		}
		if (!empty($model->providers)) {
			$criteria->addInCondition('t.provider_id', (array)$model->providers);
		}

		if(is_numeric($model->price_from)){
			$criteria->addCondition('t.price >= '.$model->price_from);
		}
		if(is_numeric($model->price_to)){
			$criteria->addCondition('t.price <= '.$model->price_to);
		}

		$criteria->scopes = ['published'];
		$criteria->with = ['producer', 'variants', 'category', 'type', 'attributesValues', 'type.typeAttributes'];
		//$criteria->with = ['producer', 'category', 'type', 'attributesValues'];

		$dataProvider = new CActiveDataProvider('Product', ['criteria' => $criteria]);

		$offers = new CDataProviderIterator($dataProvider, 100);

		/**
		 * @var StoreModule $storeModule
		 */
		$storeModule = Yii::app()->getModule('store');

		$merchant = Yii::app()->getController()->renderPartial('view', [
			'model' => $model,
			'currencies' => Yii::app()->getModule('store')->getCurrencyList(),
			'currency' => 'RUB',
			'categories' => StoreCategory::model()->publishedAll()->findAll($categoryCriteria),
			'offers' => $offers,
			'minCost' => $storeModule->minCost,
			'minCostText' => $storeModule->minCost.' '.$this->getPluralCurrency('RUB', $storeModule->minCost),
		], true);

		unset($offers, $dataProvider, $model);

		return $merchant;
	}

	public function saveXml($id, $merchant){
		$dir = Yii::getPathOfAlias('public').'/'.Yii::app()->getModule('webforma')->uploadPath.'/merchant/';
		if(!is_dir($dir)){
			CFileHelper::createDirectory($dir, null, true);
		}
		file_put_contents($dir.$id.'.xml', $merchant);
	}

	/**
	 * @param $currency
	 * @param $cost
	 * @return string
	 */
	public function getPluralCurrency($currency, $cost){
		if($currency == 'RUB'){
			return Helper::pluralForm($cost, ['рубль', 'рубля', 'рублей']);
		}
	}

	/**
	 * @return string
	 */
	public function getXmlHead()
	{
		return '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL;
	}

}