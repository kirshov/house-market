<?php

return [
    'module' => [
        'class' => 'application.modules.merchant.MerchantModule',
    ],
	'component' => [
		'merchantRepository' => [
			'class' => 'application.modules.merchant.components.MerchantRepository'
		],
	],
    'rules' => [
        [
            'merchant/export/view',
            'pattern' => '/uploads/merchant/<id:\d+>.xml',
            'urlSuffix' => ''
        ],
    ],
];
