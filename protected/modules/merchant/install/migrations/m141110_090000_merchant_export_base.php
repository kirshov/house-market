<?php

class m141110_090000_merchant_export_base extends webforma\components\DbMigration
{
    public function safeUp()
    {
        $this->createTable(
            "{{merchant_export}}",
            [
                "id" => "pk",
                "name" => "varchar(255) not null",
                "settings" => "text null default null",
            ],
            $this->getOptions()
        );
    }

    public function safeDown()
    {
        $this->dropTable("{{merchant_export}}");
    }
}
