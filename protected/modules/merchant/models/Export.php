<?php

/**
 *
 * @property integer $id
 * @property string $name
 * @property string $settings
 *
 */
class Export extends \webforma\models\WModel
{
    /**
     * @var array
     */
    public $brands = [];

    /**
     * @var array
     */
    public $providers = [];

    /**
     * @var array
     */
    public $categories = [];

    /**
     * @var
     */
    public $shop_name;
    /**
     * @var
     */
    public $shop_url;

    /**
     * @var
     */
    public $price_to;

    /**
     * @var
     */
    public $price_from;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{merchant_export}}';
    }

    /**
     * @param null|string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['name, shop_name, shop_url', 'required'],
            ['name', 'unique'],
            ['name', 'length', 'max' => 255],
            ['shop_name', 'length', 'max' => 20],
            ['shop_url', 'length', 'max' => 255],
            ['price_from, price_to', 'numerical', 'integerOnly' => true],
            ['brands, categories, providers', 'safe'],
            ['id, name, settings', 'safe', 'on' => 'search'],
        ];
    }


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('MerchantModule.default', 'Id'),
            'name' => Yii::t('MerchantModule.default', 'Title'),
            'settings' => Yii::t('MerchantModule.default', 'Settings'),
            'brands' => Yii::t('MerchantModule.default', 'Brands'),
            'providers' => Yii::t('MerchantModule.default', 'Providers'),
            'categories' => Yii::t('MerchantModule.default', 'Categories'),
            'shop_name' => Yii::t('MerchantModule.default', 'Store short title'),
            'shop_url' => Yii::t('MerchantModule.default', 'Store URL'),
            'price_from' => 'Цена от',
            'price_to' => 'Цена до',
        ];
    }


    /**
     * @return CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria();

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider(
            $this, [
                'criteria' => $criteria,
            ]
        );
    }

    /**
     *
     */
    public function afterFind()
    {
        $settings = json_decode($this->settings);
        $this->brands = $settings->brands;
        $this->providers = $settings->providers;
        $this->categories = $settings->categories;
        $this->shop_name = $settings->shop_name;
        $this->shop_url = $settings->shop_url;
        $this->price_from = $settings->price_from;
        $this->price_to = $settings->price_to;
        parent::afterFind();
    }


    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $settings = [
            'brands' => $this->brands,
            'providers' => $this->providers,
            'categories' => $this->categories,
            'shop_name' => $this->shop_name,
            'shop_url' => $this->shop_url,
            'price_from' => $this->price_from,
            'price_to' => $this->price_to,
        ];
        $this->settings = json_encode($settings);

        return parent::beforeValidate();
    }
}
