<?php
set_time_limit(0);
use webforma\components\ContentType;

/**
 * Class ExportController
 *
 */
class ExportController extends \webforma\components\controllers\FrontController//application\components\Controller
{
	public function actionView($id)
	{
		$merchant = Yii::app()->getComponent('merchantRepository')->getXml($id);
		Yii::app()->getComponent('merchantRepository')->saveXml($id, $merchant);

		\webforma\components\ContentType::setHeader(\webforma\components\ContentType::TYPE_XML);
		echo $merchant;
		Yii::app()->end();
	}
}
