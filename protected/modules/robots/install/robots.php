<?php
return [
    'module'    => [
        'class' => 'application.modules.robots.RobotsModule',
    ],
    'import'    => [],
    'component' => [],
    'rules'     => [
		'robots.txt' => 'robots/robots/index',
	],
	'params'    => [],
];
