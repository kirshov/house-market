<?php

use webforma\components\controllers\FrontController;

error_reporting(0);
/**
 * Class RobotsController
 */
class RobotsController extends FrontController
{
	public function init()
	{
		parent::init();
		$this->getModule()->checkOnCreateFile();
	}


	/**
	 *
	 */
	public function actionIndex()
	{
		$useCache = !Yii::app()->hasModule('regions');

		$robots = file_get_contents($this->getModule()->userFile);
		$robots = str_replace('{domain}', rtrim(Yii::app()->createAbsoluteUrl('/'), '/'), $robots);

		if($useCache){
			file_put_contents(Yii::getPathOfAlias('public').DIRECTORY_SEPARATOR.'/robots.txt', $robots);
		}

		header("Content-Type: text/plain");
		echo $robots;
	}
}

