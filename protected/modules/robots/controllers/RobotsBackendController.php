<?php

/**
 * Class RobotsBackendController
 */
class RobotsBackendController extends \webforma\components\controllers\BackController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin'],],
            ['allow', 'actions' => ['index'], 'roles' => ['Robots.RobotsBackend.Index'],],
            ['deny',],
        ];
    }

    /**
     *
     */
    public function actionIndex()
    {
    	$this->getModule()->checkOnCreateFile();
		$file = $this->getModule()->userFile;
	    if (($content = Yii::app()->getRequest()->getPost('robots')) !== null) {
		    file_put_contents($file, $content);
			@unlink(Yii::getPathOfAlias('public').DIRECTORY_SEPARATOR.'/robots.txt');
		    $this->redirect(['index']);
	    } else {
		    $content = file_get_contents($file);
	    }

        $this->render('index', ['robots' => $content]);
    }
}