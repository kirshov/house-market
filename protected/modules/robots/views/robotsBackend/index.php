<?php
/**
 * @var RobotsBackendController $this
 * @var \webforma\widgets\ActiveForm $form
 * @var Fields $model
 */

$this->breadcrumbs = [
    'Файл robots.txt' => ['/robots/robotsBackend/index'],
];

$this->pageTitle = 'Редактирование файла robots.txt';

$form = $this->beginWidget(
	'\webforma\widgets\ActiveForm',
	[
		'id'                     => 'robots-form',
		'enableAjaxValidation'   => false,
		'enableClientValidation' => true,
		'type'                   => 'vertical',
		'htmlOptions'            => ['class' => 'well'],
	]
); ?>

<div class="row">
	<div class='col-sm-6'>
		<div class="form-group">
			<?=CHtml::label('Содержимое файла robots.txt', 'robots', ['class' => 'control-label'])?>
			<?php echo CHtml::textArea('robots', $robots, ['class' => 'form-control', 'style' => 'width:100%; height: 400px']); ?>
			<div class="help-block">
				Используйте конструкцию <b>{domain}</b> для автоподстановки домена
			</div>
		</div>
	</div>
</div>

<?php $this->widget(
	'bootstrap.widgets.TbButton',
	[
		'buttonType' => 'submit',
		'context'    => 'primary',
		'label'      => 'Сохранить'
	]
); ?>

<?php $this->endWidget(); ?>
