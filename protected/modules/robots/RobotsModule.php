<?php

use webforma\components\WebModule;

/**
 * Class RobotsModule
 */
class RobotsModule extends WebModule
{
	/**
	 * @var
	 */
    public $srcFile;

	/**
	 * @var
	 */
    public $userFile;

	/**
	 * @var int
	 */
	public $adminMenuOrder = 110;

	public function init()
	{
		parent::init();
		$this->srcFile = Yii::getPathOfAlias('application.modules.robots.install').DIRECTORY_SEPARATOR.'robots.txt';
		$this->userFile = Yii::getPathOfAlias('public').DIRECTORY_SEPARATOR.'robots.src.txt';
	}

	/**
	 *
	 */
	public function checkOnCreateFile(){
		if(!is_file($this->userFile)){
			copy($this->srcFile, $this->userFile);
		}
	}

    /**
     * @return string
     */
    public function getCategory()
    {
        return 'Сервисы';
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/robots/robotsBackend/index';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Файл robots.txt';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return '';
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
	    return 'kirshov';
    }

    /**
     * @return string
     */
    public function getAuthorEmail()
    {
        return 'kirshov@gmail.com';
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return '';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-file-code-o';
    }
}