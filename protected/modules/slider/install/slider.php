<?php
/**
 * Файл конфигурации модуля
 **/
return [
    'module'    => [
        'class' => 'application.modules.slider.SliderModule',
    ],
    'import'    => [
        'application.modules.slider.models.*',
    ],
    'component' => [],
    'rules'     => [],
	'params'    => [
		'widgets' => [
			'slider' => 'application.modules.slider.widgets.SliderWidget',
		]
	]
];
