<?php

/**
 * Slider install migration
 * Класс миграций для модуля Slider
 *
 **/
class m000000_000000_slider_base extends webforma\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        $this->createTable(
            '{{slider}}',
            [
                'id'            => 'pk',
                'category_id'   => 'integer DEFAULT NULL',
                'name'          => 'varchar(250) NOT NULL',
                'description'   => 'text',
                'file'          => 'varchar(250) NOT NULL',
                'create_time'   => 'datetime NOT NULL',
                'alt'           => 'varchar(250) NOT NULL',
                'link'          => 'varchar(250) NOT NULL',
                'status'        => "integer NOT NULL DEFAULT '1'",
	            'sort'          => "integer NOT NULL DEFAULT '1'",
            ],
            $this->getOptions()
        );

        //ix
        $this->createIndex("ix_{{slider}}_status", '{{slider}}', "status", false);
        $this->createIndex("ix_{{slider}}_category_id", '{{slider}}', "category_id", false);

        //fk
        $this->addForeignKey(
            "fk_{{slider}}_category_id",
            '{{slider}}',
            'category_id',
            '{{category_category}}',
            'id',
            'SET NULL',
            'NO ACTION'
        );
    }

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{slider}}');
    }
}
