<?php

/**
 * Slider основная модель для работы с изображениями
 */
use webforma\widgets\WPurifier;

/**
 * This is the model class for table "Slider".
 *
 * The followings are the available columns in table 'Slider':
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string $file
 * @property string $create_time
 * @property string $alt
 * @property integer $status
 * @property integer $category_id
 * @property string $link
 *
 * The followings are the available model relations:
 * @property User $user
 */
class Slider extends webforma\models\WModel
{
    /**
     *
     */
    const STATUS_ACTIVE = 1;
    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className - class name
     *
     * @return Slider the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * table name
     *
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{slider}}';
    }

    /**
     * validation rules
     *
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['name, description, alt, link', 'filter', 'filter' => [new WPurifier(), 'purify']],
            ['name', 'required'],
            ['name, description, alt', 'filter', 'filter' => 'trim'],
            ['status, category_id', 'numerical', 'integerOnly' => true],
            ['category_id, status', 'length', 'max' => 11],
            ['alt, name, file, link', 'length', 'max' => 250],
            ['category_id', 'default', 'setOnEmpty' => true, 'value' => null],
            ['id, name, description, create_time, alt, status', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $module = Yii::app()->getModule('slider');

        return [
            'sliderUpload' => [
                'class' => 'webforma\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'file',
                'uploadPath' => $module->uploadPath,
            ],
            'sortable' => [
                'class' => 'webforma\components\behaviors\SortableBehavior',
                'attributeName' => 'sort',
            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
                'category' => [self::BELONGS_TO, 'Category', 'category_id'],
            ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('SliderModule.slider', 'id'),
            'category_id' => Yii::t('SliderModule.slider', 'Category'),
            'name' => Yii::t('SliderModule.slider', 'Title'),
            'description' => Yii::t('SliderModule.slider', 'Description'),
            'file' => 'Изображение',
            'create_time' => Yii::t('SliderModule.slider', 'Created at'),
            'alt' => Yii::t('SliderModule.slider', 'Alternative text'),
            'status' => Yii::t('SliderModule.slider', 'Status'),
            'link' => Yii::t('SliderModule.slider', 'Link'),
        ];
    }

	public function scopes()
	{
		return [
			'published' => [
				'condition' => 't.status = :status',
				'params' => [':status' => self::STATUS_ACTIVE],
			],
		];
	}

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria();

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.description', $this->description, true);
        $criteria->compare('t.file', $this->file, true);
        $criteria->compare('t.create_time', $this->create_time, true);
        $criteria->compare('t.alt', $this->alt, true);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('t.category_id', $this->category_id);

        return new CActiveDataProvider(get_class($this), [
            'criteria' => $criteria,
            'sort' => ['defaultOrder' => 't.sort'],
        ]);
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if ($this->getIsNewRecord()) {
            $this->create_time = new CDbExpression('NOW()');
        }

        return parent::beforeValidate();
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('SliderModule.slider', 'allowed'),
            self::STATUS_NOT_ACTIVE => Yii::t('SliderModule.slider', 'need to be checked'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('SliderModule.slider', '*unknown*');
    }

    /**
     * @return string
     */
    public function getCategoryName()
    {
        return ($this->category === null) ? '---' : $this->category->name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name ? $this->name : $this->alt;
    }
}
