<?php

/**
 * SliderModule основной класс модуля slider
 */

use webforma\components\WebModule;

/**
 * Class SliderModule
 */
class SliderModule extends WebModule
{
    /**
     * @var string
     */
    public $uploadPath = 'slider';

	/**
	 * @var
	 */
	public $width = 1920;

	/**
	 * @var
	 */
	public $height = 400;

	/**
	 * @var
	 */
	public $autoPlay = 1;

	/**
	 * @var
	 */
	public $autoPlaySpeed = 5;

	/**
	 *
	 */
	public $adminMenuOrder = 30;

	/**
	 * @var int
	 */
	public $useCategory = 0;

	/**
	 * @var int
	 */
	public $navigation = 0;

	/**
	 *
	 */
	public $mainCategory;

    /**
     * @return string
     */
    public function getUploadPath()
    {
        return Yii::app()->uploadManager->getBasePath().DIRECTORY_SEPARATOR.$this->uploadPath;
    }

    /**
     * @return bool
     */
    public function getInstall()
    {
        if (parent::getInstall()) {
            @mkdir($this->getUploadPath(), 0755);
        }

        return false;
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-picture-o';
    }

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [
            'useCategory' => 'Использовать категории',
            'mainCategory' => 'Основная категория',
            'width' => Yii::t('SliderModule.slider', 'Slider width'),
            'height' => Yii::t('SliderModule.slider', 'Slider height'),
			'navigation' => 'Показывать навигацию по слайдам',
			'autoPlay' => 'Автопрокрутка',
			'autoPlaySpeed' => 'Пауза при автопрокрутке',
        ];
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
        $params = [
			'autoPlay' => [
				'input-type' => 'checkbox',
			],
            'autoPlaySpeed',
            'navigation' => [
				'input-type' => 'checkbox',
			],
            'width',
            'height',
        ];

        if(DEVMODE){
        	$params = CMap::mergeArray($params, [
				'mainCategory' => CHtml::listData($this->getCategoryList(), 'id', 'name'),
				'useCategory' => [
					'input-type' => 'checkbox',
				],
			]);
		}

		return $params;
    }

    /**
     * @return array
     */
    public function getEditableParamsGroups()
    {
		$groups = [
            '1.main' => [
                'label' => Yii::t('SliderModule.slider', 'Resize settings'),
                'items' => [
                    'width',
                    'height',
					'navigation',
					'autoPlay',
					'autoPlaySpeed',
                ],
            ],
        ];

		if(DEVMODE){
			$groups = CMap::mergeArray($groups, [
				'system' => [
					'label' => 'Системные настройки',
					'items' => [
						'useCategory',
						'mainCategory',
					]
				]
			]);
		}

		return $groups;
    }

    /**
     * @return array|bool
     */
    public function checkSelf()
    {
    	return true;
        $messages = [];

        $uploadPath = Yii::app()->uploadManager->getBasePath().DIRECTORY_SEPARATOR.$this->uploadPath;

        if (!$uploadPath) {
            $messages[WebModule::CHECK_ERROR][] = [
                'type' => WebModule::CHECK_ERROR,
                'message' => Yii::t(
                    'SliderModule.slider',
                    'Please, choose catalog for sliders! {link}',
                    [
                        '{link}' => CHtml::link(
                            Yii::t('SliderModule.slider', 'Change module settings'),
                            [
                                '/webforma/backend/modulesettings/',
                                'module' => $this->id,
                            ]
                        ),
                    ]
                ),
            ];
        }

        if (!is_dir($uploadPath) || !is_writable($uploadPath)) {
            $messages[WebModule::CHECK_ERROR][] = [
                'type' => WebModule::CHECK_ERROR,
                'message' => Yii::t(
                    'SliderModule.slider',
                    'Directory "{dir}" is not accessible for writing ot not exists! {link}',
                    [
                        '{dir}' => $uploadPath,
                        '{link}' => CHtml::link(
                            Yii::t('SliderModule.slider', 'Change module settings'),
                            [
                                '/webforma/backend/modulesettings/',
                                'module' => $this->id,
                            ]
                        ),
                    ]
                ),
            ];
        }

        if (!$this->maxSize || $this->maxSize <= 0) {
            $messages[WebModule::CHECK_ERROR][] = [
                'type' => WebModule::CHECK_ERROR,
                'message' => Yii::t(
                    'SliderModule.slider',
                    'Set maximum sliders size {link}',
                    [
                        '{link}' => CHtml::link(
                            Yii::t('SliderModule.slider', 'Change module settings'),
                            [
                                '/webforma/backend/modulesettings/',
                                'module' => $this->id,
                            ]
                        ),
                    ]
                ),
            ];
        }

        return (isset($messages[WebModule::CHECK_ERROR])) ? $messages : true;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('SliderModule.slider', 'Content');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('SliderModule.slider', 'Sliders');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('SliderModule.slider', 'Module for sliders management');
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport([
			'slider.models.*',
		]);
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('SliderModule.slider', 'Sliders list'),
                'url' => ['/slider/sliderBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('SliderModule.slider', 'Add slide'),
                'url' => ['/slider/sliderBackend/create'],
            ]
        ];
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/slider/sliderBackend/index';
    }

	/**
	 * Возвращаем статус, устанавливать ли галку для установки модуля в инсталяторе по умолчанию:
	 *
	 * @return bool
	 **/
	public function getIsInstallDefault()
	{
		return true;
	}
}
