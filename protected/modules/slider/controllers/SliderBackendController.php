<?php

/**
 * SliderBackendController контроллер для управления изображениями в панели упраления
 **/
class SliderBackendController extends webforma\components\controllers\BackController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Slider.SliderBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Slider.SliderBackend.View']],
            [
                'allow',
                'actions' => ['create', 'AjaxSliderUpload', 'AjaxSliderChoose'],
                'roles' => ['Slider.SliderBackend.Create'],
            ],
            ['allow', 'actions' => ['update', 'sortable'], 'roles' => ['Slider.SliderBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Slider.SliderBackend.Delete']],
            ['deny'],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'AjaxSliderUpload' => [
                'class' => 'webforma\components\actions\YAjaxSliderUploadAction',
            ],
            'AjaxSliderChoose' => [
                'class' => 'webforma\components\actions\YAjaxSliderChooseAction',
            ],
            'sortable' => [
                'class' => 'webforma\components\actions\SortAction',
                'model' => 'Slider',
                'attribute' => 'sort',
            ],
        ];
    }

    /**
     * Отображает изображение по указанному идентификатору
     *
     * @param integer $id Идинтификатор изображение для отображения
     *
     * @return void
     */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }

    /**
     * Создает новую модель изображения.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return void
     */
    public function actionCreate()
    {
        $model = new Slider();

        if (($data = Yii::app()->getRequest()->getPost('Slider')) !== null) {

            $model->setAttributes($data);

            $transaction = Yii::app()->getDb()->beginTransaction();

            try {

                if ($model->save()) {

                    $transaction->commit();

                    Yii::app()->getUser()->setFlash(
	                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                        Yii::t('SliderModule.slider', 'Slider created!')
                    );

                    $this->redirect(
                        (array)Yii::app()->getRequest()->getPost(
                            'submit-type',
                            ['create']
                        )
                    );
                }
            } catch (Exception $e) {
                $transaction->rollback();

                Yii::app()->getUser()->setFlash(
	                webforma\widgets\WFlashMessages::ERROR_MESSAGE,
                    $e->getMessage()
                );
            }
        }

        $this->render('create', ['model' => $model]);
    }

    /**
     * Редактирование изображения.
     *
     * @param integer $id the ID of the model to be updated
     *
     * @return void
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (($data = Yii::app()->getRequest()->getPost('Slider')) !== null) {

            $model->setAttributes($data);

            if ($model->save()) {

                Yii::app()->getUser()->setFlash(
	                webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('SliderModule.slider', 'Slider updated!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['update', 'id' => $model->id]
                    )
                );
            }
        }

        $this->render('update', ['model' => $model]);
    }

    /**
     * Удаяет модель изображения из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id - идентификатор изображения, который нужно удалить
     *
     * @return void
     *
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            Yii::app()->getUser()->setFlash(
	            webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                Yii::t('SliderModule.slider', 'Slider removed!')
            );

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            Yii::app()->getRequest()->getParam('ajax') !== null || $this->redirect(
                (array)Yii::app()->getRequest()->getPost('returnUrl', 'index')
            );
        } else {
            throw new CHttpException(
                400,
                Yii::t('SliderModule.slider', 'Bad request. Please don\'t repeat similar requests anymore')
            );
        }
    }

    /**
     * Управление изображениями.
     *
     * @return void
     */
    public function actionIndex()
    {
        $model = new Slider('search');

        $model->unsetAttributes(); // clear any default values

        $model->setAttributes(
            Yii::app()->getRequest()->getParam(
                'Slider',
                []
            )
        );

        $this->render('index', ['model' => $model]);
    }

    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param integer $id идентификатор нужной модели
     *
     * @return void
     *
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        if (($model = Slider::model()->findByPk($id)) === null) {
            throw new CHttpException(
                404,
                Yii::t('SliderModule.slider', 'Requested page was not found!')
            );
        }

        return $model;
    }
}
