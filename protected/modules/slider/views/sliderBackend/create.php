<?php
$this->breadcrumbs = [
    Yii::t('SliderModule.slider', 'Sliders') => ['/slider/sliderBackend/index'],
    Yii::t('SliderModule.slider', 'Add'),
];

$this->pageTitle = Yii::t('SliderModule.slider', 'Slide - add');

$this->menu = [
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('SliderModule.slider', 'Slider management'),
        'url'   => ['/slider/sliderBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('SliderModule.slider', 'Add slide'),
        'url'   => ['/slider/sliderBackend/create']
    ],
];
?>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>
