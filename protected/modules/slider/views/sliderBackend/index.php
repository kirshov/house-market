<?php
$this->breadcrumbs = [
    Yii::t('SliderModule.slider', 'Sliders') => ['/slider/sliderBackend/index'],
    Yii::t('SliderModule.slider', 'Management'),
];

$this->pageTitle = Yii::t('SliderModule.slider', 'Slide - manage');

$this->menu = [
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('SliderModule.slider', 'Slider management'),
        'url'   => ['/slider/sliderBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('SliderModule.slider', 'Add slide'),
        'url'   => ['/slider/sliderBackend/create']
    ],
];
?>
<?php
$this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id'           => 'slider-grid',
        'sortableRows'      => true,
        'sortableAjaxSave'  => true,
        'sortableAttribute' => 'sort',
        'sortableAction'    => '/slider/sliderBackend/sortable',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => [
            [
                'name'   => Yii::t('SliderModule.slider', 'file'),
                'type'   => 'raw',
                'value'  => 'CHtml::image($data->getImageUrl(100, 100), $data->alt, array("width" => 100, "height" => 100))',
                'htmlOptions' => [
                	'style' => 'width:120px;'
				],
                'filter' => false
            ],
            'name',
            'link',
            [
                'name'   => 'category_id',
                'value'  => '$data->getCategoryName()',
                'filter' => CHtml::activeDropDownList(
					$model,
					'category_id',
					Yii::app()->getComponent('categoriesRepository')->getFormattedList(Yii::app()->getModule('slider')->mainCategory),
					['encode' => false, 'empty' => '', 'class' => 'form-control']
				),
				'visible' => Yii::app()->getModule('slider')->useCategory,
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
            ],
        ],
    ]
); ?>
