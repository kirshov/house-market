<?php
$this->breadcrumbs = [
    Yii::t('SliderModule.slider', 'Sliders') => ['/slider/sliderBackend/index'],
    $model->name                          => ['/slider/sliderBackend/view', 'id' => $model->id],
    Yii::t('SliderModule.slider', 'Edit'),
];

$this->pageTitle = Yii::t('SliderModule.slider', 'Slide - edit');

$this->menu = [
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('SliderModule.slider', 'Slider management'),
        'url'   => ['/slider/sliderBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('SliderModule.slider', 'Add slide'),
        'url'   => ['/slider/sliderBackend/create']
    ],
    ['label' => Yii::t('SliderModule.slider', 'Slider') . ' «' . mb_substr($model->name, 0, 32) . '»'],
    [
        'icon'  => 'fa fa-fw fa-pencil',
        'label' => Yii::t('SliderModule.slider', 'Edit slider'),
        'url'   => [
            '/slider/sliderBackend/update',
            'id' => $model->id
        ]
    ],
    [
        'icon'  => 'fa fa-fw fa-eye',
        'label' => Yii::t('SliderModule.slider', 'View slider'),
        'url'   => [
            '/slider/sliderBackend/view',
            'id' => $model->id
        ]
    ],
    [
        'icon'        => 'fa fa-fw fa-trash-o',
        'label'       => Yii::t('SliderModule.slider', 'Remove slider'),
        'url'         => '#',
        'linkOptions' => [
            'submit'  => ['/slider/sliderBackend/delete', 'id' => $model->id],
            'params'  => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
            'confirm' => Yii::t('SliderModule.slider', 'Do you really want to remove slider?'),
            'csrf'    => true,
        ]
    ],
];
?>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>
