<?php
/**
 * @var BackendController $this
 **/
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'id' => 'slider-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => 'vertical',
		'htmlOptions' => ['class' => 'well sticky', 'enctype' => 'multipart/form-data'],
		'clientOptions' => [
			'validateOnSubmit' => true,
		],
    ]
); ?>
<div class="alert alert-info">
    <?=  Yii::t('SliderModule.slider', 'Fields with'); ?>
    <span class="required">*</span>
    <?=  Yii::t('SliderModule.slider', 'are required.'); ?>
</div>

<?=  $form->errorSummary($model); ?>

<div class='row'>
	<div class="col-sm-6">
		<div class='row'>
			<div class='col-sm-6'>
				<?= $form->dropDownListGroup($model, 'status', [
					'widgetOptions' => [
						'data' => $model->getStatusList(),
					],
				]); ?>
			</div>

			<?if($this->getModule()->useCategory):?>
				<span class="col-sm-6">
					<?= $form->dropDownListGroup($model, 'category_id', [
						'widgetOptions' => [
							'data' => Yii::app()->getComponent('categoriesRepository')->getFormattedList(),
							'htmlOptions' => ['empty' => Yii::t('SliderModule.slider', '--choose--'), 'encode' => false],
						],
					]); ?>
				</span>
			<?endif;?>
		</div>

		<div class='row'>
			<div class="col-sm-12">
				<?=  $form->textFieldGroup($model, 'name'); ?>
			</div>
		</div>

		<div class='row'>
			<div class="col-sm-12">
				<?=  $form->textFieldGroup($model, 'link'); ?>
			</div>
		</div>

		<div class='row'>
			<div class="col-sm-12">
				<?=  $form->textAreaGroup($model, 'description'); ?>
			</div>
		</div>
	</div>

	<div class="col-sm-6">
		<div class='row'>
			<div class="col-sx-8 col-sm-8 col-sm-offset-2">
				<?php $this->widget('webforma\widgets\WInputFile', [
					'model' => $model,
					'attribute' => 'file',
				]); ?>
			</div>
		</div>
	</div>
</div>

<div class="buttons">
	<?php
	$this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context'    => 'primary',
		'label'      => $model->isNewRecord
			? Yii::t('SliderModule.slider', 'Add slide and close')
			: Yii::t('SliderModule.slider', 'Save slider and continue'),
	]); ?>

	<?php
	$this->widget('bootstrap.widgets.TbButton', [
		'buttonType'  => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label'       => $model->isNewRecord
			? Yii::t('SliderModule.slider', 'Add slide and save')
			: Yii::t('SliderModule.slider', 'Save mage and close'),
	]); ?>
</div>
<?php $this->endWidget(); ?>
