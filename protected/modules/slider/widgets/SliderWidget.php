<?php
Yii::import('application.modules.slider.models.*');

class SliderWidget extends webforma\widgets\WWidget
{
	// сколько изображений выводить на одной странице
	public $limit = null;

	/**
	 * @var string
	 */
	public $view = 'default';

	/**
	 * @var int
	 */
	public $width;

	/**
	 * @var int
	 */
	public $height;

	/**
	 * @var int
	 */
	public $categoryId;

	/**
	 * @var string
	 */
	protected $sliderId;

	/**
	 * @var SliderModule
	 */
	public function init()
	{
		$this->module = Yii::app()->getModule('slider');
		parent::init();
		if(!$this->width){
			$this->width = $this->module->width;
		}

		if(!$this->height){
			$this->height = $this->module->height;
		}

		$this->sliderId = rand(0, 9999);
		$this->getController()->initPlugin('slick');
	}


	/**
	 * Запускаем отрисовку виджета
	 *
	 * @return void
	 */
	public function run()
	{
		$criteria = new CDbCriteria();
		if($this->limit){
			$criteria->limit = $this->limit;
		}
		if($this->categoryId){
			$criteria->condition .= 'category_id = :category_id';
			$criteria->params[':category_id'] = $this->categoryId;
		}

		$criteria->order = 'sort';

		$slides = Slider::model()->published()->findAll($criteria);
		if($slides){
			$this->render($this->view, [
				'slides' => $slides,
				'width' => $this->width,
				'height' => $this->height,
				'sliderId' => $this->sliderId,
				'autoPlay' => $this->module->autoPlay ? 'true' : 'false',
				'autoPlaySpeed' => (int) $this->module->autoPlaySpeed * 1000,
				'navigation' => $this->module->navigation ? 'true' : 'false',
			]);
		}

	}
}
