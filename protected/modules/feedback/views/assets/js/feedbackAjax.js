function feedbackSendForm(form) {

    var resultField = form.find('.result-message');
	resultField
        .html('')
        .removeClass('bs-callout-danger')
        .removeClass('bs-callout-success')
        .addClass('hidden');

    $.ajax({
        url: form.attr('action'),
        type: 'POST',
        data: form.serialize(),
        dataType: 'json',
        success: function (response) {
            if(response.result){
				//form.find('.pay_percent').html('0%');
				var price = form.find('.input-price').val();
				form.trigger('reset');
				form.find('.input-price').val(price);

				var thanksModal = $('.modal-window.modal-thanks');
				if(!thanksModal.length || form.closest('.modal-window').hasClass('modal-thanks')){
					resultField.html('<p>' + response.data.message + '</p>');
					resultField.addClass('bs-callout-success').removeClass('hidden');

					if(form.closest('.popup-feedback').length){
						form.find('.modal-body .modal-inner').addClass('hidden');
					}
				} else {
					formCloseAll();
					openModal(thanksModal);
				}

				doTarget('feedback');
            } else {
                if(response.data){
					resultField.append(response.data).addClass('bs-callout-danger').removeClass('hidden');
                } else if(typeof response == 'object' && Object.keys(response).length){
					resultField.html('');
					var errors = '';
                    $.each(response, function(index, value){
						$.each(value, function(key, item){
							errors += '<li>' + item + '</li>';
							form.find('#' + index + '_em_').html(item).show();
						});
                    });
                    if(errors && resultField.hasClass('show-errors')){
						resultField.append('<p>Необходимо исправить следующие ошибки:</p><ul>' + errors + '</ul>');
						resultField.addClass('bs-callout-danger').removeClass('hidden');
					}
                }
            }
        },
        error: function () {
			resultField
                .html('Извините, при обработке запроса возникла ошибка')
			    .addClass('bs-callout-danger')
                .removeClass('hidden');
        }
    });

	form.find('button[type="submit"]').prop('disabled', false);
    return false;
}
