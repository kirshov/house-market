function feedbackSendForm(form) {

	var resultField = form.find('.result-message');
	resultField
		.html('')
		.removeClass('bs-callout-danger')
		.removeClass('bs-callout-success')
		.addClass('hidden');

	var inputs,
		processData = true,
		contentType = false;

	if(form.find('.input-file').length){
		inputs = new FormData();

		form.find(':input[name]').not('[type="file"]').each(function() {
			inputs.append($(this).attr('name'), $(this).val());
		});

		var filesField = form.find('input[type="file"]');
		var fileName = filesField.attr('name');
		var file = filesField.prop('files')[0];
		if(file){
			inputs.append(fileName, file);
		}

		processData = false;
	} else {
		inputs = form.serialize();
		contentType = 'application/x-www-form-urlencoded; charset=UTF-8';
	}
	$.ajax({
		url: form.attr('action'),
		type: 'POST',
		data: inputs,
		dataType: 'json',
		processData: processData,
		contentType: contentType,
		success: function (response) {
			if(response.result){
				form.trigger('reset');
				form.find('.is-file .file-item').html('Прикрепить файл');
				form.find('.is-file').removeClass('is-file');

				var thanksModal = $('.modal-window.modal-thanks');
				if(!thanksModal.length || form.closest('.modal-window').hasClass('modal-thanks')){
					resultField.html('<p>' + response.data.message + '</p>');
					resultField.addClass('bs-callout-success');

					if(form.closest('.popup-feedback').length){
						form.find('.modal-body .modal-inner').addClass('hidden');
					}
				} else {
					formCloseAll();
					openModal(thanksModal);
				}

				doTarget('feedback');
            } else {
                if(response.data){
					resultField.append(response.data).addClass('bs-callout-danger').removeClass('hidden');
                } else if(typeof response == 'object' && Object.keys(response).length){
					resultField.html('');
					var errors = '';
                    $.each(response, function(index, value){
						$.each(value, function(key, item){
							errors += '<li>' + item + '</li>';
							form.find('#' + index + '_em_').html(item).show();
						});
                    });
                    if(errors && resultField.hasClass('show-errors')){
						resultField.append('<p>Необходимо исправить следующие ошибки:</p><ul>' + errors + '</ul>');
						resultField.addClass('bs-callout-danger').removeClass('hidden');
					}
                }
            }
        },
        error: function () {
			resultField
                .html('Извините, при обработке запроса возникла ошибка')
			    .addClass('bs-callout-danger')
                .removeClass('hidden');
        }
    });
	form.find('button[type="submit"]').prop('disabled', false);
    return false;
}