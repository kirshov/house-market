<?php
$this->breadcrumbs = [
    Yii::t('FeedbackModule.feedback', 'Messages ') => ['/feedback/feedbackBackend/index'],
    Yii::t('FeedbackModule.feedback', 'Management'),
];

$this->pageTitle = Yii::t('FeedbackModule.feedback', 'Messages - manage');

$this->menu = [
    [
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('FeedbackModule.feedback', 'Messages management'),
        'url'   => ['/feedback/feedbackBackend/index']
    ],
    [
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('FeedbackModule.feedback', 'Create message '),
        'url'   => ['/feedback/feedbackBackend/create']
    ],
];
$assets = Yii::app()->getAssetManager()->publish(
    Yii::getPathOfAlias('feedback.views.assets')
);
Yii::app()->getClientScript()->registerScriptFile($assets . '/js/feedback.js');
Yii::app()->getClientScript()->registerCssFile($assets . '/css/feedback.css');

$mainCategory = $this->getModule()->mainCategory;
$useCategory = $this->getModule()->useCategory;
?>

<?php $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id'             => 'feed-back-list',
        'dataProvider'   => $model->search(),
        'filter'         => $model,
        'actionsButtons' => [
            CHtml::link(
                Yii::t('WebformaModule.webforma', 'Add'),
                ['/feedback/feedbackBackend/create'],
                ['class' => 'btn btn-success pull-right btn-sm']
            )
        ],
        'columns'        => [
            [
                'name' => 'theme',
                'type' => 'raw',
                'value' => function($data){
                	return CHtml::link($data->theme, Yii::app()->createUrl("/feedback/feedbackBackend/update", array("id" => $data->id)));
                },
                'filter'   => CHtml::activeTextField($model, 'theme', ['class' => 'form-control']),
            ],
            [
				'name' => 'name',
				'type' => 'raw',
				'value' => function($data){
					return CHtml::link($data->name, Yii::app()->createUrl("/feedback/feedbackBackend/update", array("id" => $data->id)));
				},
                'filter'   => CHtml::activeTextField($model, 'name', ['class' => 'form-control']),
            ],
			[
				'name' => 'phone',
				'type' => 'raw',
				'value' => function($data){
					return CHtml::link($data->phone, Yii::app()->createUrl("/feedback/feedbackBackend/update", array("id" => $data->id)));
				},
				'filter'   => CHtml::activeTextField($model, 'name', ['class' => 'form-control']),
			],
			[
				'name' => 'email',
				'type' => 'raw',
				'value' => function($data){
					return CHtml::link($data->email, Yii::app()->createUrl("/feedback/feedbackBackend/update", array("id" => $data->id)));
				},
				'filter'   => CHtml::activeTextField($model, 'name', ['class' => 'form-control']),
			],
			[
				'name'   => 'category_id',
				'value'  => '$data->getCategoryName()',
				'filter' => CHtml::activeDropDownList($model, 'category_id', Yii::app()->getComponent('categoriesRepository')->getFormattedList($mainCategory), [
				    'encode' => false,
                    'empty' => '',
                    'class' => 'form-control'
                ]),
				'visible' => $useCategory,
			],
            'create_time',
            [
                'class'   => 'webforma\widgets\EditableStatusColumn',
                'name'    => 'status',
                'url'     => $this->createUrl('/feedback/feedbackBackend/inline'),
                'source'  => $model->getStatusList(),
                'options' => [
                    FeedBack::STATUS_ANSWER_SENDED => ['class' => 'label-success'],
                    FeedBack::STATUS_FINISHED      => ['class' => 'label-success'],
                    FeedBack::STATUS_NEW           => ['class' => 'label-default'],
                    FeedBack::STATUS_PROCESS       => ['class' => 'label-info'],
                ],
            ],
            [
                'class'    => 'webforma\widgets\CustomButtonColumn',
                //'template' => '{answer}{view}{update}{delete}',
                'template' => '{view}{update}{delete}',
                'buttons'  => [
                    'answer' => [
                        'icon'  => 'fa fa-fw fa-envelope',
                        'label' => Yii::t('FeedbackModule.feedback', 'Messages - answer'),
                        'url'   => 'Yii::app()->createUrl("/feedback/feedbackBackend/update", array("id" => $data->id))',
                        'options' => ['class' => 'btn btn-sm btn-default']
                    ]
                ]
            ],
        ]
    ]
); ?>
