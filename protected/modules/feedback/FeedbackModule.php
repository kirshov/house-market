<?php

/**
 * FeedbackModule основной класс модуля feedback
 */

use webforma\components\WebModule;

/**
 * Class FeedbackModule
 */
class FeedbackModule extends WebModule
{
    /**
     * @var array
     */
    public $backEnd = ['DbFeedbackSender', 'EmailFeedbackSender'];
    /**
     * @var
     */
    public $emails;

    /**
     * @var int
     */
    public $showCaptcha = 0;
    /**
     * @var
     */
    public $notifyEmailFrom;
    /**
     * @var int
     */
    public $cacheTime = 60;
    /**
     * @var int
     */
    public $minCaptchaLength = 3;
    /**
     * @var int
     */
    public $maxCaptchaLength = 6;

	/**
	 * @var int
	 */
	public $adminMenuOrder = 5;

    /**
     *
     */
    const BACKEND_EMAIL = 'EmailFeedbackSender';
    /**
     *
     */
    const BACKEND_DB = 'DbFeedbackSender';

	/**
	 * @var
	 */
    public $mainCategory;

	/**
	 * @var int
	 */
    public $useCategory = 0;

	/**
	 * @var string
	 */
	public $message = 'Спасибо за Ваше обращения, мы свяжемся с Вами в ближайшее время';

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [
            'showCaptcha' => Yii::t('FeedbackModule.feedback', 'Show captcha'),
            'emails' => Yii::t('FeedbackModule.feedback', 'Message receivers (email, separated by comma)'),
            'notifyEmailFrom' => Yii::t('FeedbackModule.feedback', 'Email message will be send from'),
            'cacheTime' => Yii::t('FeedbackModule.feedback', 'Counter caching time (seconds)'),
            'mainCategory' => Yii::t('FeedbackModule.feedback', 'Main messages category'),
            'minCaptchaLength' => Yii::t('FeedbackModule.feedback', 'Minimum captcha length'),
            'maxCaptchaLength' => Yii::t('FeedbackModule.feedback', 'Maximum captcha length'),
			'useCategory' => 'Использовать категории',
			'message' => 'Сообщение об успешной отправке',
        ];
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
    	$module = Yii::app()->getModule('webforma');
        $params = [
	        'showCaptcha' => $this->getChoice(),
	        'notifyEmailFrom' => [
	        	'input-type' => 'text',
				'htmlOptions' => [
					'placeholder' => $module->notifyEmailFrom
				],
	        ],
	        'emails' => [
		        'input-type' => 'text',
		        'htmlOptions' => [
			        'placeholder' => $module->notifyEmailTo
		        ],
	        ],
			'message' => [
				'input-type' => 'textarea',
			]
        ];

        if(DEVMODE){
        	$params = CMap::mergeArray($params, [
				'useCategory' => [
					'input-type' => 'checkbox'
				],
				'mainCategory' => CHtml::listData($this->getCategoryList(), 'id', 'name'),
			]);
		}

        return $params;
    }

    /**
     * @return array
     */
    public function getEditableParamsGroups()
    {
        $groups =  [
            'main' => [
                'label' => Yii::t('FeedbackModule.feedback', 'General module settings'),
                'items' => [
                    'notifyEmailFrom',
                    'emails',
                    'message',
	                'showCaptcha',
                ],
            ],
        ];

		if(DEVMODE){
			$groups = CMap::mergeArray($groups, [
				'system' => [
					'label' => 'Системные настройки',
					'items' => [
						'useCategory',
						'mainCategory',
					],
				],
			]);
		}

        return $groups;
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/feedback/feedbackBackend/index';
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('FeedbackModule.feedback', 'Messages list'),
                'url' => ['/feedback/feedbackBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('FeedbackModule.feedback', 'Create message'),
                'url' => ['/feedback/feedbackBackend/create'],
            ],
            /*[
                'icon' => 'fa fa-fw fa-folder-open',
                'label' => Yii::t('FeedbackModule.feedback', 'Messages categories'),
                'url' => ['/category/categoryBackend/index', 'Category[parent_id]' => (int)$this->mainCategory],
            ],*/
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('FeedbackModule.feedback', 'Feedback');
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('FeedbackModule.feedback', 'Services');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('FeedbackModule.feedback', 'Module for feedback management');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-envelope-o';
    }

    /**
     * Возвращаем статус, устанавливать ли галку для установки модуля в инсталяторе по умолчанию:
     *
     * @return bool
     **/
    public function getIsInstallDefault()
    {
        return true;
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'feedback.models.*',
                'feedback.components.*',
                'feedback.events.*',
            ]
        );
    }
}
