<?php

/**
 * Class EmailFeedbackSender
 */
class EmailFeedbackSender extends DbFeedbackSender
{
    /**
     * @param IFeedbackForm $form
     * @return bool
     */
    public function send(IFeedbackForm $form)
    {
	    Yii::app()->getComponent('feedbackNotifyService')->sendCreatedAdminNotify($form);

        return true;
    }
}
