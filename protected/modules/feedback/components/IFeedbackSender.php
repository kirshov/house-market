<?php

/**
 * Interface IFeedbackSender
 */
interface IFeedbackSender
{
	/**
	 * @param IFeedbackForm $form
	 * @return mixed
	 */
	public function send(IFeedbackForm $form);
}
