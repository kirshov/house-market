<?php

class FeedbackNotifyService extends NotifyService
{

	/**
	 * @var string
	 */
	protected $templatePath  = 'feedback/email';

	/**
	 * @var string
	 */
	protected $messageComponent = 'feedbackMailMessage';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		$this->module = Yii::app()->getModule('feedback');
	}

	/**
	 * @param IFeedbackForm $model
	 * @return bool
	 */
	public function sendCreatedAdminNotify(IFeedbackForm $model)
	{
		$webforma = Yii::app()->getModule('webforma');
		$to = $this->module->emails ? $this->module->emails : $webforma->notifyEmailTo;
		$from = $this->module->notifyEmailFrom ? $this->module->notifyEmailFrom : $webforma->notifyEmailFrom;

		if(!$to || !$from){
			return false;
		}

		$mailContent = $this->getMailContent($model, 'newMessageAdmin', true);

		if($mailContent['subject'] && $mailContent['body']){
			foreach (explode(',', $to) as $email) {
				$this->mail->send($from, $email, $mailContent['subject'], $mailContent['body']);
			}
		}

		return true;
	}

	/**
	 * @param IFeedbackForm $model
	 * @return bool
	 */
	public function sendReplyUserNotify(IFeedbackForm $model)
	{
		if(!$model->email){
			return false;
		}
		$from = Yii::app()->getModule('webforma')->notifyEmailFrom;
		$to = $model->email;

		if(!$to || !$from){
			return false;
		}
		$code = 'replyCommentUser';

		$mailContent = $this->getMailContent($model, $code);

		if($mailContent['subject'] && $mailContent['body']){
			foreach (explode(',', $to) as $email) {
				$this->mail->send($from, $email, $mailContent['subject'], $mailContent['body']);
			}
		}

		return true;
	}

}