<?php
class FeedbackMailMessage extends WMailMessage{

	/**
	 * @var string
	 */
	public $icon = 'fa-envelope-o';

	/**
	 * @var FeedBack
	 */
	protected $model;

	/**
	 * @var string
	 */
	protected $module = 'feedback';

	/**
	 * @return array
	 */
	public function getDefaultMessages()
	{
		return [
			'newMessageAdmin' => [
				'name' => 'Сообщение администратору о новом сообщении через обратную связь',
				'subject' => 'Получено новое сообщение на сайте "%siteName%"',
				'body' => '<h2 style="'.$this->getStyle('title').'">Получено новое сообщение</h2>
					<h3 style="'.$this->getStyle('subtitle').'">на сайте "%siteName%"</h3>
					<br/>
					%info%
				',
			],

			/*'replyCommentUser' => [
				'name' => 'Сообщение пользователю об ответе на комментарий',
				'subject' => 'Получен ответ на ваш комментарий на сайте "%siteName%"',
				'body' => '<h2>Ответ на комментарий на сайте "%siteName%"</h2>
					<p style="font: %font%">
						Текст ответа: %answer%<br/>
					</p>
					<div align="center">%link%</div>
				',
			],*/
		];
	}

	/**
	 * @param $code
	 * @param bool $isAdmin
	 * @return mixed|string
	 */
	public function getContentBlock($code, $isAdmin = false)
	{
		switch ($code){
			case '%info%':
				return $this->includeContent('feedback.emailTemplate.info', ['model' => $this->model, 'isAdmin' => $isAdmin]);
			default:
				return parent::getContentBlock($code, $isAdmin);
		}
	}
}
