<?php

use webforma\components\Mail;

/**
 * Class DbFeedbackSender
 */
class DbFeedbackSender implements IFeedbackSender
{
    /**
     * @var Mail
     */
    protected $mail;

    /**
     * @var FeedbackModule
     */
    protected $module;

    /**
     * DbFeedbackSender constructor.
     * @param Mail $mail
     * @param FeedbackModule $module
     */
    public function __construct(Mail $mail, FeedbackModule $module)
    {
        $this->mail = $mail;

        $this->module = $module;
    }

    /**
     * @param IFeedbackForm $form
     * @return bool
     */
    public function send(IFeedbackForm $form)
    {
        $feedback = new FeedBack();

        $feedback->setAttributes(
            [
                'name' => $form->getName(),
                'email' => $form->getEmail(),
                'theme' => $form->getTheme(),
                'text' => $form->getText(),
                'phone' => $form->getPhone(),
            ]
        );

        if ($feedback->save()) {
            return true;
        }

        return false;
    }
}
