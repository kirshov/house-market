<?php
use webforma\widgets\WWidget;

/**
 * Class FeedbackWidget
 */
class FeedbackWidget extends WWidget
{
    /**
     * @var string
     */
    public $view = 'default';

	/**
	 * @var
	 */
    public $form;

	/**
	 * @var
	 */
	public $theme = 'Заявка с сайта';

	/**
	 * @var string
	 */
	public $title = 'Оставьте заявку';

	/**
	 * @var
	 */
	public $hasFile = false;

	/**
	 * @throws CException
	 */
	public function init()
	{
		Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getAssetManager()->publish(
			Yii::getPathOfAlias('application.modules.feedback.views.assets.js') . ($this->hasFile ? '/feedbackAjaxFile.js' : '/feedbackAjax.js')
		), CClientScript::POS_END);

		parent::init();
	}

    /**
     * @throws CException
     */
    public function run()
    {
    	$modelName = $this->form ?: 'FeedBackForm';
    	if($this->view == 'ipoteka' || $this->view == 'ipoteka-popup'){
    		$modelName = 'FeedBackIpotekaForm';

			$cs = Yii::app()->getClientScript();
			$cs->registerCoreScript('jquery.ui');
			$cs->registerCssFile($cs->getCoreScriptUrl(). '/jui/css/base/jquery-ui.css');
		}
        $model = new $modelName();
        $module = Yii::app()->getModule('feedback');

        if (Yii::app()->getUser()->isAuthenticated()) {
	        $user = Yii::app()->getUser()->getProfile();
	        $model->setAttributes([
		        'name' => $user->getFullName(),
		        'email' => $user->email,
		        'phone' => $user->phone,
	        ]);
        }

        $this->render($this->view, [
            'model' => $model,
            'module' => $module,
	        'formClass' => $this->form,
	        'theme' => $this->theme,
	        'title' => $this->title,
        ]);
    }
}