<?/**
 * @var int $newFeedbackCount
 * @var int $needAnswerCount
 * @var int $allFeedbackCount
 */?>
<div class="col-md-4 col-xs-12 col-sm-6">
	<div class="panel panel-info panel-dark">
		<div class="panel-heading">
			<h3 class="widget-profile-header">
				Сообщения
			</h3>
			<i class="panel-icon fa fa-fw fa-envelope-o"></i>
		</div>
		<div class="widget-profile-counters">
			<a href="<?=Yii::app()->createUrl('/backend/feedback/feedback/')?>?FeedBack[status]=<?=FeedBack::STATUS_NEW?>" class="col-xs-4">
				<span class="widget-profile-counter<?=($newFeedbackCount ? ' text-danger' : '')?>"><?=$newFeedbackCount?></span>
				Новых
			</a>
			<a href="<?=Yii::app()->createUrl('/backend/feedback/feedback/')?>?FeedBack[status]=<?=FeedBack::STATUS_PROCESS?>" class="col-xs-4">
				<span class="widget-profile-counter<?=($needAnswerCount ? ' text-info' : '')?>"><?=$needAnswerCount?></span>
				Ждет ответа
			</a>
			<a href="<?=Yii::app()->createUrl('/backend/feedback/feedback/')?>" class="col-xs-4">
				<span class="widget-profile-counter"><?=$allFeedbackCount?></span>
				Всего
			</a>
		</div>
		<div class="panel-footer">
			<a href="<?=Yii::app()->createUrl('/backend/feedback/feedback/')?>">Перейти к сообщениям</a>
		</div>
	</div>
</div>