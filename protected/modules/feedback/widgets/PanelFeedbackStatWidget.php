<?php

/**
 * Class PanelFeedbackStatWidget
 */
class PanelFeedbackStatWidget extends \webforma\widgets\WWidget
{
    /**
     * @throws CException
     */
    public function run()
    {
        $cacheTime = Yii::app()->getController()->webforma->coreCacheTime;

        $this->render('panel-feedback-stat', [
            /*'feedbackCount'    => FeedBack::model()->cache($cacheTime)->count(
                    'create_time >= :time',
                    [':time' => time() - 24 * 60 * 60]
                ),*/
            'allFeedbackCount' => FeedBack::model()->cache($cacheTime)->count(),
            'newFeedbackCount'  => FeedBack::model()->new()->cache($cacheTime)->count(),
            'needAnswerCount'  => FeedBack::model()->needAnswer()->cache($cacheTime)->count(),
        ]);
    }
}
