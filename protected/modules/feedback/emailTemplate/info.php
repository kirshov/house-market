<?$c=1;?>
<table cellpadding="6" cellspacing="0" style="border-collapse: collapse; width: 100%;">
	<?php if ($model->theme): ?>
		<tr>
			<td style="%info-table-<?=Helper::getParity($c);?>% width: 173px;">
				Тема
			</td>
			<td style="%info-table-<?=Helper::getParity($c);?>%">
				<?= CHtml::encode($model->theme); ?>
			</td>
		</tr>
		<?$c++;?>
	<?php endif; ?>
	<?php if ($model->name): ?>
		<tr>
			<td style="%info-table-<?=Helper::getParity($c);?>% width: 173px;">
				Имя и фамилия
			</td>
			<td style="%info-table-<?=Helper::getParity($c);?>%">
				<?= CHtml::encode($model->name); ?>
			</td>
		</tr>
		<?$c++;?>
	<?php endif; ?>
	<?php if ($model->email): ?>
		<tr>
			<td style="%info-table-<?=Helper::getParity($c);?>% width: 173px;">
				E-mail
			</td>
			<td style="%info-table-<?=Helper::getParity($c);?>%">
				<?= CHtml::encode($model->email); ?>
			</td>
		</tr>
		<?$c++;?>
	<?php endif; ?>
	<?php if ($model->phone): ?>
		<tr>
			<td style="%info-table-<?=Helper::getParity($c);?>% width: 173px;">
				Телефон
			</td>
			<td style="%info-table-<?=Helper::getParity($c);?>%">
				<?= CHtml::encode($model->phone); ?>
			</td>
		</tr>
		<?$c++;?>
	<?php endif; ?>
	<tr>
		<td style="%info-table-<?=Helper::getParity($c);?>% width: 173px;">
			IP-адрес
		</td>
		<td style="%info-table-<?=Helper::getParity($c);?>%">
			<?= Yii::app()->getRequest()->getUserHostAddress(); ?>
		</td>
		<?$c++;?>
	</tr>
	<?php if ($model->getText()): ?>
		<tr>
			<td style="%info-table-<?=Helper::getParity($c);?>% width: 173px;">
				Текст сообщения
			</td>
			<td style="%info-table-<?=Helper::getParity($c);?>%">
				<?= nl2br($model->getText()); ?>
			</td>
		</tr>
		<?$c++;?>
	<?php endif; ?>
	<?if($isAdmin && Yii::app()->hasModule('utm') && Yii::app()->getComponent('utm')->has()):?>
		<?foreach (Yii::app()->getComponent('utm')->get() as $title => $value):?>
			<tr>
				<td style="%info-table-<?=Helper::getParity($c);?>% width: 173px;">
					<?=$title?>
				</td>
				<td style="%info-table-<?=Helper::getParity($c);?>%">
					<?= CHtml::encode($value); ?>
				</td>
			</tr>
			<?$c++;?>
		<?endforeach;?>
	<?endif?>
</table>
