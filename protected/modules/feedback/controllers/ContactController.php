<?php

/**
 * ContactController контроллер публичной части для формы контактов
 **/

use webforma\widgets\WFlashMessages;

/**
 * Class ContactController
 */
class ContactController extends \webforma\components\controllers\FrontController
{

    /**
     * @var FeedbackService
     */
    protected $feedback;

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->feedback = Yii::app()->getComponent('feedback');
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'webforma\components\actions\WCaptchaAction',
                'backColor' => 0xFFFFFF,
                'testLimit' => 1,
            ],
        ];
    }

    public function actionIndex()
    {
    	if(!Yii::app()->getRequest()->getIsAjaxRequest()){
		    throw new CHttpException(404, Yii::t('FeedbackModule.feedback', 'Page was not found!'));
	    }
    	$formClass = Yii::app()->getRequest()->getPost('formClass', 'FeedBackForm');

		/**
		 * @var FeedBackForm $form
		 */
        $form = new $formClass();

        // если пользователь авторизован - подставить его данные
        if (Yii::app()->getUser()->isAuthenticated()) {
            $form->email = Yii::app()->getUser()->getProFileField('email');
            $form->name = Yii::app()->getUser()->getProFileField('nick_name');
        }

        $module = Yii::app()->getModule('feedback');

        if (Yii::app()->getRequest()->getIsPostRequest() && !empty($_POST[$formClass])) {

            $form->setAttributes(Yii::app()->getRequest()->getPost($formClass));

            if($theme = Yii::app()->getRequest()->getPost('theme')){
	            $form->theme = $theme;
            }

            if(!empty($form->fields)){
	            $form->text = $form->getPreparedText($formClass);
            }

            if(!$form->theme){
	            $form->theme = 'Заявка с сайта';
            }
	    
	    	$message = $module->message;

            if(Yii::app()->hasModule('forms')){
	            $formId = (int) Yii::app()->getRequest()->getPost('formId');
	            Yii::import('application.modules.forms.models.Forms');

	            $formsModel = Forms::model()->findByPk($formId);
	            if($formsModel){
	            	if($formsModel->message){
	            		$message = $formsModel->message;
		            }

		            if($formsModel->theme){
			            $form->theme = $formsModel->theme;
		            }
	            }
            }

            if ($form->validate()) {

                if ($this->feedback->send($form, $module)) {

                    if (Yii::app()->getRequest()->getIsAjaxRequest()) {
                        Yii::app()->ajax->success(['message' => $message]);
                    }

                    Yii::app()->getUser()->setFlash(WFlashMessages::SUCCESS_MESSAGE, $message);

                    $this->redirect($module->successPage ? [$module->successPage] : ['/feedback/contact/index/']);
                } else {
                    if (Yii::app()->getRequest()->getIsAjaxRequest()) {
                        Yii::app()->ajax->failure(Yii::t('FeedbackModule.feedback', 'It is not possible to send message!'));
                    }

                    Yii::app()->getUser()->setFlash(WFlashMessages::ERROR_MESSAGE, Yii::t('FeedbackModule.feedback', 'It is not possible to send message!'));

                }
            } else {
                if (Yii::app()->getRequest()->getIsAjaxRequest()) {
                    Yii::app()->ajax->rawText(CActiveForm::validate($form));
                }
            }
        }

        //$this->render('index', ['model' => $form, 'module' => $module]);
    }
}
