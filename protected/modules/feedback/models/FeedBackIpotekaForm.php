<?php

/**
 * FeedBackContactsForm форма обратной связи для публичной части сайта
 **/
class FeedBackIpotekaForm extends FeedBackForm implements IFeedbackForm
{
	/**
     * @var
     */
    public $mother;

	/**
	 * @var
	 */
	public $pay;

	/**
	 * @var
	 */
	public $price;

	/**
	 * @var
	 */
	public $deadline;

	/**
	 * @var array
	 */
	public $fields = ['mother', 'pay', 'price', 'deadline'];

    /**
     * @return array
     */
    public function rules()
    {
    	$rules = parent::rules();

    	return Helper::mergeArray($rules, [
			['pay, price, deadline', 'required'],
			['mother', 'filter', 'filter' => [$obj = new \webforma\widgets\WPurifier(), 'purify']],
		]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя и фамилия',
            'email' => Yii::t('FeedbackModule.feedback', 'Email'),
            'phone' => 'Телефон',
            'theme' => Yii::t('FeedbackModule.feedback', 'Topic'),
            'text' => Yii::t('FeedbackModule.feedback', 'Text'),
            'verifyCode' => Yii::t('FeedbackModule.feedback', 'Check code'),
		 	'mother' => 'Использовать материнский капитал',
		 	'pay' => 'Первоначальный взнос',
		 	'price' => 'Стоимость дома',
		 	'deadline' => 'Срок, мес.',
		];
    }
}
