<?php

/**
 * FeedBackContantsForm форма обратной связи для публичной части сайта
 **/
class FeedBackForm extends CFormModel implements IFeedbackForm
{
    /**
     * @var
     */
    public $name;
    /**
     * @var
     */
    public $email;
    /**
     * @var
     */
    public $phone;
    /**
     * @var
     */
    public $theme;
    /**
     * @var
     */
    public $text;
	/**
	 * @var
	 */
	public $attachments;
    /**
     * @var
     */
    public $verifyCode;

	/**
     * @var
     */
    public $company;


	/**
	 * @var array
	 */
	public $fields = ['company'];

    /**
     * @return array
     */
    public function rules()
    {
        $module = Yii::app()->getModule('feedback');

        return [
            ['name, phone', 'required'],
            ['name, email, phone', 'length', 'max' => 150],
            ['theme', 'length', 'max' => 250],
			['name, email, theme, text, phone, company', 'filter', 'filter' => [$obj = new \webforma\widgets\WPurifier(), 'purify']],
            ['email', 'email'],
	        [
		        'phone',
		        'match',
		        'pattern' => Yii::app()->getModule('webforma')->phonePattern,
		        'message' => Yii::t('WebformaModule.webforma', 'Incorrect phone value'),
	        ],
            [
                'verifyCode',
                'webforma\components\validators\YRequiredValidator',
                'allowEmpty' => !$module->showCaptcha || Yii::app()->getUser()->isAuthenticated(),
            ],
            [
                'verifyCode',
                'captcha',
                'allowEmpty' => !$module->showCaptcha || Yii::app()->getUser()->isAuthenticated(),
            ],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'email' => Yii::t('FeedbackModule.feedback', 'Email'),
            'phone' => Yii::t('FeedbackModule.feedback', 'Phone'),
            'theme' => Yii::t('FeedbackModule.feedback', 'Topic'),
            'text' => Yii::t('FeedbackModule.feedback', 'Text'),
            'verifyCode' => Yii::t('FeedbackModule.feedback', 'Check code'),
		 	'company' => 'Компания',
        ];
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return str_replace("\n", '<br/>', $this->text);
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

	/**
	 * @param string $formClass
	 * @return string
	 */
	public function getPreparedText($formClass = 'FeedBackForm')
	{
		$post = Yii::app()->getRequest()->getPost($formClass);
		$text = $this->text;
		$addedText = '';
		foreach ($this->fields as $field){
			if($post[$field]){
				$addedText .= $this->getAttributeLabel($field).': '.$post[$field].'<br/>';
			}
		}

		if($addedText){
			$text .= ($this->text ? '<br/>' : '').$addedText;
		}

		return $text;
	}

}
