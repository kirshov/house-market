<?php
$this->breadcrumbs = [
    Yii::t('RbacModule.rbac', 'RBAC') => ['index'],
    Yii::t('RbacModule.rbac', 'Roles list'),
];

$this->menu = CMap::mergeArray(
	[[
		'icon'  => $this->module->getIcon(),
		'label' => Yii::t('RbacModule.rbac', 'Roles list'),
		'url'   => [$this->module->getAdminPageLink()]
	]],
	Yii::app()->getModule('user')->getNavigation()
);
?>


<?php
Yii::import('bootstrap.widgets.TbExtendedGridView');
$this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id'           => 'roles-grid',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'actionsButtons' => [
        	CHtml::link(Yii::t('WebformaModule.webforma', 'Add'), ['/rbac/rbacBackend/create'], ['class' => 'btn btn-success pull-right btn-sm']),
		],
        'bulkActions' => '',
        'columns'      => [
			[
				'class'    => 'webforma\widgets\WEditableColumn',
				'name'     => 'name',
				'editable' => [
					'url'    => $this->createUrl('/rbac/rbacBackend/inline'),
					'mode'   => 'inline',
					'params' => [
						Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
					]
				],
				'filter'   => CHtml::activeTextField($model, 'name', ['class' => 'form-control']),
			],
            [
                'filter' => false,
                'value'  => function ($data) {
                        echo CHtml::link(
                            Yii::t('RbacModule.rbac', 'Rules'),
                            ['/rbac/rbacBackend/assign', 'id' => $data->id],
                            ['class' => 'btn btn-default btn-small']
                        );
                    }
            ],
        ],
    ]
);
?>
