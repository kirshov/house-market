<?php
/**
 * @var Roles $model
 * @var \webforma\components\WebModule[] $modules
 * @var array $assigns
 */
$this->breadcrumbs = [
    Yii::t('RbacModule.rbac', 'RBAC')   => ['index'],
    Yii::t('RbacModule.rbac', 'Rights assignment').' "'.$model->name.'"',
];

$this->menu = CMap::mergeArray(
	[[
		'icon'  => $this->module->getIcon(),
		'label' => Yii::t('RbacModule.rbac', 'Roles list'),
		'url'   => [$this->module->getAdminPageLink()]
	]],
	Yii::app()->getModule('user')->getNavigation()
);

$actionsModules = RolesAssign::getModulesActions();

$form = $this->beginWidget(
	'bootstrap.widgets.TbActiveForm',
	[
		'id'                   => 'assign-form',
		'enableAjaxValidation' => false,
		'htmlOptions'          => [
			'class' => 'well sticky',
		],
	]
); ?>

<table class="assign-table">
	<thead>
		<tr>
			<td class="full-check">Модуль</td>
			<? foreach (RolesAssign::getActions() as $_action => $_title):?>
				<td class="all-check" data-check="<?=$_action?>"><?=$_title?></td>
			<?endforeach;?>
		</tr>
	</thead>
	<tbody>
		<?foreach ($modules as $moduleId => $module):?>
			<tr>
				<td class="all-check" data-check="<?=$moduleId;?>"><?=$module->RBACname ?: $module->getName();?></td>
				<?foreach (RolesAssign::getActions() as $_action => $_title):?>
					<td class="text-center">
						<?if($actionsModules[$moduleId] && !in_array($_action, $actionsModules[$moduleId])):?>
							<?=CHtml::checkBox('', '', ['disabled' => true]);?>
							<?=CHtml::hiddenField('assign['.$moduleId.']['.$_action.']', 0);?>
						<?else:?>
							<?=CHtml::checkBox('assign['.$moduleId.']['.$_action.']', $assigns[$moduleId][$_action], ['class' => $moduleId.' '.$_action, 'uncheckValue' => 0]);?>
						<?endif;?>
					</td>
				<?endforeach;?>
			</tr>
		<?endforeach;?>
	</tbody>
</table>

<div class="buttons">
    <?php
    $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context'    => 'primary',
		'label'      => Yii::t('RbacModule.rbac', 'Save'),
	]);
	?>
</div>

<?php $this->endWidget(); ?>
<style type="text/css">
	.all-check, .full-check{cursor: pointer;}
</style>
<script type="text/javascript">
	$(document).ready(function () {
		$('.all-check').on('click', function () {
			if($(this).data('check')){
				var checkClass = $(this).data('check'),
					items = $('.assign-table .' + checkClass),
					count = items.length,
					countChecked = items.filter(':checked').length;

				if(count == countChecked){
					items.prop('checked', false);
				} else {
					items.prop('checked', true);
				}
			}
		});

		$('.full-check').on('click', function () {
			var items = $('.assign-table input:checkbox'),
				count = items.length,
				countChecked = items.filter(':checked').length;

			if (count == countChecked) {
				items.prop('checked', false);
			} else {
				items.prop('checked', true);
			}
		});
	});
</script>
