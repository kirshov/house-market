<?php $form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'id'                   => 'auth-item-form',
        'enableAjaxValidation' => false,
        'htmlOptions'          => [
            'class' => 'well',
        ],
    ]
); ?>

<div class="alert alert-info">
    <?=  Yii::t('RbacModule.rbac', 'Fields marked with'); ?>
    <span class="required">*</span>
    <?=  Yii::t('RbacModule.rbac', 'are required.'); ?>
</div>

<?=  $form->errorSummary($model); ?>

<div class="row">
    <div class="col-sm-5">
        <?=  $form->textFieldGroup($model, 'name'); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-5">
        <?=  $form->textAreaGroup($model, 'description'); ?>
    </div>
</div>

<div id="operations-list" style="display:none;">
    <p><b><?=  Yii::t('RbacModule.rbac', 'Operations') ?>:</b></p>

    <div class="row">
        <div class="col-sm-5">
            <?=  CHtml::textField(
                'search',
                '',
                ['class' => 'form-control', 'placeholder' => Yii::t('RbacModule.rbac', 'Filter')]
            ); ?>
        </div>
    </div>
    <p>
        <?=  CHtml::link(Yii::t('RbacModule.rbac', 'Select all'), '#', ['id' => 'check-all']); ?>
        <?=  CHtml::link(Yii::t('RbacModule.rbac', 'Clear all'), '#', ['id' => 'uncheck-all']); ?>
    </p>
    <?php foreach ($operations as $k => $v): { ?>
        <div class="row operation">
            <div class="col-sm-7">
                <div class="checkbox">
                    <label>
                        <?=  CHtml::checkBox(
                            'ChildAuthItems[]',
                            isset($checkedList[$k]),
                            ['class' => 'item', 'value' => $k, 'id' => $k]
                        ); ?>
                        <?=  $v; ?>
                    </label>
                </div>
            </div>
        </div>
    <?php } endforeach; ?>
</div>

<div id="tasks-list" style="display:none;">
    <p><b>Задачи:</b></p>
    <?php foreach ($tasks as $k => $v): { ?>
        <div class="row operation">
            <div class="col-sm-7">
                <div class="checkbox">
                    <label>
                        <?=  CHtml::checkBox(
                            'ChildAuthItems[]',
                            isset($checkedList[$k]),
                            ['class' => 'item', 'value' => $k, 'id' => $k]
                        ); ?>
                        <?=  $v; ?>
                    </label>
                </div>
            </div>
        </div>
    <?php } endforeach; ?>
</div>

<div id="roles-list" style="display:none;">
    <p><b>Роли:</b></p>
    <?php foreach ($roles as $k => $v): { ?>
        <div class="row operation">
            <div class="col-sm-7">
                <div class="checkbox">
                    <label>
                        <?=  CHtml::checkBox(
                            'ChildAuthItems[]',
                            isset($checkedList[$k]),
                            ['class' => 'item', 'value' => $k, 'id' => $k]
                        ); ?>
                        <?=  $v; ?>
                    </label>
                </div>
            </div>
        </div>
    <?php } endforeach; ?>
</div>

<br/>

<?php
$this->widget(
    'bootstrap.widgets.TbButton',
    [
        'buttonType' => 'submit',
        'context'    => 'primary',
        'label'      => $model->isNewRecord ? Yii::t('RbacModule.blog', 'Создать') : Yii::t('RbacModule.rbac', 'Save'),
    ]
);
?>

<?php $this->endWidget(); ?>
