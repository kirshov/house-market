<?php
$this->breadcrumbs = [
    Yii::t('RbacModule.rbac', 'RBAC') => ['index'],
    Yii::t('RbacModule.rbac', 'Addition'),
];

$this->menu = CMap::mergeArray(
	[[
		'icon'  => $this->module->getIcon(),
		'label' => Yii::t('RbacModule.rbac', 'Roles list'),
		'url'   => [$this->module->getAdminPageLink()]
	]],
	Yii::app()->getModule('user')->getNavigation()
);
?>


<?=  $this->renderPartial(
    '_form',
    ['model' => $model]
); ?>
