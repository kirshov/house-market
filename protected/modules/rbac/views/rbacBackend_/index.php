<?php
$this->breadcrumbs = [
    Yii::t('RbacModule.rbac', 'RBAC') => ['index'],
    Yii::t('RbacModule.rbac', 'Manage'),
];

$this->menu = $this->module->getNavigation();
?>

<h3><?=  Yii::t('RbacModule.rbac', 'Manage operations'); ?></h3>

<?php $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id'           => 'auth-item-grid',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'actionsButtons' => [
            CHtml::link(
                Yii::t('WebformaModule.webforma', 'Add'),
                ['/rbac/rbacBackend/create'],
                ['class' => 'btn btn-success pull-right btn-sm']
            )
        ],
        'columns'      => [
            [
                'class'=>'CCheckBoxColumn',
            ],
            [
                'name'  => 'name',
                'value' => 'CHtml::link($data->name, array("/rbac/rbacBackend/update", "id" => $data->name))',
                'type'  => 'html'
            ],
            [
                'name'              => 'description',
                'class'             => 'webforma\widgets\WEditableColumn',
                'headerHtmlOptions' => ['style' => 'width:500px'],
                'editable'          => [
                    'type'   => 'text',
                    'url'    => ['/rbac/rbacBackend/inlineEdit'],
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter'            => CHtml::activeTextField($model, 'description', ['class' => 'form-control']),
            ],
            [
                'name'     => 'type',
                'filter'   => CHtml::activeDropDownList(
                        $model,
                        'type',
                        AuthItem::model()->getTypeList(),
                        ['class' => 'form-control', 'empty' => '']
                    ),
                'value'    => '$data->getType()',
                'class'    => 'webforma\widgets\WEditableColumn',
                'editable' => [
                    'url'    => $this->createUrl('/rbac/rbacBackend/inlineEdit'),
                    'mode'   => 'popup',
                    'type'   => 'select',
                    'source' => AuthItem::model()->getTypeList(),
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'type'     => 'raw',
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
            ],
        ],
    ]
); ?>
