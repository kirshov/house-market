<?php

class RbacBackendController extends webforma\components\controllers\BackController
{
    public function accessRules()
    {
        return [
            ['allow', 'roles' => [Roles::ROLE_ADMIN]],
            ['deny',]
        ];
    }

    public function actions()
    {
        return [
            'inline' => [
                'class'           => 'webforma\components\actions\WInLineEditAction',
                'model'           => 'Roles',
                'validAttributes' => ['name'],
            ]
        ];
    }

    /*public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id),]);
    }*/

    public function actionAssign($id)
    {
       $model = $this->loadModel($id);

        if ($model && Yii::app()->getRequest()->isPostRequest) {
            $transaction = Yii::app()->db->beginTransaction();

            try {
                RolesAssign::model()->deleteAll('role_id = :role', [':role' => (int)$model->id]);

				$assigns = Yii::app()->getRequest()->getPost('assign');
                foreach ($assigns as $_module => $_actions) {
                	$assignModel = new RolesAssign();
                	$assignModel->role_id = (int) $model->id;
                	$assignModel->module = $_module;
                	foreach ($_actions as $_action => $_value){
						$assignModel->{$_action} = $_value;
					}

                    if (!$assignModel->save()) {
                        throw new CDbException(Yii::t(
                            'RbacModule.rbac',
                            'There is an error occurred when saving data!'
                        ));
                    }
                }

                $transaction->commit();

                Yii::app()->getUser()->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('RbacModule.rbac', 'Data was updated!')
                );

                /*сброс кэша меню*/
                Yii::app()->getCache()->delete('WAdminPanel::' . $id);

                /*сброс кеша прав*/
                Yii::app()->getCache()->delete(Yii::app()->getUser()->rbacCacheNameSpace . $id);

                $this->redirect(['assign', 'id' => $model->id]);
            } catch (Exception $e) {

                Yii::app()->getUser()->setFlash(webforma\widgets\WFlashMessages::ERROR_MESSAGE, $e->getMessage());
                $transaction->rollback();
            }
        }

        $modules = Yii::app()->getComponent('moduleManager')->getRBACModules();
        $assignData = $model->assign();
        $assigns = [];
        foreach ($assignData as $assignItem){
        	$assigns[$assignItem->module] = $assignItem->getAttributes(array_keys(RolesAssign::getActions()));
		}

        $this->render('assign', [
        	'modules' => $modules,
			'model' => $model,
			'assigns' => $assigns,
		]);
    }

	/**
	 *
	 */
    public function actionCreate()
    {
        $model = new Roles();

        if (Yii::app()->getRequest()->isPostRequest && isset($_POST['Roles'])) {
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model->attributes = Yii::app()->getRequest()->getPost('Roles');
                if ($model->save()) {
                    $transaction->commit();

                    Yii::app()->getUser()->setFlash(
                        webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                        Yii::t('RbacModule.rbac', 'The item is added!')
                    );
                    $this->redirect(['index']);
                }
            } catch (Exception $e) {
                Yii::app()->getUser()->setFlash('error', $e->getMessage());
                $transaction->rollback();
            }
        }

        $this->render('create', [
			'model'      => $model,
		]);
    }

    public function actionUpdate($id)
    {
        /* @var $model AuthItem */
        $model = AuthItem::model()->findByPk($id);

        $checkedList = [];
        foreach ($model->children as $item) {
            $checkedList[$item->childItem->name] = true;
        }

        if (Yii::app()->getRequest()->isPostRequest && isset($_POST['AuthItem'])) {
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model->attributes = $_POST['AuthItem'];
                if ($model->save()) {
                    $this->updateAuthItemChildren($model);

                    $transaction->commit();

                    Yii::app()->getUser()->setFlash(
                        webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                        Yii::t('RbacModule.rbac', 'The item is changed!')
                    );
                    $this->redirect(['update', 'id' => $model->name]);
                }
            } catch (Exception $e) {
                Yii::app()->getUser()->setFlash('error', $e->getMessage());
                $transaction->rollback();
            }
        }

        $rbacTree = new RbacTree();
        $this->render(
            'update',
            [
                'model'       => $model,
                'operations'  => $rbacTree->getItemsList(AuthItem::TYPE_OPERATION),
                'tasks'       => $rbacTree->getItemsList(AuthItem::TYPE_TASK),
                'roles'       => $rbacTree->getItemsList(AuthItem::TYPE_ROLE),
                'checkedList' => $checkedList,

            ]
        );
    }

	/**
	 * @param $id
	 * @throws CHttpException
	 */
    public function actionDelete($id)
    {
        if (!Yii::app()->getRequest()->isPostRequest) {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
        try {
            $this->loadModel($id)->delete();
        } catch (CDbException $e) {
            throw new CHttpException(Yii::t('RbacModule.rbac', 'There is an error occurred when deleting data!'));
        }

        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : ['index']);
        }
    }

    public function actionIndex()
    {
        $model = new Roles('search');
        $model->unsetAttributes();
        if (isset($_GET['Roles'])) {
            $model->attributes = $_GET['Roles'];
        }

        $this->render('index', ['model' => $model,]);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param $id integer The ID of the model to be loaded
     * @return Roles
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Roles::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('RbacModule.rbac', 'Page was not found'));
        }

        return $model;
    }
}
