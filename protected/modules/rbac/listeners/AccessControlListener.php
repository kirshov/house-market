<?php

use webforma\components\Event;

class AccessControlListener
{
    public static function onBackendControllerInit(Event $event)
    {
        $filters = Yii::app()->getModule('webforma')->getBackendFilters();
        $filters = array_replace(
            $filters,
            array_fill_keys(
                array_keys($filters, ['webforma\filters\WBackAccessControl']),
                ['rbac\filters\RbacBackAccessControl']
            )
        );
        Yii::app()->getModule('webforma')->setBackendFilters($filters);
        Yii::app()->getModule('webforma')->addBackendFilter('accessControl');
    }
}
