<?php

use webforma\components\WebModule;

/**
 * Class RbacModule
 */
class RbacModule extends WebModule
{
	/**
	 * @var int
	 */
	public $adminMenuOrder = 30;

    /**
     *
     */
    public function init()
    {
        $this->setImport(
            [
                'rbac.models.*',
                'rbac.components.*',
            ]
        );

        parent::init();
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/rbac/rbacBackend/index';
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
    	return false;
        return [
            ['label' => Yii::t('RbacModule.rbac', 'Operations')],
            [
                'icon' => 'fa fa-fw fa-align-left',
                'label' => Yii::t('RbacModule.rbac', 'Manage operations'),
                'url' => ['/rbac/rbacBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('RbacModule.rbac', 'Create operation'),
                'url' => ['/rbac/rbacBackend/create'],
            ],
            ['label' => Yii::t('RbacModule.rbac', 'Roles')],
            [
                'icon' => 'fa fa-fw fa-user',
                'label' => Yii::t('RbacModule.rbac', 'Assign roles'),
                'url' => ['/rbac/rbacBackend/userList'],
            ],
            ['label' => Yii::t('RbacModule.rbac', 'RBAC')],
            [
                'icon' => 'fa fa-fw fa-magnet',
                'label' => Yii::t('RbacModule.rbac', 'Import RBAC'),
                'url' => ['/rbac/rbacBackend/import'],
            ],
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('RbacModule.rbac', 'RBAC');
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('RbacModule.rbac', 'Users');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('RbacModule.rbac', 'Module for user rights and permissions');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-check';
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return ['user'];
    }
}
