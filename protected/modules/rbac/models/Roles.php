<?php

/**
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $status
 *
 * The followings are the available model relations:
 * @method Roles[] active
 * @method RolesAssign[] assign
 */
class Roles extends CActiveRecord
{
	/**
	 *
	 */
	const STATUS_PUBLISHED = 1;

	/**
	 *
	 */
	const STATUS_UNPUBLISHED = 0;

	/**
	 *
	 */
	const ROLE_ADMIN = 'admin';

    /**
     * Returns the static model of the specified AR class.
     * @param  string $className active record class name.
     * @return AuthItem the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{user_roles}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['name', 'required'],
            ['name', 'length', 'max' => 255],
            ['description', 'safe'],
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            ['id, name, description', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'assign' => [self::HAS_MANY, 'RolesAssign', 'role_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'name'        => Yii::t('RbacModule.rbac', 'Название'),
            'description' => Yii::t('RbacModule.rbac', 'Описание'),
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria();
        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('description', $this->description, true);

        return new CActiveDataProvider($this, [
            'criteria'   => $criteria,
            'sort'       => [
                'defaultOrder' => 'name ASC'
            ],
            'pagination' => [
                'pageSize' => 20
            ],
        ]);
    }


	/**
	 * @return array
	 */
	public function getStatusList()
	{
		return [
			self::STATUS_PUBLISHED => 'Активен',
			self::STATUS_UNPUBLISHED => 'Не активен',
		];
	}

	/**
	 * @return mixed|string
	 */
	public function getStatus()
	{
		$data = $this->getStatusList();

		return isset($data[$this->status]) ? $data[$this->status] : Yii::t('RbacModule.rbac', '*unknown*');
	}
}
