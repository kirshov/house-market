<?php

/**
 * @property int $id
 * @property integer $role_id
 * @property string $module
 * @property string $create
 * @property string $view
 * @property string $delete
 * @property string $update
 */
class RolesAssign extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param  string $className active record class name.
     * @return AuthItem the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{user_roles_assign}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['role_id', 'required'],
            ['create, update, view, delete', 'numerical', 'integerOnly' => true],
        ];
    }

	/**
	 * @return array
	 */
    public static function getModulesActions(){
    	return [
			'webforma' => ['update'],
			'log' => ['view'],
			'callback' => ['view', 'update', 'delete'],
			'feedback' => ['view', 'update', 'delete'],
			'robots' => ['update'],
			'target' => ['update'],
		];
	}

	/**
	 * @return array
	 */
    public static function getActions(){
    	return [
    		'view' => 'Просмотр',
			'create' => 'Создание',
			'update' => 'Редактирование',
			'delete' => 'Удаление'
		];
	}
}
