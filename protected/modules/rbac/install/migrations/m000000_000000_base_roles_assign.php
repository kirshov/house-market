<?php

class m000000_000000_base_roles_assign extends webforma\components\DbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{user_roles_assign}}',
            [
                'role_id'     => 'int(11) not null',
                'module'      => 'varchar(64) NOT NULL',
                'create'      => 'tinyint(1) not null default 0',
                'edit'        => 'tinyint(1) not null default 0',
                'update'      => 'tinyint(1) not null default 0',
                'delete'      => 'tinyint(1) not null default 0',
            ],
            $this->getOptions()
        );

        //pk
		$this->addPrimaryKey('pk_{{user_roles_assign}}', '{{user_roles_assign}}', ['role_id', 'module']);

		//fk
		$this->addForeignKey('fk_{{user_roles_assign}}_role_id', '{{user_roles_assign}}', 'role_id', '{{user_roles}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('{{user_roles_assign}}');
    }
}
