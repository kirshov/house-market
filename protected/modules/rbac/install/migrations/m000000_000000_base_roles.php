<?php

class m000000_000000_base_roles extends webforma\components\DbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{user_roles}}',
            [
                'id'          => 'pk',
                'name'        => 'varchar(255) NOT NULL',
                'description' => 'text',
                'status'      => 'tinyint(1) not null default 1',
            ],
            $this->getOptions()
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{user_roles}}');
    }
}
