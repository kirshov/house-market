<?php

class m190821_085548_add_user_role extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{user_user}}', 'role_id', 'int(11) default null');

		//fk
		$this->addForeignKey('fk_{{user_user}}_role_id', '{{user_user}}', 'role_id', '{{user_roles}}', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{
		$this->dropColumn('{{user_user}}', 'role_id');
	}
}