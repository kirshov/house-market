<?php

return [
    'module' => [
        'class' => 'application.modules.rbac.RbacModule'
    ],
	'import' => [
		'application.modules.rbac.listeners.*',
		'application.modules.rbac.models.*',
	],
    'component' => [
        'authManager' => [
            'class' => 'application.modules.rbac.components.WAuthManager',
        ],
        // override core ModuleManager
        'moduleManager' => [
            'class' => 'application.modules.rbac.components.ModuleManager'
        ],
        //attach event handlers
        'eventManager' => [
            'class' => 'webforma\components\EventManager',
            'events' => [
                // before backend controllers
                'webforma.backend.controller.init' => [
                    ['AccessControlListener', 'onBackendControllerInit']
                ]
            ]
        ]
    ],
];
