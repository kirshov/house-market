<?php

$this->breadcrumbs = [
    $this->getModule()->name,
];
?>

<?php $this->widget('bootstrap.widgets.TbButton', [
	'buttonType' => 'submit',
	'context' => 'primary',
	'label' => Yii::t('SitemapModule.sitemap', 'Regenerate sitemap'),
	'id' => 'regenerate-site-map',
]); ?>

<?php $this->widget('bootstrap.widgets.TbButton', [
	'buttonType' => 'button',
	'context' => 'success',
	'label' => '<i class="glyphicon glyphicon-plus">&nbsp;</i> Добавить страницу',
	'id' => 'add-site-map',
	'encodeLabel' => false,
	'htmlOptions' => [
		'data-target' => '#add-toggle',
		'data-toggle' => 'collapse',
	]
]); ?>

<div id="add-toggle" class="collapse out add-form">
	<p></p>
    <?php
    $form = $this->beginWidget(
        'bootstrap.widgets.TbActiveForm',
        [
            'id' => 'sitemap-page-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'action' => ['/sitemap/sitemapBackend/createPage'],
            'type' => 'vertical',
            'htmlOptions' => ['class' => 'well'],
        ]
    ); ?>
    <div class="alert alert-info">
        <?= Yii::t('SitemapModule.sitemap', 'Fields with'); ?>
        <span class="required">*</span>
        <?= Yii::t('SitemapModule.sitemap', 'are required.'); ?>
    </div>

    <?= $form->errorSummary($newPage); ?>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->textFieldGroup($newPage, 'url'); ?>
        </div>
        <div class="col-sm-2">
            <?= $form->textFieldGroup($newPage, 'priority'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <?= $form->dropDownListGroup($newPage, 'changefreq', [
				'widgetOptions' => [
					'data' => SitemapHelper::getChangeFreqList(),
				],
			]); ?>
        </div>
        <div class="col-sm-3">
            <?= $form->dropDownListGroup($newPage, 'status', [
				'widgetOptions' => [
					'data' => $newPage->getStatusList(),
				],
			]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2">
            <?php $this->widget( 'bootstrap.widgets.TbButton', [
				'buttonType' => 'submit',
				'context' => 'primary',
				'label' => Yii::t('SitemapModule.sitemap', 'Add'),
			]); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>

<?php $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'page-grid',
        'dataProvider' => $pages,
        'filter' => $pages,
        'sortField' => 'order',
        'actionsButtons' => false,
        'columns' => [
            [
                'class' => 'webforma\widgets\WEditableColumn',
                'name' => 'url',
                'editable' => [
                    'url' => $this->createUrl('/sitemap/sitemapBackend/inlinePage'),
                    'mode' => 'popup',
                    'params' => [
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($page, 'url', ['class' => 'form-control']),
            ],
            [
                'class' => 'webforma\widgets\WEditableColumn',
                'name' => 'changefreq',
                'editable' => [
                    'url' => $this->createUrl('/sitemap/sitemapBackend/inlinePage'),
                    'mode' => 'popup',
                    'type' => 'select',
                    'source' => SitemapHelper::getChangeFreqList(),
                    'params' => [
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeDropDownList($page, 'changefreq', SitemapHelper::getChangeFreqList(),
                    ['class' => 'form-control', 'empty' => '']),
                'htmlOptions' => ['style' => 'width: 250px;'],
            ],
            [
                'class' => 'webforma\widgets\WEditableColumn',
                'name' => 'priority',
                'editable' => [
                    'url' => $this->createUrl('/sitemap/sitemapBackend/inlinePage'),
                    'mode' => 'popup',
                    'params' => [
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($page, 'priority', ['class' => 'form-control']),
                'htmlOptions' => ['style' => 'width: 250px;'],
            ],
            [
                'class' => 'webforma\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('/sitemap/sitemapBackend/inlinePage'),
                'source' => $page->getStatusList(),
                'options' => [
                    SitemapPage::STATUS_ACTIVE => ['class' => 'label-success'],
                    SitemapPage::STATUS_NOT_ACTIVE => ['class' => 'label-default'],
                ],
                'htmlOptions' => ['style' => 'width: 150px;'],
                'filter' => CHtml::activeDropDownList($page, 'status', $page->getStatusList(),
                    ['class' => 'form-control', 'empty' => '']),
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
                'template' => '{front_view}{delete}',
                'frontViewButtonUrl' => function ($data) {
                    return Yii::app()->createAbsoluteUrl($data->url);
                },
                'buttons' => [
                    'front_view' => [
                        'visible' => function ($row, $data) {
                            return $data->status == SitemapPage::STATUS_ACTIVE;
                        },
                    ],
                ],
            ],
        ],
    ]
);
?>

<script type="text/javascript">
    $('#regenerate-site-map').on('click', function (event) {
        event.preventDefault();
        $.post('<?= Yii::app()->createUrl('/sitemap/sitemapBackend/regenerate');?>', {
            'do': true,
            '<?= Yii::app()->getRequest()->csrfTokenName?>': '<?= Yii::app()->getRequest()->csrfToken;?>'
        }, function (response) {
            window.location.reload();
        }, 'json')
    });
</script>
