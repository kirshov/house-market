<?php

use webforma\components\WebModule;

/**
 * Class SitemapModule
 */
class SitemapModule extends WebModule
{
    /**
     * @var
     */
    public $data;

    /**
     * @var string
     */
    public $filePath = 'sitemap.xml';

	/**
	 * @var int
	 */
	public $adminMenuOrder = 30;

    /**
     * @return string
     */
    public function getSiteMapPath()
    {
    	$path = Yii::getPathOfAlias('webroot').'/'.Yii::app()->getModule('webforma')->uploadPath.'/sitemaps/';

	    if(!is_dir($path)){
		    CFileHelper::createDirectory($path, null, true);
	    }
        return $path;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('SitemapModule.sitemap', 'Sitemap');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('SitemapModule.sitemap', 'Module for management sitemap.xml');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-sitemap';
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('SitemapModule.sitemap', 'Services');
    }


    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'sitemap.components.*',
                'sitemap.events.*',
                'sitemap.models.*',
            ]
        );
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/sitemap/sitemapBackend/settings';
    }

	/**
	 * @return array
	 */
	public function getNavigation()
	{
		return [];
	}


	/**
	 * @return bool
	 */
	public function getIsInstallDefault()
	{
		return true;
	}
}
