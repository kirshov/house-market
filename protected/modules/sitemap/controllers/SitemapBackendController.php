<?php

use webforma\components\controllers\BackController;
use webforma\widgets\WFlashMessages;

/**
 * Class SitemapBackendController
 */
class SitemapBackendController extends BackController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'roles' => ['SitemapModule.SitemapBackend.manage']],
            ['deny'],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'inlineModel' => [
                'class' => 'webforma\components\actions\WInLineEditAction',
                'model' => 'SitemapModel',
                'validAttributes' => ['changefreq', 'priority', 'status'],
            ],
            'inlinePage' => [
                'class' => 'webforma\components\actions\WInLineEditAction',
                'model' => 'SitemapPage',
                'validAttributes' => ['url', 'changefreq', 'priority', 'status'],
            ],
        ];
    }

    /**
     *
     */
    public function actionSettings()
    {
        $pages = new SitemapPage('search');
        $pages->unsetAttributes();
        $pages->setAttributes(
            Yii::app()->getRequest()->getParam('SitemapPage', [])
        );

	    $model = new SitemapPage();
	    $model->priority = '0.5';
	    $model->changefreq = SitemapHelper::FREQUENCY_DAILY;
	    $model->status = SitemapPage::STATUS_ACTIVE;

	    $this->render('settings', ['pages' => $pages->search(), 'page' => $pages, 'newPage' => $model]);
    }

    /**
     *
     */
    public function actionCreatePage()
    {
        $model = new SitemapPage('search');

        if ($data = Yii::app()->getRequest()->getPost('SitemapPage')) {
            $model->setAttributes($data);
            if ($model->save()) {
                Yii::app()->getUser()->setFlash(WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('SitemapModule.sitemap', 'Page added!'));
                $this->redirect(['settings']);
            }
        }

        $this->render('settings', ['pages' => $model->search(), 'page' => $model]);
    }

    /**
     * @throws CHttpException
     */
    public function actionRegenerate()
    {
        if (!Yii::app()->getRequest()->getIsPostRequest() || !Yii::app()->getRequest()->getPost('do')) {
            throw new CHttpException(404);
        }

        if (SitemapHelper::remove()) {
            Yii::app()->getUser()->setFlash(WFlashMessages::SUCCESS_MESSAGE,
                Yii::t('SitemapModule.sitemap', 'message.success'));
            Yii::app()->ajax->success();
        }

        Yii::app()->getUser()->setFlash(WFlashMessages::ERROR_MESSAGE,
            Yii::t('SitemapModule.sitemap', 'message.error'));
        Yii::app()->ajax->failure();
    }

    /**
     * @param $id
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            $page = SitemapPage::model()->findByPk($id);

            if (null === $page) {
                throw new CHttpException(404);
            }

            $page->delete();

            if (!isset($_GET['ajax'])) {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : ['settings']);
            }
        }
    }
}
