<?php

use webforma\components\controllers\FrontController;


/**
 * Class SitemapController
 */
class SitemapController extends FrontController
{
    /**
     * @var
     */
    protected $generator;

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->generator = Yii::app()->getComponent('sitemapGenerator');

    }

    /**
     *
     */
    public function actionIndex()
    {
	    $path = Yii::app()->getModule('sitemap')->getSiteMapPath();
	    $sitemapFile = $path.$_SERVER['HTTP_HOST'].'_sitemap.xml';

	    if (!file_exists($sitemapFile)) {
		    $staticPages = SitemapPage::model()->getData();
		    $this->generator->generate($sitemapFile, $staticPages);
	    }

	    header('Content-type: text/xml');
	    echo file_get_contents($sitemapFile);
	    Yii::app()->end();
    }
}
