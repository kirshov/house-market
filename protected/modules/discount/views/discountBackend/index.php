<?php
/**
 * @var Discount $model
 */

$this->breadcrumbs = [
    Yii::t('DiscountModule.discount', 'Discounts') => ['/discount/discountBackend/index'],
    Yii::t('DiscountModule.discount', 'Manage'),
];

$this->pageTitle = Yii::t('DiscountModule.discount', 'Discounts - manage');

$this->menu = [
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('DiscountModule.discount', 'Manage discounts'),
        'url' => ['/discount/discountBackend/index'],
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('DiscountModule.discount', 'Create discount'),
        'url' => ['/discount/discountBackend/create'],
    ],
];
?>

<?php
$this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id' => 'discount-grid',
        'type' => 'condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => [
			[
				'name' => 'type',
				'type' => 'raw',
				'value' => function (Discount $data) {
					return DiscountType::title($data->type);
				},
				'visible' => false,
			],
            'total_cost_orders',
            'value',
            [
                'class' => 'webforma\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('/discount/discountBackend/inline'),
                'source' => $model->getStatusList(),
				'options' => [
					Discount::STATUS_ACTIVE => ['class' => 'label-success'],
					Discount::STATUS_NOT_ACTIVE => ['class' => 'label-default'],
				],
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
            ],
        ],
    ]
); ?>
