<?php
/**
 * @var Discount $model
 */

$this->breadcrumbs = [
    Yii::t('DiscountModule.discount', 'Discounts') => ['/discount/discountBackend/index'],
    $model->value.'% от суммы '.$model->total_cost_orders => ['/discount/discountBackend/view', 'id' => $model->id],
    Yii::t('DiscountModule.discount', 'Edition'),
];

$this->pageTitle = Yii::t('DiscountModule.discount', 'Discounts - edition');

$this->menu = [
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('DiscountModule.discount', 'Manage discounts'),
        'url' => ['/discount/discountBackend/index'],
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('DiscountModule.discount', 'Create discount'),
        'url' => ['/discount/discountBackend/create'],
    ],
    ['label' => Yii::t('DiscountModule.discount', 'Discount') . ' «' . mb_substr($model->value.'% от суммы '.$model->total_cost_orders, 0, 32) . '»'],
    [
        'icon' => 'fa fa-fw fa-pencil',
        'label' => Yii::t('DiscountModule.discount', 'Update discount'),
        'url' => [
            '/discount/discountBackend/update',
            'id' => $model->id,
        ],
    ],
    [
        'icon' => 'fa fa-fw fa-trash-o',
        'label' => Yii::t('DiscountModule.discount', 'Delete discount'),
        'url' => '#',
        'linkOptions' => [
            'submit' => ['/discount/discountBackend/delete', 'id' => $model->id],
            'confirm' => Yii::t('DiscountModule.discount', 'Do you really want to remove this discount?'),
            'params' => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
            'csrf' => true,
        ],
    ],
];
?>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>
