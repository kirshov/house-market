<?php
/**
 * @var Discount $model
 */

$this->breadcrumbs = [
    Yii::t('DiscountModule.discount', 'Discounts') => ['/discount/discountBackend/index'],
    Yii::t('DiscountModule.discount', 'Creating'),
];

$this->pageTitle = Yii::t('DiscountModule.discount', 'Discounts - creating');

$this->menu = [
    [
        'icon' => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('DiscountModule.discount', 'Manage discounts'),
        'url' => ['/discount/discountBackend/index'],
    ],
    [
        'icon' => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('DiscountModule.discount', 'Create discount'),
        'url' => ['/discount/discountBackend/create'],
    ],
];
?>

<?$this->title = Yii::t('DiscountModule.discount', 'Discounts').'<br><small>'.Yii::t('DiscountModule.discount', 'creating').'</small>';?>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>
