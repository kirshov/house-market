<?php
/**
 * @var Discount $model
 * @var TbActiveForm $form
 */
?>

<?php
/* @var $model Discount */
$form = $this->beginWidget(
	'bootstrap.widgets.TbActiveForm',
	[
		'id' => 'discount-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => 'vertical',
		'htmlOptions' => ['class' => 'well sticky'],
		'clientOptions' => [
			'validateOnSubmit' => true,
		],
	]
);
?>
<div class="alert alert-info">
	<?=  Yii::t('DiscountModule.discount', 'Fields with'); ?>
	<span class="required">*</span>
	<?=  Yii::t('DiscountModule.discount', 'are required'); ?>
</div>

<?=  $form->errorSummary($model); ?>
<div class="row">
	<?/*
	<div class="col-sm-3">
		<?=  $form->dropDownListGroup($model, 'type', [
			'widgetOptions' => [
				'data' => DiscountType::all(),
			],
		]); ?>
	</div>*/?>
	<div class="col-sm-3">
		<?=  $form->dropDownListGroup($model, 'status', [
			'widgetOptions' => [
				'data' => $model->getStatusList(),
			],
		]); ?>
	</div>
</div>

<div class="row">
	<div class="col-sm-3">
		<?=  $form->textFieldGroup($model, 'total_cost_orders'); ?>
	</div>
</div>

<div class="row">
	<div class="col-sm-3">
		<?=  $form->textFieldGroup($model, 'value'); ?>
	</div>
</div>

<div class="buttons">
	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context' => 'primary',
		'label' => $model->getIsNewRecord() ? Yii::t('DiscountModule.discount', 'Add discount and continue') : Yii::t('DiscountModule.discount', 'Save discount and continue'),
	]); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label' => $model->getIsNewRecord() ? Yii::t('DiscountModule.discount', 'Add discount and close') : Yii::t('DiscountModule.discount', 'Save discount and close'),
	]); ?>
</div>

<?php $this->endWidget(); ?>