<?php

use webforma\components\WebModule;

/**
 * Class DiscountModule
 */
class DiscountModule extends WebModule
{
    /**
     *
     */
    public $adminMenuOrder = 8;

    public function getDependencies()
    {
        return ['order'];
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('DiscountModule.discount', 'Discounts list'),
                'url' => ['/discount/discountBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('DiscountModule.discount', 'Create discount'),
                'url' => ['/discount/discountBackend/create'],
            ],

        ];
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/discount/discountBackend/index';
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('DiscountModule.discount', 'Store');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('DiscountModule.discount', 'Discounts');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('DiscountModule.discount', 'Store discount module');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-percent';
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'discount.models.*',
            ]
        );
    }
}
