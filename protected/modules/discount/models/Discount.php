<?php
Yii::import('application.modules.discount.DiscountModule');
/**
 * @property integer $id
 * @property double $value
 * @property double $total_cost_orders
 * @property integer $type
 * @property integer $status
 *
 * @method $this active
 *
 */
class Discount extends webforma\models\WModel
{
	/**
	 *
	 */
	const STATUS_NOT_ACTIVE = 0;

	/**
	 *
	 */
	const STATUS_ACTIVE = 1;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{store_discount}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['total_cost_orders, value', 'required'],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            //['type', 'in', 'range' => DiscountType::keys()],
            ['total_cost_orders', 'numerical', 'integerOnly' => true, 'min' => 1],
            ['value', 'numerical',],
            [
                'id, type, total_cost_orders, numerical, status',
                'safe',
                'on' => 'search'
            ],
        ];
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'active' => [
                'condition' => 'status = :status',
                'params' => [':status' => self::STATUS_ACTIVE],
            ],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('DiscountModule.discount', 'ID'),
            'value' => Yii::t('DiscountModule.discount', 'Discount value'),
            'total_cost_orders' => Yii::t('DiscountModule.discount', 'Order price'),
            'type' => Yii::t('DiscountModule.discount', 'Type'),
            'status' => Yii::t('DiscountModule.discount', 'Status'),
        ];
    }

    /**
     * @return CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
		$criteria->compare('value', $this->value);
		$criteria->compare('total_cost_orders', $this->total_cost_orders);
        $criteria->compare('type', $this->type);
        $criteria->compare('status', $this->status);


        return new CActiveDataProvider(
            $this, [
                'criteria' => $criteria,
            ]
        );
    }


    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Discount - the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @param int $price
     * @return array
     */
    public function getDiscountErrors($price = 0)
    {
        $errors = [];

        if ($this->status == DiscountStatus::STATUS_NOT_ACTIVE) {
            $errors[] = Yii::t('DiscountModule.discount', 'Not active');
        }
        if ($price < $this->min_order_price) {
	        $minPrice = $this->min_order_price;
	        if($minPrice == (int) $minPrice){
		        $minPrice = (int) $minPrice;
	        }
	        $minPrice = ProductHelper::getFormatPrice($minPrice);

            $errors[] = Yii::t('DiscountModule.discount', 'Min order price') . ' ' . Yii::t(
                    'DiscountModule.discount',
                    '{n} RUB|{n} RUB|{n} RUB',
                    [$minPrice]
                );
        }
        if (!is_null($this->quantity) && $this->quantity <= 0) {
            $errors[] = Yii::t('DiscountModule.discount', 'Discounts are ended');
        }
        if (!is_null($this->quantity_per_user) && !Yii::app()->getUser()->getIsGuest() && ($this->getNumberUsagesByUser(Yii::app()->getUser()->getId()) >= $this->quantity_per_user)
        ) {
            $errors[] = Yii::t('DiscountModule.discount', 'You\'ve used up all your discounts');
        }
        if ($this->registered_user && Yii::app()->getUser()->getIsGuest()) {
            $errors[] = Yii::t('DiscountModule.discount', 'Discount is available only for registered users');
        }
        if ($this->start_time && (time() < strtotime($this->start_time))) {
            $errors[] = Yii::t('DiscountModule.discount', 'Discount start date not yet come');
        }
        if ($this->end_time && (time() > strtotime($this->end_time))) {
            $errors[] = Yii::t('DiscountModule.discount', 'Discount expired');
        }

        return $errors;
    }

    /**
     * @param int $price
     * @return bool
     */
    public function getIsAvailable($price = 0)
    {
        return !$this->getDiscountErrors($price);
    }

    /**
     * @param $price
     * @return float
     */
    public function getDiscount($price)
    {
        $discount = 0.00;

        if (!$this->getIsAvailable($price)) {
            return $discount;
        }

        switch ($this->type) {
            case DiscountType::TYPE_SUM:
                $discount += $this->value;
                break;
            case DiscountType::TYPE_PERCENT:
                $discount += ($this->value / 100) * $price;
                break;
        }

        return $discount;
    }

	/**
	 * @return array
	 */
    public function getStatusList(){
    	return [
    		self::STATUS_NOT_ACTIVE => 'Не активен',
			self::STATUS_ACTIVE => 'Активен',
		];
	}

	/**
	 * @return mixed|string
	 */
	public function getStatus()
	{
		$data = $this->getStatusList();

		return isset($data[$this->status]) ? $data[$this->status] : Yii::t('WebformaModule.webforma', '*unknown*');
	}
}
