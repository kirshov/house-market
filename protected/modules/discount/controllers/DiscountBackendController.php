<?php

class DiscountBackendController extends webforma\components\controllers\BackController
{
    public function actions()
    {
        return [
            'inline' => [
                'class' => 'webforma\components\actions\WInLineEditAction',
                'model' => 'Discount',
                'validAttributes' => [
                    'status',
                ],
            ],
        ];
    }

    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin'],],
            ['allow', 'actions' => ['index'], 'roles' => ['Discount.DiscountBackend.Index'],],
            ['allow', 'actions' => ['view'], 'roles' => ['Discount.DiscountBackend.View'],],
            ['allow', 'actions' => ['create'], 'roles' => ['Discount.DiscountBackend.Create'],],
            ['allow', 'actions' => ['update', 'inline'], 'roles' => ['Discount.DiscountBackend.Update'],],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Discount.DiscountBackend.Delete'],],
            ['deny',],
        ];
    }

    public function actionCreate()
    {
        $model = new Discount();
        $attributes = Yii::app()->getRequest()->getPost('Discount');

        if ($attributes) {
            $model->attributes = $attributes;

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('DiscountModule.discount', 'Record created!')
                );

                $submitType = Yii::app()->getRequest()->getPost('submit-type');

                if ($submitType) {
                    $this->redirect([$submitType]);
                }

                $this->redirect(['create']);
            }
        } else {
        	$model->status = Discount::STATUS_ACTIVE;
		}

        $this->render('create', ['model' => $model]);
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $attributes = Yii::app()->getRequest()->getPost('Discount');

        if ($attributes) {
            $model->attributes = $attributes;

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('DiscountModule.discount', 'Record updated!')
                );

                $submitType = Yii::app()->getRequest()->getPost('submit-type');

                if ($submitType) {
                    $this->redirect([$submitType]);
                }

                $this->redirect(['update', 'id' => $model->id]);
            }
        }

        $this->render('update', ['model' => $model]);
    }

    public function actionDelete($id)
    {
        if (!Yii::app()->getRequest()->getIsPostRequest()) {
            throw new CHttpException(400, Yii::t('DiscountModule.discount', 'Unknown request. Don\'t repeat it please!'));
        }

        $this->loadModel($id)->delete();

        Yii::app()->user->setFlash(
            webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
            Yii::t('DiscountModule.discount', 'Record removed!')
        );

        if (!Yii::app()->getRequest()->getQuery('ajax')) {
            $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
        }
    }


    public function actionIndex()
    {
        $model = new Discount('search');
        $attributes = Yii::app()->getRequest()->getQuery('Discount');

        $model->unsetAttributes();

        if ($attributes) {
            $model->attributes = $attributes;
        }

        $this->render('index', ['model' => $model]);
    }


    public function loadModel($id)
    {
        $model = Discount::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('DiscountModule.discount', 'Page not found!'));
        }

        return $model;
    }
}
