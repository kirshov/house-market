<?php

/**
 * Class DiscountManager
 */
class DiscountManager extends CApplicationComponent
{
	/**
	 *
	 */
	const DISCOUNT_TYPE_PERSONAL = 1;

	/**
	 *
	 */
	const DISCOUNT_TYPE_ORDERS = 2;

	/**
	 * @var
	 */
	protected $_discountType;

	/**
	 * @var
	 */
	protected $_totalOrdersAmount;

    /**
     * @var string
     */
    protected $discountStateKey = 'webforma::store::discount';

	/**
	 * @var string
	 */
    protected $discountTotalAmountStateKey = 'webforma::store::discount::amount';

	/**
	 * @param $userId
	 * @return float
	 */
	public function getDiscountFromOrders($userId = null){
		if($userId === null){
			$userId = Yii::app()->getUser()->getId();
		}

		$dataContainer = Yii::app()->getComponent('dataContainer');
		$cacheKey = $this->discountStateKey.'::'.$userId;

		if($dataContainer->has($cacheKey)){
			$discount = $dataContainer->get($cacheKey);
		} else {
			$discount = Yii::app()->getCache()->get($cacheKey);
			$dataContainer->set($cacheKey, $discount);
		}


		if($discount !== false){
			return $discount;
		}
		$discount = 0;

		$totalDiscountAmount = $this->getTotalAmount($userId);
		if($totalDiscountAmount){
			$modelDiscount = Discount::model()->active()->find([
				'condition' => 't.total_cost_orders <= :orders_sum',
				'params' => [
					':orders_sum' => (float) $totalDiscountAmount,
				],
				'order' => 't.total_cost_orders DESC',
			]);

			$discount = (float) $modelDiscount->value;
		}

		$dataContainer->set($cacheKey, $discount);
		Yii::app()->getCache()->set($cacheKey, $discount, Yii::app()->getModule('webforma')->liteCacheTime);
		return $discount;
	}

	/**
	 * @param $userId
	 * @return int
	 */
	public function getTotalAmount($userId = null){
		if($userId === null){
			$userId = Yii::app()->getUser()->getId();
		}
		$totalAmount = Yii::app()->getCache()->get($this->discountTotalAmountStateKey.'::'.$userId);
		if($totalAmount !== false){
			return $totalAmount;
		}
		$totalAmount = 0;

		$modelTotal = Order::model()->find([
			'select' => new CDbExpression('SUM(t.total_price) totalAll'),
			'condition' => 't.status_id = :status AND t.user_id = :user_id',
			'params' => [
				':status' => OrderStatus::STATUS_FINISHED,
				':user_id' => (int) $userId,
			],
		]);

		if($modelTotal){
			$totalAmount = $modelTotal->totalAll;
		}

		Yii::app()->getCache()->set($this->discountTotalAmountStateKey.'::'.$userId, $totalAmount, Yii::app()->getModule('webforma')->liteCacheTime);

		return $totalAmount;
	}

	/**
	 * @return int
	 */
	public function getDiscount(){
		$personalDiscount = $this->getPersonalDiscount();
		$discountFromOrders = $this->getDiscountFromOrders();

		$this->_discountType = false;

		if($personalDiscount > $discountFromOrders){
			$this->_discountType = self::DISCOUNT_TYPE_PERSONAL;
			return $personalDiscount;
		} else {
			$this->_discountType = self::DISCOUNT_TYPE_ORDERS;
			return $discountFromOrders;
		}
	}

	/**
	 * @return int
	 */
	public function getPersonalDiscount(){
		$profile = Yii::app()->getUser()->getProfile();
		return (int) $profile->discount;
	}

	/**
	 * @return null|int
	 */
	public function getDiscountType(){
		if($this->_discountType === null){
			$this->getDiscount();
		}

		return $this->_discountType;
	}

	/**
	 * @return array
	 */
	public function getDiscountTypeList(){
		return [
			self::DISCOUNT_TYPE_PERSONAL => 'Персональная скидка',
			self::DISCOUNT_TYPE_ORDERS => 'Накопительная скидка',
		];
	}

	/**
	 * @return bool|mixed
	 */
	public function getDiscountTypeName(){
		$list = $this->getDiscountTypeList();
		$discountType = $this->getDiscountType();
		if(!$discountType){
			return false;
		}
		return $list[$discountType];
	}

	/**
	 * @return Discount[]
	 */
	public function getDiscountList(){
		return Discount::model()->active()->findAll(['order' => 'value']);
	}
}
