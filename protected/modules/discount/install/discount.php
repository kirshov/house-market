<?php

return [
    'module' => [
        'class' => 'application.modules.discount.DiscountModule',
    ],
    'import' => [
        'application.modules.discount.models.*',
        'application.modules.discount.helpers.*',
    ],
    'component' => [
        'discountManager' => [
            'class' => 'application.modules.discount.components.DiscountManager',
        ],
    ],
	'params' => [
		'widgets' => [
			'discountInfo' => 'application.modules.discount.widgets.DiscountInfoWidget',
			'discounts' => 'application.modules.discount.widgets.DiscountListWidget',
		]
	],
    'rules' => [
        '/discount/<action:\w+>' => 'discount/discount/<action>',
        '/discount/<action:\w+>/<id:\w+>' => 'discount/discount/<action>',
    ],
];
