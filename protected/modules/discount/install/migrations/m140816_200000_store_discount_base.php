<?php

class m140816_200000_store_discount_base extends webforma\components\DbMigration
{
    public function safeUp()
    {
        $this->createTable(
            "{{store_discount}}",
            [
                "id" => "pk",
                "type" => "tinyint not null default '0'",
				"total_cost_orders" => "decimal(10, 0) not null default '0'",
                "value" => "decimal(10, 1) not null default '0'",
                "status" => "tinyint not null default '0'",
            ],
            $this->getOptions()
        );
    }

    public function safeDown()
    {
        $this->dropTable("{{store_discount}}");
    }
}
