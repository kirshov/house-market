<?php

class DiscountListWidget extends \webforma\widgets\WWidget
{
	/**
	 * @var string
	 */
	public $view = 'index';

	/**
	 *
	 */
	public function run()
	{
		/**
		 * DiscountManager $discountManager
		 */
		$discountManager = Yii::app()->getComponent('discountManager');
		$this->render($this->view, [
			'discounts' => $discountManager->getDiscountList(),
			'currency' => Yii::t('StoreModule.store', Yii::app()->getModule('store')->currency),
		]);
	}
}