<?php

class DiscountInfoWidget extends \webforma\widgets\WWidget
{
	/**
	 * @var string
	 */
	public $view = 'index';

	/**
	 *
	 */
	public function run()
	{
		/**
		 * DiscountManager $discountManager
		 */
		$discountManager = Yii::app()->getComponent('discountManager');
		$discount = $discountManager->getDiscount();
		$this->render($this->view, [
			'title' => $discountManager->getDiscountTypeName(),
			'type' => $discountManager->getDiscountType(),
			'value' => $discount,
			'totalOrdersAmount' => Helper::getFormatPrice($discountManager->getTotalAmount()),
			'currency' => Yii::t('StoreModule.store', Yii::app()->getModule('store')->currency),
		]);
	}
}