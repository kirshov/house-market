<?php

use webforma\widgets\WWidget;

class ImagesWidget extends WWidget
{
	/**
	 * @var
	 */
	public $galleryId;

	/**
	 * @var
	 */
	public $categoryId;

	/**
	 * @var int
	 */
	public $limit = 5;

	/**
	 * @var string
	 */
	public $order = 't.sort';

	/**
	 * @var
	 */
	public $view = 'default';

    public function run()
    {
    	$criteria = new CDbCriteria();
		$criteria->compare('status', Image::STATUS_CHECKED);

		if ($this->galleryId && Yii::app()->hasModule('gallery')) {
			$criteria->with = ['gallery', 'image'];
			$criteria->compare('gallery_id', $this->galleryId);
			$criteria->together = true;
		}

		if($this->categoryId){
			$criteria->compare('category_id', $this->categoryId);
		}

		if($this->order){
			$criteria->order = $this->order;
		}

		$this->render($this->view, [
			'images' =>  Image::model()->findAll($criteria),
		]);
    }
}
