<?php
/**
 * Файл конфигурации модуля
 **/
return [
    'module'    => [
        'class' => 'application.modules.image.ImageModule',
    ],
    'import'    => [
        'application.modules.image.models.*',
    ],
    'component' => [],
	'params' => [
		'widgets' => [
			'images' => 'application.modules.image.widgets.ImagesWidget',
		],
	],
    'rules'     => [],
];
