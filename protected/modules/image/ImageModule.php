<?php

/**
 * ImageModule основной класс модуля image
 */

use webforma\components\WebModule;

/**
 * Class ImageModule
 */
class ImageModule extends WebModule
{
	/**
	 *
	 */
	public $adminMenuOrder = 100;

    /**
     * @var string
     */
    public $uploadPath = 'images';

	/**
	 * @var
	 */
	public $mainCategory;

    /**
     * @var
     */
    public $useCategory = 0;

    /**
     * @return bool
     */
    public function getInstall()
    {
        if (parent::getInstall()) {
            @mkdir(Yii::app()->uploadManager->getBasePath().DIRECTORY_SEPARATOR.$this->uploadPath, 0755);
        }

        return false;
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-picture-o';
    }

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [
            'mainCategory' => Yii::t('ImageModule.image', 'Main images category'),
            'useCategory' => 'Использовать категории',
            'uploadPath' => Yii::t('ImageModule.image', 'Directory for uploading images'),
            'width' => Yii::t('ImageModule.image', 'Image width'),
            'height' => Yii::t('ImageModule.image', 'Image height'),
        ];
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
    	$params = [];

    	if(DEVMODE){
    		$params = CMap::mergeArray($params, [
				'useCategory' => [
					'input-type' => 'checkbox'
				],
				'mainCategory' => CHtml::listData($this->getCategoryList(), 'id', 'name'),
			]);
		}
        return $params;
    }

    /**
     * @return array
     */
    public function getEditableParamsGroups()
    {
        $groups = [];
		if(DEVMODE){
			$groups = CMap::mergeArray($groups, [
				'system' => [
					'label' => 'Системные настройки',
					'items' => [
						'useCategory',
						'mainCategory',
					],
				],
			]);
		}

		return $groups;
    }

    /**
     * @return array|bool
     */
    public function checkSelf()
    {
    	return true;
        $messages = [];

        $uploadPath = Yii::app()->uploadManager->getBasePath().DIRECTORY_SEPARATOR.$this->uploadPath;

        if (!is_dir($uploadPath) || !is_writable($uploadPath)) {
            $messages[WebModule::CHECK_ERROR][] = [
                'type' => WebModule::CHECK_ERROR,
                'message' => Yii::t(
                    'ImageModule.image',
                    'Directory "{dir}" is not accessible for writing ot not exists! {link}',
                    [
                        '{dir}' => $uploadPath,
                        '{link}' => CHtml::link(
                            Yii::t('ImageModule.image', 'Change module settings'),
                            [
                                '/webforma/backend/modulesettings/',
                                'module' => $this->id,
                            ]
                        ),
                    ]
                ),
            ];
        }

        return (isset($messages[WebModule::CHECK_ERROR])) ? $messages : true;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('ImageModule.image', 'Content');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('ImageModule.image', 'Images');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('ImageModule.image', 'Module for images management');
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $forImport = [];

        if (Yii::app()->hasModule('gallery')) {
            $forImport[] = 'gallery.models.*';
        }

        $this->setImport(
            array_merge(
                [
                    'image.models.*',
                ],
                $forImport
            )
        );
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ImageModule.image', 'Images list'),
                'url' => ['/image/imageBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ImageModule.image', 'Add image'),
                'url' => ['/image/imageBackend/create'],
            ],
        ];
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/image/imageBackend/index';
    }

    /**
     * Получаем разрешённые форматы:
     *
     * @return array of allowed extensions
     **/
    public function allowedExtensions()
    {
        return explode(',', $this->allowedExtensions);
    }
}
