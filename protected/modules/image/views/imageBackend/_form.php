<?php
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'id'                     => 'image-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => 'vertical',
		'htmlOptions'            => ['class' => 'well sticky', 'enctype' => 'multipart/form-data'],
		'clientOptions' => [
			'validateOnSubmit' => true,
		],
    ]
); ?>
<div class="alert alert-info">
    <?=  Yii::t('ImageModule.image', 'Fields with'); ?>
    <span class="required">*</span>
    <?=  Yii::t('ImageModule.image', 'are required.'); ?>
</div>

<?=  $form->errorSummary($model); ?>

<div class='row'>
	<div class='col-sm-6'>
		<div class='row'>
			<div class="col-sm-12">
				<?=  $form->textFieldGroup($model, 'name'); ?>
			</div>
		</div>

		<div class='row'>
			<?php if (Yii::app()->hasModule('gallery')) : { ?>
				<div class='col-sm-6'>
					<?=  $form->dropDownListGroup($model, 'galleryId', [
						'widgetOptions' => [
							'data'        => $model->galleryList(),
							'htmlOptions' => [
								'empty' => Yii::t('ImageModule.image', '--choose--'),
								'options' => [
									$model->gallery ? $model->gallery->id : 0 => ['selected' => true]
								]
							],
						],
					]); ?>
				</div>
			<?php } endif; ?>

			<?if(Yii::app()->getModule('image')->useCategory):?>
				 <span class="col-sm-6">
					<?=  $form->dropDownListGroup($model, 'category_id', [
						'widgetOptions' => [
							'data'        => Yii::app()->getComponent('categoriesRepository')->getFormattedList(Yii::app()->getModule('image')->mainCategory),
							'htmlOptions' => ['empty' => Yii::t('ImageModule.image', '--choose--'), 'encode' => false],
						],
					]); ?>
				</span>
			<?endif;?>
		</div>
	</div>

	<div class="col-sm-6">
		<div class='row'>
			<div class="col-sx-8 col-sm-8 col-sm-offset-2">
				<?php $this->widget('webforma\widgets\WInputFile', [
					'model' => $model,
					'attribute' => 'file',
					'altAttribute' => 'alt',
				]); ?>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12 form-group">
		<?= $form->labelEx($model, 'description'); ?>
		<?php $this->widget($this->module->getVisualEditor(), [
			'model' => $model,
			'attribute' => 'description',
		]); ?>
	</div>
</div>

<div class="row">
	<div class='col-sm-3'>
		<?=  $form->dropDownListGroup($model, 'status', [
			'widgetOptions' => [
				'data' => $model->getStatusList(),
			],
		]); ?>
	</div>
</div>

<div class="buttons">
	<?php
	$this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context'    => 'primary',
		'label'      => $model->isNewRecord ? Yii::t('ImageModule.image', 'Add image and close') : Yii::t(
				'ImageModule.image',
				'Save image and continue'
			),
	]); ?>

	<?php
	$this->widget('bootstrap.widgets.TbButton', [
		'buttonType'  => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label'       => $model->isNewRecord ? Yii::t('ImageModule.image', 'Add image and save') : Yii::t(
				'ImageModule.image',
				'Save mage and close'
			),
	]); ?>
</div>
<?php $this->endWidget(); ?>
