<?php
/**
 * CategoryModule основной класс модуля category
 */

use webforma\components\WebModule;

/**
 * Class CategoryModule
 */
class CategoryModule extends WebModule
{
    /**
     * @var string
     */
    public $uploadPath = 'category';

	/**
	 * @var int порядок следования модуля в меню панели управления (сортировка)
	 */
	public $adminMenuOrder = 20;

	/**
	 * @return array
	 */
	public function getDependencies()
	{
		return [];
	}

    /**
     * @return array|bool
     */
    public function checkSelf()
    {
    	return true;
        $messages = [];

        $uploadPath = Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath;

        if (!is_writable($uploadPath)) {
            $messages[WebModule::CHECK_ERROR][] = [
                'type' => WebModule::CHECK_ERROR,
                'message' => Yii::t(
                    'CategoryModule.category',
                    'Directory "{dir}" is available for write! {link}',
                    [
                        '{dir}' => $uploadPath,
                        '{link}' => CHtml::link(
                            Yii::t('CategoryModule.category', 'Change settings'),
                            [
                                '/webforma/backend/modulesettings/',
                                'module' => 'category',
                            ]
                        ),
                    ]
                ),
            ];
        }

        return isset($messages[WebModule::CHECK_ERROR]) ? $messages : true;
    }

    /**
     * @return bool
     */
    public function getInstall()
    {
        if (parent::getInstall()) {
            @mkdir(Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath, 0755);
        }

        return false;
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [
            'uploadPath' => Yii::t('CategoryModule.category', 'File uploading catalog (uploads)'),
        ];
    }

    /**
     * @return string
     */
    public function getCategory()
    {
    	if(!Yii::app()->getModule('webforma')->useCategory){
    		return false;
		}
        return Yii::t('CategoryModule.category', 'Structure');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('CategoryModule.category', 'Categories');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('CategoryModule.category', 'Module for categories/sections management');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-folder-open';
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'category.models.*',
                'category.components.*',
            ]
        );
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('CategoryModule.category', 'Categories list'),
                'url' => ['/category/categoryBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('CategoryModule.category', 'Create category'),
                'url' => ['/category/categoryBackend/create'],
            ],
        ];
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/category/categoryBackend/index';
    }

	/**
	 * Возвращаем статус, устанавливать ли галку для установки модуля в инсталяторе по умолчанию:
	 *
	 * @return bool
	 **/
	public function getIsInstallDefault()
	{
		return true;
	}

	/**
	 * Возвращаем статус, возможно ли модуль отключать:
	 *
	 * @return bool
	 **/
	public function getIsNoDisable()
	{
		return true;
	}
}
