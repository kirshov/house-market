<?php
use webforma\components\Event;

/**
 * Class CategoryListener
 */
class CategoryListener
{
    /**
     * onAfterSave event listener
     *
     * @param Event $event
     */
    public static function onAfterSave(Event $event)
    {
	    \TaggedCache\TaggingCacheHelper::deleteTag([CategoryHelper::CATEGORY_CACHE_TAG]);
    }

    /**
     * @param Event $event
     */
    public static function onAfterDelete(Event $event)
    {
	    \TaggedCache\TaggingCacheHelper::deleteTag([CategoryHelper::CATEGORY_CACHE_TAG]);
    }

}