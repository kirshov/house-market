<?php

class m150415_150436_rename_fields extends webforma\components\DbMigration
{
    public function safeUp()
    {
        $this->renameColumn('{{category_category}}', 'alias', 'slug');
    }
}