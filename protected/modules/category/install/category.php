<?php
/**
 * Конфигурационный файл модуля
 */
return [
    'module' => [
        'class' => 'application.modules.category.CategoryModule',
    ],
    'import' => [
        'application.modules.category.models.*',
        'application.modules.category.events.*',
        'application.modules.category.listeners.*',
        'application.modules.category.helpers.*',
        'application.modules.category.components.*',
    ],
    'component' => [
        'eventManager' => [
            'class' => 'webforma\components\EventManager',
            'events' => [
                'category.after.save' => [
                    ['\CategoryListener', 'onAfterSave']
                ],
                'category.after.delete' => [
                    ['\CategoryListener', 'onAfterDelete']
                ],

            ]
        ],
        'categoriesRepository' => [
            'class' => 'application.modules.category.components.CategoryRepository'
        ],
    ],
    'rules' => [],
];
