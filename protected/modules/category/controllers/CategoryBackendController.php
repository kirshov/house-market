<?php

/**
 * CategoryBackendController контроллер для управления категориями в панели управления
 */
class CategoryBackendController extends webforma\components\controllers\BackController
{
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Category.CategoryBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Category.CategoryBackend.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['Category.CategoryBackend.Create']],
            ['allow', 'actions' => ['update', 'inline'], 'roles' => ['Category.CategoryBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Category.CategoryBackend.Delete']],
            ['deny']
        ];
    }

    public function actions()
    {
        return [
            'inline' => [
                'class' => 'webforma\components\actions\WInLineEditAction',
                'model' => 'Category',
                'validAttributes' => ['name', 'description', 'slug', 'status']
            ]
        ];
    }

    /**
     * Отображает категорию по указанному идентификатору
     *
     * @param integer $id Идинтификатор категорию для отображения
     *
     * @return void
     */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }

    /**
     * Создает новую модель категории.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return void
     */
    public function actionCreate()
    {
        $model = new Category();

        if (($data = Yii::app()->getRequest()->getPost('Category')) !== null) {

            $model->setAttributes($data);

            if ($model->save()) {

                Yii::app()->user->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('CategoryModule.category', 'Record was created!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['create']
                    )
                );
            }
        }

        $this->render('create', ['model' => $model]);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id the ID of the model to be updated
     *
     * @return void
     */
    public function actionUpdate($id)
    {
        // Указан ID новости страницы, редактируем только ее
        $model = $this->loadModel($id);

        if (($data = Yii::app()->getRequest()->getPost('Category')) !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('Category'));

            if ($model->save()) {

                Yii::app()->user->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('CategoryModule.category', 'Category was changed!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id,
                        ]
                    )
                );
            }
        }

        $this->render(
            'update',
            [
                'model' => $model,
            ]
        );
    }

    /**
     * Удаяет модель категории из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id идентификатор категории, который нужно удалить
     *
     * @return void
     *
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            $transaction = Yii::app()->db->beginTransaction();

            try {
                // поддерживаем удаление только из POST-запроса
                $this->loadModel($id)->delete();
                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser

                $transaction->commit();

                if (!isset($_GET['ajax'])) {
                    $this->redirect(
                        (array)Yii::app()->getRequest()->getPost('returnUrl', 'index')
                    );
                }
            } catch (Exception $e) {
                $transaction->rollback();

                Yii::log($e->__toString(), CLogger::LEVEL_ERROR);
            }

        } else {
            throw new CHttpException(
                400,
                Yii::t('CategoryModule.category', 'Bad request. Please don\'t use similar requests anymore')
            );
        }
    }

    /**
     * Управление категориями.
     *
     * @return void
     */
    public function actionIndex($module = null, $parent = null)
    {
        $model = new Category('search');
        $model->unsetAttributes(); // clear any default values

        if (isset($_GET['Category'])) {
            $model->attributes = $_GET['Category'];
        }

        if (null !== $module && null !== $parent) {
            $model->parent_id = $parent;
        }

        $this->render('index', ['model' => $model]);
    }

    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param integer $id идентификатор нужной модели
     *
     * @return Category $model
     *
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Category::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('CategoryModule.category', 'Page was not found!'));
        }

        return $model;
    }

}
