<?php
/**
 * @var $this CategoryBackendController
 * @var $form \webforma\widgets\ActiveForm
 * @var $model Category
 */

$form = $this->beginWidget(
    '\webforma\widgets\ActiveForm',
    [
        'id' => 'category-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => false,
		'type' => 'vertical',
		'htmlOptions' => ['class' => 'well sticky', 'enctype' => 'multipart/form-data'],
		'clientOptions' => [
			'validateOnSubmit' => true,
		],
    ]
); ?>
<div class="alert alert-info">
    <?= Yii::t('CategoryModule.category', 'Fields with'); ?>
    <span class="required">*</span>
    <?= Yii::t('CategoryModule.category', 'are required'); ?>
</div>

<?= $form->errorSummary($model); ?>

<div class="row">
	<div class="col-sm-6">
		<div class="row">
			<div class='col-sm-6'>
				<?= $form->dropDownListGroup($model, 'status', [
					'widgetOptions' => [
						'data' => $model->getStatusList(),
					],
				]); ?>
			</div>

			<div class='col-sm-6'>
				<?= $form->dropDownListGroup($model, 'parent_id', [
					'widgetOptions' => [
						'data' => Yii::app()->getComponent('categoriesRepository')->getFormattedList(),
						'htmlOptions' => [
							'empty' => Yii::t('CategoryModule.category', '--no--'),
							'encode' => false,
						],
					],
				]); ?>
			</div>
		</div>

		<div class='row'>
			<div class="col-sm-12">
				<?= $form->textFieldGroup($model, 'name'); ?>
			</div>
		</div>

		<div class='row'>
			<div class="col-sm-12">
				<?= $form->slugFieldGroup($model, 'slug', ['sourceAttribute' => 'name']); ?>
			</div>
		</div>
	</div>


	<div class="col-sm-6">
		<div class='row'>
			<div class="col-sx-8 col-sm-8 col-sm-offset-2">
				<?php $this->widget('webforma\widgets\WInputFile', [
					'model' => $model,
					'attribute' => 'image',
				]); ?>
			</div>
		</div>
	</div>
</div>

<div class='row'>
    <div class="col-sm-12">
        <div class="form-group">
            <?= $form->labelEx($model, 'description'); ?>
            <?php $this->widget($this->module->getVisualEditor(), [
				'model' => $model,
				'attribute' => 'description',
			]); ?>
        </div>
    </div>
</div>

<div class='row'>
    <div class="col-sm-12">
        <div class="form-group">
            <?= $form->labelEx($model, 'short_description'); ?>
            <?php $this->widget(
                $this->module->getVisualEditor(),
                [
                    'model' => $model,
                    'attribute' => 'short_description',
                ]
            ); ?>
        </div>
    </div>
</div>

<div class="buttons">
	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context' => 'primary',
		'label' => $model->isNewRecord
			? Yii::t('CategoryModule.category', 'Create category and continue')
			: Yii::t('CategoryModule.category', 'Save category and continue'),
	]); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label' => $model->isNewRecord
			? Yii::t('CategoryModule.category', 'Create category and close')
			: Yii::t('CategoryModule.category', 'Save category and close'),
	]); ?>
</div>

<?php $this->endWidget(); ?>
