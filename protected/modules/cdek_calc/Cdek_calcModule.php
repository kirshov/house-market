<?php

use webforma\components\WebModule;

/**
 * Class Cdek_calcModule
 */
class Cdek_calcModule extends WebModule
{
	/**
	 *
	 */
	public $adminMenuOrder = 20;

	/**
	 *
	 */
	public $login;

	/**
	 *
	 */
	public $password;

	/**
	 *
	 */
	public $cityFrom;

	/**
	 *
	 */
	public $tariffId;

	/**
	 *
	 */
	public $pickupTariffs = [136];

	/**
	 * @return array
	 */
	public function getDependencies()
	{
		return ['order', 'store'];
	}

    /**
     * @return array
     */
    public function getEditableParams()
    {
        return [
            'login',
            'password',
            'cityFrom',
            'tariffId',
        ];
    }

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [
           'login' => 'Логин ИМ магазина',
           'password' => 'Пароль ИМ магазина',
           'cityFrom' => 'Id города отправления',
           'tariffId' => 'Id тарифа',
        ];
    }

	public function getEditableParamsGroups()
	{
		return [
			'main' => [
				'label' => Yii::t('WebformaModule.webforma', 'Main module settings'),
				'items' => [
					'login',
					'password',
					'cityFrom',
					'tariffId',
				]
			],
		];
	}

    /**
     * @return string
     */
    public function getCategory()
    {
        return 'Магазин';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Доставка СДЭК';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return '';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-calculator';
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport([
        	'cdek_calc.components.*',
        ]);
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return $this->getSettingsUrl();
    }

    public function setCityId(){
	    if(!$_SESSION['geo']['cdek_city_id'] && $_SESSION['geo']['city']['name_ru']){
		    $cdekCity = new CdekCity();
		    $cityId = $cdekCity->getByName($_SESSION['geo']['city']['name_ru']);
		    $_SESSION['geo']['cdek_city_id'] = $cityId;
	    }
    }
}
