<?php
class CdekCity
{
	public function getFileName()
	{
		$path = Yii::getPathOfAlias('webroot').'/'.Yii::app()->getModule('webforma')->uploadPath.'/cdek';
		if(!is_dir($path)){
			CFileHelper::createDirectory($path, null, true);
		}
		return $path.'/cities.xml';
	}

	public function getByName($name){
		if(!$name){
			return false;
		}
		$data = $this->getCities();
		if(!$data){
			return false;
		}
		$xml = new SimpleXMLElement($data);
		$cityData = $xml->xpath("//PvzList/Pvz[@City='{$name}'][position() <= 1]");
		if($cityData){
			$city = (int) $cityData[0]['CityCode'];
			if($city){
				return $city;
			}
		}

		return false;
	}

	public function getOffices($name){
		if(!$name){
			return false;
		}
		$data = $this->getCities();
		if(!$data){
			return false;
		}
		$xml = new SimpleXMLElement($data);
		$cityData = $xml->xpath("//PvzList/Pvz[@City='{$name}']");

		$offices = [];
		if($cityData){
			foreach ($cityData as $cityItem){
				$address = (string) $cityItem['Address'];
				if($address){
					$offices[$address] = $address;
				}
			}

			if(!empty($offices)){
				asort($offices);
				array_unshift($offices, 'Выберите пункт выдачи');
			}

			return$offices;
		}

		return false;
	}

	protected function getCities(){
		$file = $this->getFileName();
		if(!file_exists($file) || strtotime('+1 mont', filemtime($file)) > time()){
			return $this->loadCities();
		}

		return file_get_contents($file);
	}

	protected function loadCities(){
		$xml = file_get_contents('https://integration.cdek.ru/pvzlist/v1/xml');
		file_put_contents($this->getFileName(), $xml);
		return $xml;
	}
}