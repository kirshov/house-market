<?php

class CdekCalcProcess
{
	public $module;
	public function __construct()
	{
		$this->module = Yii::app()->getModule('cdek_calc');
	}

	/**
	 * @param $tariffId
	 * @return bool
	 */
	public function getDeliveryPrice($tariffId){
		$cookieKey = 'cdek_'.$_SESSION['geo']['cdek_city_id'].'_'.$tariffId;
		if($_COOKIE[$cookieKey]){
			return $_COOKIE[$cookieKey];
		}
		$cdekCalc = new CalculatePriceDeliveryCdek();
		$cdekCalc->setAuth($this->module->login, $this->module->password);
		$cdekCalc->setSenderCityId($this->module->cityFrom);
		$cdekCalc->setReceiverCityId($_SESSION['geo']['cdek_city_id']);
		$cdekCalc->setTariffId($tariffId);
		$cdekCalc->setModeDeliveryId($this->getModeByTariff($tariffId));
		$cdekCalc->addGoodsItemBySize(1, 10, 10, 10);
		if ($cdekCalc->calculate() === true) {
			$result = $cdekCalc->getResult();
			$price = $result['result']['price'];

			if($price > 0 && $price < 150){
				$price += 30;
			}

			setcookie ($cookieKey, $price, time() + 60 * 60 * 24, '/');
			return $price;
		} else {
			Log::addToFile($cdekCalc->getError(), 'cdek_calc.log');
		}

		return false;
	}

	/**
	 * @param $tariff
	 * @return int
	 */
	public function getModeByTariff($tariff){
		$data = [
			136 => 4,
			137 => 3,
		];

		return $data[$tariff] ?: 4;
	}
}