<?php
return [
    'module' => [
        'class' => 'application.modules.cdek_calc.Cdek_calcModule',
    ],
    'import' => [
        'application.modules.cdek_calc.components.*',
    ],
    'component' => [],
    'rules' => [
    	'/getPriceCdek' => '/cdek_calc/cdek_calc/getCdekPrice'
    ]
];
