<?php

use webforma\components\controllers\FrontController;

/**
 * Class Cdek_calcController
 */
class Cdek_calcController extends FrontController
{
	/**
	 * @var Cdek_calcModule $module
	 */
	public $module;

	public function init()
	{
		parent::init();
		$this->module = Yii::app()->getModule('cdek_calc');
	}

    public function actionCdek()
    {

    }

	public function actionGetCdekPrice(){
		if (!$this->module->cityFrom) {
			$this->displayUndefined();
		}

		$this->module->setCityId();
		if (!$_SESSION['geo']['cdek_city_id']) {
			$this->displayUndefined();
		}

		$calcProcess = new CdekCalcProcess();
		$currency = Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency);
		$tariffs = Yii::app()->getRequest()->getParam('tariffs');
		if($tariffs){
			$data = [];
			foreach ($tariffs as $tariff){
				$_price = false;
				if(is_numeric($tariff)){
					$_price = $calcProcess->getDeliveryPrice($tariff);
				}

				$data[$tariff] = [
					'price' =>	$_price ? Helper::getFormatPrice($_price). ' '.$currency : 'Сервер ТК не ответил',
					'price_num' => (int) $_price,
					'available' => is_numeric($_price),
				];
			}
			echo json_encode($data);
		} else {
			if(Yii::app()->getRequest()->getIsAjaxRequest()){
				$priceStock = $calcProcess->getDeliveryPrice(136);
				$priceApartment = $calcProcess->getDeliveryPrice(137);
				echo json_encode([
					'cdek_stock' => is_numeric($priceStock) ? Helper::getFormatPrice($priceStock). ' '.$currency : 'Сервер ТК не ответил',
					'cdek_apartment' => is_numeric($priceApartment) ? Helper::getFormatPrice($priceApartment). ' '.$currency : 'Сервер ТК не ответил',
				]);
			}
		}

		Yii::app()->end();
	}

	/**
	 * @param string $title
	 */
	protected function displayUndefined($title = 'Недоступно'){
		$tariffs = Yii::app()->getRequest()->getParam('tariffs');
		$data = [];
		if($tariffs){
			foreach ($tariffs as $tariff){
				$data[$tariff] = [
					'price' => $title,
					'price_num' => 0,
					'available' => false,
				];
			}
		} else {
			$data = [
				'cdek_stock' => $title,
				'cdek_apartment' => $title,
			];
		}

		echo json_encode($data);
		Yii::app()->end();
	}

}
