<?php
namespace notify\components;

use Yii;
use CApplicationComponent;
use User;
use webforma\components\Mail;

class Notify extends CApplicationComponent
{
    /**
     * @var
     */
    public $mail;

    /**
     *
     */
    public function init()
    {
        parent::init();
        $mailer = Yii::createComponent($this->mail);
        $mailer->init();
        $this->setMail($mailer);
    }

    /**
     * @param Mail $mail
     */
    public function setMail(Mail $mail)
    {
        $this->mail = $mail;
    }

    /**
     * @param  User $user
     * @param $theme
     * @param $view
     * @param $data
     * @param bool $isRenderPartial
     * @return mixed
     */
    public function send(User $user, $theme, $view, $data, $isRenderPartial = false)
    {
        $data['user'] = $user;

        if($isRenderPartial){
        	$body = Yii::app()->getController()->renderPartial($view, $data, true);
        } else {
	        $body = Yii::app()->getController()->render($view, $data, true);
        }

        return $this->mail->send(
            Yii::app()->getModule('webforma')->notifyEmailFrom,
            $user->email,
            $theme,
	        $body
        );
    }

	/**
	 * @param $theme
	 * @param $body
	 * @return mixed
	 */
	public function sendToAdmin($theme, $body)
	{
		return $this->mail->send(
			Yii::app()->getModule('webforma')->notifyEmailFrom,
			Yii::app()->getModule('webforma')->notifyEmailAdmin,
			$theme,
			$body
		);
	}

	/**
	 * @param $theme
	 * @param $body
	 * @return mixed
	 */
	public function sendDefaultNotify($theme, $body)
	{
		return $this->mail->send(
			'notify@'.$_SERVER['HTTP_HOST'],
			'info@'.$_SERVER['HTTP_HOST'],
			$theme,
			$body
		);
	}
}
