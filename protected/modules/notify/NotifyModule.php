<?php

use webforma\components\WebModule;

/**
 * Class NotifyModule
 */
class NotifyModule extends WebModule
{
    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            'mail',
        ];
    }

    /**
     * @return bool
     */
    public function getIsNoDisable()
    {
        return true;
    }

    /**
     *
     */
    public function init()
    {
        $this->setImport(
            [
                'notify.models.*',
            ]
        );

        parent::init();
    }

    /**
     * @return string
     */
    public function getCategory()
    {
    	return false;
        return Yii::t('NotifyModule.notify', 'Users');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('NotifyModule.notify', 'Notify');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('NotifyModule.notify', 'Module for user notifications');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return "fa fa-fw fa-paper-plane";
    }


    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/notify/notifyBackend/index';
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('NotifyModule.notify', 'Notify settings'),
                'url' => ['/notify/notifyBackend/index'],
            ],
        ];
    }

	/**
	 * @return bool
	 */
	public function getIsInstallDefault()
	{
		return true;
	}
}
