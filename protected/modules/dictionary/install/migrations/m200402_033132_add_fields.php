<?php

class m200402_033132_add_fields extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{dictionary_data}}', 'data', 'text default null');
		$this->addColumn('{{dictionary_group}}', 'type', 'varchar(255) not null');
	}

	public function safeDown()
	{

	}
}