<?php

class m190719_033137_add_sort extends webforma\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{dictionary_data}}', 'position', 'int(11) default 1');
		$this->createIndex('ix_{{dictionary_data}}_position', '{{dictionary_data}}', 'position', false);
	}

	public function safeDown()
	{

	}
}