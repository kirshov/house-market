<?php

/**
 * Dictionary install migration
 **/
class m000000_000000_dictionary_base extends webforma\components\DbMigration
{

    public function safeUp()
    {
        $this->createTable(
            '{{dictionary_group}}',
            [
                'id'             => 'pk',
                'code'           => 'varchar(100) NOT NULL',
                'name'           => "varchar(250) NOT NULL",
                'description'    => "varchar(250) NOT NULL DEFAULT ''",
                'create_time'  => 'datetime NOT NULL',
                'update_time'    => 'datetime NOT NULL',
                'create_user_id' => 'integer DEFAULT NULL',
            ],
            $this->getOptions()
        );

        //ix
        $this->createIndex("ux_{{dictionary_group}}_code", '{{dictionary_group}}', "code", true);
        $this->createIndex("ix_{{dictionary_group}}_create_user_id",'{{dictionary_group}}',"create_user_id",false);

        //fk
        $this->addForeignKey("fk_{{dictionary_group}}_create_user_id",'{{dictionary_group}}','create_user_id','{{user_user}}','id','SET NULL','NO ACTION');

        /**
         * Dictionary_data
         **/
        $this->createTable(
            '{{dictionary_data}}',
            [
                'id'             => 'pk',
                'group_id'       => 'integer NOT NULL',
                'name'           => 'varchar(250) NOT NULL',
                'image'          => "varchar(250) DEFAULT NULL",
                'description'    => "TEXT DEFAULT NULL",
                'create_time'    => 'datetime NOT NULL',
                'update_time'    => 'datetime NOT NULL',
                'create_user_id' => 'integer DEFAULT NULL',
                'status'         => "integer NOT NULL DEFAULT '1'",
            ],
            $this->getOptions()
        );

        $this->createIndex("ix_{{dictionary_data}}_group_id", '{{dictionary_data}}', "group_id",false);
        $this->createIndex("ix_{{dictionary_data}}_create_user_id",'{{dictionary_data}}',"create_user_id",false);

        $this->createIndex("ix_{{dictionary_data}}_status",'{{dictionary_data}}',"status",false);

        //fk
        $this->addForeignKey("fk_{{dictionary_data}}_create_user_id",'{{dictionary_data}}','create_user_id','{{user_user}}','id','SET NULL','NO ACTION');

        $this->addForeignKey("fk_{{dictionary_data}}_data_group_id",'{{dictionary_data}}','group_id','{{dictionary_group}}','id','CASCADE','NO ACTION');
    }

    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{dictionary_data}}');
        $this->dropTableWithForeignKeys('{{dictionary_group}}');
    }
}
