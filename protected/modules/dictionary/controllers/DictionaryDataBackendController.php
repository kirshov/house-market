<?php

/**
 * DictionaryDataBackend контроллер для управления данными справочников в панели управления
 */
class DictionaryDataBackendController extends webforma\components\controllers\BackController
{
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Dictionary.DictionaryDataBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Dictionary.DictionaryDataBackend.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['Dictionary.DictionaryDataBackend.Create']],
            ['allow', 'actions' => ['update', 'inline'], 'roles' => ['Dictionary.DictionaryDataBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Dictionary.DictionaryDataBackend.Delete']],
            ['deny']
        ];
    }

    public function actions()
    {
        return [
            'inline' => [
                'class'           => 'webforma\components\actions\WInLineEditAction',
                'model'           => 'DictionaryData',
                'validAttributes' => ['name', 'group_id', 'status']
            ],
	        'sortable' => [
		        'class' => 'webforma\components\actions\SortAction',
		        'model' => 'DictionaryData',
		        'attribute' => 'position',
	        ],
        ];
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return void
     */
    public function actionCreate()
    {
		$group = $this->loadGroup();
		$group_id = (int)Yii::app()->getRequest()->getQuery('group_id');

		$modelName = $group->type ?: 'DictionaryData';
		$model = new $modelName();

		if ($group_id) {
			$model->group_id = $group_id;
		}

		if (($data = Yii::app()->getRequest()->getPost($modelName)) !== null) {
			$model->setAttributes($data);

            if ($model->save()) {
                Yii::app()->user->setFlash(
	                webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('DictionaryModule.dictionary', 'Record was added!')
                );

				if(Yii::app()->getRequest()->getPost('submit-type')){
					$redirect = [Yii::app()->getRequest()->getPost('submit-type'), 'group_id' => $model->group_id];
				} else {
					$redirect = ['create', 'group_id' => $model->group_id];
				}
				$this->redirect($redirect);
            }
        }

        $this->render('create', [
        	'group' => $group,
        	'model' => $model,
		]);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id the ID of the model to be updated
     *
     * @return void
     */
    public function actionUpdate($id)
    {
		$group = $this->loadGroup();

		$modelName = $group->type ?: 'DictionaryData';

        $model = $this->loadModel($id, $modelName);

		if (($data = Yii::app()->getRequest()->getPost($modelName)) !== null) {

            $model->setAttributes($data);

            if ($model->save()) {

                Yii::app()->user->setFlash(
	                webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('DictionaryModule.dictionary', 'Record was updated!')
                );

                if(Yii::app()->getRequest()->getPost('submit-type')){
                	$redirect = [Yii::app()->getRequest()->getPost('submit-type'), 'group_id' => $model->group_id];
				} else {
                	$redirect = ['update', 'id' => $model->id, 'group_id' => $model->group_id];
				}
                $this->redirect($redirect);
            }
        }

        $this->render('update', [
        	'group' => $group,
        	'model' => $model,
		]);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     *
     * @param integer $id the ID of the model to be deleted
     *
     * @return void
     *
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {

            // we only allow deletion via POST request
            $model = $this->loadModel($id);
            $groupId = $model->group_id;
            $model->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            Yii::app()->user->setFlash(
	            webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                Yii::t('DictionaryModule.dictionary', 'Record was removed!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!isset($_GET['ajax'])) {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : ['index', 'group_id' => $groupId]);
            }

        } else {
            throw new CHttpException(
                400,
                Yii::t('DictionaryModule.dictionary', 'Bad request. Please don\'t repeate similar requests anymore')
            );
        }
    }

    /**
     * Manages all models.
     *
     * @return void
     */
    public function actionIndex()
    {
        $model = new DictionaryData('search');

        $model->unsetAttributes(); // clear any default values

        $group_id = (int)Yii::app()->getRequest()->getQuery('group_id');

        $group = $this->loadGroup();

        if ($group_id) {
            $model->group_id = $group_id;
        }

        $model->setAttributes(Yii::app()->getRequest()->getParam('DictionaryData', []));

        $this->render('index', [
        	'model' => $model,
        	'group' => $group,
		]);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     *
     * @param integer the ID of the model to be loaded
     * @param string $type
     *
     * @return DictionaryData $model
     *
     * @throws CHttpException
     */
    public function loadModel($id, $type = 'DictionaryData')
    {
        $model = $type::model()->findByPk((int)$id);

        if ($model === null) {
            throw new CHttpException(404, Yii::t('DictionaryModule.dictionary', 'Requested page was not found'));
        }

        return $model;
    }

	/**
	 * @throws CHttpException
	 * @return DictionaryGroup
	 */
    public function loadGroup(){
		$groupId = (int) Yii::app()->getRequest()->getQuery('group_id');

		if (!$groupId) {
			throw new CHttpException(404, Yii::t('DictionaryModule.dictionary', 'Requested page was not found'));
		}

		$group = DictionaryGroup::model()->findByPk($groupId);
		if ($group === null) {
			throw new CHttpException(404, Yii::t('DictionaryModule.dictionary', 'Requested page was not found'));
		}
		return $group;
	}
}
