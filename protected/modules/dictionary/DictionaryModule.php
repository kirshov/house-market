<?php

/**
 * DictionaryModule основной класс модуля dictionary
 */
class DictionaryModule extends webforma\components\WebModule
{
	/**
	 *
	 */
	public $adminMenuOrder = 110;

	public $allowCreate = false;
    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            'user',
        ];
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('DictionaryModule.dictionary', 'Content');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('DictionaryModule.dictionary', 'Dictionaries');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('DictionaryModule.dictionary', 'Module for simple dictionary');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-book';
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/dictionary/dictionaryBackend/index';
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
		$data = [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => 'Все справочники',
                'url' => ['/dictionary/dictionaryBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('DictionaryModule.dictionary', 'Create dictionary'),
                'url' => ['/dictionary/dictionaryBackend/create'],
				'visible' => $this->allowCreate || DEVMODE,
            ],
        ];

		$dictionaries = DictionaryGroup::model()->findAll(['order' => 't.name']);
		if(!$dictionaries){
			return $data;
		}

		$data[] = ['label' => Yii::t('DictionaryModule.dictionary', 'Dictionaries')];
		foreach ($dictionaries as $item){
			$data[] = [
				'icon' => 'fa fa-fw fa-list-alt',
				'label' => strip_tags($item->name),
				'url' => ['/dictionary/dictionaryDataBackend/index', 'group_id' => $item->id],
			];
		}

		return $data;
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'dictionary.models.*',
            ]
        );
    }
}
