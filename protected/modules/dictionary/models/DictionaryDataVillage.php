<?php

class DictionaryDataVillage extends DictionaryData
{
	/**
	 * @var
	 */
	public $map;

	/**
	 * @var
	 */
	public $bold;
	/**
	 * @var bool
	 */
	public $showDescription = false;

	/**
	 * @var bool
	 */
	public $showImage = false;

	/**
	 * @var array
	 */
	public $fields = [
		'bold',
		'map',
	];

	/**
	 * @var array
	 */
	public $fieldsTitle = [
		'bold' => 'Выделить жирным',
		'map' => 'Код карты поселка',
	];

	/**
	 * @var array
	 */
	public $fieldsType = [
		'bold' => 'checkbox',
		'map' => 'textarea',
	];

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @param $id
	 * @param string $key
	 * @return mixed
	 */
	public static function getValue($id, $key = 'name'){
		return self::getDictionaryValue($id, $key);
	}

	/**
	 * @param $id
	 * @param string $key
	 * @return mixed
	 */
	protected static function getDictionaryValue($id, $key = 'name'){
		$dataContainer = Yii::app()->getComponent('dataContainer');
		$cacheKey = 'dictionary_'.$id;

		if($dataContainer->has($cacheKey)){
			return $dataContainer->get($cacheKey)->{$key};
		}

		$data = self::model()->published()->findAll();
		foreach ($data as $item){
			$dataContainer->set('dictionary_'.$item->id, $item);
		}

		return $dataContainer->get($cacheKey)->{$key};
	}
}
