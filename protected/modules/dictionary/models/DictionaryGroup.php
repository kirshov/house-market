<?php
/**
 * DictionaryData модель "справочник"
 */

/**
 * This is the model class for table "dictionary_group".
 *
 * The followings are the available columns in table 'dictionary_group':
 * @property string $id
 * @property string $code
 * @property string $name
 * @property string $type
 * @property string $description
 * @property string $create_time
 * @property string $update_time
 * @property string $create_user_id
 *
 * The followings are the available model relations:
 * @property DictionaryData[] $dictionaryData
 * @property User $updateUser
 * @property User $createUser
 */
class DictionaryGroup extends webforma\models\WModel
{
	const TYPE_STANDARD = 'DictionaryData';
	const TYPE_VILLAGE = 'DictionaryDataVillage';
    /**
     * Returns the static model of the specified AR class.
     * @param  string $className
     * @return DictionaryGroup the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{dictionary_group}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['code, name, type', 'required'],
            ['code, type', 'length', 'max' => 100],
            ['name, description', 'length', 'max' => 250],
            ['create_user_id', 'length', 'max' => 10],
            ['code', 'webforma\components\validators\YSLugValidator'],
            ['code', 'unique'],
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            [
                'id, code, name, description, create_time, update_time, create_user_id',
                'safe',
                'on' => 'search'
            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'dictionaryData' => [self::HAS_MANY, 'DictionaryData', 'group_id', 'order' => 'position'],
            'createUser'     => [self::BELONGS_TO, 'User', 'create_user_id'],
            'dataCount'      => [self::STAT, 'DictionaryData', 'group_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id'             => Yii::t('DictionaryModule.dictionary', 'id'),
            'code'           => Yii::t('DictionaryModule.dictionary', 'Code'),
            'name'           => Yii::t('DictionaryModule.dictionary', 'Title'),
            'description'    => Yii::t('DictionaryModule.dictionary', 'Description'),
            'create_time'  => Yii::t('DictionaryModule.dictionary', 'Created at'),
            'update_time'    => Yii::t('DictionaryModule.dictionary', 'Updated at'),
            'create_user_id' => Yii::t('DictionaryModule.dictionary', 'Created by.'),
            'type' => 'Тип справочника',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria();

        $criteria->compare('id', $this->id, true);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);
        $criteria->compare('create_user_id', $this->create_user_id, true);

        return new CActiveDataProvider(get_class($this), ['criteria' => $criteria]);
    }

	/**
	 * @return bool
	 */
    public function beforeSave()
    {
        $this->update_time = new CDbExpression('NOW()');

        if ($this->isNewRecord) {
            $this->create_user_id = Yii::app()->user->getId();
            $this->create_time = $this->update_time;
        }

        \TaggedCache\TaggingCacheHelper::deleteTag(['navigation']);
        return parent::beforeSave();
    }

	public function delete()
	{
		\TaggedCache\TaggingCacheHelper::deleteTag(['navigation']);
		return parent::delete();
	}
	
	/**
	 * @param $code
	 * @return DictionaryGroup
	 */
	public static function getByCode($code)
	{
		return self::model()->cache(Helper::getCacheTime())->find('code = :code', ['code' => $code]);
	}

	/**
	 * @return DictionaryData[]
	 */
    public function getData()
    {
    	$modelName = $this->type ?: self::TYPE_STANDARD;

        return $modelName::model()->cache(Helper::getCacheTime(), \TaggedCache\TaggingCacheHelper::getDependency(['dictionary-items-'.$this->id]))->findAll(
            [
                'condition' => 'group_id = :group_id',
                'params'    => [':group_id' => $this->id],
                'scopes'    => ['published'],
                'order'     => 'position',
            ]
        );
    }

	/**
	 * @param $code
	 * @param $key
	 * @param $value
	 * @return array
	 */
	public static function getHtmlList($code, $key = 'id', $value = 'name'){
		$data = [];

		$model = self::getByCode($code);

		if($model){
			$data = CHtml::listData($model->dictionaryData, $key, $value);
		}

		return $data;
	}

	/**
	 * @return array
	 */
	public function getFormattedList()
	{
		return CHtml::listData(self::model()->findAll(), 'id', 'name');
	}

	/**
	 * @return array
	 */
	public function getTypes(){
		return [
			self::TYPE_STANDARD => 'Обычный справочник',
			self::TYPE_VILLAGE => 'Справочник поселков',
		];
	}

	/**
	 * @param $code
	 * @param $key
	 * @param $value
	 * @return array
	 */
	public static function getVillageOptionsList($code){
		$data = [];

		$model = self::getByCode($code);

		if($model){
			foreach (DictionaryDataVillage::model()->findAll('group_id = :group_id', [':group_id' => $model->id]) as $item){
				$data[$item->id]['class'] = $item->bold ? 'item-bold' : '';
			}
		}

		return $data;
	}
}
