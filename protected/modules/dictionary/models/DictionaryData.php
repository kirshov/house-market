<?php
/**
 * DictionaryData модель "данные справочника"
 */

/**
 * This is the model class for table "dictionary_data".
 *
 * The followings are the available columns in table 'dictionary_data':
 * @property string $id
 * @property string $group_id
 * @property string $name
 * @property string $description
 * @property string $create_time
 * @property string $update_time
 * @property integer $create_user_id
 * @property integer $status
 * @property integer $position
 * @property string $data
 *
 * The followings are the available model relations:
 * @property User $updateUser
 * @property DictionaryGroup $group
 * @property User $createUser
 *
 * @method DictionaryData published
 */
class DictionaryData extends webforma\models\WModel
{
    const STATUS_ACTIVE = 1;
    const STATUS_DELETED = 0;

	/**
	 * @var array
	 */
    public $fields = [];

	/**
	 * @var array
	 */
    public $fieldsTitle = [];

	/**
	 * @var array
	 */
	public $fieldsType = [];

	/**
	 *
	 */
	public $showDescription = true;

	/**
	 *
	 */
	public $showImage = true;

    /**
     * Returns the static model of the specified AR class.
     * @return DictionaryData the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{dictionary_data}}';
    }

	public function behaviors()
	{
		return [
			'upload' => [
				'class' => 'webforma\components\behaviors\ImageUploadBehavior',
				'attributeName' => 'image',
				'uploadPath' => 'dictionary',
			],
			'sortable' => [
				'class' => 'webforma\components\behaviors\SortableBehavior',
				'attributeName' => 'position',
			],
		];
	}


	/**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        $data = [
            ['group_id, name', 'required'],
            ['status', 'numerical', 'integerOnly' => true],
            ['group_id, create_user_id', 'length', 'max' => 10],
            ['name, image', 'length', 'max' => 250],
	        ['description', 'filter', 'filter' => [new \webforma\widgets\WPurifier(), 'purify']],
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            [
                'id, group_id, name, description, create_time, update_time, create_user_id, status',
                'safe',
                'on' => 'search'
            ],
        ];

        if(!empty($this->fields)){
        	$data[] = [implode(', ', $this->fields), 'filter', 'filter' => 'trim'];
		}

        return $data;
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'group'      => [self::BELONGS_TO, 'DictionaryGroup', 'group_id'],
            'createUser' => [self::BELONGS_TO, 'User', 'create_user_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array_merge([
            'id'             => Yii::t('DictionaryModule.dictionary', 'id'),
            'group_id'       => Yii::t('DictionaryModule.dictionary', 'Dictionary'),
            'name'           => Yii::t('DictionaryModule.dictionary', 'Title'),
            'description'    => Yii::t('DictionaryModule.dictionary', 'Description'),
            'create_time'  => Yii::t('DictionaryModule.dictionary', 'Created at'),
            'update_time'    => Yii::t('DictionaryModule.dictionary', 'Updated at.'),
            'create_user_id' => Yii::t('DictionaryModule.dictionary', 'Created by.'),
            'status'         => Yii::t('DictionaryModule.dictionary', 'Active'),
            'image'         => Yii::t('DictionaryModule.dictionary', 'Image'),
        ], $this->fieldsTitle);
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria();

        $criteria->compare('id', $this->id, true);
        $criteria->compare('group_id', $this->group_id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);
        $criteria->compare('create_user_id', $this->create_user_id, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider(get_class($this), [
        	'criteria' => $criteria,
	        'sort' => ['defaultOrder' => 't.position'],
        ]);
    }

	/**
	 * @return array
	 */
	public function scopes()
	{
		return [
			'published' => [
				'condition' => 'status = :status',
				'params' => [':status' => self::STATUS_ACTIVE],
			],
		];
	}
	/**
	 * @return bool
	 */
    public function beforeSave()
    {
        $this->update_time = new CDbExpression('NOW()');

        if ($this->isNewRecord) {
            $this->create_user_id = Yii::app()->user->getId();
            $this->create_time = $this->update_time;
        }

        if($this->fields){
        	$data = [];
        	foreach ($this->fields as $item){
        		$data[$item] = $this->{$item};
			}
			$this->data = json_encode($data);
		}

        return parent::beforeSave();
    }

	/**
	 *
	 */
	protected function afterFind()
	{
		parent::afterFind();

		if($this->fields){
			$data = json_decode($this->data, true);
			foreach ($this->fields as $item){
				$this->{$item} = $data[$item];
			}
		}
	}

	/**
	 *
	 */
	protected function afterSave()
	{
		\TaggedCache\TaggingCacheHelper::deleteTag(['dictionary-items-'.$this->group_id]);
		parent::afterSave();
	}

	/**
	 *
	 */
	protected function afterDelete()
	{
		\TaggedCache\TaggingCacheHelper::deleteTag(['dictionary-items-'.$this->group_id]);
		parent::afterDelete();
	}

	/**
	 * @return array
	 */
    public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE  => Yii::t('DictionaryModule.dictionary', 'Yes'),
            self::STATUS_DELETED => Yii::t('DictionaryModule.dictionary', 'No'),
        ];
    }

	/**
	 * @return mixed|string
	 */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('DictionaryModule.dictionary', '*unknown*');
    }

	/**
	 * @param $field
	 * @return mixed|string
	 */
    public function getFieldType($field){
    	return isset($this->fieldsType[$field]) ? $this->fieldsType[$field] : 'text';
	}


	/**
	 * @param $floor
	 * @return string
	 */
	public static function getFloor($floor){
		return strip_tags(self::getDictionaryValue($floor, 'description'));
	}

	/**
	 * @param $id
	 * @param $key
	 * @return mixed
	 */
	public static function getValue($id, $key = 'name'){
		return self::getDictionaryValue($id, $key);
	}

	/**
	 * @param Product $product
	 * @return mixed
	 */
	public static function getReadiness($product){
		$value = self::getDictionaryValue($product->done_id);
		if($product->done_id == 5){
			return $value .'<span title="Дата сдачи">'.$product->date.'</span>';
		} else {
			return $value;
		}
		if($product->done_id == 5 && $product->date && $product->date != '0000-00-00'){
			$value .= ' <span title="Дата сдачи">'.$product->date.'</span>';
		}

		return $value;
	}

	/**
	 * @param $id
	 * @param string $key
	 * @return mixed
	 */
	protected static function getDictionaryValue($id, $key = 'name'){

		$dataContainer = Yii::app()->getComponent('dataContainer');
		$cacheKey = 'dictionary_'.$id;

		if($dataContainer->has($cacheKey)){
			return $dataContainer->get($cacheKey)->{$key};
		}

		$data = self::model()->published()->findAll();
		foreach ($data as $item){
			$dataContainer->set('dictionary_'.$item->id, $item);
		}

		return $dataContainer->get($cacheKey)->{$key};
	}
}
