<?php
Yii::import('application.modules.dictionary.models.*');
Yii::import('application.modules.dictionary.DictionaryModule');

/**
 * Class DictionaryWidget
 */
class DictionaryWidget extends webforma\widgets\WWidget
{
    /**
     * @var string
     */
    public $view;

	/**
	 * @var string
	 */
	public $code;

	/**
	 * @var integer
	 */
	public $id;

	/**
	 * @var bool
	 */
	public $displayTitle = false;

	/**
	 * @return bool|void
	 */
    public function run()
    {
    	if($this->id){
			$model = DictionaryGroup::model()->findByPk($this->id);
		} else if($this->code) {
			$model = DictionaryGroup::getByCode($this->code);
		}

		if(!$model){
			return false;
		}

		if($this->view){
			$view = $this->view;
		} else {
			$viewPath = $this->getViewPath();
			if(file_exists($viewPath.'/'.$this->code.'.php')){
				$view = $this->code;
			}
		}

		if(!$view){
			$view = 'default';
		}

        $this->render($view, [
        	'model' => $model,
        	'displayTitle' => $this->displayTitle,
        	'items' => $model->getData(),
        ]);
    }
}
