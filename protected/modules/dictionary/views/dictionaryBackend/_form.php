<?php
/**
 * @var $this DictionaryBackendController
 * @var $model DictionaryGroup
 * @var $form \webforma\widgets\ActiveForm
 */
$form = $this->beginWidget(
    'webforma\widgets\ActiveForm',
    [
        'id'                     => 'dictionary-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'type'                   => 'vertical',
	    'htmlOptions' 			 => ['class' => 'well sticky'],
    ]
); ?>

<div class="alert alert-info">
    <?=  Yii::t('DictionaryModule.dictionary', 'Fields with'); ?>
    <span class="required">*</span>
    <?=  Yii::t('DictionaryModule.dictionary', 'are required.'); ?>
</div>

<?=  $form->errorSummary($model); ?>

<div class='row'>
    <div class="col-sm-6">
        <?=  $form->textFieldGroup($model, 'name'); ?>
    </div>
</div>
<div class='row'>
	<div class="col-sm-6">
		<?=  $form->dropDownListGroup($model, 'type', [
			'widgetOptions' => [
				'data'        => $model->getTypes(),
			],
		]); ?>
	</div>
</div>

<div class='row'>
	<div class="col-sm-6">
		<?=  $form->slugFieldGroup($model, 'code', ['sourceAttribute' => 'name']); ?>
	</div>
</div>

<div class="row">
    <div class="col-sm-12">
	    <?=  $form->textAreaGroup($model, 'description', ['rows' => 2]); ?>
    </div>
</div>
<div class="buttons">
	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context'    => 'primary',
		'label'      => $model->isNewRecord
			? Yii::t('DictionaryModule.dictionary', 'Create dictionary and continue')
			: Yii::t('DictionaryModule.dictionary', 'Save dictionary and continue'),
	]); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType'  => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label'       => $model->isNewRecord
			? Yii::t('DictionaryModule.dictionary', 'Create dictionary and close')
			: Yii::t('DictionaryModule.dictionary', 'Save dictionary and close'),
	]); ?>
</div>

<?php $this->endWidget(); ?>
