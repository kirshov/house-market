<?php
$this->breadcrumbs = [
    Yii::t('DictionaryModule.dictionary', 'Dictionaries') => ['/dictionary/dictionaryBackend/index'],
    Yii::t('DictionaryModule.dictionary', 'Management'),
];

$this->pageTitle = Yii::t('DictionaryModule.dictionary', 'Dictionaries - manage');

$this->menu = [
    [
        'label' => Yii::t('DictionaryModule.dictionary', 'Dictionaries'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('DictionaryModule.dictionary', 'Dictionaries management'),
                'url'   => ['/dictionary/dictionaryBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('DictionaryModule.dictionary', 'Dictionary crate'),
                'url'   => ['/dictionary/dictionaryBackend/create']
            ],
        ]
    ]
];
?>

<?php $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id'           => 'dictionary-group-grid',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'actionsButtons' => [
            CHtml::link(
                Yii::t('WebformaModule.webforma', 'Add'),
                ['/dictionary/dictionaryBackend/create'],
                ['class' => 'btn btn-success pull-right btn-sm']
            )
        ],
        'columns'      => [
            [
                'name'        => 'id',
                'htmlOptions' => ['style' => 'width:20px'],
                'type'        => 'raw',
                'value'       => 'CHtml::link($data->id, array("/dictionary/dictionaryBackend/update", "id" => $data->id))'
            ],
            [
                'class'    => 'webforma\widgets\WEditableColumn',
                'name'     => 'name',
                'editable' => [
                    'url'    => $this->createUrl('/dictionary/dictionaryBackend/inline'),
                    'mode'   => 'popup',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'name', ['class' => 'form-control']),
            ],
            [
                'class'    => 'webforma\widgets\WEditableColumn',
                'name'     => 'code',
                'editable' => [
                    'url'    => $this->createUrl('/dictionary/dictionaryBackend/inline'),
                    'mode'   => 'popup',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'code', ['class' => 'form-control']),
            ],
            [
                'header' => Yii::t('DictionaryModule.dictionary', 'Records'),
                'type'   => 'raw',
                'value'  => function ($data){
                    return CHtml::link('Значения: '.$data->dataCount, array('/dictionary/dictionaryDataBackend/index', 'group_id' => $data->id),['class' => 'label label-default', 'title' => 'Перейти к значениям справочника']);
                },
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
                'template' => '{add} {update} {delete}',
                'buttons'  => [
                    'add' => [
                        'icon'  => 'fa fa-fw fa-plus-square',
                        'label' => 'Добавить значение в справочник',
                        'url'   => 'Yii::app()->createUrl("/dictionary/dictionaryDataBackend/create", array("group_id" => $data->id))',
                        'options' => ['class' => 'btn btn-sm btn-default']
                    ],
                ],
            ],
        ],
    ]
); ?>
