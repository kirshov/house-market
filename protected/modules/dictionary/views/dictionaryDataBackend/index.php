<?php
/**
 * @var DictionaryGroup $group
 * @var DictionaryData $model
 */
$request = Yii::app()->getRequest();

$this->breadcrumbs = [
    Yii::t('DictionaryModule.dictionary', 'Dictionaries')     => ['/dictionary/dictionaryBackend/index'],
	mb_strimwidth(strip_tags($group->name), 0, 30, '...') => ['/dictionary/dictionaryDataBackend/index', 'group_id' => $group->id],
    Yii::t('DictionaryModule.dictionary', 'Management'),
];

$this->pageTitle = Yii::t('DictionaryModule.dictionary', 'Dictionary items - management');

$this->menu = [
    [
        'label' => Yii::t('DictionaryModule.dictionary', 'Dictionaries'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('DictionaryModule.dictionary', 'Dictionaries management'),
                'url'   => ['/dictionary/dictionaryBackend/index']
            ],
        ]
    ],
    [
        'label' => Yii::t('DictionaryModule.dictionary', 'Items'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('DictionaryModule.dictionary', 'Items list'),
				'url'   => ['/dictionary/dictionaryDataBackend/index', 'group_id' => $request->getParam('group_id')],
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('DictionaryModule.dictionary', 'Create item'),
                'url'   => ['/dictionary/dictionaryDataBackend/create', 'group_id' => $request->getParam('group_id')]
            ],
        ]
    ],
];
?>

<?php $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id'           => 'dictionary-data-grid',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'bulkButtonsParams' => ['group_id' => $group->id],
	    'sortableRows'      => true,
	    'sortableAjaxSave'  => true,
	    'sortableAttribute' => 'position',
	    'sortableAction'    => '/dictionary/dictionaryDataBackend/sortable',
        'columns'      => [
	        [
		        'type' => 'raw',
		        'value' => function ($data) {
    	            if(!$data->image){
    	            	return '';
	                }
			        return CHtml::image($data->getImageUrl(100, 100), $data->name, ["width" => 100, "height" => 100, "class" => "img-thumbnail"]);
		        },
		        'htmlOptions' => [
			        'style' => 'width: 80px;',
		        ],
		        'filter' => CHtml::activeTextField($model, 'id', ['class' => 'form-control']),
	        ],
            [
                'class'    => 'webforma\widgets\WEditableColumn',
                'name'     => 'name',
                'editable' => [
                    'url'    => $this->createUrl('/dictionary/dictionaryDataBackend/inline'),
                    'mode'   => 'popup',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'name', ['class' => 'form-control']),
            ],
            [
                'class'    => 'webforma\widgets\WEditableColumn',
                'editable' => [
                    'url'    => $this->createUrl('/dictionary/dictionaryDataBackend/inline'),
                    'mode'   => 'popup',
                    'type'   => 'select',
                    'source' => CHtml::listData(DictionaryGroup::model()->findAll(), 'id', 'name'),
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'name'     => 'group_id',
                'type'     => 'raw',
                'value'    => '$data->group->name',
                'filter'   => CHtml::activeDropDownList(
                    $model,
                    'group_id',
                    CHtml::listData(DictionaryGroup::model()->findAll(), 'id', 'name'),
                    ['class' => 'form-control', 'empty' => '']
                ),
				'visible' => $request->getParam('group_id') == null,
            ],
            [
                'class'   => 'webforma\widgets\EditableStatusColumn',
                'name'    => 'status',
                'url'     => $this->createUrl('/dictionary/dictionaryDataBackend/inline'),
                'source'  => $model->getStatusList(),
                'options' => [
                    DictionaryData::STATUS_ACTIVE  => ['class' => 'label-success'],
                    DictionaryData::STATUS_DELETED => ['class' => 'label-default'],
                ],
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
				'template' => '{update} {delete}',
				'buttons' => [
					'update' => [
						'url' => 'Yii::app()->createUrl("/dictionary/dictionaryDataBackend/update", array("id" => $data->id, "group_id" => $data->group_id))',
					]
				],
            ],
        ],
    ]
); ?>
