<?php
/**
 * @var $this DictionaryDataBackendController
 * @var $model DictionaryData
 * @var $form \webforma\widgets\ActiveForm
 */
$form = $this->beginWidget(
    'webforma\widgets\ActiveForm',
    [
        'id'                     => 'dictionary-data-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'type'                   => 'vertical',
	    'htmlOptions' 			 => ['class' => 'well sticky', 'enctype' => 'multipart/form-data'],
    ]
); ?>
<div class="alert alert-info">
    <?=  Yii::t('DictionaryModule.dictionary', 'Fields with'); ?>
    <span class="required">*</span>
    <?=  Yii::t('DictionaryModule.dictionary', 'are required.'); ?>
</div>

<?=  $form->errorSummary($model); ?>

<div class='row'>
    <div class="col-sm-6">
        <?=  $form->textFieldGroup($model, 'name'); ?>
    </div>
</div>
<?if($model->showImage):?>
	<div class='row'>
		<div class="col-sm-6">
			<?php $this->widget('webforma\widgets\WInputFile', [
				'model' => $model,
				'attribute' => 'image',
			]); ?>
		</div>
	</div>
<?endif?>
<?if($model->fields){
	foreach ($model->fields as $item){
		echo CHtml::openTag('div', ['class' => 'row']).CHtml::openTag('div', ['class' => 'col-sm-6']);
		switch ($model->getFieldType($item)){
			case 'text':
				echo $form->textFieldGroup($model, $item);
				break;

			case 'textarea':
				echo $form->textAreaGroup($model, $item);
				break;

			case 'checkbox':
				echo $form->checkboxGroup($model, $item);
				break;

			case 'editor':
				echo $form->labelEx($model, $item);
				$this->widget($this->webforma->getVisualEditor(), [
					'model'     => $model,
					'attribute' => $item,
				]);
				echo '<br/>';
				break;
		}
		echo CHtml::closeTag('div').CHtml::closeTag('div');
	}
}?>
<?if($model->showDescription):?>
	<br/>
	<div class="row">
		<div class="col-sm-12 form-group">
			<?=  $form->labelEx($model, 'description'); ?>
			<?php $this->widget($this->webforma->getVisualEditor(), [
				'model'     => $model,
				'attribute' => 'description',
				'options' => [
					'rows' => '10',
				],
			]); ?>
		</div>
	</div>
<?endif?>

<div class="row">
	<div class="col-sm-3">
		<?= $form->dropDownListGroup($model, 'status', [
			'widgetOptions' => [
				'data' => $model->getStatusList(),
			]
		]); ?>
	</div>
</div>


<div class="buttons">
	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context'    => 'primary',
		'label'      => $model->isNewRecord
			? Yii::t('DictionaryModule.dictionary', 'Create item and continue')
			: Yii::t('DictionaryModule.dictionary', 'Save value and continue'),
	]); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', [
		'buttonType'  => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label'       => $model->isNewRecord
			? Yii::t('DictionaryModule.dictionary', 'Create item and close')
			: Yii::t('DictionaryModule.dictionary', 'Save value and close'),
	]); ?>
</div>
<?php $this->endWidget(); ?>
