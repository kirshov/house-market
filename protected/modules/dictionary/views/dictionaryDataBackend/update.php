<?php
/**
 * @var DictionaryGroup $group
 * @var DictionaryData $model
 */
$request = Yii::app()->getRequest();

$this->breadcrumbs = [
    Yii::t('DictionaryModule.dictionary', 'Dictionaries')     => ['/dictionary/dictionaryBackend/index'],
    mb_strimwidth(strip_tags($group->name), 0, 30, '...') => ['/dictionary/dictionaryDataBackend/index', 'group_id' => $group->id],
	mb_strimwidth(strip_tags($model->name), 0, 30, '...') . ' - ' .Yii::t('DictionaryModule.dictionary', 'Edit'),
];

$this->pageTitle = Yii::t('DictionaryModule.dictionary', 'Dictionary items - edit');

$this->menu = [
    [
        'label' => Yii::t('DictionaryModule.dictionary', 'Dictionaries'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('DictionaryModule.dictionary', 'Dictionaries management'),
                'url'   => ['/dictionary/dictionaryBackend/index']
            ],
        ]
    ],
    [
        'label' => Yii::t('DictionaryModule.dictionary', 'Items'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('DictionaryModule.dictionary', 'Items list'),
                'url'   => ['/dictionary/dictionaryDataBackend/index', 'group_id' => $group->id]
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('DictionaryModule.dictionary', 'Create item'),
                'url'   => ['/dictionary/dictionaryDataBackend/create', 'group_id' => $group->id]
            ],
            [
                'label' => Yii::t('DictionaryModule.dictionary', 'Dictionary item') . ' «' . mb_substr(
                        $model->name,
                        0,
                        32
                    ) . '»'
            ],
            [
                'icon'  => 'fa fa-fw fa-pencil',
                'label' => Yii::t('DictionaryModule.dictionary', 'Edit dictionary item'),
                'url'   => [
                    '/dictionary/dictionaryDataBackend/update',
                    'id' => $model->id,
                    'group_id' => $group->id,
                ]
            ],
            [
                'icon'        => 'fa fa-fw fa-trash-o',
                'label'       => Yii::t('DictionaryModule.dictionary', 'Remove dictionary item'),
                'url'         => '#',
                'linkOptions' => [
                    'submit'  => ['/dictionary/dictionaryDataBackend/delete', 'id' => $model->id],
                    'confirm' => Yii::t('DictionaryModule.dictionary', 'Do you really want do delete dictionary item?'),
                    'csrf'    => true,
                ]
            ],
        ]
    ],
];
?>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>
