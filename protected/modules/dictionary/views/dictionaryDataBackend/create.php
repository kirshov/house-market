<?php
/**
 * @var DictionaryGroup $group
 * @var DictionaryData $model
 */
$request = Yii::app()->getRequest();

$this->breadcrumbs = [
    Yii::t('DictionaryModule.dictionary', 'Dictionaries')     => ['/dictionary/dictionaryBackend/index'],
    mb_strimwidth(strip_tags($group->name), 0, 30, '...') => ['/dictionary/dictionaryDataBackend/index', 'group_id' => $group->id],
    Yii::t('DictionaryModule.dictionary', 'Create'),
];

$this->pageTitle = Yii::t('DictionaryModule.dictionary', 'Dictionary items - create');

$this->menu = [
    [
        'label' => Yii::t('DictionaryModule.dictionary', 'Dictionaries'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('DictionaryModule.dictionary', 'Dictionaries management'),
                'url'   => ['/dictionary/dictionaryBackend/index']
            ],
        ]
    ],
    [
        'label' => Yii::t('DictionaryModule.dictionary', 'Items'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('DictionaryModule.dictionary', 'Items list'),
                'url'   => ['/dictionary/dictionaryDataBackend/index', 'group_id' => $request->getParam('group_id')],
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('DictionaryModule.dictionary', 'Create item'),
                'url'   => ['/dictionary/dictionaryDataBackend/create', 'group_id' => $request->getParam('group_id')]
            ],
        ]
    ],
];
?>
<?=  $this->renderPartial('_form', ['model' => $model]); ?>
