<?php

/**
 * File Doc Comment
 * Vote install migration
 * Класс миграций для модуля Vote
 **/
class m000000_000000_vote_base extends webforma\components\DbMigration
{

    public function safeUp()
    {
        $this->createTable(
            '{{vote}}',
            [
                'id'          => 'pk',
                'name'        => 'varchar(255) NOT NULL',
                'description' => 'varchar(255) NOT NULL',
	            'sort'        => "integer NOT NULL DEFAULT '1'",
                'status'      => "integer NOT NULL DEFAULT '1'",
            ],
            $this->getOptions()
        );

        //ix
        $this->createIndex("ix_{{vote}}_status", '{{vote}}', "status", false);
        /**
         * vote_item:
         **/
        $this->createTable(
            '{{vote_item}}',
            [
                'id'               => 'pk',
                'vote_id'       => 'integer NOT NULL',
                'title'            => 'varchar(150) NOT NULL',
                'votes'            => "integer NOT NULL DEFAULT '0'",
                'sort'             => "integer NOT NULL DEFAULT '1'",
                'status'           => "integer NOT NULL DEFAULT '1'",
            ],
            $this->getOptions()
        );

        $this->createIndex("ix_{{vote_item}}_vote_id", '{{vote_item}}', "vote_id", false);
        $this->createIndex("ix_{{vote_item}}_sort", '{{vote_item}}', "sort", false);
        $this->createIndex("ix_{{vote_item}}_status", '{{vote_item}}', "status", false);

        //fk
        $this->addForeignKey(
            "fk_{{vote_item}}_vote_id",
            '{{vote_item}}',
            'vote_id',
            '{{vote}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{vote_item}}');
        $this->dropTableWithForeignKeys('{{vote_gallery}}');
    }
}
