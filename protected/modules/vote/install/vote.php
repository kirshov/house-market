<?php
/**
 * Файл конфигурации модуля
 **/
return [
    'module'    => [
        'class' => 'application.modules.vote.VoteModule',
    ],
    'import'    => [],
    'component' => [],
	'params'    => [
		'widgets'   => [
			'vote' => 'application.modules.vote.widgets.VoteWidget',
		],
	],
    'rules'     => [
	    '/set-vote/' => 'vote/vote/set/',
	    //'/vote/' => 'vote/vote/index/',
	    //'/vote/<id:\d+>' => '/vote/vote/view',
    ],
];
