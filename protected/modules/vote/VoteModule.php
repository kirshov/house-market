<?php

/**
 * VoteModule основной класс модуля vote
 */
class VoteModule extends webforma\components\WebModule
{
    /**
     * @var string
     */
    public $defaultController = 'vote';

    /**
     * @var string
     */
    public $voteCache = 'vote.cache';

	/**
	 * @var int порядок следования модуля в меню панели управления (сортировка)
	 */
	public $adminMenuOrder = 200;

	/**
	 * @var int
	 */

    /**
     * @return string
     */
    public function getCategory()
    {
        return 'Контент';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('VoteModule.vote', 'Vote');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('VoteModule.vote', 'Vote management module');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-fw fa-list-ol';
    }

    /**
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/vote/voteBackend/index';
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            ['label' => Yii::t('VoteModule.vote', 'Vote')],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VoteModule.vote', 'Manage vote'),
                'url' => ['/vote/voteBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VoteModule.vote', 'Create vote'),
                'url' => ['/vote/voteBackend/create'],
            ],
            ['label' => Yii::t('VoteModule.vote', 'Vote items')],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VoteModule.vote', 'Manage vote items'),
                'url' => ['/vote/voteitemBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VoteModule.vote', 'Create vote item'),
                'url' => ['/vote/voteitemBackend/create'],
            ],
        ];
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'application.modules.vote.models.*',
            ]
        );
    }

    /**
     * @return array
     */
    public function getAuthItems()
    {
        return [
            [
                'name' => 'Vote.VoteManager',
                'description' => Yii::t('VoteModule.vote', 'Manage votes'),
                'type' => AuthItem::TYPE_TASK,
                'items' => [
                    //vote
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Vote.VoteBackend.Create',
                        'description' => Yii::t('VoteModule.vote', 'Creating vote'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Vote.VoteBackend.Delete',
                        'description' => Yii::t('VoteModule.vote', 'Removing vote'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Vote.VoteBackend.Index',
                        'description' => Yii::t('VoteModule.vote', 'List of vote'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Vote.VoteBackend.Update',
                        'description' => Yii::t('VoteModule.vote', 'Editing vote'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Vote.VoteBackend.View',
                        'description' => Yii::t('VoteModule.vote', 'Viewing vote'),
                    ],
                    //vote items
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Vote.VoteitemBackend.Create',
                        'description' => Yii::t('VoteModule.vote', 'Creating vote item'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Vote.VoteitemBackend.Delete',
                        'description' => Yii::t('VoteModule.vote', 'Removing vote item'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Vote.VoteitemBackend.Index',
                        'description' => Yii::t('VoteModule.vote', 'List of vote items'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Vote.VoteitemBackend.Update',
                        'description' => Yii::t('VoteModule.vote', 'Editing vote items'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Vote.VoteitemBackend.View',
                        'description' => Yii::t('VoteModule.vote', 'Viewing vote items'),
                    ],
                ],
            ],
        ];
    }
}
