<?php
/**
 * Файл представления vote/update
 **/
$this->breadcrumbs = [
    Yii::t('VoteModule.vote', 'Vote') => ['/vote/voteBackend/index'],
    $model->name                      => ['/vote/voteBackend/view', 'id' => $model->id],
    Yii::t('VoteModule.vote', 'Edit'),
];

$this->pageTitle = Yii::t('VoteModule.vote', 'Vote - edit');

$this->menu = [
    [
        'label' => Yii::t('VoteModule.vote', 'Vote'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VoteModule.vote', 'Manage vote'),
                'url'   => ['/vote/voteBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VoteModule.vote', 'Create vote'),
                'url'   => ['/vote/voteBackend/create']
            ],
            [
                'icon'  => 'fa fa-fw fa-pencil',
                'label' => Yii::t('VoteModule.vote', 'Change vote'),
                'url'   => [
                    '/vote/voteBackend/update',
                    'id' => $model->id
                ]
            ],
            [
                'icon'        => 'fa fa-fw fa-trash-o',
                'label'       => Yii::t('VoteModule.vote', 'Remove vote'),
                'url'         => '#',
                'linkOptions' => [
                    'submit'  => ['/vote/voteBackend/delete', 'id' => $model->id],
                    'confirm' => Yii::t('VoteModule.vote', 'Do you really want to remove vote?'),
                    'params'  => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
                ],
            ],
        ]
    ],
    [
        'label' => Yii::t('VoteModule.vote', 'Vote items'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VoteModule.vote', 'Manage vote items'),
                'url'   => ['/vote/voteitemBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VoteModule.vote', 'Create vote item'),
                'url'   => ['/vote/voteitemBackend/create']
            ],
        ]
    ],
]; ?>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>
