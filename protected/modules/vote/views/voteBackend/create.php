<?php
/**
 * Файл представления vote/create:
 **/
$this->breadcrumbs = [
    Yii::t('VoteModule.vote', 'Vote') => ['/vote/voteBackend/index'],
    Yii::t('VoteModule.vote', 'Create'),
];

$this->pageTitle = Yii::t('VoteModule.vote', 'Vote - insert');

$this->menu = [
    [
        'label' => Yii::t('VoteModule.vote', 'Vote'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VoteModule.vote', 'Manage vote'),
                'url'   => ['/vote/voteBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VoteModule.vote', 'Create vote'),
                'url'   => ['/vote/voteBackend/create']
            ],
        ]
    ],
    [
        'label' => Yii::t('VoteModule.vote', 'Vote items'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VoteModule.vote', 'Manage vote items'),
                'url'   => ['/vote/voteitemBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VoteModule.vote', 'Create vote item'),
                'url'   => ['/vote/voteitemBackend/create']
            ],
        ]
    ],
];
?>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>
