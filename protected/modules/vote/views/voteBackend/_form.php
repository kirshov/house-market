<?php
$form = $this->beginWidget(
    'webforma\widgets\ActiveForm',
    [
        'id'                     => 'vote-form',
	    'enableAjaxValidation' => false,
	    'enableClientValidation' => false,
	    'type' => 'vertical',
	    'htmlOptions' => ['class' => 'well sticky'],
	    'clientOptions' => [
		    'validateOnSubmit' => true,
	    ],
    ]
); ?>
<div class="alert alert-info">
    <?=  Yii::t('VoteModule.vote', 'Fields with'); ?>
    <span class="required">*</span>
    <?=  Yii::t('VoteModule.vote', 'are required.'); ?>
</div>

<?=  $form->errorSummary($model); ?>

<div class="row">
    <div class="col-sm-6">
        <?=  $form->textFieldGroup($model, 'name'); ?>
    </div>
</div>
<?/*
<div class="row">
    <div class="col-sm-6">
        <?=  $form->slugFieldGroup($model, 'slug', [
			'sourceAttribute' => 'name',
		]); ?>
    </div>
</div>


<div class="row">
    <div class="col-sm-12">
	    <?= $form->labelEx($model, 'description'); ?>
		<?
		$this->widget($this->module->getVisualEditor(), [
			'model'     => $model,
			'attribute' => 'description',
		]); ?>
    </div>
</div>
*/?>
<div class="row">
    <div class="col-sm-3">
        <?=  $form->dropDownListGroup($model, 'status', [
			'widgetOptions' => [
				'data'        => $model->getStatusList(),
			],
		]); ?>
    </div>
</div>

<div class="buttons">
	<?php
	$this->widget('bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'context'    => 'primary',
		'label'      => $model->getIsNewRecord()
			? Yii::t('VoteModule.vote', 'Create vote and continue')
			: Yii::t('VoteModule.vote', 'Save vote and continue'),
	]); ?>

	<?php
	$this->widget('bootstrap.widgets.TbButton', [
		'buttonType'  => 'submit',
		'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
		'label'       => $model->getIsNewRecord()
			? Yii::t('VoteModule.vote', 'Create vote and close')
			: Yii::t('VoteModule.vote', 'Save vote and close'),
	]); ?>
</div>
<?php $this->endWidget(); ?>
