<?php
/**
 * Файл представления vote/index
 **/
$this->breadcrumbs = [
    Yii::t('VoteModule.vote', 'Vote') => ['/vote/voteBackend/index'],
    Yii::t('VoteModule.vote', 'Manage')
];

$this->pageTitle = Yii::t('VoteModule.vote', 'Vote - manage');

$this->menu = [
    [
        'label' => Yii::t('VoteModule.vote', 'Vote'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VoteModule.vote', 'Manage vote'),
                'url'   => ['/vote/voteBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VoteModule.vote', 'Create vote'),
                'url'   => ['/vote/voteBackend/create']
            ],
        ]
    ],
    [
        'label' => Yii::t('VoteModule.vote', 'Vote items'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VoteModule.vote', 'Manage vote items'),
                'url'   => ['/vote/voteitemBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VoteModule.vote', 'Create vote item'),
                'url'   => ['/vote/voteitemBackend/create']
            ],
        ]
    ],
];
?>
<?php $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id'           => 'vote-grid',
        'sortableRows'      => true,
        'sortableAjaxSave'  => true,
        'sortableAttribute' => 'position',
        'sortableAction'    => '/vote/voteBackend/sortable',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => [
            [
                'class'    => 'webforma\widgets\WEditableColumn',
                'name'     => 'name',
                'editable' => [
                    'url'    => $this->createUrl('/vote/voteBackend/inline'),
                    'mode'   => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'name', ['class' => 'form-control']),
            ],
            [
                'class'   => 'webforma\widgets\EditableStatusColumn',
                'name'    => 'status',
                'url'     => $this->createUrl('/vote/voteBackend/inline'),
                'source'  => $model->getStatusList(),
                'options' => [
                    Vote::STATUS_ACTIVE   => ['class' => 'label-success'],
                    Vote::STATUS_DISABLED => ['class' => 'label-default'],
                ],
            ],
            [
                'header' => Yii::t('VoteModule.vote', 'Items'),
                'type'   => 'raw',
                'value'  => 'CHtml::link(count($data->voteItems), Yii::app()->createUrl("/vote/voteitemBackend/index", array("VoteItem[vote_id]" => $data->id)))',
            ],
            [
                'class'    => 'webforma\widgets\CustomButtonColumn',
                'template' => '{update}{delete}{add}',
                'buttons'  => [
                    'add' => [
                        'icon'  => 'fa fa-fw fa-plus-square',
                        'label' => Yii::t('VoteModule.vote', 'Create vote item'),
                        'url'   => 'Yii::app()->createUrl("/vote/voteitemBackend/create", array("mid" => $data->id))',
                        'options' => ['class' => 'btn btn-sm btn-default']
                    ],
                ],
            ],
        ],
    ]
); ?>
