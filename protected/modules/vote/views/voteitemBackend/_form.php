<?php
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'id' => 'vote-item-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'type' => 'vertical',
        'htmlOptions' => ['class' => 'well sticky'],
    ]
); ?>
<div class="alert alert-info">
    <?= Yii::t('VoteModule.vote', 'Fields with'); ?>
    <span class="required">*</span>
    <?= Yii::t('VoteModule.vote', 'are required.'); ?>
</div>

<?= $form->errorSummary($model); ?>

<div class="row">
    <div class="col-sm-6">
        <?= $form->dropDownListGroup(
            $model,
            'vote_id',
            [
                'widgetOptions' => [
                    'data' => CHtml::listData(Vote::model()->findAll(), 'id', 'name'),
                    'htmlOptions' => [
                        'empty' => Yii::t('VoteModule.vote', '--choose vote--'),
                    ],
                ],
            ]
        ); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?= $form->textFieldGroup($model, 'title'); ?>
    </div>
</div>
<div class="row">
	<div class="col-sm-3">
		<?= $form->textFieldGroup($model, 'votes'); ?>
	</div>

    <div class="col-sm-3">
        <?= $form->dropDownListGroup($model, 'status', [
			'widgetOptions' => [
				'data' => $model->getStatusList(),
			],
		]); ?>
    </div>
</div>

<div class="row hidden">
    <div class="col-sm-6">
        <?= $form->textFieldGroup($model, 'sort'); ?>
    </div>
</div>


<div class="buttons">
	<?php $this->widget(
		'bootstrap.widgets.TbButton',
		[
			'buttonType' => 'submit',
			'context' => 'primary',
			'label' => $model->getIsNewRecord() ? Yii::t('VoteModule.vote', 'Create vote item and continue') : Yii::t(
				'VoteModule.vote',
				'Save vote item and continue'
			),
		]
	); ?>

	<?php $this->widget(
		'bootstrap.widgets.TbButton',
		[
			'buttonType' => 'submit',
			'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
			'label' => $model->getIsNewRecord() ? Yii::t('VoteModule.vote', 'Create vote item and close') : Yii::t(
				'VoteModule.vote',
				'Save vote item and close'
			),
		]
	); ?>
</div>
<?php $this->endWidget(); ?>
