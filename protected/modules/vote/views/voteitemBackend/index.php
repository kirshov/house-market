<?php
$this->breadcrumbs = [
    Yii::t('VoteModule.vote', 'Vote') => ['/vote/voteBackend/index'],
    Yii::t('VoteModule.vote', 'Vote items'),
];

$this->pageTitle = Yii::t('VoteModule.vote', 'Vote items - remove');

$this->menu = [
    [
        'label' => Yii::t('VoteModule.vote', 'Vote'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VoteModule.vote', 'Manage vote'),
                'url'   => ['/vote/voteBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VoteModule.vote', 'Create vote'),
                'url'   => ['/vote/voteBackend/create']
            ],
        ]
    ],
    [
        'label' => Yii::t('VoteModule.vote', 'Vote items'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VoteModule.vote', 'Manage vote items'),
                'url'   => ['/vote/voteitemBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VoteModule.vote', 'Create vote item'),
                'url'   => ['/vote/voteitemBackend/create']
            ],
        ]
    ],
];
?>


<?php $this->widget(
    'webforma\widgets\CustomGridView',
    [
        'id'                => 'vote-items-grid',
        'sortableRows'      => true,
        'sortableAjaxSave'  => true,
        'sortableAttribute' => 'sort',
        'sortableAction'    => '/vote/voteitemBackend/sortable',
        'dataProvider'      => $model->search(),
        'filter'            => $model,
        'actionsButtons'    => [
            CHtml::link(
                Yii::t('WebformaModule.webforma', 'Add'),
                ['/vote/voteitemBackend/create'],
                ['class' => 'btn btn-success pull-right btn-sm']
            )
        ],
        'columns'           => [
            [
                'class'    => 'webforma\widgets\WEditableColumn',
                'name'     => 'title',
                'editable' => [
                    'url'    => $this->createUrl('/vote/voteitemBackend/inline'),
                    'mode'   => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'title', ['class' => 'form-control']),
            ],
            [
                'name'   => 'vote_id',
                'value'  => '$data->vote->name',
                'filter' => CHtml::listData(Vote::model()->findAll(), 'id', 'name')
            ],
            [
                'class'   => 'webforma\widgets\EditableStatusColumn',
                'name'    => 'status',
                'url'     => $this->createUrl('/vote/voteitemBackend/inline'),
                'source'  => $model->getStatusList(),
                'options' => [
                    VoteItem::STATUS_ACTIVE   => ['class' => 'label-success'],
                    VoteItem::STATUS_DISABLED => ['class' => 'label-default'],
                ],
            ],
            [
                'name'   => 'votes',
                'filter' => false,
            ],
            [
                'class' => 'webforma\widgets\CustomButtonColumn',
            ],
        ],
    ]
); ?>
