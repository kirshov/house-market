<?php
$this->breadcrumbs = [
    Yii::t('VoteModule.vote', 'Vote')       => ['/vote/voteBackend/index'],
    Yii::t('VoteModule.vote', 'Vote items') => ['/vote/voteitemBackend/index'],
    $model->title                           => ['/vote/voteitemBackend/view', 'id' => $model->id],
    Yii::t('VoteModule.vote', 'Edit'),
];

$this->pageTitle = Yii::t('VoteModule.vote', 'Vote items - edit');

$this->menu = [
    [
        'label' => Yii::t('VoteModule.vote', 'Vote'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VoteModule.vote', 'Manage vote'),
                'url'   => ['/vote/voteBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VoteModule.vote', 'Create vote'),
                'url'   => ['/vote/voteBackend/create']
            ],
        ]
    ],
    [
        'label' => Yii::t('VoteModule.vote', 'Vote items'),
        'items' => [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VoteModule.vote', 'Manage vote items'),
                'url'   => ['/vote/voteitemBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VoteModule.vote', 'Create vote item'),
                'url'   => ['/vote/voteitemBackend/create']
            ],
            ['label' => Yii::t('VoteModule.vote', 'Vote item') . ' «' . $model->title . '»'],
            [
                'icon'  => 'fa fa-fw fa-pencil',
                'label' => Yii::t('VoteModule.vote', 'Change vote item'),
                'url'   => [
                    '/vote/voteitemBackend/update',
                    'id' => $model->id
                ]
            ],
            [
                'icon'  => 'fa fa-fw fa-eye',
                'label' => Yii::t('VoteModule.vote', 'View vote item'),
                'url'   => [
                    '/vote/voteitemBackend/view',
                    'id' => $model->id
                ]
            ],
            [
                'icon'        => 'fa fa-fw fa-trash-o',
                'label'       => Yii::t('VoteModule.vote', 'Remove vote item'),
                'url'         => '#',
                'linkOptions' => [
                    'submit'  => ['/vote/voteitemBackend/delete', 'id' => $model->id],
                    'confirm' => Yii::t('VoteModule.vote', 'Do you really want to remove vote item?'),
                    'params'  => [Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken],
                ],
            ],
        ]
    ],
];
?>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>
