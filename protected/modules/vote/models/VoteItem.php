<?php
/**
 * Vote основная модель для пунктов меню
 */

/**
 * This is the model class for table "vote_item".
 *
 * The followings are the available columns in table 'vote_item':
 * @property string $id
 * @property string $vote_id
 * @property string $title
 * @property integer $votes
 * @property integer $sort
 * @property integer $status

 * The followings are the available model relations:
 * @property Vote $vote
 */
class VoteItem extends webforma\models\WModel
{
    /**
     * @var bool
     */
    public $regular_link = true;

    /**
     *
     */
    const STATUS_DISABLED = 0;
    /**
     *
     */
    const STATUS_ACTIVE = 1;

	/**
	 * @var int
	 */
    public $percent = 0;
    /**
     * Returns the static model of the specified AR class.
     * @param  string $className active record class name.
     * @return VoteItem the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{vote_item}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['vote_id, title', 'required', 'except' => 'search'],
            ['sort, status, votes', 'numerical', 'integerOnly' => true],
            [
                'id, vote_id, title',
                'safe',
                'on' => 'search',
            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'vote' => [self::BELONGS_TO, 'Vote', 'vote_id'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'sortable' => [
                'class' => 'webforma\components\behaviors\SortableBehavior',
                'attributeName' => 'sort',
            ],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('VoteModule.vote', 'Id'),
            'vote_id' => Yii::t('VoteModule.vote', 'Vote'),
            'title' => Yii::t('VoteModule.vote', 'Title'),
            'votes' => 'Количество проголосовавших',
            'sort' => Yii::t('VoteModule.vote', 'Sorting'),
            'status' => Yii::t('VoteModule.vote', 'Status'),
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria();

        $criteria->compare('t.id', $this->id, true);
        $criteria->compare('t.vote_id', $this->vote_id, true);
        $criteria->compare('t.title', $this->title, true);


        $criteria->compare('t.sort', $this->sort);
        $criteria->compare('t.status', $this->status);
        $criteria->with = ['vote'];

        return new CActiveDataProvider(
            get_class($this), [
                'criteria' => $criteria,
                'sort' => ['defaultOrder' => 't.sort'],
            ]
        );
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'public' => [
                'condition' => 'status = :status',
                'params' => [
                    ':status' => self::STATUS_ACTIVE,
                ],
            ],
        ];
    }


    /**
     *
     */
    protected function afterSave()
    {
        return parent::afterSave();
    }

    /**
     *
     */
    protected function afterDelete()
    {
        return parent::afterDelete();
    }


    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('VoteModule.vote', 'active'),
            self::STATUS_DISABLED => Yii::t('VoteModule.vote', 'not active'),
        ];
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return array_key_exists($this->status, $data) ? $data[$this->status] : $this->status;
    }
}
