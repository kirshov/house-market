<?php
/**
 * Vote основная модель для vote
 */
use webforma\widgets\WPurifier;

/**
 * This is the model class for table "vote".
 *
 * The followings are the available columns in table 'vote':
 * @property string $id
 * @property string $image
 * @property string $name
 * @property string $description
 * @property integer $status
 * @property integer $sort
 *
 * The followings are the available model relations:
 * @property VoteItem[] $voteItems
 *
 * @method Vote active()
 */
class Vote extends webforma\models\WModel
{
    /**
     *
     */
    const STATUS_DISABLED = 0;
    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     * Returns the static model of the specified AR class.
     * @param  string $className active record class name.
     * @return Vote   the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{vote}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['name', 'required', 'except' => 'search'],
            ['status, sort', 'numerical', 'integerOnly' => true],
            ['name', 'filter', 'filter' => [$obj = new WPurifier(), 'purify']],
            ['name, description', 'length', 'max' => 255],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            ['id, name, description, status', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'voteItems' => [self::HAS_MANY, 'VoteItem', 'vote_id', 'condition' => 'voteItems.status = '.VoteItem::STATUS_ACTIVE, 'order' => 'voteItems.sort ASC'],
        ];
    }

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'sortable' => [
				'class' => 'webforma\components\behaviors\SortableBehavior',
				'attributeName' => 'sort',
			],
		];
	}

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('VoteModule.vote', 'Id'),
            'name' => Yii::t('VoteModule.vote', 'Name'),
            'image' => 'Изображение',
            'description' => Yii::t('VoteModule.vote', 'Description'),
            'status' => Yii::t('VoteModule.vote', 'Status'),
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria();

        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider(
            get_class($this), [
                'criteria' => $criteria,
		        'sort' => ['defaultOrder' => 't.sort'],
            ]
        );
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'active' => [
                'condition' => 't.status = :status',
                'params' => [
                    ':status' => self::STATUS_ACTIVE,
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_DISABLED => Yii::t('VoteModule.vote', 'not active'),
            self::STATUS_ACTIVE => Yii::t('VoteModule.vote', 'active'),
        ];
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return array_key_exists($this->status, $data) ? $data[$this->status] : $this->status;
    }

	/**
	 *
	 */
    public function calculateResult(){
    	if($this->voteItems){
    		$allVotes = 0;
    		foreach ($this->voteItems as $voteItem){
    			$allVotes += $voteItem->votes;
		    }

    		$allPercent = 0;
    		if($allVotes){
			    foreach ($this->voteItems as $voteItem){
				    $voteItem->percent = round($voteItem->votes * 100 / $allVotes);
				    $allPercent += $voteItem->percent;
			    }

			    if($allPercent > 100){
				    $this->voteItems[0]->percent -= ($allPercent - 100);
			    } elseif($allPercent < 100){
				    $this->voteItems[0]->percent += (100 - $allPercent);
			    }
		    }
	    }
    }
}
