<?php

/**
 * VoteBackendController контроллер для управления меню в панели управления
 */
class VoteBackendController extends webforma\components\controllers\BackController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Vote.VoteBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Vote.VoteBackend.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['Vote.VoteBackend.Create']],
            ['allow', 'actions' => ['update', 'inline'], 'roles' => ['Vote.VoteBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Vote.VoteBackend.Delete']],
            ['deny']
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'inline' => [
                'class'           => 'webforma\components\actions\WInLineEditAction',
                'model'           => 'Vote',
                'validAttributes' => ['name', 'status', 'description']
            ],
	         'sortable' => [
			    'class' => 'webforma\components\actions\SortAction',
			    'model' => 'Vote',
			    'attribute' => 'sort',
		    ],
        ];
    }

    /**
     * Отображает меню по указанному идентификатору
     * @param integer $id Идинтификатор меню для отображения
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);

        $code = "<?php \$this->widget('vote', ['name' => '{$model->code}']); ?>";

        $highlighter = new CTextHighlighter();
        $highlighter->language = 'PHP';
        $example = $highlighter->highlight($code);

        $this->render(
            'view',
            [
                'model'   => $model,
                'example' => $example,
            ]
        );
    }

    /**
     * Создает новую модель меню.
     * Если создание прошло успешно - перенаправляет на просмотр.
     */
    public function actionCreate()
    {
        $model = new Vote();

        if (($data = Yii::app()->getRequest()->getPost('Vote')) !== null) {

            $model->setAttributes($data);

            if ($model->save()) {
                Yii::app()->getUser()->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('VoteModule.vote', 'Vote was created!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['create']
                    )
                );
            }
        }

        $this->render('create', ['model' => $model]);
    }

    /**
     * Редактирование меню.
     * @param integer $id Идинтификатор меню для редактирования
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (($data = Yii::app()->getRequest()->getPost('Vote')) !== null) {

            $model->setAttributes($data);

            if ($model->save()) {

                Yii::app()->getUser()->setFlash(
                    webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('VoteModule.vote', 'Record was updated!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['update', 'id' => $model->id]
                    )
                );
            }
        }
        $this->render('update', ['model' => $model]);
    }


    /**
     * @param $id
     * @throws CDbException
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            Yii::app()->getUser()->setFlash(
                webforma\widgets\WFlashMessages::SUCCESS_MESSAGE,
                Yii::t('VoteModule.vote', 'Record was removed!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            Yii::app()->getRequest()->getParam('ajax') !== null || $this->redirect(
                (array)Yii::app()->getRequest()->getPost('returnUrl', 'index')
            );
        } else {
            throw new CHttpException(
                400,
                Yii::t('VoteModule.vote', 'Bad request. Please don\'t try similar requests anymore!')
            );
        }
    }

    /**
     * Управление блогами.
     *
     * @return void
     */
    public function actionIndex()
    {
        $model = new Vote('search');

        $model->unsetAttributes(); // clear any default values

        $model->setAttributes(
            Yii::app()->getRequest()->getParam(
                'Vote',
                []
            )
        );

        $this->render('index', ['model' => $model]);
    }

    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param integer идентификатор нужной модели
     *
     * @return Vote $model
     *
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        if (($model = Vote::model()->findByPk($id)) === null) {
            throw new CHttpException(
                404,
                Yii::t('VoteModule.vote', 'Page was not found!')
            );
        }

        return $model;
    }
}
