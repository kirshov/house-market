<?php

class VoteController extends \webforma\components\controllers\FrontController
{
	public function actionSet(){
		if(!Yii::app()->getRequest()->getIsAjaxRequest()){
			throw new CHttpException(404, Yii::t('WebformaModule.webforma', 'Page was not found!'));
		}
		$voteId = Yii::app()->getRequest()->getPost('vote');
		if(!$voteId){
			throw new CHttpException(404, Yii::t('WebformaModule.webforma', 'Page was not found!'));
		}

		$model = VoteItem::model()->findByPk($voteId);
		if(!$model){
			throw new CHttpException(404, Yii::t('WebformaModule.webforma', 'Page was not found!'));
		}

		$model->votes = $model->votes + 1;
		$model->save();

		\webforma\components\WCookie::setCookie('vote-'.$model->vote_id, 1);
		Yii::app()->getUser()->setState('vote-'.$model->vote_id, 1);

		$this->widget('vote', ['id' => $model->vote_id, 'isVoted' => true]);

		Yii::app()->end();
	}

	/**
     *
     */
    public function actionIndex()
    {
    }

	/**
	 * @param $id
	 * @throws CDbException
	 * @throws CHttpException
	 */
	public function actionView($id)
	{
	}
}
