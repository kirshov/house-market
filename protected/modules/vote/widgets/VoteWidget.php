<?php
Yii::import('application.modules.vote.models.*');

use webforma\widgets\WWidget;

class VoteWidget extends WWidget
{
	/**
	 * @var
	 */
	public $id;

	/**
	 * @var bool
	 */
	public $isVoted;

	public function run()
	{
		if($this->id){
			$vote = Vote::model()->active()->/*with('voteItems')->*/findByPk($this->id);
		} else {
			$criteria = new CDbCriteria();
			$criteria->compare('t.status', VoteItem::STATUS_ACTIVE);
			//$criteria->with = ['voteItems'];

			$criteria->order = 'RAND()';
			$vote = Vote::model()->find($criteria);
		}

		if($this->isVoted === null){
			$this->isVoted = \webforma\components\WCookie::getCookie('vote-'.$vote->id) || Yii::app()->getUser()->getState('vote-'.$vote->id);
		}

		if($this->isVoted){
			$vote->calculateResult();
		}

		$this->render('index', [
			'vote' =>  $vote,
			'isVoted' =>  $this->isVoted,
		]);
	}
}
