<?php
namespace application\controllers;

use webforma\components\controllers\FrontController;

class HomeController extends FrontController
{
    /**
     * Отображение главной страницы
     */
    public function actionIndex()
    {
        $this->render('index');
    }
}
