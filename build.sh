#!/usr/bin/env bash

init() {
	clean()
    chmod a+rw ./public/assets ./public/uploads ./protected/runtime
    chmod a+rw ./protected/config/db.php
    cp ./protected/config/db.back.php ./protected/config/db.php

    rm -R ./protected/config/modules/*
    cp ./protected/modules/install/install/install.php ./protected/config/modules/install.php
}

clean() {
    rm -R protected/runtime/cache/*
    rm -R protected/runtime/CSS/*
    rm -R protected/runtime/HTML/*
    rm -R protected/runtime/debug/*
    rm -R protected/runtime/URI/*
    rm -R protected/runtime/application.log*

    rm -R protected/runtime/*
    rm -R public/assets/*
}

showHelp() {
    echo "supported commands:"
    echo "init - proxy to init php"
    echo "clean - proxy to remove project runtime files"
}

case "$1" in
-h|--help)
    showHelp
    ;;
*)
    if [ ! -z $(type -t $1 | grep function) ]; then
        $1 $2
    else
        showHelp
    fi
    ;;
esac